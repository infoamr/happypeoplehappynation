<li class="form-element {#required}required{/required}" id="element-{position}" data-label="{label}" data-position="{position}" data-type="element-date">
    <label>
        <span class="label-title">{label}</span>
        {#required}<span class="required-star"> *</span>{/required}
    </label>
    <input type="date" class="form-control datepicker" value="" disabled />
</li>