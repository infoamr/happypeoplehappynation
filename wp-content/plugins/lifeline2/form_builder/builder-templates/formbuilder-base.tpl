<div id="master-container">
    <div id="form-container">
        <div class="container" id="tabs-container">
            <div class="left-col" id="toolbox-col" style="padding-top: 18px">
                <ul class="nav-tabs" role="tablist">
                    <li class="active toolbox-tab" data-target="#add-field">@add_fields@</li>
                    <li class="toolbox-tab" data-target="#field-settings">@field_setting@</li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" style="padding: 20px;" id="add-field">
                        <div class="col-sm-6">
                            <button class="button new-element" data-type="element-single-line-text" style="width: 100%;">@single_line_txt@</button>
                            <button class="button new-element" data-type="element-textarea" style="width: 100%;">@paragraph_txt@</button>
                            <button class="button new-element" data-type="element-multiple-choice" style="width: 100%;">@multiple_choice@</button>
                            <button class="button new-element" data-type="element-number" style="width: 100%;">@number@</button>
                        </div>
                        <div class="col-sm-6">
                            <button class="button new-element" data-type="element-checkboxes" style="width: 100%;">@checkboxes@</button>
                            <button class="button new-element" data-type="element-dropdown" style="width: 100%;">@dropdown@</button>
                            <button class="button new-element" data-type="element-email" style="width: 100%;">@email@</button>
                            <button class="button new-element" data-type="element-date" style="width: 100%;">@date@</button>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="tab-pane" id="field-settings" style="padding: 20px; display: none; margin: none;">
                        <div class="section">
                            <div class="form-group">
                                <label>@field_label@</label>
                                <input type="text" class="form-control" id="field-label" value="Untitled" />
                            </div>
                        </div>
                        <div class="section" id="field-choices" style="display: none;">
                            <div class="form-group">
                                <label>@field_choice@</label>
                            </div>
                        </div>
                        <div class="section" id="field-description"> 
                            <div class="form-group">
                                <label>@field_desc@</label>
                            </div>
                            <div class="field-description">
                                <textarea id="description"></textarea>
                            </div>
                        </div>
                        <button class="button danger" id="control-remove-field"><i class="dashicons-wp-app-delete"></i>@remove_field@</button>&nbsp;
                        <button class="button" id="control-add-field"><i class="dashicons-wp-app-plusicon"></i>@add_field@</button>
                    </div>
                </div>
            </div>
            <div class="right-col" id="form-col">
                <div class="loading">
                    @loading@...
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
<div style="clear: both"></div>
