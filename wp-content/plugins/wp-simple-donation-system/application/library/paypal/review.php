<?php
/* ==================================================================
  PayPal Express Checkout Call
  ===================================================================
 */
// Check to see if the Request object contains a variable named 'token'	
//if ( session_id() == '' ) session_start();
ob_start();
$token = "";
if ( isset( $_REQUEST['token'] ) ) {
    $token = WPD_Common::wpd_set( $_REQUEST, 'token' );
}

// If the Request object contains the variable 'token' then it means that the user is coming from PayPal site.	
if ( $token != "" ) {

    require_once ("paypalfunctions.php");
    $sh_express = new WPD_Express_checkout;
    /*
      '------------------------------------
      ' Calls the GetExpressCheckoutDetails API call
      '
      ' The GetShippingDetails function is defined in PayPalFunctions.jsp
      ' included at the top of this file.
      '-------------------------------------------------
     */


    $resArray = $sh_express->GetShippingDetails( $token );
    //printr($resArray);
    $ack      = strtoupper( WPD_Common::wpd_set( $resArray, "ACK" ) );
    if ( $ack == "SUCCESS" || $ack == "SUCESSWITHWARNING" ) {
        /*
          ' The information that is returned by the GetExpressCheckoutDetails call should be integrated by the partner into his Order Review
          ' page
         */
        $email                         = WPD_Common::wpd_set( $resArray, "EMAIL" ); // ' Email address of payer.
        $payerId                       = WPD_Common::wpd_set( $resArray, "PAYERID" ); // ' Unique PayPal customer account identification number.
        $payerStatus                   = WPD_Common::wpd_set( $resArray, "PAYERSTATUS" ); // ' Status of payer. Character length and limitations: 10 single-byte alphabetic characters.
        $salutation                    = WPD_Common::wpd_set( $resArray, "SALUTATION" ); // ' Payer's salutation.
        $firstName                     = WPD_Common::wpd_set( $resArray, "FIRSTNAME" ); // ' Payer's first name.
        $middleName                    = WPD_Common::wpd_set( $resArray, "MIDDLENAME" ); // ' Payer's middle name.
        $lastName                      = WPD_Common::wpd_set( $resArray, "LASTNAME" ); // ' Payer's last name.
        $suffix                        = WPD_Common::wpd_set( $resArray, "SUFFIX" ); // ' Payer's suffix.
        $cntryCode                     = WPD_Common::wpd_set( $resArray, "COUNTRYCODE" ); // ' Payer's country of residence in the form of ISO standard 3166 two-character country codes.
        $business                      = WPD_Common::wpd_set( $resArray, "BUSINESS" ); // ' Payer's business name.
        $shipToName                    = WPD_Common::wpd_set( $resArray, "SHIPTONAME" ); // ' Person's name associated with this address.
        $shipToStreet                  = WPD_Common::wpd_set( $resArray, "SHIPTOSTREET" ); // ' First street address.
        $shipToStreet2                 = WPD_Common::wpd_set( $resArray, "SHIPTOSTREET2" ); // ' Second street address.
        $shipToCity                    = WPD_Common::wpd_set( $resArray, "SHIPTOCITY" ); // ' Name of city.
        $shipToState                   = WPD_Common::wpd_set( $resArray, "SHIPTOSTATE" ); // ' State or province
        $shipToCntryCode               = WPD_Common::wpd_set( $resArray, "SHIPTOCOUNTRYCODE" ); // ' Country code. 
        $shipToZip                     = WPD_Common::wpd_set( $resArray, "SHIPTOZIP" ); // ' U.S. Zip code or other country-specific postal code.
        $addressStatus                 = WPD_Common::wpd_set( $resArray, "ADDRESSSTATUS" ); // ' Status of street address on file with PayPal   
        $invoiceNumber                 = WPD_Common::wpd_set( $resArray, "INVNUM" ); // ' Your own invoice or tracking number, as set by you in the element of the same name in SetExpressCheckout request .
        $phonNumber                    = WPD_Common::wpd_set( $resArray, "PHONENUM" ); // ' Payer's contact telephone number. Note:  PayPal returns a contact telephone number only if your Merchant account profile settings require that the buyer enter one. 
        $_SESSION['TOKEN']             = WPD_Common::wpd_set( $_REQUEST, 'token' );
        $_SESSION['email']             = WPD_Common::wpd_set( $resArray, "EMAIL" );
        $_SESSION['payer_id']          = WPD_Common::wpd_set( $resArray, "PAYERID" );
        $_SESSION['payer_status']      = WPD_Common::wpd_set( $resArray, "PAYERSTATUS" );
        $_SESSION['first_name']        = WPD_Common::wpd_set( $resArray, "FIRSTNAME" );
        $_SESSION['last_name']         = WPD_Common::wpd_set( $resArray, "LASTNAME" );
        $_SESSION['shipToName']        = WPD_Common::wpd_set( $resArray, "SHIPTONAME" );
        $_SESSION['shipToStreet']      = WPD_Common::wpd_set( $resArray, "SHIPTOSTREET" );
        $_SESSION['shipToCity']        = WPD_Common::wpd_set( $resArray, "SHIPTOCITY" );
        $_SESSION['shipToState']       = WPD_Common::wpd_set( $resArray, "SHIPTOSTATE" );
        $_SESSION['shipToZip']         = WPD_Common::wpd_set( $resArray, "SHIPTOZIP" );
        $_SESSION['shipToCountryCode'] = WPD_Common::wpd_set( $resArray, "SHIPTOCOUNTRYCODE" );
        $_SESSION['shipToCountryName'] = WPD_Common::wpd_set( $resArray, "SHIPTOCOUNTRYNAME" );
        $_SESSION['AddressStatus']     = WPD_Common::wpd_set( $resArray, "ADDRESSSTATUS" );
    } else {
        //Display a user friendly Error on the page using any of the following error information returned by PayPal
        $ErrorCode         = urldecode( WPD_Common::wpd_set( $resArray, "L_ERRORCODE0" ) );
        $ErrorShortMsg     = urldecode( WPD_Common::wpd_set( $resArray, "L_SHORTMESSAGE0" ) );
        $ErrorLongMsg      = urldecode( WPD_Common::wpd_set( $resArray, "L_LONGMESSAGE0" ) );
        $ErrorSeverityCode = urldecode( WPD_Common::wpd_set( $resArray, "L_SEVERITYCODE0" ) );

        echo "GetExpressCheckoutDetails API call failed. ";
        echo "Detailed Error Message: " . $ErrorLongMsg;
        echo "Short Error Message: " . $ErrorShortMsg;
        echo "Error Code: " . $ErrorCode;
        echo "Error Severity Code: " . $ErrorSeverityCode;
    }
}
?>
<div class="confirm_popup">
    <h2><?php _e( 'Confirm your informations', WPD_NAME ) ?></h2>
    <table>
        <tbody>
            <tr>
                <td><?php _e( 'Name:', WPD_NAME ) ?>
                <td><?php echo $lastName . " " . $firstName ?>
            </tr>
            <tr>
                <td><?php _e( 'Email:', WPD_NAME ) ?>
                <td><?php echo $email; ?>
            </tr>
            <tr>
                <td><?php echo __( 'You will pay ', WPD_NAME ) . WPD_Common::wpd_set( $_SESSION, "Currency_Symbol" ) . WPD_Common::wpd_set( $_SESSION, "Payment_Amount" ) . __( ' on every', WPD_NAME ) . WPD_Common::wpd_set( $_SESSION, "Billing_Frequency" ) . ' ' . WPD_Common::wpd_set( $_SESSION, "Billing_Period" ); ?>
            </tr>
        <tbody>
    </table>
    <form action='' METHOD='POST'>
        <input id="paypal_confirmation" type="submit" value="<?php _e( 'Confirm Transaction', WPD_NAME ) ?>"/>
    </form>
</div>
<?php
$response_output = ob_get_contents();
ob_end_clean();
return $response_output;
?>