<?php
$default = get_option( 'wp_donation_authorized_settings', TRUE );
$options = WPD_Common::wpd_set( $default, 'wp_donation_authorized_settings' );
if ( WPD_Common::wpd_set( $options, 'status' ) == 'enable' ) {
    echo '<script>
				jQuery(document).ready(function($){
					$("article#authorized_accordion div#show_settings").show();
					$("article#authorized_accordion #contact-form .check #enable_authorized_settings").prop("checked", true);
				});
			  </script>';
}
$tyapes = array( 'sandbox' => __( 'SandBox', WPD_NAME ), 'live' => __( 'live', WPD_NAME ) );
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="authorized_accordion">
            <h3><?php _e( 'Authorized.net Settings', WPD_NAME ) ?></h3>
            <form id="contact-form">
                <div class="overlay">
                    <div class="my_loader"></div>
                </div>
                <div id="authorized_alerts"></div>
                <div class="check">
                    <input type="checkbox" value="enable" id="enable_authorized_settings" name="check"/>
                    <label for="enable_authorized_settings"><span><?php _e( 'Enable Authorized.net', WPD_NAME ) ?></span></label>
                </div>
                <div id="show_settings">
                    <p><?php _e( 'Mode', WPD_NAME ) ?></p>
                    <div class="left_margin">
                        <select id="authorized_type">
                            <?php
                            foreach ( $tyapes as $k => $op ) :
                                $selected = ( WPD_Common::wpd_set( $options, 'type' ) == $k ) ? 'selected="selected"' : '';
                                echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <p><?php _e( 'Authorize.net Login ID', WPD_NAME ) ?></p>
                    <input id="auth_login_id" name="auth_login_id" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'auth_login_id' ) ?>">

                    <p><?php _e( 'Authorize.net Transaction Key', WPD_NAME ) ?></p>
                    <input id="auth_trans_key" name="auth_trans_key" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'auth_trans_key' ) ?>">
                </div>
                <button name="submit" type="submit" id="authorized_submit_setttings"><i class="icon-signin"></i><?php _e( 'Save Changes', WPD_NAME ) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function () {
        df_enable("#enable_authorized_settings")
    });</script>