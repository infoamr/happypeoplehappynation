<?php 
	$default	=	get_option( 'wp_donation_bank_settings', true );
	$options	=	WPD_Common::wpd_set( $default, 'wp_donation_bank_settings' );
	if( WPD_Common::wpd_set( $options, 'status' ) == 'enable' )
	{
		echo '<script>
				jQuery(document).ready(function($){
					$("article#bank_accordion div#show_settings").show();
					$("article#bank_accordion #contact-form .check #enable_bank_settings").prop("checked", true);
				});
			  </script>';
	}
?>
<div class="mainContent">
  <div class="content">
    <article class="topcontent" id="bank_accordion">
	<h3><?php _e( 'Bank Settings', WPD_NAME )?></h3>
      <form id="contact-form" >
      	<div class="overlay"><div class="my_loader"></div></div>
		<div id="bank_alerts"></div>
        <div class="check">
            <input type="checkbox" value="enable" id="enable_bank_settings" name="check" />
            <label for="enable_bank_settings"><span><?php _e( 'Enable Bank Settings', WPD_NAME )?></span></label>
        </div>
        <div id="show_settings">
        	 <p><?php _e( 'Payable To', WPD_NAME ) ?></p>
             <input placeholder="John Smith" id="bank_payable" name="bank_payable" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'name' )?>">
              
            <p><?php _e( 'Bank Name', WPD_NAME ) ?></p>
            <input placeholder="HDFC" id="bank_name" name="bank_name" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'bank_name' )?>">
            
            <p><?php _e( 'BIC Code', WPD_NAME ) ?></p>
            <input placeholder="55855555" id="bank_bic_code" name="bank_bic_code" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'bic' )?>">
            
            <p><?php _e( 'IBAN Number', WPD_NAME ) ?></p>
            <input placeholder="545454544545" id="iban_number" name="iban_number" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'iban' )?>">
            
            <p><?php _e( 'Branch Code', WPD_NAME ) ?></p>
            <input placeholder="I8578" id="branch_code" name="branch_code" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'branch_code' )?>">
            
            <p><?php _e( 'Account Number', WPD_NAME ) ?></p>
            <input placeholder="7852899652555" id="account_number" name="account_number" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'account_num' )?>">
            
            <p><?php _e( 'Street Address', WPD_NAME ) ?></p>
            <input id="street_address" name="street_address" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'address' )?>">
            
            <p><?php _e( 'Postal Address', WPD_NAME ) ?></p>
            <input placeholder="Austin (512) 328-0999" id="postal_address" name="postal_address" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'postal_address' )?>">
            
            <p><?php _e( 'Contact Number', WPD_NAME ) ?></p>
            <input placeholder="01245-988887888" id="contact_no" name="contact_no" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'contact_no' )?>">
            
            <p><?php _e( 'FAX Number', WPD_NAME ) ?></p>
            <input placeholder="0114547854855" id="fax_no" name="fax_no" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'fax_no' )?>">
            
            <p><?php _e( 'Email', WPD_NAME ) ?></p>
            <input placeholder="name@domain.com" id="email" name="email" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'email' )?>">
        </div>
        <button name="submit" type="submit" id="bank_submit_setttings"><i class="icon-signin"></i><?php _e( 'Save Changes', WPD_NAME )?></button>
        <!-- End Submit Button -->
      </form>
    </article>
  </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function(){ df_enable("#enable_bank_settings") });</script>
