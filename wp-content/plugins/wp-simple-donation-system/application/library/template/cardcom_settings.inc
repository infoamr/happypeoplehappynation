<?php
$default = get_option('wp_donation_cardcome_settings', TRUE);
$options = WPD_Common::wpd_set($default, 'wp_donation_cardcom_settings');
if (WPD_Common::wpd_set($options, 'status') == 'enable') {
    echo '<script>
				jQuery(document).ready(function($){
					$("article#cardcom_accordion div#show_settings").show();
					$("article#cardcom_accordion #contact-form .check #enable_cardcom_settings").prop("checked", true);
				});
			  </script>';
}
$tyapes = array('sandbox' => __('SandBox', WPD_NAME), 'live' => __('live', WPD_NAME));
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="cardcom_accordion">
            <h3><?php _e('CardCom Settings', WPD_NAME) ?></h3>
            <form id="contact-form">
                <div class="overlay">
                    <div class="my_loader"></div>
                </div>
                <div id="cardcom_alerts"></div>
                <div class="check">
                    <input type="checkbox" value="enable" id="enable_cardcom_settings" name="check"/>
                    <label for="enable_cardcom_settings"><span><?php _e('Enable Cardcom', WPD_NAME) ?></span></label>
                </div>
                <div id="show_settings">
                    <p><?php _e('Mode', WPD_NAME) ?></p>
                    <div class="left_margin">
                        <select id="cardcom_type">
                            <?php
                            foreach ($tyapes as $k => $op) :
                                $selected = (WPD_Common::wpd_set($options, 'type') == $k) ? 'selected="selected"' : '';
                                echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <p><?php _e('Private Key', WPD_NAME) ?></p>
                    <input id="private_key" name="private_key" type="text" value="<?php echo WPD_Common::wpd_set($options, 'private_key') ?>">

                    <p><?php _e('Public Key', WPD_NAME) ?></p>
                    <input id="public_key" name="public_key" type="text" value="<?php echo WPD_Common::wpd_set($options, 'public_key') ?>">

                    <p><?php _e('Seller Id', WPD_NAME) ?></p>
                    <input id="sellerId" name="sellerId" type="text" value="<?php echo WPD_Common::wpd_set($options, 'sellerId') ?>">
                </div>
                <button name="submit" type="submit" id="cardcom_submit_setttings"><i class="icon-signin"></i><?php _e('Save Changes', WPD_NAME) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function () {
        df_enable("#enable_cardcom_settings")
    });</script>