<?php
$default = get_option( 'wp_donation_credit_caard_settings', true );
$options = WPD_Common::wpd_set( $default, 'wp_donation_credit_caard_settings' );
if ( WPD_Common::wpd_set( $options, 'status' ) == 'enable' ) {
    echo '<script>
				jQuery(document).ready(function($){
					$("article#card_accordion div#show_settings").show();
					$("article#card_accordion #contact-form .check #enable_credit_settings").prop("checked", true);
				});
			  </script>';
}
$tyapes = array( 'sandbox' => __( 'SandBox', WPD_NAME ), 'live' => __( 'live', WPD_NAME ) );
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="card_accordion">
            <h3><?php _e( 'Credit Card Settings', WPD_NAME ) ?></h3>
            <form id="contact-form" >
                <div class="overlay"><div class="my_loader"></div></div>
                <div id="credit_alerts"></div>
                <div class="check">
                    <input type="checkbox" value="enable" id="enable_credit_settings" name="check" />
                    <label for="enable_credit_settings"><span><?php _e( 'Enable Credit Card', WPD_NAME ) ?></span></label>
                </div>
                <div id="show_settings">
                    <p><?php _e( 'Credit Card Type', WPD_NAME ) ?></p>
                    <div class="left_margin">
                        <select id="credit_card_type">
                            <?php
                            foreach ( $tyapes as $k => $op ) :
                                $selected = ( WPD_Common::wpd_set( $options, 'type' ) == $k ) ? 'selected="selected"' : '';
                                echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <p><?php _e( 'Stripe Api Secret Key', WPD_NAME ) ?></p>
                    <input id="stripe_api_key" name="stripe_api_key" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'key' ) ?>">

                    <p><?php _e( 'Stripe API Publish Key', WPD_NAME ) ?></p>
                    <input id="stripe_publish_key" name="stripe_publish_key" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'publish_key' ) ?>">

                    <p><?php _e( 'Select Payment Cycle', WPD_NAME ) ?></p>
                    <?php $plans = array( 'day' => __( 'Daily', WPD_NAME ), 'week' => __( 'Weekly', WPD_NAME ), 'month' => __( 'Monthly', WPD_NAME ), 'year' => __( 'Yearly', WPD_NAME ) ) ?>
                    <div class="left_margin">
                        <select id="credit_card_plans" multiple="multiple">
                            <?php foreach ( $plans as $k => $p ): //print_r(WPD_Common::wpd_set( $options, 'cycle' ));exit; ?>
                                <option <?php if ( WPD_Common::wpd_set( $options, 'cycle' ) && in_array( $k, WPD_Common::wpd_set( $options, 'cycle' ) ) ): echo 'selected'; endif; ?> value="<?php echo $k ?>">
                                    <?php echo $p; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <p><?php _e( 'Select Payment Number of Cycle', WPD_NAME ) ?></p>
                    <div class="left_margin">
                        <select id="credit_card_payment_cycle" multiple="multiple">
                            <?php
                            $cycle = range( 1, 12 );
                            foreach ( $cycle as $m ):
                                if ( WPD_Common::wpd_set( $options, 'num_of_cycle' ) && in_array( $m, WPD_Common::wpd_set( $options, 'num_of_cycle' ) ) ): $select = 'selected'; else: $select = ''; endif;
                                echo '<option ' . $select . ' value="' . $m . '">' . $m . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <button name="submit" type="submit" id="credit_submit_setttings"><i class="icon-signin"></i><?php _e( 'Save Changes', WPD_NAME ) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function () {
        df_enable("#enable_credit_settings")
    });</script>