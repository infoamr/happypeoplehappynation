<?php
$default = get_option( 'wp_donation_basic_settings', true );
$options = WPD_Common::wpd_set( $default, 'wp_donation_basic_settings' );
//print_r($options); exit;
$symbols = WPD_Currencies::get_instance()->getSymbol();
$builder = WPD_Common::wpd_set( $options, 'builder_vals' );
if ( WPD_Common::wpd_set( $options, 'builder_status' ) == 1 ) {
    echo '<script>
				jQuery(window).ready(function($){
					$("div.price_builder").show();
					$("#enable_builder_settings").prop("checked", true);
				});
			  </script>';
} else {
    echo '<script>
				jQuery(window).ready(function($){
					$("div.price_builder").hide();
					$("#enable_builder_settings").prop("checked", false);
				});
			  </script>';
}

if ( WPD_Common::wpd_set( $options, 'lang_bar' ) == 1 ) {
    echo '<script>
				jQuery(window).ready(function($){
					$("div.language_uploader").show();
					$("#enable_language_selector").prop("checked", true);
				});
			  </script>';
} else {
    echo '<script>
				jQuery(window).ready(function($){
					$("div.language_uploader").hide();
					$("#enable_language_selector").prop("checked", false);
				});
			  </script>';
}
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="basic_accordion">
            <h3><?php _e( 'Basic Settings', WPD_NAME ) ?></h3>
            <form id="contact-form" >
                <div class="overlay"><div class="my_loader"></div></div>
                <div id="basic_alerts"></div>
                <p><?php _e( 'Show Transactions History:', WPD_NAME ) ?></p>
                <div class="left_margin">
                    <select id="transaction_history">
<?php
$opt = array( 'true' => __( 'True', WPD_NAME ), 'false' => __( 'False', WPD_NAME ) );
foreach ( $opt as $k => $op ) :
    $selected = ( WPD_Common::wpd_set( $options, 'history' ) == $op ) ? 'selected="selected"' : '';
    echo '<option value="' . $op . '" ' . $selected . '>' . $op . '</option>';
endforeach;
?>
                    </select>
                </div>

                <p><?php _e( 'Number of show transactions:', WPD_NAME ) ?></p>
                <input placeholder="<?php _e( 'Enter Number of show transactions', WPD_NAME ) ?>" id="trans_num" name="trans_num" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'trans_num' ) ?>">

                <p><?php _e( 'Enter Title:', WPD_NAME ) ?></p>
                <input placeholder="<?php _e( 'Wp Easy Donation System', WPD_NAME ) ?>" id="wpd_title" name="wpd_title" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'wpd_title' ) ?>">

                <p><?php _e( 'Enter Success Message:', WPD_NAME ) ?></p>
                <input placeholder="<?php _e( 'Success Message', WPD_NAME ) ?>" id="smsg" name="smsg" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'smsg' ) ?>">
                <p><?php _e( 'Enter Success Subtitle:', WPD_NAME ) ?></p>
                <input placeholder="<?php _e( 'Success Subtitle', WPD_NAME ) ?>" id="dmsg" name="dmsg" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'dmsg' ) ?>">
                <p><?php _e( 'Choose Message Background Color:', WPD_NAME ) ?></p>
                <input id="clrmsg" name="clrmsg" type="color" value="<?php echo WPD_Common::wpd_set( $options, 'clrmsg' ) ?>">
                <p><?php _e( 'Choose Message Text Color:', WPD_NAME ) ?></p>
                <input id="clrtext" name="clrtext" type="color" value="<?php echo WPD_Common::wpd_set( $options, 'clrtext' ) ?>">
                <p><?php _e( 'Enter Success Message Description:', WPD_NAME ) ?></p>
                <input placeholder="<?php _e( 'Success Description', WPD_NAME ) ?>" id="msgdes" name="msgdes" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'msgdes' ) ?>">
                
                <?php /* ?> <p><?php _e( 'Show Language Bar:', WPD_NAME ) ?></p>
                  <div class="left_margin">
                  <select id="language_bar_status">
                  <?php
                  $opt		=	array( 'true' => __( 'True', WPD_NAME ), 'false' => __( 'False', WPD_NAME ) );
                  foreach ( $opt as $k => $op ) :
                  $selected	=	( WPD_Common::wpd_set( $options, 'language_bar_status' ) == $op ) ? 'selected="selected"' : '';
                  echo '<option value="'.$op.'" '.$selected.'>'.$op.'</option>';
                  endforeach; ?>
                  </select>
                  </div><?php */ ?>
                <?php /* ?> <p><?php _e( 'Color Scheme:', WPD_NAME ) ?></p>
                  <input type="text" name="color" id="color" class="color-picker" /><?php */ ?>

                <p><?php _e( 'Show Currency Selector:', WPD_NAME ) ?></p>
                <div class="left_margin">
                    <select id="currency_selector">
                        <?php
                        $opt = array( 'true' => __( 'True', WPD_NAME ), 'false' => __( 'False', WPD_NAME ) );
                        foreach ( $opt as $k => $op ) :
                            $selected = ( WPD_Common::wpd_set( $options, 'currency_selector' ) == $op ) ? 'selected="selected"' : '';
                            echo '<option value="' . $op . '" ' . $selected . '>' . $op . '</option>';
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_language_selector" name="check" />
                    <label for="enable_language_selector"><span><?php _e( 'Enable Language Selector:', WPD_NAME ) ?></span></label>
                </div>
                <div class="language_uploader">
                    <?php
                    $opt = WPD_Common::wpd_get_languages();
                    if ( $opt ):
                        ?>
                        <div class="left_margin">
                            <select id="language_selector">
                                <option></option>
                                <?php
                                foreach ( $opt as $k => $op ) :
                                    $selected = ( WPD_Common::wpd_set( $options, 'lang_' ) == $op ) ? 'selected="selected"' : '';
                                    echo '<option value="' . $op . '" ' . $selected . '>' . $op . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
<?php endif; ?>
                    <br  />
                    <div id="lang_error"></div>
                    <input name="lang_file" id="lang_file" type="file" />
                    <button id="lang_upload_btn"><?php _e( 'Upload', WPD_NAME ); ?></button>

                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_builder_settings" name="check" />
                    <label for="enable_builder_settings"><span><?php _e( 'Enable Custom Donation Values', WPD_NAME ) ?></span></label>
                </div>

                <div class="price_builder">
                    <p><?php _e( 'Donation Values', WPD_NAME ) ?></p>
                    <?php
                    if ( $builder && !empty( $builder ) ):
                        $builder_val = explode( ',', $builder );
                        $counter     = 0;
                        foreach ( $builder_val as $value ):
                            ?>
                            <div id="paypal_default_value<?php echo $counter ?>" class="clone">
                                <div class="heading" >
                                        <?php _e( 'Donation Values', WPD_NAME ) ?> 
                                    <div id="remvoe_clone" href="javascript:void(0)" > 
        <?php _e( 'Remove', WPD_NAME ) ?> 
                                    </div>
                                </div>
                                <input placeholder="<?php _e( 'Enter Value', WPD_NAME ) ?>" id="paypal_default_value<?php echo $counter ?>" name="paypal_default_value<?php echo $counter ?>" type="text" value="<?php echo $value; ?>">
                            </div>
                            <script>
        			jQuery(window).load(function () {
        			    var header = jQuery('div#paypal_default_value<?php echo $counter ?> > div');
        			    jQuery(header).trigger('click');
        			})
                    // Block non-numeric chars.
                    jQuery(document).on('keypress', 'input#paypal_default_value<?php echo $counter ?>', function (event) {
                        return ((event.which < 58) || (event.which == 13));
                    });

                            </script>
        <?php $counter++; endforeach; else: ?>
                        <div id="paypal_default_value0" class="clone">
                            <div class="heading" >
                                    <?php _e( 'Donation Values', WPD_NAME ) ?> 
                                <div id="remvoe_clone" href="javascript:void(0)" > 
    <?php _e( 'Remove', WPD_NAME ) ?> 
                                </div>
                            </div>
                            <input placeholder="<?php _e( 'Enter Value', WPD_NAME ) ?>" id="paypal_default_value0" name="paypal_default_value" type="text">
                        </div>

<?php endif; ?>

                    <button name="clone" type="submit" id="clone_price"><i class="icon-signin"></i><?php _e( 'Add More Donation Value', WPD_NAME ) ?></button>
<script>
    // Block non-numeric chars.
    jQuery(document).on('keypress', 'input#paypal_default_value0', function (event) {
        return ((event.which < 58) || (event.which == 13));
    });
</script>
                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_custom_amount_box" name="check" <?php if ( WPD_Common::wpd_set( $options, 'custom_amount' ) == 1 ): echo 'checked'; endif; ?> />
                    <label for="enable_custom_amount_box"><span><?php _e( 'Enable Custom Amount Box', WPD_NAME ) ?></span></label>
                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_custom_logo" name="check" <?php if ( WPD_Common::wpd_set( $options, 'custom_logo' ) == 1 ): echo 'checked'; endif; ?>/>
                    <label for="enable_custom_logo"><span><?php _e( 'Enable Invoice Logo', WPD_NAME ) ?></span></label>
                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_single_payment" name="check" <?php if ( WPD_Common::wpd_set( $options, 'single' ) == 1 ): echo 'checked'; endif; ?>/>
                    <label for="enable_single_payment"><span><?php _e( 'Enable Single Payment', WPD_NAME ) ?></span></label>
                </div>

                <div class="check">
                    <input type="checkbox" value="enable" id="enable_reccuring_payment" name="check" <?php if ( WPD_Common::wpd_set( $options, 'recuring' ) == 1 ): echo 'checked'; endif; ?> />
                    <label for="enable_reccuring_payment"><span><?php _e( 'Enable Reccuring Payment', WPD_NAME ) ?></span></label>
                </div>



                <?php /* ?><p><?php _e( 'Paypal API Username', WPD_NAME ) ?></p>
                  <input placeholder="name@domain.com" id="paypal_api_user" name="paypal_api_user" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'api_user' )?>">

                  <p><?php _e( 'Paypal API Password', WPD_NAME ) ?></p>
                  <input placeholder="*******************" id="paypal_api_pass" name="paypal_api_pass" type="password" value="<?php echo WPD_Common::wpd_set( $options, 'api_pass' )?>">

                  <p><?php _e( 'Paypal API Signature', WPD_NAME ) ?></p>
                  <input id="paypal_api_signature" name="paypal_api_signature" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'api_sign' )?>"><?php */ ?>

                <button name="submit" type="submit" id="basic_submit_setttings"><i class="icon-signin"></i><?php _e( 'Save Changes', WPD_NAME ) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>

