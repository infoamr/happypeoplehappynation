<?php
$default = get_option('wp_donation_paymaster_settings', TRUE);
$options = WPD_Common::wpd_set($default, 'wp_donation_paymaster_settings');
if (WPD_Common::wpd_set($options, 'status') == 'enable') {
    echo '<script>
				jQuery(document).ready(function($){
					$("article#paymaster_accordion div#show_settings").show();
					$("article#paymaster_accordion #contact-form .check #enable_paymaster_settings").prop("checked", true);
				});
			  </script>';
}
$tyapes = array('test' => __('Test', WPD_NAME), 'production' => __('Production', WPD_NAME));
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="paymaster_accordion">
            <h3><?php _e('PayMaster Settings', WPD_NAME) ?></h3>
            <form id="contact-form">
                <div class="overlay">
                    <div class="my_loader"></div>
                </div>
                <div id="paymaster_alerts"></div>
                <div class="check">
                    <input type="checkbox" value="enable" id="enable_paymaster_settings" name="check"/>
                    <label for="enable_paymaster_settings"><span><?php _e('Enable PayMaster', WPD_NAME) ?></span></label>
                </div>
                <div id="show_settings">
                    <p><?php _e('Mode', WPD_NAME) ?></p>
                    <div class="left_margin">
                        <select id="paymaster_type">
                            <?php
                            foreach ($tyapes as $k => $op) :
                                $selected = (WPD_Common::wpd_set($options, 'type') == $k) ? 'selected="selected"' : '';
                                echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <p><?php _e('Account ID', WPD_NAME) ?></p>
                    <input id="LMI_MERCHANT_ID" name="LMI_MERCHANT_ID" type="text" value="<?php echo WPD_Common::wpd_set($options, 'paymaster_merchant') ?>">



                </div>
                <button name="submit" type="submit" id="paymaster_submit_setttings"><i class="icon-signin"></i><?php _e('Save Changes', WPD_NAME) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function () {
        df_enable("#enable_paymaster_settings")
    });</script>