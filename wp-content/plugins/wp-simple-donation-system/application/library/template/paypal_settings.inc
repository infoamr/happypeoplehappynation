<?php 
	$default	=	get_option( 'wp_donation_papypal_settings', true );
	$options	=	WPD_Common::wpd_set( $default, 'wp_donation_papypal_settings' );
	if( WPD_Common::wpd_set( $options, 'status' ) == 'enable' )
	{
		echo '<script>
				jQuery(document).ready(function($){
					$("article#paypal_accordion div#show_settings").show();
					$("article#paypal_accordion #contact-form .check #enable_payapl_settings").prop("checked", true);
				});
			  </script>';
	}
	$tyapes		=	array( 'sandbox'	=>	__( 'SandBox', WPD_NAME ), 'live'	=>	__( 'live', WPD_NAME ) );
?>
<div class="mainContent">
  <div class="content">
    <article class="topcontent" id="paypal_accordion">
	<h3><?php _e( 'PayPal Settings', WPD_NAME )?></h3>
      <form id="contact-form" >
      	<div class="overlay"><div class="my_loader"></div></div>
		<div id="paypal_alerts"></div>
        <div class="check">
            <input type="checkbox" value="enable" id="enable_payapl_settings" name="check" />
            <label for="enable_payapl_settings"><span><?php _e( 'Enable PayPal', WPD_NAME )?></span></label>
        </div>
        <div id="show_settings">
        	 <p><?php _e( 'Paypal Type', WPD_NAME ) ?></p>
             <div class="left_margin">
                <select id="paypal_type">
                    <?php foreach ( $tyapes as $k => $op ) : 	
                        $selected = ( WPD_Common::wpd_set( $options, 'type' ) == $k ) ? 'selected="selected"' : '';
                        echo '<option value="'.$k.'" '.$selected.'>'.$op.'</option>';
                      endforeach; ?>
                </select>
			</div>
              
            <p><?php _e( 'Paypal Username', WPD_NAME ) ?></p>
            <input placeholder="name@domain.com" id="paypal_user" name="paypal_user" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'user' )?>">
            
            <p><?php _e( 'Paypal API Username', WPD_NAME ) ?></p>
            <input placeholder="name@domain.com" id="paypal_api_user" name="paypal_api_user" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'api_user' )?>">
            
            <p><?php _e( 'Paypal API Password', WPD_NAME ) ?></p>
            <input placeholder="*******************" id="paypal_api_pass" name="paypal_api_pass" type="password" value="<?php echo WPD_Common::wpd_set( $options, 'api_pass' )?>">
            
            <p><?php _e( 'Paypal API Signature', WPD_NAME ) ?></p>
            <input id="paypal_api_signature" name="paypal_api_signature" type="text" value="<?php echo WPD_Common::wpd_set( $options, 'api_sign' )?>">
            
            <p><?php _e( 'Select Payment Cycle', WPD_NAME ) ?></p>
            <?php $plans = array( 'daily' => __( 'Daily', WPD_NAME ), 'weekly' => __( 'Weekly', WPD_NAME ), 'fortnightly' => __( 'Fortnightly', WPD_NAME ), 'monthly' => __( 'Monthly', WPD_NAME ), 'quaterly' => __( 'Quarterly', WPD_NAME ), 'half yearly' => __( 'Half Yearly', WPD_NAME ), 'yearly' => __( 'Yearly', WPD_NAME )) ?>
            <div class="left_margin">
            	<select id="paypal_plans" multiple="multiple">
            	<?php foreach( $plans as $k => $p ): ?>
            		<option <?php if( WPD_Common::wpd_set( $options, 'paypal_plans' ) && in_array( $k, WPD_Common::wpd_set( $options, 'paypal_plans' ) ) ): echo 'selected'; endif; ?> value="<?php echo $k ?>">
						<?php echo $p; ?>
					</option>
                <?php endforeach; ?>
            </select>
            </div>
            
        </div>
        <button name="submit" type="submit" id="paypal_submit_setttings"><i class="icon-signin"></i><?php _e( 'Save Changes', WPD_NAME )?></button>
        <!-- End Submit Button -->
      </form>
    </article>
  </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function(){ df_enable("#enable_payapl_settings") });</script>
