<?php
$default = get_option('wp_donation_yandex_settings', TRUE);
$options = WPD_Common::wpd_set($default, 'wp_donation_yandex_settings');
if (WPD_Common::wpd_set($options, 'status') == 'enable') {
    echo '<script>
				jQuery(document).ready(function($){
					$("article#yandex_accordion div#show_settings").show();
					$("article#yandex_accordion #contact-form .check #enable_yandex_settings").prop("checked", true);
				});
			  </script>';
}
$tyapes = array('test' => __('Test', WPD_NAME), 'production' => __('Production', WPD_NAME));
?>
<div class="mainContent">
    <div class="content">
        <article class="topcontent" id="yandex_accordion">
            <h3><?php _e('Yandex Settings', WPD_NAME) ?></h3>
            <form id="contact-form">
                <div class="overlay">
                    <div class="my_loader"></div>
                </div>
                <div id="yandex_alerts"></div>
                <div class="check">
                    <input type="checkbox" value="enable" id="enable_yandex_settings" name="check"/>
                    <label for="enable_yandex_settings"><span><?php _e('Enable Yandex', WPD_NAME) ?></span></label>
                </div>
                <div id="show_settings">
                    <p><?php _e('Mode', WPD_NAME) ?></p>
                    <div class="left_margin">
                        <select id="yandex_type">
                            <?php
                            foreach ($tyapes as $k => $op) :
                                $selected = (WPD_Common::wpd_set($options, 'type') == $k) ? 'selected="selected"' : '';
                                echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <p><?php _e('shop Id', WPD_NAME) ?></p>
                    <input id="shopId" name="shopId" type="text" value="<?php echo WPD_Common::wpd_set($options, 'shopId') ?>">

                    <p><?php _e('Secret Id', WPD_NAME) ?></p>
                    <input id="scid" name="scid" type="text" value="<?php echo WPD_Common::wpd_set($options, 'scid') ?>">
                </div>
                <button name="submit" type="submit" id="yandex_submit_setttings"><i class="icon-signin"></i><?php _e('Save Changes', WPD_NAME) ?></button>
                <!-- End Submit Button -->
            </form>
        </article>
    </div>
</div>
<div class="clearfix"></div>
<script>jQuery(document).ready(function () {
        df_enable("#enable_yandex_settings")
    });</script>