<?php
WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_modernizr', 'WPD_bootstrap', 'WPD_select', 'WPD_plugin', 'WPD_date', 'WPD_stripe', 'WPD_script','WPD_bootstrap'));

$default = get_option('wp_donation_basic_settings', true);
$basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
$trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;
$hide = '';
global $wpdb;
global $charset_collate;
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
$table_name = $wpdb->prefix . "wpd_easy_donation";
$wpdb->show_errors();

//$all_query = "SELECT COUNT(id) FROM $table_name";
//$all_result = $wpdb->get_results($all_query);
//$count_all = WPD_Common::wpd_set(WPD_Common::wpd_set($all_result, '0'), 'COUNT(id)');


//$success_q = "SELECT COUNT(id) FROM $table_name where source IN ('Credit Card','PayPal')";
//$success_r = $wpdb->get_results($success_q);
//$success_all = WPD_Common::wpd_set(WPD_Common::wpd_set($success_r, '0'), 'COUNT(id)');

//$fail_q = "SELECT COUNT(id) FROM $table_name where source IN( 'Bank' )";
//$fail_r = $wpdb->get_results($fail_q);
//$fail_all = WPD_Common::wpd_set(WPD_Common::wpd_set($fail_r, '0'), 'COUNT(id)');

//$res_q = "SELECT * FROM $table_name ORDER BY id DESC LIMIT 0, $trans_num";
//$res_r = $wpdb->get_results($res_q);

//update_option('donation_recent_query', $res_q);
?>
<div id="f_overlay"></div>
<div class="overlay">
    <div class="my_loader"></div>
</div>
<div id="reporting_popup"></div>

<div class="wp-donation-system">
    <?php if (!is_admin()): ?><h6><a href="javascript:void(0)" onclick="history.go(-1);
                return false;"><?php _e('BACK', WPD_NAME) ?></a></h6> <?php endif; ?>
    <div class="easy-donation">
        <div class="donation-title">
            <h1><?php _e('Add Custom Transaction', WPD_NAME) ?></h1>            
        </div>
        

        <div class="donation-fields">
            <div class="row">
                <div class="col-md-12">
                    <div class='donation_for'>
                    <label for="usr"><?php _e('Donation For:', WPD_NAME) ?></label>
                    <label class="radio-inline"><input type="radio" id="general" name="radAnswer"><?php _e('General', WPD_NAME) ?></label>
                    <label class="radio-inline"><input type="radio" id="lif_causes" name="radAnswer"><?php _e('Causes', WPD_NAME) ?></label>
                    <label class="radio-inline"><input type="radio" id="lif_project" name="radAnswer"><?php _e('Projects', WPD_NAME) ?></label> 
                    </div>
                    <div class="product_list"></div>
                    <div class="message_box"></div>
                    <div class="form-group">
                        <label for="usr"><?php _e('Donation Amount:', WPD_NAME) ?></label>
                        <input type="text" class="form-control" id="amount">
                    </div>
                    <h4><?php _e('Donor Information', WPD_NAME) ?></h4>            
                    <div class="form-group">
                        <label for="usr"><?php _e('First Name:', WPD_NAME) ?></label>
                        <input type="text" class="form-control" id="f_name">
                    </div>
                    <div class="form-group">
                        <label for="usr"><?php _e('Last Name:', WPD_NAME) ?></label>
                        <input type="text" class="form-control" id="l_name">
                    </div>
                    <div class="form-group">
                        <label for="usr"><?php _e('Email Id:', WPD_NAME) ?></label>
                        <input type="text" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="usr"><?php _e('Phone Number:', WPD_NAME) ?></label>
                        <input type="text" class="form-control" id="phone">
                    </div>
                    <div class="form-group">
                        <label for="usr"><?php _e('Address:', WPD_NAME) ?></label>
                        <textarea class="form-control" rows="5" id="address"></textarea>
                    </div>

                </div>

                <div class="col-md-12">
                    <button type="submit" class="add_payment"><?php _e('Sumbit', WPD_NAME) ?></button>
                </div>
            </div>
        </div>
    </div>
</div>