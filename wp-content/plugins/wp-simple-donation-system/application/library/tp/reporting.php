<?php
WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_modernizr', 'WPD_bootstrap', 'WPD_select', 'WPD_plugin', 'WPD_date', 'WPD_stripe', 'WPD_script'));

$default = get_option('wp_donation_basic_settings', true);
$basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
$trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;
$hide = '';
global $wpdb;
global $charset_collate;
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
$table_name = $wpdb->prefix . "wpd_easy_donation";
$wpdb->show_errors();

$all_query = "SELECT COUNT(id) FROM $table_name";
$all_result = $wpdb->get_results($all_query);
$count_all = WPD_Common::wpd_set(WPD_Common::wpd_set($all_result, '0'), 'COUNT(id)');


$success_q = "SELECT COUNT(id) FROM $table_name where source IN ('Credit Card','PayPal','custom')";
$success_r = $wpdb->get_results($success_q);
$success_all = WPD_Common::wpd_set(WPD_Common::wpd_set($success_r, '0'), 'COUNT(id)');

$fail_q = "SELECT COUNT(id) FROM $table_name where source IN( 'Bank' )";
$fail_r = $wpdb->get_results($fail_q);
$fail_all = WPD_Common::wpd_set(WPD_Common::wpd_set($fail_r, '0'), 'COUNT(id)');

$res_q = "SELECT * FROM $table_name ORDER BY id DESC LIMIT 0, $trans_num";
$res_r = $wpdb->get_results($res_q);

update_option('donation_recent_query', $res_q);
?>
<div id="f_overlay"></div>
<div class="overlay">
    <div class="my_loader"></div>
</div>
<div id="reporting_popup"></div>

<div class="wp-donation-system">
    <?php if (!is_admin()): ?><h6><a href="javascript:void(0)" onclick="history.go(-1); return false;"><?php _e('BACK', WPD_NAME) ?></a></h6> <?php endif; ?>
    <div class="easy-donation">
        <div class="donation-title">
            <h1><?php _e('Transaction Details', WPD_NAME) ?></h1>
            <div class="date-range">
                <div class="field"><input id="date_from" type="text" placeholder="From"/><i class="ti-calendar"></i></div>
                <div class="field"><input id="date_tor" type="text" placeholder="To"/><i class="ti-calendar"></i></div>
            </div>
            <div class="search">
                <input type="text" id="s_" placeholder="<?php _e('Search Your Keyword', WPD_NAME) ?>"/>
                <button id="_s_"><i class="ti-search"></i></button>
            </div>
        </div>

        <div id="msg_"></div>

        <div class="donation-fields">
            <div class="row">
                <div class="col-md-2"><strong class="wpdonation-subtitle"><?php _e('Total Transactions : ', WPD_NAME) ?><?php echo $count_all; ?></strong></div>
                <div class="col-md-4">
                    <select id="type_all">
                        <option value="all"><?php _e('All', WPD_NAME) ?></option>
                        <option value="reccuring"><?php _e('Reccuring', WPD_NAME) ?></option>
                        <option value="paypal"><?php _e('PayPal', WPD_NAME) ?></option>
                        <option value="credit_card"><?php _e('Credit Card', WPD_NAME) ?></option>
                        <option value="bank_transfer"><?php _e('Bank Transfer', WPD_NAME) ?></option>
                        <option value="authorized"><?php _e('Authorized.net', WPD_NAME) ?></option>
                        <option value="2Checkout"><?php _e('2Checkout', WPD_NAME) ?></option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="type_numbers">
                        <option value="all"><?php _e('All ', WPD_NAME) ?>( <?php echo $count_all ?> )</option>
                        <option value="success"><?php _e('Successfull ', WPD_NAME) ?>( <?php echo $success_all ?> )</option>
                        <option value="faild"><?php _e('Faild ', WPD_NAME) ?>( <?php echo $fail_all ?> )</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button id="clear_filters" class="clear-filter" type="submit"><?php _e('Clear Filters', WPD_NAME) ?></button>
                </div>

                <div class="col-md-12">
                    <div class="message_box"></div>
                    <div id="history_wrap" class="history-wrap">
                        <?php if (!empty($res_r)): ?>
                            <?php foreach ($res_r as $res): ?>
                                <?php
                                $date = strtotime(WPD_Common::wpd_set($res, 'date'));
                                $new_date = date('d M Y', $date);
                                ?>
                                <div class="history">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="wpdonation-payment">
                                                <i><?php echo WPD_Common::wpd_set($res, 'currency') ?></i>
                                                <?php echo WPD_Common::wpd_set($res, 'amount') ?>.00
                                                <a href="javascript:void(0)" data-id="<?php echo WPD_Common::wpd_set($res, 'id') ?>" id="view_invoice" title=""><?php _e('View Invoice', WPD_NAME) ?></a>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="history-detail">
                                                <span><?php echo $new_date ?></span>
                                                <h5><strong><?php _e('Name:', WPD_NAME) ?></strong> <?php echo WPD_Common::wpd_set($res, 'f_name') ?> <?php echo WPD_Common::wpd_set($res, 'l_name') ?></h5>
                                                <i><?php echo WPD_Common::wpd_set($res, 'source') ?></i>
                                                <i class="delet_record" id="<?php echo WPD_Common::wpd_set($res, 'id') ?>"><?php echo _('Delete') ?></i>
                                                <ul>
                                                    <li><strong><?php _e('Email Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'email') ?></li>
                                                    <li><strong><?php _e('Order Id:', WPD_NAME) ?> </strong><?php echo WPD_Common::wpd_set($res, 'id') ?></li>
                                                </ul>
                                            </div><!-- History Detail -->
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php $hide = 'style="display:none;"'; ?>
                            <div class="no-record"><p><?php _e('No Record Found', WPD_NAME) ?></p></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-12" <?php echo $hide; ?>>
                    <div class="trans-history">
                        <a href="javascript:void(0)" id="transaction_more" title=""><?php _e('Show More', WPD_NAME) ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>