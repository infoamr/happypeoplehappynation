<?php
WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_modernizr', 'WPD_bootstrap', 'WPD_select', 'WPD_plugin', 'WPD_date', 'WPD_stripe', 'WPD_twochecout', 'WPD_script'));
$default = get_option('wp_donation_basic_settings', TRUE);
$options = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');

$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
$sidebar    = lifeline2_set( $page_meta, 'metaSidebar' );
$position   = lifeline2_set( $page_meta, 'layout' );
$span       = ( $sidebar ) ? 'col-md-9' : 'col-md-12';

$builder = WPD_Common::wpd_set($options, 'builder_vals');
$title = (WPD_Common::wpd_set($options, 'title')) ? WPD_Common::wpd_set($options, 'title') : $title;
$symbols = WPD_Currencies::get_instance()->getSymbol();

$credit_cycle = get_option('wp_donation_credit_caard_settings', TRUE);
$cycle_period = WPD_Common::wpd_set($credit_cycle, 'wp_donation_credit_caard_settings');
$cycles = WPD_Common::wpd_set($cycle_period, 'cycle');
$cycles_times = WPD_Common::wpd_set($cycle_period, 'num_of_cycle');

$paypal_cycle = get_option('wp_donation_papypal_settings', TRUE);
$paypal_period = WPD_Common::wpd_set($paypal_cycle, 'wp_donation_papypal_settings');
$plans = WPD_Common::wpd_set($paypal_period, 'paypal_plans');
//print_r($paypal_period); exit;

$bank_settings = get_option('wp_donation_bank_settings', TRUE);
$bank__ = WPD_Common::wpd_set($bank_settings, 'wp_donation_bank_settings');

$authorized = get_option('wp_donation_authorized_settings', TRUE);
$authorizedSettings = WPD_Common::wpd_set($authorized, 'wp_donation_authorized_settings');

$twoChecout = get_option('wp_donation_twocheckout_settings', TRUE);
$twochecoutSettings = WPD_Common::wpd_set($twoChecout, 'wp_donation_twocheckout_settings');

$paymaster = get_option('wp_donation_paymaster_settings', TRUE);
$paymasterSettings = WPD_Common::wpd_set($paymaster, 'wp_donation_paymaster_settings');

$yandex = get_option('wp_donation_yandex_settings', TRUE);
$yandexSettings = WPD_Common::wpd_set($yandex, 'wp_donation_yandex_settings');

$braintree = get_option('wp_donation_braintree_settings', TRUE);
$braintreeSettings = WPD_Common::wpd_set($braintree, 'wp_donation_braintree_settings');

$bluepay = get_option('wp_donation_bluepay_settings', TRUE);
$bluepaySettings = WPD_Common::wpd_set($bluepay, 'wp_donation_bluepay_settings');

$conekta = get_option('wp_donation_conekta_settings', TRUE);
$conektaSettings = WPD_Common::wpd_set($conekta, 'wp_donation_conekta_settings');

$paystack = get_option('wp_donation_paystack_settings', TRUE);
$paystackSettings = WPD_Common::wpd_set($paystack, 'wp_donation_paystack_settings');

$payumoney = get_option('wp_donation_payumoney_settings', TRUE);
$payumoneySettings = WPD_Common::wpd_set($payumoney, 'wp_donation_payumoney_settings');

$quickpay = get_option('wp_donation_quickpay_settings', TRUE);
$quickpaySettings = WPD_Common::wpd_set($quickpay, 'wp_donation_quickpay_settings');

$cardcom = get_option('wp_donation_cardcom_settings', TRUE);
$cardcomSettings = WPD_Common::wpd_set($cardcom, 'wp_donation_cardcom_settings');

if (WPD_Common::wpd_set($_GET, 'action') && WPD_Common::wpd_set($_GET, 'action') == 'wpd_success') {
    //printr($_POST);
    if (isset($_POST['txn_id'])) {
        global $wpdb;
        global $charset_collate;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $tax_id = $_POST['txn_id'];

        $check_ = "select transaction_id from $table_name where transaction_id = '$tax_id'";
        $res = $wpdb->get_results($check_);
        if (empty($res) and is_array($res)) {
            session_start();

            $amount = $_SESSION['amount'];
            $f_name = $_SESSION['f_name'];
            $l_name = $_SESSION['l_name'];
            $email_ = $_SESSION['email_'];
            $contact_no = $_SESSION['contact_no'];
            $address = $_SESSION['address'];
            $date_ = $_SESSION['date_'];
            $source = $_SESSION['source'];
            $type = $_SESSION['type'];
            $symbol = $_SESSION['symbol'];
            $donatioType = $_SESSION['donationType'];
            $postId = $_SESSION['donationPost'];

            echo "<div class='confirm_popup'>
                            <h2>" . __('Thanks', WPD_NAME) . "</h2>
                            <table><tbody><tr><td>" . __('I would like to thank you for your contribution of ' . $_SESSION['symbol'] . $_SESSION['amount'] . '. Your financial support helps us continue in our mission and the assist those in our community', WPD_NAME) . "</td></tr></tbody></table>";
            echo '<form><input id="single_payment_close" type="button" value="' . __('Close', WPD_NAME) . '" class="button"></form></div>';

            $query = "INSERT INTO $table_name ( amount, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id ) VALUES ( '" . $amount . "', '" . $f_name . "', '" . $l_name . "', '" . $email_ . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date_ . "', '" . $tax_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";

            $result = $wpdb->query($query);
            echo "<script>jQuery(document).ready(function(){ wpd_confirm_popup(); });</script>";
        } else {
            echo "<div class='confirm_popup'>
                            <h2>" . __('Error', WPD_NAME) . "</h2>
                            <table><tbody><tr><td>" . __('You have been already make this transaction.', WPD_NAME) . "</td></tr></tbody></table>";
            echo '<form><input id="single_payment_close" type="button" value="' . __('Close', WPD_NAME) . '" class="button"></form></div>';
            echo "<script>jQuery(document).ready(function(){ wpd_confirm_popup(); });</script>";
        }
    } else {
        echo "<div class='confirm_popup'>
				<h2>" . __('Error', WPD_NAME) . "</h2>
				<table><tbody><tr><td>" . __('You Don\'t make any transaction', WPD_NAME) . "</td></tr></tbody></table>";
        echo '<form><input id="single_payment_close" type="button" value="' . __('Close', WPD_NAME) . '" class="button"></form></div>';
        echo "<script>jQuery(document).ready(function(){ wpd_confirm_popup(); });</script>";
    }
}
?>
 <style type="text/css">
 .donation-figures {
    display: flex;
}
.nav.nav-tabs {
    display: flex !important;
}
 
 </style>
<div 
 class="<?php echo esc_attr( $span ); ?> column"
>

    <div id="f_overlay"></div>
    <div class="overlay">
        <div class="my_loader"></div>
    </div>
    <div id="bank_transcation"></div>
    <div class="wp-donation-system">
        <?php if (WPD_Common::wpd_set($options, 'history') == 'True'): ?>
            <h6>
                <a href="<?php echo esc_url($link) ?>">
                    <?php _e('VIEW TRANSACTION HISTORY', WPD_NAME) ?>
                </a>
            </h6>
        <?php endif; ?>
        <div class="easy-donation">
            <div class="donation-title">
                <h1><?php echo $title; ?></h1>
            </div>
            <?php
            global $opt;
            if (lifeline2_set($opt, 'donation_template_type') == 'donation_page_template') {
                if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
                    $paypal_res = require_once(WPD_ROOT . 'application/library/paypal/review.php');
                    echo $paypal_res;
                    echo '<script>
                    jQuery(document).ready(function(){
                            wpd_confirm_popup();
                    });
              </script>';
                }
                ?>
                <div id="paypal_recuring_confirm"></div>
                <?php
            }
            ?>
            <div class="donation-fields">
                <div class="row">
                    <?php if (WPD_Common::wpd_set($options, 'country_selector') == 'True'): ?>
                        <div class="col-md-8 col-md-offset-2">
                            <select id="currency_">
                                <?php foreach ($symbols as $c): ?>
                                    <option value="<?php echo WPD_Common::wpd_set($c, 'value') ?>"><?php echo WPD_Common::wpd_set($c, 'label') ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php else: ?>
                        <input type="hidden" id="currency_" value="USD"/>
                    <?php endif; ?>
                    <div class="wpdonation-box">
                        <?php if (WPD_Common::wpd_set($options, 'builder_status') == 1 && WPD_Common::wpd_set($options, 'builder_vals') != 0): ?>
                            <h2 class="wpdonation-title"><?php _e('How much would you like to donate?', WPD_NAME) ?></h2>
                            <ul class="donation-figures">
                                <?php
                                $values = explode(',', $builder);
                                foreach ($values as $val):
                                    ?>
                                    <li><a class="wpdonation-button" href="#" title=""><?php echo $val ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <?php if (WPD_Common::wpd_set($options, 'custom_amount') == 1): ?>
                            <div class="donation-amount">
                                <div class="textfield"><textarea id="donation_amount" placeholder="<?php _e('Enter The Amount You Want', WPD_NAME) ?>"></textarea><span id="errmsg"></span></div>
                            </div>
                        <?php else: ?>
                            <div style="display:none;" class="donation-amount">
                                <div class="textfield"><textarea id="donation_amount" placeholder="<?php _e('Enter The Amount You Want', WPD_NAME) ?>"></textarea><span id="errmsg"></span></div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (WPD_Common::wpd_set($options, 'single') == 1 || WPD_Common::wpd_set($options, 'recuring') == 1): ?>
                        <div class="wpdonation-box">
                            <div id="no_selection"></div>
                            <h2 class="wpdonation-title"><?php _e('Select Your Payment Type', WPD_NAME) ?></h2>
                            <ul role="tablist" class="nav nav-tabs" id="myTab">
                                <?php if (WPD_Common::wpd_set($options, 'recuring') == 1): ?>
                                    <li><a id="reccuring_" data-toggle="tab" href="#reccuring"><?php _e('Recurring', WPD_NAME); ?></a></li><?php endif; ?>
                                <?php if (WPD_Common::wpd_set($options, 'single') == 1): ?>
                                    <li><a id="payment_" data-toggle="tab" href="#payment"><?php _e('One Time Payment', WPD_NAME); ?></a></li><?php endif; ?>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div id="reccuring" class="tab-pane fade">
                                    <div class="payment-type">
                                        <h3><?php _e('Select Your Payment Option', WPD_NAME) ?></h3>
                                        <ul role="tablist" class="nav nav-tabs" id="child-tab">
                                            <?php if (WPD_Common::wpd_set($cycle_period, 'status') == 'enable'): ?>
                                                <li><a id="credit_" data-toggle="tab" href="#credit"><?php _e('Credit Card', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($paypal_period, 'status') == 'enable'): ?>
                                                <li><a data-toggle="tab" id="paypal_" href="#paypal"><?php _e('Paypal', WPD_NAME) ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                        <div class="tab-content" id="myTabContent2">
                                            <div id="credit" class="tab-pane fade">
                                                <p><?php _e('In Case of credit card ,Transaction in Dollar($) will be accepted.', WPD_NAME) ?></p>
                                                <div class="form">
                                                    <div id="rec_alerts"></div>
                                                    <?php if ($cycles && !empty($cycles)): ?>
                                                        <div class="col-md-offset-1 col-md-5">
                                                            <div class="textfield">
                                                                <select id="rec_cycle">
                                                                    <option selected="selected" value=""><?php _e('Payment Cycle', WPD_NAME) ?></option>
                                                                    <?php foreach ($cycles as $c): ?>
                                                                        <option value="<?php echo strtolower($c); ?>"><?php echo ucfirst($c); ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if ($cycles_times && !empty($cycles_times)): ?>
                                                        <div class="col-md-offset-1 col-md-5">
                                                            <div class="textfield">
                                                                <select id="rec_cycle_time">
                                                                    <option selected="selected" value=""><?php _e('Number of Cycle', WPD_NAME) ?></option>
                                                                    <?php foreach ($cycles_times as $t): ?>
                                                                        <option value="<?php echo strtolower($t); ?>"><?php echo ucfirst($t); ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="rec_card_no" type="text" placeholder="<?php _e('Enter your 16 digit card number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="rec_card-expiry-month">
                                                            <option selected="selected" value=""><?php _e('Expire Month', WPD_NAME) ?></option>
                                                            <?php
                                                            $mnth = range(1, 12);
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="rec_card-expiry-year">
                                                            <option selected="selected" value=""><?php _e('Expire Year', WPD_NAME) ?></option>
                                                            <?php
                                                            $year = range(2015, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-3 col-md-7">
                                                        <div class="textfield">
                                                            <input type="text" id="rec_card_cvc" placeholder="<?php _e('Card Verification Number', WPD_NAME) ?>"/>
                                                            <span id="errmsg"></span>
                                                            <img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="paypal" class="tab-pane fade">
                                                <div id="rec_alerts_paypal"></div>
                                                <?php if (!empty($plans)): ?>
                                                    <div class="col-md-offset-1 col-md-10">
                                                        <div class="textfield">
                                                            <select id="rec_cycle_paypal">
                                                                <option selected="selected" value=""><?php _e('Payment Cycle', WPD_NAME) ?></option>
                                                                <?php foreach ($plans as $c): ?>
                                                                    <option value="<?php echo strtolower($c); ?>"><?php echo ucfirst($c); ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="payment" class="tab-pane fade">
                                    <div id="single_payment"></div>
                                    <div class="payment-type">
                                        <h3><?php _e('Select Your Payment Option', WPD_NAME) ?></h3>
                                        <ul role="tablist" class="nav nav-tabs" id="child-tab">
                                            <?php if (WPD_Common::wpd_set($cycle_period, 'status') == 'enable'): ?>
                                                <li><a id="credit2_" data-toggle="tab" aria-expanded="true" href="#credit2"><?php _e('Credit Card', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($bank__, 'status') == 'enable'): ?>
                                                <li><a id="transfer2_" data-toggle="tab" aria-expanded="false" href="#transfer2"><?php _e('Banking Transfer', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($paypal_period, 'status') == 'enable'): ?>
                                                <li><a id="paypal2_" data-toggle="tab" aria-expanded="false" href="#paypal2"><?php _e('Paypal', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($authorizedSettings, 'status') == 'enable'): ?>
                                                <li><a id="authorized_" data-toggle="tab" aria-expanded="false" href="#authorized"><?php _e('Authorized.net', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($paymasterSettings, 'status') == 'enable'): ?>
                                                <li><a id="paymaster_" data-toggle="tab" aria-expanded="false" href="#paymaster"><?php _e('Paymaster', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($yandexSettings, 'status') == 'enable'): ?>
                                                <li><a id="yandex_" data-toggle="tab" aria-expanded="false" href="#yandex"><?php _e('Yandex', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($braintreeSettings, 'status') == 'enable'): ?>
                                                <li><a id="braintree_" data-toggle="tab" aria-expanded="false" href="#braintree"><?php _e('Braintree', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($bluepaySettings, 'status') == 'enable'): ?>
                                                <li><a id="bluepay_" data-toggle="tab" aria-expanded="false" href="#bluepay"><?php _e('Bluepay', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

	                                        <?php if (WPD_Common::wpd_set($conektaSettings, 'status') == 'enable'): ?>
                                                <li><a id="conekta_" data-toggle="tab" aria-expanded="false" href="#conekta"><?php _e('Conekta', WPD_NAME) ?></a></li>
	                                        <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($paystackSettings, 'status') == 'enable'): ?>
                                                <li><a id="paystack_" data-toggle="tab" aria-expanded="false" href="#paystack"><?php _e('Paystack', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

	                                        <?php if (WPD_Common::wpd_set($payumoneySettings, 'status') == 'enable'): ?>
                                                <li><a id="payumoney_" data-toggle="tab" aria-expanded="false" href="#payumoney"><?php _e('Payumoney', WPD_NAME) ?></a></li>
	                                        <?php endif; ?>

	                                        <?php if (WPD_Common::wpd_set($quickpaySettings, 'status') == 'enable'): ?>
                                                <li><a id="quickpay_" data-toggle="tab" aria-expanded="false" href="#quickpay"><?php _e('QuickPay', WPD_NAME) ?></a></li>
	                                        <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($cardcomSettings, 'status') == 'enable'): ?>
                                                <li><a id="cardcom_" data-toggle="tab" aria-expanded="false" href="#cardcom"><?php _e('Cardcom', WPD_NAME) ?></a></li>
                                            <?php endif; ?>

                                            <?php if (WPD_Common::wpd_set($twochecoutSettings, 'status') == 'enable'): ?>
                                                <li><a id="twocheckout_" data-toggle="tab" aria-expanded="false" href="#twocheckout"><?php _e('2Checkout', WPD_NAME) ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                        <div class="tab-content" id="myTabContent2">
                                            <div id="credit2" class="tab-pane fade ">
                                                <p><?php _e('In Case of credit card ,Transaction in Dollar($) will be accepted.', WPD_NAME) ?></p>
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_card_no" type="text" placeholder="<?php _e('Enter your 16 digit card number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="card-expiry-month">
                                                            <?php
                                                            $mnth = range(1, 12);
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="card-expiry-year">
                                                            <?php
                                                            $year = range(2015, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="card_cvc" type="text" placeholder="<?php _e('Card Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="twocheckout" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_twocheckout_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="twocheckout-expiry-month">
                                                            <?php
                                                            $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="twocheckout-expiry-year">
                                                            <?php
                                                            $year = range(2016, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="twocheckout_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="braintree" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_braintree_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="braintree-expiry-month">
                                                            <?php
                                                            $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="braintree-expiry-year">
                                                            <?php
                                                            $year = range(2016, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="braintree_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="bluepay" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_bluepay_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="bluepay-expiry-month">
                                                            <?php
                                                            $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="bluepay-expiry-year">
                                                            <?php
                                                            $year = range(2016, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="bluepay_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="conekta" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_conekta_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="conekta-expiry-month">
					                                        <?php
					                                        $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
					                                        foreach ($mnth as $m):
						                                        echo '<option value="' . $m . '">' . $m . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="conekta-expiry-year">
					                                        <?php
					                                        $year = range(2016, 2025);
					                                        foreach ($year as $y):
						                                        echo '<option value="' . $y . '">' . $y . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="conekta_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="paystack" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_paystack_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="paystack-expiry-month">
                                                            <?php
                                                            $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="paystack-expiry-year">
                                                            <?php
                                                            $year = range(2016, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="paystack_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="payumoney" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_payumoney_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="payumoney-expiry-month">
					                                        <?php
					                                        $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
					                                        foreach ($mnth as $m):
						                                        echo '<option value="' . $m . '">' . $m . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="payumoney-expiry-year">
					                                        <?php
					                                        $year = range(2016, 2025);
					                                        foreach ($year as $y):
						                                        echo '<option value="' . $y . '">' . $y . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="payumoney_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="quickpay" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_quickpay_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="quickpay-expiry-month">
					                                        <?php
					                                        $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
					                                        foreach ($mnth as $m):
						                                        echo '<option value="' . $m . '">' . $m . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="quickpay-expiry-year">
					                                        <?php
					                                        $year = range(2016, 2025);
					                                        foreach ($year as $y):
						                                        echo '<option value="' . $y . '">' . $y . '</option>';
					                                        endforeach;
					                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="quickpay_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="cardcom" class="tab-pane fade ">
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_cardcom_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="cardcom-expiry-month">
                                                            <?php
                                                            $mnth = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="cardcom-expiry-year">
                                                            <?php
                                                            $year = range(2016, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="cardcom_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="authorized" class="tab-pane fade ">
                                                <p><?php _e('In Case of credit authorized ,Transaction in Dollar($) will be accepted.', WPD_NAME) ?></p>
                                                <div class="form">
                                                    <div class="col-md-offset-1 col-md-11">
                                                        <div class="textfield"><input id="single_authorized_no" type="text" placeholder="<?php _e('Enter your 16 digit authorized number', WPD_NAME) ?>"/><span id="errmsg"></span></div>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="authorized-expiry-month">
                                                            <?php
                                                            $mnth = range(1, 12);
                                                            foreach ($mnth as $m):
                                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5 col-md-offset-1">
                                                        <select id="authorized-expiry-year">
                                                            <?php
                                                            $year = range(2015, 2025);
                                                            foreach ($year as $y):
                                                                echo '<option value="' . $y . '">' . $y . '</option>';
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-8">
                                                        <div class="textfield"><input id="authorized_cvc" type="text" placeholder="<?php _e('authorized Verification Number', WPD_NAME) ?>"/><span id="errmsg"></span><img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="transfer2" class="tab-pane fade"></div>
                                            <div id="paypal2" class="tab-pane fade"></div>
                                            <div id="paymaster" class="tab-pane fade"></div>
                                            <div id="yandex" class="tab-pane fade"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php $http = (is_ssl()) ? 'https://' : 'http://';?>
                <div class="wpdonation-box">
                    <div id="no_selections"></div>
                    <h2 class="wpdonation-title"><?php _e('PERSONAL DETAIL', WPD_NAME) ?></h2>
                    <div class="form">
                        <div class="col-md-6">
                            <div class="textfield"><input id="f_name" type="text" placeholder="<?php _e('First Name', WPD_NAME) ?>"/></div>
                        </div>
                        <div class="col-md-6">
                            <div class="textfield"><input id="l_name" type="text" placeholder="<?php _e('Last Name', WPD_NAME) ?>"/></div>
                        </div>
                        <div class="col-md-6">
                            <div class="textfield"><input id="email" type="email" placeholder="<?php _e('Email Id', WPD_NAME) ?>"/></div>
                        </div>
                        <div class="col-md-6">
                            <div class="textfield"><input id="contact_no" type="text" placeholder="<?php _e('Phone Number', WPD_NAME) ?>"/></div>
                        </div>
                        <div class="col-md-12">
                            <div class="textfield"><textarea id="address" placeholder="<?php _e('Address', WPD_NAME) ?>"></textarea></div>
                        </div>
                        <input type="hidden" id="return_url" name="return_url" value="<?php echo esc_url(strtok($http.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], '?')) ?>"/>
                        <input type="hidden" id="myurl" name="myurl" value="<?php //echo esc_url( get_permalink($donation_success_page) ); ?>"/>
                    </div>
                </div>
                <button id="wpd_donate_now" type="submit"><?php _e('Donate Now', WPD_NAME) ?></button>
            </div>

        </div>

    </div>

</div>

<?php 
