jQuery(document).ready(function ($) {
    var field = $('form#contact-form #purchase_code');
    $(field).mask('AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA', {
        placeholder: "________-____-____-____-____________"
    });

    $("#my_accord").accordion({
        collapsible: true,
        header: "> div > div > article > h3",
    }).sortable({
        axis: "y",
        handle: "h3",
        stop: function (event, ui) {
            ui.item.children("h3").triggerHandler("focusout");
            $(this).accordion("refresh");
        }
    });

    jQuery("input#enable_builder_settings").on('click', function () {
        if (jQuery(this).is(':checked')) {
            var val = jQuery(this).val();
            if (val == 'enable') {
                jQuery(this).parent().next().show();
            }
        } else {
            jQuery(this).parent().next().hide();
        }
    });

    jQuery("input#enable_language_selector").on('click', function () {
        if (jQuery(this).is(':checked')) {
            var val = jQuery(this).val();
            if (val == 'enable') {
                jQuery(this).parent().next().show();
            }
        } else {
            jQuery(this).parent().next().hide();
        }
    });

    $('select#paypal_type, select#credit_card_type, #default_cruncy, #transaction_history, #language_bar_status, #currency_selector, #credit_card_plans, #language_selector, #authorized_type, #twocheckout_type, #paymaster_type, #yandex_type, #braintree_type, #bluepay_type, #conekta_type, #paystack_type, #payumoney_type, #quickpay_type, #cardcom_type').select2({
        allowClear: false,
    });

    $('#language_selector').select2({
        placeholder: "Select Language",
        allowClear: true,
    });

    $('#credit_card_plans, #credit_card_payment_cycle, #paypal_plans').select2({
        allowClear: false,
    });

    $('form#contact-form #paypal_submit_setttings').on('click', function () {

        $('form#contact-form #paypal_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_payapl_settings').is(':checked')) {

            var type = $('form#contact-form #paypal_type').val();
            var user = $('form#contact-form #paypal_user').val();
            var api_user = $('form#contact-form #paypal_api_user').val();
            var api_pass = $('form#contact-form #paypal_api_pass').val();
            var api_sign = $('form#contact-form #paypal_api_signature').val();
            var plans = $('form#contact-form #paypal_plans').val();

            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var alerts = [];
            var data = 'type=' + type + '&user=' + user + '&api_user=' + api_user + '&api_pass=' + api_pass + '&api_sign=' + api_sign + '&plans=' + plans + '&action=wpd_wp_donation_paypal_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (user == '') {
                alerts.push('Please Enter Your PayPal User Name');
            }
            if (filter.test(user) === false) {
                alerts.push('Please Enter Valid PayPal Email Address');
            }
            if (api_user == '') {
                alerts.push('Please Enter Your PayPal API User Name');
            }
            if (api_pass == '') {
                alerts.push('Please Enter Your PayPal API Password');
            }
            if (api_sign == '') {
                alerts.push('Enter Your PayPal API Signature');
            }
            console.log(alerts);
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #paypal_alerts').fadeIn('slow').empty();
                        $('form#contact-form #paypal_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #paypal_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #paypal_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #paypal_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #paypal_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #paypal_alerts').fadeIn('slow').empty();
            //$('form#contact-form #paypal_alerts').html("<div class=\"alert alert-warning\" role=\"alert\">Please Enabale PayPal Settings</div>");
            var data = 'status=disable&action=wpd_wp_donation_paypal_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #paypal_alerts').fadeIn('slow').empty();
                    $('form#contact-form #paypal_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #paypal_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #paypal_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('form#contact-form #paymaster_submit_setttings').on('click', function () {

        $('form#contact-form #paymaster_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_paymaster_settings').is(':checked')) {

            var type = $('form#contact-form #paymaster_type').val();
            //var user = $('form#contact-form #paymaster_user').val();
            var paymaster_merchant = $('form#contact-form #LMI_MERCHANT_ID').val();
            //var api_pass = $('form#contact-form #paymaster_api_pass').val();
            //var api_sign = $('form#contact-form #paymaster_api_signature').val();
            //var plans = $('form#contact-form #paymaster_plans').val();

            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var alerts = [];
            var data = '&paymaster_merchant=' + paymaster_merchant + '&action=wpd_wp_donation_paymaster_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            // if (filter.test(user) === false) {
            //     alerts.push('Please Enter Valid PayMaster Email Address');
            // }
            if (paymaster_merchant == '') {
                alerts.push('Please Enter Your PayMaster Account ID');
            }
            console.log(alerts);
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #paymaster_alerts').fadeIn('slow').empty();
                        $('form#contact-form #paymaster_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #paymaster_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #paymaster_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #paymaster_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #paymaster_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #paymaster_alerts').fadeIn('slow').empty();
            //$('form#contact-form #paypal_alerts').html("<div class=\"alert alert-warning\" role=\"alert\">Please Enabale PayPal Settings</div>");
            var data = 'status=disable&action=wpd_wp_donation_paymaster_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #paymaster_alerts').fadeIn('slow').empty();
                    $('form#contact-form #paymaster_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #paymaster_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #paymaster_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('form#contact-form #yandex_submit_setttings').on('click', function () {

        $('form#contact-form #yandex_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_yandex_settings').is(':checked')) {

            var type = $('form#contact-form #yandex_type').val();
            var shopId = $('form#contact-form #shopId').val();
            var scid = $('form#contact-form #scid').val();

            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var alerts = [];
            var data = 'type=' + type + '&shopId=' + shopId + '&scid=' + scid + '&action=wpd_wp_donation_yandex_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (shopId == '') {
                alerts.push('Please Enter Your Yandex Shop Id');
            }
            if (scid == '') {
                alerts.push('Enter Your Yandex Secret Id');
            }
            console.log(alerts);
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #yandex_alerts').fadeIn('slow').empty();
                        $('form#contact-form #yandex_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #yandex_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #yandex_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #yandex_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #yandex_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #yandex_alerts').fadeIn('slow').empty();
            //$('form#contact-form #paypal_alerts').html("<div class=\"alert alert-warning\" role=\"alert\">Please Enabale PayPal Settings</div>");
            var data = 'status=disable&action=wpd_wp_donation_yandex_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #yandex_alerts').fadeIn('slow').empty();
                    $('form#contact-form #yandex_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #yandex_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #yandex_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('form#contact-form #bank_submit_setttings').on('click', function () {

        $('form#contact-form #bank_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_bank_settings').is(':checked')) {

            var name = $('form#contact-form #bank_payable').val();
            var bank_name = $('form#contact-form #bank_name').val();
            var bic = $('form#contact-form #bank_bic_code').val();
            var iban = $('form#contact-form #iban_number').val();
            var branch_code = $('form#contact-form #branch_code').val();
            var account_num = $('form#contact-form #account_number').val();
            var address = $('form#contact-form #street_address').val();
            var postal_address = $('form#contact-form #postal_address').val();
            var contact_no = $('form#contact-form #contact_no').val();
            var fax_no = $('form#contact-form #fax_no').val();
            var email = $('form#contact-form #email').val();

            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var alerts = [];
            var data = 'name=' + name + '&bank_name=' + bank_name + '&bic=' + bic + '&branch_code=' + branch_code + '&iban=' + iban + '&account_num=' + account_num + '&address=' + address + '&postal_address=' + postal_address + '&contact_no=' + contact_no + '&fax_no=' + fax_no + '&email=' + email + '&action=wpd_wp_donation_bank_settings';

            if (name == '') {
                alerts.push('Please Enter Payable Name');
            }
            if (bank_name == '') {
                alerts.push('Please Enter Your Bank Name');
            }
            if (bic == '') {
                alerts.push('Please Enter Your BIC Code');
            }
            if (iban == '') {
                alerts.push('Please Enter Your IBAN Number');
            }
            if (branch_code == '') {
                alerts.push('Please Enter Your Branch Code');
            }
            if (account_num == '') {
                alerts.push('Please Enter Your Account Number');
            }
            if (address == '') {
                alerts.push('Please Enter Your Address');
            }
            if (postal_address == '') {
                alerts.push('Please Enter Your Postal Address');
            }
            if (contact_no == '') {
                alerts.push('Please Enter Your Contact No');
            }
            if (fax_no == '') {
                alerts.push('Please Enter Your FAX Number');
            }
            if (email == '') {
                alerts.push('Please Enter Your Email Address');
            }
            if (filter.test(email) === false) {
                alerts.push('Please Enter Valid Email Address');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #bank_alerts').fadeIn('slow').empty();
                        $('form#contact-form #bank_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #bank_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #bank_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #bank_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #bank_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #bank_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_donation_bank_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #bank_alerts').fadeIn('slow').empty();
                    $('form#contact-form #bank_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #bank_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #bank_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('form#contact-form #credit_submit_setttings').on('click', function () {
        $('form#contact-form #credit_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_credit_settings').is(':checked')) {
            var type = $('form#contact-form #credit_card_type').val();
            var key = $('form#contact-form #stripe_api_key').val();
            var publish_key = $('form#contact-form #stripe_publish_key').val();
            var cycle = $('form#contact-form #credit_card_plans').val();
            var number_of_cycle = $('form#contact-form #credit_card_payment_cycle').val();

            var alerts = [];
            var data = 'key=' + key + '&type=' + type + '&publish_key=' + publish_key + '&cycles=' + cycle + '&num_of_cycle=' + number_of_cycle + '&action=wpd_wp_credit_card_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (key == '') {
                alerts.push('Please Enter Stripe Api Key');
            }
            if (publish_key == '') {
                alerts.push('Please Enter Stripe Api Publish Key');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                        $('form#contact-form #credit_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #credit_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #credit_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #credit_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_credit_card_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('form#contact-form #authorized_submit_setttings').on('click', function () {
        $('form#contact-form #authorized_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_authorized_settings').is(':checked')) {
            var type = $('form#contact-form #authorized_type').val();
            var loginid = $('form#contact-form #auth_login_id').val();
            var transkey = $('form#contact-form #auth_trans_key').val();

            var alerts = [];
            var data = 'loginid=' + loginid + '&transkey=' + transkey + '&type=' + type + '&action=wpd_wp_authorized_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (loginid == '') {
                alerts.push('Please Enter Login ID');
            }
            if (transkey == '') {
                alerts.push('Please Enter Transaction Key');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #authorized_alerts').fadeIn('slow').empty();
                        $('form#contact-form #authorized_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #authorized_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #authorized_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #authorized_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #authorized_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #authorized_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_authorized_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#twocheckout_submit_setttings').on('click', function () {
        $('form#contact-form #twocheckout_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_twocheckout_settings').is(':checked')) {
            var type = $('#twocheckout_accordion  #twocheckout_type').val();
            var private_key = $('#twocheckout_accordion #private_key').val();
            var public_key = $('#twocheckout_accordion #public_key').val();
            var sellerId = $('#twocheckout_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&public_key=' + public_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_twocheckout_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #twocheckout_alerts').fadeIn('slow').empty();
                        $('form#contact-form #twocheckout_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #twocheckout_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #twocheckout_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #twocheckout_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #twocheckout_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #twocheckout_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_twocheckout_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#braintree_submit_setttings').on('click', function () {
        $('form#contact-form #braintree_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_braintree_settings').is(':checked')) {
            var type = $('#braintree_accordion  #braintree_type').val();
            var private_key = $('#braintree_accordion #private_key').val();
            var public_key = $('#braintree_accordion #public_key').val();
            var sellerId = $('#braintree_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&public_key=' + public_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_braintree_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #braintree_alerts').fadeIn('slow').empty();
                        $('form#contact-form #braintree_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #braintree_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #braintree_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #braintree_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #braintree_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #braintree_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_braintree_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#bluepay_submit_setttings').on('click', function () {
        $('form#contact-form #bluepay_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_bluepay_settings').is(':checked')) {
            var type = $('#bluepay_accordion  #bluepay_type').val();
            var private_key = $('#bluepay_accordion #private_key').val();
            //var public_key = $('#bluepay_accordion #public_key').val();
            var sellerId = $('#bluepay_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_bluepay_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #bluepay_alerts').fadeIn('slow').empty();
                        $('form#contact-form #bluepay_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #bluepay_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #bluepay_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #bluepay_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #bluepay_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #bluepay_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_bluepay_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#conekta_submit_setttings').on('click', function () {
        $('form#contact-form #conekta_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_conekta_settings').is(':checked')) {
            var type = $('#conekta_accordion  #conekta_type').val();
            var private_key = $('#conekta_accordion #private_key').val();
            //var public_key = $('#conekta_accordion #public_key').val();
            var sellerId = $('#conekta_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_conekta_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #conekta_alerts').fadeIn('slow').empty();
                        $('form#contact-form #conekta_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #conekta_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #conekta_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #conekta_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #conekta_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #conekta').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_conekta_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#paystack_submit_setttings').on('click', function () {
        $('form#contact-form #paystack_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_paystack_settings').is(':checked')) {
            var type = $('#paystack_accordion  #paystack_type').val();
            var private_key = $('#paystack_accordion #private_key').val();
            //var public_key = $('#paystack_accordion #public_key').val();
            var sellerId = $('#paystack_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_paystack_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #paystack_alerts').fadeIn('slow').empty();
                        $('form#contact-form #paystack_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #paystack_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #paystack_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #paystack_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #paystack_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #paystack_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_paystack_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#payumoney_submit_setttings').on('click', function () {
        $('form#contact-form #payumoney_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_payumoney_settings').is(':checked')) {
            var type = $('#payumoney_accordion  #payumoney_type').val();
            var private_key = $('#payumoney_accordion #private_key').val();
            //var public_key = $('#payumoney_accordion #public_key').val();
            var sellerId = $('#payumoney_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_payumoney_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #payumoney_alerts').fadeIn('slow').empty();
                        $('form#contact-form #payumoney_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #payumoney_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #payumoney_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #payumoney_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #payumoney_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #payumoney_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_payumoney_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#quickpay_submit_setttings').on('click', function () {
        $('form#contact-form #quickpay_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_quickpay_settings').is(':checked')) {
            var type = $('#quickpay_accordion  #quickpay_type').val();
            var private_key = $('#quickpay_accordion #private_key').val();
            //var public_key = $('#quickpay_accordion #public_key').val();
            var sellerId = $('#quickpay_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_quickpay_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #quickpay_alerts').fadeIn('slow').empty();
                        $('form#contact-form #quickpay_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #quickpay_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #quickpay_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #quickpay_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #quickpay_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #quickpay_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_quickpay_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });

    $('#cardcom_submit_setttings').on('click', function () {
        $('form#contact-form #cardcom_alerts').fadeOut('slow').empty();

        if ($('form#contact-form #enable_cardcom_settings').is(':checked')) {
            var type = $('#cardcom_accordion  #cardcom_type').val();
            var private_key = $('#cardcom_accordion #private_key').val();
            var public_key = $('#cardcom_accordion #public_key').val();
            var sellerId = $('#cardcom_accordion #sellerId').val();

            var alerts = [];
            var data = 'private_key=' + private_key + '&public_key=' + public_key + '&sellerId=' + sellerId + '&type=' + type + '&action=wpd_wp_cardcom_settings';

            if (type == '') {
                alerts.push('Please Select Type');
            }
            if (private_key == '') {
                alerts.push('Please Enter Private Key');
            }
            if (sellerId == '') {
                alerts.push('Please Enter Seller Id');
            }
            if (alerts.length === 0) {
                jQuery.ajax({
                    type: "post",
                    url: adminurl,
                    data: data,
                    beforeSend: function () {
                        jQuery('form#contact-form .overlay').fadeIn(500);
                    },
                    success: function (res) {
                        $('form#contact-form .overlay').fadeOut(500);
                        $('form#contact-form #cardcom_alerts').fadeIn('slow').empty();
                        $('form#contact-form #cardcom_alerts').html(res);
                        setTimeout(function () {
                            $('form#contact-form #cardcom_alerts').fadeOut('slow').empty();
                        }, 5000);
                    }
                });
            } else {
                $('form#contact-form #cardcom_alerts').fadeIn('slow');
                var newHTML = $.map(alerts, function (value) {
                    return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                });
                $('form#contact-form #cardcom_alerts').html(newHTML.join(""));
                setTimeout(function () {
                    $('form#contact-form #cardcom_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        } else {
            $('form#contact-form #cardcom_alerts').fadeIn('slow').empty();
            var data = 'status=disable&action=wpd_wp_cardcom_settings_disable';
            //$( 'form#contact-form #credit_alerts' ).html( "<div class=\"alert alert-warning\" role=\"alert\">Please Enabale Credit Card Settings</div>" );
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('form#contact-form .overlay').fadeIn(500);
                },
                success: function (res) {
                    $('form#contact-form .overlay').fadeOut(500);
                    $('form#contact-form #credit_alerts').fadeIn('slow').empty();
                    $('form#contact-form #credit_alerts').html(res);
                    setTimeout(function () {
                        $('form#contact-form #credit_alerts').fadeOut('slow').empty();
                    }, 5000);
                }
            });
            setTimeout(function () {
                $('form#contact-form #credit_alerts').fadeOut('slow').empty();
            }, 5000);
        }
        return false;
    });


    $('form#contact-form #basic_submit_setttings').on('click', function () {
        $('form#contact-form #basic_alerts').fadeOut('slow').empty();

        var builder_vals = [];
        $('div.price_builder .clone').find("input[type='text']").each(function () {
            builder_vals.push($(this).val());
        });
        var history = $('#transaction_history').val();
        var trans_num = $('#trans_num').val();
        var title = $('#wpd_title').val();
        var smsg = $('#smsg').val();
        var dmsg = $('#dmsg').val();
        var clrmsg = $('#clrmsg').val();
        var clrtext = $('#clrtext').val();
        var msgdes = $('#msgdes').val();
        var lang_bar = $("input[id='enable_language_selector']:checked").length
        var lang_ = $('#language_selector').val();
        var cunt_selector = $('#currency_selector').val();
        var status = $("input[id='enable_builder_settings']:checked").length
        var custom_logo = $("input[id='enable_custom_logo']:checked").length
        var single = $("input[id='enable_single_payment']:checked").length
        var recuring = $("input[id='enable_reccuring_payment']:checked").length
        var custom_amount = $("input[id='enable_custom_amount_box']:checked").length

        var data = 'history=' + history + '&trans_num=' + trans_num + '&title=' + title + '&smsg=' + smsg +  '&dmsg=' + dmsg + '&clrmsg=' + clrmsg + '&clrtext=' + clrtext + '&msgdes=' + msgdes + '&lang_bar=' + lang_bar + '&lang_=' + lang_ + '&country_selector=' + cunt_selector + '&builder_vals=' + builder_vals + '&status=' + status + '&custom_logo=' + custom_logo + '&single=' + single + '&recuring=' + recuring + '&custom_amount=' + custom_amount + '&action=wpd_save_basic_settings';

        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('form#contact-form .overlay').fadeIn(500);
            },
            success: function (res) {
                $('form#contact-form .overlay').fadeOut(500);
                $('form#contact-form #basic_alerts').fadeIn('slow').empty();
                $('form#contact-form #basic_alerts').html(res);
                setTimeout(function () {
                    $('form#contact-form #basic_alerts').fadeOut('slow').empty();
                }, 10000);
            }
        });

        return false;
    });

    $("#basic_accordion #contact-form .price_builder > div ").accordion({
        collapsible: true,
    });

    var regex = /^(.*)(\d)+$/i;
    var cloneIndex = $("#basic_accordion #contact-form .price_builder").length;

    function clone() {
        $(this).parents().children('.clone').last().clone(true, true)
            .insertBefore("#basic_accordion #contact-form .price_builder button#clone_price")
            .attr("id", "paypal_default_value" + cloneIndex)
            .children('input').attr("id", "paypal_default_value" + cloneIndex).attr("name", "paypal_default_value" + cloneIndex).attr("value", "")
            .find("*")
            .each(function () {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {
                    this.id = match[1] + (cloneIndex);
                }
            })
            .on('click', '#basic_accordion #contact-form .price_builder #clone_price', clone);
        cloneIndex++;
        $(this).parents().children('.clone').last().find('.heading').trigger('click');
        return false;
    }

    $("#basic_accordion #contact-form .price_builder #clone_price").on("click", clone);
    $("#basic_accordion #contact-form .price_builder").on('click', "div#remvoe_clone", function (e) {
        if (typeof $(this).parent().parent().attr('id') !== "undefined") {
            var x;
            var r = confirm("This action can not be undone, are you sure?");
            if (r == true) {
                $(this).closest(".clone").remove();
                e.preventDefault();
            }
        } else {
            alert('Please Keep At least 1 Value');
        }
    });
    function _(el) {
        return document.getElementById(el);
    }

    $("#lang_upload_btn").on('click', function () {
        if ($(".language_uploader > input[type='file']").val().length != 0) {
            jQuery('form#contact-form .overlay').fadeIn(500);
            var ftype = _("lang_file").files[0].name;
            var extension = ftype.substr((ftype.lastIndexOf(".") + 1));
            switch (extension) {
                case "zip":
                    break;
                default:
                    jQuery('div#lang_error').empty();
                    jQuery("div#lang_error").html("<div class=\"alert alert-warning\" role=\"alert\"><b>" + ftype + "</b> Unsupported file type!</div>");
                    jQuery('form#contact-form .overlay').fadeOut(500);
                    setTimeout(function () {
                        $('div#lang_error').fadeOut('slow').empty();
                    }, 10000);
                    return false;
            }
            var url = plug_uri;
            var file = _("lang_file").files[0];
            var formdata = new FormData();
            formdata.append("lang_file", file);
            var ajax = new XMLHttpRequest();
            ajax.open("POST", url + "/application/library/upload_lang.php");
            ajax.send(formdata);
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    jQuery('div#lang_error').empty();
                    jQuery("div#lang_error").html("<div class=\"alert alert-warning\" role=\"alert\"><b>" + ajax.responseText + "</b> </div>");
                }
            }
            jQuery('form#contact-form .overlay').fadeOut(500);
        } else {
            alert("Please Select Some File");
        }
        setTimeout(function () {
            $('div#lang_error').fadeOut('slow').empty();
        }, 10000);
        return false;
    });
});

function df_enable(selector) {
    jQuery(selector).on('click', function () {
        if (jQuery(this).is(':checked')) {
            var val = jQuery(this).val();
            var parent = jQuery(this).parent().parent().parent().attr('id');
            if (val == 'enable') {
                jQuery('article#' + parent + ' div#show_settings').show();
            }
        } else {
            var parent_ = jQuery(this).parent().parent().parent().attr('id');
            jQuery('article#' + parent_ + ' div#show_settings').hide();
        }
    });
}