<?php
if ( version_compare( PHP_VERSION, '5.3.0' ) < 0 ) {
	die( "You're runing WordPress on outdated PHP version. Please contact your hosting company and updgrade PHP to 5.3 or above. Learn more about new features in PHP 5.3 - http://www.php.net/manual/en/migration53.new-features.php For cPanel users - you may easily switch PHP version using your hosting settings." );
}
require_once( plugin_dir_path( __FILE__ ) . '/init.php' );
WPD_Donation_Module_Init::WPD_Init();

if ( isset( $_POST[ 'recurring_pp_submit' ] ) ) {
	require_once( WPD_ROOT . 'application/library/paypal/expresscheckout.php' );
}
