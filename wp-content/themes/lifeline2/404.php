<?php
get_header();
$opt = lifeline2_get_theme_options();
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 column">
                    <div class="error-page">
                        <div class="error-inner">
                        <i class="fa fa-exclamation-triangle"></i>
                        <h3><?php echo wp_kses(lifeline2_set($opt, '404_title'), true) ?></h3>
                        <strong><?php echo wp_kses(lifeline2_set($opt, '404_sub_title'), true) ?></strong>
                        <p><?php echo esc_attr(lifeline2_set($opt, '404_description')) ?></p>
                        <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                            <input id="s" name="s" type="text" placeholder ="<?php esc_attr_e('Search Other Keyword', 'lifeline2') ?>" />
                            <button><i class="fa fa-search"></i></button>
                        </form>
                    </div><!-- Error Page -->
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>