jQuery(function ($) {
    $('#soundcloud_track').hide().prev().hide();
    $('#own-audio').hide().prev().hide();
    $('select#lifeline2_audio_type').select2();
    lifeline2_show_correct_format($('#post-formats-select input[name="post_format"]:checked').attr('value'));
    $('#post-formats-select input[name="post_format"]').change(function () {
        lifeline2_show_correct_format($(this).attr('value'));
    });

    $('select#lifeline2_audio_type').on('change',function(e){
        if($(this).val() == 'SoundCloud'){
            $('#soundcloud_track').show().prev().show();
            $('#own-audio').hide().prev().hide();
        }else if($(this).val() == 'Own Audio'){
            $('#own-audio').show().prev().show();
            $('#soundcloud_track').hide().prev().hide();
        }
    });
});

function lifeline2_show_correct_format(format) {
    if (format == 'image') {
        lifeline2_featured_image_position();
    } else {
        lifeline2_reset_featured_image_position();
    }
    jQuery('#lifeline2_format_quote').hide();
    jQuery('#lifeline2_format_link').hide();
    jQuery('#lifeline2_format_audio').hide();
    jQuery('#lifeline2_format_gallery').hide();
    jQuery('#lifeline2_format_video').hide();
    jQuery('#lifeline2_format_status').hide();
    jQuery('#lifeline2_format_' + format).show();
}

function lifeline2_featured_image_position() {
    jQuery('#postimagediv').insertAfter('#postdivrich').find('.inside').css('text-align', 'center');
    jQuery('#postimagediv').addClass('top');
}
function lifeline2_reset_featured_image_position() {
    jQuery('#postimagediv').insertAfter('#tagsdiv-post_tag');
    jQuery('#postimagediv').removeClass('top');
}
