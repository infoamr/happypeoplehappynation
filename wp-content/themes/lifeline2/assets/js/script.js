jQuery(document).ready(function ($) {
    "use strict";

    $('.blog-detail-page img').each(function () {
        var title = $(this).attr('title');
        if (title == 'Image Alignment 1200x400') {
            $(this).parent('figure').attr('style', '');
            $(this).attr('height', '100%');
            $(this).attr('width', '100%');
        }
    });



    /*=================== Cart Item Cross Button ===================*/
    $(".cart-item i.ti-close").on("click", function () {
        $(this).parent().parent().parent().remove();
    });
    $(".checkout-page .cart-heading").on("click", function () {
        $(".cart-detail , .cart-bottom").slideUp();
        $(this).parent().find(".cart-detail , .cart-bottom").slideToggle();
    });


    /*=================== Signup and Login Buttons ===================*/
    $(".topbar a.signup-btn, .topbar a.login-btn, .join-team a.signup-btn").on("click", function () {
        $("body").find(".popup").fadeIn();
        if ($(this).hasClass("signup-btn")) {
            setTimeout(function () {
                $(".popup").find(".signup-form").addClass("active").fadeIn();
            });
        } else if ($(this).hasClass("login-btn")) {
            setTimeout(function () {
                $(".popup").find(".login-form").addClass("active").fadeIn();
            });
        }
        return false;
    });
    $("html, .close-btn").on("click", function () {
        $(".popup-form").removeClass("active").slideUp();
        $("body").find(".popup").fadeOut();
    });
    $(".popup-form").on("click", function (e) {
        e.stopPropagation();
    });

    /*=================== Responsive Menu ===================*/
    $("#responsive-menu > span.open-menu").on("click", function () {
        $(this).next(".menu-links").addClass("slide");
        $("body").addClass("move");
    });
    $(".menu-links span.close-btn").on("click", function () {
        $(".menu-links").removeClass("slide");
        $("body").removeClass("move");
        $("#responsive-menu .menu-links > ul li.menu-item-has-children ul").slideUp();
    });
    $("#responsive-menu .menu-links ul.menu li.menu-item-has-children i").live("click", function () {
        $(this).prev("ul").slideToggle();
        //alert('ok');
        return false;
    });
    $("#responsive-menu > span.open-menu,#responsive-menu .menu-links > ul li.menu-item-has-children i").on("click", function (e) {
        e.stopPropagation();
    });
    $("#responsive-menu .menu-links > ul li.menu-item-has-children").append('<i class="fa fa-angle-down"></i>');
    $("#responsive-menu > span.show-topbar").on("click", function () {
        $(this).parent().parent().find(".topbar").slideToggle();
        $(this).toggleClass("slide");
    });




    /*=================== STICKY HEADER ===================*/
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 70) {
            $(".stick").addClass("sticky");
        } else {
            $(".stick").removeClass("sticky");
        }
    });

    /*--- sticky header new ---*/
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        //>=, not <=
        if (scroll >= 8) {
            //clearHeader, not clearheader - caps H
            $(".logo-area").addClass("sticked");
        }
        else {
            //clearHeader, not clearheader - caps H
            $(".logo-area").removeClass("sticked");
        }
    });

//    /*=================== Set The Header Margin ===================*/
//    var menu_height = $(".menu").height();
//    if ($(".menu").hasClass("transparent")) {
//	$("header").css({"margin-bottom": "0"});
//    } else if ($(".menu").hasClass("black-transparent")) {
//	$("header").css({"margin-bottom": "0"});
//    } else {
//	$("header").css({"margin-bottom": menu_height});
//    }


    $(".donate-btn").on("click", function () {
        $(this).next(".enter-amount").toggleClass("proceed");
    });


//    $(".frequency li a").live("click", function () {
//	var thiss = this;
//
//	$(thiss).parent('li').parent('.frequency').children('li').each(function (element, selector) {
//	    $(selector).children('a').removeClass('active');
//	});
//	$(thiss).addClass("active");
//    });


    var wpdonation_button = $(".donation-figures li a");
    $(".donation-figures li a").on("click", function () {
        $(wpdonation_button).removeClass("active");
        $(this).addClass("active");
        var amount_val = $(this).attr("data-bind");
        $("#donation_amount").val(amount_val);
        $(".proceed-to-donate").css('display', 'block');
        return false;
    });


    $(".proceed-to-donate").on("click", function () {
        if ($('.frequency.payment-method > li > a.active').length != 0) {
            if ($('.loader-overlay').length == 0) {
                $('body').append('<div class="loader-overlay"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></div>');
            }
            var payment_method = $('.payment-box .payment-method li a.active').attr('data-bind');
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: 'payment_method=' + payment_method + '&action=lifeline2_payment_info',
                success: function (data) {
                    $('.donation-fields .payment-info').html(data);
                    $(".select").select2();
                    $('.loader-overlay').remove();
                }
            });
        } else {
            alert('Please select payment type in which you want to make transaction!');
        }

        return false;
    });
    $(".popup-centralize span.close").on("click", function () {
        $(this).parent().parent().parent().fadeOut();
        $("body,html").removeClass("stop");
    });


    /*=================== Video Active Class ===================*/
    $(".video > a").on("click", function () {
        $(this).parent().toggleClass("active");
        $('.video-title').toggleClass('active');
        return false;
    });
    // var iframe = document.getElementById('video');
    // // $f == Froogaloop
    // var player = $f(iframe);
    // // bind events
    // var playButton = jQuery(".video a.play");
    // playButton.bind("click", function() {
    //     player.api("play");
    // });
    // var pauseButton = jQuery(".video a.pause");
    // pauseButton.bind("click", function() {
    //     player.api("pause");
    // });



/*=================== Events Toggle ===================*/
    var event_desc = $(".event-desc");
    $(".event-toggle:first").addClass("active").find(".event-desc").slideDown();

    $(".event-toggle").on("click", function () {
        $(event_desc).slideUp();
        $(this).find(".event-desc").slideDown();
        $(".event-toggle").removeClass("active");
        $(this).addClass("active");
    });


    /*=================== Accordion ===================*/
    $(function () {
        $('.toggle .content').hide();
        $('.toggle h2:first').addClass('active').next().slideDown(500).parent().addClass("activate");
        $('.toggle h2').on("click", function () {
            if ($(this).next().is(':hidden')) {
                $('.toggle h2').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
            }
        });
    });


    /*=================== En Scroll ===================*/
    $('.menu-links > ul, .sideheader-menu').enscroll({
        showOnHover: false,
        verticalTrackClass: 'track3',
        verticalHandleClass: 'handle3'
    });

    /*=================== SideHeader ===================*/
    $(".sideheader .c-hamburger").on("click", function () {
        $(this).parent().toggleClass("show");
        $(this).parent().find(".side-megamenu").removeClass("active");
        $(".sideheader-menu > ul li.menu-item-has-children > ul").slideUp();
        $(".sideheader-menu > ul li.menu-item-has-children > a").removeClass("active");
    });
    $(".causemenu-btn .c-hamburger").on("click",function(){
        $(".sideheader.fancy, .logo-area").toggleClass("show");
        $(this).parent().find(".side-megamenu").removeClass("active");
        $(".sideheader-menu > ul li.menu-item-has-children > ul,.sideheader-menu > ul li.menu-item-has-children1 > ul").slideUp();
        $(".sideheader-menu > ul li.menu-item-has-children > a,.sideheader-menu > ul li.menu-item-has-children1 > a").removeClass("active");
    });
    $(".sideheader-menu > ul li.menu-item-has-children > a").on("click", function () {
        $(this).next("ul").slideToggle();
        $(this).toggleClass("active");
        $(this).parent().find(".side-megamenu").toggleClass("active");
        return false;
    });

});
/*=== Document.Ready Ends Here ===*/

jQuery(window).load(function () {
    "use strict";

    jQuery(".banner-popup .close").on("click", function () {
        jQuery(this).parent().parent().parent().removeClass('show').addClass('none');
    });
    jQuery('.parallax').scrolly({bgParallax: true});
    /*=================== Team Page ===================*/
    var l = jQuery("#team-detail-img > ul li").length;
    for (var i = 0; i <= l; i++) {
        var team_list = jQuery("#team-detail-img > ul li").eq(i);
        var team_width = jQuery(team_list).find("p").width();
        jQuery(team_list).find("p").css({
            "margin-right": -team_width - 21
        });
    }
});
/*=== Window.Load Ends Here ===*/

function lifeline2_color_scheme(color) {
    var rep;
    if (color === '') {
        rep = '#e47257';
    } else {
        rep = color;
    }
    var file = lifeline2_url + 'assets/css/colors/color.css';
    jQuery.ajax({
        url: file,
        dataType: "text",
        success: function (cssText) {
            jQuery("<style></style>").appendTo("head").html(cssText.replace(/#e47257/g, rep));
        }
    });
}


jQuery(window).on('load',function () {
    jQuery('div.bg_overlay').hide();


});


// start donation modal settings
jQuery(document).ready(function ($) {
    $('a.donation-modal-box-caller').live('click', function () {
        var donation = $(this).data('donation');
        var postId = $(this).data('post');
        var modal = '';
        if ($(this).attr('data-modal')) {
            modal = $(this).data('modal');
        }
        var data = 'modal=' + modal + '&type=' + donation + '&id=' + postId + '&action=lifeline2_donationModalCaller';
        $.ajax({
            type: "post",
            url: adminurl,
            data: data,
            dataType: 'json',
            beforeSend: function () {
                donationPreIn();
            },
            success: function (response) {
                donationPreOut();
                $("div.donation-modal-box").empty();
                if (response.status === true) {
                    $('html').addClass('modalOpen');
                    $("div.donation-modal-box").html(response.msg);
                    $("div.donation-modal-box").show();
                    $('div.donation-modal-box > div.donation-popup').show();
                }
            }
        });
        return false;
    });
    // show donation preloader
    function donationPreIn() {
        jQuery('div.donation-modal-wraper').fadeIn(500);
        jQuery('div.donation-modal-preloader').fadeIn(500);
    }

    function donationPreOut() {
        jQuery('div.donation-modal-wraper').fadeOut(500);
        jQuery('div.donation-modal-preloader').fadeOut(500);
    }

    $('div.donation-modal-box span.close').live('click', function () {
        $('html').removeClass('modalOpen');
        $("div.donation-modal-box").empty();
        $("div.donation-modal-box").hide();
    });

    $(".proceed-to-donate").live("click", function () {
        $('.donation-step2').removeClass('hide');
    });

    // start donation type

    $(".frequency.donation-type li a").live("click", function () {

        $('div#step1-error').empty();
        $('div#step2-error').empty();
        $('.paragraph_default').hide();
        $('.donation-step1').removeClass('hide');
        
        var $this = this;
        var type = $(this).data('type');
        var donationFor = $(this).data('for');
        var postId = $(this).data('post');
        $($this).parent('li').parent('.frequency').children('li').each(function (element, selector) {
            $(selector).children('a').removeClass('active');
        });
        $($this).addClass("active");
        var data = 'donationfor=' + donationFor + '&postid=' + postId + '&type=' + type + '&action=lifeline2_donationType';
        $.ajax({
            type: "post",
            url: adminurl,
            data: data,
            dataType: 'json',
            beforeSend: function () {
                donationPreIn();
            },
            success: function (response) {
                donationPreOut();
                $("div.donation-fields.donation-step1").empty();
                $("div.donation-fields.donation-step2").empty();
                if (response.status === true) {
                    $("div.donation-fields.donation-step1").html(response.msg);
                    $("div.donation-fields.donation-step1").slideDown('slow');
                }
            }
        });
        return false;
    });

    $('select#donation_currency').live('change', function () {
        var val = $(this).val();
        var amount = '';
        if ($('textarea#donation_amount').length > 0) {
            var textVal = $('textarea#donation_amount').val();
            if (!textVal == '') {
                amount = textVal;
            }
        } else {
            amount = $('a.wpdonation-button.active').data('value');
        }
        $('a.proceed.proceed-to-donate').attr('data-amount', amount).attr('data-currency', val);
    });

    $('a.wpdonation-button').live('click', function () {
        $(document).find('a.wpdonation-button').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var amount = $(this).data('value');
        var currency = $('select#donation_currency').val();
        $('a.proceed.proceed-to-donate').attr('data-amount', amount).attr('data-currency', currency);
        if ($('textarea#donation_amount').length > 0) {
            $('textarea#donation_amount').val(amount);
        }
        return false;
    });

    $("textarea#donation_amount").live('keydown', function (e) {
        $('a.wpdonation-button.active').removeClass('active');
        var currency = $('select#donation_currency').val();
        var amount = $(this).val();
        $('a.proceed.proceed-to-donate').attr('data-amount', amount).attr('data-currency', currency);
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("textarea#donation_amount").live('blur', function () {
        var currency = $('select#donation_currency').val();
        var amount = $(this).val();
        $('a.proceed.proceed-to-donate').attr('data-amount', amount).attr('data-currency', currency);
    });

    // step 2
    $("a.proceed.proceed-to-donate").live("click", function () {
        var $this = this;
        var type = $(this).data('type');
        var amount = $(this).data('amount');
        var currency = $(this).data('currency');
        var donationFor = $(this).data('for');
        var postId = $(this).data('post');
        var data = 'donationfor=' + donationFor + '&postid=' + postId + '&type=' + type + '&amount=' + amount + '&curecny=' + currency + '&action=lifeline2_donationStep2';
        $.ajax({
            type: "post",
            url: adminurl,
            data: data,
            dataType: 'json',
            beforeSend: function () {
                donationPreIn();
            },
            success: function (response) {
                donationPreOut();
                if (response.status === true) {
                    $("#step1-error").empty();
                    $("div.donation-fields.donation-step2").empty();
                    $("div.donation-fields.donation-step1").slideUp('slow');
                    $("div.donation-fields.donation-step2").html(response.msg);
                    $("div.donation-fields.donation-step2").slideDown('slow');
                } else if (response.status === false) {
                    $('div#step1-error').empty();
                    $('div#step1-error').html(response.msg);
                    $('div#step1-error').slideDown();

                    var scrollTop = $('.donation-popup').scrollTop(),
                        elementOffset = $("#step1-error").offset().top,
                        distance = (elementOffset - scrollTop);
                    $('.donation-popup').animate({scrollTop: distance});

                }
            }
        });
        return false;
    });

    $('a.wpdonation-button.rec-payment-gateway').live('click', function () {
        var type = $(this).data('type');
        $(document).find('a.wpdonation-button.rec-payment-gateway').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        if (type == 'credit') {
            $('div.recuring-credit-card').slideDown('slow');
            $('div.recuring-paypal').slideUp('slow');
        } else if (type == 'paypal') {
            $('div.recuring-credit-card').slideUp('slow');
            $('div.recuring-paypal').slideDown('slow');
            //lifeline2_donationStep2
        }
    });

    $('a.wpdonation-button.single-payment-gateway').live('click', function () {
        var type = $(this).data('type');
        $(document).find('a.wpdonation-button.single-payment-gateway').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        if (type == 'scredit' || type == 'authorized' || type == '2checkout' || type == 'braintree' || type == 'bluepay' || type == 'conekta' || type == 'cardcom') {
            $('div.single-credit-card').slideDown('slow');
        } else if (type == 'spaypal') {
            $('div.single-credit-card').slideUp('slow');
        } else if (type == 'bank') {
            $('div.single-credit-card').slideUp('slow');
    } else if (type == 'quickpay') {
            $('div.single-credit-card').slideUp('slow');
        } else if (type == 'payumoney') {
            $('div.single-credit-card').slideUp('slow');
        }

    });

    $("button#wpd_donate_now").live("click", function () {
        var type = $(this).data('type');
        var amount = $(this).data('amount');
        var currency = $(this).data('currency');
        var donationFor = $(this).data('for');
        var postId = $(this).data('post');
        var isChecked = '';
        if ($('a.wpdonation-button').hasClass('rec-payment-gateway')) {
            $('a.wpdonation-button.rec-payment-gateway').each(function () {
                if ($(this).hasClass('active')) {
                    isChecked = $(this).data('type');
                }
            });
        } else {
            $('a.wpdonation-button.single-payment-gateway').each(function () {
                if ($(this).hasClass('active')) {
                    isChecked = $(this).data('type');
                }
            });
        }
        if (isChecked == '') {
            $('div#step2-error').empty();
            $('div#step2-error').html(lf2.rpc);
            $('div#step2-error').show();
            sTop();
            return false;
        }
        var donation = [type, amount, currency, donationFor, postId];
        if (isChecked == 'credit') {
            recCreditCard(donation);
        }
        if (isChecked == 'paypal') {
            recPaypal(donation);
        }

        // single payment transaction
        if (isChecked == 'scredit') {
            singleCreditCard(donation);
        }
        if (isChecked == 'authorized') {
            singleAuthorized(donation);
        }
        if (isChecked == '2checkout') {
            single2Checkout(donation);
        }
        if (isChecked == 'spaypal') {
            singlePaypal(donation);
        }
        if (isChecked == 'paymaster') {
            singlePaymaster(donation);
        }
        if (isChecked == 'yandex') {
            singleYandex(donation);
        }
        if (isChecked == 'braintree') {
            singlebraintree(donation);
        }
        if (isChecked == 'bluepay') {
            singlebluepay(donation);
        }
        if (isChecked == 'conekta') {
            singleconekta(donation);
        }
        if (isChecked == 'paystack') {
            singlepaystack(donation);
        }
        if (isChecked == 'payumoney') {
            singlepayumoney(donation);
        }
        if (isChecked == 'quickpay') {
            singlequickpay(donation);
        }
        if (isChecked == 'cardcom') {
            singlecardcom(donation);
        }
        if (isChecked == 'bank') {
            bnakTransfer(donation);
        }
    });

    $('button#popup-back-step').live('click', function () {
        $('div#step2-error').empty();
        $('div#step1-error').empty();
        $("div.donation-fields.donation-step2").empty();
        $("div.donation-fields.donation-step1").slideDown('slow');
        $("div.donation-fields.donation-step2").slideUp('slow');
    });

    function sTop() {
        var scrollTop = $('.donation-popup').scrollTop(),
            elementOffset = $("#step1-error").offset().top,
            distance = (elementOffset - scrollTop);
        $('.donation-popup').animate({scrollTop: distance});
    }

    function recCreditCard(dataArray) {
        var rec_cycle = $('select#rec_cycle').val();
        var rec_cycle_time = $('select#rec_ctime').val();
        var card_no = $('input#rec_cn').val();
        var exp_month = $('select#rec_cem').val();
        var exp_year = $('select#rec_cey').val();
        var cvc = $('input#rec_ccvc').val();

        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();

        var messages = [lf2.rpc, lf2.payCycle, lf2.payNubCycl, lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [rec_cycle, rec_cycle_time, card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            Stripe.setPublishableKey(stripe_key);
            var generateToken = function (e) {
                $("#step2-error").empty();
                var form = jQuery('div.form.easy-donation-box');
                form.find('button#wpd_donate_now').prop('disabled', true);
                Stripe.create(form, stripeResponseHandler);
                e.preventDefault();
            };

            // form processing
            var stripeResponseHandler = function (status, response) {
                var form = jQuery('div.form.easy-donation-box');
                if (response.error) {
                    $("#step2-error").empty();
                    $("#step2-error").html(response.error.message);
                    form.find('button#submit-button').prop('disabled', false);
                    donationPreOut();
                } else {
                    $('<input>', {
                        'type': 'hidden',
                        'name': 'stripeToken',
                        'value': response.id,
                        'id': 'payment_access_tocken'
                    }).appendTo(form);
                    var token = $('input#payment_access_tocken').val();
                    var amount = dataArray['1'];
                    var curr = dataArray['2'];
                    var donationFor = dataArray['3'];
                    var postId = dataArray['4'];
                    var rec_cycle = $('#rec_cycle').val();
                    var rec_cycle_time = $('#rec_ctime').val();
                    var email = $('#email').val();
                    var data = 'mail=' + email + '&interval=' + rec_cycle + '&interval_count=' + rec_cycle_time + '&donationFor=' + donationFor + '&postId=' + postId + '&token=' + token + '&currency=' + curr + '&amout=' + amount + '&action=lifeline2_processRecCredit';
                    jQuery.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        dataType: 'json',
                        beforeSend: function () {
                            donationPreIn();
                        },
                        success: function (credi_respnse) {
                            $("#step2-error").empty();
                            if (credi_respnse.status === true) {
                                dataArray.push(credi_respnse.msg);
                                createInvoice(dataArray, true)
                            } else if (credi_respnse.status === false) {
                                $("#step2-error").empty();
                                donationPreOut();
                                $("#step2-error").html(credi_respnse.msg);
                                $("#step2-error").show();
                                var scrollTop = $('.donation-popup').scrollTop(),
                                    elementOffset = $("#step1-error").offset().top,
                                    distance = (elementOffset - scrollTop);
                                $('.donation-popup').animate({scrollTop: distance});
                            }
                        }
                    });
                    return false;
                    donationPreOut();
                }
            }
            // form processing
            donationPreIn();
            Stripe.createToken({
                number: card_no,
                cvc: cvc,
                exp_month: exp_month,
                exp_year: exp_year,
            }, stripeResponseHandler);
        } else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function recPaypal(dataArray) {
        var amount = dataArray['1'];
        var rec_cycle = jQuery('#rec_cycle_paypal').val();
        var f_name = jQuery('#f_name').val();
        var l_name = jQuery('#l_name').val();
        var email = jQuery('#email').val();
        var contact_no = jQuery('#contact_no').val();
        var address = jQuery('#address').val();
        var currency = dataArray['2'];
        var data = 'amount=' + amount + '&currency=' + currency + '&rec_cycle=' + rec_cycle + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_processPaypalRec';
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var messages = [lf2.rpc, lf2.payCycle, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [amount, rec_cycle, f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;
        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });

        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            if (payPalRecData(dataArray) == 'ok') {
                setTimeout(function () {
                    $.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        beforeSend: function () {
                            donationPreIn();
                        },
                        success: function (credi_respnse) {

                            $("div#step2-error").empty();
                            $("div#step2-error").html(credi_respnse);
                            $("div#step2-error").show();
                            donationPreOut();
                        }
                    });
                }, 1000);
            }
        } else {
            jQuery("#step2-error").fadeIn('slow');
            var newHTML = jQuery.map(alerts, function (value) {
                return (value);
            });
            jQuery("#step2-error").html(newHTML.join(""));
            sTop();
            setTimeout(function () {
                jQuery("#step2-error").fadeOut('slow').empty();
            }, 15000);
        }
    }

    function createInvoice(dataArray, rec) {
        var rec_cycle = $('#rec_cycle').val();
        var rec_cycle_time = $('#rec_ctime').val();
        var amount = dataArray['1'];
        var currency = dataArray['2'];
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();
        var dType = dataArray['3']
        var dPost = dataArray['4'];
        var data = '';
        if (rec === true) {
            data = 'rec_cycle=' + rec_cycle + '&cycle_time=' + rec_cycle_time + '&recuring=yes&transid=' + dataArray['5'] + '&donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_SaveDonationRecord';
        } else if (rec === false) {
            data = 'transid=' + dataArray['5'] + '&donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_SaveDonationRecord';
        }
        $.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                donationPreIn();
            },
            success: function (credi_respnse) {
                donationPreOut();
                alert(credi_respnse);
                window.location.replace(lf2.homeUrl);
            }
        });
    }
    function lifeline2_confirm_popup() {
        jQuery('#f_overlay').fadeIn();
        var get_height = jQuery('div.confirm_popup').height();
        var height = get_height / 2;
        jQuery('div.confirm_popup').css({
            'margin-top': -height
        });
    }
    function payPalRecData(dataArray) {
        var amount = dataArray['1'];
        var rec_cycle = $('#rec_cycle_paypal').val();
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();
        var curr = dataArray['2'];
        var dType = dataArray['3'];
        var dPost = dataArray['4'];
        var data = 'donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&rec_cycle=' + rec_cycle + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&currency=' + curr + '&address=' + address + '&action=lifeline2_savePaypalRecDetails';
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                donationPreIn();
            },
            success: function (response) {
                $("div#step2-error").empty();
                $("div#step2-error").html(response);
                $("div#step2-error").show();
                donationPreOut();
            }
        });
        return 'ok';
    }

    $("#paypal_confirmation").live('click', function () {
        jQuery('div.confirm_popup').fadeOut();
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: 'action=lifeline2_confirmPaypalRecuring',
            beforeSend: function () {
                $('div.donation-modal-wraper').css({
                    'z-index': 99999999
                });
                $('div.donation-modal-preloader').show();
            },
            success: function (paypal_respnse) {
                jQuery("div#paypal_recuring_confirm").empty();
                jQuery("div#paypal_recuring_confirm").html(paypal_respnse);
                jQuery("div#paypal_recuring_confirm").show();
                $('div.donation-modal-wraper').css({
                    'z-index': 999999
                });
                $('div.donation-modal-preloader').hide();
            }
        });
        return false;
    });

    function singleCreditCard(dataArray) {
        var card_no = $('input#rec_cn').val();
        var exp_month = $('select#rec_cem').val();
        var exp_year = $('select#rec_cey').val();
        var cvc = $('input#rec_ccvc').val();

        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();

        var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            Stripe.setPublishableKey(stripe_key);
            var generateToken = function (e) {
                $("#step2-error").empty();
                var form = jQuery('div.form.easy-donation-box');
                form.find('button#wpd_donate_now').prop('disabled', true);
                Stripe.create(form, stripeResponseHandler);
                e.preventDefault();
            };

            // form processing
            var stripeResponseHandler = function (status, response) {
                var form = jQuery('div.form.easy-donation-box');
                if (response.error) {
                    $("#step2-error").empty();
                    $("#step2-error").html(response.error.message);
                    form.find('button#submit-button').prop('disabled', false);
                    donationPreOut();
                } else {
                    $('<input>', {
                        'type': 'hidden',
                        'name': 'stripeToken',
                        'value': response.id,
                        'id': 'payment_access_tocken'
                    }).appendTo(form);
                    var token = $('input#payment_access_tocken').val();
                    var amount = dataArray['1'];
                    var curr = dataArray['2'];
                    var donationFor = dataArray['3'];
                    var postId = dataArray['4'];
                    var data = 'donationFor=' + donationFor + '&postId=' + postId + '&token=' + token + '&currency=' + curr + '&amout=' + amount + '&action=lifeline2_processSingleCredit';
                    jQuery.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        dataType: 'json',
                        beforeSend: function () {
                            donationPreIn();
                        },
                        success: function (credi_respnse) {
                            $("#step2-error").empty();
                            if (credi_respnse.status === true) {
                                dataArray.push(credi_respnse.msg);
                                createInvoice(dataArray, false)
                            } else if (credi_respnse.status === false) {
                                $("#step2-error").empty();
                                donationPreOut();
                                $("#step2-error").html(credi_respnse.msg);
                                $("#step2-error").show();
                                sTop();
                            }
                        }
                    });
                    return false;
                    donationPreOut();
                }
            }
            // form processing
            donationPreIn();
            Stripe.createToken({
                number: card_no,
                cvc: cvc,
                exp_month: exp_month,
                exp_year: exp_year,
            }, stripeResponseHandler);
        } else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function singleAuthorized(dataArray) {
        var card_no = $('input#rec_cn').val();
        var exp_month = $('select#rec_cem').val();
        var exp_year = $('select#rec_cey').val();
        var cvc = $('input#rec_ccvc').val();

        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();

        var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            var amount = dataArray['1'];
            var curr = dataArray['2'];
            var donationFor = dataArray['3'];
            var postId = dataArray['4'];
            var data = 'donationFor=' + donationFor +
                '&postId=' + postId +
                '&card_no=' + card_no +
                '&exp_month=' + exp_month +
                '&exp_year=' + exp_year +
                '&cvc=' + cvc +
                '&f_name=' + f_name +
                '&l_name=' + l_name +
                '&email=' + email +
                '&contact_no=' + contact_no +
                '&address=' + address +
                '&currency=' + curr +
                '&amout=' + amount +
                '&action=lifeline2_processAuthorized';
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    donationPreIn();
                },
                success: function (credi_respnse) {
                    $("#step2-error").empty();
                    donationPreOut();
                    if (credi_respnse.status === true) {
                        alert(credi_respnse.msg);
                        window.location.replace(homeUrl);
                    } else if (credi_respnse.status === false) {
                        $("#step2-error").empty();
                        donationPreOut();
                        $("#step2-error").html(credi_respnse.msg);
                        $("#step2-error").show();
                        sTop();
                    }
                }
            });
            return false;
        }

        else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function single2Checkout(dataArray) {
        TCO.loadPubKey('sandbox', function () {
            if (twocheckout_key != '') {
                var card_no = $('input#rec_cn').val();
                var exp_month = $('select#rec_cem').val();
                var exp_year = $('select#rec_cey').val();
                var cvc = $('input#rec_ccvc').val();

                var f_name = $('#f_name').val();
                var l_name = $('#l_name').val();
                var email = $('#email').val();
                var contact_no = $('#contact_no').val();
                var address = $('#address').val();

                var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
                var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                var alerts = [];
                var counter = 0;

                $(fields).each(function (index, element) {
                    if (element == '') {
                        alerts.push(messages[counter]);
                    }
                    counter++;
                });
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(email) === false) {
                    alerts.push(lf2.vMail);
                }
                if (alerts.length === 0) {
                    var errorCallback = function (data) {
                        if (data.errorCode === 200) {
                            tokenRequest();
                        } else {
                            $("#step2-error").empty();
                            donationPreOut();
                            $("#step2-error").html(data.errorMsg);
                            $("#step2-error").show();
                            sTop();
                            setTimeout(function () {
                                $("#step2-error").empty();
                            }, 5000);
                            return false;
                        }
                    };
                    var successCallback = function (data) {
                        var amount = dataArray['1'];
                        var curr = dataArray['2'];
                        var donationFor = dataArray['3'];
                        var postId = dataArray['4'];
                        var data = 'donationFor=' + donationFor +
                            '&token=' + data.response.token.token +
                            '&postId=' + postId +
                            '&card_no=' + card_no +
                            '&exp_month=' + exp_month +
                            '&exp_year=' + exp_year +
                            '&cvc=' + cvc +
                            '&f_name=' + f_name +
                            '&l_name=' + l_name +
                            '&email=' + email +
                            '&contact_no=' + contact_no +
                            '&address=' + address +
                            '&currency=' + curr +
                            '&amout=' + amount +
                            '&action=lifeline2_process2Checkout';
                        jQuery.ajax({
                            type: "post",
                            url: adminurl,
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                donationPreIn();
                            },
                            success: function (credi_respnse) {
                                $("#step2-error").empty();
                                donationPreOut();
                                if (credi_respnse.status === true) {
                                    alert(credi_respnse.msg);
                                    window.location.replace(homeUrl);
                                } else if (credi_respnse.status === false) {
                                    $("#step2-error").empty();
                                    donationPreOut();
                                    $("#step2-error").html(credi_respnse.msg);
                                    $("#step2-error").show();
                                    sTop();
                                }
                            }
                        });
                    }
                    var args = {
                        sellerId: twocheckout_sellerid,
                        publishableKey: twocheckout_key,
                        ccNo: card_no,
                        cvv: cvc,
                        expMonth: exp_month,
                        expYear: exp_year
                    };
                    TCO.requestToken(successCallback, errorCallback, args);
                } else {
                    $("#step2-error").empty();
                    var newHTML = $.map(alerts, function (value) {
                        return (value);
                    });
                    $("#step2-error").html(newHTML.join(""));
                    $("#step2-error").show();
                }
            } else {
                $("#step2-error").empty();
                donationPreOut();
                $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your 2Checkout Account Information in Plugin Options</div>");
                $("#step2-error").show();
                sTop();
            }
        });
        return false;
    }

    function singlePaypal(dataArray) {
        var amount = dataArray['1'];
        var curre = dataArray['2'];
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();
        var returnUrl = document.URL;
        var dType = dataArray['3'];
        var dPost = dataArray['4'];
        var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_ProcessPaypalOnetime';

        var messages = [lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    donationPreIn();
                },
                success: function (credi_respnse) {
                    jQuery("div#step2-error").empty();
                    jQuery("div#step2-error").html(credi_respnse);
                    jQuery("div#step2-error").show();
                    donationPreOut();
                    PaypalConfirmPopup();
                }
            });
            return false;
        } else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function singlePaymaster(dataArray) {
        var amount = dataArray['1'];
        var curre = dataArray['2'];
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();
        var returnUrl = document.URL;
        var dType = dataArray['3'];
        var dPost = dataArray['4'];
        var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_ProcessPaymaster';

        var messages = [lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    donationPreIn();
                },
                success: function (credi_respnse) {
                    jQuery("div#step2-error").empty();
                    jQuery("div#step2-error").html(credi_respnse);
                    jQuery("div#step2-error").show();
                    donationPreOut();
                    PaymasterConfirmPopup();
                }
            });
            return false;
        } else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function singleYandex(dataArray) {
        var amount = dataArray['1'];
        var curre = dataArray['2'];
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();
        var returnUrl = document.URL;
        var dType = dataArray['3'];
        var dPost = dataArray['4'];
        var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=lifeline2_ProcessYandex';

        var messages = [lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }
        if (alerts.length === 0) {
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    donationPreIn();
                },
                success: function (credi_respnse) {
                    jQuery("div#step2-error").empty();
                    jQuery("div#step2-error").html(credi_respnse);
                    jQuery("div#step2-error").show();
                    donationPreOut();
                    YandexConfirmPopup();
                }
            });
            return false;
        } else {
            $("#step2-error").empty();
            var newHTML = $.map(alerts, function (value) {
                return (value);
            });
            $("#step2-error").html(newHTML.join(""));
            $("#step2-error").show();
        }
    }

    function singlebraintree(dataArray) {
       // TCO.loadPubKey('sandbox', function () {
            if (braintree_key != '') {

                var card_no = $('input#rec_cn').val();
                var exp_month = $('select#rec_cem').val();
                var exp_year = $('select#rec_cey').val();
                var cvc = $('input#rec_ccvc').val();

                var f_name = $('#f_name').val();
                var l_name = $('#l_name').val();
                var email = $('#email').val();
                var contact_no = $('#contact_no').val();
                var address = $('#address').val();

                var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
                var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                var alerts = [];
                var counter = 0;

                $(fields).each(function (index, element) {
                    if (element == '') {
                        alerts.push(messages[counter]);
                    }
                    counter++;
                });
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(email) === false) {
                    alerts.push(lf2.vMail);
                }
                if (alerts.length === 0) {

                    // var errorCallback = function (data) {
                    //     if (data.errorCode === 200) {
                    //         //tokenRequest();
                    //     } else {
                    //         $("#step2-error").empty();
                    //         donationPreOut();
                    //         $("#step2-error").html(data.errorMsg);
                    //         $("#step2-error").show();
                    //         sTop();
                    //         setTimeout(function () {
                    //             $("#step2-error").empty();
                    //         }, 5000);
                    //         return false;
                    //     }
                    // };
                    //var successCallback = function (data) {

                        var amount = dataArray['1'];
                        var curr = dataArray['2'];
                        var donationFor = dataArray['3'];
                        var postId = dataArray['4'];
                        var data = 'donationFor=' + donationFor +
                            //'&token=' + data.response.token.token +
                            '&postId=' + postId +
                            '&card_no=' + card_no +
                            '&exp_month=' + exp_month +
                            '&exp_year=' + exp_year +
                            '&cvc=' + cvc +
                            '&f_name=' + f_name +
                            '&l_name=' + l_name +
                            '&email=' + email +
                            '&contact_no=' + contact_no +
                            '&address=' + address +
                            '&currency=' + curr +
                            '&amout=' + amount +
                            '&action=lifeline2_processbraintree';

                        jQuery.ajax({

                            type: "post",
                            url: adminurl,
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                donationPreIn();
                            },
                            success: function (credi_respnse) {

                                $("#step2-error").empty();
                                donationPreOut();
                                if (credi_respnse.status === true) {
                                    alert(credi_respnse.msg);
                                    window.location.replace(homeUrl);
                                } else if (credi_respnse.status === false) {
                                    $("#step2-error").empty();
                                    donationPreOut();
                                    $("#step2-error").html(credi_respnse.msg);
                                    $("#step2-error").show();
                                    sTop();
                                }
                            }
                        });
                    //}
                    var args = {
                        sellerId: braintree_sellerid,
                        publishableKey: braintree_key,
                        ccNo: card_no,
                        cvv: cvc,
                        expMonth: exp_month,
                        expYear: exp_year
                    };
                    //TCO.requestToken(successCallback, args);
                } else {
                    $("#step2-error").empty();
                    var newHTML = $.map(alerts, function (value) {
                        return (value);
                    });
                    $("#step2-error").html(newHTML.join(""));
                    $("#step2-error").show();
                }
            } else {
                $("#step2-error").empty();
                donationPreOut();
                $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Braintree Account Information in Plugin Options</div>");
                $("#step2-error").show();
                sTop();
            }
       // });
        return false;
    }

    function singlebluepay(dataArray) {
        // TCO.loadPubKey('sandbox', function () {
        if (bluepay_key == '') {

            var card_no = $('input#rec_cn').val();
            var exp_month = $('select#rec_cem').val();
            var exp_year = $('select#rec_cey').val();
            var cvc = $('input#rec_ccvc').val();

            var f_name = $('#f_name').val();
            var l_name = $('#l_name').val();
            var email = $('#email').val();
            var contact_no = $('#contact_no').val();
            var address = $('#address').val();

            var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
            var alerts = [];
            var counter = 0;

            $(fields).each(function (index, element) {
                if (element == '') {
                    alerts.push(messages[counter]);
                }
                counter++;
            });
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(email) === false) {
                alerts.push(lf2.vMail);
            }
            if (alerts.length === 0) {

                // var errorCallback = function (data) {
                //     if (data.errorCode === 200) {
                //         //tokenRequest();
                //     } else {
                //         $("#step2-error").empty();
                //         donationPreOut();
                //         $("#step2-error").html(data.errorMsg);
                //         $("#step2-error").show();
                //         sTop();
                //         setTimeout(function () {
                //             $("#step2-error").empty();
                //         }, 5000);
                //         return false;
                //     }
                // };
                //var successCallback = function (data) {

                var amount = dataArray['1'];
                var curr = dataArray['2'];
                var donationFor = dataArray['3'];
                var postId = dataArray['4'];
                var data = 'donationFor=' + donationFor +
                    //'&token=' + data.response.token.token +
                    '&postId=' + postId +
                    '&card_no=' + card_no +
                    '&exp_month=' + exp_month +
                    '&exp_year=' + exp_year +
                    '&cvc=' + cvc +
                    '&f_name=' + f_name +
                    '&l_name=' + l_name +
                    '&email=' + email +
                    '&contact_no=' + contact_no +
                    '&address=' + address +
                    '&currency=' + curr +
                    '&amout=' + amount +
                    '&action=lifeline2_processbluepay';

                jQuery.ajax({

                    type: "post",
                    url: adminurl,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        donationPreIn();
                    },
                    success: function (credi_respnse) {

                        $("#step2-error").empty();
                        donationPreOut();
                        if (credi_respnse.status === true) {
                            alert(credi_respnse.msg);
                            window.location.replace(homeUrl);
                        } else if (credi_respnse.status === false) {
                            $("#step2-error").empty();
                            donationPreOut();
                            $("#step2-error").html(credi_respnse.msg);
                            $("#step2-error").show();
                            sTop();
                        }
                    }
                });
                //}
                var args = {
                    sellerId: bluepay_sellerid,
                    publishableKey: bluepay_key,
                    ccNo: card_no,
                    cvv: cvc,
                    expMonth: exp_month,
                    expYear: exp_year
                };
                //TCO.requestToken(successCallback, args);
            } else {
                $("#step2-error").empty();
                var newHTML = $.map(alerts, function (value) {
                    return (value);
                });
                $("#step2-error").html(newHTML.join(""));
                $("#step2-error").show();
            }
        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Bluepay Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function singleconekta(dataArray) {
        // TCO.loadPubKey('sandbox', function () {
        if (conekta_key == '') {

            var card_no = $('input#rec_cn').val();
            var exp_month = $('select#rec_cem').val();
            var exp_year = $('select#rec_cey').val();
            var cvc = $('input#rec_ccvc').val();

            var f_name = $('#f_name').val();
            var l_name = $('#l_name').val();
            var email = $('#email').val();
            var contact_no = $('#contact_no').val();
            var address = $('#address').val();

            var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
            var alerts = [];
            var counter = 0;

            $(fields).each(function (index, element) {
                if (element == '') {
                    alerts.push(messages[counter]);
                }
                counter++;
            });
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(email) === false) {
                alerts.push(lf2.vMail);
            }
            if (alerts.length === 0) {

                // var errorCallback = function (data) {
                //     if (data.errorCode === 200) {
                //         //tokenRequest();
                //     } else {
                //         $("#step2-error").empty();
                //         donationPreOut();
                //         $("#step2-error").html(data.errorMsg);
                //         $("#step2-error").show();
                //         sTop();
                //         setTimeout(function () {
                //             $("#step2-error").empty();
                //         }, 5000);
                //         return false;
                //     }
                // };
                //var successCallback = function (data) {

                var amount = dataArray['1'];
                var curr = dataArray['2'];
                var donationFor = dataArray['3'];
                var postId = dataArray['4'];
                var data = 'donationFor=' + donationFor +
                    //'&token=' + data.response.token.token +
                    '&postId=' + postId +
                    '&card_no=' + card_no +
                    '&exp_month=' + exp_month +
                    '&exp_year=' + exp_year +
                    '&cvc=' + cvc +
                    '&f_name=' + f_name +
                    '&l_name=' + l_name +
                    '&email=' + email +
                    '&contact_no=' + contact_no +
                    '&address=' + address +
                    '&currency=' + curr +
                    '&amout=' + amount +
                    '&action=lifeline2_processconekta';

                jQuery.ajax({

                    type: "post",
                    url: adminurl,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        donationPreIn();
                    },
                    success: function (credi_respnse) {

                        $("#step2-error").empty();
                        donationPreOut();
                        if (credi_respnse.status === true) {
                            alert(credi_respnse.msg);
                            window.location.replace(homeUrl);
                        } else if (credi_respnse.status === false) {
                            $("#step2-error").empty();
                            donationPreOut();
                            $("#step2-error").html(credi_respnse.msg);
                            $("#step2-error").show();
                            sTop();
                        }
                    }
                });
                //}
                var args = {
                    sellerId: conekta_sellerid,
                    publishableKey: conekta_key,
                    ccNo: card_no,
                    cvv: cvc,
                    expMonth: exp_month,
                    expYear: exp_year
                };
                //TCO.requestToken(successCallback, args);
            } else {
                $("#step2-error").empty();
                var newHTML = $.map(alerts, function (value) {
                    return (value);
                });
                $("#step2-error").html(newHTML.join(""));
                $("#step2-error").show();
            }
        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Conekta Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function singlepaystack(dataArray) {

        // TCO.loadPubKey('sandbox', function () {
        if (paystack_key == '') {
            var key    = 'pk_test_72ba26f6e5d645b80becfbf4767df9a452b4f098';
            var amount = dataArray['1']+ '' + 0 + '' + 0;

            var email  = $('#email').val();
            var name   = $('#f_name').val();

            var handler = PaystackPop.setup({
                key: key,
                email: email,
                amount: amount,

                metadata: {
                    custom_fields: [
                        {
                            display_name: "Mobile Number",
                            variable_name: "mobile_number",
                            value: ""
                        },
                        {
                            display_name: "Donor Name",
                            variable_name: "donor_name",
                            value: name
                        }
                    ]
                },
                callback: function(response){

                    alert('success. transaction ref is ' + response.reference);
                    //location.href = "";
                    var name   = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();
                    var amount = dataArray['1'];
                    var email  = $('#email').val();
                    var currency  = dataArray['2'];
                    var trans_id     =  response.reference;
                    var token     =   response.reference
                    var data = '&trans_id=' + trans_id + '&token=' + token + '&f_name=' + name + '&l_name=' + l_name + '&contact_no=' + contact_no + '&address=' + address + '&email=' + email + '&amount=' + amount + '&currency=' + currency + '&action=lifeline2_processpaystack';
                    $.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data,
                        cache: false,
                        dataType: "json",
                        beforeSend: function () {
                            jQuery('div.loader-wrapper').fadeIn('slow');
                        },
                        success: function (response) {
                            var session_id = response.session;
                            after_process(session_id);
                        }
                    });
                    jQuery('div.loader-wrapper').fadeOut('slow');
                    return false;
                },
                onClose: function(){
                    alert('window closed');

                }
            });
            handler.openIframe();

// PayStack donation

            //start//

            //end//
        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Paystack Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function singlepayumoney(dataArray) {
        $(location).attr('href', 'https://www.payumoney.com/merchant-account/#/login');
        // TCO.loadPubKey('sandbox', function () {
        if (payumoney_key == '') {
            var key    = 'pk_test_72ba26f6e5d645b80becfbf4767df9a452b4f098';
            var amount = dataArray['1'];

            var email  = $('#email').val();
            var name   = $('#f_name').val();

            var handler = PayumoneyPop.setup({
                key: key,
                email: email,
                amount: amount,

                metadata: {
                    custom_fields: [
                        {
                            display_name: "Mobile Number",
                            variable_name: "mobile_number",
                            value: ""
                        },
                        {
                            display_name: "Donor Name",
                            variable_name: "donor_name",
                            value: name
                        }
                    ]
                },
                callback: function(response){

                    alert('success. transaction ref is ' + response.reference);
                    //location.href = "";
                    var name   = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();
                    var amount = dataArray['1'];
                    var email  = $('#email').val();
                    var currency  = dataArray['2'];
                    var trans_id     =  response.reference;
                    var token     =   response.reference
                    var data = '&trans_id=' + trans_id + '&token=' + token + '&f_name=' + name + '&l_name=' + l_name + '&contact_no=' + contact_no + '&address=' + address + '&email=' + email + '&amount=' + amount + '&currency=' + currency + '&action=lifeline2_processpayumoney';
                    $.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data,
                        cache: false,
                        dataType: "json",
                        beforeSend: function () {
                            jQuery('div.loader-wrapper').fadeIn('slow');
                        },
                        success: function (response) {
                            var session_id = response.session;
                            after_process(session_id);
                        }
                    });
                    jQuery('div.loader-wrapper').fadeOut('slow');
                    return false;
                },
                onClose: function(){
                    alert('window closed');

                }
            });
            handler.openIframe();

        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Payumoney Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function singlequickpay(dataArray) {
        //alert('aaa');
        //$(location).attr('href', 'https://payment.quickpay.net');
        // TCO.loadPubKey('sandbox', function () {
        if (quickpay_key == '') {
            var key    = '13f3f8c109de8aed7c6c9829fe521a427adcc5093bd4383a2feb20e615a8fbdb';
            var amount = dataArray['1'];

            var email  = $('#email').val();
            var name   = $('#f_name').val();

                    var name   = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();
                    var amount = dataArray['1'];
                    var email  = $('#email').val();
                    var currency  = dataArray['2'];
                    //var trans_id     =  response.reference;
                    //var token     =   response.reference
                    var data = '&f_name=' + name + '&l_name=' + l_name + '&contact_no=' + contact_no + '&address=' + address + '&email=' + email + '&amount=' + amount + '&currency=' + currency + '&action=lifeline2_processquickpay';
                    $.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data,
                        cache: false,
                        dataType: "json",
                        beforeSend: function () {
                            jQuery('div.loader-wrapper').fadeIn('slow');
                        },
                        success: function (response) {
                            //alert(response.msg);
                            //var session_id = response.session;
                            donationPreOut();
                            if (response.status === true) {
                                alert(response.msg);
                                window.location.replace(homeUrl);
                            }
                        }
                    });

        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Quickpay Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function singlecardcom(dataArray) {
        // TCO.loadPubKey('sandbox', function () {
        if (cardcom_key != '') {

            var card_no = $('input#rec_cn').val();
            var exp_month = $('select#rec_cem').val();
            var exp_year = $('select#rec_cey').val();
            var cvc = $('input#rec_ccvc').val();

            var f_name = $('#f_name').val();
            var l_name = $('#l_name').val();
            var email = $('#email').val();
            var contact_no = $('#contact_no').val();
            var address = $('#address').val();

            var messages = [lf2.cardNo, lf2.cardExm, lf2.cardExy, lf2.cardCvc, lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
            var alerts = [];
            var counter = 0;

            $(fields).each(function (index, element) {
                if (element == '') {
                    alerts.push(messages[counter]);
                }
                counter++;
            });
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(email) === false) {
                alerts.push(lf2.vMail);
            }
            if (alerts.length === 0) {

                // var errorCallback = function (data) {
                //     if (data.errorCode === 200) {
                //         //tokenRequest();
                //     } else {
                //         $("#step2-error").empty();
                //         donationPreOut();
                //         $("#step2-error").html(data.errorMsg);
                //         $("#step2-error").show();
                //         sTop();
                //         setTimeout(function () {
                //             $("#step2-error").empty();
                //         }, 5000);
                //         return false;
                //     }
                // };
                //var successCallback = function (data) {

                var amount = dataArray['1'];
                var curr = dataArray['2'];
                var donationFor = dataArray['3'];
                var postId = dataArray['4'];
                var data = 'donationFor=' + donationFor +
                    //'&token=' + data.response.token.token +
                    '&postId=' + postId +
                    '&card_no=' + card_no +
                    '&exp_month=' + exp_month +
                    '&exp_year=' + exp_year +
                    '&cvc=' + cvc +
                    '&f_name=' + f_name +
                    '&l_name=' + l_name +
                    '&email=' + email +
                    '&contact_no=' + contact_no +
                    '&address=' + address +
                    '&currency=' + curr +
                    '&amout=' + amount +
                    '&action=lifeline2_processcardcom';

                jQuery.ajax({

                    type: "post",
                    url: adminurl,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        donationPreIn();
                    },
                    success: function (credi_respnse) {

                        $("#step2-error").empty();
                        donationPreOut();
                        if (credi_respnse.status === true) {
                            alert(credi_respnse.msg);
                            window.location.replace(homeUrl);
                        } else if (credi_respnse.status === false) {
                            $("#step2-error").empty();
                            donationPreOut();
                            $("#step2-error").html(credi_respnse.msg);
                            $("#step2-error").show();
                            sTop();
                        }
                    }
                });
                //}
                var args = {
                    sellerId: cardcom_sellerid,
                    publishableKey: cardcom_key,
                    ccNo: card_no,
                    cvv: cvc,
                    expMonth: exp_month,
                    expYear: exp_year
                };
                //TCO.requestToken(successCallback, args);
            } else {
                $("#step2-error").empty();
                var newHTML = $.map(alerts, function (value) {
                    return (value);
                });
                $("#step2-error").html(newHTML.join(""));
                $("#step2-error").show();
            }
        } else {
            $("#step2-error").empty();
            donationPreOut();
            $("#step2-error").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Cardcom Account Information in Plugin Options</div>");
            $("#step2-error").show();
            sTop();
        }
        // });
        return false;
    }

    function bnakTransfer(dataArray) {
        var f_name = $('#f_name').val();
        var l_name = $('#l_name').val();
        var email = $('#email').val();
        var contact_no = $('#contact_no').val();
        var address = $('#address').val();

        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var messages = [lf2.fName, lf2.lName, lf2.mail, lf2.pNub, lf2.address];
        var fields = [f_name, l_name, email, contact_no, address]
        var alerts = [];
        var counter = 0;

        $(fields).each(function (index, element) {
            if (element == '') {
                alerts.push(messages[counter]);
            }
            counter++;
        });

        if (filter.test(email) === false) {
            alerts.push(lf2.vMail);
        }

        if (alerts.length === 0) {
            var amount = dataArray['1'];
            var currency = dataArray['2'];
            var dType = dataArray['3'];
            var dPost = dataArray['4'];
            var data = 'donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&amout=' + amount + '&action=lifeline2_bankTransfer';
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    donationPreIn();
                },
                success: function (credi_respnse) {
                    if (credi_respnse.status === true) {
                        $("div#bank_transcation").empty();
                        $('div.overlay').fadeIn(500);
                        $("div#bank_transcation").html(credi_respnse.msg);
                        $('div.donation-modal-preloader').fadeOut('slow');
                        $("div#bank_transcation").show();
                    } else if (credi_respnse.status === false) {
                        $("div#step2-error").empty();
                        $("div#step2-error").html(credi_respnse.msg);
                        $("div#step2-error").show();
                        donationPreOut();
                        sTop();
                    }
                }
            });
            return false;
        } else {
            donationPreOut();
            jQuery("div#step2-error").empty();
            var newHTML = jQuery.map(alerts, function (value) {
                return (value);
            });
            jQuery("div#step2-error").html(newHTML.join(""));
            jQuery("div#step2-error").show();
            setTimeout(function () {
                jQuery("div#step2-error").fadeOut('slow').empty();
            }, 15000);
        }
    }

    function PaypalConfirmPopup() {
        donationPreOut();
        var get_height = jQuery('div.confirm_popup').height();
        var height = get_height / 2;
        jQuery('div.confirm_popup').css({
            'margin-top': -height
        });
    }
    // Accordion //
            $('#toggle2 .content').hide();
            $('#toggle2 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle2 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle2 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
             // Accordion //
            $('#toggle3 .content').hide();
            $('#toggle3 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle3 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle3 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
             // Accordion //
            $('#toggle4 .content').hide();
            $('#toggle4 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle4 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle4 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
             // Accordion //
            $('#toggle5 .content').hide();
            $('#toggle5 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle5 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle5 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
            // Accordion //
            $('#toggle6 .content').hide();
            $('#toggle6 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle6 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle6 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
            // Accordion //
            $('#toggle7 .content').hide();
            $('#toggle7 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle7 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle7 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
            // Accordion //
            $('#toggle8 .content').hide();
            $('#toggle8 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle8 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle8 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
            // Accordion //
            $('#toggle9 .content').hide();
            $('#toggle9 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
            $('#toggle9 h3').on("click", function () {
                if ($(this).next().is(':hidden')) {
                    $('#toggle9 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                    $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                    return false;
                }
            });
//===== Scroll Function =====//
    $(function() {
        $('.woocommerce-review-link').on('click',function() {
            $('.tabs li.reviews_tab a').trigger('click');
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }
            }
            return false;
        });
    });
})
;
// end donation modal settings