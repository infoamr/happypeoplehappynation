jQuery(document).ready(function ($) {
    "use strict";
    $('div.cal-action ul li.delete a').on('click', function () {
	if (confirm('Are you sure ?')) {
	    var $this = $(this);
	    var id = $(this).data('id');
	    var data = 'id=' + id + '&action=lifelin2_volunteerDelRec';
	    jQuery.ajax({
		type: "post",
		url: ajaxurl,
		data: data,
		dataType: 'json',
		beforeSend: function () {
		    $('div.loader').fadeIn('slow');
		},
		success: function (res) {
		    $('div.loader').fadeOut('slow');
		    if (res.status === true) {
			$($this).parents('td').addClass('temp-remove');
			$($this).parents('tr').css({'background': '#CF3A35'});
			$('td.temp-remove').css({'color': '#fff'});
			$($this).parents('tr').slideUp(1000);
			setTimeout(function () {
			    $($this).parents('tr').remove();
			}, 1500);
		    } else if (res.status === false) {
			alert(res.msg);
		    }
		}
	    });
	}
	return false;
    });


    $('div.cal-action ul li.approved a').on('click', function () {
	var $this = $(this);
	var id = $(this).data('id');
	var data = 'id=' + id + '&action=lifelin2_volunteerAppRec';
	jQuery.ajax({
	    type: "post",
	    url: ajaxurl,
	    data: data,
	    dataType: 'json',
	    beforeSend: function () {
		$('div.loader').fadeIn('slow');
	    },
	    success: function (res) {
		$('div.loader').fadeOut('slow');
		if (res.status === true) {
		    $($this).parents('tr').children('td').eq(4).html(res.msg);
		    $($this).parent('li').remove('li.approved');
		} else if (res.status === false) {
		    alert(res.msg);
		}
	    }
	});
	return false;
    });

    $('div.cal-action ul li.view a').on('click', function () {
	var id = $(this).data('id');
	var data = 'id=' + id + '&action=lifelin2_volunteerViewRec';
	jQuery.ajax({
	    type: "post",
	    url: ajaxurl,
	    data: data,
	    dataType: 'json',
	    beforeSend: function () {
		$('div.loader').fadeIn('slow');
	    },
	    success: function (res) {
		$('div.loader').fadeOut('slow');
		$('div#volunteer-popup').empty();
		if (res.status === true) {
		    $('div#volunteer-popup').html(res.msg);
		    $('div#volunteer-popup').show();
		    $('div.volunter-popup').show();
		} else if (res.status === false) {
		    alert(res.msg);
		}
	    }
	});
	return false;
    });
    $('span.close').live('click', function () {
	$('div.volunter-popup').fadeOut('slow');
	setTimeout(function () {
	    $('div#volunteer-popup').empty();
	}, 1000);

    });
});