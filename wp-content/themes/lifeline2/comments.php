<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Idealise
 * @since Twenty Idealise 1.0
 */
/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) return;

$count = wp_count_comments( get_the_ID() );
?>
<div id="comments">
    <?php if ( lifeline2_set( $count, 'total_comments' ) > 0 ): ?>
        <div class="comments-thread">

            <?php if ( have_comments() ) : ?>
                <div class="heading">
                    <h3><?php echo esc_html( lifeline2_set( $count, 'total_comments' ) ); ?> <span><?php esc_html_e( 'Comments Found', 'lifeline2' ); ?></span></h3></div>
                <ul itemscope itemtype="http://schema.org/UserComments">
                    <?php
                    wp_list_comments( 'type=comment&callback=lifeline2_Common::lifeline2_comments_list&format=null' );
                    ?>
                </ul>

                <?php
                // Are there comments to navigate through?
                if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
                    ?>
                    <nav class="navigation comment-navigation" role="navigation">
                        <h1 class="screen-reader-text section-heading"><?php esc_html_e( 'Comment navigation', 'lifeline2' ); ?></h1>
                        <div class="nav-previous"><?php previous_comments_link( '&larr; '.esc_html__( 'Older Comments', 'lifeline2' ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'lifeline2' ).' &rarr;' ); ?></div>
                    </nav><!-- .comment-navigation -->
                <?php endif; // Check for comment navigation  ?>
                <?php if ( !comments_open() && get_comments_number() ) : ?>
                    <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'lifeline2' ); ?></p>
                <?php endif; ?>
            <?php endif; // have_comments() ?>
        </div>
        <?php
    endif;
    if ( comments_open() ):
        lifeline2_Common::lifeline2_comment_form();
    endif;
    ?>

</div>

