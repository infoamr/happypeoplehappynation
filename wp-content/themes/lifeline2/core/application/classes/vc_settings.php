<?php
if ( function_exists( 'vc_map' ) ) {

    function lifeline2_custom_css_classes_for_vc_row_and_vc_column( $class_string, $tag ) {
        
        if ( $tag == 'vc_row' || $tag == 'vc_row_inner' ) {
            $class_string = str_replace( 'vc_row-fluid', 'my_row-fluid', $class_string ); // This will replace "vc_row-fluid" with "my_row-fluid"
        }
        if ( $tag == 'vc_column' || $tag == 'vc_column_inner' ) {
            $class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', 'col-md-$1', $class_string ); // This will replace "vc_col-sm-%" with "my_col-sm-%"
        }
        return $class_string;

    }
    add_filter( 'vc_shortcodes_css_class', 'lifeline2_custom_css_classes_for_vc_row_and_vc_column', 10, 2 );

    function vc_theme_vc_row( $atts, $content = null ) {
        extract( shortcode_atts(
                array(
            'el_class'          => '',
            'bg_image'          => '',
            'bg_color'          => '',
            'bg_image_repeat'   => '',
            'font_color'        => '',
            'padding'           => '',
            'margin_bottom'     => '',
            'container'         => '',
            'css'               => '',
            //my custom params
            'my_container'      => 'true',
            'show_title'        => '',
            'col_title'         => '',
            'col_sub_title'     => '',
            'title_description' => '',
            'title_style'       => '',
            'show_parallax'     => '',
            'show_parallax_bg'  => '',
            'parallax_clr'      => '',
            'parallax_type'     => '',
            'parallax_bg'       => '',
            'parallax_video_bg' => '',
            'miscellaneous'     => '',
            'gap' => '',
            'padding'           => '',
            'remove_bottom'     => '',
            'gaps'              => '',
            'gray_section'      => '',
            'overlap_section'   => '',
                ), $atts )
        );

        $atts['base'] = '';
        wp_enqueue_style( 'js_composer_front' );
        wp_enqueue_script( 'wpb_composer_front_js' );
        wp_enqueue_style( 'js_composer_custom_css' );
        $vc_row       = new WPBakeryShortCode_VC_Row( $atts );
        $el_class     = $vc_row->getExtraClass( $el_class );
        $css_class    = $el_class;
        if ( $css ) $css_class .= vc_shortcode_custom_css_class( $css, ' ' ) . ' ';
        $style        = $vc_row->buildStyle( $bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom, $gap );

        $my_class = $el_class;
        $my_gap = ($gap) ? ' vc_row vc_column-gap-'.$gap : '';

        if ( $miscellaneous != 'false' && $padding != 'false' ) {
            $my_class .= $padding . ' ';
        }
        if ( $miscellaneous != 'false' && $gaps != 'false' ) {
            $my_class .= $gaps . ' ';
        }
        if ( $miscellaneous != 'false' && $remove_bottom != 'false' ) {
            $my_class .= $remove_bottom . ' ';
        }
        if ( $miscellaneous != 'false' && $gray_section != 'false' ) {
            $my_class .= $gray_section;
        }
        if ( $miscellaneous != 'false' && $overlap_section == 'true' ) {
            $my_class .= ' overlap';
        }
        $c = '';
        if ( $my_container == 'true' ) {
            $c .= 'container';
        }
        $my_parallax = '';
        if ( $show_parallax == 'true' && !empty( $parallax_bg ) ) {

            if ( $parallax_bg ):
                $img = wp_get_attachment_image_src( $parallax_bg, 'full' );

            else:
                $img = array( '0' => '' );
            endif;
            $my_class .= 'parallax-sec';
            if ( $parallax_type != '' && $parallax_type == 'fixed-bg' ) {
                $my_parallax .= '<div class="fixed-bg" style="background:url(' . $img[0] . ') repeat scroll 0 0 rgba(0, 0, 0, 0);"></div>';

            } else {
                $my_parallax .= '<div class="parallax" style="background:url(' . $img[0] . ') repeat scroll 0 0 rgba(0, 0, 0, 0);" data-velocity="-.1"></div>';
            }
        }

        $my_video_parallax = '';
        if ( $show_parallax == 'true' && !empty( $parallax_video_bg ) ) {

            if ( $parallax_video_bg ):
                $vid =  $parallax_video_bg ;
                //printr($vid);
                $my_video_parallax .= '<div class="fixed-bg"><iframe frameborder="0" height="100%" width="100%" 
    src="' .$vid. '?autoplay=1&controls=0&showinfo=0&autohide=1">
  </iframe></div>';
                //printr($my_video_parallax);

            endif;
            $my_class .= 'parallax-sec';

        }

        $parallax_layer = '';
        if ( $show_parallax == 'true' && $parallax_clr != 'no-layer' ) {
            $my_class .= ' ' . $parallax_clr;
        }

        $titles = '';
        if ( $show_title != 'false' && !empty( $col_title ) && $title_style == 'centered_description' ):
            $t_st = ($title_style == 'side') ? ' style2' : '';
            $titles .= '<div class="title">';
            $titles .= ($col_sub_title) ? '<span>' . $col_sub_title . '</span>' : '';
            $titles .= '<h2>' . $col_title . '</h2>';
            $titles .= '<p>' . $title_description . '</p>';
            $titles .= '</div>';
        elseif ( $show_title != 'false' && !empty( $col_title ) ):
            $t_st = ($title_style == 'side') ? ' style2' : '';
            $titles .= '<div class="title' . $t_st . '">';
            //$titles .= '<h2>' . $col_title . '</h2>';
            $title_concrate = explode(' ', $col_title, 2);
            $titles .= ($col_title) ? '<h2>' . lifeline2_set($title_concrate, '0') . ' <span>' . lifeline2_set($title_concrate, '1') . '</span></h2>' : '';
            $titles .= ($col_sub_title) ? '<span>' . $col_sub_title . '</span>' : '';
            $titles .= '</div>';
        endif;

        $output = '';
        $output .= '<section>' . "\n"
            . '<div class="block ' . $my_class . '"> ' . "\n";
        $output .= $my_parallax . "\n";
        $output .= $my_video_parallax . "\n";
        $output .= '<div class="' . $c . '">' . "\n";
        $output .= '<div class="row ' .$my_gap. '">' . "\n";
        $output .= $titles . "\n";
        $output .= wpb_js_remove_wpautop( $content ) . "\n";
        $output .= '</div>' . "\n";
        $output .= '</div>' . "\n";
        $output .= '</div>' . "\n";
        $output .= '</section>' . "\n";
        return $output;
    }

    function vc_theme_vc_row_inner( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'el_class'  => '',
            'container' => '',
            'row'       => '',
                ), $atts ) );
        $atts['base'] = '';
        wp_enqueue_style( 'js_composer_front' );
        wp_enqueue_script( 'wpb_composer_front_js' );
        wp_enqueue_style( 'js_composer_custom_css' );

        $output    = '';
        $css_class = $el_class;
        if ( $container ) return
                '<section>
                <div class="container">
                    ' . wpb_js_remove_wpautop( $content ) . '
                </div>
            </section>' . "\n";

        return
            '<section class="' . $css_class . ' row" >
                    ' . wpb_js_remove_wpautop( $content ) . '
            </section>' . "\n";
    }

    function vc_theme_vc_column_inner( $atts, $content = null ) {
        extract( shortcode_atts( array( 'width' => '1/1', 'el_class' => '' ), $atts ) );

        $width_col = wpb_translateColumnWidthToSpan( $width );
        $width     = str_replace( 'vc_col-sm-', 'col-md-', $width_col . ' column' );
        $el_class  = ($el_class) ? ' ' . $el_class : '';
        return
            '<div class="wpb_column ' . $width . $el_class . '">
                    ' . do_shortcode( $content ) . '
            </div>' . "\n";
    }

    function vc_theme_vc_column( $atts, $content = null ) {
        extract( shortcode_atts(
                array(
            'width'               => '1/1',
            'el_class'            => '',
            'show_title'          => '',
            'col_title'           => '',
            'col_sub_title'       => '',
            'title_desc'          => '',
            'title_style'         => '',
            'overlap_col_section' => '',
                ), $atts )
        );

        $titles = '';
        if ( $show_title != 'false' && !empty( $col_title ) && $title_style == 'centered_description' ):
            $t_st = ($title_style == 'side') ? ' style2' : '';
            $titles .= '<div class="title">';
            $titles .= ($col_sub_title) ? '<span>' . $col_sub_title . '</span>' : '';
            $titles .= '<h2>' . $col_title . '</h2>';
            $titles .= '<p>' . $title_description . '</p>';
            $titles .= '</div>';
        elseif ( $show_title != 'false' && !empty( $col_title ) ):
            $t_st = ($title_style == 'side') ? ' style2' : '';
            $titles .= '<div class="title' . $t_st . '">';
            $titles .= '<h2>' . $col_title . '</h2>';
            $titles .= ($col_sub_title) ? '<span>' . $col_sub_title . '</span>' : '';
            $titles .= '</div>';
        endif;

        $width = wpb_translateColumnWidthToSpan( $width );
        $width = str_replace( 'vc_col-sm-', 'col-md-', $width . ' column' );

        $el_class = ( $el_class ) ? ' ' . $el_class : '';
        $el_class .= ( $overlap_col_section == 'true' ) ? ' overlap' : '';
        $output   = '<div class="' . ' ' . $width . ' ' . $el_class . '">';
        $output .= $titles;
        $output .= do_shortcode( $content );
        $output .= '</div>';
        return $output;
    }
    $container = array(
        "type"        => "dropdown",
        "class"       => "",
        "heading"     => esc_html__( "Container", 'lifeline2' ),
        "param_name"  => "my_container",
        "value"       => array( esc_html__( 'True', 'lifeline2' ) => 'true', esc_html__( 'False', 'lifeline2' ) => 'false' ),
        "description" => esc_html__( "Show container for this section.", 'lifeline2' ),
    );

    // start vc row and column customized
    $miscellaneous   = array(
        "type"        => "dropdown",
        "class"       => "",
        "group"       => esc_html__( "Miscellaneous", 'lifeline2' ),
        "heading"     => esc_html__( "Miscellaneous Settings", 'lifeline2' ),
        "param_name"  => "miscellaneous",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'true' ),
        "description" => esc_html__( "Show miscellaneous settings for this section.", 'lifeline2' ),
    );
    $overlap_section = array(
        "type"        => "checkbox",
        "class"       => "",
        "group"       => esc_html__( "Miscellaneous", 'lifeline2' ),
        "heading"     => esc_html__( 'Overlap Section', 'lifeline2' ),
        "param_name"  => "overlap_section",
        "value"       => array( 'Enable Overlap' => 'true' ),
        "description" => esc_html__( 'Enable to overlap this section to upward', 'lifeline2' ),
        'dependency'  => array(
            'element' => 'miscellaneous',
            'value'   => array( 'true' )
        ),
    );
    $remove_gap      = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Miscellaneous', 'lifeline2' ),
        "heading"     => esc_html__( "Remove Gape", 'lifeline2' ),
        "param_name"  => "gaps",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'remove-gap' ),
        "description" => esc_html__( "Remove the Gap from top of the Section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'miscellaneous',
            'value'   => array( 'true' )
        ),
    );

    $padding = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Miscellaneous', 'lifeline2' ),
        "heading"     => esc_html__( "No Padding", 'lifeline2' ),
        "param_name"  => "padding",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'no-padding' ),
        "description" => esc_html__( "Remove the Padding from top and bottom of the Section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'miscellaneous',
            'value'   => array( 'true' )
        ),
    );

    $remove_bottom = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Miscellaneous', 'lifeline2' ),
        "heading"     => esc_html__( "Remove Bottom", 'lifeline2' ),
        "param_name"  => "remove_bottom",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'remove-bottom' ),
        "description" => esc_html__( "Remove the Padding from bottom of the Section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'miscellaneous',
            'value'   => array( 'true' )
        ),
    );

    $gray_section = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Miscellaneous', 'lifeline2' ),
        "heading"     => esc_html__( "Gray Section", 'lifeline2' ),
        "param_name"  => "gray_section",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'Light Gray', 'lifeline2' ) => 'gray', esc_html__( 'Dark Gray', 'lifeline2' ) => 'dark-gray' ),
        "description" => esc_html__( "Make this section Gray then true.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'miscellaneous',
            'value'   => array( 'true' )
        ),
    );

    // start parallax section
    $show_parallax = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Show Parallax", 'lifeline2' ),
        "param_name"  => "show_parallax",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'true' ),
        "description" => esc_html__( "Make this section parallax then true.", 'lifeline2' )
    );

    $show_parallax_bg = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Show Parallax BG", 'lifeline2' ),
        "param_name"  => "show_parallax_bg",
        "value"       => array( esc_html__( 'Image', 'lifeline2' ) => 'image', esc_html__( 'Video', 'lifeline2' ) => 'video' ),
        "description" => esc_html__( "Make this section parallax then true.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_parallax',
            'value'   => array( 'true' )
        ),
    );

    $parallax_clr = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Parallax Layout Scheme", 'lifeline2' ),
        "param_name"  => "parallax_clr",
        "value"       => array(
            esc_html__( 'No Layer', 'lifeline2' ) => 'no-layer',
            esc_html__( 'Black', 'lifeline2' )    => 'blackish',
            esc_html__( 'White', 'lifeline2' )    => 'whitish',
            esc_html__( 'Gray', 'lifeline2' )     => 'grayish',
        ),
        "description" => esc_html__( "Chose Style for Parallax.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_parallax',
            'value'   => array( 'true' )
        ),
    );

    $parallax_type = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Parallax Type", 'lifeline2' ),
        "param_name"  => "parallax_type",
        "value"       => array(
            esc_html__( 'Please Select', 'lifeline2' )       => '',
            esc_html__( 'Fixed', 'lifeline2' )  => 'fixed-bg',
            esc_html__( 'Scroll', 'lifeline2' ) => 'parallax',
        ),
        "description" => esc_html__( "Choose type for parallax.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_parallax',
            'value'   => array( 'true' )
        ),
    );

    $parallax_img = array(
        "type"        => "attach_image",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Parallax Background", 'lifeline2' ),
        "param_name"  => "parallax_bg",
        "description" => esc_html__( "Make this section as parallax.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_parallax_bg',
            'value'   => array( 'image' )
        ),
    );
    $parallax_video = array(
        "type"        => "textfield",
        "class"       => "",
        'group'       => esc_html__( 'Parallax', 'lifeline2' ),
        "heading"     => esc_html__( "Video Background", 'lifeline2' ),
        "param_name"  => "parallax_video_bg",
        "description" => esc_html__( "Put here YouTube Embed url.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_parallax_bg',
            'value'   => array( 'video' )
        ),
    );

    //start title settings
    $show_heading = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Show Title", 'lifeline2' ),
        "param_name"  => "show_title",
        "value"       => array( esc_html__( 'False', 'lifeline2' ) => 'false', esc_html__( 'True', 'lifeline2' ) => 'true' ),
        "description" => esc_html__( "Make this section with title then true.", 'lifeline2' )
    );
    $title_style  = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Title Style", 'lifeline2' ),
        "param_name"  => "title_style",
        "value"       => array(
            esc_html__( 'Centered Title', 'lifeline2' )                  => 'centered',
            esc_html__( 'Centered Title With Description', 'lifeline2' ) => 'centered_description',
            esc_html__( 'Side Title', 'lifeline2' )                      => 'side',
        ),
        "description" => esc_html__( "Chose Style for Parallax.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_title',
            'value'   => array( 'true' )
        ),
    );
    $title        = array(
        "type"        => "textfield",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Title", 'lifeline2' ),
        "param_name"  => "col_title",
        "description" => esc_html__( "Enter the title of this section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_title',
            'value'   => array( 'true' )
        ),
    );

    $subtitle = array(
        "type"        => "textfield",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Sub Title", 'lifeline2' ),
        "param_name"  => "col_sub_title",
        "description" => esc_html__( "Enter the sub title of this section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_title',
            'value'   => array( 'true' )
        ),
    );

    $title_description = array(
        "type"        => "textfield",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Title Description", 'lifeline2' ),
        "param_name"  => "title_description",
        "description" => esc_html__( "Enter the title description of this section.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'title_style',
            'value'   => array( 'centered_description' )
        ),
    );

    $title_style = array(
        "type"        => "dropdown",
        "class"       => "",
        'group'       => esc_html__( 'Title Setting', 'lifeline2' ),
        "heading"     => esc_html__( "Title Style", 'lifeline2' ),
        "param_name"  => "title_style",
        "value"       => array(
            esc_html__( 'Centered Title', 'lifeline2' )                  => 'centered',
            esc_html__( 'Centered Title With Description', 'lifeline2' ) => 'centered_description',
            esc_html__( 'Side Title', 'lifeline2' )                      => 'side',
        ),
        "description" => esc_html__( "Chose Style for Parallax.", 'lifeline2' ),
        'dependency'  => array(
            'element' => 'show_title',
            'value'   => array( 'true' )
        ),
    );

    $overlap_col_section = array(
        "type"        => "checkbox",
        "class"       => "",
        "heading"     => esc_html__( 'Overlap Section', 'lifeline2' ),
        "param_name"  => "overlap_col_section",
        "value"       => array( 'Enable Overlap' => 'true' ),
        "description" => esc_html__( 'Enable to overlap this section to upward', 'lifeline2' ),
    );

    //vc column settings
    vc_add_param( 'vc_column', $show_heading );
    vc_add_param( 'vc_column', $title );
    vc_add_param( 'vc_column', $subtitle );
    vc_add_param( 'vc_column', $title_description );
    vc_add_param( 'vc_column', $title_style );
    vc_add_param( 'vc_column', $overlap_col_section );

    //vc row settings
    vc_add_param( 'vc_row', $container );
    vc_add_param( 'vc_row', $show_heading );
    vc_add_param( 'vc_row', $title );
    vc_add_param( 'vc_row', $subtitle );
    vc_add_param( 'vc_row', $title_description );
    vc_add_param( 'vc_row', $title_style );
    vc_add_param( 'vc_row', $miscellaneous );
    vc_add_param( 'vc_row', $remove_gap );
    vc_add_param( 'vc_row', $padding );
    vc_add_param( 'vc_row', $remove_bottom );
    vc_add_param( 'vc_row', $gray_section );
    vc_add_param( 'vc_row', $overlap_section );
    vc_add_param( 'vc_row', $show_parallax );
    vc_add_param( 'vc_row', $show_parallax_bg );
    vc_add_param( 'vc_row', $parallax_clr );
    vc_add_param( 'vc_row', $parallax_type );
    vc_add_param( 'vc_row', $parallax_img );
    vc_add_param( 'vc_row', $parallax_video );

   vc_remove_param( "vc_row", "full_width" );
    vc_remove_param( "vc_row", "parallax_image" );
    vc_remove_param( "vc_row", "el_id" );
    vc_remove_param( "vc_row", "full_height" );
    vc_remove_param( "vc_row", "content_placement" );
    vc_remove_param( "vc_row", "video_bg" );
    vc_remove_param( "vc_row", "video_bg_url" );
    vc_remove_param( "vc_row", "video_bg_parallax" );
    vc_remove_param( "vc_row", "parallax" );
    vc_remove_param( "vc_row", "columns_placement" );
    vc_remove_param( "vc_row", "equal_height" );
    vc_remove_param( "vc_row", "parallax_speed_bg" );
    vc_remove_param( "vc_row", "parallax_speed_video" );
    vc_remove_param( "vc_row", "css" );

    vc_remove_param( "vc_column", "css" );
    vc_remove_param( "vc_column", "width" );
    vc_remove_param( "vc_column", "offset" );
}
