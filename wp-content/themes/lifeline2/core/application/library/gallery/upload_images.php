<?php
wp_nonce_field( 'lif_gallery_content_nonce', 'lif_gallery_content_nonce' );
?>
<div id="gal-ctrls">
    <div class="left_ctrl">
        <a id="lifeline2_gal" href="javascript:void(0)" class="lifeline2_gal button left" data-editor="content" title="Add Media">
            <span class="wp-media-buttons-icon"></span>
            <?php esc_html_e( 'Add / Edit Media', 'lifeline2' ); ?>
        </a> 
        <span class="spinner" id="gallery_spinner"></span>
    </div>
    <div class="clear"></div>
</div>
<div class="upload_gallery">
    <ul id="image-gallery" class="align_left">
        <?php
        global $post;
        $ids = (get_post_meta( $post->ID, 'lifeline2_gal_ids', true )) ? get_post_meta( $post->ID, 'lifeline2_gal_ids', true ) : '';
        ?>
        <input type="hidden" name="lifeline2_gal_array" value="<?php echo esc_attr( $ids ) ?>" />
        <?php
        if ( $ids != '' ) {
            $explode = explode( ',', $ids );
            foreach ( $explode as $_id ) {
                $src   = wp_get_attachment_image_src( $_id, 'thumbnail' );
                $title = get_the_title( $_id );
                ?>
                <li>
                    <span data-id="<?php echo esc_attr( $_id ) ?>" style="display:none;"></span>
                    <img src="<?php echo esc_url( lifeline2_set( $src, '0' ) ) ?>" alt="<?php echo esc_attr( $title ) ?>" />
                    <div class="upload_gal_control">
                        <i class="featured_upload_img"></i>
                        <i class="delete_upload_img"></i>
                    </div>
                    <i class="move_upload_img"></i>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>
<div class="clear"></div>
