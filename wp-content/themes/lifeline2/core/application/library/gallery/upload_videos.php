<?php
wp_nonce_field('lif_gallery_content_nonce', 'lif_gallery_content_nonce');
global $post;
$jsOutput = 'var lifeline2_videos = ' . json_encode(get_post_meta($post->ID, 'lifeline2_videos', true));
wp_add_inline_script('lifeline2_' . 'gal_videos', $jsOutput);
?>
<p>
<div id="video-ctrls">
    <div class="left_ctrl">
        <input type="button" id="add-videos" href="javascript:void(0)" class="button left" value="<?php esc_html_e('Show Video Graber', 'lifeline2'); ?>">
    </div>
    <div class="right_ctrl" id="vid-filter">
    </div>
    <div class="clear">
    </div>
</div>

<div id="vid-pagination"></div>
<div class="upload_gallery" id="video-gallery">
    <div id="video-uploader">
        <p>
            <label for="smashing-post-class">
                <?php esc_html_e('Enter the URL of VIMEO / DailyMotion / YouTube videos to upload, you can upload multiple videos at once with comma separated with new line', 'lifeline2'); ?>
            </label>
        </p>
        <div class="ui-progressbar progressbar">
            <div class="ui-progressbar-value"></div>
        </div>
        <div class="gallery_upload_list">
            <textarea id="video_manager" rows="5" cols="90" name="video_manager"></textarea>
            <button id="process-videos" class="button button-primary">
                <?php esc_html_e('Process Videos', 'lifeline2'); ?>
            </button>
        </div>
        <div class="gallery_upload_log">
            <ul id="logs">
                <li>
                    <h1><i>
                            <?php esc_html_e('Live Videos Log', 'lifeline2'); ?>
                        </i></h1>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <ul id="thumbnail">
    </ul>
</div>
<div class="clear"></div>
<div id="play-video"></div>
</p>
