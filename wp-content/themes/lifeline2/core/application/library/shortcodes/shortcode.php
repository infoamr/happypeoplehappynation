<?php

if ( !defined( "lifeline2_DIR" ) )
	die( '!!!' );

abstract class lifeline2_VC_ShortCode {

	public static function _options( $method ) {
		$called_class = get_called_class();
		return $called_class::$method( 'lifeline2_Shortcodes_Map' );
	}

}
