<?php

$method = get_class_methods( get_class() );
$func = lifeline2_set( $method, '0' );
$get_array = self::$func( 'lifeline2_Shortcodes_Map' );
$get_params = lifeline2_set( $get_array, 'params' );
$create_array = array();
$temp = '';
include lifeline2_ROOT . 'core/application/library/shortcodes_default_attr.php';
$get_params = array_merge( $get_params, $shortcode_section );
foreach ( $get_params as $param ) {
	if ( array_key_exists( 'value', $param ) ) {
		if ( lifeline2_set( $param, 'type' ) == 'checkbox' ) {
			$param['value'] = '';
		}
		$temp_val = lifeline2_set( $param, 'value' );
		if ( is_array( $temp_val ) ) {
			$temp = array_shift( $temp_val );
		} else {
			$temp = $temp_val;
		}
	}
	$create_array[lifeline2_set( $param, 'param_name' )] = $temp;
}
$values = wp_parse_args( $atts, $create_array );
extract( $values );



