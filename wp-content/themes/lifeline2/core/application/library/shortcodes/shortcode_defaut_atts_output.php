<?php

if ( $section == 'true' ) {

	$my_class = '';
	if ( $section_padding == 'no-padding' ) {
		$my_class .= $section_padding . ' ';
	}
	if ( $gap == 'true' ) {
		$my_class .= $gap . ' ';
	}
	if ( $remove_bottom == 'remove-bottom' ) {
		$my_class .= $remove_bottom . ' ';
	}
	if ( $gap == 'remove-top' ) {
		$my_class .= $gap . ' ';
	}

	$titles = '';

	if ( $show_title == 'true' && !empty( $col_title ) ):

		$t_st = ($col_heading == 'style2') ? ' style2' : '';
		$titles .= '<div class="title' . $t_st . '">';
		$titles .= '<h2>' . $col_title . '</h2>';
		$titles .= ($col_sub_title) ? '<span>' . $col_sub_title . '</span>' : '';
		$titles .= '</div>';
	endif;

	$output = '<section class="block ' . $my_class . '">';
	$output .= $titles;
}
