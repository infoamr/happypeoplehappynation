<?php

$shortcode_section = array();
$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Section", 'lifeline2'),
    "param_name" => "section",
    "value" => array(__('False', 'lifeline2') => 'false', esc_html__('True', 'lifeline2') => 'true'),
    "description" => esc_html__("Enable section for this shortcode", 'lifeline2'),
);
$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Remove Top Space", 'lifeline2'),
    "param_name" => "gap",
    "value" => array('False' => 'false', 'True' => 'remove-top'),
    "description" => esc_html__("Remove the Gap from top of the Section.", 'lifeline2'),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);
$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("No Padding", 'lifeline2'),
    "param_name" => "section_padding",
    "value" => array(__('False', 'lifeline2') => 'false', esc_html__('True', 'lifeline2') => 'no-padding'),
    "description" => esc_html__("Remove the Padding from top and bottom of the Section.", 'lifeline2'),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);
$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Remove Bottom", 'lifeline2'),
    "param_name" => "remove_bottom",
    "value" => array(__('False', 'lifeline2') => 'false', esc_html__('True', 'lifeline2') => 'remove-bottom'),
    "description" => esc_html__("Remove the Padding from bottom of the Section.", 'lifeline2'),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);
//start title settings
$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Show Title", 'lifeline2'),
    "param_name" => "show_title",
    "value" => array(__('False', 'lifeline2') => 'false', esc_html__('True', 'lifeline2') => 'true'),
    "description" => esc_html__("Make this section with title then true.", 'lifeline2'),
    "dependency" => array(
        'element' => 'section',
        'value' => array('true')
    ),
);

$shortcode_section[] = array(
    "type" => "dropdown",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Heading Styles", 'lifeline2'),
    "param_name" => "col_heading",
    "description" => esc_html__("Select Heading Style.", 'lifeline2'),
    "value" => array(
        esc_html__('Select Title Style', 'lifeline2') => '',
        esc_html__('Center Title', 'lifeline2') => 'style1',
        esc_html__('Side Title', 'lifeline2') => 'style2',
       
    ),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);

$shortcode_section[] = array(
    "type" => "textfield",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Enter the Title", 'lifeline2'),
    "param_name" => "col_title",
    "description" => esc_html__("Enter the title of this section.", 'lifeline2'),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);

$shortcode_section[] = array(
    "type" => "textfield",
    
    "class" => "",
    'group' => esc_html__('Section Settings', 'lifeline2'),
    "heading" => esc_html__("Enter the Sub Title", 'lifeline2'),
    "param_name" => "col_sub_title",
    "description" => esc_html__("Enter the sub title of this section.", 'lifeline2'),
    'dependency' => array(
        'element' => 'section',
        'value' => array('true')
    ),
);
