jQuery(document).ready(function ($) {
    "use strict";
    var $this = '';
    $('button#demo-importer').live('click', function () {
        var parent = $(this).parents('div.extention-bottom2');
        var id = $(this).data('id');
        $this = (this);
        var name = $(this).data('name');
        var isCheck = $(parent).find('input');
        var media = '';
        if ($(isCheck).is(':checked')) {
            media = 'true';
        }
        var data = 'media=' + media + '&fileid=' + id + '&name=' + name + '&action=crazyblog_installDemo';
        ajaxCall(data);
    });
    function ajaxCall(data) {
        jQuery.ajax({
            type: "post",
            url: ajax_url,
            data: data,
            //contentType: false,
            processData: false,
            beforeSend: function () {
                $(this).prop('disabled', true);
                $($this).find('i').fadeIn('slow');
                $('div.page-wraper').fadeIn('slow');
            },
        }).done(function (response) {
            if ('undefined' !== typeof response.status && 'newAJAX' === response.status) {
                ajaxCall(data);
            } else if ('undefined' !== typeof response.message) {
                $('div.page-wraper').fadeOut('slow');
                $($this).prop('disabled', false);
                $($this).find('i').fadeOut('slow');
                $('div.popup-wrapper p#response-data').empty();
                $('div.popup-wrapper').fadeIn('slow');
                $('div.popup-wrapper p#response-data').html(response.message);
            } else {
                $('div.page-wraper').fadeOut('slow');
                $($this).prop('disabled', false);
                $($this).find('i').fadeOut('slow');
                $('div.popup-wrapper p#response-data').empty();
                $('div.popup-wrapper').fadeIn('slow');
                $('div.popup-wrapper p#response-data').html(response);
            }
        }).fail(function (error) {
            $('div.page-wraper').fadeOut('slow');
            $($this).prop('disabled', false);
            $($this).find('i').fadeOut('slow');
            $('div.popup-wrapper p#response-data').empty();
            $('div.popup-wrapper').fadeIn('slow');
            $('div.popup-wrapper p#response-data').html(error.statusText);
        });
    }

});