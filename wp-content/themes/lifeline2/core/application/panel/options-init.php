<?php
$optName = 'lifeline2_theme_options';
$theme = wp_get_theme();
$args = array(
    'default_show' => FALSE,
    'disable_tracking' => FALSE,
    'opt_name' => $optName,
    'use_cdn' => FALSE,
    'display_name' => 'Lifeline2',
    'display_version' => FALSE,
    'page_slug' => 'lifeline2_options',
    'page_title' => esc_html__('Theme Options', 'lifeline2'),
    'update_notice' => FALSE,
    'admin_bar' => TRUE,
    'menu_type' => 'submenu',
    'menu_title' => esc_html__('Theme Options', 'lifeline2'),
    'allow_sub_menu' => TRUE,
    'page_parent' => 'themes.php',
    'page_parent_post_type' => '',
    'customizer' => TRUE,
    'default_mark' => '*',
    'google_api_key' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'class' => 'lifeline2',
    'hints' => array(
        'icon_position' => 'right',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
            'shadow' => '1',
            'rounded' => '1',
            'style' => 'youtube',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'duration' => '500',
                'event' => 'mouseover mouseenter click',
            ),
            'hide' => array(
                'effect' => 'fade',
                'duration' => '500',
                'event' => 'mouseleave unfocus',
            ),
        ),
    ),
    'output' => TRUE,
    'output_tag' => TRUE,
    'settings_api' => TRUE,
    'cdn_check_time' => '1440',
    'compiler' => TRUE,
    'global_variable' => 'lifeline2',
    'page_permissions' => 'manage_options',
    'save_defaults' => TRUE,
    'show_import_export' => TRUE,
    'open_expanded' => FALSE,
    'database' => 'options',
    'transient_time' => '3600',
    'network_sites' => TRUE,
    'hide_reset' => TRUE,
    'async_typography' => TRUE,
);
Redux::setArgs($optName, $args);
$files = scandir(lifeline2_ROOT . "core/application/library/vp_options/");
unset($files['0']);
unset($files['0']);
if (!empty($files) && count($files) > 0) {
    foreach ($files as $file) {
        $fileInfo = pathinfo($file);
        locate_template('core/application/library/vp_options/' . lifeline2_set($fileInfo, 'filename') . '.php', true, true);
    }
}

//$filesArray = unload_ThemeInit::unload_singleton()->unload_getSetting( 'optionsArray' );
$filesArray = array(
    'general_setting' => array(
        'header_setting',
        'footer_setting',
        'mailchimp_setting',
        'apis_setting',
        'donationbanner_setting',
        'signup_setting',
        'donationbox_settings',
        'custom_script_settings'
    ),
    'blog_setting' => array(
        'post_setting',
        'author_setting',
        'archive_setting',
        'category_setting',
        'tag_setting',
        'search_setting',
        '404_setting'
    ),
    'templates_setting' => array(
        'project_setting',
        'cause_setting',
        'story_setting',
        'team_setting',
        'event_setting',
        'gallery_setting',
        'service_setting',
        'product_page_setting',
        
    ),
    'sidebar_setting' => array(),
    'under_construction_setting' => array(),
    'typography_setting' => array(
        'heading_setting',
        'body_font_setting',
        'menu_font_setting'
    ),
    'language_settings'=>array(),
);
if (!empty($filesArray) && count($filesArray) > 0) {
    $optionsArray = array();
    foreach ($filesArray as $parent => $option) {
        $class = 'lifeline2_' . $parent . '_menu';
        $getMethods = get_class_methods($class);
        $class_obj = new $class;
        $method = lifeline2_set($getMethods, '1');
        $getProperties = get_class_vars($class);

        $title = (lifeline2_set($class_obj, 'title') != '') ? lifeline2_set($class_obj, 'title') : '';
        $desc = (lifeline2_set($class_obj, 'description') != '') ? lifeline2_set($class_obj, 'description') : '';
        $icon = lifeline2_set($getProperties, 'icon');
        $id = lifeline2_set($getProperties, 'id');
        //printr((new $class() )->$method());
        $menu = array(
            'title' => $title,
            'id' => $id,
            'desc' => $desc,
            'fields' => (new $class())->$method()
        );
        
        if (!empty($icon)) {
            $menu['icon'] = 'el ' . $icon;
        }
        Redux::setSection($optName, $menu);

        if (is_array($option) && !empty($option)) {
            foreach ($option as $subMenu) {
                $class = 'lifeline2_' . $subMenu . '_menu';
                $class_child_obj = new $class;
                $getMethods = get_class_methods($class);
                $method = lifeline2_set($getMethods, '1');
                $getProperties = get_class_vars($class);

                $title = (lifeline2_set($class_child_obj, 'title') != '') ? lifeline2_set($class_child_obj, 'title') : '';
                $desc = (lifeline2_set($class_child_obj, 'description') != '') ? lifeline2_set($class_child_obj, 'description') : '';
                $icon = lifeline2_set($getProperties, 'icon');
                $id = lifeline2_set($getProperties, 'id');
                $innerMenu = array(
                    'title' => $title,
                    'id' => $id,
                    'desc' => $desc,
                    'subsection' => true,
                    'fields' => (new $class())->$method()
                );
                if (!empty($icon)) {
                    $menu['icon'] = 'el ' . $icon;
                }
                Redux::setSection($optName, $innerMenu);
            }
        }
    }
}
