(function ($) {
    "use strict";
    redux.field_objects = redux.field_objects || {};
    redux.field_objects.importer = redux.field_objects.importer || {};
    var ajax_request = null;
    redux.field_objects.importer.init = function (selector) {
        if (!selector) {
            selector = $(document).find(".redux-group-tab:visible").find('.redux-container-importer:visible');
        }
        $(selector).each(
                function () {
                    var el = $(this);
                    var parent = el;
                    if (!el.hasClass('redux-field-container')) {
                        parent = el.parents('.redux-field-container:first');
                    }
                    if (parent.is(":hidden")) {
                        return;
                    }
                    if (parent.hasClass('redux-field-init')) {
                        parent.removeClass('redux-field-init');
                    } else {
                        return;
                    }
                    redux.field_objects.importer.modInit(el);
                }
        );
    };

    redux.field_objects.importer.ajaxCall = function (data) {

        ajax_request = $.ajax({
            dataType: 'json',
            type: "post",
            url: ajaxurl,
            data: data,
        }).done(function (response) {
            if (response.next_request == true){
                redux.field_objects.importer.ajaxCall(response.data);
                var progress_split = 100/response.total_request;
                var progress_value = progress_split*(response.request_number-1);
                $( "#progressbar" ).progressbar({
                    value: progress_value
                });
                $('.download-result').html(parseInt(progress_value)+'% demo imported please be patience to complete the process!');
                $('.download-result').removeClass('alert-success');
                $('.download-result').addClass('alert-info');
            }else if(response.next_request == false){
                if(response.message_type == false){
                    $('.download-result').html(response.message);
                    $('.download-result').removeClass('alert-success');
                    $('.download-result').addClass('alert-warning');
                    $('.demo-importer').removeAttr("disabled");
                }else{
                    $('.download-result').html(response.message);
                    $('.download-result').removeClass('alert-info');
                    $('.download-result').addClass('alert-success');
                    $( "#progressbar" ).progressbar({
                        value: 100
                    });
                    $('.demo-importer').removeAttr("disabled");
                }
            }
        }).fail(function (error) {
            if(error.status == 504){
                if($('.download-result').hasClass('alert-info'))
                    $('.download-result').removeClass('alert-info');
                if($('.download-result').hasClass('alert-success'))
                    $('.download-result').removeClass('alert-success');
                $('.download-result').html('<h2>Sorry there was a problem with demo installation!</h2><p>One click importer needs the following specs in php.ini:<br>1- post_max_size = 120M<br>2- upload_max_filesize = 120M<br>3- memory_limit = 128M<br>4- max_execution_time = 300<br>5- max_input_time = 300<br>If you have verified the specifications which are listed above. Please try again! or submit a ticket to our <a href="https://webinane.ticksy.com/">Support Team</a></p>');
                $('.download-result').addClass('alert-warning');
            }
        });
    }


    redux.field_objects.importer.modInit = function (el) {
        el.find('a#overlay-install-demo, div#install-demo').on('click', function () {
            var uri = $(this).data('uri');
            $('.demo-importer').data('uri', uri);
            if ($('body').find('div.preloader-wrapper') > 0) {
            } else {
                $('body').prepend('<div class="preloader-wrapper"></div>');
            }
            if($('.download-result').length != 0)
                $('.download-result').remove();
            if($('#progressbar').length != 0)
                $('#progressbar').hide();
            $('.preloader-wrapper').show();

        });
        $('.demo-importer').on('click', function () {
            if (!$("input[name='demo_type']:checked").val()) {
                alert('Please select demo content type which you want to import on site!');
                return false;
            }
            var demo_type = $("input[name='demo_type']:checked").val();
            $('.demo-importer').attr("disabled","disabled");
            $('.preloader-wrapper .box-content').prepend('<p class="download-result alert alert-info">Downloading demo data...</p>');
            var uri = $(this).data('uri');
            var data = 'uri=' + uri + '&action=lifeline2_download_demo&demo_type='+demo_type;
            ajax_request = $.ajax({
                dataType: 'json',
                type: "post",
                url: ajaxurl,
                data: data,
            }).done(function (response) {
               
                if (response.next_request == true){
                    $('.download-result').html(response.message);
                    $('.download-result').removeClass('alert-info');
                    $('.download-result').addClass('alert-success');
                    $('#progressbar').show();
                    $( "#progressbar" ).progressbar({
                        value: 2,
                        classes: {
                          "ui-progressbar": "highlight"
                        }
                    });
                    redux.field_objects.importer.ajaxCall(response.data);
                }else{
                    $('.download-result').html(response.message);
                    $('.download-result').removeClass('alert-info');
                    $('.download-result').addClass('alert-danger');
                }
                
            }).fail(function (error) {
                if(error.status == 504){
                    if($('.download-result').hasClass('alert-info'))
                        $('.download-result').removeClass('alert-info');
                    if($('.download-result').hasClass('alert-success'))
                        $('.download-result').removeClass('alert-success');
                    $('.download-result').html('<h2>Sorry there was a problem with demo installatopm!</h2><p>One click importer needs the following specs in php.ini:<br>1- post_max_size = 120M<br>2- upload_max_filesize = 120M<br>3- memory_limit = 128M<br>4- max_execution_time = 300<br>5- max_input_time = 300<br>If you have verified the specifications which are listed above. Please submit a ticket to our <a href="https://webinane.ticksy.com/">Support Link</a></p>');
                    $('.download-result').addClass('alert-warning');
                }
            });
            return false;
        });
        $('div.importer-box .close').live('click', function () {
            $('div.preloader-wrapper').hide();
            if(ajax_request != null)
                ajax_request.abort();
        });
    };
})(jQuery);

