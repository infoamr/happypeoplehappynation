
<a class="button button-primary" href="<?php echo get_site_url(null, '/wp-admin/themes.php?page=lifeline-setup'); ?>">
                                        <?php esc_html_e( 'Import Demo Content', 'lifeline2' ) ?>
</a>
<?php
//$demosArray = array(
//    array(
//        'name'    => esc_html__( 'Animal', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/animal.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/animal/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/animal-ngo/',
//    ),
//    array(
//        'name'    => esc_html__( 'One Cause', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/one-cause.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/causes/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/cause/',
//    ),
//    array(
//        'name'    => esc_html__( 'Charity', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/charity.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/charity/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/charity/',
//    ),
//    array(
//        'name'    => esc_html__( 'Fundraise', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/fundraise.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/fundrise/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/fundraise/',
//    ),
//    array(
//        'name'    => esc_html__( 'Main', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/main.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/main/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/',
//    ),
//    array(
//        'name'    => esc_html__( 'NGO', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/ngo.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/ngo/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/ngo/',
//    ),
//    array(
//        'name'    => esc_html__( 'Non Profit', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/non-profit.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/non-profit/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/nonprofit/',
//    ),
//    array(
//        'name'    => esc_html__( 'Non Profit Blog', 'lifeline2' ),
//        'src'     => lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/demo/non-profit-blog.jpg',
//        'uri'     => 'http://webinane.com/demoData/lifeline2/non-profit-blog/demo.zip',
//        'preview' => 'http://themes.webinane.com/wp/lifeline2/nonprofit-blog/',
//    )
//);
