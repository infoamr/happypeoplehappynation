<?php

/**
 * importer field extension class.
 *
 * @version     1.0.0
 */
/* exit if accessed directly. */
if (!defined('ABSPATH'))
    exit;

/* don't duplicate me! */
if (!class_exists('ReduxFramework_extension_importer')) {


    /**
     *  ReduxFramework_extension_importer class.
     *
     * @since       1.0.0
     */
    class ReduxFramework_extension_importer extends ReduxFramework {

        /**
         * @access      protected
         * @since       1.0.0
         * @var         array $parent ReduxFramework object
         */
        protected $parent;

        /**
         * @access      public
         * @static
         * @since       1.0.0
         * @var         object $instance singleton instance
         */
        public static $instance;

        /**
         * class Constructor - defines the args for the extions class.
         *
         * @access      public
         * @since       1.0.0
         * @param       array $sections panel sections
         * @param       array $args panel arguments
         * @param       array $extra_tabs extra panel tabs
         * @return      void
         */
        public function __construct($parent) {
            $this->parent = $parent;
            $this->field_name = 'importer';

            if (!isset(self::$instance)) {
                self::$instance = $this;
            }

            /* implement custom field. */
            add_filter('redux/' . $this->parent->args['opt_name'] . '/field/class/' . $this->field_name, array($this, 'overload_field_path'));
            $this->redux_importer_section();
            add_action('wp_ajax_nopriv_lifeline2_download_demo', array($this, 'lifeline2_download_demo'));
            add_action('wp_ajax_lifeline2_download_demo', array($this, 'lifeline2_download_demo'));
            add_action('wp_ajax_nopriv_lifeline2_demoImporter', array($this, 'lifeline2_demoImporter'));
            add_action('wp_ajax_lifeline2_demoImporter', array($this, 'lifeline2_demoImporter'));
        }

        /* __construct() */

        /**
         * get singleton instance.
         *
         * @access      public
         * @static
         * @since       1.0.0
         * @return      object      $instance       singleton instance
         */
        public static function getInstance() {

            if (!isset(self::$instance)) {

                /* instance class. */
                self::$instance = new self;
            }

            return self::$instance;
        }

        /* get_instance() */

        /**
         * implement custom field.
         *
         * @access      public
         * @static
         * @since       1.0.0
         * @return      object      $instance       singleton instance
         */
        public function overload_field_path($field) {
            return lifeline2_ROOT . 'core/application/panel/redux-extensions/extensions/' . $this->field_name . '/' . $this->field_name . '.php';
        }

        /* overload_field_path() */

        public function redux_importer_section() {
            for ($n = 0; $n <= count($this->parent->sections); $n++) {
                if (isset($this->parent->sections[$n]['id']) && $this->parent->sections[$n]['id'] == 'redux_importer_section') {
                    return;
                }
            }

            $redux_importer_label = trim(esc_html(apply_filters('wp_importer_label', esc_html__('Demo Importer', 'lifeline2'))));
            $redux_importer_label = (!empty($wbc_importer_label)) ? $redux_importer_label : esc_html__('Demo Importer', 'lifeline2');
            $section = array(
                'id' => 'redux_importer_section',
                'title' => $redux_importer_label,
                'desc' => 'Click on button to choose demo styles ',
                'icon' => 'el-icon-website',
                'fields' => array(
                    array(
                        'id' => 'redux_importer',
                        'type' => 'importer',
                        'full_width' => true,
                    )
                )
            );
            $this->parent->sections[] = $section;
        }

        public function lifeline2_demoImporter() {
            if (isset($_POST) && $_POST['action'] == 'lifeline2_demoImporter') {
                $url = lifeline2_set($_POST, 'uri');
                $file = 'demo.zip';
                $ext = get_loaded_extensions();
                global $wp_version, $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }

                if (!empty($url)) {

                    $response_message = lifeline2_set($_POST, 'message');
                    if(lifeline2_set($_POST, 'demo_type') == 'images'){
                        $dir = ABSPATH . 'wp-content/webinane/images_data/';
                        $demo_type = 'images';
                    }else{
                       $dir = ABSPATH . 'wp-content/webinane/placehold_data/'; 
                       $demo_type = 'placehold';
                    }

                    $total_files = count(array_diff(scandir($dir), array('.', '..')));
                    if(lifeline2_set($_POST, 'request_number'))
                        $file_number = lifeline2_set($_POST, 'request_number');
                    else
                        $file_number = 1;
                    $file =  ABSPATH . 'wp-content/webinane/'.lifeline2_set($_POST, 'demo_type').'_data/data_'.$file_number.'.xml';
                    
                    if (file_exists($file)) {
                        define('WP_LOAD_IMPORTERS', true);
                        if (!class_exists('WP_Import')) {
                            include_once( ABSPATH . 'wp-content/plugins/lifeline2/wordpress-importer/wordpress-importer.php');
                        }
                        ob_start();
                        $GLOBALS['wp_import'] = new WP_Import();
                        $GLOBALS['wp_import']->fetch_attachments = true;
                        $GLOBALS['wp_import']->import($file);
                        $message = ob_get_contents();
                        ob_end_clean();
                        
                        $message = $response_message.$message;
                        $request_num = $file_number+1;
                        
                        if($file_number == $total_files){

                            $this->lifeline2_ImportSettings();
                            $message .= sprintf(esc_html__('Awesomeness has beed created %s to enjoy it!', 'lifeline2'),  '<a target="_blank" href="'.esc_url(home_url('/')).'">'.esc_html__('click here', 'lifeline2').'</a>' );
                            echo json_encode(array('next_request' => false, 'message_type'=>true,  'message' => $message)); 
                        }else{
                            echo json_encode(array('next_request' => true, 'message_type'=>false, 'total_request' => $total_files, 'request_number' => $request_num,  'data' => "uri=".lifeline2_set($_POST, 'uri')."&demo_type=".$demo_type."&message=".$message."&request_number=".$request_num."&action=lifeline2_demoImporter"));
                        }
                        exit;
                        
                    }else{
                        echo json_encode(array('next_request' => false, 'message_type'=>false, 'message' => esc_html__('Sorry The demo could not install properly. Please submit the ticket to our support team', 'lifeline2')));
                    }
                    
                }
            } else {
                echo json_encode(array('next_request' => false, 'message_type'=>false, 'message' => esc_html__('Sorry this demo is not currently available. Please submit your ticket our support team', 'lifeline2')));
            }
            exit;
        }
        
        public function lifeline2_download_demo() {
            global $wp_version, $wp_filesystem;

            if (isset($_POST) && $_POST['action'] == 'lifeline2_download_demo') {

                $url = lifeline2_set($_POST, 'uri');
                $file = WP_CONTENT_DIR . '/webinane/demo.zip';
                $ext = get_loaded_extensions();

                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }

                $demo_type = lifeline2_set($_POST, 'demo_type') ? lifeline2_set($_POST, 'demo_type') : 'placehold';

                if (!empty($url)) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                    $wp_filesystem->mkdir(str_replace('\\', '/', WP_CONTENT_DIR) . '/webinane');

                    $query = array('secretkey' => 'dsfkdkx3232');
                    $zipdata = '';
                    if (function_exists('lifeline2_downloadZip')) {

                        $downloadUrl = add_query_arg($query, $url);

                        lifeline2_downloadZip($downloadUrl);

                    }
                    if (in_array('zip', $ext)) {



                        unzip_file( $file, WP_CONTENT_DIR . '/webinane/' );
                        unlink($file);
                        $message = esc_html__('Download completed! Now importing the demo please be patience...', 'lifeline2');
                        $request = true;

                    } else {
                        $message = esc_html__('Please Enable ZIP Extension in php.ini', 'lifeline2');
                        $request = false;
                    }
                    
                }
            } else {
                $messag = esc_html__('Given URL not present', 'lifeline2');
                $request = false;
            }
            echo json_encode(array('next_request' => $request, 'message' => $message, 'data' => "uri=".lifeline2_set($_POST, 'uri')."&request_number=1&demo_type=".$demo_type."&action=lifeline2_demoImporter"));
            exit;
        }

        public function lifeline2_rrmdir($dir) {
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (filetype($dir . "/" . $object) == "dir")
                            $this->lifeline2_rrmdir($dir . "/" . $object);
                        else
                            unlink($dir . "/" . $object);
                    }
                }
                reset($objects);
                rmdir($dir);
            }
        }

        public function lifeline2_ImportSettings() {
            if (function_exists('lifeline2_wpImporterScript')) {
                lifeline2_wpImporterScript();
            }
            $importer = new lifeline2_import_export();
            $importer->import();
            $this->lifeline2_rrmdir(realpath(ABSPATH . 'wp-content/webinane'));
            if (function_exists('lifeline2_delLogs')) {
                //lifeline2_delLogs();
            }
        }

    }

    /* class */
}   /* if */
