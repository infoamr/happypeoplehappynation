(function ($) {
    "use strict";
    redux.field_objects = redux.field_objects || {};
    redux.field_objects.language = redux.field_objects.language || {};
    redux.field_objects.language.fieldID = '';
    redux.field_objects.language.optName = '';
    redux.field_objects.language.init = function (selector) {
        if (!selector) {
            selector = $(document).find(".redux-group-tab:visible").find('.redux-container-language:visible');
        }

        $(selector).each(
            function () {
                var el = $(this);
                var parent = el;
                if (!el.hasClass('redux-field-container')) {
                    parent = el.parents('.redux-field-container:first');
                }
                if (parent.is(":hidden")) { // Skip hidden fields
                    return;
                }
                if (parent.hasClass('redux-field-init')) {
                    parent.removeClass('redux-field-init');
                } else {
                    return;
                }
                redux.field_objects.language.modInit(el);
            }
        );
    };
    redux.field_objects.language.modInit = function (el) {
        el.find('select#language_switcher').select2({
            placeholder: language.switcher,
            allowClear: true,
            dropdownCssClass: "language-select2"
        });

        /* process upload language fiel */
        var max_file_size = 2048576;
        var allowed_file_types = ['mo'];
        var result_output = el.find('#output');
        var my_form_id = el.find('form#language_uploader');
        var my_button_id = el.find('#upload_file');
        var progress_bar_id = el.find('#progress-wrp');
        var total_files_allowed = 1;
        el.find('#upload_file').on("click", function (event) {
                event.preventDefault();


                var proceed = true;
                var error = [];
                var total_files_size = 0;

                //reset progressbar
                el.find("#progress-wrp .progress-bar").css("width", "0%");
                el.find("#progress-wrp .status").text("0%");
                if (el.find('input#language_file').files.length === 0) {
                    return;
                }
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    if (!el.find("input#language_file").val()) {
                        error.push(language.no_file);
                    }
                    var fsize = el.find("input#language_file")[0].files[0].size; //get file size
                    var ftype = el.find("input#language_file")[0].files[0].name; // get file type
                    var extension = ftype.substr((ftype.lastIndexOf(".") + 1));
                    switch (extension) {
                        case "mo":
                            break;
                        default:
                            error.push("<b>" + ftype + "</b> " + language.unsupported);
                    }
                    if (fsize > 1048576) {
                        error.push("<b>" + redux.field_objects.language.bytesToSize(fsize) + "</b> " + language.bigFile);
                    }
                } else {
                    error.push(language.api_error);
                }
                // if (!window.File && window.FileReader && window.FileList && window.Blob) { //if browser doesn't supports File API
                //     error.push(language.api_error);
                // } else {
                //     console.log(my_form_id.elements);
                //     var total_selected_files = my_form_id.elements['language_file'].files.length;
                //     //iterate files in file input field
                //     $(this.elements['language_file'].files).each(function (i, ifile) {
                //         if (ifile.value !== "") {
                //             console.log(ifile.type);
                //             if (allowed_file_types.indexOf(ifile.type) === -1) {
                //                 error.push("<b>" + ifile.name + "</b> is unsupported file type!"); //push error text
                //                 proceed = false; //set proceed flag to false
                //             }
                //
                //             total_files_size = total_files_size + ifile.size; //add file size to total size
                //         }
                //     });
                //
                //     //if total file size is greater than max file size
                //     if (total_files_size > max_file_size) {
                //         error.push("You have " + total_selected_files + " file(s) with total size " + total_files_size + ", Allowed size is " + max_file_size + ", Try smaller file!"); //push error text
                //         proceed = false; //set proceed flag to false
                //     }
                //
                //     var submit_btn = $(this).find("input[type=submit]"); //form submit button
                //
                //     //if everything looks good, proceed with jQuery Ajax
                //     if (proceed) {
                //         //submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
                //         var form_data = new FormData(this); //Creates new FormData object
                //         var post_url = $(this).attr("action"); //get action URL of form
                //
                //         //jQuery Ajax to Post form data
                //         $.ajax({
                //             url: post_url,
                //             type: "POST",
                //             data: form_data,
                //             contentType: false,
                //             cache: false,
                //             processData: false,
                //             xhr: function () {
                //                 //upload Progress
                //                 var xhr = $.ajaxSettings.xhr();
                //                 if (xhr.upload) {
                //                     xhr.upload.addEventListener('progress', function (event) {
                //                         var percent = 0;
                //                         var position = event.loaded || event.position;
                //                         var total = event.total;
                //                         if (event.lengthComputable) {
                //                             percent = Math.ceil(position / total * 100);
                //                         }
                //                         //update progressbar
                //                         $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
                //                         $(progress_bar_id + " .status").text(percent + "%");
                //                     }, true);
                //                 }
                //                 return xhr;
                //             },
                //             mimeType: "multipart/form-data"
                //         }).done(function (res) { //
                //             $(my_form_id)[0].reset(); //reset form
                //             $(result_output).html(res); //output response from server
                //             submit_btn.val("Upload").prop("disabled", false); //enable submit button once ajax is done
                //         });
                //
                //     }
                // }

                $(result_output).html(""); //reset output
                $(error).each(function (i) { //output any error to output element
                    $(result_output).append('<div class="error">' + error[i] + "</div>");
                });

            }
        );
    };
    redux.field_objects.language.bytesToSize = function (bytes) {
        var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
        if (bytes == 0) return "0 Bytes";
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
    }
})(jQuery);