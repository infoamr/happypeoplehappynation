<?php
$settings = lifeline2_get_theme_options();
lifeline2_donationBanner();
?>
<?php if(lifeline2_set($settings, 'footer_color_scheme') != ''){ ?>
<style>.footer_color_scheme:before { background-color: <?php echo lifeline2_set($settings, 'footer_color_scheme');?>; }</style>
<?php } ?>
<footer <?php if(lifeline2_set($settings, 'footer_color_scheme') != ''){ echo 'class="footer_color_scheme"';}?>>
    <?php echo (lifeline2_set( lifeline2_set( $settings, 'footer_bg' ), 'background-image' )) ? '<div class="parallax" style="background:url(' . lifeline2_set( lifeline2_set( $settings, 'footer_bg' ), 'background-image' ) . ') repeat scroll 0 0 rgba(0, 0, 0, 0);" data-velocity="-.1"></div>' : ''; ?>
    <?php
    $sidebars = lifeline2_set( lifeline2_set( $settings, 'footer_widgets_section_sidebars_settings' ), 'footer_widgets_section_sidebars_settings' );

    ($sidebars) ? array_pop( $sidebars ) : '';
    if ( is_active_sidebar( 'footer_widget_area' ) ):
        ?>
        <div class="block">
            <div class="container">
                <div class="footer-widgets">
                    <div class="row">
                        <?php dynamic_sidebar( 'footer_widget_area' ); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ( lifeline2_set( $settings, 'show_newsletter_section' ) ): ?>
        <div class="footer-bar">
            <div class="container">
                <div class="row">
                    <?php if(class_exists('Lifeline2_Newsletter')):?>
                    <?php wp_enqueue_script('lifeline2_newsletter-script');?>
                    <div class="col-md-6 column">
                        <div class="newsletter-signup">
                            <?php echo (lifeline2_set( $settings, 'subc_title' )) ? '<h4 class="footer-bar-title">' . lifeline2_set( $settings, 'subc_title' ) . '</h4>' : ''; ?>
                            <form id="newsletter-form" method="post">
                                <div id="response-message"></div>
                                <input id="newsletter-email" type="text" placeholder="<?php echo esc_html( lifeline2_set( $settings, 'email_placehold' ) ); ?>" />
                                <button id="newsletter-form-submit" type="submit" class="theme-btn"><?php echo esc_html( lifeline2_set( $settings, 'subc_btn_txt', 'SUBSCRIBE' ) ); ?></button>
                            </form>
                        </div>
                    </div>
                    <?php endif;?>
                    <?php if ( lifeline2_set( $settings, 'footer_social_icons' ) ): ?>
                        <?php $footer_icons = lifeline2_set( $settings, 'footer_social_media' ); //printr($footer_icons);  ?>
                        <div class="col-md-6 column">
                            <div class="footer-social-btns">
                                <?php echo (lifeline2_set( $settings, 'social_icons_title' )) ? '<h4 class="footer-bar-title">' . lifeline2_set( $settings, 'social_icons_title' ) . '</h4>' : ''; ?>

                                <?php if ( !empty( $footer_icons ) ): ?>
                                    <?php $style = ''; ?>
                                    <div class="social">
                                        <?php foreach ( $footer_icons as $f_icon ): ?>
                                            <?php $footer_social_icons = json_decode( urldecode( lifeline2_set( $f_icon, 'data' ) ) );  //printr(); ?>
                                            <?php if ( lifeline2_set( $footer_social_icons, 'enable' ) == '' ) continue; ?>
	                                        <?php $style .='<style>.footer-icon-' . esc_attr( lifeline2_set( $footer_social_icons, 'icon' ) ) . ':hover{background:' . lifeline2_set( $footer_social_icons, 'background' ) . ' !important;color:' . lifeline2_set( $footer_social_icons, 'color' ) . ' !important;}</style>'; ?>

                                            <a class="footer-icon-<?php echo esc_attr( lifeline2_set( $footer_social_icons, 'icon' ) ); ?>" itemprop="url" href="<?php echo esc_url( lifeline2_set( $footer_social_icons, 'url' ) ); ?>" title="<?php echo esc_attr( lifeline2_set( $f_icon, 'social_title' ) ); ?>" target="_blank"><i class="fa <?php echo esc_attr( lifeline2_set( $footer_social_icons, 'icon' ) ); ?>"></i></a>

                                        <?php endforeach; ?>
                                    </div><!-- Social -->
	                                <?php echo($style); ?>
                                    <?php //wp_add_inline_style( 'lifelin2_style', $style ); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- Footer Bar -->
    <?php endif; ?>
    <?php if ( lifeline2_set( $settings, 'show_footer_bottom' ) ): ?>
        <div class="bottom-bar <?php echo lifeline2_set($settings, 'footer_css');?>">
            <div class="container">
                <div class="row">
                    <?php if ( lifeline2_set( $settings, 'show_footer_menu' ) && has_nav_menu( 'footer-menu' ) ): ?>
                        <div class="col-md-6 column">
                            <div class="bottom-links">
                                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => '', 'menu_class' => '', 'container' => false ) ); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ( lifeline2_set( $settings, 'copyright_text' ) ): ?>
                        <?php if ( lifeline2_set( $settings, 'show_footer_menu' ) && has_nav_menu( 'footer-menu' ) ): ?>
                            <div class="col-md-6 column">
                            <?php else: ?>
                                <div class="col-md-12 column">
                                <?php endif; ?>
                                <p><?php echo balanceTags( lifeline2_set( $settings, 'copyright_text' ) ); ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
</footer>
</div>
<?php
(new lifeline2_Common )->lifeline2_footer();
wp_footer();
?>
</body>
</html>
