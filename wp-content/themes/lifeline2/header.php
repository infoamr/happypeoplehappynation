<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <head>
        <?php $opt        = lifeline2_get_theme_options(); ?>
        <?php echo ( lifeline2_set( lifeline2_set( $opt, 'site_favicon' ), 'url' )) ? '<link rel="icon" type="image/png" href="' . esc_url( lifeline2_set( lifeline2_set( $opt, 'site_favicon' ), 'url' ) ) . '">' : ''; ?>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php wp_head();?>
    </head>
    <body <?php body_class(); ?> itemscope >
        <?php
        $loader     = lifeline2_set( $opt, 'css_loader_style' );
        $isLoader   = (lifeline2_set( $opt, 'site_loader' ) == 1) ? '' : 'style=display:none';
        $boxed      = (lifeline2_set( $opt, 'boxed_layout_status' ) == 1 ) ? 'boxed' : '';
        $top_margin = (lifeline2_set( $opt, 'boxed_layout_status' ) == 1 && lifeline2_set( $opt, 'boxed_top' ) != "") ? ' style=margin-top:' . esc_attr( lifeline2_set( $opt, 'boxed_top' ) ) . 'px' : '';
        $grid_left = (lifeline2_set( $opt, 'grid_layout_status' ) == 1 && lifeline2_set( $opt, 'grid_left' ) != "") ? ' style=padding-left:' . esc_attr( lifeline2_set( $opt, 'grid_left' ) ) . 'px;' . 'padding-right:' . esc_attr( lifeline2_set( $opt, 'grid_right' ) ) . 'px;' : '';
        $grid_right = (lifeline2_set( $opt, 'grid_layout_status' ) == 1 && lifeline2_set( $opt, 'grid_right' ) != "") ? ' style=padding-right:' . esc_attr( lifeline2_set( $opt, 'grid_right' ) ) . 'px' : '';
        ?>
        <div class="donation-modal-wraper"></div>
        <div id="bank_transcation"></div>
        <div class="donation-modal-preloader">
            <div class="my_loader"></div>
        </div>
        <div class="donation-modal-box"></div>
        <div class="theme-layout <?php echo esc_attr( $boxed ) ?>"<?php echo esc_attr( $top_margin ) ?><?php echo esc_attr( $grid_left ) ?>>
            <div class="bg_overlay" <?php echo esc_attr( $isLoader ) ?>>
                <div class="loader-center loader-inner <?php echo esc_attr( $loader ) ?>">
                    <?php echo balanceTags( lifeline2_Common::lifeline2_loader_html( $loader ) ); ?>
                </div>
            </div>
            <?php do_action( 'lifeline2_custom_header' ); ?>
