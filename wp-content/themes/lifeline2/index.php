<?php
get_header();
global $wp_query;
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
//printr($settings);
if ( !$queried_object ) {
    $lifeline2_sidebar     = (lifeline2_set( $settings, 'blog_page_sidebar' )) ? lifeline2_set( $settings, 'blog_page_sidebar' ) : 'primary_widget_area';
    $layout      = (lifeline2_set( $settings, 'blog_sidebar_layout' )) ? lifeline2_set( $settings, 'blog_sidebar_layout' ) : 'right';
    $page_title  = (lifeline2_set( $settings, 'blog_page_title' )) ? lifeline2_set( $settings, 'blog_page_title' ) : 'Blog';
    $top_section = lifeline2_set( $settings, 'blog_page_title_section' );
    $background  = (lifeline2_set( lifeline2_set( $settings, 'title_section_bg' ), 'background-image' )) ? 'style="background:url(' . lifeline2_set( lifeline2_set( $settings, 'title_section_bg' ), 'background-image' ) . ') no-repeat scroll 0 0 / 100% 100%"' : '';
	//$breadcrumb_section = lifeline2_set( $settings, 'blog_page_breadcrumb' );
} else {
    $page_meta   = lifeline2_Common::lifeline2_post_data( $queried_object->ID, 'page' );
    $lifeline2_sidebar     = ($wp_query->is_posts_page) ? lifeline2_set( $page_meta, 'metaSidebar' ) : lifeline2_set( $page_meta, 'metaSidebar', 'primary_widget_area' );
    $layout      = ($wp_query->is_posts_page) ? lifeline2_set( $page_meta, 'layout' ) : sh_set( $page_meta, 'layout' );
    $page_title  = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : $queried_object->post_title;
    $background  = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
    $top_section = lifeline2_set( $page_meta, 'show_title_section' );
	//$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
}

$lifeline2_style       = (lifeline2_set( $settings, 'blog_listing_style' )) ? lifeline2_set( $settings, 'blog_listing_style' ) : 'list';
$lifeline2_limit       = (lifeline2_set( $settings, 'character_limit' )) ? lifeline2_set( $settings, 'character_limit' ) : 100;
$lifeline2_title_limit = (lifeline2_set( $settings, 'title_character_limit' )) ? lifeline2_set( $settings, 'title_character_limit' ) : -1;
$lifeline2_author      = lifeline2_set( $settings, 'blog_post_author' );
$lifeline2_date        = lifeline2_set( $settings, 'blog_post_date' );
$lifeline2_more_btn    = lifeline2_set( $settings, 'post_read_more' ); //printr($more);
$lifeline2_label       = lifeline2_set( $settings, 'post_read_more_label' );
$lifeline2_tags        = lifeline2_set( $settings, 'blog_post_tags' );
if ( $top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, true ) );
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $lifeline2_sidebar && $layout == 'left' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo (($layout == 'left' || $layout == 'right') && $lifeline2_style != 'grid_3_cols') ? 'col-md-9' : 'col-md-12'; ?> column">
                    <div class="blog-list<?php echo ($lifeline2_style == 'list') ? ' list-style' : ''; ?>">
                        <div class="row">

                            <?php
                            echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '<div class="col-md-12"><div class="masonary">' : '';
                            if ( have_posts() ) {
                                while ( have_posts() ) {
                                    the_post();
                                    include(lifeline2_ROOT . 'post-format/content-standard.php');
                                }
                            }
                            echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '</div></div>' : '';
                            ?>

                        </div>
                    </div><!-- Blog List -->
                    <?php
                    lifeline2_Common::lifeline2_pagination();
                    ?>
                </div>
                <?php if ( $lifeline2_sidebar && $layout == 'right' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
    wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
?>
<?php get_footer(); ?>
