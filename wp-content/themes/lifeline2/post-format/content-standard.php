<?php
$archive_year  = get_the_time( 'Y' );
$archive_month = get_the_time( 'm' );
$archive_day   = get_the_time( 'd' );
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<?php if ( $lifeline2_style == 'grid_3_cols' || $lifeline2_style == 'grid_2_cols' ): ?>
    <div class="<?php echo ($lifeline2_style == 'grid_3_cols') ? 'col-md-4' : 'col-md-6'; ?>">
        <div itemtype="http://schema.org/BlogPosting" itemscope="" <?php post_class( 'blog-post' ); ?>>
            <div class="blog-img">
                <span>
                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                    <?php else: ?>
                        <?php the_post_thumbnail('full'); ?>
                    <?php endif; ?>
                    <a title="" href="<?php echo esc_url( get_permalink() ); ?>">+</a>
                </span>
            </div>
            <div class="blog-detail">
                <h3 itemprop="headline"><a title="" href="<?php echo esc_url( get_permalink() ); ?>" itemprop="url"><?php echo esc_html( lifeline2_Common::lifeline2_contents( get_the_title(), $lifeline2_title_limit ) ); ?></a></h3>
                <?php if ( $lifeline2_date || $lifeline2_author ): ?>
                    <ul class="meta">
                        <?php if ( $lifeline2_date ): ?>
                            <li itemprop="datePublished" content="<?php echo get_the_date( get_option( 'date_format', get_the_ID() ) ); ?>"><i class="fa fa-calendar"></i> <a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day ); ?>"><?php echo get_the_date( get_option( 'date_format', get_the_ID() ) ); ?></a></li>
                        <?php endif; ?>
                        <?php if ( $lifeline2_author ): ?>
                            <li><i class="fa fa-user"></i> <?php esc_html_e( 'By', 'lifeline2' ); ?> <a title="<?php ucwords( the_author_meta( 'display_name' ) ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" itemprop="url"><?php ucwords( the_author_meta( 'display_name' ) ); ?></a></li>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div><!-- Blog Post -->
    </div>
<?php else: ?>
    <div itemtype="http://schema.org/BlogPosting" itemscope="" <?php post_class( 'post-listview' ); ?>>
        <?php
        $grid_col = (has_post_thumbnail()) ? 'col-md-6' : 'col-md-12';
        if ( has_post_thumbnail() ) {
            ?>
            <div class="col-md-6">
                <div class="post-img">
                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                    <?php else: ?>
                        <?php the_post_thumbnail('full'); ?>
                    <?php endif; ?>
                    <?php if ( $lifeline2_more_btn || $lifeline2_date ): ?>
                        <span content="<?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?>" itemprop="datePublished">
                            <?php if ( $lifeline2_date ): ?>
                                <i class="fa fa-calendar-o"></i><span><?php echo get_the_date( get_option( 'date_format', get_the_ID() ) ); ?></span>
                            <?php endif; ?>
                            <?php if ( $lifeline2_more_btn ): ?>
                            <?php
                                $opt = lifeline2_get_theme_options();
                                $label = lifeline2_set($opt, 'tag_post_read_more_label');
                                ?>

                                <a title="<?php esc_attr( get_the_title() ); ?>" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" itemprop="url"><?php echo esc_html( $label ); ?></a>
                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                </div><!-- Post Image -->
            </div>
        <?php } ?>
        <div class="<?php echo esc_attr( $grid_col ) ?>">
            <div class="post-detail">
                <h3 itemprop="headline">
                    <a title="" href="<?php echo esc_url( get_permalink() ); ?>" itemprop="url">
                        <?php echo esc_html( lifeline2_Common::lifeline2_contents( get_the_title(), $lifeline2_title_limit ) ); ?>
                    </a>
                </h3>
                <?php if ( $lifeline2_author || $lifeline2_tags ): ?>
                    <ul class="meta">
                        <?php if ( $lifeline2_author ): ?>
                            <li><i class="fa fa-user"></i> <?php esc_html_e( 'By', 'lifeline2' ); ?> <a title="<?php ucwords( the_author_meta( 'display_name' ) ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" itemprop="url"><?php ucwords( the_author_meta( 'display_name' ) ); ?></a></li>
                            <?php
                        endif;
                        if ( $grid_col == 'col-md-12' && get_the_title() == '' ):
                            ?>
                            <li><i class="fa fa-calendar-o"></i><a href="<?php the_permalink() ?>"><?php echo get_the_date( get_option( 'date_format', get_the_ID() ) ); ?></a></li>
                            <?php
                        endif;
                        if ( $lifeline2_tags && has_tag() ):
                            ?>
                            <li><i class="fa fa-tags"></i> <?php the_tags( '', ',' ) ?></li>
                            <?php endif; ?>
                    </ul>
                <?php endif; ?>
                <p itemprop="description">
                    <?php echo esc_html( lifeline2_Common::lifeline2_character_limiter( get_the_excerpt(), $lifeline2_limit, '' ) ); ?>
                </p>
            </div><!-- Post Detail -->
        </div>
    </div>
<?php endif; ?>
