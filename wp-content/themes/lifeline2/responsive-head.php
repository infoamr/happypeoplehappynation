<div class="responsive-header">
    <?php if ( $topbar == 1 ): ?>
        <div class="responsive-topbar">
            <div class="container">
                <div class="row">
                    <?php if ( $style == 'style1' && lifeline2_set( $opt, 'top_bar_styles1_menu' ) == 1 ): ?>
                        <div class="responsive-secondary-menu">
                            <?php
                            //add_filter('nav_menu_link_attributes', 'wpse183311_filter', 3, 10);
                            wp_nav_menu( array( 'theme_location' => 'topbar-menu', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>' ) );
                            ?>
                        </div>
                    <?php elseif ( $style == 'style2' && lifeline2_set( $opt, 'top_bar_styles2_news_ticker' ) == 1 ): ?>
                        <?php
                        $args    = array(
                            'post_type'      => 'post',
                            'posts_per_page' => lifeline2_set( $opt, 'ticker_posts' ),
                            'cat'            => lifeline2_set( $opt, 'news_ticker_cat' ),
                        );
                        
                        $counter = 0;
                        query_posts( $args );
                        ?>
                        <div class = "recent-news">
                            <strong><?php echo strtoupper( lifeline2_set( $opt, 'ticker_title' ) ); ?></strong>
                            <div class="responsive-news-carousel" itemscope itemtype="http://schema.org/Article">
                                <?php
                                if ( have_posts() ):
                                    while ( have_posts() ):
                                        the_post();
                                        $date = (lifeline2_set( $opt, 'news_ticker_date' ) != 'time_ago') ? get_the_date( 'd M Y', get_the_ID() ) : lifeline2_Common::lifeline2_x_time( get_the_date( 'Y/m/d h:i:s', get_the_ID() ) );
                                        ?>
                                        <div class="news-bar">
                                            <span itemprop="datePublished"><?php echo esc_html( $date ) ?></span>
                                            <p itemprop="description"><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content(), 80 ) ) ?>
                                                <a itemprop="headline" itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a>
                                            </p>
                                        </div>
                                        <?php
                                        $counter++;
                                    endwhile;
                                    wp_reset_query();
                                endif;
                                lifeline2_View::get_instance()->lifeline2_enqueue( array( 'lifeline2_' . 'carousel' ) );
                                $loop     = ($counter > 1) ? 'true' : 'false';
                                ?>
                            </div>
                            <?php
                            $jsOutput = "jQuery(document).ready(function ($) {
                                    $('.news-carousel').owlCarousel({
                                        autoplay: true,
                                        autoplayTimeout: 2500,
                                        smartSpeed: 2000,
                                        loop:".esc_js($loop).",
                                        dots: false,
                                        nav: false,
                                        margin: 0,
                                        singleItem: true,
                                        mouseDrag: true,
                                        items: 1,
                                        autoHeight: true,
                                        animateIn: 'fadeIn',
                                        animateOut: 'fadeOut'
                                    });
                                });";
                            wp_add_inline_script( 'lifeline2_' . 'owl_carousel', $jsOutput );
                            ?>
                        </div>
                    <?php endif; ?>
                    <?php if ( $style == 'style1' && lifeline2_set( $opt, 'top_bar_styles1_seen_at' ) == 1 ): ?>
                        <div class="seen-at">
                            <span><?php esc_html_e( 'We Are Seen At:', 'lifeline2' ) ?></span>
                            <?php
                            $params  = array( 'width' => 89, 'height' => 21 );
                            $seen_at = lifeline2_Common::lifeline2_opt_array( 'sh_affiliates_data', 'sh_affiliates_data' );
                            if ( $seen_at ) {
                                foreach ( $seen_at as $at ) {
                                    if ( lifeline2_set( $at, 'tocopy' ) ) break;
                                    echo '<a itemprop="url" href="' . esc_url( lifeline2_set( $at, 'affiliates_link' ) ) . '" title="">
                                                            <img itemprop="image" src="' . esc_url( bfi_thumb( lifeline2_set( $at, 'affiliates_image' ), $params ) ) . '" alt="" />
                                                        </a>';
                                }
                            }
                            ?>
                        </div>
                    <?php elseif ( $style == 'style2' && lifeline2_set( $opt, 'top_bar_styles2_social' ) == 1 ): ?>
                        <ul class="social-boxes">
                            <?php
                            $social = lifeline2_Common::lifeline2_opt_array( 'social_media', 'social_media' );
                            if ( $social ) {
                                foreach ( $social as $s ) {
                                    if ( lifeline2_set( $s, 'tocopy' ) ) break;

                                    echo '<li><a itemprop="url" style="color:' . lifeline2_set( $s, 'icon_color_scheme' ) . ';" href="' . esc_url( lifeline2_set( $s, 'social_link' ) ) . '" title=""><i class="fa ' . lifeline2_set( $s, 'social_icon' ) . '"></i></a></li>';
                                }
                            }
                            ?>
                        </ul>
                    <?php endif;
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="responsive-menu">
        <span class="show-topbar"><i class="fa fa-angle-double-down"></i></span>
        <?php $Logo = '<img itemprop="image" src="' . esc_url( $logo ) . '" alt="" />'; ?>
        <div itemprop="logo" class="logo">
            <a itemprop="url" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>"><?php echo wp_kses( $Logo, true ); ?></a>
        </div>
        <span class="open-menu"><i class="fa fa-align-justify"></i></span>
        <div class="menu-links">

            <?php
//add_filter('nav_menu_link_attributes', 'wpse183311_filter', 3, 10);
            wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>', 'fallback_cb' => false, 'walker' => new lifeline2_Megamenu_walker ) );
            ?>

        </div>
    </div>
</div></div>