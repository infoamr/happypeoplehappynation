<form action="<?php echo home_url('/');?>" class="contact-form" method="get" role="search">
    <div class="row">
        <div class="col-md-12">
            <input type="text" placeholder="<?php esc_attr_e('Enter Search Item', 'lifeline2'); ?>" class="search" name="s" value="<?php echo get_search_query(); ?>">
        </div>
        <div class="col-md-12">
            <button type="submit"><?php esc_attr_e('Search', 'lifeline2');?></button>
        </div>
    </div>
</form>