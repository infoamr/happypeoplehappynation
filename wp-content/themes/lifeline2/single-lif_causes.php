<?php
get_header();
while (have_posts()) : the_post();
    $settings = lifeline2_get_theme_options();
    $page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes'); //printr($page_meta);
    $sidebar = (lifeline2_set($page_meta, 'metaSidebar')) ? lifeline2_set($page_meta, 'metaSidebar') : '';
    $layout = (lifeline2_set($page_meta, 'layout')) ? lifeline2_set($page_meta, 'layout') : '';
    $page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
    $background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
    $top_section = lifeline2_set($page_meta, 'show_title_section');
	$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
    $span = ($sidebar && ($layout == 'left' || $layout == 'right')) ? 'col-md-9' : 'col-md-12';
    $video_url = esc_url(get_post_meta(get_the_ID(), 'lifeline2_video_url', true));
    $video = lifeline2_Common::lifeline2_vd_details($video_url);
    $type = get_post_meta($post->ID, 'lifeline2_audio_type', true);
    $soundcloud = get_post_meta($post->ID, 'lifeline2_soundcloud_track_id', true);
    $own = get_post_meta($post->ID, 'lifeline2_own_audio', true);
    $gallery = get_post_meta(get_the_id(), 'lifeline2_gallery_images', true);
    $shrink = ($gallery != '') ? explode(',', $gallery) : '';
    $default = get_option('wp_donation_basic_settings', TRUE);
    $options = lifeline2_set($default, 'wp_donation_basic_settings');
    $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
    $baseAmt = (lifeline2_set($page_meta, 'donation_needed')) ? lifeline2_set($page_meta, 'donation_needed') : 0;
    $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
    if($cuurency_formate == 'select'):
        $donation_needed =$baseAmt;
    else:
        $donation_needed = ($baseAmt != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $baseAmt) : 0;
    endif;
    
    wp_enqueue_script(array('lifeline2_' . 'froogaloop', 'lifeline2_' . 'knob'));
    if ($top_section)
        echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, $breadcrumb_section, true));
    if ($sidebar && $layout == 'left')
        $sidebar_class = 'left-sidebar ';
    elseif ($sidebar && $layout == 'right')
        $sidebar_class = 'right-sidebar ';
    else
        $sidebar_class = '';
    if (class_exists('lifeline2_Resizer'))
        $img_obj = new lifeline2_Resizer();
    ?>

    <section itemscope itemtype="http://schema.org/BlogPosting">
        <div class="block gray">
            <div class="container">
                <div class="row">
                    <?php if ($sidebar && $layout == 'left' && is_active_sidebar($sidebar)) : ?>
                        <aside class="col-md-3 column sidebar">
        <?php dynamic_sidebar($sidebar); ?>
                        </aside>
    <?php endif; ?>
                    <div class="<?php echo esc_attr($sidebar_class); ?><?php echo esc_attr($span); ?> column">
                        <div class="blog-detail-page">

                            <div class="post-intro cause-intro">

                                    <?php if (lifeline2_set($page_meta, 'cause_format') == 'video'): ?>

                                    <div class="video">
                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); ?>
                                        <?php else: ?>
                                            <?php the_post_thumbnail('full'); ?>
        <?php endif; ?>
                                        <a itemprop="url" class="play" href="" title=""></a>
                                        <a itemprop="url" class="pause" href="" title=""></a>
                                    <?php echo balanceTags(lifeline2_set($page_meta, 'cause_video')); ?>
                                    </div>

                                    <?php elseif (lifeline2_set($page_meta, 'cause_format') == 'slider' || lifeline2_set($page_meta, 'cause_format') == 'gallery'): ?>
                                        <?php $images = lifeline2_set($page_meta, 'cause_images'); ?>
                                        <?php wp_enqueue_script(array('lifeline2_' . 'owl-carousel')); ?>

                                    <div class="image-carousel">
                                        <?php if ($images != ''): ?>
                                        <?php foreach ($images as $k => $v): ?>
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($k, 'full'), 1170, 531, true)); ?>
                                            <?php else: ?>
                <?php echo wp_get_attachment_image($k, 'full'); ?>
                                        <?php endif; ?> 

                                    <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>

                                    <?php
                                    $jsOutput = "jQuery(document).ready(function ($) {
                                            $('.image-carousel').owlCarousel({
                                                autoplay: true,
                                                autoplayTimeout: 2500,
                                                smartSpeed: 2000,
                                                autoplayHoverPause: true,
                                                loop: true,
                                                dots: false,
                                                nav: false,
                                                margin: 0,
                                                mouseDrag: true,
                                                items: 1,
                                                singleItem: true,
                                                autoHeight: true
                                            });
                                        });";
                                    wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
                                else:
                                    ?>
                                    <div class="post-thumb">
                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
        <?php endif; ?>

                                    </div>
                                        <?php endif; ?>

                                <div class="row">
                                    <div class="col-md-9">
                                            <?php
                                            if (lifeline2_set($settings, 'causes_detail_show_author') || lifeline2_set($settings, 'causes_detail_show_date') || lifeline2_set($settings, 'causes_detail_show_location')):
                                                ?>
                                            <ul class="meta">
                                                <?php if (lifeline2_set($settings, 'causes_detail_show_date')): ?>
                                                    <li content="<?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?></li>
                                                <?php endif; ?>
                                                <?php if (lifeline2_set($settings, 'causes_detail_show_location')): ?>
                                                    <li><i class="fa fa-map-marker"></i><?php echo esc_html(lifeline2_set($page_meta, 'location')) ?></li>
                                            <?php endif; ?>
                                            <?php if (lifeline2_set($settings, 'causes_detail_show_author')): ?>
                                                    <li itemprop="author"><i class="fa fa-user"></i> <?php esc_html_e('By', 'lifeline2'); ?> <a title="<?php ucwords(the_author_meta('display_name')); ?>" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" itemprop="url"><?php ucwords(the_author_meta('display_name')); ?></a></li>
                                            <?php endif; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <h1 class="post-title" itemprop="headline"><?php the_title(); ?></h1>
                                            <?php if (lifeline2_set($settings, 'causes_detail_show_social_share')): ?>
                                                <?php $social_icons = lifeline2_set($settings, 'causes_detail_social_media'); ?>
                                                <?php if (!empty($social_icons)): ?>
                                                <div class="share-this">
                                                    <?php echo (lifeline2_set($settings, 'causes_detail_show_social_share_title')) ? '<span>' . lifeline2_set($settings, 'causes_detail_show_social_share_title') . '</span>' : ''; ?>
                                                    <?php
                                                    foreach ($social_icons as $k => $v) {
                                                        if ($v == '')
                                                            continue;
                                                        lifeline2_Common::lifeline2_social_share_output($k);
                                                    }
                                                    ?>

                                                </div><!-- Share This -->
        <?php endif; ?>
    <?php endif; ?>
                                    </div>
                                                <?php if (lifeline2_set($settings, 'causes_detail_show_donation') == 1): ?>
                                        <div class="col-md-3">
                                            <div class="cause-detail">
                                                <span><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html($donation_needed) ?></span>
                                                <strong>
                                                    <?php
                                                    esc_html_e('Needed Donation', 'lifeline2');
                                                    if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                        $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                        $queryParams = array('data_donation' => 'causes', 'postId' => get_the_id());
                                                        ?>
                                                        <a itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                        <?php esc_html_e('Donate Now', 'lifeline2') ?>
                                                        </a>
                                                        <?php
                                                    elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                        $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                        ?>
                                                        <a itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title=""><?php esc_html_e('Donate Now', 'lifeline2') ?></a>
                                                            <?php
                                                        else:
                                                            ?>
                                                        <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                                            <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="donation-modal-box-caller" href="javascript:void(0)" title="">
                                                            <?php esc_html_e('Donate Now', 'lifeline2') ?>
                                                            </a>
            <?php endif; ?>
                                        <?php
                                        endif;
                                        ?>
                                                </strong>
                                            </div>
                                        </div>
                    <?php endif ?>
                                </div>
                            </div>
                        <?php the_content(); ?>
                        </div><!-- Blog Detail -->
                    </div>
    <?php if ($sidebar && $layout == 'right' && is_active_sidebar($sidebar)) : ?>
                        <aside class="col-md-3 column sidebar">
        <?php dynamic_sidebar($sidebar); ?>
                        </aside>
    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;
get_footer();
