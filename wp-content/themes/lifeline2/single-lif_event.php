<?php get_header(); ?>
<?php
$settings = lifeline2_get_theme_options(); //printr($settings);
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'event');
$sidebar = (lifeline2_set($page_meta, 'metaSidebar')) ? lifeline2_set($page_meta, 'metaSidebar') : '';
$layout = (lifeline2_set($page_meta, 'layout')) ? lifeline2_set($page_meta, 'layout') : 'right';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
$top_section = lifeline2_set($page_meta, 'show_title_section');
$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
$sponsers = get_post_meta(get_the_ID(), 'sponsers', true);
if ($top_section)
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, $breadcrumb_section, true));
if ($sidebar && $layout == 'left')
    $sidebar_class = 'left-sidebar ';
elseif ($sidebar && $layout == 'right')
    $sidebar_class = 'right-sidebar ';
else
    $sidebar_class = '';
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section itemscope itemtype="http://schema.org/BlogPosting">
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $layout == 'left' && is_active_sidebar($sidebar)) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo esc_attr($sidebar_class); ?><?php echo ($layout == 'left' || $layout == 'right') ? 'col-md-9' : 'col-md-12'; ?> column">
                    <div class="blog-detail-page">
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="post-intro">
                                <div class="post-thumb">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                </div>
                                <?php if (lifeline2_set($settings, 'event_detail_show_date') == 1 || lifeline2_set($settings, 'event_detail_show_location') == 1 || lifeline2_set($settings, 'event_detail_show_author') == 1): ?>
                                    <ul class="meta">
                                        <?php if (lifeline2_set($settings, 'event_detail_show_date') == 1): ?><li itemprop="startDate" content="<?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?>"><i class="ti-calendar"></i><?php echo date('F j, Y h:i a', lifeline2_set($page_meta, 'start_date')); ?> - <?php echo date('F j, Y h:i a', lifeline2_set($page_meta, 'end_date')); ?></li><?php endif; ?>
                                        <?php if (lifeline2_set($settings, 'event_detail_show_location') == 1): ?>
                                            <li itemprop="location" itemscope itemtype="http://schema.org/Place">
                                                <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="ti-location-pin"></i> <span itemprop="streetAddress"><?php echo esc_html(lifeline2_set($page_meta, 'location')); ?></span></span>
                                            </li>
                                        <?php endif; ?>
                                        <?php if (lifeline2_set($settings, 'event_detail_show_author') == 1): ?><li><i class="ti-user"></i> <?php esc_html_e('By ', 'lifeline2'); ?><a title="<?php ucwords(the_author_meta('display_name')); ?>" itemprop="url" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php ucwords(the_author_meta('display_name')); ?></a></li><?php endif; ?>
                                    </ul>
                                <?php endif; ?>
                                <h1 class="post-title" itemprop="headline"><?php the_title(); ?></h1>

                                <?php if (lifeline2_set($settings, 'event_detail_show_social_share')): ?>
                                    <?php $social_icons = lifeline2_set($settings, 'event_detail_social_media'); ?>
                                    <?php if (!empty($social_icons)): ?>
                                        <div class="share-this">
                                            <?php echo (lifeline2_set($settings, 'event_detail_show_social_share_title')) ? '<span>' . lifeline2_set($settings, 'event_detail_show_social_share_title') . '</span>' : ''; ?>
                                            <?php
                                            foreach ($social_icons as $k => $v) {
                                                if ($v == '')
                                                    continue;
                                                lifeline2_Common::lifeline2_social_share_output($k);
                                            }
                                            ?>

                                        </div><!-- Share This -->
                                    <?php endif; ?>
                                    <?php
                                endif;
                                if (lifeline2_set($settings, 'event_detail_show_org_info')):
                                    ?>
                                    <div class="event-org">
                                        <?php echo (lifeline2_set($settings, 'organizer_info_section_label')) ? '<h4>' . balanceTags(lifeline2_set($settings, 'organizer_info_section_label')) . '</h4>' : ''; ?>
                                        <ul>
                                            <?php echo (lifeline2_set($page_meta, 'event_organizer_name')) ? '<li><i class="ti-user"></i> ' . esc_html(lifeline2_set($page_meta, 'event_organizer_name')) . '</li>' : ''; ?>
                                            <?php echo (lifeline2_set($page_meta, 'event_organizer_number')) ? '<li><i class="ti-mobile"></i> ' . esc_html(lifeline2_set($page_meta, 'event_organizer_number')) . '</li>' : ''; ?>
                                            <?php echo (lifeline2_set($page_meta, 'event_organizer_website')) ? '<li><i class="ti-world"></i> ' . esc_html(lifeline2_set($page_meta, 'event_organizer_website')) . '</li>' : ''; ?>
                                        </ul>
                                    </div><!-- Event Organizer -->
                                <?php endif; ?>
                            </div><!-- Post Intro -->

                            <?php the_content(); ?>

                        <?php endwhile; ?>
                        <?php if (lifeline2_set($settings, 'events_comments')): ?>
                            <?php comments_template(); ?>
                        <?php endif; ?>

                    </div><!-- Blog Detail -->
                    <?php if ($sponsers): ?>                    
                        <div class="block    ">
                                <div class="row">                                   
                                        <div class="sponsors-carousel sponsors-carousel-event">
                                            <?php foreach ($sponsers as $sponser): ?>                        
                                                <div class="sponsor"><a itemprop="url" href="<?php echo esc_url(lifeline2_set($sponser, 'sponser_link')); ?>" title="<?php the_title(); ?>">
                                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(lifeline2_set($sponser, 'brand_image_id'), 'full'), 159, 51, true)); ?>
                                                        <?php else: ?>
                                                            <?php echo wp_get_attachment_image(lifeline2_set($sponser, 'brand_image_id'), 'full'); ?>
                                                        <?php endif; ?></a></div>
                                            <?php endforeach; ?>
                                        </div>                                    
                                </div>
                        </div>
                        <?php
                        wp_enqueue_script('lifeline2_' . 'owl-carousel');
                        $jsOutput = 'jQuery(document).ready(function( $ ) {
                $(".sponsors-carousel-event").owlCarousel({
                    autoplay:true,
                    autoplayTimeout:2500,
                    smartSpeed:2000,
                    loop:true,
                    dots:false,
                    nav:true,
                    margin:10,
                    mouseDrag:true,
                    autoHeight:true,
                    autoplayHoverPause:true,
                    items:5,
                    responsive : {0 : {items : 1},
                    480 : {items : 1 },
                    768 : {items : 3 },
                     768 : {items : 3 },
                    1200: {items : 4 },';
                        $jsOutput .='}});});';
                        wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
                    endif;

                    $init_script = '
            jQuery(document).ready(function($) {
                $(\'.owl-carousel .owl-item\').on(\'mouseenter\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'stop.owl.autoplay\');
                })
                $(\'.owl-carousel .owl-item\').on(\'mouseleave\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'play.owl.autoplay\', [500]);
                });
            });';

                    wp_add_inline_script( 'lifeline2_owl-carousel', $init_script );
                    ?>

                </div>
                <?php if ($sidebar && $layout == 'right' && is_active_sidebar($sidebar)) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
