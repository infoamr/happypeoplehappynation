<?php
get_header();
$settings = lifeline2_get_theme_options();
while ( have_posts() ) : the_post();
    $page_meta   = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' );
    $sidebar     = (lifeline2_set( $page_meta, 'metaSidebar' )) ? lifeline2_set( $page_meta, 'metaSidebar' ) : '';
    $layout      = (lifeline2_set( $page_meta, 'layout' )) ? lifeline2_set( $page_meta, 'layout' ) : '';
    $page_title  = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
    $background  = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
    $top_section = lifeline2_set( $page_meta, 'show_title_section' );
	$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
    $span        = ($sidebar && ($layout == 'left' || $layout == 'right')) ? 'col-md-9' : 'col-md-12';

    $show_service = (lifeline2_set( $settings, 'service_detail_show_services' )) ? 'col-md-7' : 'col-md-12';
    $service      = get_post_meta( get_the_ID(), 'service_features', true );

    $exp_sbTitle = explode( ' ', lifeline2_set( $page_meta, 'feature_title' ), 2 );

    if ( $top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );

    ?>
    <section itemscope itemtype="http://schema.org/BlogPosting">
        <div class="block gray">
            <div class="container">
                <div class="row">
                    <?php if ( $sidebar && $layout == 'left' && is_active_sidebar( $sidebar ) ) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    <?php endif; ?>
                    <div class="<?php echo esc_attr( $span ); ?> column">
                        <div class="service-detail-page">
                            <h2><?php the_title() ?></h2>
                            <div class="service-detail-img">
                                <?php the_post_thumbnail( 'full' ) ?>
                                <span>
                                    <i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $page_meta, 'serive_icon' ) ) ) . esc_attr( lifeline2_set( $page_meta, 'serive_icon' ) ) ?>"></i>
                                </span>
                            </div>
                            <div class="service-description">
                                <div class="row">
                                    <?php if ( lifeline2_set( $settings, 'service_detail_show_services' ) && count( $service ) > 0 ): ?>
                                        <div class="col-md-5">
                                            <div class="service-features-wrapper">
                                                <span><?php echo esc_html( lifeline2_set( $page_meta, 'feature_title_tag' ) ) ?></span>
                                                <h3><?php echo esc_html( lifeline2_set( $exp_sbTitle, '0' ) ) ?> <span><?php echo esc_html( lifeline2_set( $exp_sbTitle, '1' ) ) ?></span></h3>
                                                <div class="all-skills">
                                                    <?php if(!empty($service) && count($service) >0){
														foreach ( $service as $s ): ?>
                                                        <div class="skills">
                                                            <span><i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $s, 'feature_icon' ) ) ) . esc_attr( lifeline2_set( $s, 'feature_icon' ) ) ?>"></i></span>
                                                            <h5><?php echo esc_html( lifeline2_set( $s, 'service_title' ) ) ?></h5>
                                                            <p><?php echo esc_html( lifeline2_set( $s, 'service_description' ) ) ?></p>
                                                        </div>
                                                    <?php 
													endforeach; 
													}
													?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="<?php echo esc_attr( $show_service ) ?>">
                                        <?php the_content() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ( $sidebar && $layout == 'right' && is_active_sidebar( $sidebar ) ) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<?php if ( lifeline2_set( $settings, 'related_services_section' ) ): ?>
    <section>
        <div class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 column">
                        <?php if ( lifeline2_set( $settings, 'related_service_title' ) || lifeline2_set( $settings, 'related_service_description' ) ): ?>
                            <div class="title">
                                <?php echo (lifeline2_set( $settings, 'related_service_title' )) ? '<h2>' . lifeline2_set( $settings, 'related_service_title' ) . '</h2>' : ''; ?>
                                <?php echo (lifeline2_set( $settings, 'related_service_description' )) ? '<span>' . lifeline2_set( $settings, 'related_service_description' ) . '</span>' : ''; ?>
                            </div>
                        <?php endif; ?>
                        <div class="all-skills">
                            <div class="row">
                                <?php lifeline2_Common::lifeline2_realted_services( get_the_ID(), lifeline2_set( $settings, 'related_service_limit' ), lifeline2_set( $settings, 'related_service_num' ) ); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>
