<?php
get_header();
while (have_posts()) : the_post();
    $settings = lifeline2_get_theme_options();
    $page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'story');
    $sidebar = (lifeline2_set($page_meta, 'metaSidebar')) ? lifeline2_set($page_meta, 'metaSidebar') : '';
    $layout = (lifeline2_set($page_meta, 'layout')) ? lifeline2_set($page_meta, 'layout') : '';
    $page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
    $background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
    $top_section = lifeline2_set($page_meta, 'show_title_section');
	$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
    $span = ($sidebar && ($layout == 'left' || $layout == 'right')) ? 'col-md-9' : 'col-md-12';
    $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
    $money_title = lifeline2_set($page_meta, 'money_title');
	$project_cost = lifeline2_set($page_meta, 'project_cost');
    $baseAmt =  (lifeline2_set($page_meta, 'project_cost')) ? lifeline2_set($page_meta, 'project_cost') : 0;
    $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
    if ($cuurency_formate == 'select'):
        $donation_needed = $baseAmt;
    else:
        $donation_needed = ($baseAmt != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $baseAmt) : 0;
    endif;

    if ($top_section)
        echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, $breadcrumb_section, true));
    if ($sidebar && $layout == 'left')
        $sidebar_class = 'left-sidebar ';
    elseif ($sidebar && $layout == 'right')
        $sidebar_class = 'right-sidebar ';
    else
        $sidebar_class = '';
    if (class_exists('lifeline2_Resizer'))
        $img_obj = new lifeline2_Resizer();
    ?>
    <section itemscope itemtype="http://schema.org/BlogPosting">
        <div class="block gray">
            <div class="container">
                <div class="row">
                    <?php if ($sidebar && $layout == 'left' && is_active_sidebar($sidebar)) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar($sidebar); ?>
                        </aside>
                    <?php endif; ?>
                    <div class="<?php echo esc_attr($sidebar_class); ?><?php echo esc_attr($span); ?> column">
                        <div class="blog-detail-page">

                            <div class="post-intro cause-intro">
                                <div class="post-thumb">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <?php
                                        if (lifeline2_set($settings, 'story_detail_show_author') || lifeline2_set($settings, 'story_detail_show_date') || lifeline2_set($settings, 'story_detail_show_location')):
                                            ?>
                                            <ul class="meta">
                                                <?php if (lifeline2_set($settings, 'story_detail_show_date')): ?>
                                                    <li content="<?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?></li>
                                                <?php endif; ?>
                                                <?php if (lifeline2_set($settings, 'story_detail_show_location')): ?>
                                                    <li><i class="ti-location-pin"></i> <?php echo esc_html(lifeline2_set($page_meta, 'location')) ?></li>
                                                <?php endif; ?>
                                                <?php if (lifeline2_set($settings, 'story_detail_show_author')): ?>
                                                    <li itemprop="author"><i class="fa fa-user"></i> <?php esc_html_e('By', 'lifeline2'); ?> <a title="<?php ucwords(the_author_meta('display_name')); ?>" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" itemprop="url"><?php ucwords(the_author_meta('display_name')); ?></a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <h1 class="post-title" itemprop="headline"><?php the_title(); ?></h1>
                                        <?php if (lifeline2_set($settings, 'story_detail_show_social_share')): ?>
                                            <?php $social_icons = lifeline2_set($settings, 'story_detail_social_media'); ?>
                                            <?php if (!empty($social_icons)): ?>
                                                <div class="share-this">
                                                    <?php echo (lifeline2_set($settings, 'story_detail_show_social_share_title')) ? '<span>' . lifeline2_set($settings, 'project_detail_show_social_share_title') . '</span>' : ''; ?>
                                                    <?php
                                                    foreach ($social_icons as $k => $v) {
                                                        if ($v == '')
                                                            continue;
                                                        lifeline2_Common::lifeline2_social_share_output($k);
                                                    }
                                                    ?>

                                                </div><!-- Share This -->
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (lifeline2_set($settings, 'story_detail_show_donation') == 1): ?>
                                        <div class="col-md-3">
                                            <div class="cause-detail">
                                            <?php if(!empty($project_cost)): ?>
                                                <span><i><?php echo esc_html($symbol) ?></i> 
                                                    <?php echo esc_html($donation_needed) ?>
                                                </span>
                                            <?php endif; ?>
                                            <?php if(!empty($money_title)): ?>
                                                <strong><?php esc_html_e($money_title) ?></strong>
                                            <?php endif; ?>    
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                            <?php the_content(); ?>
                        </div><!-- Blog Detail -->
                    </div>
                    <?php if ($sidebar && $layout == 'right' && is_active_sidebar($sidebar)) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar($sidebar); ?>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;
get_footer();
