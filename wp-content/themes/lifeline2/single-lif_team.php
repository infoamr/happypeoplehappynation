<?php get_header(); ?>
<?php
$settings      = lifeline2_get_theme_options();
$page_meta     = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'team' );
$sidebar       = (lifeline2_set( $page_meta, 'metaSidebar' )) ? lifeline2_set( $page_meta, 'metaSidebar' ) : '';
$layout        = (lifeline2_set( $page_meta, 'layout' )) ? lifeline2_set( $page_meta, 'layout' ) : '';
$page_title    = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$background    = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$top_section   = lifeline2_set( $page_meta, 'show_title_section' );
$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
$social_icons  = get_post_meta( get_the_ID(), 'social_media_icons', true );
$member_skills = get_post_meta( get_the_ID(), 'member_skills', true );
if ( $top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section itemscope itemtype="http://schema.org/BlogPosting">
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $layout == 'left' && is_active_sidebar( $sidebar ) ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo ($layout == 'left' || $layout == 'right' && !empty( $sidebar )) ? 'col-md-9' : 'col-md-12'; ?> column">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="team-detail">
                            <div class="col-md-6 column">
                                <div id="team-detail-img">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 570, 644, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                    
                                    <?php if ( (lifeline2_set( $settings, 'team_member_detail_show_email' ) || lifeline2_set( $settings, 'team_member_detail_show_address' ) || lifeline2_set( $settings, 'team_member_detail_show_phone' ) ) ): ?>
                                        <ul>
                                            <?php echo (lifeline2_set( $page_meta, 'email' ) && lifeline2_set( $settings, 'team_member_detail_show_email' )) ? '<li><span><i class="ti-email"></i></span><p><i>' . esc_html__( 'Email:', 'lifeline2' ) . '</i> ' . lifeline2_set( $page_meta, 'email' ) . '</p></li>' : ''; ?>
                                            <?php echo (lifeline2_set( $page_meta, 'address' ) && lifeline2_set( $settings, 'team_member_detail_show_address' )) ? '<li><span><i class="ti-location-pin"></i></span><p><i>' . esc_html__( 'Address:', 'lifeline2' ) . '</i> ' . lifeline2_set( $page_meta, 'address' ) . '</p></li>' : ''; ?>
                                            <?php echo (lifeline2_set( $page_meta, 'phone' ) && lifeline2_set( $settings, 'team_member_detail_show_phone' )) ? '<li><span><i class="ti-mobile"></i></span><p><i>' . esc_html__( 'Phone:', 'lifeline2' ) . '</i> ' . lifeline2_set( $page_meta, 'phone' ) . '</p></li>' : ''; ?>
                                            <?php if ( lifeline2_set( $settings, 'team_member_cat_show_cat' ) ): ?>
                                                <li>
                                                    <span><i class="ti-list"></i></span><p><i><?php esc_html_e( 'Category', 'lifeline2' ) ?>:</i> <?php lifeline2_get_terms( 'team_category', 2, 'a', true, '' ) ?></p>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <?php if ( !empty( $social_icons ) && lifeline2_set( $settings, 'team_member_detail_show_social_media' ) ): ?>
                                        <div class="member-social">
                                            <?php echo (lifeline2_set( $settings, 'team_member_detail_show_social_media' )) ? '<span>' . lifeline2_set( $settings, 'social_media_section_title' ) . '</span>' : ''; ?>
                                            <div class="social-links">
                                                <?php foreach ( $social_icons as $s_icon ): ?>
                                                    <a itemprop="url" title="<?php echo esc_url( lifeline2_set( $s_icon, 'social_title' ) ); ?>" href="<?php echo esc_url( lifeline2_set( $s_icon, 'social_link' ) ); ?>" target="_blank"><i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $s_icon, 'social_icon' ) ) ) . lifeline2_set( $s_icon, 'social_icon' ); ?>"></i></a>
                                                <?php endforeach; ?>
                                            </div>
                                        </div><!-- Members -->
                                    <?php endif; ?>
                                </div><!-- Team Detail Image -->
                            </div>
                            <div class="col-md-6 column">
                                <div class="team-desc">
                                    <?php the_content(); ?>
                                    <?php if ( !empty( $member_skills ) && lifeline2_set( $settings, 'team_member_skills' ) ): ?>
                                        <div class="all-skills">
                                            <?php foreach ( $member_skills as $m_skills ): ?>
                                                <div class="skills">
                                                    <?php echo (lifeline2_set( $m_skills, 'skill_icon' )) ? '<span><i class="' . lifeline2_Header::lifeline2_get_icon( lifeline2_set( $m_skills, 'skill_icon' ) ) . lifeline2_set( $m_skills, 'skill_icon' ) . '"></i></span>' : ''; ?>
                                                    <?php echo (lifeline2_set( $m_skills, 'skill_title' )) ? '<h5>' . lifeline2_set( $m_skills, 'skill_title' ) . '</h5>' : ''; ?>
                                                    <?php echo (lifeline2_set( $m_skills, 'skill_description' )) ? '<p>' . lifeline2_set( $m_skills, 'skill_description' ) . '</p>' : ''; ?>
                                                </div><!-- Skills -->
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div><!-- Team Description -->
                            </div>
                        </div><!-- Team Detail -->
                    <?php endwhile; ?>
                    <?php if ( lifeline2_set( $settings, 'join_us_section' ) ): ?>
                        <section>
                            <div class="block remove-bottom gray">
                                <div class="row">
                                    <div class="col-md-12 column">
                                        <div class="join-team">
                                            <?php echo (lifeline2_set( $settings, 'join_us_section_title' )) ? '<h4>' . esc_html( lifeline2_set( $settings, 'join_us_section_title' ) ) . '</h4>' : ''; ?>
                                            <?php echo (lifeline2_set( $settings, 'join_us_section_description' )) ? '<p>' . esc_html( lifeline2_set( $settings, 'join_us_section_description' ) ) . '</p>' : ''; ?>
                                            <?php echo (lifeline2_set( $settings, 'join_us_button' ) ) ? '<a itemprop="url" title="" href="#" class="theme-btn signup-btn">' . esc_html( lifeline2_set( $settings, 'join_us_button_label' ) ) . '</a>' : ''; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php endif; ?>
                </div>
                <?php if ( $sidebar && $layout == 'right' && !empty( $sidebar ) && is_active_sidebar( $sidebar ) ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
