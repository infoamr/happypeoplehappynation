<?php get_header(); ?>
<?php
$lifeline2_settings      = lifeline2_get_theme_options();
$lifeline2_page_meta     = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
$lifeline2_sidebar       = (lifeline2_set( $lifeline2_page_meta, 'metaSidebar' )) ? lifeline2_set( $lifeline2_page_meta, 'metaSidebar' ) : '';
$lifeline2_layout        = (lifeline2_set( $lifeline2_page_meta, 'layout' )) ? lifeline2_set( $lifeline2_page_meta, 'layout' ) : '';
$lifeline2_page_title    = (lifeline2_set( $lifeline2_page_meta, 'banner_title' )) ? lifeline2_set( $lifeline2_page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$lifeline2_background    = (lifeline2_set( $lifeline2_page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $lifeline2_page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$lifeline2_top_section   = lifeline2_set( $lifeline2_page_meta, 'show_title_section' );
$breadcrumb_section = lifeline2_set( $lifeline2_page_meta, 'banner_breadcrumb' );
$lifeline2_span          = ($lifeline2_sidebar && ($lifeline2_layout == 'left' || $lifeline2_layout == 'right')) ? 'col-md-9' : 'col-md-12';
$lifeline2_video_url     = esc_url( get_post_meta( get_the_ID(), 'lifeline2_video_url', true ) );
$lifeline2_video         = lifeline2_Common::lifeline2_vd_details( $lifeline2_video_url );
$lifeline2_type          = get_post_meta( $post->ID, 'lifeline2_audio_type', true );
$lifeline2_soundcloud    = get_post_meta( $post->ID, 'lifeline2_soundcloud_track_id', true );
$lifeline2_own           = get_post_meta( $post->ID, 'lifeline2_own_audio', true );
$lifeline2_gallery       = get_post_meta( get_the_id(), 'lifeline2_gallery_images', true );
$lifeline2_shrink        = ($lifeline2_gallery != '') ? explode( ',', $lifeline2_gallery ) : '';
if ( $lifeline2_top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $lifeline2_page_title, $lifeline2_background, $breadcrumb_section, true ) );
if ( $lifeline2_sidebar && $lifeline2_layout == 'left' ) $lifeline2_sidebar_class = 'left-sidebar ';
elseif ( $lifeline2_sidebar && $lifeline2_layout == 'right' ) $lifeline2_sidebar_class = 'right-sidebar ';
else $lifeline2_sidebar_class = '';
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section itemscope itemtype="http://schema.org/BlogPosting">
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'left' && is_active_sidebar( $lifeline2_sidebar ) ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $lifeline2_sidebar_class ); ?><?php echo esc_attr( $lifeline2_span ); ?> column">
                    <div class="blog-detail-page">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <div class="post-intro">
                                <?php
                                $format = get_post_format( get_the_ID() );
                                if ( $format == 'video' ) {
                                    echo '<div class="post-audio">';
                                    $lifeline2_video = lifeline2_Common::lifeline2_vd_details( $lifeline2_video_url );
                                    echo balanceTags( lifeline2_set( $lifeline2_video, 'embed_video' ) );
                                    echo '</div>';
                                } elseif ( $format == 'audio' ) {
                                    if ( $lifeline2_type == 'SoundCloud' ):
                                        ?>
                                        <div class="post-audio">
                                            <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo esc_attr( $lifeline2_soundcloud ) ?>&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                        <?php elseif ( $lifeline2_type == 'Own Audio' ): wp_enqueue_script( array( 'wp-mediaelement' ) ) ?>
                                            <?php echo do_shortcode( '[audio src="' . esc_url( $lifeline2_own ) . '"]' ); ?>
                                        <?php endif; ?>

                                        <?php
                                    }elseif ( $format == 'gallery' ) {
                                        if ( $lifeline2_shrink ):
                                            echo '<div class="simple-blog-carousel">';
                                            foreach ( $lifeline2_shrink as $image ):
                                                if (class_exists('Lifeline2_Resizer')){
                                                    echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($image, 'full'), 1170, 531, true)); 
                                                }else{
                                                    echo wp_get_attachment_image( $image, 'full' );
                                                }
                                            endforeach;
                                            echo '</div>';
                                        endif;
                                        ?>
                                        <?php
                                        lifeline2_View::get_instance()->lifeline2_enqueue( array( 'lifeline2_' . 'owl-carousel' ) );

                                        $jsOutput = "jQuery(document).ready(function ($) {
        					$('.simple-blog-carousel').owlCarousel({
        					    autoplay: true,
        					    autoplayTimeout: 3000,
        					    smartSpeed: 2000,
        					    loop: true,
        					    dots: false,
        					    nav: true,
        					    margin: 0,
        					    singleItem: true,
        					    mouseDrag: true,
        					    items: 1,
        					    autoHeight: true,
        					    animateIn: 'fadeIn',
        					    animateOut: 'fadeOut'
        					});
        				    });";
                                        wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
                                    }else {
                                        echo '<div class="post-thumb">';
                                        if ( has_post_thumbnail() ) {
                                            if (class_exists('Lifeline2_Resizer')){
                                               echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 1170, 531, true)); 
                                            }else{
                                                the_post_thumbnail( 'full' );
                                            }
                                        }
                                        echo '</div>';
                                    }
                                    ?>
                                    <?php if ( lifeline2_set( $lifeline2_settings, 's_post_author' ) || lifeline2_set( $lifeline2_settings, 's_post_date' ) ): ?>
                                        <ul class="meta">
                                            <?php if ( lifeline2_set( $lifeline2_settings, 's_post_date' ) ): ?>
                                                <li content="<?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?></li>
                                            <?php endif; ?>
                                            <?php if ( lifeline2_set( $lifeline2_settings, 's_post_author' ) ): ?>
                                                <li itemprop="author"><i class="fa fa-user"></i> <?php esc_html_e( 'By', 'lifeline2' ); ?> <a title="<?php ucwords( the_author_meta( 'display_name' ) ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" itemprop="url"><?php ucwords( the_author_meta( 'display_name' ) ); ?></a></li>
                                            <?php endif; ?>
                                            <?php if ( lifeline2_set( $lifeline2_settings, 's_post_cat' ) ): ?>
                                                <li><i class="fa fa-list"></i><?php the_category( ', ' ); ?></li> 
                                            <?php endif; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <h1 class="post-title" itemprop="headline"><?php the_title(); ?></h1>
                                    <?php if ( lifeline2_set( $lifeline2_settings, 's_share_icons' ) ): ?>
                                        <?php $social_icons = lifeline2_set( $lifeline2_settings, 'single_post_social_share' ); ?>
                                        <?php if ( !empty( $social_icons ) ): ?>
                                            <div class="share-this">
                                                <?php echo (lifeline2_set( $lifeline2_settings, 'social_sharing_section_title' )) ? '<span>' . lifeline2_set( $lifeline2_settings, 'social_sharing_section_title' ) . '</span>' : ''; ?>
                                                <?php
                                                foreach ( $social_icons as $k => $v ) {
                                                    if ( $v == '' ) continue;
                                                    lifeline2_Common::lifeline2_social_share_output( $k );
                                                }
                                                ?>

                                            </div><!-- Share This -->
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div><!-- Post Intro -->

                                <?php the_content(); ?>
                                <?php wp_link_pages(); ?>
                                <?php if ( lifeline2_set( $lifeline2_settings, 's_post_tags' ) ): ?>
                                    <div class="tagclouds">
                                        <div class="heading"><h3><?php esc_html_e( 'Tags', 'lifeline2' ); ?> <span><?php esc_html_e( 'CLOUD', 'lifeline2' ); ?></span></h3></div>
                                        <?php echo balanceTags( lifeline2_Common::lifeline2_get_tags() ); ?>
                                    </div><!-- Tags -->
                                <?php endif; ?>
                            <?php endwhile; ?>

                            <?php comments_template(); ?>

                        </div><!-- Blog Detail -->
                    </div>
                    <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'right' && is_active_sidebar( $lifeline2_sidebar ) ) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
        </div>
</section>

<?php get_footer(); ?>