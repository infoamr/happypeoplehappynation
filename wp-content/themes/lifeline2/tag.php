<?php
get_header();
$lifeline2_settings    = lifeline2_get_theme_options();
$object                = get_queried_object();
$lifeline2_sidebar     = (lifeline2_set( $lifeline2_settings, 'tag_page_sidebar' )) ? lifeline2_set( $lifeline2_settings, 'tag_page_sidebar' ) : '';
$lifeline2_layout      = (lifeline2_set( $lifeline2_settings, 'tag_page_sidebar_layout' )) ? lifeline2_set( $lifeline2_settings, 'tag_page_sidebar_layout' ) : '';
$lifeline2_page_title  = lifeline2_set( $object, 'name' );
$lifeline2_top_section = lifeline2_set( $lifeline2_settings, 'tag_page_title_section' );
$lifeline2_background  = (lifeline2_set( lifeline2_set( $lifeline2_settings, 'tag_title_section_bg' ), 'background-image' )) ? 'style="background:url(' . lifeline2_set( lifeline2_set( $lifeline2_settings, 'tag_title_section_bg' ), 'background-image' ) . ') no-repeat scroll 0 0 / 100% 100%"' : '';

$lifeline2_style       = (lifeline2_set( $lifeline2_settings, 'tag_posts_listing_style' )) ? lifeline2_set( $lifeline2_settings, 'tag_posts_listing_style' ) : 'list';
$lifeline2_limit       = lifeline2_set( $lifeline2_settings, 'tag_page_posts_character_limit' );
$lifeline2_title_limit = (lifeline2_set( $lifeline2_settings, 'tag_title_character_limit' )) ? lifeline2_set( $lifeline2_settings, 'tag_title_character_limit' ) : 30;
$lifeline2_author      = lifeline2_set( $lifeline2_settings, 'tag_blog_post_author' );
$lifeline2_date        = lifeline2_set( $lifeline2_settings, 'tag_blog_post_date' );
$lifeline2_more_btn    = lifeline2_set( $lifeline2_settings, 'tag_post_read_more' ); //printr($more);
$lifeline2_label       = lifeline2_set( $lifeline2_settings, 'tag_post_read_more_label' );
$lifeline2_tags        = lifeline2_set( $lifeline2_settings, 'tag_blog_post_tags' );
if ( $lifeline2_top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $lifeline2_page_title, $lifeline2_background, true ) );
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'left' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo (($lifeline2_layout == 'left' || $lifeline2_layout == 'right') && $lifeline2_style != 'grid_3_cols') ? 'col-md-9' : 'col-md-12'; ?> column">
                    <div class="blog-list<?php echo ($lifeline2_style == 'list') ? ' list-style' : ''; ?>">
                        <div class="row">
                            <?php
                            echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '<div class="col-md-12">' : '';
                            if ( have_posts() ) {
                                while ( have_posts() ) {
                                    the_post();

                                    include(lifeline2_ROOT . 'post-format/content-standard.php');
                                }
                            }
                            echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '</div>' : '';
                            ?>
                        </div>
                    </div><!-- Blog List -->
                    <?php
                    lifeline2_Common::lifeline2_pagination();
                    ?>
                </div>
                <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'right' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
