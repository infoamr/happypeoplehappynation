<?php
get_header();
$queried_object = get_queried_object();
$settings = lifeline2_get_theme_options();
$sidebar = (lifeline2_set($settings, 'causes_cat_sidebar') != 'full') ? lifeline2_set($settings, 'causes_cat_sidebar') : '';
$position = (lifeline2_set($settings, 'causes_cat_layout') != 'full') ? lifeline2_set($settings, 'causes_cat_layout') : '';

$span = (!empty($sidebar) && !empty($position) ) ? 'col-md-9' : 'col-md-12';
$inner_col = (lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-1-2-col' || lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-2-2-col') ? 'col-md-6' : 'col-md-4';
$background = (lifeline2_set(lifeline2_set($settings, 'causes_cat_title_section_bg'), 'background-image')) ? 'style="background: url(' . lifeline2_set(lifeline2_set($settings, 'causes_cat_title_section_bg'), 'background-image') . ') repeat scroll 50% 422.28px transparent;"' : '';
$title = lifeline2_set($queried_object, 'name');
if (lifeline2_set($settings, 'causes_cat_show_title_section'))
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($title, $background, true));
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <?php
                    if (have_posts()):
                        wp_enqueue_script(array('lifeline2_' . 'froogaloop', 'lifeline2_' . 'knob'));
                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                        $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                        if ($cuurency_formate == 'select'):
                            $donation_needed = $donationNeededUsd;
                        else:
                            $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                        endif;
                        ?>
                        <?php if (lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-1-2-col' || lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-1-3-col'): ?>
                            <div class="help-needed">
                                <div class="row">
                                    <?php while (have_posts()):the_post(); ?>
                                        <div class="<?php echo esc_attr($inner_col); ?>">
                                            <div class="help">
                                                <div class="help-img">
                                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                                    <?php else: ?>
                                                        <?php the_post_thumbnail('full'); ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="help-detail">
                                                    <?php if (lifeline2_set($settings, 'cause_cat_cats')): ?>
                                                        <div class="cats">
                                                            <?php lifeline2_Common::lifeline2_get_post_categories(get_the_ID(), 'causes_category', ','); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <h3><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php echo esc_html(get_the_title(get_the_ID())); ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'causes_cat_title_limit', 30))) ?></a></h3>
                                                    <?php
                                                    if (lifeline2_set($settings, 'causes_cat_donation')) {
                                                        if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                            $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                            $queryParams = array('data_donation' => 'lif_causes', 'postId' => get_the_id());
                                                            ?>
                                                            <a class="donate-btn call-popup" itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                                <span><img itemprop="image" src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/resource/donate-icon.png'); ?>" alt="" /></span>
                                                                <i><?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?></i>     
                                                            </a>
                                                            <?php
                                                        elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                            $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                            ?>
                                                            <a class="donate-btn call-popup" itemprop="url" href="<?php echo esc_url($url) ?>"  title="">
                                                                <span><img itemprop="image" src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/resource/donate-icon.png'); ?>" alt="" /></span>
                                                                <i><?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?></i>
                                                            </a>
                                                            <?php
                                                        else:
                                                            ?>

                                                            <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="donate-btn call-popup donation-modal-box-caller" href="javascript:void(0)" title="">
                                                                <span><img itemprop="image" src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/resource/donate-icon.png'); ?>" alt="" /></span>
                                                                <i><?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?></i>
                                                            </a>
                                                        <?php
                                                        endif;
                                                    }
                                                    ?>
                                                </div>
                                            </div><!-- Help -->
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        <?php elseif (lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-2-2-col' || lifeline2_set($settings, 'causes_cat_listing_type') == 'gird-2-3-col'): ?>
                            <div class="needed">
                                <div class="row">
                                    <?php while (have_posts()): the_post(); ?>
                                        <?php
                                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                                        $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                                        if ($cuurency_formate == 'select'):
                                            $donation_needed = $donationNeededUsd;
                                        else:
                                            $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                                        endif;
                                        ?>
                                        <div class="<?php echo esc_attr($inner_col); ?>">
                                            <div class="needed-donation">
                                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                                <?php else: ?>
                                                    <?php the_post_thumbnail('full'); ?>
                                                <?php endif; ?>
                                                <div class="overlay-title"><h3><a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php echo esc_html(get_the_title(get_the_ID())); ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'causes_cat_title_limit', 30))) ?></a></h3></div>
                                                <?php if (lifeline2_set($settings, 'causes_cat_donation')): ?>
                                                    <div class="overlay-donation">
                                                        <span><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html(round($donation_needed, 0)) ?></span>
                                                        <?php
                                                        if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                            $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                            $queryParams = array('data_donation' => 'lif_causes', 'postId' => get_the_id());
                                                            ?>
                                                            <a class="theme-btn" itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                                <?php echo (lifeline2_set($settings, 'causes_cat_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : sc_html_e('Donate Now', 'lifeline2') ?>
                                                            </a>
                                                            <?php
                                                        elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                            $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                            ?>
                                                            <a class="theme-btn" itemprop="url" href="<?php echo esc_url($url) ?>" title="">
                                                                <?php echo (lifeline2_set($settings, 'causes_cat_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : sc_html_e('Donate Now', 'lifeline2') ?>
                                                            </a>
                                                            <?php
                                                        else:
                                                            ?>
                                                            <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="theme-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                                                <?php echo (lifeline2_set($settings, 'causes_cat_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : sc_html_e('Donate Now', 'lifeline2') ?>
                                                            </a>
                                                        <?php
                                                        endif;
                                                        ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div><!-- Needed Donation -->
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="urgent-cause-list">
                                <?php while (have_posts()):the_post(); ?>
                                    <?php
                                    $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'causes');
                                    $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                                    $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                                    $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                                    if ($cuurency_formate == 'select'):
                                        $donation_needed = $donationNeededUsd;
                                    else:
                                        $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                                    endif;
                                    $donation_collected = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', false);
                                    $percent = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'causes', true);
                                    $donation_percentage = $percent;
                                    ?>
                                    <div class="urgent-cause">
                                        <div class="row">
                                            <div class="col-md-6 column">
                                                <div class="urgentcause-detail">
                                                    <?php if (lifeline2_set($meta, 'location')): ?>
                                                        <span><i class="fa fa-map-marker"></i><?php echo esc_html(lifeline2_set($meta, 'location')); ?></span>
                                                    <?php endif; ?>
                                                    <h3><a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>"><?php the_title(); ?></a></h3>
                                                    <p><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), lifeline2_set($settings, 'causes_cat_content_limit', 150), '')); ?></p>
                                                    <?php if (lifeline2_set($settings, 'causes_cat_donation')): ?>
                                                        <div class="urgent-progress">
                                                            <div class="row">
                                                                <div class="col-md-4"><div class="amount"><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html(round($donation_collected, 0)) ?><span><?php esc_html_e('CURRENT COLLECTION', 'lifeline2') ?></span></div></div>
                                                                <div class="col-md-4">
                                                                    <div class="circular">
                                                                        <input class="knob" data-fgColor="#e47257" data-bgColor="#dddddd" data-thickness=".10" readonly value="<?php echo esc_attr($donation_percentage) ?>"/>
                                                                        <?php
                                                                        if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                                            $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                                            $queryParams = array('data_donation' => 'causes', 'postId' => get_the_id());
                                                                            ?>
                                                                            <a itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                                                <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>
                                                                            </a>
                                                                            <?php
                                                                        elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                                            $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                                            ?>
                                                                            <a itemprop="url" href="<?php echo esc_url($url) ?>" title="">
                                                                                <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>    
                                                                            </a>
                                                                            <?php
                                                                        else:
                                                                            ?>
                                                                            <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="donation-modal-box-caller" href="javascript:void(0)" title="">
                                                                                <?php echo (lifeline2_set($settings, 'causes_template_btn_label')) ? lifeline2_set($settings, 'causes_template_btn_label') : esc_html__('Donate Now', 'lifeline2'); ?>
                                                                            </a>
                                                                        <?php
                                                                        endif;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4"><div class="amount"><i><?php echo esc_html($symbol) ?></i> <?php echo esc_html(round($donation_needed, 0)) ?><span><?php esc_html_e('Target Needed', 'lifeline2') ?></span></div></div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <?php if (lifeline2_set($meta, 'cause_format') == 'gallery'): ?>
                                                <?php
                                                $images = lifeline2_set($meta, 'cause_images');
                                                $grids = array(7, 5, 5, 7);
                                                $sizes = array(array('width' => 368, 'height' => 200), array('width' => 270, 'height' => 270), array('width' => 270, 'height' => 270), array('width' => 368, 'height' => 200));
                                                ?>
                                                <?php if (!empty($images)): ?>
                                                    <div class="col-md-6 column">
                                                        <div class="urgentcause-gallery masonary lightbox">
                                                            <?php $i = 0; ?>
                                                            <?php foreach ($images as $k => $v): ?>
                                                                <div class="col-md-<?php echo esc_attr($grids[$i]); ?>"><a itemprop="url" href="<?php echo esc_url(wp_get_attachment_url($k, 'full')); ?>" title="">
                                                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($k, 'full'), $sizes[$i]['width'], $sizes[$i]['height'], true)); ?>
                                                                        <?php else: ?>
                                                                            <?php echo wp_get_attachment_image($k, 'full'); ?>
                                                                        <?php endif; ?>   
                                                                    </a></div>
                                                                <?php if ($i == 3) break; ?>
                                                                <?php $i++; ?>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php elseif (lifeline2_set($meta, 'cause_format') == 'video'): ?>
                                                <div class="col-md-6 column">
                                                    <div class="video">
                                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                                        <?php else: ?>
                                                            <?php the_post_thumbnail('full'); ?>
                                                        <?php endif; ?>
                                                        <a itemprop="url" class="play" href="" title=""></a>
                                                        <a itemprop="url" class="pause" href="" title=""></a>
                                                        <?php echo balanceTags(lifeline2_set($meta, 'causes_video')); ?>
                                                    </div>
                                                </div>	<!--Video -->
                                            <?php elseif (lifeline2_set($meta, 'cause_format') == 'slider'): ?>
                                                <?php $images = lifeline2_set($meta, 'cause_images'); ?>
                                                <?php wp_enqueue_script(array('lifeline2_' . 'owl-carousel')); ?>
                                                <div class="col-md-6 column">
                                                    <div class="image-carousel">
                                                        <?php foreach ($images as $k => $v): ?>
                                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($k, 'full'), 800, 519, true)); ?>
                                                            <?php else: ?>
                                                                <?php echo wp_get_attachment_image($k, 'full'); ?>
                                                            <?php endif; ?>   
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>	<!--Video -->
                                                <?php
                                                $jsOutput = "jQuery(document).ready(function ($) {
                                                        $('.image-carousel').owlCarousel({
                                                            autoplay: true,
                                                            autoplayTimeout: 2500,
                                                            smartSpeed: 2000,
                                                            autoplayHoverPause: true,
                                                            loop: true,
                                                            dots: false,
                                                            nav: false,
                                                            margin: 0,
                                                            mouseDrag: true,
                                                            items: 1,
                                                            singleItem: true,
                                                            autoHeight: true
                                                        });
                                                    });";
                                                wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
                                            else:
                                                ?>
                                                <div class="col-md-6 column">
                                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                                    <?php else: ?>
                                                        <?php the_post_thumbnail('full'); ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                                ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ($sidebar && $position == 'right') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?> 
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
