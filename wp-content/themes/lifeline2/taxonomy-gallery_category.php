<?php
get_header();
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
$sidebar        = lifeline2_set( $settings, 'gallery_cat_sidebar' );
$position       = lifeline2_set( $settings, 'gallery_cat_layout' );
$span           = ( $sidebar && lifeline2_set( $settings, 'gallery_cat_columns' ) != 'col-md-3' ) ? 'col-md-9' : 'col-md-12';
//printr($position);
$background     = (lifeline2_set( lifeline2_set( $settings, 'gallery_cat_title_section_bg' ), 'background-image' )) ? 'style="background: url(' . lifeline2_set( lifeline2_set( $settings, 'gallery_cat_title_section_bg' ), 'background-image' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$title          = (lifeline2_set( $settings, 'gallery_cat_title' )) ? lifeline2_set( $settings, 'gallery_cat_title' ) : $queried_object->name;
if ( lifeline2_set( $settings, 'gallery_cat_show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $title, $background, true ) );
$cols           = (lifeline2_set( $settings, 'gallery_cat_columns' )) ? lifeline2_set( $settings, 'gallery_cat_columns' ) : 'col-md-4';
$size           = array( array('width'=>387,'height'=>415), array('width'=>770,'height'=>562), array('width'=>387,'height'=>415), array('width'=>387,'height'=>415), array('width'=>770,'height'=>562), array('width'=>770,'height'=>562));
wp_enqueue_script( array( 'lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize' ) );
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' && lifeline2_set( $settings, 'gallery_cat_columns' ) != 'col-md-3' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                    <?php if ( $sidebar && $position == 'left' || $sidebar && $position == 'right'): ?>
                        <div class="<?php echo esc_attr( $span ); ?> column">
                    <?php else: ?>
                        <div class="col-md-12 column">
                    <?php endif; ?>
                    <div class="gallery-page<?php echo (lifeline2_set( $settings, 'gallery_cat_style' ) == 'grid-view-title' || lifeline2_set( $settings, 'gallery_cat_style' ) == 'grid-view-notitle') ? ' masonary' : ''; ?>">
                        <?php
                        $i = 0;
                        while ( have_posts() ): the_post();
                            $meta        = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'gallery' );
                            $gal_video   = get_post_meta( get_the_ID(), 'lifeline2_videos', true );
                            $gal_img     = get_post_meta( get_the_ID(), 'lifeline2_gal_ids', true );
                            $converArray = explode( ',', $gal_img );
                            ?>
                            <?php if ( lifeline2_set( $settings, 'gallery_cat_style' ) == 'grid-view-title' ): ?>
                                <?php $title_limit = (lifeline2_set( $settings, 'gallery_cat_title_limit' )) ? lifeline2_set( $settings, 'gallery_cat_title_limit' ) : 30; ?>
                                <div class="<?php echo esc_attr( $cols ); ?>">
                                    <div class="gallery">
                                        <div class="gallery-img">
                                            <a title="<?php the_title(); ?>" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" itemprop="url">
                                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $size[$i]['width'], $size[$i]['height'], true)); ?>
                                                <?php else: ?>
                                                    <?php the_post_thumbnail('full'); ?>
                                                <?php endif; ?>
                                                
                                            </a>
                                        </div>
                                        <div class="gallery-detail">
                                            <?php if ( lifeline2_set( $settings, 'gallery_cat_categories' ) ): ?>
                                                <ul>
                                                    <?php lifeline2_Common::lifeline2_get_post_categories( get_the_ID(), 'gallery_category', '', 'list' ); ?>
                                                </ul>
                                            <?php endif; ?>
                                            <h3><a title="" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" itemprop="url"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), $title_limit ) ); ?></a></h3>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif ( lifeline2_set( $settings, 'gallery_cat_style' ) == 'grid-view-notitle' ): ?>
                                <div class="<?php echo esc_attr( $cols ); ?>">
                                    <div class="gallery">
                                        <div class="gallery-img">
                                            <a title="<?php the_title(); ?>" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" itemprop="url"><?php the_post_thumbnail( $size[$i] ); ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="modern-gallery">
                                    <div class="mod-gallery-img">
                                        <?php the_post_thumbnail( '768x356' ); ?>
                                        <?php if ( lifeline2_set( $settings, 'gallery_cat_video_count' ) || lifeline2_set( $settings, 'gallery_cat_image_count' ) ): ?>
                                            <div class="mod-gallery-info">
                                                <?php if ( lifeline2_set( $settings, 'gallery_cat_image_count' ) ): ?>
                                                    <a href="javascript:void(0)" title=""><i class="ti-instagram"></i><?php echo count( $converArray ) ?> <?php esc_html_e( 'Images', 'lifeline2' ) ?></a>
                                                <?php endif; ?>
                                                <?php if ( lifeline2_set( $settings, 'gallery_cat_video_count' ) ): ?>
                                                    <a href="javascript:void(0)" title=""><i class="fa fa-play-circle"></i><?php echo count( $gal_video ) ?> <?php esc_html_e( 'Videos', 'lifeline2' ) ?></a>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="mod-gallery-detail">
                                        <h3><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h3>
                                        <?php if ( lifeline2_set( $settings, 'gallery_cat_date' ) ): ?>    
                                            <ul class="meta">
                                                <li content="<?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?></li>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $i++; ?>
                            <?php if ( $i == 6 ) $i = 0; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' && lifeline2_set( $settings, 'gallery_cat_columns' ) != 'col-md-3' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
