<?php
get_header();
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
$sidebar        = (lifeline2_set( $settings, 'story_cat_sidebar' )) ? lifeline2_set( $settings, 'story_cat_sidebar' ) : '';
$position       = (lifeline2_set( $settings, 'story_cat_layout' ) != 'full') ? lifeline2_set( $settings, 'story_cat_layout' ) : '';
$span           = (!empty( $sidebar ) && !empty( $position ) ) ? 'col-md-9' : 'col-md-12';
$style          = (lifeline2_set( $settings, 'story_cat_column' )) ? lifeline2_set( $settings, 'story_cat_column' ) : '';
//printr(lifeline2_set($settings, 'story_cat_column'));
$background     = (lifeline2_set( lifeline2_set( $settings, 'story_cat_title_section_bg' ), 'background-image' )) ? 'style="background: url(' . lifeline2_set( lifeline2_set( $settings, 'story_cat_title_section_bg' ), 'background-image' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$title          = lifeline2_set( $queried_object, 'name' );
$inner_col      = (lifeline2_set( $settings, 'story_cat_column' ) == 'style1-2-col' || lifeline2_set( $settings, 'story_cat_column' ) == 'style2-2-col') ? 'col-md-6' : 'col-md-4';
if ( lifeline2_set( $settings, 'story_cat_show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $title, $background, true ) );
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>

                <div class="<?php echo esc_attr( $span ); ?> column">
                    <?php if ( $style == 'style1-2-col' || $style == 'style1-3-col' ) { ?>
                        <div class="successful-stories">
                            <div class="row">
                            <?php } elseif ( $style == 'style2-2-col' || $style == 'style2-3-col' ) { ?>
                                <div class="fancy-projects">
                                    <div class="row">
                                        <?php
                                    }
                                    while ( have_posts() ): the_post();
                                        $meta                = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'story' );
                                        $symbol              = (lifeline2_set( $meta, 'symbol' )) ? lifeline2_Currencies::get_instance()->wpd_symbols( lifeline2_set( $meta, 'symbol' ) ) : '$';
                                        $donation_needed     = (lifeline2_set( $meta, 'project_cost' )) ? lifeline2_set( $meta, 'project_cost' ) : 0;
                                        $donation_collected  = (lifeline2_set( $meta, 'donation_collected' )) ? lifeline2_set( $meta, 'donation_collected' ) : 0;
                                        $percent             = ($donation_needed) ? ( int ) str_replace( ',', '', $donation_collected ) / ( int ) str_replace( ',', '', $donation_needed ) : 0;
                                        $donation_percentage = round( $percent * 100, 0 );
                                        if ( $style == 'style1-2-col' || $style == 'style1-3-col' ) {
                                            ?>
                                            <div class="<?php echo esc_attr( $inner_col ) ?>">
                                                <div class="story">
                                                    <div class="story-img">
                                                        <?php the_post_thumbnail( 'medium' ) ?>
                                                        <a itemprop="url" href="#" title="">+</a>
                                                    </div>
                                                    <div class="story-detail">
                                                        <span><?php echo esc_html( lifeline2_set( $meta, 'location' ) ) ?></span>
                                                        <h3><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), lifeline2_set( $settings, 'story_cat_title_limit', 30 ) ) ) ?></a></h3>
                                                    </div>
                                                    <?php if ( lifeline2_set( $settings, 'story_cat_budget' ) ): ?>
                                                        <div class="spent-bar">
                                                            <span><?php echo (lifeline2_set( $settings, 'story_cat_budget_title' )) ? lifeline2_set( $settings, 'story_cat_budget_title' ) : esc_html_e( 'Money Spent', 'lifeline2' ); ?></span>
                                                            <span class="price"><i><?php echo esc_html( $symbol ) ?></i><?php echo esc_html( $donation_needed ) ?></span>
                                                        </div>
                                                    <?php endif; ?>
                                                </div><!-- Story -->
                                            </div>

                                            <?php
                                        } elseif ( $style == 'style2-2-col' || $style == 'style2-3-col' ) {
                                            ?>
                                            <div class="<?php echo esc_attr( $inner_col ) ?>">
                                                <div class="urgent">
                                                    <?php the_post_thumbnail( 'medium' ) ?>
                                                    <div class="urgent-detail">
                                                        <div class="cause-title">
                                                            <h5><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), lifeline2_set( $settings, 'story_cat_title_limit', 30 ) ) ) ?></a></h5>
                                                        </div>

                                                        <?php if ( lifeline2_set( $settings, 'story_cat_donation' ) ): ?>
                                                            <div class="cause-donation">
                                                                <span class="collected-amount"><i><?php echo esc_html( $symbol ) ?></i><?php echo esc_html( $donation_needed ) ?></span>
                                                                <i><?php esc_html_e( 'Spent Amount', 'lifeline2' ) ?></i>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><!-- Urgent Cause -->
                                            </div>

                                            <?php
                                        }
                                        ?>

                                    <?php endwhile; ?>
                                </div>
                                <?php if ( $style == 'style2-2-col' || $style == 'style2-3-col' || $style == 'style1-2-col' || $style == 'style1-3-col' ) { ?>
                                </div>
                            </div>
                            <?php
                        }

                        if ( $sidebar && $position == 'right' ) :
                            ?>
                            <div class="col-md-3 sidebar">
                                <?php dynamic_sidebar( $sidebar ); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            </section>

            <?php
            get_footer();
            