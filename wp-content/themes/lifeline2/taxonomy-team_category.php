<?php
get_header();
 if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
$sidebar        = lifeline2_set( $settings, 'team_member_cat_sidebar' );
$position       = lifeline2_set( $settings, 'team_member_cat_layout' );
$span           = (!empty( $sidebar ) && ( $position != 'full' ) ) ? 'col-md-9' : 'col-md-12';
$inner_col      = (lifeline2_set( $settings, 'team_member_cat_column' )) ? lifeline2_set( $settings, 'team_member_cat_column' ) : 'col-md-6';
$background     = (lifeline2_set( lifeline2_set( $settings, 'team_member_cat_title_section_bg' ), 'background-image' )) ? 'style="background: url(' . lifeline2_set( lifeline2_set( $settings, 'team_member_cat_title_section_bg' ), 'background-image' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$title          = (lifeline2_set( $settings, 'team_member_cat_title' )) ? lifeline2_set( $settings, 'team_member_cat_title' ) : $queried_object->name;
$posts_per_page = (lifeline2_set($settings, 'team_category_pagination_num') && lifeline2_set($settings, 'team_category_pagination')) ? lifeline2_set($settings, 'team_category_pagination_num') : -1;
$ordertitle     = (lifeline2_set( $settings, 'team_orderby' )) ? lifeline2_set( $settings, 'team_orderby' ) : 'title';
$order          = (lifeline2_set( $settings, 'team_order' )) ? lifeline2_set( $settings, 'team_order' ) : 'ASC';

$args = array(
	'post_type'         => 'lif_team',
	'post_status'       => 'publish',
	'posts_per_page'    => $posts_per_page,
	'orderby'           => $ordertitle,
	'order'             => $order,
	'paged'             => $paged
);
if ( lifeline2_set( $settings, 'team_member_cat_show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $title, $background, true ) );
?>
<?php
$query = new WP_Query($args);
//printr($paged);
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                <?php if ($query->have_posts()): ?>
                <div class="members">
                        <div class="row">
                            <?php
                            while ($query->have_posts()): $query->the_post();
                                $meta   = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'team' );
                                $social = get_post_meta( get_the_ID(), 'social_media_icons', true );
                                ?>
                                 <?php  
                        if ($inner_col == 3) {
                            $width = 297;
                            $height = 297;
                        } else {
                            $width = 436;
                            $height = 436;
                        } 
                    ?>
                                <div class="<?php echo esc_attr( $inner_col ) ?>">
                                    <div class="team-member" itemscope itemtype="http://schema.org/Person">
                                        <div class="member-img">
                                             <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true)); ?>
                                    </a>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                            <?php if ( lifeline2_set( $settings, 'team_member_cat_social_media' ) ): ?>
                                                <div class="member-social">
                                                    <span><?php echo (lifeline2_set( $settings, 'team_member_cat_social_media_title' )) ? lifeline2_set( $settings, 'team_member_cat_social_media_title' ) : esc_html__( 'Get In Touch', 'lifeline2' ); ?></span>
                                                    <?php if ( !empty( $social ) && count( $social ) > 0 ): ?>
                                                        <div class="social-links">
                                                            <?php
                                                            foreach ( $social as $s ) {
                                                                echo '<a itemprop="url" href="' . esc_url( lifeline2_set( $s, 'social_link' ) ) . '" title="' . lifeline2_set( $s, 'social_title' ) . '"><i class="' . lifeline2_Header::lifeline2_get_icon( lifeline2_set( $s, 'social_icon' ) ) . lifeline2_set( $s, 'social_icon' ) . '"></i></a>';
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div><!-- Member Image -->
                                        <h4 itemprop="name"><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), lifeline2_set( $settings, 'team_member_cat_title_limit', 30 ) ) ) ?></a></h4>
                                        <i itemprop="jobTitle"><?php echo esc_html( lifeline2_set( $meta, 'designation' ) ) ?></i>
                                    </div><!-- Team Member -->
                                </div>

                            <?php endwhile; ?>
                        </div>
                    </div>

	                <?php
	                if (lifeline2_set($settings, 'team_category_pagination')) {
		                //lifeline2_Common::lifeline2_pagination($query->max_num_pages);
	                }

                endif;
	                wp_reset_postdata();
	                ?>
                </div>

                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
