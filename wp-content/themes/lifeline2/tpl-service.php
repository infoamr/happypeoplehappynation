<?php
// Template Name: Services Listing

get_header();
$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' ); //printr($page_meta);
$settings   = lifeline2_get_theme_options();
$sidebar    = lifeline2_set( $page_meta, 'metaSidebar' );
$position   = lifeline2_set( $page_meta, 'layout' );
$span       = ( $sidebar && $position != 'full' ) ? 'col-md-9' : 'col-md-12';
$background = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$page_title = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
if ( lifeline2_set( $page_meta, 'show_title_section' ) ) {
    echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, true ) );
}
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
$text_limit     = (lifeline2_set( $settings, 'services_template_text_limit' )) ? lifeline2_set( $settings, 'services_template_text_limit' ) : 150;
$posts_per_page = (lifeline2_set( $settings, 'services_template_pagination_num' )) ? lifeline2_set( $settings, 'services_template_pagination_num' ) : -1;

$args  = array(
    'post_type'      => 'lif_service',
    'post_status'    => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged'          => $paged
);
$query = new WP_Query( $args );
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <div class="services-listing">
                        <?php
                        while ( $query->have_posts() ): $query->the_post();
                            $meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' );
                            ?>
                            <div class="service">
                                <div class="service-img">
                                    <?php if ( lifeline2_set( $settings, 'service_template_icon' ) ): ?>
                                        <span>
                                            <i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $meta, 'serive_icon' ) ) ) . esc_attr( lifeline2_set( $meta, 'serive_icon' ) ) ?>"></i>
                                        </span>
                                    <?php endif; ?>
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 370, 303, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="service-detail">
                                    <?php if ( lifeline2_set( $settings, 'service_template_tag_line' ) ): ?>
                                        <span><?php echo esc_html( lifeline2_set( $meta, 'tag_line' ) ) ?></span>
                                    <?php endif; ?>
                                    <h3><a href="<?php echo esc_url( get_the_permalink() ) ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h3>
                                    <p><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content(), $text_limit ) ) ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php
                        if ( lifeline2_set( $settings, 'services_template_pagination' ) ) {
                            lifeline2_Common::lifeline2_pagination( $query->max_num_pages );
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
