<?php
// Template Name: Story Listing

get_header();
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page');
$settings = lifeline2_get_theme_options();
$sidebar = lifeline2_set($page_meta, 'metaSidebar');
$position = lifeline2_set($page_meta, 'layout');
$span = ( $sidebar) ? 'col-md-9' : 'col-md-12';
$style = (lifeline2_set($settings, 'story_template_column')) ? lifeline2_set($settings, 'story_template_column') : '';
$inner_col = (lifeline2_set($settings, 'story_template_column') == 'style1-2-col' || lifeline2_set($settings, 'story_template_column') == 'style2-2-col') ? 'col-md-6' : 'col-md-4';
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
if (lifeline2_set($page_meta, 'show_title_section')) {
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, true));
}
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
$posts_per_page = (lifeline2_set($settings, 'stories_template_pagination_num') && lifeline2_set($settings, 'story_template_pagination')) ? lifeline2_set($settings, 'stories_template_pagination_num') : -1;

$args = array(
    'post_type' => 'lif_story',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged
);
$query = new WP_Query($args);
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <?php if ($style == 'style1-2-col' || $style == 'style1-3-col') { ?>
                        <div class="successful-stories">
                            <div class="row">
                            <?php } elseif ($style == 'style2-2-col' || $style == 'style2-3-col') { ?>
                                <div class="fancy-projects">
                                    <div class="row">
                                        <?php
                                    }
                                    while ($query->have_posts()): $query->the_post();
                                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'story');
                                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                                        $donationNeededUsd = (int) (lifeline2_set($meta, 'project_cost')) ? lifeline2_set($meta, 'project_cost') : 0;
                                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                                        if ($cuurency_formate == 'select'):
                                            $donation_needed = $donationNeededUsd;
                                        else:
                                            $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                                        endif;
                                        if ($style == 'style1-2-col' || $style == 'style1-3-col') {
                                            ?>
                                            <div class="<?php echo esc_attr($inner_col) ?>">
                                                <div class="story">
                                                    <div class="story-img">
                                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                                        <?php else: ?>
                                                            <?php the_post_thumbnail('full'); ?>
                                                        <?php endif; ?>
                                                        <a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php echo esc_attr(get_the_title(get_the_ID())) ?>">+</a>
                                                    </div>
                                                    <div class="story-detail">
                                                        <span><?php echo esc_html(lifeline2_set($meta, 'location')) ?></span>
                                                        <h3><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'story_template_title_limit', 30))) ?></a></h3>
                                                    </div>
                                                    <?php if (lifeline2_set($settings, 'story_template_budget')): ?>
                                                        <div class="spent-bar">
                                                            <span><?php echo (lifeline2_set($settings, 'story_template_budget_title')) ? lifeline2_set($settings, 'story_template_budget_title') : esc_html__('Money Spent', 'lifeline2'); ?></span>
                                                            <span class="price"><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></span>
                                                        </div>
                                                    <?php endif; ?>
                                                </div><!-- Story -->
                                            </div>

                                            <?php
                                        } elseif ($style == 'style2-2-col' || $style == 'style2-3-col') {
                                            ?>
                                            <div class="<?php echo esc_attr($inner_col) ?>">
                                                <div class="urgent">
                                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 800, 519, true)); ?>
                                                    <?php else: ?>
                                                        <?php the_post_thumbnail('full'); ?>
                                                    <?php endif; ?>
                                                    <div class="urgent-detail">
                                                        <div class="cause-title">
                                                            <h5><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                                                    <?php if(!empty(lifeline2_set($settings, 'story_template_title_limit'))){ ?>
                                                                    <?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'story_template_title_limit', 30))) ?>
                                                                <?php }else{
                                                                        echo get_the_title(get_the_ID());
                                                                    } ?>
                                                                </a></h5>
                                                        </div>
                                                        <?php if (lifeline2_set($settings, 'story_tamplate_donation')): ?>
                                                            <div class="cause-donation">
                                                                <i><?php esc_html_e('Money Spent', 'lifeline2') ?></i>
                                                                <span class="collected-amount"><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($donation_needed) ?></span>
                                                            </div>

                                                        <?php endif; ?>
                                                    </div>
                                                </div><!-- Urgent Cause -->
                                            </div>

                                            <?php
                                        }
                                    endwhile;
                                    wp_reset_postdata();
                                    ?>
                                    <?php if ($style == 'style2-2-col' || $style == 'style2-3-col' || $style == 'style1-2-col' || $style == 'style1-3-col') { ?>
                                    </div>
                                </div>
                                <?php
                            }
                            if (lifeline2_set($settings, 'story_template_pagination')) {
                                lifeline2_Common::lifeline2_pagination($query->max_num_pages);
                            }
                            ?>
                        </div>

                        <?php if ($sidebar && $position == 'right' && lifeline2_set($settings, 'story_list_column') != 'col-md-3') : ?>
                            <div class="col-md-3 sidebar">
                                <?php dynamic_sidebar($sidebar); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            </section>
            <?php
            get_footer();
            