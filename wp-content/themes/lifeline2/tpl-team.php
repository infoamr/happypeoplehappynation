<?php
// Template Name: Team Listing

get_header();
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page');
$settings = lifeline2_get_theme_options();
$sidebar = lifeline2_set($page_meta, 'metaSidebar');
$position = lifeline2_set($page_meta, 'layout');
$span = ( $sidebar && $position != 'full') ? 'col-md-9' : 'col-md-12';
$inner_col = (lifeline2_set($settings, 'team_member_template_column')) ? lifeline2_set($settings, 'team_member_template_column') : 'col-md-3';
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
if (lifeline2_set($page_meta, 'show_title_section')) {
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, true));
}
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
$posts_per_page = (lifeline2_set($settings, 'team_member_template_pagination_num') && lifeline2_set($settings, 'team_member_template_pagination')) ? lifeline2_set($settings, 'team_member_template_pagination_num') : -1;

$args = array(
    'post_type' => 'lif_team',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged
);
$query = new WP_Query($args);
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <div class="members">
                        <div class="row">
                            <div class="masonary">
                            <?php
                            while ($query->have_posts()): $query->the_post();
                                $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'team');
                                $social = get_post_meta(get_the_ID(), 'social_media_icons', true);
                                ?>
                                 <?php  
                        if ($inner_col == 4) {
                            $width = 297;
                            $height = 297;
                        } else {
                            $width = 436;
                            $height = 436;
                        } 
                    ?>
                                <div class="<?php echo esc_attr($inner_col) ?>">
                                    <div class="team-member" itemscope itemtype="http://schema.org/Person">
                                        <div class="member-img">

                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <a href="<?php echo get_permalink();?>">
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true)); ?>
                                                </a>
                                                <?php else: ?>

                                                <?php the_post_thumbnail('full'); ?>

                                                <?php endif; ?>

                                            <?php if (lifeline2_set($settings, 'team_member_template_social_media')): ?>
                                                <div class="member-social">
                                                    <span><?php echo (lifeline2_set($settings, 'team_member_template_social_media_title')) ? lifeline2_set($settings, 'team_member_template_social_media_title') : esc_html__('Get In Touch', 'lifeline2'); ?></span>
                                                    <?php if (!empty($social) && count($social) > 0): ?>
                                                        <div class="social-links">
                                                            <?php
                                                            foreach ($social as $s) {
                                                                echo '<a itemprop="url" href="' . esc_url(lifeline2_set($s, 'social_link')) . '" title="' . lifeline2_set($s, 'social_title') . '"><i class="' . lifeline2_Header::lifeline2_get_icon(lifeline2_set($s, 'social_icon')) . lifeline2_set($s, 'social_icon') . '"></i></a>';
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div><!-- Member Image -->
                                        <h4 itemprop="name"><a itemprop="url" href="<?php echo get_the_permalink(get_the_ID()) ?>" title="<?php the_title() ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'team_member_template_title_limit', 30))) ?></a></h4>
                                        <i itemprop="jobTitle"><?php echo esc_html(lifeline2_set($meta, 'designation')) ?></i>
                                    </div><!-- Team Member -->
                                </div>
                                <?php
                            endwhile;
                            if (lifeline2_set($settings, 'team_member_template_pagination')) {
                                lifeline2_Common::lifeline2_pagination($query->max_num_pages);
                            }
                            wp_reset_postdata();
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($sidebar && $position == 'right') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
