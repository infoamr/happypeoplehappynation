<?php
// Template Name: Testimonila Listing

get_header();
$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' ); //printr($page_meta);
$settings   = lifeline2_get_theme_options();
$sidebar    = lifeline2_set( $page_meta, 'metaSidebar' );
$position   = lifeline2_set( $page_meta, 'layout' );
$span       = ( $sidebar ) ? 'col-md-9' : 'col-md-12';
$inner_col  = ($span == 'col-md-9') ? 'col-md-4' : 'col-md-3';
$background = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
if ( lifeline2_set( $page_meta, 'show_title_section' ) ) {
    echo balanceTags( lifeline2_Common::lifeline2_page_top_section( get_the_title( get_the_ID() ), $background, true ) );
}

$args  = array(
    'post_type'   => 'lif_testimonial',
    'post_status' => 'publish'
);
$query = new WP_Query( $args );
wp_enqueue_script( array( 'lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize' ) );
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <div class="testimonials-list">
                        <div class="row">
                            <div class="masonary">
                                <?php
                                while ( $query->have_posts() ): $query->the_post();
                                    ?>
                                    <div class="<?php echo esc_attr( $innter_co ) ?>">
                                        <div class="product clients-testimonials">
                                            <div class="review">
                                                <blockquote>"<?php echo get_the_content() ?>"</blockquote>
                                                <?php the_post_thumbnail( 'thumbnail' ) ?>
                                                <span>- <?php the_title() ?></span>
                                            </div><!-- Review -->
                                        </div><!-- Clients Testimonials -->
                                    </div>
                                    <?php
                                endwhile;
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
