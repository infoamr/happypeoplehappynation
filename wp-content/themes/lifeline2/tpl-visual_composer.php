<?php
/* Template Name: Fullwidth */
get_header();
$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
$page_title = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$background = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
if ( lifeline2_set( $page_meta, 'show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
while ( have_posts() ): the_post();
    echo do_shortcode( the_content() );
endwhile;

get_footer();
