<?php
/**
 * Cart totals
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="cart-box">
    <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>



            <?php //wc_cart_totals_shipping_html(); ?>



    <?php elseif ( WC()->cart->needs_shipping() ) : ?>

            <tr class="shipping">
                    <th><?php esc_html_e( 'Shipping', 'lifeline2' ); ?></th>
                    <td><?php woocommerce_shipping_calculator(); ?></td>
            </tr>

    <?php endif; ?>
    <div class="cart_totals cart-bottom <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">
        <div class="row">
            <div class="col-md-3">
                <div class="cart-price">
                <?php esc_html_e( 'Sub-Total:', 'lifeline2' ); ?>
                <?php wc_cart_totals_subtotal_html(); ?>
                </div>
            </div>
            <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                <div class="col-md-3 cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                    <div class="cart-price">
                        <?php wc_cart_totals_coupon_label( $coupon ); ?>
                        <span><?php wc_cart_totals_coupon_html( $coupon ); ?></span>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
                <div class="fee col-md-3">
                    <div class="cart-price">
                    <?php echo esc_html( $fee->name ); ?>
                    <span><?php wc_cart_totals_fee_html( $fee ); ?></span>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'excl' ) : ?>
                <?php if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) : ?>
                    <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                        <div class="col-md-3 tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
                            <div class="cart-price">
                            <?php echo esc_html( $tax->label ); ?>
                            <span><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
                            </div>
                            </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="tax-total col-md-3">
                        <div class="cart-price">
                        <?php echo esc_html( WC()->countries->tax_or_vat() ); ?>
                        <span><?php wc_cart_totals_taxes_total_html(); ?></span>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="order-total col-md-3 main-total">
                <div class="cart-price">
                <?php esc_html_e( 'Total', 'lifeline2' ); ?>
                <?php wc_cart_totals_order_total_html(); ?>
                </div>
            </div>
            <div class="col-md-3"><?php do_action( 'woocommerce_proceed_to_checkout' ); ?></div>
        </div>
    </div>
</div>


<?php if ( WC()->cart->get_cart_tax() ) : ?>
    <p class="wc-cart-shipping-notice"><small><?php

            $estimated_text = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
                    ? sprintf( ' ' . esc_html__( ' (taxes estimated for %s)', 'lifeline2' ), WC()->countries->estimated_for_prefix() . WC()->countries->countries[ WC()->countries->get_base_country() ] )
                    : '';

            printf( esc_html__( 'Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information.', 'lifeline2' ), $estimated_text );

    ?></small></p>
<?php endif; ?>

<?php do_action( 'woocommerce_after_cart_totals' ); ?>
