<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $opt;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
    $woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
    $woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( !$product || !$product->is_visible() ) {
    return;
}

// Increase loop count
$woocommerce_loop['loop'] ++;
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
    $classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
    $classes[] = 'last';
}
$classes[] = 'product-loop';
if(lifeline2_set( $opt, 'products_style' ) == 'small_image'){
    $width = 387;
    $height = 415;
}else{
    $width = 638;
    $height = 638; 
}
$grid      = (lifeline2_set( $opt, 'products_listing_grid_style' )) ? lifeline2_set( $opt, 'products_listing_grid_style' ) : 'col-md-3';
$limit     = (lifeline2_set( $opt, 'product_title_limit' )) ? lifeline2_set( $opt, 'product_title_limit' ) : 30;
?>
<div class="<?php echo esc_attr( $grid ); ?>">
    <div <?php post_class( $classes ); ?>>
        <div class="product-img">
            <?php if (class_exists('Lifeline2_Resizer')): ?>
                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true)); ?>
            <?php else: ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
           
            <?php
            echo apply_filters( 'woocommerce_loop_add_to_cart_link', sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class=" %s product_type_%s"><i class="ti-shopping-cart"></i></a>', esc_url( $product->add_to_cart_url() ), esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), $product->is_purchasable() ? 'add_to_cart_button' : '', esc_attr( $product->get_type() ), esc_html( $product->add_to_cart_text() )
                ), $product );
            ?>
        </div>
        <h4><a itemprop="url" title="<?php the_title(); ?>" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_title( get_the_ID() ), $limit ) ); ?></a></h4>
        <?php echo balanceTags($product->get_price_html()); ?>
    </div>
</div>
