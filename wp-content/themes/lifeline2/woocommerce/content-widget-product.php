<?php
/**
 * @author      WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
global $product;
?>
<li>
    <a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
        <?php echo balanceTags($product->get_image()); ?>
        <span class="product-title"><?php echo esc_html($product->get_title()); ?></span>
    </a>
    <?php if ( !empty( $show_rating ) ) echo balanceTags($product->get_rating_html()); ?>
    <?php echo balanceTags($product->get_price_html()); ?>
</li>