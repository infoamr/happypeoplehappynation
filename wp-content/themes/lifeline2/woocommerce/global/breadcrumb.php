<?php
/**
 * Shop breadcrumb
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) {

	echo wp_kses($wrap_before, true);

	foreach ( $breadcrumb as $key => $crumb ) {

		echo wp_kses($before, true);

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} else {
			echo esc_html( $crumb[0] );
		}

		echo wp_kses($after, true);

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo balanceTags($delimiter);
		}

	}

	echo wp_kses($wrap_after, true);

}
