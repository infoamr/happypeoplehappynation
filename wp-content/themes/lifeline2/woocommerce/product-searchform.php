<?php
/**
 * @author 	WooThemes
 * @package 	WooCommerce/Templates
 * @version     5.5.0
 */
?>
<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label class="screen-reader-text" for="s"><?php esc_html_e( 'Search for:', 'lifeline2' ); ?></label>
    <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Products', 'placeholder', 'lifeline2' ).'&hellip;'; ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'lifeline2' ); ?>" />
    <input type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'lifeline2' ); ?>" />
    <input type="hidden" name="post_type" value="product" />
</form>
