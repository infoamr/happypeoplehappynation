<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
get_header( 'shop' );
?>
<?php
$page_meta     = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
$sidebar       = (lifeline2_set( $page_meta, 'metaSidebar' )) ? lifeline2_set( $page_meta, 'metaSidebar' ) : '';
$layout        = (lifeline2_set( $page_meta, 'layout' )) ? lifeline2_set( $page_meta, 'layout' ) : '';
$page_title    = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$background    = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$top_section   = lifeline2_set( $page_meta, 'show_title_section' );
$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
$span          = ( $sidebar ) ? 'col-md-9' : 'col-md-12';
if ( $top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
if ( $sidebar && $layout == 'left' ) $sidebar_class = 'left-sidebar ';
elseif ( $sidebar && $layout == 'right' ) $sidebar_class = 'right-sidebar ';
else $sidebar_class = '';
?>
<section itemscope itemtype="http://schema.org/BlogPosting">
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $layout == 'left' && is_active_sidebar( $sidebar ) ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $sidebar_class ); ?><?php echo esc_attr( $span ); ?> column">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php wc_get_template_part( 'content', 'single-product' ); ?>
                    <?php endwhile; // end of the loop. ?>

                    <?php
                    /**
                     * woocommerce_after_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    //do_action( 'woocommerce_after_main_content' );
                    ?>
                </div>
                <?php if ( $sidebar && $layout == 'right' && is_active_sidebar( $sidebar ) ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer( 'shop' ); ?>
