<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

$settings  = lifeline2_get_theme_options();
if ( lifeline2_set( $settings, 'optIsProductSocial' ) ) {
    $social_icons = lifeline2_set( $settings, 'optProductSocial' );
    if ( !empty( $social_icons ) ):
        ?>
        <div class="share-this">
            <?php echo (lifeline2_set( $settings, 'optShareTitle' )) ? '<span>' . lifeline2_set( $settings, 'optShareTitle' ) . '</span>' : ''; ?>
            <?php
            $socialArray = array();
            foreach ( $social_icons as $k => $v ) {
                if ( $v == '1' ) {
                    $socialArray[] = $k;
                }
            }
            if ( !empty( $socialArray ) && count( $socialArray ) > 0 ) {
                foreach ( $socialArray as $icon ) {
                    lifeline2_Common::lifeline2_social_share_output( strtolower( $icon ) );
                }
            }
            ?>
        </div>
        <?php
    endif;
}
?>

<div class="product_meta">

    <?php do_action( 'woocommerce_product_meta_start' ); ?>

    <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

        <span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'lifeline2' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'lifeline2' ); ?></span></span>

    <?php endif; ?>

    <?php echo balanceTags(wc_get_product_category_list( ', ', '<span class="posted_in">' . _n( 'Category', 'Categories', $cat_count, 'lifeline2' ) . ': ', '</span>' )); ?>

    <?php echo balanceTags(wc_get_product_tag_list ( '</span><span>', '<span class="tagges_in tagclouds">' . _n( 'Tag:', 'Tags:', $tag_count, 'lifeline2' ) . '<span>', '</span></span>' )); ?>

    <?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
